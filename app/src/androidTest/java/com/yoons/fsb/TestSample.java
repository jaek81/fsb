package com.yoons.fsb;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;
import android.widget.Button;

import com.yoons.fsb.student.ui.StudyBookSelectActivity;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.uiautomator.Until.hasObject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by 임효영 on 2017-10-11.
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class TestSample {
    private UiDevice mDevice;
    private static final int LAUNCH_TIMEOUT = 5000;
    private static final String BASIC_SAMPLE_PACKAGE = "com.yoons.fsb.student";


    @Before
    public void init() throws Exception {

        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
//        mDevice.wait(hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);    // Clear out any previous instances
        context.startActivity(intent);


    }

    @Test
    public void testcase() throws Exception {

        final String p_name = "com.yoons.fsb.student";

        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());


        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(p_name);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()).waitForIdle();


     /*   try {
            mDevice.wait(hasObject(pkg(p_name).depth(0)), 10000);

        } catch (Exception e) {
            e.printStackTrace();
            mDevice.wait(hasObject(pkg(p_name).depth(0)), 10000);
        }*/

        final String btn_login = "com.yoons.fsb.student:id/login_btn";

        BySelector btn_login_selector = By.res(btn_login);

        //버튼이 나타나기 까지 2초간 대기
        boolean waitForResult = mDevice.wait(hasObject(btn_login_selector), 500000000);

        assertThat(waitForResult, Matchers.is(true));

        UiObject2 loginObject = mDevice.findObject(btn_login_selector);

        boolean clickAndWaitForResult = loginObject.clickAndWait(Until.newWindow(), 5000);
        Log.i("hy", "hy" + clickAndWaitForResult);


        //학습 목록 이동
        mDevice.wait(Until.hasObject(By.pkg(StudyBookSelectActivity.class.getPackage().getName()).depth(0)), LAUNCH_TIMEOUT);


        //학습 목록
      /*  UiObject list = mDevice.findObject(new UiSelector().resourceId("com.yoons.fsb.student:id/book_listview").instance(0));
        list.click();*/

        UiObject listview = mDevice.findObject(new UiSelector().className("android.widget.ListView"));
        listview.getChild(new UiSelector().clickable(true).index(0)).click();


        mDevice.wait(Until.hasObject(By.text("OK").clazz(Button.class)), LAUNCH_TIMEOUT);

        UiObject2 okButton2 = mDevice.findObject(By.text("OK").clazz(Button.class));
        okButton2.click();

    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }
}
