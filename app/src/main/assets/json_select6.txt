﻿{
    "out1": [
        {
            "is_auto_artt": 1,
            "is_dictation": 1,
            "is_caption": 1,
            "is_fast_rewind": 1,
            "customer_name": "M060_01",
            "customer_no": 20001282,
            "is_review_book": 1,
            "three_type": 1,
            "is_studyguide": ""
        }
    ],
    "out3": [
        {
            "st_code": 28,
            "procdate": "2015-10-0610: 51: 58",
            "restudycnt": 1,
            "acode": 1000,
            "teststate": "A",
            "studyexamyn": 0,
            "itemcode": 48087,
            "endofbook": 0,
            "studyunitcode": "01A",
            "studyplanymd": "2015-10-06",
            "tcode": 200070,
            "studyconfirm": "",
            "mcode": 20001282,
            "telmanymd": ""
        }
    ],
    "out4": [
        {
            "restudy_movie_cnt": 0,
            "study_unit_code": "01A",
            "item_no": 2,
            "movie_cnt": 0,
            "series_name": "윤선생SMART중학영어1학년",
            "product_no": 48087,
            "is_cdn_audio_file": 1,
            "is_cdn_b_file": 1,
            "dictation_cnt": 5,
            "series_no": 5881,
            "study_order": 1,
            "is_exam_zip": 1
        }
    ],
    "out5": [
        {
            "question_order": 1,
            "sm_word_question_no": 117267,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "answer": "4",
            "distractor_4": "고슴도치",
            "distractor_3": "코뿔소",
            "product_no": 48087,
            "distractor_2": "호랑이",
            "meaning": "고슴도치",
            "question": "hedgehog",
            "distractor_1": "사슴"
        },
        {
            "question_order": 2,
            "sm_word_question_no": 117268,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "answer": "2",
            "distractor_4": "사진가",
            "distractor_3": "사업가",
            "product_no": 48087,
            "distractor_2": "용의자",
            "meaning": "용의자",
            "question": "suspect",
            "distractor_1": "변호사"
        },
        {
            "question_order": 3,
            "sm_word_question_no": 117269,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "answer": "1",
            "distractor_4": "~을구하다",
            "distractor_3": "~을버리다",
            "product_no": 48087,
            "distractor_2": "~을믿다",
            "meaning": "~을찾다",
            "question": "look for",
            "distractor_1": "~을찾다"
        },
        {
            "question_order": 4,
            "sm_word_question_no": 117270,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "answer": "3",
            "distractor_4": "준비하다",
            "distractor_3": "기억하다",
            "product_no": 48087,
            "distractor_2": "버리다",
            "meaning": "기억하다",
            "question": "remember",
            "distractor_1": "숨기다"
        }
    ],
    "out6": [
        {
            "question_order": 1,
            "sm_sentence_question_no": 74561,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "sentence": "Hegetsscaredveryeasily.",
            "product_no": 48087,
            "text_sentence": "Hegetsscaredveryeasily."
        },
        {
            "question_order": 2,
            "sm_sentence_question_no": 74562,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "sentence": "IsChaseeatingcarrots?",
            "product_no": 48087,
            "text_sentence": "IsChaseeatingcarrots?"
        },
        {
            "question_order": 3,
            "sm_sentence_question_no": 74563,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "sentence": "Sparkisplayingwithaboomerang.",
            "product_no": 48087,
            "text_sentence": "Sparkisplayingwithaboomerang."
        },
        {
            "question_order": 4,
            "sm_sentence_question_no": 74564,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "sentence": "Cocoisrunningsohardwithhershortlegs.",
            "product_no": 48087,
            "text_sentence": "Cocoisrunningsohardwithhershortlegs."
        },
        {
            "question_order": 5,
            "sm_sentence_question_no": 74565,
            "study_unit_code": "01A",
            "last_update_dt": "2015-07-0111: 56: 08",
            "sentence": "Yuna,   movenexttoCoco.",
            "product_no": 48087,
            "text_sentence": "Yuna,  movenexttoCoco."
        }
    ],
    "out7": []
}