package com.yoons.fsb.student;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;

/**
 * 블루투스 MediaButtonReceiver
 * 블루투스 media 이벤트가 기본 플레이어가 동작하게 할 수 있으므로
 * 전달되지 않게만 만든 클래스
 * @author ejlee
 */
public class MediaButtonReceiver extends BroadcastReceiver {
	public static final String TAG = "[MediaButtonReceiver]";
	
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		String action = arg1.getAction();
		if (action.equals(Intent.ACTION_MEDIA_BUTTON)) {
			KeyEvent event = (KeyEvent)arg1.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
			 if (event == null)
		            return;
		        
		        int event_action = event.getAction();
		        int keycode = event.getKeyCode();
		        
		        String keyaction = "other";
		        if (event_action == KeyEvent.ACTION_UP)
		        	keyaction = "KEY_UP";
		        else
		        if (event_action == KeyEvent.ACTION_DOWN)
		        	keyaction = "KEY_DOWN";
		        
		        Log.e(TAG, String.format("action [%s] keycode [%d]",keyaction,keycode));
		        
			Log.e(TAG, action);
		}
	}
}
