package com.yoons.fsb.student;

import android.content.Context;
import android.view.KeyEvent;

import com.yoons.fsb.student.util.CommonUtil;

public class ServiceCommon {

    /**
     * blob스토리지 관련
     */
    public static final String storageConnectionString = "DefaultEndpointsProtocol=http;" + "AccountName=yoonsfiletest;"
            + "AccountKey=vU9lxg/CJsd53KR4DLlfmNQgGVt6mFsBBISUYSWv5H4NwoTlJeDhZvis9thr16Zu2ohmPmZo8e+5ixTNKHyqLw==";


    /**
     * Broadcast Action
     */
    public static final String ACTION_SET_ALARM = "com.yoons.fsb.student.ACTION_SET_ALARM";
    public static final String ACTION_PUSH_MESSAGE = "com.yoons.fsb.student.PUSH_MESSAGE";
    public static final String ACTION_NOTI_CALL = "com.yoons.fsb.student.NOTI_CALL";
    public static final String ACTION_SLOG = "com.yoons.fsb.student.SLOG";
    public static final String ACTION_NOTI_FOUCS = "com.yoons.fsb.student.NOTI_FOUCS";
    public static final String ACTION_NOTI_INIT = "com.yoons.fsb.student.NOTI_INIT";

    /**
     * Extra Name
     */
    public static final String EXTRA_PUSH_MESSAGE = "message";

    /**
     * Package Name
     */
    public static final String NOTI_PACKAGE = "com.yoons.fsb.student.service.NotiService";
    public static final String ALARM_PACKAGE = "com.yoons.fsb.student.alarmservice";
    public static final String LAUNCHER_PACKAGE = "com.yoons.fsb.launchertoapp";
    public static final String LOGIN_PACKAGE = "com.yoons.fsb.login";
    public static final String PM_PACKAGE = "com.yoons.pm"; // SM-T300 단말의 런처
    public static final String FOURSKILL_PACKAGE = "com.yoons.fourskillbefly"; // 4SKILL
    // 패키지

    /**
     * Extra Name
     */
    public static final String TARGET_ACTIVITY = "targetActivity";

    /**
     * 컨텐츠 검증을 위한 전체 복습 인지 여부
     **/
    public static boolean IS_CONTENTS_TEST = false;
    /**
     * 4개 교재 체험 계정 인지 여부
     **/
    public static boolean IS_EXPERIENCES = false;
    /**
     * launcher에서 동작되는지 여부
     **/
    public static boolean IS_LAUNCHER = false;

    public static String CONTENT_URL = ServiceCommon.CDN_SERVER_URL + "study/%d/%d/%d_%d_%d"; // 학습
    // 신규문항
    public static String CONTENT_POPUP_URL = ServiceCommon.CDN_SERVER_URL + "studyExt/HomeStudy/%d/%d/%s"; // Popup학습

    public static String OLD_ANSWER_URL = ServiceCommon.CDN_SERVER_URL + "answer/%d/%d/%d_%d_%d"; // 학습
    public static int RECORD_VOLUME = 0;
    // 콘텐츠
    // URL

    public static String STUDY_SERVER_URL = ""; // 학습관련 메타 데이터를 관리하는 was 서버 URL
    public static String FSB_SERVER_URL = ""; // 학습관련 메타 데이터를 관리하는 was 서버 URL
    public static String SEC_SERVER_URL = ""; // 2교시관련 서버 URL 나중에 개발끝나면 합쳐야함
    public static String DEV_SERVER_URL = ""; // 개발자모드 서버 URL
    public static String SBM_SERVER_URL = ""; // 중앙서버 URL//
    public static String SB_SERVER_URL = ""; // 페이지 정보 받는 URL

    public static final String CDN_SERVER_URL = "http://yoons.beflys.gscdn.com/smartbefly/contents/"; // 학습
    // 콘텐츠
    // cdn
    // URL

    public static String UPLOAD_SERVER_URL = "";
    ; // 음성 Upload URL
    public static final String THUMBNAIL_URL = "http://yoonsbeflys.yoons.com/images/books/main/"; // 실제
    // 책자
    // Thumbnail
    // URL

    public static final String STORAGE_PATH = CommonUtil.getInternalStoragePath(); // 기본
    // 내장
    // 스토리지
    // path
    public static final String LOG_PATH = STORAGE_PATH + "/fsblog/"; // 로그 path
    public static final String CONTENTS_PATH = STORAGE_PATH + "/fsbdata/"; // fsb
    // 컨텐츠
    // 패스(db
    // 및
    // 음성,
    // 콘텐츠
    // 파일)

    public static final String DB_FILE = "study.db"; // DB파일 이름
    public static final String DB_PATH = CONTENTS_PATH + DB_FILE; // DB파일 path
    public static final String STUDY_CONTENT_PATH = CONTENTS_PATH + "study/"; // 학습
    // 콘텐츠
    // path
    public static final String AUDIO_RECORD_PATH = CONTENTS_PATH + "audiorecord/"; // 오디오
    // 학습
    // 녹음
    // path
    public static final String EXAM_RECORD_PATH = CONTENTS_PATH + "examrecord/"; // 문장
    // 시험
    // 녹음
    // path
    public static final String EXAM_RECORD_UPLOAD_PATH = CONTENTS_PATH + "examrecordupload/"; // 업로드
    // 되어야할
    // 파일을
    // 모아둔
    // path
    public static final String THUMBNAIL_PATH = CONTENTS_PATH + "thumbnail/"; // Thumbnail
    // 저장
    // path
    public static final String DB_BACK_UP_PATH = STORAGE_PATH + "/fsbbackup/"; // DB
    // 백업
    // 저장
    // path

    public static final String TAG_FILE_TAIL = ".tag";
    public static final String OGG_FILE_TAIL = ".ogg";
    public static final String MP3_FILE_TAIL = ".mp3";
    public static final String MP4_FILE_TAIL = ".mp4";
    public static final String WAV_FILE_TAIL = ".wav";
    public static final String YDA_FILE_TEMP_HEAD = "temp_";

    public static final String AUDIO_RECORD_FILE_NAME = "audio_record";

    public static final String WRONG_SENTENCE_RECORD_FILE_NAME = "wrong_sentence_record";

    public final static String PARAM_MUTE_GUIDE = "MuteGuideMessage";

    public final static String PARAM_GO_SETTING = "GoSetting";

    public final static String PARAM_IS_LOGIN_SETTING = "IsLoginSetting";

    public static String CUSTOMER_ID = "";
    public static String CUSTOMER_PW = "";
    public static String DEVICE_ID = "";
    public static int CUSTOMER_NO = 0;
    public static boolean IS_GUIDE_SHOWN = false;
    /**
     * 구교재 리스트
     */
    public final static int[] OLD_BOOK_LIST = {4396, 4217, 4216, 4198, 4197, 4196, 4176, 4156, 4139, 4138, 4137, 4136, 1001300, 1001200};


    /**
     * Handler Message
     */
    public static final int MSG_WHAT_PLAYER = 0;
    public static final int MSG_WHAT_RECORDER = 1;
    public static final int MSG_WHAT_CONVERT = 2;
    public static final int MSG_WHAT_STUDY = 3;
    public static final int MSG_WHAT_TAG_EVENT = 4;
    public static final int MSG_WHAT_TAG_WARNING = 5;
    public static final int MSG_WHAT_BOOK = 6;
    public static final int MSG_WHAT_BOOK_WARNING = 7;
    public static final int MSG_WHAT_VIDEO_PLAYER = 8;
    public static final int MSG_WHAT_VIDEO_CONTROL = 9;
    public static final int MSG_WHAT_VIDEO_PLAYER_ERROR = 10;
    public static final int MSG_WHAT_AUDIO_PLAYBACK_RATE = 11;
    public static final int MSG_WHAT_CALL_NOTI = 21;
    public static final int MSG_WHAT_FOCUS_NOTI = 22;
    public static final int MSG_WHAT_SLOG = 23;

    public static final int MSG_PROGRESS_UPDATE = 1000;
    public static final int MSG_PROGRESS_COMPLETION = 1001;

    public static final int MSG_SEEK_FWD_ABLE = 1100;
    public static final int MSG_SEEK_FWD_DISABLE = 1101;

    public static final int MSG_STUDY_PROGRESS_START = 2000;
    public static final int MSG_STUDY_PROGRESS_UPDATE = 2001;
    public static final int MSG_STUDY_PROGRESS_COMPLETION = 2002;
    public static final int MSG_STUDY_PROGRESS_RESTART = 2003;

    public static final int MSG_STUDY_WRONG_PROGRESS_START = 3000;
    public static final int MSG_STUDY_WRONG_PROGRESS_UPDATE = 3001;
    public static final int MSG_STUDY_WRONG_PROGRESS_COMPLETION = 3002;
    public static final int MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE = 3003;
    public static final int MSG_STUDY_WRONG_PROGRESS_UPDATE_NEXT = 3004;

    public static final int MSG_REC_READY = 4000;
    public static final int MSG_REC_START = 4001;
    public static final int MSG_REC_END = 4002;
    public static final int MSG_REC_WARNING = 4003;
    public static final int MSG_REC_AUTO_END = 4004;
    public static final int MSG_REC_PLAY = 4005;
    public static final int MSG_REC_PLAY_END = 4006;
    public static final int MSG_REC_ING = 4007;

    public static final int MSG_REC_ERROR_GET_MIN_BUFFERSIZE = 4100;
    public static final int MSG_REC_ERROR_CREATE_FILE = 4101;
    public static final int MSG_REC_ERROR_REC_START = 4102;
    public static final int MSG_REC_ERROR_AUDIO_RECORD = 4103;
    public static final int MSG_REC_ERROR_AUDIO_ENCODE = 4104;
    public static final int MSG_REC_ERROR_WRITE_FILE = 4105;
    public static final int MSG_REC_ERROR_CLOSE_FILE = 4106;

    public static final int MSG_YDA_ERROR_FILE_NOT_FOUND = 5000; // YDA 파일 미존재
    public static final int MSG_YDA_ERROR_FILE_COPY = 5001; // YDA 파일 복사본 생성시 오류
    public static final int MSG_YDA_ERROR_LIB_DECRYPT = 5002; // YDA 라이브러리 변환 오류

    public static final int MSG_STUDY_VIDEO_PROGRESS_UPDATE = 6000;
    public static final int MSG_STUDY_VIDEO_PROGRESS_CONFIRM = 6001;
    public static final int MSG_STUDY_VIDEO_PROGRESS_COMPLETION = 6002;

    // 신규문항
    public static final int MSG_STUDY_QUIZ_COMPLETION = 7000;
    public static final int MSG_STUDY_TOUCHPLAY_COMPLETION = 7001;
    public static final int MSG_STUDY_ROLEPLAY_UPDATE = 7002;
    public static final int MSG_STUDY_ROLEPLAY_COMPLETION = 7003;
    public static final int MSG_STUDY_IDEAMAP_COMPLETION = 7004;

    public static final int MSG_AUDIO_PR_START = 7005;
    public static final int MSG_AUDIO_PR_STOP = 7006;
    public static final int MSG_AUDIO_PR_CHANGE = 7007;
    public static final int MSG_AUDIO_PR_PAUSE = 7008;
    public static final int MSG_AUDIO_PR_END = 7009;

    public static final int MSG_SUCCESS = 100;
    public static final int MSG_FAIL = 101;

    public static final int MSG_HTTP_REQUEST_SUCCESS = 200;
    public static final int MSG_HTTP_REQUEST_FAIL = 201;

    public static final int REQUEST_ID_TIME_SYNC_START = 1001;
    public static final int REQUEST_ID_TIME_SYNC_END = 1002;

    public static final int MSG_UPDATE_DOWNLOAD_SUCCESS = 9999;
    public static final int MSG_UPDATE_DOWNLOAD_FAIL = 9998;

    public static final int MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS = 210;
    public static final int MSG_DATABASE_DOWNLOAD_SYNC_FAIL = 211;
    public static final int MSG_DATABASE_DOWNLOAD_SYNC_START = 212;
    public static final int MSG_DATABASE_NEW_DOWNLOAD_SYNC_SUCCESS = 213;

    public static final int MSG_DATABASE_UPLOAD_SYNC_SUCCESS = 220;
    public static final int MSG_DATABASE_UPLOAD_SYNC_FAIL = 221;
    public static final int MSG_DATABASE_UPLOAD_SYNC_START = 222;
    public static final int MSG_DATABASE_UPLOAD_SYNC_DEVICE_ERROR_FAIL = 223; // 디바이스가
    // 다른
    // 경우에
    // 발생.
    // sbm_study_result
    public static final int MSG_DATABASE_BACKUP_UPLOAD_SYNC_START = 224;

    public static final int MSG_OLD_CONTENTS_FILE_DELETE_COMPLETE = 225;

    public static final int MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE = 230;

    /**
     * 녹음 컨텐츠 업로드
     */
    public static final int MSG_RECORDING_FILE_UPLOAD = 240;

    public static final int MSG_RECORDING_OLD_FILE_DELETE_START = 250;
    public static final int MSG_RECORDING_OLD_FILE_DELETE_END = 251;

    public static final int MSG_NETWORK_CHECK_CONNECTED = 260;
    public static final int MSG_NETWORK_CHECK_DISCONNECTED = 261;

    public static final int MSG_STUDY_FINISH = 280;

    public static final int STATUS_REVIEW_AUDIO = 0;
    public static final int STATUS_REVIEW_WORD = 1;
    public static final int STATUS_REVIEW_SENTENCE = 2;
    public static final int STATUS_REVIEW_END = 3;

    public static final String STATUS_START = "START";
    public static final String STATUS_END = "END";
    public static final String STATUS_STOP = "STOP";
    public static final String STATUS_PAUSE = "PAUSE";
    public static final String STATUS_PLAY = "PLAY";
    public static final String STATUS_REC = "RECORD";

    public static final int BUTTONTAG_SEEK_BACK = 0;
    public static final int BUTTONTAG_SEEK_FWD = 1;

    public static final int BUTTONTAG_ANSWER_1 = 1;
    public static final int BUTTONTAG_ANSWER_2 = 2;
    public static final int BUTTONTAG_ANSWER_3 = 3;
    public static final int BUTTONTAG_ANSWER_4 = 4;

    // 신규문항
    public static final int TAG_TYPE_POP_1 = 1; // 선다형
    public static final int TAG_TYPE_POP_2 = 2; // drag & drop
    public static final int TAG_TYPE_POP_3 = 3; // 교재집중형
    public static final int TAG_TYPE_TOUCH_PLAY = 4;
    public static final int TAG_TYPE_ROLE_PLAY = 5;
    public static final int TAG_TYPE_POP_4 = 6; // OX 퀴즈

    public static final int TAG_TIME_FIX_TERM_100 = 100;

    public static final int TAG_TIME_FIX_TERM_200 = 200;

    public static final int TAG_TIME_FIX_TERM_300 = 300;

    public static final int SEEK_TIME_3SEC = 3000;

    public static final int SEEK_TIME_30SEC = 30000;

    public static final int RECORD_SAMPLE_RATE = 44100; // 44.1KHz

    public static final int PLAYER_SAMPLE_RATE = 44100;

    public static final int MAX_EXAM_PREPARING_TIME = 180000;

    public static final int MAX_REC_TIME_3MIN = 180000;

    public static final int MAX_REC_TIME_20SEC = 20000;

    public static final int WARNING_REC_TIME = 180000;

    public static final int MAX_SENTENCE_LENGTH = 60;

    public static final int MAX_WORD_LENGTH = 8;

    public static final int MAX_VANISHING_LENGTH = 40;
    public static final int MAX_VANISHING_LINE = 3;
    public static final int VANISHING_REVISE = 11;
    public static final int VANISHING_REVISE_MIDDLE = 16;
    public static final int VANISHING_REVISE_LONG = 24;
    public static final int VANISHING_TEXT_SHORT = 135;
    public static final int VANISHING_TEXT_LONG = 155;

    public static final int PART_GB_PART1 = 1;// 2교시 part1
    public static final int PART_GB_PART2 = 2;// 2교시 part1

    public static final String QUESTION_TYPE_1 = "T1";//
    public static final String QUESTION_TYPE_2 = "T2";//

    /**
     * 사용가능한 최대 계정 수
     */
    public static final int MAX_ACCOUNT_SIZE = 2;

    /**
     * 블루투스 크래를을 위한 key event
     */
    // KeyEvent.KEYCODE_MEDIA_PLAY(126), KeyEvent.KEYCODE_MEDIA_PAUSE(127) api
    // level 11
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_PLAY = 126; // play,normal_ok,write_ok,record_ok
    // button
    // key
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_PAUSE = 127; // pause
    // button
    // key
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_STOP = KeyEvent.KEYCODE_MEDIA_STOP; // record
    // start
    // button
    // key
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_REWIND = KeyEvent.KEYCODE_MEDIA_REWIND; // backward,
    // number2
    // button
    // key
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD = KeyEvent.KEYCODE_MEDIA_FAST_FORWARD; // forward,
    // number4
    // button
    // key
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS = KeyEvent.KEYCODE_MEDIA_PREVIOUS; // volume
    // down,
    // number3
    // button
    // key
    public static final int BLUETOOTH_KEY_EVENT_MEDIA_NEXT = KeyEvent.KEYCODE_MEDIA_NEXT; // volume
    // up,
    // number1
    // button
    // key

    public static final String KEY_TEST_URL = "TestUrl";
    //----중앙화
    public static final String KEY_FOREST_CODE = "KEY_FOREST_CODE";
    public static final String DEFAULT_FOREST_CODE = "0";//

    public static final String KEY_TEACHER_CODE = "KEY_TEACHER_CODE";
    public static final String DEFAULT_TEACHER_CODE = "0";//

    public static final String KEY_CENTER = "KEY_CENTER";
    public static final String DEFAULT_CENTER = "0";//

    public static final String KEY_LMS_STATUS = "KEY_LMS_STATUS";
    public static final String DEFAULT_LMS_STATUS = "0";//


    public static final int TEST_SERVER = 1;
    public static final int REAL_SERVER = 2;

    public static final int TEST_CONTENT = 3;
    public static final int REAL_CONTENT = 4;

    public static final int LOCAL_MODE = 5;//로컬 LMS 모드 192.168.0.2
    public static final int CENTER_MODE = 6;//중앙화 모드

    //<-- 세팅 -->
    public static final int SERVER = REAL_SERVER;
    public static final int CONTENT_MODE = REAL_CONTENT;
    public static int SERVER_MODE = CENTER_MODE;
    public static boolean VOC_MODE = false;//voc 대응용으로 나간거 로그,딕테이션 1번만 설정
    public static boolean DEV_MODE = false;// 개발자모드
    //<!-- 세팅 -->



    public final static String SEC_GETS = "GETS";// 2교시에 학습해야할 문장, 단어/구를 출력한다.
    public final static String SEC_QLOG = "QLOG";// 2교시 문제 답
    public final static String SEC_QINF = "QINF";// 수신된 학습목록 중 마지막 학습차시에 대한 값을
    // 통해 2교시 학습을 시작한다.
    public final static String SEC_QPRO = "QPRO";// 2교시학습 로그 기록
    public final static String SEC_QUST = "QUST";// 2교시에 학습해야할 문장, 단어/구를 출력한다.
    public final static String SEC_QGET = "QGET";// 2교시에 학습해야할 문장, 단어/구를 출력한다.
    public final static String SEC_QORE = "QORE";// 2교시에 학습해야할 문장, 단어/구를 출력한다.
    public final static String SEC_QTET = "QTET";// 일주간평가문항
    public final static String SEC_BDIF = "BDIF";// 차시 정보를 얻어 옴
    public final static String SEC_QTLG = "QTLG";// 일 주간 평가 정답
    public final static String SEC_SLOG = "SLOG";// 학습 로그 기록
    public final static String LGIN = "LGIN";// 로그인
    public final static String MLGNL = "MLGNL";// 로그인로그
    public final static String MLGNB = "MLGNB";// 로그인가능 확인
    public final static String CASE = "CASE";// 로그인 비활성화 기능에 따른 로그인CASE값(2 또는 3) 학습이 마친 상태에서 서버측에 전달한다.
    public final static String GWSI = "GWSI"; // 로컬 일주일 학습 결과 데이터

    /**
     * app version Name에 따라 개발, 상용 서버 URL을 설정하는 함수 Version Name이 홀수면 개발, 짝수면 상용
     *
     * @param context
     */
    public static void setServerUrl(Context context, String url) {

        if (CONTENT_MODE == REAL_CONTENT) {//운영 컨텐츠
            CONTENT_URL = ServiceCommon.CDN_SERVER_URL + "study/%d/%d/%d_%d_%d"; // 학습
            // 신규문항
            CONTENT_POPUP_URL = ServiceCommon.CDN_SERVER_URL + "studyExt/HomeStudy/%d/%d/%s"; // Popup학습

        } else {
            CONTENT_URL = ServiceCommon.CDN_SERVER_URL + "test/%d/%d/%d_%d_%d"; // 학습
            // 신규문항
            CONTENT_POPUP_URL = ServiceCommon.CDN_SERVER_URL + "studyExt/HomeStudyTest/%d/%d/%s"; // Popup학습
        }

        if (SERVER == REAL_SERVER && SERVER_MODE == LOCAL_MODE) {//운영 & 로컬서버일때

            STUDY_SERVER_URL = "http://192.168.0.2/index.php?/newapi";
            UPLOAD_SERVER_URL = "http://192.168.0.2/index.php?/newapi?p_nm=QXVkaW9fRmlsZV9VcGxvYWRfU1BfVklQ";/**/
            FSB_SERVER_URL = "http://m4sb.yoons.com/mb/mb_FcheckLauncher_center.jsp";
            SEC_SERVER_URL = "http://192.168.0.2/api/api.php";
            SBM_SERVER_URL = "http://sbms.yoons.com/api.php";
            SB_SERVER_URL  = "http://sbhio.yoons.com:7000/hssb/cust/requestData";
        } else if (SERVER == REAL_SERVER && SERVER_MODE == CENTER_MODE) {//운영 & 중앙서버일때

            STUDY_SERVER_URL = "http://lmsapi.yoons.com/sync/YS20/getYS20Value.do";
            UPLOAD_SERVER_URL = "http://192.168.0.2/index.php?/newapi?p_nm=QXVkaW9fRmlsZV9VcGxvYWRfU1BfVklQ";/**/
            FSB_SERVER_URL = "http://m4sb.yoons.com/mb/mb_FcheckLauncher_center.jsp";
            SEC_SERVER_URL = "http://192.168.0.2/api/api.php";
            SBM_SERVER_URL = "http://sbms.yoons.com/api.php";
            SB_SERVER_URL  = "http://sbhio.yoons.com:7000/hssb/cust/requestData";
        } else if (SERVER == TEST_SERVER && SERVER_MODE == LOCAL_MODE) {//테스트 & 로컬서버일때

            STUDY_SERVER_URL = "http://172.16.14.73/index.php?/newapi";
            UPLOAD_SERVER_URL = "http://172.16.14.73/index.php?/newapi?p_nm=QXVkaW9fRmlsZV9VcGxvYWRfU1BfVklQ";
            SEC_SERVER_URL = "http://172.16.14.73/api/api.php";
            FSB_SERVER_URL = "http://testrenewal.iyoons.com/mb/mb_FcheckLauncher_center.jsp";
            //SBM_SERVER_URL = "http://sbms.yoons.com/api.php";
            SBM_SERVER_URL = "http://sbms.yoons.or.kr/api.php";
            SB_SERVER_URL = "http://sb2was.yoons.or.kr:7000/hssb/cust/requestData";
        } else if (SERVER == TEST_SERVER && SERVER_MODE == CENTER_MODE) {//테스트 & 중앙서버일때

//            STUDY_SERVER_URL = "http://lmsapi.yoons.com/sync/YS20/getYS20Value.do";
            STUDY_SERVER_URL = "http://lmstestapi.yoons.or.kr/sync/YS20/getYS20Value.do";
            UPLOAD_SERVER_URL = "http://172.16.14.73/index.php?/newapi?p_nm=QXVkaW9fRmlsZV9VcGxvYWRfU1BfVklQ";
//            SEC_SERVER_URL = "http://lmsapi.yoons.com/sync/YS20/getYS20Value.do";
            SEC_SERVER_URL = "http://lmstestapi.yoons.or.kr/sync/YS20/getYS20Value.do";
            FSB_SERVER_URL = "http://testrenewal.iyoons.com/mb/mb_FcheckLauncher_center.jsp";
            //SBM_SERVER_URL = "http://sbms.yoons.com/api.php";
            SBM_SERVER_URL = "http://sbms.yoons.or.kr/api.php";
            SB_SERVER_URL = "http://sb2was.yoons.or.kr:7000/hssb/cust/requestData";
        }


        if (url != null && !"".equals(url)) {// 개발자 모드 일때
            if (SERVER_MODE == CENTER_MODE) {
                STUDY_SERVER_URL = "http://" + url + "/sync/YS20/getYS20Value.do";
                UPLOAD_SERVER_URL = "http://" + url + "/index.php?/newapi?p_nm=QXVkaW9fRmlsZV9VcGxvYWRfU1BfVklQ";
                SEC_SERVER_URL = "http://" + url + "/api/api.php";
            } else {
                STUDY_SERVER_URL = "http://" + url + "/index.php?/newapi";
                UPLOAD_SERVER_URL = "http://" + url + "/index.php?/newapi?p_nm=QXVkaW9fRmlsZV9VcGxvYWRfU1BfVklQ";
                SEC_SERVER_URL = "http://" + url + "/api/api.php";
            }
            DEV_MODE = true;
        } else {// 개발자 모드 아닐때
            DEV_MODE = false;
            ServiceCommon.DEV_SERVER_URL = "";
        }
    }
}
