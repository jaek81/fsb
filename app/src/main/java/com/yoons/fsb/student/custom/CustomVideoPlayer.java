package com.yoons.fsb.student.custom;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.util.CommonUtil;

/**
 * Video Player Class 
 * android.media.MediaPlayer 사용 
 * 
 * @author jaek
 */
public class CustomVideoPlayer extends FrameLayout implements Callback, OnClickListener, OnSeekBarChangeListener {
	
	public static final int MEDIA_ERROR_TIMED_OUT = -110;
	
	public static final int MEDIA_ERROR_IO = -1004;
	
	public static final int MEDIA_ERROR_MALFORMED = -1007;
	
	public static final int MEDIA_ERROR_UNSUPPORTED = -1010;
	
	public OnDialogDismissListener mDialogDismissListener = null;
	
	private MediaPlayer.OnPreparedListener mPreparedListener = null;
	
	private MediaPlayer.OnErrorListener mErrorListener = null;
	
	private MediaPlayer.OnCompletionListener mCompletionListener = null;
	
	private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener = null;
	
	private MediaPlayer.OnSeekCompleteListener mSeekListener = null;
	
	private SurfaceView mSurfaceView = null;
	
	private SurfaceHolder mHolder = null;
	
	public MediaPlayer mPlayer = null;
	
	private Handler mHandler = null;
	
	private Thread mStartThread = null, mPauseThread = null, mSeekThread = null, mPlayInfoThread = null;
	
	private LinearLayout mControls = null, mLoading = null;
	
	private SeekBar mSeekBar = null;
	
	private ImageView mPlay = null/*, mReSize = null*/;
	
	private TextView mPlayTime = null, mTotalTime = null, mSkip = null;
	
	private String mVideoFilePath = "";
	
	private boolean mIsPrepared = false, mIsSeeking = false, mIsFullSize = false, mIsReStart = false;
	
	private int mDuration = 0, mCurPos = 0, mSeekPos = 0, mBufferingPos = 0;
	
	public CustomVideoPlayer(Context context, Handler handler, String filePath, boolean isFullSize, boolean isSeek) {
		super(context);
		
		mHandler = handler;
		
		mVideoFilePath = filePath;
		
		mIsFullSize = isFullSize;
		
		mIsReStart = isSeek;
		
		init();
	}
	
	private void setPlayerListener() {
		mPreparedListener = new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				showLoading(View.GONE);
				initSeekBar();
				mp.start();
				startPlayInfoThread();
				mIsPrepared = true;
			}
		};
		
		mErrorListener = new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int errorWhat, int errorExtra) {
				end(ServiceCommon.MSG_WHAT_VIDEO_PLAYER_ERROR, errorWhat, errorExtra);
				return true;
			}
		};
		
		mCompletionListener = new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				pause(ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_CONFIRM);
			}
		};
		
		mSeekListener = new MediaPlayer.OnSeekCompleteListener() {
			@Override
			public void onSeekComplete(MediaPlayer mp) {
				seekComplete();
			}
		};
		
		mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				updateBufferingInfo(percent);
			}
		};
		
		mDialogDismissListener = new OnDialogDismissListener(){
			@Override
			public void onDialogDismiss(int result, int dialogId) {
				if (BaseDialog.DIALOG_CONFIRM == result) 
					reStart();
				else 
					end(ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_COMPLETION);
			}
		};
	}
	
	private void init() {
		View.inflate(getContext(), R.layout.layout_video_frame, this);
		
		setPlayerListener();
		
		mSurfaceView = (SurfaceView) findViewById(R.id.video_view);
		mControls = (LinearLayout) findViewById(R.id.video_controls);
		mLoading = (LinearLayout) findViewById(R.id.video_loading);
 		mPlay = (ImageView) findViewById(R.id.video_play_pause_btn);
		mSkip = (TextView) findViewById(R.id.video_skip_btn);
//		mReSize = (ImageView) findViewById(R.id.video_resize_btn);
		mPlayTime = (TextView) findViewById(R.id.video_play_time_text);
		mTotalTime = (TextView) findViewById(R.id.video_total_time_text);
		mSeekBar = (SeekBar) findViewById(R.id.video_seek_bar);
		
		mPlay.setOnClickListener(this);
		mSkip.setOnClickListener(this);
//		mReSize.setOnClickListener(this);
		
		mSkip.setVisibility(View.GONE);
	
		SurfaceHolder holder = mSurfaceView.getHolder();
		holder.addCallback(this);
	}
	
	private void setPlayer() {
		Thread initPlayerThread = new Thread("videoPlayerInitThread"){
    		public void run() {
    			try {		
    				mPlayer = new MediaPlayer();
    				mPlayer.reset();
    				mPlayer.setDataSource(mVideoFilePath);
    				mPlayer.setOnPreparedListener(mPreparedListener);
    				mPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
    				mPlayer.setOnSeekCompleteListener(mSeekListener);
    				mPlayer.setOnCompletionListener(mCompletionListener);
    				mPlayer.setOnErrorListener(mErrorListener);
    				mPlayer.setDisplay(mHolder);
    				mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC); 
    				
    				mPlayer.prepareAsync();
    			} catch (Exception e) {
    				e.printStackTrace();
    			} 
			}
    	};
    	
    	initPlayerThread.start();
	}
	
	private void initPlayer(SurfaceHolder holder) {
		setPlayerSize(mIsFullSize);
		
		showLoading(View.VISIBLE);
		
		mHolder = holder;
		
		setPlayer();
	}
	
	private void initSeekBar() {
		mSeekBar.setMax(mPlayer.getDuration());
		mSeekBar.setOnSeekBarChangeListener(this);
		mSeekBar.setEnabled(mIsReStart);
	}
	
	private void setPlayerSize(boolean isFullSize) {
		mIsFullSize = isFullSize;
		ViewGroup.LayoutParams surfaceParam = null, loadingParam = null, controlParam = null;
		surfaceParam = mSurfaceView.getLayoutParams();
		controlParam = mControls.getLayoutParams();
		loadingParam = mLoading.getLayoutParams();
		LinearLayout titlebar = null, statusbar = null;
		try {
			titlebar = (LinearLayout) ((LinearLayout) getParent().getParent()).findViewById(R.id.ganji_titlebar);
			statusbar = (LinearLayout) ((LinearLayout) getParent().getParent()).findViewById(R.id.setp_statusbar);
		} catch (Exception e){
			e.printStackTrace();
		}
		
		if (mIsFullSize) {
			if (null != titlebar)
				titlebar.setVisibility(View.GONE);
			if (null != statusbar)
				statusbar.setVisibility(View.GONE);
			
			surfaceParam.width = ViewGroup.LayoutParams.MATCH_PARENT;
			controlParam.width = ViewGroup.LayoutParams.MATCH_PARENT;
			loadingParam.width = ViewGroup.LayoutParams.MATCH_PARENT;
		} else {
			if (null != titlebar)
				titlebar.setVisibility(View.VISIBLE);
			if (null != statusbar)
				statusbar.setVisibility(View.VISIBLE);
			
			surfaceParam.width = getResources().getDimensionPixelSize(R.dimen.dimen_395);
			controlParam.width = getResources().getDimensionPixelSize(R.dimen.dimen_395);
			loadingParam.width = getResources().getDimensionPixelSize(R.dimen.dimen_395);
		}
		
	/*	mReSize.setImageDrawable(mIsFullSize ? getResources().getDrawable(R.drawable.selector_normal_size_btn_bg) 
				: getResources().getDrawable(R.drawable.selector_full_size_btn_bg));*/
		
		mSurfaceView.setLayoutParams(surfaceParam);
		mControls.setLayoutParams(controlParam);
		mLoading.setLayoutParams(loadingParam);
	}
	
	private void reStart() {
		mIsReStart = true;
		mSeekBar.setEnabled(false);
		mSkip.setVisibility(View.VISIBLE);
		showLoading(View.VISIBLE);
		end();
		setPlayer();
	}
	
	private void showLoading(int visibility) {
		if (visibility == mLoading.getVisibility())
			return;

		mLoading.setVisibility(visibility);
	}
	
	private void showControls(int visibility) {
		if (!isPrepared())
			return;
		
		mControls.setVisibility(visibility);
		if (visibility == View.VISIBLE) {
			mVideoHandler.removeMessages(ServiceCommon.MSG_WHAT_VIDEO_CONTROL);
			mVideoHandler.sendEmptyMessageDelayed(ServiceCommon.MSG_WHAT_VIDEO_CONTROL, 5000);
		}
	}
	
	private void startPlayInfoThread() {
		stopPlayThread();
		
		mPlayInfoThread = new Thread("videoPlayInfoThread") {
			public void run() {
				while (null != mPlayInfoThread && !mPlayInfoThread.isInterrupted()) {
					mVideoHandler.sendMessage(mVideoHandler
							.obtainMessage(ServiceCommon.MSG_WHAT_VIDEO_PLAYER, ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_UPDATE, 0));
					
					SystemClock.sleep(100);
				}
			}
		};
		mPlayInfoThread.start();
	}
	
	private void stopPlayThread() {
		if (null == mPlayInfoThread)
			return;
		
		mPlayInfoThread.interrupt();
		mPlayInfoThread = null;
	}
	
	private void updatePlayInfo() {
		if (!isPrepared())
			return;
		
		mCurPos = mPlayer.getCurrentPosition();
		mDuration = mPlayer.getDuration();
		
		mPlayTime.setText(CommonUtil.convMsecToMinSec(mCurPos));
		mTotalTime.setText(CommonUtil.convMsecToMinSec(mDuration));
		
		if (!isSeeking())
			mSeekBar.setProgress(mCurPos);
		
		if (isPlaying()) {
			int marginPos = mCurPos + 10000;
			if (mDuration < marginPos)
				marginPos = mDuration;
			if(mVideoFilePath.contains("http")){
				showLoading(marginPos > mBufferingPos ? View.VISIBLE : View.GONE);
			}
			
		}
	}
	
	private void updateBufferingInfo(int percent) {
		if (!isPrepared())
			return;
		
		mBufferingPos = (mDuration * percent) / 100;
		
		mSeekBar.setSecondaryProgress(mBufferingPos);
	}
	
	private Handler mVideoHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case ServiceCommon.MSG_WHAT_VIDEO_PLAYER:
					if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_UPDATE)
						updatePlayInfo();
					break;
					
				case ServiceCommon.MSG_WHAT_VIDEO_CONTROL:
					if (null != mPlayer) {
						if (mControls.getVisibility() == View.VISIBLE)
							mControls.setVisibility(View.GONE);
					}
					break;
					
				default:
					super.handleMessage(msg);
			}
		}
	};
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		initPlayer(holder);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		end();
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (fromUser) {
			mSeekPos = progress;
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		mVideoHandler.removeMessages(ServiceCommon.MSG_WHAT_VIDEO_CONTROL);
		mIsSeeking = true;
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mVideoHandler.sendEmptyMessageDelayed(ServiceCommon.MSG_WHAT_VIDEO_CONTROL, 5000);
		seekTo(mSeekPos);
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
		showControls(mControls.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
		return false;
    }
	
	@Override
	public void onClick(View v) {
		if (!isPrepared())
			return;
		
		int id = v.getId();
		
		switch(id) {
			/*case R.id.video_resize_btn:
				setPlayerSize(!mIsFullSize);
				break;*/
				
			case R.id.video_skip_btn:
				pause(ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_CONFIRM);
				break;
				
			case R.id.video_play_pause_btn:
				if (isPlaying())
					pauseBtn();
				else
					startBtn();
				break;
		}
	}

	
	public void setPlayPressed(boolean pressed) {
		mPlay.setPressed(pressed);
	}
	
	public boolean isPlaying() {
		if (null == mPlayer)
			return false;
		
		return mPlayer.isPlaying();
	}
	
	public boolean isPrepared() {
		if (null == mPlayer)
			return false;
		
		return mIsPrepared;
	}
	
	public boolean isSeeking() {
		if (null == mPlayer)
			return false;
		
		return mIsSeeking;
	}
	
	public void start() {
		if (null == mPlayer)
			return;
		
		if (isPlaying())
			return;
		
		mStartThread = new Thread("videoPlayerStartThread"){
    		public void run() {
    			mPlayer.start();
			}
    	};
    	
    	mStartThread.start();
	}
	
	public void startBtn() {
		start();
		mPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_video_pause_btn_bg));
	}
	
	public void pause() {
		if (!isPlaying())
			return;
		
		mPauseThread = new Thread("videoPlayerPauseThread"){
    		public void run() {
    			mPlayer.pause();
			}
    	};
    	
    	mPauseThread.start();
	}
	
	private void pause(int message) {
		pause();
		mIsPrepared = false;
		mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_VIDEO_PLAYER, message, 0));
	}
	
	public void pauseBtn() {
		pause();
		mPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_video_play_btn_bg));
	}
	
	private void seekTo(int time) {
		if (null == mPlayer)
			return;
		
		if (0 > time)
			time = 0;
		
		if (mDuration < time)
			time = mDuration;
		
		final int seekTime = time;
		
		mSeekThread = new Thread("videoPlayerSeekThread"){
    		public void run() {
    			mPlayer.seekTo(seekTime);
			}
    	};
    	
    	mSeekThread.start();
	}
	
	private void seekComplete() {
		mIsSeeking = false;
	}
	
	private void stop() {
		if (!isPlaying())
			return;
		
		mPlayer.stop();
		mIsPrepared = false;
	}
	
	private void release() {
		if (null == mPlayer)
			return;
		
		mPlayer.release();
		mPlayer = null;
	}
	
	private void end() {
		if (isPlaying()) 
			stop();
		
		stopPlayThread();
		
		release();
	}
	
	private void end(int message) {
		end();
		mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_VIDEO_PLAYER, message, 0));
	}
	
	private void end(int what, int message1, int message2) {
		end();
		mHandler.sendMessage(mHandler.obtainMessage(what, message1, message2));
	}
}
