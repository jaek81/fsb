package com.yoons.fsb.student.custom;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.hssb.student.util.VorbisFileInputStream;

/**
 * Ogg File Player Class vorbis library, android.media.AudioTrack 사용
 *
 * @author jaek
 */
public class CustomVorbisPlayer {

    public interface OnVorbisSeekCompleteListener {
        public void OnSeekCompleteEvent(int time, int btnTag);
    }

    private OnVorbisSeekCompleteListener listener;

    private Handler mHandler = null;

    private AudioTrack mPlayer = null;

    private VorbisFileInputStream mVorbisFileInputStream = null;

    private Thread mPlayThread = null, mSeekThread = null;

    private int mMinSize = 0;

    private int mDuration = 0;

    private int mPrevPos = 0;

    private int mCurPos = 0;

    private int mSeekPos = 0;

    private int mMaxPlayPos = 0;

    private int mCurBtnTag = 0;

    private boolean mIsFwdAble = false;

    private boolean mIsMoreFast = false;

    private boolean mIsSeekBack = false;

    private boolean mIsSeekFwd = false;

    private boolean mIsSeekExcute = false;

    private boolean mIsProcess = false;

    private boolean mStop = false;//플레이 쓰레드 read여부

    public final View.OnTouchListener seekTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent event) {
            try {
                int btnTag = (Integer) view.getTag();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (singleProcessChecker() || isSeeking())
                            return true;

                        view.playSoundEffect(SoundEffectConstants.CLICK);
                        playerSeekPrecede(btnTag);
                        break;

                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_OUTSIDE:
                        playerSeekExcute(btnTag);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    };

    public CustomVorbisPlayer(Handler handler, String filePath) {
        //Log.e("hy", "CustomVorbisPlayer");
        try {
            mHandler = handler;
            setPlayer(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CustomVorbisPlayer(Handler handler, String filePath, boolean isFwdAble) {
        //Log.e("hy", "CustomVorbisPlayer2");
        try {
            mHandler = handler;
            mIsFwdAble = isFwdAble;
            setPlayer(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Player를 설정함 AudioTrack, VorbisFileInputStream, PlayThread 생성
     *
     * @param filePath 재생 파일 경로
     */
    private void setPlayer(String filePath) {
        //Log.e("hy", "setPlayer");
        if (null != mPlayer)
            mPlayer = null;
        try {
            mMinSize = android.media.AudioTrack.getMinBufferSize(ServiceCommon.PLAYER_SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);

            mPlayer = new AudioTrack(AudioManager.STREAM_MUSIC, ServiceCommon.PLAYER_SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, mMinSize, AudioTrack.MODE_STREAM);
            mVorbisFileInputStream = new VorbisFileInputStream(filePath);

            mCurPos = 0;
            mMaxPlayPos = 0;
            mDuration = getDuration();

            startPlayThread();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player의 상태값을 반환함
     *
     * @return Player의 상태값
     */
    public int getState() {
        //Log.e("hy", "getState");
        return mPlayer.getState();
    }

    /**
     * Player의 Play 상태값을 반환함
     *
     * @return Player의 Play 상태값
     */
    public int getPlayState() {
        //Log.e("hy", "getPlayState");
        return mPlayer.getPlayState();
    }

    /**
     * 총 재생 시간을 반환함
     *
     * @return 총 재생 시간
     */
    public int getDuration() {
        //Log.e("hy", "getDuration");
        try {
            if (null != mVorbisFileInputStream)
                mDuration = (int) (mVorbisFileInputStream.getDuration() * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mDuration;
    }

    /**
     * 현재 재생 시간을 반환함
     *
     * @return 현재 재생 시간
     */
    public int getCurrentPosition() {
        //Log.e("hy", "getCurrentPosition");
        if (null != mVorbisFileInputStream) {
            mPrevPos = mCurPos;

            try {
                mCurPos = (int) (mVorbisFileInputStream.getCurrentPosition() * 1000);
                //Log.e("hy", "getCurrentPosition1" + mCurPos);
            } catch (Exception e) {
                //Log.e("hy", "getCurrentPosition2" + mPrevPos);
                mCurPos = mPrevPos;
            }

            try {
                if (isSeeking()) {
                    //Log.e("hy", "getCurrentPosition3");
                    if (mSeekPos != mCurPos) {
                        mCurPos = mSeekPos;
                        //Log.e("hy", "getCurrentPosition4");
                    }
                }

                if (mDuration < mCurPos || 0 > mCurPos) {
                    mCurPos = mPrevPos;
                    //Log.e("hy", "getCurrentPosition5");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mCurPos;
    }

    /**
     * Player의 재생 여부를 반환함
     *
     * @return Player의 재생 여부
     */
    public boolean isPlaying() {
        //Log.e("hy", "isPlaying");
        boolean isPlaying = false;

        try {
            if (mPlayer.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                //Log.e("hy", "isPlaying1");
                isPlaying = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return isPlaying;
    }

    /**
     * AudioTrack Write & Update Progress Thread
     */
    private void startPlayThread() {
        //Log.e("hy", "startPlayThread");
        try {
            mPlayThread = new Thread("vorbisPlayerPlayThread") {
                public void run() {
                    mStop = false;
                    //Log.e("hy", "startPlayThread2");
                    try {
                        short[] buffer = new short[mMinSize];
                        int readSize = 0;

                        while (null != mPlayThread && !mPlayThread.isInterrupted()) {
                            //Log.e("hy", "startPlayThread3");
                            if (isPlaying()) {
                                //Log.e("hy", "startPlayThread4");
                                if (!mStop) {

                                    if (null != mVorbisFileInputStream) {
                                        //Log.e("hy", "startPlayThread5" + "buffer" + buffer.length);
                                        readSize = mVorbisFileInputStream.read(buffer);
                                    }

                                    if (0 <= readSize) {
                                        //Log.e("hy", "startPlayThread6" + buffer.length + "," + readSize);
                                        mPlayer.write(buffer, 0, readSize);
                                    }

                                    updatePlayProgress();
                                }
                            } else {
                                //Log.e("hy", "startPlayThread7");
                                SystemClock.sleep(100);
                                if (mIsSeekExcute) {
                                    //Log.e("hy", "startPlayThread8");
                                    mPlayer.play();
                                    mIsSeekExcute = false;
                                    if (mCurBtnTag == ServiceCommon.BUTTONTAG_SEEK_BACK) {
                                        //Log.e("hy", "startPlayThread9");
                                        mIsSeekBack = false;
                                    } else {
                                        //Log.e("hy", "startPlayThread10");
                                        mIsSeekFwd = false;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //Log.e("hy", "startPlayThread11");
                        mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));
                    }
                }
            };
            if (mPlayThread != null) {
                mPlayThread.start();
                //Log.e("hy", "startPlayThread12");
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.e("hy", "startPlayThread13");
        }
    }

    /**
     * PlayThread를 종료함
     */
    private void stopPlayThread() {
        //Log.e("hy", "stopPlayThread");
        try {
            if (null != mPlayThread) {
                //Log.e("hy", "stopPlayThread1");
                mPlayThread.interrupt();
                //Log.e("hy", "stopPlayThread2");
                mPlayThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 재생시 Progress를 업데이트 & 빨리감기 버튼 활성화 여부를 판단하여 메시지를 전달함
     */
    private void updatePlayProgress() {
        //Log.e("hy", "updatePlayProgress");
        try {
            if (null != mPlayThread && !mPlayThread.isInterrupted()) {
                if (mDuration == mCurPos) {
                    mStop = true;
                    //Log.e("hy", "updatePlayProgress2");
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));
                }
                if (!mIsFwdAble) {
                    //Log.e("hy", "updatePlayProgress3");
                    if (mMaxPlayPos < mCurPos) {
                        mMaxPlayPos = mCurPos;
                        //Log.e("hy", "updatePlayProgress4");
                    }

                    if (mMaxPlayPos > mCurPos) {
                        mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_SEEK_FWD_ABLE, 0));
                        //Log.e("hy", "updatePlayProgress5");
                    } else {
                        mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_SEEK_FWD_DISABLE, 0));
                        //Log.e("hy", "updatePlayProgress6");
                    }
                }
                if (mStop) {
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_UPDATE, mDuration));
                } else {
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_UPDATE, getCurrentPosition()));
                }
                //Log.e("hy", "updatePlayProgress7");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 재생을 시작함
     */
    public void play() {
        //Log.e("hy", "play");
        try {
            if (null == mPlayer)
                return;

            if (!isPlaying()) {
                mPlayer.play();
                StudyData.getInstance().audioPlaying = ServiceCommon.STATUS_PLAY;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 재생을 일시정지함
     */
    public void pause() {
        //Log.e("hy", "pause");
        try {
            stopSeekThread();

            if (null == mPlayer) {
                //Log.e("hy", "pause1");
                return;
            }

            if (isPlaying()) {
                //Log.e("hy", "pause2");
                mPlayer.pause();
                StudyData.getInstance().audioPlaying = ServiceCommon.STATUS_PAUSE;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 재생을 멈춤
     */
    public void stop() {
        //Log.e("hy", "stop");
        try {
            stopSeekThread();

            if (null == mPlayer)
                return;

            if (isPlaying()) {
                mPlayer.stop();
                StudyData.getInstance().audioPlaying = ServiceCommon.STATUS_STOP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player 사용을 종료함
     */
    public void release() {
        //Log.e("hy", "release");
        try {

            stopPlayThread();

            Thread releaseThread = new Thread("vorbisPlayerReleaseThread") {
                public void run() {
                    try {
                        //Log.e("hy", "release1");
                        if (null != mVorbisFileInputStream) {
                            //Log.e("hy", "release2");
                            mVorbisFileInputStream.close();
                            mVorbisFileInputStream = null;
                        }
                        //Log.e("hy", "release3");
                        mPlayer.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            releaseThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player의 Seeking 여부를 반환함
     *
     * @return Player의 Seeking 여부
     */
    public boolean isSeeking() {
        //Log.e("hy", "isSeeking");
        boolean isSeeking = false;

        if (mIsSeekBack || mIsSeekFwd)
            isSeeking = true;
        //Log.e("hy", "isSeeking" + isSeeking);
        return isSeeking;
    }

    /**
     * 원하는 재생 시간으로 이동함
     *
     * @param time 원하는 재생 시간
     */
    public void seekTo(double time) {
        //Log.e("hy", "seekTo->" + time);
        try {
            if (null != mVorbisFileInputStream)
                mVorbisFileInputStream.seekTo(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void seekTo(double time, boolean isStart) {
        //Log.e("hy", "seekTo->" + time + "b" + isStart);
        try {
            mCurPos = (int) (time * 1000);
            seekTo(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Seek Thread를 시작함
     */
    public void startSeekThread(Runnable runnable) {
        //Log.e("hy", "startSeekThread");
        try {
            mSeekThread = new Thread(runnable);
            mSeekThread.setName("vorbisPlayerSeekThread");
            mSeekThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Seek Thread를 종료함
     */
    public void stopSeekThread() {
        //Log.e("hy", "stopSeekThread");
        try {
            if (null != mSeekThread) {
                mSeekThread.interrupt();
                mSeekThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Seek 진행시 Seek 시간을 계산하여 Progress 표시를 위한 시간 정보를 전달함
     *
     * @param isBack Seek 역방향 여부
     */
    private void playerSeek(boolean isBack) {
        //Log.e("hy", "playerSeek");
        try {
            if (isBack)
                mSeekPos -= mIsMoreFast ? ServiceCommon.SEEK_TIME_30SEC : ServiceCommon.SEEK_TIME_3SEC;
            else
                mSeekPos += mIsMoreFast ? ServiceCommon.SEEK_TIME_30SEC : ServiceCommon.SEEK_TIME_3SEC;

            if (mDuration < mSeekPos)
                mSeekPos = mDuration;

            if (0 > mSeekPos)
                mSeekPos = 0;

            if (!mIsFwdAble) {
                if (mMaxPlayPos < mSeekPos)
                    mSeekPos = mMaxPlayPos;
            }

            if (18000 < Math.abs(mSeekPos - mCurPos))
                mIsMoreFast = true;

            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_UPDATE, mSeekPos));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * playerSeek() 함수를 수행할 Thread를 시작하며 Player는 일시정지 시킴
     *
     * @param btnTag Seek Button의 방향을 알 수 있는 Tag값
     */
    public void playerSeekPrecede(final int btnTag) {
        //Log.e("hy", "playerSeekPrecede");
        try {
            if (btnTag == ServiceCommon.BUTTONTAG_SEEK_BACK)
                mIsSeekBack = true;
            else
                mIsSeekFwd = true;

            Runnable runnable = new Runnable() {
                public void run() {
                    int sleep = 0;
                    try {
                        while (null != mSeekThread && !mSeekThread.isInterrupted()) {
                            if (0 == sleep % 300) {
                                playerSeek(btnTag == ServiceCommon.BUTTONTAG_SEEK_BACK ? true : false);
                                sleep = 0;
                            }

                            SystemClock.sleep(100);
                            sleep += 100;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                }
            };

            mIsMoreFast = false;
            mSeekPos = mCurPos;
            mPlayer.pause();
            startSeekThread(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * playerSeek() 함수에서 계산된 재생 위치로 최종 seekTo()를 수행 또한 일시정지된 Player를 play 가능하도록
     * mIsSeekExcute 값을 설정함
     *
     * @param btnTag Seek Button의 방향을 알 수 있는 Tag값
     */
    public void playerSeekExcute(final int btnTag) {
        //Log.e("hy", "playerSeekExcute");
        try {
            boolean isExcute = false;

            getCurrentPosition();

            if (mIsSeekBack && btnTag == ServiceCommon.BUTTONTAG_SEEK_BACK)
                isExcute = true;
            else if (mIsSeekFwd && btnTag == ServiceCommon.BUTTONTAG_SEEK_FWD)
                isExcute = true;

            if (isExcute) {
                stopSeekThread();
                seekTo((double) mSeekPos / 1000);
                mIsSeekExcute = true;
                mCurBtnTag = btnTag;

                if (listener != null) {
                    listener.OnSeekCompleteEvent(mSeekPos, btnTag);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnVorbisSeekCompleteListener(OnVorbisSeekCompleteListener listener) {
        //Log.e("hy", "setOnVorbisSeekCompleteListener");
        this.listener = listener;
    }

    /**
     * Process 진행 시간(300msec) 동안 다른 Process 요청을 방지하기 위해 플래그를 설정함
     *
     * @return Process 진행 여부
     */
    public boolean singleProcessChecker() {
        //Log.e("hy", "singleProcessChecker");
        try {
            if (mIsProcess)
                return true;

            mIsProcess = true;

            Thread flagThread = new Thread() {
                public void run() {
                    SystemClock.sleep(300);
                    mIsProcess = false;
                }
            };

            flagThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}