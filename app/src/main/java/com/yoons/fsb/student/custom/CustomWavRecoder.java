package com.yoons.fsb.student.custom;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import com.yoons.hssb.student.util.SimpleLame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CustomWavRecoder {
	private final String TAG = "CustomWavRecoder";
	private static final int RECORDER_BPP = 16;
	private static final int RECORDER_SAMPLERATE = 16000;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

	private AudioRecord mRecorder = null;
	private int mBufferSize = 0;
	private Thread mRecordingThread = null;
	private boolean mIsRecording = false;
	private boolean mIsStopThread = false;
	private String mRecordFilePath = "";
	private String mTempFilePath = "";
	private String mMp3FilePath = "";

	public CustomWavRecoder(String recordPath) {
		mBufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING) * 100;

		mRecordFilePath = recordPath + ".wav";
		mTempFilePath = recordPath + ".raw";
		mMp3FilePath = recordPath + ".mp3";
	}

	public void startRecording() {
		mRecorder = new AudioRecord(MediaRecorder.AudioSource.VOICE_RECOGNITION, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, mBufferSize);

		int i = mRecorder.getState();
		if (i == 1)
			mRecorder.startRecording();

		mIsRecording = true;

		mRecordingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				writeAudioDataToFile();
			}
		}, "AudioRecorder Thread");

		mRecordingThread.start();
	}

	private void writeAudioDataToFile() {
		byte data[] = new byte[mBufferSize];
		FileOutputStream os = null;

		try {
			os = new FileOutputStream(mTempFilePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int read = 0;

		if (null != os) {
			mIsStopThread = false;
			while (mIsRecording) {
				read = mRecorder.read(data, 0, mBufferSize);
				//                    Log.e("READ", "read value : " + read);

				if (read > 0) {
					//                    if(AudioRecord.ERROR_INVALID_OPERATION != read){
					try {
						os.write(data, 0, read);
						os.flush();
						os.getFD().sync();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			try {
				os.flush();
				os.getFD().sync();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			mIsStopThread = true;
		}
	}

	public void stopRecording() {
		if (null != mRecorder) {
			mIsRecording = false;
			int i = mRecorder.getState();
			if (i == 1)
				mRecorder.stop();
			mRecorder.release();

			mRecorder = null;
			mRecordingThread = null;
		}

		while (!mIsStopThread) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		copyWaveFile(mTempFilePath, mRecordFilePath);

		wavToMp3();

		deleteTempFile();
	}

	private void deleteTempFile() {
		File file = new File(mTempFilePath);

		file.delete();
	}

	private void copyWaveFile(String inFilename, String outFilename) {
		FileInputStream in = null;
		FileOutputStream out = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = 1;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

		byte[] data = new byte[mBufferSize];
		int readbytes = 0;

		try {
			in = new FileInputStream(inFilename);
			out = new FileOutputStream(outFilename);
			totalAudioLen = in.getChannel().size();
			totalDataLen = totalAudioLen + 36;

			Log.e("", "File size: " + totalDataLen);

			WriteWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate, channels, byteRate);

			while ((readbytes = in.read(data)) != -1) {
				out.write(data, 0, readbytes);
			}

			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen, long totalDataLen, long longSampleRate, int channels, long byteRate) throws IOException {

		byte[] header = new byte[44];

		header[0] = 'R'; // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f'; // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16; // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1; // format = 1
		header[21] = 0;
		header[22] = (byte) channels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (2 * 16 / 8); // block align
		header[33] = 0;
		header[34] = RECORDER_BPP; // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

		out.write(header, 0, 44);
	}

	public void CombineWaveFile(String file1, String file2, String mMergFilePath) {
		FileInputStream in1 = null, in2 = null;
		FileOutputStream out = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = 2;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

		byte[] data = new byte[mBufferSize];

		try {
			in1 = new FileInputStream(file1);
			in2 = new FileInputStream(file2);

			out = new FileOutputStream(mMergFilePath);

			totalAudioLen = in1.getChannel().size() + in2.getChannel().size();
			totalDataLen = totalAudioLen + 36;

			WriteWaveFileHeader(out, totalAudioLen, totalDataLen, longSampleRate, channels, byteRate);

			while (in1.read(data) != -1) {

				out.write(data);

			}
			while (in2.read(data) != -1) {

				out.write(data);
			}

			out.close();
			in1.close();
			in2.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void wavToMp3() {
		SimpleLame.init(RECORDER_SAMPLERATE, 1, RECORDER_SAMPLERATE, 32);

		try {
			File srcFile = new File(mRecordFilePath);
			//short[] shortArr = new short[RECORDER_SAMPLERATE * (16 / 8) * 2 * 5];
			short[] shortArr = new short[(int) srcFile.length()];
			byte[] mp3buffer = new byte[(int) (7200 + shortArr.length * 2 * 1.25)];

			Log.e("test", "RECORDER_SAMPLERATE:"+RECORDER_SAMPLERATE+",shortArr.length"+shortArr.length);
			Log.e("test", "shortArr[]:"+shortArr.length+",mp3buffer"+mp3buffer.length);
			
			
			FileOutputStream output2 = null;
			output2 = new FileOutputStream(new File(mMp3FilePath));

			
			if (srcFile.isFile()) {
				Log.e("test", "shortArr[]:"+shortArr.length+",mp3buffer"+mp3buffer.length+"srcFile"+srcFile.length());
				FileInputStream in2 = new FileInputStream(srcFile);
				//ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("gilad-OutPut.bin"));
				byte[] buf = new byte[(int) srcFile.length()];

				int k = in2.read(buf);
				for (int i = 0; i < buf.length / 2; i++) {
					//	output.writeShort((short) ((buf[i * 2] & 0xff) | (buf[i * 2 + 1] << 8)));
					shortArr[i] = ((short) ((buf[i * 2] & 0xff) | (buf[i * 2 + 1] << 8)));
				}

				in2.close();

				int a = SimpleLame.encode(shortArr, shortArr, k, mp3buffer);

				output2.write(mp3buffer, 0, (int) (a * 0.55));
				int b = SimpleLame.flush(mp3buffer);
			}
		} catch (Exception e) {
			Log.e("test", "에러남!!");
			e.printStackTrace();
			// TODO: handle exception
		}

	}

}
