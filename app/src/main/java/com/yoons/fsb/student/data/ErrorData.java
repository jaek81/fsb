package com.yoons.fsb.student.data;


public class ErrorData {
	public static final int ERROR_UNKNOWN = 1;	// 오류가 기록되지 않았을 때
	public static final int ERROR_NOT_AVAILABLE_NETWORKS = 100;	// 일반 메타데이터 통신시 네트워크 연결 오류
	public static final int ERROR_NOT_AVAILABLE_NETWORKS2 = 101;	// 컨텐츠 다운로드 시도시 네트워크 연결 오류.
	public static final int ERROR_UPLOAD_SYNC = 102;	// 업로드 sync Error
	public static final int ERROR_DOWNLOAD_SYNC = 103;	// 다운로드 sync Error
	public static final int ERROR_HTTP_REQUEST = 104;	// HTTP request Error
	public static final int ERROR_HTTP_FILE_UPLOAD = 105;	// 음원 파일 업로드 오류
	public static final int ERROR_HTTP_FILE_DOWNLOAD = 106;	// 콘텐츠 파일 다운로드 오류
	public static final int ERROR_HTTP_FILE_NOT_FOUND = 107;	// 콘텐츠 파일이 서버에 없음.
	
	private static ErrorData mErrorData = null;
	private static int mErrorCode = ERROR_UNKNOWN; 
	private static String mErrorMsg = "";
	
	public static void setErrorMsg(int errorCode, Exception e) {
		String str = e.toString();
		if(str != null){
			int index = str.indexOf(":");
			
			if(index == -1) {
				setErrorMsg(errorCode, str);
			} else {			
				setErrorMsg(errorCode, str.substring(0, index));
			}
		} else {
			setErrorMsg(errorCode, str);
		}
	}
	
	public static void setErrorMsg(int errorCode, String error) {
		if(mErrorData == null) {
			mErrorData = new ErrorData();
		}
		
		mErrorCode = errorCode;
		mErrorMsg = error;
	}
	
	public static String getErrorMsg() {
		if(mErrorData == null) {
			mErrorData = new ErrorData();
		}
		
		return String.format("[%d] %s", mErrorCode, mErrorMsg);
	}
	
	public static int getErrorCode() {
		return mErrorCode;
	}
}
