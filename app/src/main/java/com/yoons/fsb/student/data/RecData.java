package com.yoons.fsb.student.data;

import lombok.ToString;

@ToString
public class RecData {

	public int customer_no = 0; // 회원번호 //I_CUSTOMER_NO
	public int product_no = 0; // 교재코드 //I_PRODUCT_NO
	public String study_unit_code = ""; // 학습단위코드 //I_STUDY_UNIT_CODE
	public int re_study_cnt = 0; // 재학습순서 //I_RE_STUDY_CNT
	public int study_kind = 0; //학습구분(1:본학습, 2:복습) //I_STUDY_KIND
	public int study_order = 0; //  순서 //I_STUDY_ORDER
	public int is_correct = 0; // 정답여부
	public String is_correct_answer = ""; // 정답
	public String is_customer_answer = ""; // 회원 답
	public int is_upload = 1; // 업로드 여부
	public int sentence_question_no = 0;
	public int study_result_no = 0; // 클래스id 
	public int book_detail_id; //차시
	public int student_book_id; //회원-교재 ID
	public String rec_type = ""; // 녹음 타입
	public int start_tag_time = 0; // 태그 시간
	public String start_tag_name = ""; // 태그 이름
	public String end_tag_time = ""; // 태그 시간
	public int start_merge_time = 0; // 병합 첫시간
	public int end_merge_time = 0; // 병합 마지막시간
	public int rec_len = 0; // 녹음시간
	public String reTryCnt = "";//
	public int rep_cnt = 0; // 반복횟수
	
	public int is_merge = 0; // 병합 여부(0:false 1:true)
	public String crud = ""; // 수정 여부
	public String file_path = ""; // 파일 경로
	
	
	
	public String url = "";//통신 URL
	public String blob_url = "";//중앙화 전용 파일경로

}
