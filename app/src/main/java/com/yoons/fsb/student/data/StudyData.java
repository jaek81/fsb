package com.yoons.fsb.student.data;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.db.DatabaseData.TodayStudyUnit;

import java.util.ArrayList;

/**
 * 학습을 진행하기 위해 생성하는 data Class
 *
 * @author ejlee
 */

// 학습 시작전 학습을 선택하면 생성되는 내용
public class StudyData {
    private static final String TAG = "[StudyData]";
    private static StudyData mStudyData = null;

    private static ArrayList<TodayStudyUnit> mStudyUnitList = null;

    public long mStudyStartTime = 0; // 학습 시작 시간

    // 학습 옵션 관련 내용
    public boolean mIsAutoArtt = false; // 오디오 학습 자동 artt (사용자 정보에 내려옴)
    public boolean mIsCaption = false; // 학습모드(true : 캡션을 보여줌, false : 보여주지 않음)
    public boolean mIsStudyGuide = true; // 옵션에 설정되어 있음.
    public boolean mIsFastRewind = true; // 빨리 감기 여부
    public boolean mIsDictation = false; // 받아쓰기 할지 여부
    public int mPracticeCnt = 3; // 본학습 틀린 문제 반복 횟수
    public int mReviewPracticeCnt = 3;// 보충학습 틀린 문제 반복 횟수
    public boolean mIsAudioRecordUpload = false; // 오디오 학습 태그 녹음 파일 업로드 여부
    public boolean mIsParagraph = true; // 문단연습 할지 여부
    public boolean mIsVIPSkip = true; // VIP활동만 있는 차시의 경우  여부
    public boolean mIsTextKorean = false; // IGSE 한글 해석 스킵

    //본 학습정보(복습도 동일)
    public int mCustomerNo = 0; // 회원 번호

    public String mStudyPlanYmd = ""; // 본 학습 계획일
    public String mRecordFileYmd = ""; // 본 학습 녹음 파일 일자
    public int mProductNo = 0; // 교재코드
    public String mStudyUnitCode = ""; // 학습 단위 코드
    public int mReStudyCnt = 0; // 재학습순서 (학습 계획에서 가져오며 db기록할때 사용함)
    public int mStudyUnitNo = 0; // 학습 순서(차시)
    public String mProductName = ""; // 교재이름 화면 상단에 표시하는 이름.
    public int mSeriesNo = 0; // 시리즈 번호
    public String mSeriesName = ""; // 시리즈 이름
    public int mBookNo = 0; // 권 번호
    public int mRealBookNo = 0; 		// 화면상 표시되는 권 번호
    public String mAudioTagFile = ""; // 오디오 테그 파일
    public String mAudioSoundFile = ""; // 오디오 학습 음원 파일 경로
    // 신규문항
    public String mPopupStudyPath = ""; // 팝업 학습 콘텐츠 파일 경로
    //	public String 	mVideoFile = "";		// 비디오 학습 파일 경로(jhcho )
    public ArrayList<String> mVideoFile = new ArrayList<String>();    // 비디오 학습 파일 경로
    public int mAudioTotalMin = 0; // 오디오 파일 음원 길이(분)
    public String mAudioRecordPath = ""; // 오디오 학습 녹음 파일 이름 prefix
    public int mAudioRecordIndex = 0; // 오디오 학습 녹음 파일 index
    public String mAudioRecordFilePath = ""; // 에코에 들려줄 오디오 학습 녹음 파일
    public boolean mIsVoiceRecoginition = false; // 음성 인식 여부
    public int mAudioStudyTime = 0; // 오디오 학습 진행시간(초), 중단학습에서 이어서 할때 사용하는 정보로 실시간으로 기록하지는 않음.
    public int mRTagRepeatCnt = 1; // R1~R2 반복 횟수
    public ArrayList<PageInfo> mPageInfo = new ArrayList<PageInfo>(); // 페이지 정보
    public ArrayList<WordQuestion> mWordQuestion = new ArrayList<WordQuestion>(); // 단어 시험 문제
    public ArrayList<SentenceQuestion> mSentenceQuestion = new ArrayList<SentenceQuestion>(); // 문장 시험 문제
    public ArrayList<OneWeekData> mOneWeekData = new ArrayList<OneWeekData>(); // 문장 시험 문제
    public ArrayList<String> mDictation = new ArrayList<String>(); // 받아쓰기 path가 있음.
    public ArrayList<WordResult> mWordResult = new ArrayList<WordResult>();// 단어 시험 결과
    public ArrayList<SentenceResult> mSentenceResult = new ArrayList<SentenceResult>();// 문장 시험 결과
    public ArrayList<WordQuestion> mWrongWordQuestion = new ArrayList<WordQuestion>(); // 단어 시험 중 틀린 문제
    public ArrayList<SentenceQuestion> mWrongSentenceQuestion = new ArrayList<SentenceQuestion>(); // 문장 시험 중 틀린 문제
    public ArrayList<String> mAniTalk = new ArrayList<String>(); // 애니톡 URL
    public boolean mIsMovieExist = false; // 문법 동영상이 있는지 여부
    public boolean mIsAudioExist = false; // 학습음원이 있는지 여부
    public boolean mIsZipExist = false; // popup학습이 있는지 여부
    public boolean mIsOldAnswerExist = false; // 숲 구교재있는지 여부
    public String mMovieFile = ""; // 문법 동영상 URL
    public String mOldAnswerFile = ""; // 구교재 정답파일

    //추가 딕테이션
    public boolean mIsDicPirvate = true; // 딕테이션 실행여부
    public boolean mIsDicAnswer = true; // 딕테이션_정답 노출여부


    public int mStudyResultNo = 0;//classid
    public String rRecType = "";
    //c추가
    public int mBookDetailId = 0; //
    public int mStudentBookId = 0; //
    public String mReviewStudyStatus = ""; //

    //숲우영
    //	public int mAgencyCode = 0; //center_code (jhcho )
    //	public int mClassNo = 0;
    //	public int mSiteKind = 0;

    //문단연습
    public ArrayList<VanishingQuestion> mVanishingQuestion = new ArrayList<VanishingQuestion>(); // 문단 시험 문제
    public ArrayList<VanishingResult> mVanishingResult = new ArrayList<VanishingResult>();// 문단 시험 결과
    public String[] mVanishingList;//문단 공백 리스트

    // 신규문항
    public ArrayList<ExamResult> mExamResult = new ArrayList<ExamResult>(); // Popup 학습 결과

    // 현재 학습 상태
    // 본학습 학습상태 코드(S01:학습시작, S11:본학습_음원듣기시작,S12:종료, S21:시험준비,S22 , S31,S32:본학습_단어, S41,S42:본학습_문장, S51,S52:본학습_받아쓰기, S61,S62:본학습_문법동영상, SFN:학습종료)
    // 복습 학습상태 코드 (R01:학습시작(복습), R11:복습_음원듣기시작,R12:종료.., R31,R32:복습_단어, R41,R42:복습_문장, RFN:학습종료(복습))
    public String mCurrentStatus = "";

    // 보충학습(여기는 db테이블 기준으로 하므로 review로 함)
    public boolean mIsReviewExist = false; // 보충할 학습이 있는지 여부(최초 실행시나 중단 학습일 경우에 없을 수 있음)
    public String mReviewStudyPlanYmd = ""; // 학습 계획일
    public int mReviewProductNo = 0; // 교재 코드
    public String mReviewStudyUnitCode = ""; // 학습 단위 코드
    public int mReviewReStudyCnt = 0; // 재 학습 순서
    public int mReviewStudyUnitNo = 0; // 학습순서(차시)
    public String mReviewProductName = ""; // 교재이름 (시리즈 이름 + 권 + 학습단위코드)
    public int mReviewSeriesNo = 0; // 시리즈 번호
    public String mReviewSeriesName = ""; // 시리즈 이름
    public int mReviewBookNo = 0; // 권번호
    public boolean mReviewIsBodyExist = false; // 본문 음원 파일이 있는지 여부
    public boolean mReviewISBodyVideoExist = false;// 복습 동영상 파일이 있는지 여부
    public String mReviewBodySoundFile = ""; // 본문 음원 파일 경로
    public String mReviewBodyVideoFile = "";
    public boolean mReviewCheck = false;//R01체크
    public int mReStudyResultNo = 0;//복습 resultNo
    public int mReviewDuration = 0;
    //복습 시간
    // 본문 음원 파일 경로
    /*
	 * public String mReviewAudioRecordPath = ""; // 복습 오디오 학습 녹음 파일 이름 prefix
	 * public int mReviewAudioRecordIndex = 0; // 복습 오디오 학습 녹음 파일 index
	 */ public boolean mIsReviewVoiceRecoginition = false; // 음성 인식 여부
    public ArrayList<WordQuestion> mReviewWordQuestion = new ArrayList<WordQuestion>(); // 단어 시험 문제
    public ArrayList<SentenceQuestion> mReviewSentenceQuestion = new ArrayList<SentenceQuestion>(); // 문장 시험 문제
    public ArrayList<WordResult> mReviewWordResult = new ArrayList<WordResult>(); // 단어 시험 결과
    public ArrayList<SentenceResult> mReviewSentenceResult = new ArrayList<SentenceResult>(); // 문장 시험 결과
    public ArrayList<WordQuestion> mWrongReviewWordQuestion = new ArrayList<WordQuestion>(); // 틀린 이전학습 복습 단어 문제
    public ArrayList<SentenceQuestion> mWrongReviewSentenceQuestion = new ArrayList<SentenceQuestion>(); // 틀린 이전학습 복습 문장 문제

    public int mRecRTime = 0;//권장 녹음 시간
    public int mRecMCnt = 0;//수동 누적횟수
    public int mRecMTime = 0;//수동 누적시간
    public int mTotalRecTime = 0;//녹음 총 시간

    public boolean mPassRcheck = false;//수동녹음 체크
    public String audioPlaying = ServiceCommon.STATUS_STOP;//녹음이 됬는지 (stop,pause,play)\

    public boolean call = false;//호출

    public String[] mPopupQuizList;

    //new Dictation 여부
    public boolean isNewDication = false;

    //교재단계 팝퀴즈 용도로 사용
    public int mProductType = 0;

    // 중앙화관련
    public String forestCode = "";
    public String teacherCode = "";
    public String lmsStatus = "";


    // 단어 시험 문제
    public static class WordQuestion {
        public int mWordQuestionNo = 0; // 단어 문제 코드(db 기록시에만 사용함)
        public int mQuestionOrder = 0; // 문제 순번
        public String mQuestion = ""; // 문제
        public String mMeaning = ""; // 뜻
        public int mAnswer = 0; // 정답
        public String mSoundFile = ""; // 단어 음원 파일 경로
        public ArrayList<String> mDistractorList = new ArrayList<String>(); // 보기(4개 고정)
    };

    // 페이지 정보
    public static class PageInfo {
        public String	mType = "";					// 페이지 종류
        public int 		mPageOrder = 0;				// 페이지 순번
        public int		mPageNo = 0;				// 페이지 번호
        public String 	mText = "";					// 페이지 텍스트
        public int		mTagTime = 0;				// 페이지 태그 시간
        public boolean	mIsUsed = true;				// 페이지 사용유무
    };

    // 문장 시험 문제
    public static class SentenceQuestion {
        public int mSentenceQuestionNo = 0; // 문장 문제 코드(db 기록시에만 사용함)
        public int mQuestionOrder = 0; // 문제 순번
        public String mSentence = ""; // 문장
        public String mTextSentence = ""; // 음성 인식 문장
        public String mSoundFile = ""; // 문장 음원 파일 경로
        public String mRecordFile = ""; // 저장해야할 음원 파일 패스
    }

    ;

    // 문단 연습 문제
    public static class VanishingQuestion {
        public int mVanishingQuestionNo = 0; // 문단연습 코드(db 기록시에만 사용함)
        public int mQuestionOrder = 0; // 문제 순번
        public String mSentence = ""; // 문장
        public String mTextSentence = ""; // 음성 인식 문장
        public String mSoundFile = ""; // 문장 음원 파일 경로
        public String mRecordFile = ""; // 저장해야할 음원 파일 패스
        public int mRetryCnt; // 재도전횟수
    }

    ;

    // 단어 시험 결과
    public static class WordResult {
        public int mQuestionOrder = 0; // 문제 순번
        public boolean mIsCorrect = false; // 채점 결과(1 : 맞음, 0 : 틀림)
        public int mCorrectAnswer = 0; // 정답
        public int mCustomerAnswer = 0; // 회원 입력 단어 0이면 5초가 지나서 틀린 것으로 간주함.
        public int mWordQuestionNo = 0; // 단어 코드
        public String mMeaning = ""; // 틀린 단어 연습할 때 보여주는 단어
        public String mSoundFile = ""; // 음원 파일
    }

    // 문장 시험 결과
    public static class SentenceResult {
        public int mQuestionOrder = 0; // 문제 순번
        public int mIsCorrect = 0; // 채점결과
        public String mCorrectAnswer = ""; // 문제의 문장 // mSentence가 들어감. 틀린 문장 연습할 때 보여주는 문장
        public String mCustomerAnswer = ""; // 회원 입력 문장 (음성 인식에 의해 들어가는 데이터)
        public int mSentenceQuestionNo = 0; // 문장 코드
        public String mSoundFile = ""; // 음원 파일
    }

    // 문단 연습 결과
    public static class VanishingResult {
        public int mQuestionOrder = 0; // 문제 순번
        public int mIsCorrect = 0; // 채점결과
        public String mCorrectAnswer = ""; // 문제의 문장 // mSentence가 들어감. 틀린 문장 연습할 때 보여주는 문장
        public String mCustomerAnswer = ""; // 회원 입력 문장 (음성 인식에 의해 들어가는 데이터)
        public int mRetryCnt = 0;
        public int mVanishingQuestionNo = 0; // 문장 코드
        public String mSoundFile = ""; // 음원 파일
    }

    // 신규문항
    // Popup 학습 결과
    public static class ExamResult {
        public int mStudyOrder = 0; // 학습 순번
        public int mIsCorrect = 0; // 채점결과
        public int mTagType = 0; // 태그 유형
        public int mQuestionOrder = 0; // 문제 순번
        public String mQuestionNo = ""; // 문항 코드
        public String mCustomerAnswer = ""; // 회원 입력 답
        public int mMaxRecTime = 0;
    }

    // 학습 결과
    public static class StudyResult {
        public String mProductName = ""; // 학습한 교재 이름
        public int mWordTotal = 0; // 단어 시험 총 개수
        public int mWordCorrect = 0; // 단어 시험 맞은 개수
        public int mSentenceTotal = 0; // 문장 시험 총 개수
        public int mSentenceCorrect = 0; // 문장 시험 맞은 개수
        public int mStudyTotalTime = 0; // 총 학습 시간(분)

        public int mVanishingTotal = 0; // 문단연습 총 개수
        public int mVanishingCorrect = 0; // 문단연습 맞은 개수
    }

    // 일일/주간평가
    public static class OneWeekData {
        public int ret_code = 0; // 결과
        public int quizdw_no = 0; // 문항 번호
        public int book_id = 0; // 교재
        public int book_detail_id = 0; // 차시
        public String sentence = ""; // 문장 단어/구
        public String autosentence = ""; // 음성인식용 문장
        public String means = ""; // 뜻
        public String question_type = ""; // 문제유형 T1. T2
        public String instruction = ""; // 문항지시문
        public String statement = ""; // 제시문
        public String ex_1 = ""; // 보기
        public String ex_2 = ""; // 보기
        public String ex_3 = ""; // 보기
        public String correct = ""; // 정답

        public String audio_path = ""; // 오디오 경로
        public String audio_path2 = ""; // 오디오 경로
        public String choose = "0"; //
        public boolean test_result = false;//테스트 맞고틀림

        public ArrayList<String> distractorList;

        public ArrayList<String> getDistractorList() {
            if (distractorList == null) {
                distractorList = new ArrayList<String>();
                distractorList.add(ex_1);
                distractorList.add(ex_2);
                distractorList.add(ex_3);
            }

            return distractorList;
        }

    }

    /**
     * instance를 구해옴.
     *
     * @return StudyData
     */
    public static StudyData getInstance() {
        if (mStudyData == null) {
            mStudyData = new StudyData();
            mStudyData.mStudyStartTime = System.currentTimeMillis();
        }

        return mStudyData;
    }

    public static ArrayList<TodayStudyUnit> getmStudyUnit() {
        if (mStudyUnitList == null) {
            mStudyUnitList = new ArrayList<TodayStudyUnit>();
        }
        return mStudyUnitList;
    }

    public static void setmStudyUnit(TodayStudyUnit mStudyUnit) {
        if (mStudyUnitList == null) {
            mStudyUnitList = new ArrayList<TodayStudyUnit>();
        }
        mStudyUnitList.add(mStudyUnit);
    }

    /**
     * 이전 데이터 삭제함.
     *
     * @return StudyData
     */
    public static void clear() {
        mStudyData = null;//new StudyData(); ..getInstance에서의 시간 설정 누락으로 인해 수정함
    }

    public static void listClear() {
        mStudyUnitList = null;
    }

    public StudyData() {

    }


}
