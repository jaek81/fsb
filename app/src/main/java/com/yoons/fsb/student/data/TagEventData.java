package com.yoons.fsb.student.data;

/**
 * 스마트 오디오 학습 ARTT를 위한 Tag Data Class
 * 
 * @author jaek
 */
public class TagEventData {

	public static final String TAG_S0 = "S0";
	public static final String TAG_S1 = "S1";
	public static final String TAG_S2 = "S2";
	public static final String TAG_LS0 = "LS0";
	public static final String TAG_LS1 = "LS1";
	public static final String TAG_LS2 = "LS2";
	public static final String TAG_W0 = "W0";
	public static final String TAG_W1 = "W1";
	public static final String TAG_W2 = "W2";
	public static final String TAG_G1 = "G1";
	public static final String TAG_G2 = "G2";
	public static final String TAG_LG1 = "LG1";
	public static final String TAG_LG2 = "LG2";
	public static final String TAG_U0 = "U0";
	public static final String TAG_U1 = "U1";
	public static final String TAG_U2 = "U2";
	public static final String TAG_J1 = "J1";
	public static final String TAG_J2 = "J2";
	public static final String TAG_R1 = "R1";
	public static final String TAG_R2 = "R2";
	public static final String TAG_V = "V";
	public static final String TAG_V1 = "V1";
	public static final String TAG_V2 = "V2";
	// 신규문항
	public static final String TAG_PQ0 = "PQ0";
	public static final String TAG_PQ1 = "PQ1";
	public static final String TAG_PQ2 = "PQ2";
	public static final String TAG_PA0 = "PA0";
	public static final String TAG_PA1 = "PA1";
	public static final String TAG_PA2 = "PA2";
	public static final String TAG_PI0 = "PI0";
	public static final String TAG_PI1 = "PI1";
	public static final String TAG_PI2 = "PI2";
	public static final String TAG_PT0 = "PT0";
	public static final String TAG_PT1 = "PT1";
	public static final String TAG_PT2 = "PT2";
	public static final String TAG_PR0 = "PR0";
	public static final String TAG_PR1 = "PR1";
	public static final String TAG_PR2 = "PR2";

	public static final String TAG_END = "END";

	public static final String TAG_NUM1 = "1";
	public static final String TAG_NUM2 = "2";

	public static final String TAG_PAGE0 = "PAGE0";
	public static final String TAG_PAGE1 = "PAGE1";
	public static final String TAG_PAGE2 = "PAGE2";

	//제외태그
	public static final String TAG_P1 = "P1";
	public static final String TAG_P2 = "P2";

	//mp3 합치는거 관련 태그
	public static final String TAG_M = "M1";
	
	//숲 우영 태그
	public static final String TAG_JF1 = "JF1";
	public static final String TAG_JF2 = "JF2";

	//우영만
	public static final String TAG_JW1 = "JW1";
	public static final String TAG_JW2 = "JW2";

	//IGSE만
	public static final String TAG_O1 = "O1";
	public static final String TAG_O2 = "O2";

	// tag 속성 
	public static final String TAG_ARG_01_SPEAKGUIDE_I = "01_speakguide_i";
	public static final String TAG_ARG_01_TEXTSPEAKGUIDE_I = "01_text(speakguide)_i";
	public static final String TAG_ARG_01_WORDSPEAKGUIDE_I = "01_word(speakguide)_i";
	public static final String TAG_ARG_01_ROLEPLAY_I = "01_roleplay_i";
	public static final String TAG_ARG_01_ROLEPLAY_O = "01_roleplay_o";
	public static final String TAG_ARG_01_ROLEPLAYGUIDE_I = "01_roleplayguide_i";
	public static final String TAG_ARG_01_ROLEPLAYGUIDE_O = "01_roleplayguide_o";

	private static final String[] TAG_EVENT_TYPE_ARRAY = { "S0", "S1", "S2", "W0", "W1", "W2", "LS0", "LS1", "LS2", "G1", "G2", "LG1", "LG2", "U0", "U1", "U2", "J1", "J2",
			"O1", "O2", "R1", "R2", "V", "V1", "V2",  "PQ0", "PQ1", "PQ2", "PA0", "PA1", "PA2", "PI0", "PI1", "PI2", "PT0", "PT1", "PT2", "PR0", "PR1", "PR2", "JF1","JF2","JW1","JW2",
			"PAGE0" ,"PAGE1" ,"PAGE2", "END" };

	public static enum TAG_EVENT_TYPE_ENUM {

		TAG_EVENT_S0, TAG_EVENT_S1, TAG_EVENT_S2, TAG_EVENT_W0, TAG_EVENT_W1, TAG_EVENT_W2, TAG_EVENT_LS0, TAG_EVENT_LS1, TAG_EVENT_LS2,
		TAG_EVENT_G1, TAG_EVENT_G2, TAG_EVENT_LG1, TAG_EVENT_LG2, TAG_EVENT_U0, TAG_EVENT_U1, TAG_EVENT_U2, TAG_EVENT_J1, TAG_EVENT_J2,
		TAG_EVENT_O1, TAG_EVENT_O2, TAG_EVENT_R1, TAG_EVENT_R2, TAG_EVENT_V, TAG_EVENT_V1, TAG_EVENT_V2,
		//신규문항
		TAG_EVENT_PQ0, TAG_EVENT_PQ1, TAG_EVENT_PQ2, TAG_EVENT_PA0, TAG_EVENT_PA1, TAG_EVENT_PA2, TAG_EVENT_PI0, TAG_EVENT_PI1,
		TAG_EVENT_PI2, TAG_EVENT_PT0, TAG_EVENT_PT1, TAG_EVENT_PT2, TAG_EVENT_PR0, TAG_EVENT_PR1, TAG_EVENT_PR2,TAG_JF1,TAG_JF2,TAG_JW1,TAG_JW2,
		//페이지
		TAG_EVENT_PAGE0, TAG_EVENT_PAGE1, TAG_EVENT_PAGE2,
		TAG_EVENT_END;

		private static TAG_EVENT_TYPE_ENUM[] te = null;

		public static int checkEventType(String typeIn) {
			int result = -1;
			for (String type : TAG_EVENT_TYPE_ARRAY) {
				result++;
				if (type.equalsIgnoreCase(typeIn.trim()))
					return result;
			}
			return result;
		}

		public static TAG_EVENT_TYPE_ENUM fromInt(int i) {
			if (null == TAG_EVENT_TYPE_ENUM.te)
				TAG_EVENT_TYPE_ENUM.te = TAG_EVENT_TYPE_ENUM.values();

			return TAG_EVENT_TYPE_ENUM.te[i];
		}
	}

	// Tag 인덱스 
	private int mEventIndex = 0;

	// Tag 시간 
	private int mEventTime = 0;

	// Tag 타입 
	private String mEventType = TAG_EVENT_TYPE_ARRAY[0];

	// Tag 속성 
	private String mEventTypeArg = "";

	// R1~R2 구간의 인덱스  
	private int mRepeatIndex = 0;

	// R1 Tag의 시간 (R2 Tag에서 반복을 시작하기 위해 Seek 되는 시간) 
	private int mRepeatStartTime = 0;

	// V Tag 인덱스 
	private int mVideoIndex = 0;
	
	// V1 Tag 인덱스 
	private int mV1TagIndex = 0;
		
	private int mPageIndex = 0;

	// 신규문항
	// PQ Tag 인덱스
	private int mQuizIndex = 0;

//	// PT Tag 인덱스
//	private int mTouchPlayIndex = 0;
//	
//	// PR Tag 인덱스
//	private int mRolePlayIndex = 0;

	public static String quizJsonArray;

	public TagEventData(int index, String type, int msec, String arg) {
		mEventType = type;
		mEventIndex = index;
		mEventTime = msec;
		mEventTypeArg = arg;
	}

	public TagEventData(int index, String type, int msec, String arg, int repeatIndex, int repeatStartTime, int videoIndex,int v1TagIndex, int quizeIndex, int pageIndex) {
		mEventType = type;
		mEventIndex = index;
		mEventTime = msec;
		mEventTypeArg = arg;
		mRepeatIndex = repeatIndex;
		mRepeatStartTime = repeatStartTime;
		mVideoIndex = videoIndex;
		mV1TagIndex = v1TagIndex;
		mQuizIndex = quizeIndex;
		mPageIndex = pageIndex;
	}

	public void fixEventData(String type, int msec, String arg, int repeatIndex, int repeatStartTime, int quizeIndex) {
		mEventType = type;
		mEventTime = msec;
		mEventTypeArg = arg;
		mRepeatIndex = repeatIndex;
		mRepeatStartTime = repeatStartTime;
		mQuizIndex = quizeIndex;
	}

	public void fixEventData(String type, int msec, String arg, int repeatIndex, int repeatStartTime) {
		mEventType = type;
		mEventTime = msec;
		mEventTypeArg = arg;
		mRepeatIndex = repeatIndex;
		mRepeatStartTime = repeatStartTime;
	}

	public void setEventTime(int time) {
		mEventTime = time;
	}

	public void setRepeatStartTime(int repeatStartTime) {
		mRepeatStartTime = repeatStartTime;
	}

	public int getEventIndex() {
		return mEventIndex;
	}

	public int getEventTime() {
		return mEventTime;
	}

	public String getEventType() {
		return mEventType;
	}

	public String getEventTypeArg() {
		return mEventTypeArg;
	}

	public int getRepeatIndex() {
		return mRepeatIndex;
	}

	public int getRepeatStartTime() {
		return mRepeatStartTime;
	}

	public int getVideoIndex() {
		return mVideoIndex;
	}
	
	public int getV1TagIndex() {
		return mV1TagIndex;
	}

	public int getPageIndex() {
		return mPageIndex;
	}

	// 신규문항
	public int getQuizIndex() {
		return mQuizIndex;
	}

//	public int getTouchPlayIndex() {
//		return mTouchPlayIndex;
//	}
//	
//	public int getRolePlayIndex() {
//		return mRolePlayIndex;
//	}

	/**
	 * Tag 타입을 체크함
	 * 
	 * @return Tag 타입 배열에서 Tag 타입과 일치하는 항목의 인덱스 값
	 */
	public int checkEventType() {
		int result = -1;
		for (String type : TAG_EVENT_TYPE_ARRAY) {
			result++;
			if (type.equalsIgnoreCase(mEventType.trim()))
				return result;
		}
		return result;
	}

	public static synchronized String getQuizJsonArray() {
		return quizJsonArray;
	}

	public static synchronized void setQuizJsonArray(String quizJsonArray) {
		TagEventData.quizJsonArray = quizJsonArray;
	}

}
