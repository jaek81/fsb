package com.yoons.fsb.student.data;

import com.yoons.fsb.student.ServiceCommon;

public class VorbisData {
	/**
	 * The number of channels to be encoded. For your sake, here are the
	 * official channel positions for the first five according to Xiph.org.
	 * one channel - the stream is monophonic
	 * two channels - the stream is stereo. channel order: left, right
     * three channels - the stream is a 1d-surround encoding.
     * 		channel order: left, center, right
     * four channels - the stream is quadraphonic surround. 
     * 		channel order: front left, front right, rear left, rear right
     * five channels - the stream is five-channel surround.
     * 		channel order: front left, center, front right, rear left, rear right
	 */
	public int 		channels = 2;
	
	/**
	 * The number of samples per second of pcm data.
	 */
	public int 		sampleRate = ServiceCommon.PLAYER_SAMPLE_RATE;
	
	/**
	 * The recording quality of the encoding. This is not currently
	 * set for the decoder. The range goes from -.1 (worst) to 1 (best)
	 */
	public float 	quality = 0.4f;
	
	/**
	 * the total number of samples from the recording. This field means
	 * nothing to the encoder.
	 */
	public long		length;
}
