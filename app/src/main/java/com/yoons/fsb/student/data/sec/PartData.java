package com.yoons.fsb.student.data.sec;

import java.util.ArrayList;

import lombok.Data;

@Data
public class PartData {

	/**
	 * 공통
	 */
	private int ret_code = 0;//결과
	private int quiz_no = 0;//문장 훈련 번호id 879..880...
	private int book_id = 0;;//교재
	private int book_detail_id = 0;;//차시
	private int seq = 0;;;//순서
	private String means = "";//단어/문장의미
	private String correct = "0";//정답
	private String ex_1 = "";//보기1
	private String ex_2 = "";//보기2
	private String ex_3 = "";//보기3
	private String score = "";//음성인식점수
	private String choose = "";//선택
	private int answers = 0;//음성인식점수
	private String audio_path = "";//오디오 경로
	private String record_path = "";//녹음 경로
	private String down_url = "";//다운경로
	private ArrayList<String> distractorList = null;//문제 EX1,2,3,4 배열로
	private int bookvolume = 0;//권
	private int bookserise = 0;//시리즈
	private int bookseq = 0;//북 차시
	private String answer = "0";//중단학습일 경우 학생에 푼 답을 전달함	
	private int questionnumber = 0;//순서1..2..3..
	private String book_detail_names = ""; //차시

	/**
	 * part1
	 */

	private String word = "";//단어
	private String ex_4 = "";//보기4

	/**
	 * part2
	 */

	private String sentence = "";//문장 단어/구
	private String autosentence = "";//음성인식용 문장
	private String question_type = "";//문제유형
	private String instruction = "";//문항지시문
	private String statement = "";//제시문
	private String vcsentence = "";//vip 문장
	private String vcautosentence = "";//vip 음성인식 문장

	public ArrayList<String> getDistractorList() {
		if (distractorList == null) {
			distractorList = new ArrayList<String>();
			distractorList.add(ex_1);
			distractorList.add(ex_2);
			distractorList.add(ex_3);
			if (!"".equals(ex_4)) {
				distractorList.add(ex_4);
			}
		}

		return distractorList;
	}

	public boolean is_result() {
		boolean result = false;

		if (!answer.equals("0") && !correct.equals("0") && correct.equals(answer)) {
			result = true;
		}
		return result;
	}

}
