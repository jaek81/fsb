package com.yoons.fsb.student.data.sec;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SecStudyData {
	public static volatile SecStudyData instance = null;

	private Exception exception = null;//QUST통신에서 에러낫을 경우
	//엑티비티
	static public final int TP_TYPE_SEC_PART1_GANJI = 0;//Part1 간지
	static public final int TP_TYPE_SEC_PART1_SUMMARY = 1;//Part1 Summary
	static public final int TP_TYPE_SEC_WORD_UNDERSTAND_GANJI = 2;//단어이해간지
	static public final int TP_TYPE_SEC_WORD_UNDERSTAND = 3;//단어이해
	static public final int TP_TYPE_SEC_WORD_READ_GANJI = 4;//단어읽기간지
	static public final int TP_TYPE_SEC_WORD_READ = 5;//단어읽기
	static public final int TP_TYPE_SEC_WORD_TEST_GANJI = 6;//단어 테스트 간지
	static public final int TP_TYPE_SEC_WORD_TEST = 7;//단어테스트
	static public final int TP_TYPE_SEC_PART2_GANJI = 8;//PART2 간지
	static public final int TP_TYPE_SEC_PART2_SUMMARY = 9;//문장훈련 Summary
	static public final int TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI = 10;//문장이해 간지
	static public final int TP_TYPE_SEC_SENTENCE_UNDERSTAND = 11;//문장이해
	static public final int TP_TYPE_SEC_SENTENCE_READ_GANJI = 12;//문장읽기 간지
	static public final int TP_TYPE_SEC_SENTENCE_READ = 13;//문장읽기
	static public final int TP_TYPE_SEC_SENTENCE_VIP_GANJI = 14;//VIP훈련간지
	static public final int TP_TYPE_SEC_SENTENCE_VIP = 15;//VIP훈련
	static public final int TP_TYPE_SEC_SENTENCE_TEST_GANJI = 16;//문장TEST간지
	static public final int TP_TYPE_SEC_SENTENCE_TEST = 17;//문장TEST
	static public final int TP_TYPE_SEC_SENTENCE_ANSWER_GANJI = 18;//정답확인간지
	static public final int TP_TYPE_SEC_SENTENCE_PART1_RESULT = 19;//PART1결과화면
	static public final int TP_TYPE_SEC_SENTENCE_PART1_INCORRECT = 20;//단어/구 오답 정리
	static public final int TP_TYPE_SEC_SENTENCE_PART2_RESULT = 21;//PART2결과화면
	static public final int TP_TYPE_SEC_SENTENCE_PART2_INCORRECT = 22;//문장 오답정리
	static public final int TP_TYPE_SEC_SENTENCE_FINISH = 23;//완료화면
	//엑티비티 끝
	static public final int W1 = TP_TYPE_SEC_WORD_UNDERSTAND_GANJI;//단어이해간지
	static public final int W2 = TP_TYPE_SEC_WORD_READ_GANJI;//단어읽기간지
	static public final int W3 = TP_TYPE_SEC_WORD_TEST_GANJI;//단어 테스트 간지
	static public final int S1 = TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI;//문장이해 간지
	static public final int S2 = TP_TYPE_SEC_SENTENCE_READ_GANJI;//문장읽기 간지
	static public final int S3 = TP_TYPE_SEC_SENTENCE_VIP_GANJI;//VIP훈련간지
	static public final int S4 = TP_TYPE_SEC_SENTENCE_TEST_GANJI;///문장TEST간지
	static public final int N1 = TP_TYPE_SEC_SENTENCE_ANSWER_GANJI;////정답확인간지
	//엑티비티 끝
	static public final String STR_W1 = "W1";//단어이해간지
	static public final String STR_W2 = "W2";//단어읽기간지
	static public final String STR_W3 = "W3";//단어 테스트 간지
	static public final String STR_S1 = "S1";//문장이해 간지
	static public final String STR_S2 = "S2";//문장읽기 간지
	static public final String STR_S3 = "S3";//VIP훈련간지
	static public final String STR_S4 = "S4";///문장TEST간지
	static public final String STR_N1 = "N1";///오답정리
	static public final String STR_Z = "Z";///학습종료

	private int current_part = ServiceCommon.PART_GB_PART1;//part1,2 로 변경
	private int ret_code = 0;//통신상태
	private int mcode = 0;//회원번호
	private int user_no = 0;//사용자번호
	private int book_id = 0;//교재
	private int book_detail_id = 0;//차시
	private int class_second_id = 0;//클래스
	private int is_halt_study = 1;//중단 학습 유무 **/안씀 초기 인터페이스 설꼐시 만들어둔거
	private int is_class_count = 0;//학습횟수
	private String class_study_ymd = ""; //학습계획일
	// 중앙화관련
	public String forestCode = "";
	public String teacherCode = "";
	public String lmsStatus = "";

	private ArrayList<PartData> part1_list = new ArrayList<PartData>();//part1 문제
	private ArrayList<PartData> part2_list = new ArrayList<PartData>();//part2 문제
	private String mac_addr = "";//Mac주소
	private Queue<Integer> status_q = new LinkedList<Integer>();

	private int last_book_detail_id = 0;//마지막 학습차시 (2교시 수업으로 추가)
	private int total_count = 0;//이건나중에뭉러보자
	private boolean is_caption = true;////상하모드)

	private int vip_cnt = 0;//vip숫자

	private String step = "";//중도학습 단계
	@Getter(AccessLevel.NONE)
	private int quiz_seq = 0;//중도 학습 index

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private int[] all_status = {
			//TP_TYPE_SEC_PART1_GANJI, //	Part1 간지
			//TP_TYPE_SEC_PART1_SUMMARY, //	Part1 Summary
			TP_TYPE_SEC_WORD_UNDERSTAND_GANJI, //	단어이해간지
			TP_TYPE_SEC_WORD_UNDERSTAND, //	단어이해
			TP_TYPE_SEC_WORD_READ_GANJI, //	단어읽기간지
			TP_TYPE_SEC_WORD_READ, //	단어읽기
			TP_TYPE_SEC_WORD_TEST_GANJI, //	단어 테스트 간지
			TP_TYPE_SEC_WORD_TEST, //	단어테스트
			//TP_TYPE_SEC_PART2_GANJI, //	PART2 간지
			//TP_TYPE_SEC_PART2_SUMMARY, //	문장훈련 Summary
			TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI, //	문장이해 간지
			TP_TYPE_SEC_SENTENCE_UNDERSTAND, //	문장이해
			TP_TYPE_SEC_SENTENCE_READ_GANJI, //	문장읽기 간지
			TP_TYPE_SEC_SENTENCE_READ, //	문장읽기
			TP_TYPE_SEC_SENTENCE_VIP_GANJI, //	VIP훈련간지
			TP_TYPE_SEC_SENTENCE_VIP, //	VIP훈련
			TP_TYPE_SEC_SENTENCE_TEST_GANJI, //	문장TEST간지
			TP_TYPE_SEC_SENTENCE_TEST, //	문장TEST
			TP_TYPE_SEC_SENTENCE_ANSWER_GANJI, //	정답확인간지
			//TP_TYPE_SEC_SENTENCE_PART1_RESULT, //	PART1결과화면
			TP_TYPE_SEC_SENTENCE_PART1_INCORRECT, //	단어/구 오답 정리
			//TP_TYPE_SEC_SENTENCE_PART2_RESULT, //	PART2결과화면
			//TP_TYPE_SEC_SENTENCE_PART2_INCORRECT, //	문장 오답정리
			TP_TYPE_SEC_SENTENCE_FINISH //	완료화면
	};
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private int[] part2_status = {
			//TP_TYPE_SEC_PART2_GANJI, //	PART2 간지
			//TP_TYPE_SEC_PART2_SUMMARY, //	문장훈련 Summary
			TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI, //	문장이해 간지
			TP_TYPE_SEC_SENTENCE_UNDERSTAND, //	문장이해
			TP_TYPE_SEC_SENTENCE_READ_GANJI, //	문장읽기 간지
			TP_TYPE_SEC_SENTENCE_READ, //	문장읽기
			TP_TYPE_SEC_SENTENCE_VIP_GANJI, //	VIP훈련간지
			TP_TYPE_SEC_SENTENCE_VIP, //	VIP훈련
			TP_TYPE_SEC_SENTENCE_TEST_GANJI, //	문장TEST간지
			TP_TYPE_SEC_SENTENCE_TEST, //	문장TEST
			TP_TYPE_SEC_SENTENCE_ANSWER_GANJI, //	정답확인간지
			//TP_TYPE_SEC_SENTENCE_PART1_RESULT, //	PART1결과화면
			TP_TYPE_SEC_SENTENCE_PART1_INCORRECT, //	단어/구 오답 정리
			//TP_TYPE_SEC_SENTENCE_PART2_RESULT, //	PART2결과화면
			//TP_TYPE_SEC_SENTENCE_PART2_INCORRECT, //	문장 오답정리
			TP_TYPE_SEC_SENTENCE_FINISH //	완료화면
	};

	/**
	 * 학습단계 초기화
	 * 
	 * @param mStopLeaning
	 *            true:중단학습 false:처음학습
	 */
	public void init_Status_list(String step) {

		try {
			status_q.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.i("", "step => " + step);
		//step = "W3";
		if ("".equals(step)) {//처음학습일떄
			for (int status : all_status) {

				if (((status == TP_TYPE_SEC_SENTENCE_VIP || status == TP_TYPE_SEC_SENTENCE_VIP_GANJI) && vip_cnt == 0)) {
					continue;
				}
				status_q.offer(status);
			}
		} else {//중단학습일때
			status_q = stopStudyList(step);
		}
	}

	public static SecStudyData getInstance() {
		if (instance == null) {
			synchronized (SecStudyData.class) {
				instance = new SecStudyData();
			}
		}
		return instance;
	}

	public static SecStudyData setInstance(SecStudyData secStudyData) {
		instance = null;
		synchronized (SecStudyData.class) {
			instance = secStudyData;
		}
		return instance;

	}

	public void clear() {
		instance = null;
	}

	public ArrayList<PartData> getPartList() {
		if (current_part == ServiceCommon.PART_GB_PART1) {
			return part1_list;
		} else {
			return part2_list;
		}
	}

	/**
	 * 파트 read 숫자
	 * 
	 * @return
	 */
	public int getPartReadCnt() {
		if (current_part == ServiceCommon.PART_GB_PART1) {
			return part1_list.size();
		} else {
			return part2_list.size() - vip_cnt;
		}
	}

	/**
	 * 단어/문장/ 영어인거 뽑아오기
	 * 
	 * @param index
	 * @return
	 */
	public String getEngWordSentence(int index) {
		String wordSenetece;
		if (current_part == ServiceCommon.PART_GB_PART1) {//단어일때
			if (CommonUtil.isKorean(part1_list.get(index).getWord()) || CommonUtil.isNum(part1_list.get(index).getWord())) {//word가 한글일때
				wordSenetece = part1_list.get(index).getMeans();
			} else {
				wordSenetece = part1_list.get(index).getWord();
			}

		} else {//문장일때 문장은 일단
			if (CommonUtil.isKorean(part2_list.get(index).getSentence())) {//word가 한글일때
				wordSenetece = part2_list.get(index).getMeans();
			} else {
				wordSenetece = part2_list.get(index).getSentence();
			}
		}

		return wordSenetece;
	}

	/**
	 * 단어/문장/ 영어인거 뽑아오기
	 * 
	 * @param index
	 * @return
	 */
	public String getEngWordSentence(int index, int part, ArrayList<PartData> partList) {
		String wordSenetece;
		if (part == ServiceCommon.PART_GB_PART1) {//단어일때
			if (CommonUtil.isKorean(partList.get(index).getWord()) || CommonUtil.isNum(partList.get(index).getWord())) {//word가 한글일때
				wordSenetece = partList.get(index).getMeans();
			} else {
				wordSenetece = partList.get(index).getWord();
			}

		} else {//문장일때 문장은 일단
			if (CommonUtil.isKorean(partList.get(index).getSentence())) {//word가 한글일때
				wordSenetece = partList.get(index).getMeans();
			} else {
				wordSenetece = partList.get(index).getSentence();
			}
		}

		return wordSenetece;
	}

	/**
	 * 단어/문장/뜻 한글인거 뽑아오기
	 * 
	 * @param index
	 * @return
	 */
	public String getKorMeans(int index) {
		String korMeans;
		if (current_part == ServiceCommon.PART_GB_PART1) {//단어일때
			if (CommonUtil.isKorean(part1_list.get(index).getWord()) || CommonUtil.isNum(part1_list.get(index).getWord())) {//word가 한글일때
				korMeans = part1_list.get(index).getWord();
			} else {
				korMeans = part1_list.get(index).getMeans();
			}

		} else {//문장일때 문장은 일단
			if (CommonUtil.isKorean(part2_list.get(index).getSentence())) {//word가 한글일때
				korMeans = part2_list.get(index).getSentence();
			} else {
				korMeans = part2_list.get(index).getMeans();
			}
		}
		return korMeans;
	}

	/**
	 * 단어/문장/뜻 한글인거 뽑아오기
	 * 
	 * @param index
	 * @return
	 */
	public String getKorMeans(int index, int part, ArrayList<PartData> partList) {
		String korMeans;
		if (part == ServiceCommon.PART_GB_PART1) {//단어일때
			if (CommonUtil.isKorean(partList.get(index).getWord()) || CommonUtil.isNum(partList.get(index).getWord())) {//word가 한글일때
				korMeans = partList.get(index).getWord();
			} else {
				korMeans = partList.get(index).getMeans();
			}

		} else {//문장일때 문장은 일단
			if (CommonUtil.isKorean(partList.get(index).getSentence())) {//word가 한글일때
				korMeans = partList.get(index).getSentence();
			} else {
				korMeans = partList.get(index).getMeans();
			}
		}
		return korMeans;
	}

	/**
	 * 중단학습일때 q에 담을꺼 정리
	 * 
	 * @param step
	 * @return
	 */
	public Queue<Integer> stopStudyList(String step) {
		Queue<Integer> result = new LinkedList<Integer>();
		int int_step = 0;

		if (STR_W1.equals(step)) {
			int_step = W1;
		} else if (STR_W2.equals(step)) {
			int_step = W2;
		} else if (STR_W3.equals(step)) {
			int_step = W3;
		} else if (STR_S1.equals(step)) {
			int_step = S1;
		} else if (STR_S2.equals(step)) {
			int_step = S2;
		} else if (STR_S3.equals(step)) {
			int_step = S3;
		} else if (STR_S4.equals(step)) {
			int_step = S4;
		} else if (STR_N1.equals(step)) {
			int_step = N1;
		}

		for (int i = int_step; i < all_status.length; i++) {
			if (all_status[i] == TP_TYPE_SEC_SENTENCE_VIP_GANJI || all_status[i] == TP_TYPE_SEC_SENTENCE_VIP) {//vip는 없을때 제외
				if (vip_cnt != 0) {//vip 있을때
					result.add(all_status[i]);
				}
			} else {
				result.add(all_status[i]);
			}

		}
		return result;
	}

	/**
	 * test 결과저장
	 * 
	 * @param index
	 * @param result
	 */
	public void setTestResult(int index, String answer) {

		if (current_part == ServiceCommon.PART_GB_PART1) {
			part1_list.get(index).setAnswer(answer);
		} else {
			part2_list.get(index).setAnswer(answer);
		}

	}

	/**
	 * test 결과저장
	 * 
	 * @param index
	 * @param result
	 */
	public int getTestWorngCnt(int partNum) {

		ArrayList<PartData> list = null;
		int cnt = 0;

		if (partNum == ServiceCommon.PART_GB_PART1) {
			list = part1_list;
		} else if (partNum == ServiceCommon.PART_GB_PART2) {
			list = part2_list;
		}

		for (PartData data : list) {

			if (!data.is_result()) {
				cnt++;
			}
		}
		return cnt;
	}

	public synchronized int getQuiz_seq() {
		int tmp_seq = quiz_seq;
		if (tmp_seq != 0) {
			tmp_seq = tmp_seq - 1;//인덱스 0 부터라..
			quiz_seq = 0;
		}
		return tmp_seq;
	}

}
