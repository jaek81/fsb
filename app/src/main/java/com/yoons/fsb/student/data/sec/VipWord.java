package com.yoons.fsb.student.data.sec;

import android.content.Context;

import com.yoons.fsb.student.vanishing.layout.VanishingWordLayout;

import lombok.Data;

@Data
public class VipWord {

	private VanishingWordLayout wordLayout;//textview
	private int index = 0;//몇번째 사라지는건지 0이면 안사라짐
	private int lenLevel;
	private int small;
	private String txt;

	public VipWord(Context mContext, String txt, int index, int small, int lenLevel, boolean isNewLine) {
		wordLayout = new VanishingWordLayout(mContext, txt, small, lenLevel, isNewLine);

		this.index = index;
		this.lenLevel = lenLevel;
		this.small = small;
		this.txt = txt;
	}

}
