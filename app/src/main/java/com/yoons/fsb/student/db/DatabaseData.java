package com.yoons.fsb.student.db;

import com.yoons.fsb.student.data.StudyData.OneWeekData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * db에서 조회시 리턴해주는 Data class
 *
 * @author ejlee
 */
public class DatabaseData {
    // 로그인가능한 회원 정보
    public static class LoginCustomer {
        public int mCustomerNo = 0; // 회원 번호
        public String mUserId = ""; // 아이디
        public String mUserPassword = ""; // 비밀 번호
        public String mCustomerName = ""; // 이름
        public String mLastLogin = ""; // 마지막 로그인 시간
        public boolean mIsStudyGuide = true; // 학습 가이드 설정 여부
    }

    // 컨텐츠 테스트를 위한 컨텐츠 권 조회
    public static class ReviewStudyBook {
        public int mProductNo = 0; // 교재코드
        public int mSeriesNo = 0; // 시리즈 번호
        public String mSeriesName = ""; // 시리즈 이름
        public int mBookNo = 0; // 권 번호
        public ArrayList<ReviewStudyBookUnit> mUnitList = new ArrayList<ReviewStudyBookUnit>(); // 차시
        // 리스트

        public boolean isExpand;
    }

    // 컨텐츠 테스트를 위한 컨텐츠 유닛 조회
    public static class ReviewStudyBookUnit {
        public String mStudyUnitCode = ""; // 학습 단위 코드
        public int mStudyUnitNo = 0; // 학습순서(차시)
    }

    /**
     * 스마트 학습 음원을 듣기 위한 권 정보
     *
     * @author jhcho
     */
    public static class ReviewStudyAudio {
        public int mProductNo = 0; // 교재코드
        public int mSeriesNo = 0; // 시리즈 번호
        public String mSeriesName = ""; // 시리즈 이름
        public int mBookNo = 0; // 권 번호
        public int mRestudyCnt = 0;
        public ArrayList<ReviewStudyAudioUnit> mUnitList = new ArrayList<ReviewStudyAudioUnit>(); // 차시
        // 리스트
        public boolean isExpand; //리스트에서 펼쳐져 있는지 여부
    }

    /**
     * 스마트 학습 음원을 듣기 위한 컨텐츠 유닛
     *
     * @author jhcho
     */
    public static class ReviewStudyAudioUnit {
        public String mStudyUnitCode = ""; // 학습 단위 코드
        public int mStudyUnitNo = 0; // 학습순서(차시)

        public int mAudioPlayTime = 0;
        public int mReviewTime = 0;
        public int mUseTime = 0;
        public int mUseCnt = 0;
    }

    // 학습 데이터를 만들기 위한 class
    public static class StudyUnit {
        public String mStudyPlanYmd = ""; // 학습 계획일
        public int mProductNo = 0; // 교재코드
        public String mStudyUnitCode = ""; // 학습 단위 코드
        public int mStudyUnitNo = 0; // 학습순서(차시)
        public int mReStudyCnt = 0; // 재학습순서 (학습 계획에서 가져오며 db기록할때 사용함)
        public int mSeriesNo = 0; // 시리즈 번호
        public String mSeriesName = ""; // 시리즈 이름
        public int mBookNo = 0; // 권 번호
        public int mRealBookNo = 0; // // 화면상 표시되는 권 번호
        public boolean mIsCdnBFile = false; // 본문이 있는지 여부
        public boolean mIsCdnVideoFile = false; //복습 동영상 파일 존재 여부
        public boolean mIsCdnAudio = false; // 학습음원 존재 여부
        public boolean mIsCdnZip = false; // popup학습 존재 여부
        public boolean mIsMovie = false; // 문법 동영상 존재 여부
        //c추가
        public int mBookDetailId = 0; //
        public int mStudentBookId = 0; //
        public int mStudyResultNo = 0; //
        //교재구분(현재 팝퀴즈 구분용으로 추가)
        public int mProductType = 0; //

    }

    // 다운로드 받을 목록을 만들기 위한 차시 정보
    public static class DownloadStudyUnit {
        public int mProductNo = 0; // 품목 번호
        public int mSeriesNo = 0; // 시리즈 번호
        public int mBookNo = 0; // 권 번호
        public int mRealBookNo = 0; // 화면상 표시되는 권 번호
        public int mStudyUnitNo = 0; // 차시 번호
        public String mStudyUnitCode = ""; // 차시 코드
        public int mDictationCnt = 0; // 받아쓰기 개수
        public boolean mIsCdnBFile = false; // 본문 파일 존재 여부
        public boolean mIsCdnVideoFile = false; //복습 동영상 파일 존재 여부
        public boolean mIsCdnAudio = false; // 학습음원 존재 여부
        public boolean mIsCdnZip = false; // popup학습 존재 여부
        public ArrayList<String> mWordQuestionList = new ArrayList<String>(); // 단어

        public int mSentenceQuestionCnt = 0; // 문장 시험 개수
        public int mVanishingQuestionCnt = 0; // 문장 시험 개수
        public int mAniTalkCnt = 0; // 애니톡 개수
        public boolean mIsMovie = false; // 문법 동영상 존재 여부
        public boolean mIsPriority = false; // 다운로드 우선순위 여부
        public ArrayList<OneWeekData> mOneWeekList = null; // 일일/주간평가 음성개수
        public boolean mIsOlndAnswer = false; // 구교재 정답
    }

    // 오늘 학습에 대한 정보
    public static class TodayStudyUnit extends StudyUnit {
        public String mStudyStatus = ""; // 학습 진행 상태
        public String mBookCheckDt = ""; // 중단학습시 중단된 오디오 학습시간을 기록함. 중단학습이 아닐시에는
        // 다시 확인이 필요함.
        public String mRecordFileYmd = ""; // 녹음 파일 일시
        public String mReviewStudyStatus = ""; // 복습학습 진행 상태
        public boolean mIsOlndAnswer = false; // 구교재 정답
        public boolean mIsNewDictation = false; // 구교재 정답

        public TodayStudyUnit() {

        }

        public TodayStudyUnit(JSONObject jsonObject) {
            try {
                if (!jsonObject.isNull("product_no")) {
                    mProductNo = jsonObject.getInt("product_no");
                }
                if (!jsonObject.isNull("series_name")) {
                    mSeriesName = jsonObject.getString("series_name").replace("u0027", "'");
                }
                if (!jsonObject.isNull("series_no")) {
                    mSeriesNo = jsonObject.getInt("series_no");
                }
                if (!jsonObject.isNull("studyunitcode")) {
                    mStudyUnitCode = jsonObject.getString("studyunitcode");
                }
                if (!jsonObject.isNull("item_no")) {
                    mBookNo = jsonObject.getInt("item_no");
                }
                if (!jsonObject.isNull("item_no")) {
                    mRealBookNo = jsonObject.getInt("item_no");
                }
                if (!jsonObject.isNull("study_order")) {
                    mStudyUnitNo = jsonObject.getInt("study_order");
                }
                if (!jsonObject.isNull("study_status")) {
                    mStudyStatus = jsonObject.getString("study_status");
                }
                if (!jsonObject.isNull("restudycnt")) {
                    mReStudyCnt = jsonObject.getInt("restudycnt");
                }
                if (!jsonObject.isNull("studyplanymd")) {
                    mStudyPlanYmd = jsonObject.getString("studyplanymd");
                }
                if (!jsonObject.isNull("is_cdn_b_file")) {
                    mIsCdnBFile = (jsonObject.getInt("is_cdn_b_file") == 1) ? true : false;
                }
                if (!jsonObject.isNull("movie_cnt")) {
                    mIsMovie = (jsonObject.getInt("movie_cnt") == 1) ? true : false;
                }
                if (!jsonObject.isNull("book_check_dt")) {
                    mBookCheckDt = jsonObject.getString("book_check_dt");
                }
                if (!jsonObject.isNull("record_file_ymd")) {
                    mRecordFileYmd = jsonObject.getString("record_file_ymd");
                }
                if (!jsonObject.isNull("restudy_movie_cnt")) {
                    mIsCdnVideoFile = (jsonObject.getInt("restudy_movie_cnt") == 1) ? true : false;
                }
                if (!jsonObject.isNull("is_cdn_audio_file")) {
                    mIsCdnAudio = (jsonObject.getInt("is_cdn_audio_file") == 1) ? true : false;
                }
                if (!jsonObject.isNull("is_exam_zip")) {
                    mIsCdnZip = (jsonObject.getInt("is_exam_zip") == 1) ? true : false;
                }
                if (!jsonObject.isNull("bookdetailid")) {
                    mBookDetailId = jsonObject.getInt("bookdetailid");
                }
                if (!jsonObject.isNull("studentofbookid")) {
                    mStudentBookId = jsonObject.getInt("studentofbookid");
                }
                if (!jsonObject.isNull("study_result_no")) {
                    mStudyResultNo = jsonObject.getInt("study_result_no");
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        //숲우영
        //		public int mAgencyCode = 0; 		//학습 센터 코드 (jhcho )
        //		public int mClassNo = 0;			//숲SB 연동
        //		public int mSiteKind = 0;			//Site 종류
    }

    // 학습 결과에서 삭제 없을 수도 있는 차시 때문에
    // 해당 product의 최대 차시를 구해놓음.
    public static class StudyResultUnitMax {
        public int mProductNo = 0; // 품목 번호
        public int mStudyUnitNo = 0; // 차시 번호
    }

    // 주간 달력 데이터를 만들기 위한 class
    public static class WeekCalendarUnit {
        public String mDate = "";      	 	// 날짜
        public String mDisplayText = "";	// 요일 + 날짜 표시
        public int mWeekDay = 0;    	   	// 요일 일요일 :1, 월요일 :2 ... 토요일 :7
        public int mStudyUnitCnt = 0;		// 학습 완료한 차시 수
        public int mWeekStudyCnt = 0;		// 학습 해야할 차시 수
    }

    // 학습 옵션
    public static class StudyOption {
        // sbm_customer table에서 조회
        public boolean mIsAutoArtt = false; // 오디오 학습 자동 artt (사용자 정보에 내려옴)
        public boolean mIsCaption = false; // 학습모드( 값은 다시 확인, 사용자 정보에 내려옴)
        public boolean mIsStudyGuide = true; // 옵션에 설정되어 있음.
        public boolean mIsFastRewind = true; // 빨리 감기 여부
        public boolean mIsDictation = true; // 받아쓰기 여부
        public int mMiddleRepeatCnt = 1; // 중등 반복 횟수(사용하지 않음)
        public int mElementarytRepeatCnt = 3; // 초등 반복횟수(사용하지 않음)
        public boolean mIsReviewBook = true; // (복습)본문 듣기 할것인지 여부
        public int mRepeatCnt = 1; // R1, R2반복 횟수

        // sbm_customer_config table에서 조회
        public boolean isRecordUpload = false;
        public boolean isParagraph = true;
        public boolean isVIPSkip = true;
        public boolean isTextKorean = false;

        //추가 딕테이션
        public boolean mIsDicPirvate = true; // 딕테이션 실행여부
        public boolean mIsDicAnswer = true; // 딕테이션_정답 노출여부
    }

    // 다운로드할 컨텐츠 파일 정보
    public static class DownloadContent {
        private static final String TAG = "DownloadContent";

        public static class UnitFile {
            public int mDownloadId = 0; // 다운로드 ID sequence
            public String mDownloadUrl = ""; // 다운로드 받을 파일 URL
            public String mSavePath = ""; // 로컬 저장 패스

            public UnitFile(int downloadId, String downloadUrl, String savePath) {
                mDownloadId = downloadId;
                mDownloadUrl = downloadUrl;
                mSavePath = savePath;
            }
        }

        ;

        public boolean mPriority = false; // 우선 순위 여부
        public int mStudyUnitId = 0; // 차시 id
        public ArrayList<UnitFile> mUnitFileList = new ArrayList<UnitFile>();

        public DownloadContent(int studyUnitId, boolean priority) {
            mStudyUnitId = studyUnitId;
            mPriority = priority;
        }

        public void addStudyUnit(UnitFile unitFile) {
            mUnitFileList.add(unitFile);
        }

        public int getStudyUnitId() {
            return mStudyUnitId;
        }

		/*
         * public void logFieldValue() { Field[] fields =
		 * getClass().getFields(); for(int i = 0; i < fields.length; i++) {
		 * Field field = fields[i]; try { Log.e(TAG, field.getName() + " : " +
		 * field.get(this).toString()); } catch (IllegalArgumentException e) {
		 * e.printStackTrace(); } catch (IllegalAccessException e) {
		 * e.printStackTrace(); } } }
		 */
    }
}
