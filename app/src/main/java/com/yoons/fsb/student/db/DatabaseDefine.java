package com.yoons.fsb.student.db;


/**
 * db Define 클래스
 * 각종 create 및 긴 Query를 가지고 있음.
 * @author ejlee
 */
public class DatabaseDefine {
	// column과 value를 저장하기 위한 클래스
	public static class ColValue {
		String mColumn = "";	// 컬럼 이름
		String mValue = "";		// 컬럼 값
	};
	
	// 동기화 시간을 기록하는 테이블
	public static final String CREATE_SYNC_INFO = "CREATE TABLE IF NOT EXISTS sync_info "
			+ " (no INTEGER, last_sync_dt TEXT, UNIQUE(no)) ";
	
	// 로컬 쿼리를 저장하는 테이블(사용하지 않음)
	public static final String CREATE_SBM_LOCALQUERY = "CREATE TABLE IF NOT EXISTS sbm_localquery "
			+ " (func_name TEXT, "			// 교사 번호
			+ " seq_no INTEGER, "			// 고객 번호
			+ " query TEXT, "				// 요일
			+ " argument TEXT, "			// 예약시간(0:없음, HH24MI)
			+ " last_update_dt TEXT, "		// 최종수정일시
			+ " UNIQUE(func_name, seq_no)) ";
			
	// 사용자 테이블
	public static final String CREATE_SBM_CUSTOMER = "CREATE TABLE IF NOT EXISTS sbm_customer "
			+ " (customer_no INTEGER PRIMARY KEY, "		// 고객 번호 
			+ " udid TEXT, " 				// 디바이스 ID
			+ " gcm_id TEXT, "				// google registraion ID
			+ " os_version TEXT, "			// os버전
			+ " device_model TEXT, "		// 디바이스 모델명
			+ " is_only_wifi INTEGER, "		// 이동통신망 사용 유무
			+ " status INTEGER, "			// 상태(0 : 미사용, 1 : 사용)
			+ " study_start_dt TEXT, "		// 학습 시작일시
			+ " is_studyguide INTEGER DEFAULT 1, "	// 학습 가이드 유무
			+ " is_auto_artt INTEGER, "		// 자동 artt유무
			+ " is_caption INTEGER, "		// 학습모드(자막) 
			+ " is_fast_rewind INTEGER, "	// 빨리감기 유무
			+ " is_review_book INTEGER DEFAULT 1, " // 이전학습 복습 본문 듣기 여부
			+ " repeat_cnt INTEGER DEFAULT 1, " // R1, R2 반복 횟수
			+ " user_id TEXT, "				// 아이디
			+ " user_password TEXT, "		// 비밀번호
			+ " customer_name TEXT, "		// 고객명
			+ " sex INTEGER, "				// 성별
			+ " school TEXT, "				// 학교
			+ " school_year INTEGER, "		// 학년
			+ " zip_code TEXT, "			// 우편번호
			+ " sido TEXT, "				// 시/도
			+ " gugun TEXT, "				// 구/군
			+ " dong TEXT, "				// 동
			+ " remain_address TEXT, "		// 기타주소
			+ " phone TEXT, "				// 전화번호
			+ " mobile_phone TEXT, "		// 핸드폰번호
			+ " email TEXT, "				// 이메일
			+ " en_nick_name TEXT, "		// 영어별명
			+ " real_birthday TEXT, "		// 실제생년월일
			+ " birth_ymd TEXT, "			// 생년월일
			+ " agency_no INTEGER, "		// 교육센터번호
			+ " agency_name TEXT," 			// 교육센터명
			+ " is_dictation TEXT, "		// 받아쓰기 실행 여부
			+ " is_dic_private TEXT, "		// 숲/우영 받아쓰기 실행여부
			+ " is_dic_answer TEXT, "		// 숲/우영 받아쓰기 정답여부
			+ " one_type INTEGER, "			// 중등 반복 횟수(사용하지 않음)
			+ " three_type INTEGER, "		// 초등 반복 횟수(사용하지 않음)
			+ " last_login_dt TEXT) ";

	// 페이지 테이블
	public static final String CREATE_SBM_PAGE = "CREATE TABLE IF NOT EXISTS sbm_page "
			+ " (series_no INTEGER, "		// 시리즈번호
			+ " real_book_no INTEGER, "		// 실제권번호
			+ " study_order INTEGER, "		// 학습순서
			+ " type TEXT, "				// 페이지유형
			+ " page_order INTEGER, "		// 페이지순서
			+ " page_no INTEGER, "			// 페이지번호
			+ " is_used INTEGER, "			// 사용유무
			+ " text TEXT, "				// 페이지문구
			+ " last_update_dt TEXT, " 	// 마지막 업데이트 날짜.
			+ " UNIQUE(type, real_book_no, page_order, series_no, study_order)) ";

	// 회원 설정 테이블
	public static final String CREATE_SBM_CUSTOMER_CONFIG = "CREATE TABLE IF NOT EXISTS sbm_customer_config "
			+ " (customer_no INTEGER, "		// 고객번호
			+ " conf_name TEXT, "			// config 이름
			+ " conf_value INTEGER, "		// config 값
			+ " UNIQUE(customer_no, conf_name)) ";
	
	// 교사가 설정한 학습 시간 정보 테이블
	public static final String CREATE_SBM_STUDY_DAY_CONFIG = "CREATE TABLE IF NOT EXISTS sbm_study_day_config "
			+ " (teacher_no INTEGER, "		// 교사번호
			+ " customer_no INTEGER, "		// 고객번호
			+ " weekday INTEGER, "			// 요일(1:일요일, 2:월요일 ....)
			+ " reserve_time TEXT, "		// 예약시간(0:없음, HH24MI)
			+ " last_update_dt TEXT, "		// 최종 수정일시
			+ " UNIQUE(teacher_no, customer_no, weekday)) ";
	
	//음원다시듣기 스마트 학습 음원듣기 결과 테이블
	public static final String CREATE_SBM_STUDY_RESULT_REVIEW = "CREATE TABLE IF NOT EXISTS sbm_study_result_review  " +
			"(customer_no INTEGER,  " +		// 고객번호
			"product_no INTEGER,  " +		// 교재코드
			"study_unit_code TEXT,  " +		// 학습단위코드
			"re_study_cnt INTEGER,  " +		// 재학습순서
			"audio_play_time INTEGER,  " +	// 학습 재생시간
			"review_time INTEGER,  " +		// review 가능한 시간
			"use_time INTEGER,  " +			// User review 재생시간
			"use_cnt INTEGER,  " +			// User review 횟수
			"UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt));";
			
	// 학습 결과 테이블
	public static final String CREATE_SBM_STUDY_RESULT = "CREATE TABLE IF NOT EXISTS sbm_study_result "
			+ " (customer_no INTEGER, "					// 고객번호
			+ " product_no INTEGER, "					// 교재코드
			+ " study_unit_code TEXT, "					// 학습단위코드
			+ " re_study_cnt INTEGER, "					// 재학습순서
			+ " study_result_no INTEGER, "				// 학습결과코드
			+ " review_study_result_no INTEGER, "		// 지난학습결과코드
			+ " last_update_dt TEXT, "					// 최종수정일시
			+ " study_status TEXT, "					// 본학습상태코드
			+ " book_start_dt TEXT, "					// 본학습본문시작일시
			+ " book_end_dt TEXT, "						// 본학습본문종료일시
			+ " word_exam_start_dt TEXT, "				// 본학습단어시험시작일시
			+ " word_exam_end_dt TEXT, "				// 본학습단어시험종료일시
			+ " word_cnt INTEGER, "						// 본학습단어시험전체개수
			+ " word_incorrect_cnt INTEGER, "			// 본학습단어시험오답수
			+ " is_voice_recoginition INTEGER, "		// 본학습 음성인식 유무
			+ " sentence_start_dt TEXT, "				// 본학습문장시험시작일시
			+ " sentence_end_dt TEXT, "					// 본학습문장시험종료일시
			+ " sentence_cnt INTEGER, "					// 본학습문장시험전체개수
			+ " sentence_incorrect_cnt INTEGER, "		// 본학습문장시험오답수
			+ " paragraph_start_dt TEXT, "				// 본학습문장시험시작일시
			+ " paragraph_end_dt TEXT, "				// 본학습문장시험종료일시
			+ " dictation_start_dt TEXT, "				// 본학습받아쓰기시작시간
			+ " dictation_end_dt TEXT, "				// 본학습받아쓰기종료시간
			+ " study_finish_dt TEXT, "					// 본학습완료일시
			+ " review_status TEXT, "					// 복습학습상태코드
			+ " review_book_start_dt TEXT, "			// 복습본문시작일시
			+ " review_book_end_dt TEXT, "				// 복습본문종료일시
			+ " review_word_exam_start_dt TEXT, "		// 복습단어시험시작일시
			+ " review_word_exam_end_dt TEXT, "			// 복습단어시험종료일시
			+ " review_word_cnt INTEGER, "				// 복습단어시험전체개수
			+ " review_word_incorrect_cnt INTEGER, "	// 복습단어시험오답개수
			+ " is_review_voice_recoginition INTEGER, "	// 복습음성인식유무
			+ " review_sentence_start_dt TEXT, "		// 복습문장시험시작일시
			+ " review_sentence_end_dt TEXT, "			// 복습문장시험종료일시
			+ " review_sentence_cnt INTEGER, "			// 복습문장시험전체개수
			+ " review_sentence_incorrect_cnt INTEGER, "// 복습문장시험오답개수
			+ " review_finish_dt TEXT, "				// 복습완료일시
			+ " exam_ready_start_dt TEXT, "				// 시험시작시간
			+ " exam_ready_end_dt TEXT, "				// 시험종료시간
			+ " book_check_status_end INTEGER, "		// 본학습본문학습상태최종 0: 정상 10: 빠름, 11: 너무빠름, 20: 느림, 21 : 너무 느림
			+ " is_auto_artt INTEGER, "					// 자동ARTT유무
			+ " is_caption INTEGER, "					// 학습모드(자막)
			+ " book_audio_totalmin INTEGER, "			// 오디오 시간(분)
			+ " review_product_no INTEGER, "			// 복습 교재코드
			+ " review_study_unit_code TEXT, "			// 복습 학습단위코드
			+ " review_re_study_cnt INTEGER, "			// 복습 재학습순서
			+ " record_file_ymd TEXT, "					// 음성 녹음 파일 ymd
			+ " book_check_dt TEXT, "					// 본학습본문체크일시 (오디오 학습의 경우 10초마다 시간을 기록함)
			+ " study_movie_start_dt TEXT, "			// 본학습문법동영상시작시간		
			+ " study_movie_end_dt TEXT, "				// 본학습문법동영상종료시간
//숲우영
//			+ " agency_no INTEGER, "					// studyplan에 있는 acode 데이터 전달용 jhcho
//			+ " class_no INTEGER, "						// 숲SB 연동용
//			+ " site_kind INTEGER, "					// 사이트 종료
			+ " crud TEXT, "
			+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt)) ";
			
	// 단어시험 결과 테이블
	public static final String CREATE_SBM_STUDY_WORD_DETAIL = "CREATE TABLE IF NOT EXISTS sbm_study_word_detail "
			+ " (customer_no INTEGER, "			// 고객번호
			+ " product_no INTEGER, "			// 교재코드
			+ " study_unit_code TEXT, "			// 학습단위코드
			+ " re_study_cnt INTEGER, "			// 재학습순서
			+ " study_kind INTEGER, "			// 학습구분(1:본학습, 2:복습)
			+ " study_order INTEGER, "			// 순서
			+ " is_correct INTEGER, "			// 채점결과(1:맞음, 0:틀림)
			+ " correct_answer TEXT, "			// 정답
			+ " customer_answer TEXT, "			// 회원입력단어
			+ " last_update_dt TEXT, "			// 최종수정일시
			+ " word_question_no INTEGER, "		// 단어코드
//숲우영
//			+ " class_no INTEGER, "				// 숲SB 연동용
			+ " crud TEXT, "
			+ " study_result_no INTEGER, "
			+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt, study_kind, study_order)) ";
	
	// 문장시험 결과 테이블
	public static final String CREATE_SBM_STUDY_SENTENCE_DETAIL = "CREATE TABLE IF NOT EXISTS sbm_study_sentence_detail "
			+ " (customer_no INTEGER, "			// 고객번호
			+ " product_no INTEGER, "			// 교재코드
			+ " study_unit_code TEXT, "			// 학습단위코드
			+ " re_study_cnt INTEGER, "			// 재학습순서
			+ " study_kind INTEGER, "			// 학습구분(1:본학습, 2:복습)
			+ " study_order INTEGER, "			// 순서
			+ " is_correct INTEGER, "			// 채점결과
			+ " correct_answer TEXT, "			// 정답
			+ " customer_answer TEXT, "			// 회원인식문장
			+ " is_upload INTEGER, "			// 업로드유무
			+ " sentence_question_no INTEGER, "	// 문장코드
			+ " last_update_dt TEXT, "			// 최종수정일시
//숲우영
//			+ " class_no INTEGER, "				// 숲SB 연동용
			+ " crud TEXT, "
			+ " study_result_no INTEGER, "
			+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt, study_kind, study_order)) ";
	
	// 문단시험 결과 테이블
		public static final String CREATE_SBM_STUDY_VANISHING_DETAIL = "CREATE TABLE IF NOT EXISTS sbm_study_pa_sentence_detail "
				+ " (customer_no INTEGER, "			// 고객번호
				+ " product_no INTEGER, "			// 교재코드
				+ " study_unit_code TEXT, "			// 학습단위코드
				+ " re_study_cnt INTEGER, "			// 재학습순서
				+ " study_kind INTEGER, "			// 학습구분(1:본학습, 2:복습)
				+ " study_order INTEGER, "			// 순서
				+ " is_correct INTEGER, "			// 채점결과
				+ " correct_answer TEXT, "			// 정답
				+ " customer_answer TEXT, "			// 회원인식문장
				+ " is_upload INTEGER, "			// 업로드유무
				+ " retry_cnt INTEGER, "				// 재시도유무
				+ " sentence_question_no INTEGER, "// 문장코드
				+ " last_update_dt TEXT, "			// 최종수정일시
				+ " crud TEXT, "
				+ " study_result_no INTEGER, "
				+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt, study_kind, study_order)) ";
	
	// 신규문항
	// popup 시험 결과 테이블
	public static final String CREATE_SBM_STUDY_EXAM_DETAIL = "CREATE TABLE IF NOT EXISTS sbm_study_exam_detail "
			+ " (customer_no INTEGER, "			// 고객번호
			+ " product_no INTEGER, "			// 교재코드
			+ " study_unit_code TEXT, "			// 학습단위코드
			+ " re_study_cnt INTEGER, "			// 재학습순서
			+ " study_kind INTEGER, "			// 학습구분(1:본학습, 2:복습)
			+ " study_order INTEGER, "			// 순서
			+ " tag_type INTEGER, "				// 태그유형 태그유형(1-pop(선다형), 2-pop(드래그&드랍), 3-pop(교재집중형), 4-T&P(터치&플레이), 5-R&P(롤&플레이),6-pop(OX퀴즈))
			+ " is_correct INTEGER, "			// 채점결과(1:맞음, 0:틀림)
			+ " is_upload INTEGER, "			// 업로드유무(1:정상완료, 0:미완료)
			+ " last_update_dt TEXT, "			// 최종수정일시
			+ " question_order TEXT, "			// 문제 순서
			+ " question_no TEXT, "				// 솔루션 활동코드
			+ " customer_answer TEXT, "			// 회원인식문장
			+ " crud TEXT, "
			+ " study_result_no INTEGER, "
			+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt, study_kind, study_order, question_no, question_order)) ";

	// 오디오 음원 파일 테이블
	public static final String CREATE_SBM_STUDY_RECORD_DETAIL = "CREATE TABLE IF NOT EXISTS sbm_study_record_detail "
			+ " (customer_no INTEGER, "			// 고객번호
			+ " product_no INTEGER, "			// 교재코드
			+ " study_result_no INTEGER, "				// 클래스id
			+ " book_detail_id INTEGER, "		//차시
			+ " student_book_id INTEGER, "		//회원-교재 ID
			+ " study_unit_code TEXT, "			// 학습단위코드
			+ " re_study_cnt INTEGER, "			// 재학습순서
			+ " study_kind INTEGER, "			// 학습구분(1:본학습, 2:복습)
			+ " study_order INTEGER, "			// 순서
			+ " rec_type TEXT, "				// 녹음 타입
			+ " start_tag_name TEXT, "			// 태그 이름
			+ " start_tag_time TEXT, "			// 태그 시간
			+ " end_tag_time TEXT, "			// 태그 시간
			+ " start_merge_time TEXT, "		// 병합 첫시간
			+ " end_merge_time TEXT, "			// 병합 마지막시간
			+ " rec_len TEXT, "					// 녹음시간
			+ " is_upload INTEGER, "			// 업로드 여부
			+ " is_merge INTEGER, "				// 병합 여부(0:false 1:true)
			+ " rep_cnt INTEGER, "				// 반복횟수
			+ " crud TEXT, "					// 수정 여부
			+ " file_path TEXT, "				// 파일 경로
			+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt, study_kind, study_order)) ";
	
	// 오디오 학습 warning 테이블
	public static final String CREATE_SBM_STUDY_BOOK_WARNING = "CREATE TABLE IF NOT EXISTS sbm_study_book_warning "
			+ " (customer_no INTEGER, "			// 고객번호
			+ " product_no INTEGER, "			// 교재코드
			+ " study_unit_code TEXT, "			// 학습단위코드
			+ " re_study_cnt INTEGER, "			// 재학습순서
			+ " book_check_status INTEGER, "	// warning 종류
			+ " book_check_dt TEXT, "			// 체크일시
			+ " crud TEXT, "					// 수정 여부
			+ " study_result_no INTEGER, "
			+ " UNIQUE(customer_no, product_no, study_unit_code, re_study_cnt, book_check_status)) ";
	
	// 교재 Unit 테이블
	public static final String CREATE_WYE_BEFLY_STUDY_UNIT = "CREATE TABLE IF NOT EXISTS wye_befly_study_unit "
			+ " (product_no INTEGER, "				// 교재코드
			+ " study_unit_code TEXT, " 			// 다시확인 학습단위코드
			+ " study_order INTEGER, "				// 학습순서
			+ " text_start_page INTEGER, "			// 시작페이지
			+ " text_end_page INTEGER, "			// 끝페이지
			+ " seriesstudy_goal TEXT, "			// 시리즈학습목표
			+ " is_end_of_book INTEGER, "			// 권말여부
			+ " is_divided_book INTEGER, "			// 분권여부
			+ " real_book_no INTEGER, "				// 실제권번호
			+ " item_no INTEGER, "					// sb 권 번호
			+ " subtext_start_page INTEGER, "		// 부속교재시작페이지
			+ " subtext_end_page INTEGER, "			// 부속교재끝페이지
			+ " last_update_dt TEXT, "				// 최종수정일시
			+ " study_goal TEXT, "					// 학습목표
			+ " befl_note_start_page INTEGER, "		// 베플노트 시작 페이지
			+ " product_name TEXT, "				// 교재명
			+ " product_kind INTEGER, "				// 교재구분
			+ " product_type INTEGER, "				// 제품유형
			+ " audio_constitution INTEGER, "		// 오디오매체구성
			+ " book_constitution_start INTEGER, "	// 책구성시작권
			+ " book_constitution_last INTEGER, "	// 책구성마지막권
			+ " is_study_plan INTEGER, "			// 학습계획생성교재(0:생성불가, 1:가능)
			+ " is_step_eval INTEGER, "				// 단계별평가대상교재(0:비대상, 1:대상)
			+ " finish_product_kind INTEGER, "		// 완제품구분(1:완제품, 2:패키지, 3:패키지부속완성품)
			+ " series_name TEXT, "					// 시리즈명
			+ " series_no INTEGER, "				// 시리즈번호
			+ " series_contraction TEXT, "			// 시리즈약호
			+ " school_year_grade INTEGER, "		// 시리즈학년등급
			+ " kind INTEGER, "						// 시리즈구분(0,10,20,70:베플드릴즈)
			+ " total_book_no INTEGER, "			// 시리즈전체책권수
			+ " repeat_count TEXT, "				// NB녹음반복횟수(1:중등, 3:초등) // 사용하지 않음.
			+ " dictation_cnt INTEGER, "			// 받아쓰기문항수
			+ " is_cdn_b_file INTEGER, "			// 본문(b)파일이 존재 유무
			+ " debate_cnt INTEGER, "				// 애니톡 동영상 개수
			+ " movie_cnt INTEGER, "				// 문법 동영상 개수(boolean값이며 기본적으로 1개)
			+ " restudy_movie_cnt INTEGER, "		// 복습 동영상 개수 
			// 신규문항
			+ " is_cdn_audio_file INTEGER, "		// 학습 오디오 파일 존재 유무
			+ " is_exam_zip INTEGER, "				// popup 학습 zip 파일 존재 유무
			+ " is_new_dictation INTEGER, "		    // 딕테이션여부
			+ " UNIQUE(product_no, study_unit_code)) ";
	
	// 단어시험 정보 테이블 
	public static final String CREATE_YFS_SM_WORD_QUESTION = "CREATE TABLE IF NOT EXISTS yfs_sm_word_question "
			+ " (sm_word_question_no INGEGER PRIMARY KEY, "	//단어시험번호
			+ " question_order INTEGER, "	// 문제순번
			+ " question TEXT, "			// 문제
			+ " meaning TEXT, "				// 뜻
			+ " answer TEXT, "				// 정답
			+ " distractor_1 TEXT, "		// 보기1
			+ " distractor_2 TEXT, "		// 보기2
			+ " distractor_3 TEXT, "		// 보기3
			+ " distractor_4 TEXT, "		// 보기4
			+ " product_no INTEGER, "		// 교재번호
			+ " study_unit_code TEXT, "		// 학습단위코드
			+ " last_update_dt TEXT) "; 	// 마지막 업데이트 날짜.
	
	// 문장시험 정보 테이블
	public static final String CREATE_YFS_SM_SENTENCE_QUESTION = "CREATE TABLE IF NOT EXISTS yfs_sm_sentence_question "
			+ " (sm_sentence_question_no INTEGER PRIMARY KEY, "	// 문장시험번호
			+ " question_order INTEGER, "	// 문제순번
			+ " sentence TEXT, "			// 문장
			+ " text_sentence TEXT, "		// 음성인식문장
			+ " product_no INTEGER, "		// 교재코드
			+ " study_unit_code TEXT," 		// 학습단위코드
			+ " last_update_dt TEXT) ";		// 마지막 업데이트 날짜
	
	// 문장시험 정보 테이블
		public static final String CREATE_YFS_SM_VANISHING_QUESTION = "CREATE TABLE IF NOT EXISTS yfs_sm_pa_sentence_question "
				+ " (sm_pa_sentence_question_no INTEGER PRIMARY KEY, "	// 문장시험번호
				+ " question_order INTEGER, "	// 문제순번
				+ " sentence TEXT, "			// 문장
				+ " text_sentence TEXT, "		// 음성인식문장
				+ " product_no INTEGER, "		// 교재코드
				+ " study_unit_code TEXT," 		// 학습단위코드
				+ " last_update_dt TEXT) ";		// 마지막 업데이트 날짜
	
	// 복습 관련 테이블(사용하지 않음)
	public static final String CREATE_SBM_STUDY_PAST_HISTORY = "CREATE TABLE IF NOT EXISTS sbm_study_past_history "
			+ " (customer_no INTEGER, "	// 고객번호
			+ " product_no INTEGER, "	// 교재코드
			+ " study_unit_code TEXT, "	// 학습단위코드
			+ " past_start_dt TEXT, "	// 복습시작시간
			+ " past_end_dt TEXT, "		// 복습완료시간
			+ " last_update_dt TEXT, "	// 최종수정일시
			+ " UNIQUE(customer_no, product_no, study_unit_code, past_start_dt)) ";
	
	// 테이블 sync와 관련된 테이블(사용하지 않음)
	public static final String CREATE_SBM_SYNC_TABLES = "CREATE TABLE IF NOT EXISTS sbm_sync_tables "
			+ " (table_name TEXT PRIMARY KEY, "	// 테이블명
			+ " sync_kind INTEGER, "	// 동기화종류
			+ " maxdt_column TEXT, "	// 마지막데이터컬럼
			+ " term INTEGER, "			// 동기화기간
			+ " key_column TEXT, "		// 키컬럼
			+ " description TEXT, "		// 설명(메시지)
			+ " last_update_dt TEXT) ";	// 최종수정일시
	
	// 학습 계획 테이블
	public static final String CREATE_TB_STUDYPLAN = "CREATE TABLE IF NOT EXISTS tb_studyplan "
			+ " (mcode INTEGER, "		// 회원코드
			+ " studyplanymd TEXT, "	// 학습계획년월일
			+ " itemcode INTEGER, "		// 교재코드
			+ " studyunitcode TEXT, "	// 학습단위코드
			+ " restudycnt INTEGER, "	// 재학습차수
			+ " endofbook INTEGER, "	// 권말여부
			+ " teststate TEXT, "		// 학습진행상태코드
			+ " studyconfirm TEXT, "	// 학습확인방법
			+ " acode INTEGER, "		// 교육센터코드
			+ " tcode INTEGER, "		// 교사코드
			+ " telmanymd TEXT, "		// 전화관리년월일
			+ " studyexamyn INTEGER, "	// 평가Tape선택여부
			+ " procdate TEXT, "		// 처리일시
			+ " st_code INTEGER, "		// 
			+ " student_book_id INTEGER, "		// 
			+ " book_detail_id INTEGER, "		// 
//숲우영
//			+ " class_no INTEGER, "		// 숲SB 연동용
//			+ " site_kind INTEGER, "		// 숲SB 연동용
			+ " UNIQUE(mcode, studyplanymd, itemcode, studyunitcode, restudycnt)) ";
	
	// 다운로드할 컨텐츠 관리 테이블
	public static final String CREATE_DOWNLOAD_CONTENT = "CREATE TABLE IF NOT EXISTS download_content "
			+ " (download_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, "	// 다운로드 id
			+ " study_unit_id INTEGER, "	// 학습단위코드
			+ " filename TEXT, "			// 파일 이름. // unique로 download_url로 걸기에는 너무 길어 사용.
			+ " download_url TEXT, "		// 다운로드 url
			+ " save_path TEXT, "			// 단말저장패스
			+ " priority INTEGER, "			// 우선순위 여부(0:우선순위 아님, 1:우선순위임)
			+ " UNIQUE(study_unit_id, filename)) ";
	
	// 네트워크 사용량 테이블
	public static final String CREATE_SBM_NETWORK_USED = "CREATE TABLE IF NOT EXISTS sbm_network_used "
			+ " (udid TEXT, "			// 디바이스ID
			+ " ymd TEXT, "				// 년월일
			+ " network_kind TEXT, "	// 네트워크 종류
			+ " used INTEGER) ";		// 사용량

	// 학습 로그 테이블
	public static final String CREATE_SBM_FAIL_STUDY_LOG = "CREATE TABLE IF NOT EXISTS sbm_study_log "
			+ " (url TEXT, "			// 실패한 url 정보
			+ " UNIQUE(url)) ";

	// 달력 정보 테이블
	public static final String CREATE_SBM_CALENDAR = "CREATE TABLE IF NOT EXISTS sbm_calendar "
			+ " (calendarymd TEXT PRIMARY KEY, "	// 달력날짜
			+ " weekday INTEGER, "					// 요일
			+ " week INTEGER, "						// 주 수
			+ " holidayyn INTEGER DEFAULT 0, "		// 휴일 여부 1: 휴일 0: 평일
			+ " holidaytxt TEXT, "					// 휴일 문구
			+ " last_update_dt TEXT) ";				// 마지막 업데이트 날짜

	// 오늘 다운로드 받아야 하는 학습 단위코드를 구함.
	// 오늘 이후 14일과 밀린 학습
	public static final String SELECT_DOWNLOAD_STUDY_UNIT = 
		" SELECT itemcode, series_no, item_no, study_order, studyunitcode,                       	  "
		+"dictation_cnt, is_cdn_b_file, debate_cnt, movie_cnt, is_download_prority, restudy_movie_cnt, is_cdn_audio_file, is_exam_zip "
		+"FROM   (SELECT *                                                                            "
		+"        FROM  (SELECT p.studyplanymd,                                                       "
		+"                      p.itemcode,                                                           "
		+"                      p.studyunitcode,                                                      "
		+"                      p.restudycnt,                                                         "
		+"                      Ifnull(r.study_status, '') study_status,                              "
		+"					 case WHEN p.studyplanymd <= SUBSTR(DATETIME('NOW', 'LOCALTIME'), 1 , 10) "
		+"					 	   THEN 1                                                             "
		+"					 	   ELSE 0                                                             "
		+"					END is_download_prority                                                   "
		+"               FROM   tb_studyplan AS p                                                     "
		+"                      LEFT JOIN sbm_study_result r                                          "
		+"                             ON p.mcode = r.customer_no                                     "
		+"                                AND p.itemcode = r.product_no                               "
		+"                                AND p.studyunitcode = r.study_unit_code                     "
		+"                                AND p.restudycnt = r.re_study_cnt                           "
		+"				WHERE  p.studyplanymd <= SUBSTR(DATETIME('NOW', '+14 day'), 1 , 10)           "
		+"                      AND p.mcode = %d)                                                     "
		+"        WHERE  study_status <> 'SFN') pl,                                                    "
		+"       wye_befly_study_unit unit                                                            "
		+"WHERE  pl.itemcode = unit.product_no                                                        "
		+"       AND pl.studyunitcode = unit.study_unit_code                                          "
		+"UNION                                                                                       "
		+"SELECT review.product_no itemcode, unit.series_no series_no, unit.item_no item_no,          "
		+"unit.study_order,  review.study_unit_code studyunitcode, dictation_cnt, 					  "
		+"is_cdn_b_file, debate_cnt, movie_cnt, 1 , restudy_movie_cnt, is_cdn_audio_file, is_exam_zip 							  "
		+"   FROM wye_befly_study_unit unit,                                                          "
		+" (SELECT product_no,                                                                        "
		+"               study_unit_code,                                                             "
		+"               re_study_cnt,                                                                "
		+"               record_file_ymd,                                                             "
		+"               study_finish_dt                                                              "
		+"        FROM   sbm_study_result                                                             "
		+"        WHERE  customer_no = %d                                                             "
		+"               AND study_status = 'SFN'                                                     "
		+"        GROUP  BY study_finish_dt                                                           "
		+"        HAVING study_finish_dt = (SELECT MAX(study_finish_dt)                               "
		+"                                  FROM   sbm_study_result WHERE customer_no = %d )) review                          "
		+" WHERE  unit.product_no = review.product_no                                                 "
		+"       AND unit.study_unit_code = review.study_unit_code 		                              ";
	
	
	// 오늘 학습 가능한 차시를 구함.
	public static final String SELECT_TODAY_STUDY_UNIT =
		"	SELECT itemcode,  series_no, series_name, item_no, study_order, studyunitcode,  "
		+"		study_status, restudycnt, studyplanymd, is_cdn_b_file, movie_cnt, " 
		+" 		book_check_dt, record_file_ymd, restudy_movie_cnt, is_cdn_audio_file, is_exam_zip  "
		+"	FROM   (SELECT p.studyplanymd,                                                  "
		+"	               p.itemcode,                                                      "
		+"	               p.studyunitcode,                                                 "
		+"	               p.restudycnt,                                                    "
		+"	               Ifnull(r.study_status, '') study_status,                         "
		+"	               p.studyplanymd,                                                  "
		+"				   ifnull(r.book_check_dt, '') book_check_dt,      					"
		+"				   ifnull(r.record_file_ymd, '') record_file_ymd					"
		+"	        FROM   tb_studyplan AS p                                                "
		+"	               LEFT JOIN sbm_study_result r                                     "
		+"	                      ON p.mcode = r.customer_no                                "
		+"	                         AND p.itemcode = r.product_no                          "
		+"	                         AND p.studyunitcode = r.study_unit_code                "
		+"	                         AND p.restudycnt = r.re_study_cnt                      "
		+"	        WHERE  p.mcode = %d) pl                                         		"
		+"	       JOIN wye_befly_study_unit unit                                           "
		+"	         ON unit.product_no = pl.itemcode                                       "
		+"	            AND unit.study_unit_code = pl.studyunitcode                         "
		+"	WHERE  study_status <> 'SFN'                                                    "
		+"	       AND (SELECT Min(studyplanymd)                                            "
		+"	            FROM   tb_studyplan s_plan                                          "
		+"	            WHERE  pl.itemcode = s_plan.itemcode 								"
		+"					   AND pl.restudycnt = s_plan.restudycnt) <= SUBSTR(       		"
		+"	           Datetime('NOW', 'LOCALTIME'), 1, 10)                                 "
		+"	ORDER BY studyplanymd, restudycnt, itemcode, study_order    								";
	// 오늘 학습 가능한 차시를 구함.
	public static final String SELECT_NEW_TODAY_STUDY_UNIT =
			"	SELECT itemcode,  series_no, series_name, item_no, study_order, studyunitcode,  "
					+"		study_status, restudycnt, studyplanymd, is_cdn_b_file, movie_cnt, " 
					+" 		book_check_dt, record_file_ymd, restudy_movie_cnt, is_cdn_audio_file, is_exam_zip,book_detail_id,student_book_id,study_result_no,review_status,is_new_dictation,product_type,real_book_no"
					+"	FROM   (SELECT p.studyplanymd,                                                  "
					+"	               p.itemcode,                                                      "
					+"	               p.studyunitcode,                                                 "
					+"	               p.restudycnt,                                                    "
					+"	               Ifnull(r.study_status, '') study_status,                         "
					+"	               p.studyplanymd,                                                  "
					+"				   ifnull(r.book_check_dt, '') book_check_dt,      					"
					+"				   ifnull(r.record_file_ymd, '') record_file_ymd,					"
					+"				   p.book_detail_id,												"
					+"				   p.student_book_id,												"
					+"				   r.study_result_no,												"
					+"				   r.review_status													"
					+"	        FROM   tb_studyplan AS p                                                "
					+"	               LEFT JOIN sbm_study_result r                                     "
					+"	                      ON p.mcode = r.customer_no                                "
					+"	                         AND p.itemcode = r.product_no                          "
					+"	                         AND p.studyunitcode = r.study_unit_code                "
					+"	                         AND p.restudycnt = r.re_study_cnt                      "
					+"	        WHERE  p.mcode = %d) pl                                         		"
					+"	       JOIN wye_befly_study_unit unit                                           "
					+"	         ON unit.product_no = pl.itemcode                                       "
					+"	            AND unit.study_unit_code = pl.studyunitcode                         "
					+"	WHERE  study_status <> 'SFN'                                                    "
					+"	       AND (SELECT Min(studyplanymd)                                            "
					+"	            FROM   tb_studyplan s_plan                                          "
					+"	            WHERE  pl.itemcode = s_plan.itemcode 								"
					+"					   AND pl.restudycnt = s_plan.restudycnt) <= SUBSTR(       		"
					+"	           Datetime('NOW', 'LOCALTIME'), 1, 10)                                 "
					+"	           AND itemcode=%d                                 "
					+"	           AND studyunitcode=%s                                 "
					+"	ORDER BY studyplanymd, restudycnt, itemcode, study_order    								";
	
	// 오늘 학습 가능한 차시를 구함. 숲우영
//		public static final String SELECT_TODAY_STUDY_UNIT =
//			"	SELECT itemcode,  series_no, series_name, item_no, study_order, studyunitcode,  "
//			+"		study_status, restudycnt, studyplanymd, is_cdn_b_file, movie_cnt, " 
//			+" 		book_check_dt, record_file_ymd, acode, class_no, site_kind, restudy_movie_cnt   												"
//			+"	FROM   (SELECT p.studyplanymd,                                                  "
//			+"	               p.itemcode,                                                      "
//			+"	               p.studyunitcode,                                                 "
//			+"	               p.restudycnt,                                                    "
//			+"	               Ifnull(r.study_status, '') study_status,                         "
//			+"	               p.studyplanymd,                                                  "
//			+"				   ifnull(r.book_check_dt, '') book_check_dt,      					"
//			+"				   ifnull(r.record_file_ymd, '') record_file_ymd,					"
//			+"				   p.acode, p.class_no, p.site_kind  								"
//			+"	        FROM   tb_studyplan AS p                                                "
//			+"	               LEFT JOIN sbm_study_result r                                     "
//			+"	                      ON p.mcode = r.customer_no                                "
//			+"	                         AND p.itemcode = r.product_no                          "
//			+"	                         AND p.studyunitcode = r.study_unit_code                "
//			+"	                         AND p.restudycnt = r.re_study_cnt                      "
//			+"	        WHERE  p.mcode = %d) pl                                         		"
//			+"	       JOIN wye_befly_study_unit unit                                           "
//			+"	         ON unit.product_no = pl.itemcode                                       "
//			+"	            AND unit.study_unit_code = pl.studyunitcode                         "
//			+"	WHERE  study_status <> 'SFN'                                                    "
//			+"	       AND (SELECT Min(studyplanymd)                                            "
//			+"	            FROM   tb_studyplan s_plan                                          "
//			+"	            WHERE  pl.itemcode = s_plan.itemcode 								"
//			+"					   AND pl.restudycnt = s_plan.restudycnt) <= SUBSTR(       		"
//			+"	           Datetime('NOW', 'LOCALTIME'), 1, 10)                                 "
//			+"	ORDER BY studyplanymd, restudycnt, itemcode, study_order    								";
//		
	
	
	// 오늘 학습 가능한 차시가 없을 경우 7일 내로 학습 계획이 잡힌 학습을 구함.
	public static final String SELECT_NEXT_STUDY_UNIT =
	"  SELECT itemcode,  series_no, series_name, item_no, study_order, studyunitcode, "
	+" study_status, restudycnt, studyplanymd, is_cdn_b_file, movie_cnt,              "
	+" 	book_check_dt, record_file_ymd, restudy_movie_cnt, is_cdn_audio_file, is_exam_zip  "
	+" FROM   (SELECT p.studyplanymd,                                                 "
	+"                p.itemcode,                                                     "
	+"                p.studyunitcode,                                                "
	+"                p.restudycnt,                                                   "
	+"                Ifnull(r.study_status, '') study_status,                        "
	+"                p.studyplanymd,                                                 "
	+" 			   ifnull(r.book_check_dt, '') book_check_dt,      					  "
	+" 			   ifnull(r.record_file_ymd, '') record_file_ymd					  "
	+"         FROM   tb_studyplan AS p                                               "
	+"                LEFT JOIN sbm_study_result r                                    "
	+"                       ON p.mcode = r.customer_no                               "
	+"                          AND p.itemcode = r.product_no                         "
	+"                          AND p.studyunitcode = r.study_unit_code               "
	+"                          AND p.restudycnt = r.re_study_cnt                     "
	+"         WHERE  p.mcode = %d) pl                                         		  "
	+"        JOIN wye_befly_study_unit unit                                          "
	+"          ON unit.product_no = pl.itemcode                                      "
	+"             AND unit.study_unit_code = pl.studyunitcode                        "
	+" WHERE  study_status <> 'SFN'                                                   " 
	+"        AND (SELECT Min(studyplanymd)                                           "
	+"             FROM   tb_studyplan s_plan                                         "
	+"             WHERE  pl.itemcode = s_plan.itemcode								  "
	+"					  AND pl.restudycnt = s_plan.restudycnt) <= SUBSTR(			  "
	+"       DATETIME('NOW', '+7 day'), 1, 10)                                        "
	+" ORDER BY studyplanymd, restudycnt, itemcode, study_order                                   ";

	
	// 오늘 학습 가능한 차시가 없을 경우 7일 내로 학습 계획이 잡힌 학습을 구함. 숲우영
//		public static final String SELECT_NEXT_STUDY_UNIT =
//		"  SELECT itemcode,  series_no, series_name, item_no, study_order, studyunitcode, "
//		+" study_status, restudycnt, studyplanymd, is_cdn_b_file, movie_cnt,              "
//		+" 	book_check_dt, record_file_ymd, acode, class_no, site_kind, restudy_movie_cnt   												  "
//		+" FROM   (SELECT p.studyplanymd,                                                 "
//		+"                p.itemcode,                                                     "
//		+"                p.studyunitcode,                                                "
//		+"                p.restudycnt,                                                   "
//		+"                Ifnull(r.study_status, '') study_status,                        "
//		+"                p.studyplanymd,                                                 "
//		+" 			   ifnull(r.book_check_dt, '') book_check_dt,      					  "
//		+" 			   ifnull(r.record_file_ymd, '') record_file_ymd,					  "
//		+"             p.acode, p.class_no, p.site_kind  			                      "
//		+"         FROM   tb_studyplan AS p                                               "
//		+"                LEFT JOIN sbm_study_result r                                    "
//		+"                       ON p.mcode = r.customer_no                               "
//		+"                          AND p.itemcode = r.product_no                         "
//		+"                          AND p.studyunitcode = r.study_unit_code               "
//		+"                          AND p.restudycnt = r.re_study_cnt                     "
//		+"         WHERE  p.mcode = %d) pl                                         		  "
//		+"        JOIN wye_befly_study_unit unit                                          "
//		+"          ON unit.product_no = pl.itemcode                                      "
//		+"             AND unit.study_unit_code = pl.studyunitcode                        "
//		+" WHERE  study_status <> 'SFN'                                                   " 
//		+"        AND (SELECT Min(studyplanymd)                                           "
//		+"             FROM   tb_studyplan s_plan                                         "
//		+"             WHERE  pl.itemcode = s_plan.itemcode								  "
//		+"					  AND pl.restudycnt = s_plan.restudycnt) <= SUBSTR(			  "
//		+"       DATETIME('NOW', '+7 day'), 1, 10)                                        "
//		+" ORDER BY studyplanymd, restudycnt, itemcode, study_order                                   ";

	
	// 학습 결과 중 가장 높은 차시를 구해옴.
	public static final String SELECT_STUDY_RESULT_MAX_STUDY_UNIT_NO =
		" SELECT  s_result.product_no                                                            "
		+"   , MAX(s_unit.study_order), s_result.re_study_cnt								   "
		+"   FROM sbm_study_result s_result                                                    "
		+"   	JOIN wye_befly_study_unit s_unit ON s_unit.product_no = s_result.product_no     "
		+"        	AND s_unit.study_unit_code = s_result.study_unit_code                       "
		+"			AND s_result.customer_no = %d												"
		+"   GROUP BY s_unit.product_no                                                        ";
	
	// 가장 최근의 학습중 보충학습을 안한 값을 가져옴.
	public static final String SELECT_RECENT_NO_REVIEW_UNIT =
		" 	SELECT review.product_no,                                        "
		+"        review.study_unit_code,                                    "
		+"        unit.study_order,                                          "
		+"        review.re_study_cnt,                                       "
		+"        unit.series_no,                                            "
		+"        unit.series_name,                                          "
		+"        unit.item_no book_no,                                      "
		+"        unit.is_cdn_b_file,                                        "
		+"        unit.movie_cnt,                                        	 "
		+"        review.record_file_ymd,                                    "
		+"        unit.restudy_movie_cnt,                                    "
		+"        review.study_result_no,                                     "
		+"        unit.real_book_no	                                         "
		+" FROM   wye_befly_study_unit unit,                                 "
		+"        (SELECT product_no,                                        "
		+"                study_unit_code,                                   "
		+"                re_study_cnt,                                      "
		+"                record_file_ymd,                                   "
		+"                study_finish_dt,                                    "
		+"                study_result_no                                    "
		+"         FROM   sbm_study_result                                   "
		+"         WHERE  customer_no = %d                                   "
		+"                AND study_status = 'SFN'                           "
		+"         GROUP  BY study_finish_dt                                 "
		+"         HAVING study_finish_dt = (SELECT MAX(study_finish_dt)     "
		+"                                   FROM   sbm_study_result		 " 
		+"									WHERE customer_no=%d)) review	 "
		+" WHERE  unit.product_no = review.product_no                        "
		+"        AND unit.study_unit_code = review.study_unit_code;         ";
	
	/**
	 * 컨텐츠 중 DB상에서 유효한 차시만 구해옴. 
	 * 학습 계획에서 완료되지 않은 차시와 마지막 완료된 차시 구함.
	 */
	public static final String SELECT_VALID_UNIT = 
		"   SELECT series_no,														   "
		+"         book_no,                                                            "
		+"         study_order                                                         "
		+"  FROM   (SELECT u.series_no series_no,                                      "
		+"                 u.item_no   book_no,                                        "
		+"                 u.study_order                                               "
		+"          FROM   wye_befly_study_unit u,                                     "
		+"                 tb_studyplan p                                              "
		+"          WHERE  p.itemcode = u.product_no                                   "
		+"                 AND p.studyunitcode = u.study_unit_code                     "
		+"          UNION                                                              "
		+"          SELECT unit.series_no series_no,                                   "
		+"                 unit.item_no   book_no,                                     "
		+"                 unit.study_order                                            "
		+"          FROM   wye_befly_study_unit unit,                                  "
		+"                 (SELECT product_no,                                         "
		+"                         study_unit_code                                     "
		+"                  FROM   sbm_study_result                                    "
		+"                  WHERE  study_status = 'SFN'                                "
		+"                  GROUP  BY study_finish_dt                                  "
		+"                  HAVING study_finish_dt = (SELECT Max(study_finish_dt)      "
		+"                                            FROM   sbm_study_result)) review "
		+"          WHERE  unit.product_no = review.product_no                         "
		+"                 AND unit.study_unit_code = review.study_unit_code) e        "
		+"  ORDER  BY series_no,                                                       "
		+"            book_no,                                                         "
		+"            study_order                                                      ";
	                                      
	// 틀린 단어 시험 문항을 조회함.
	public static final String SELECT_INCORRECT_WORD_QUESTION = 
		"	SELECT                                              "
		+"	   word_question_no,                                "
		+"	   question_order,                                  "
		+"	   question,                                        "
		+"	   meaning,                                         "
		+"	   answer,                                          "
		+"	   distractor_1,                                    "
		+"	   distractor_2,                                    "
		+"	   distractor_3,                                    "
		+"	   distractor_4                                     "
		+"	FROM                                                "
		+"	   yfs_sm_word_question question ,                  "
		+"	   (   SELECT                                       "
		+"	       distinct word_question_no                              "
		+"	   FROM                                             "
		+"	      sbm_study_word_detail                         "
		+"	   WHERE                                            "
		+"	      customer_no = %d                        		"
		+"	      AND product_no = %d                        	"
		+"	      AND study_unit_code = %s                   	"
		+"		  AND re_study_cnt = %d 					    "
		+"        AND study_kind = 1						    "
		+"	      AND is_correct = 0)  result                   "
		+"	WHERE                                               "
		+"	   question.sm_word_question_no = word_question_no  "
		+"	order by                                            "
		+"	   question_order                                   ";
	
	// 단어 시험 문항을 조회함.
	public static final String SELECT_WORD_QUESTION =
		"	SELECT                                              "
		+"	   sm_word_question_no word_question_no,            "
		+"	   question_order,                                  "
		+"	   question,                                        "
		+"	   meaning,                                         "
		+"	   answer,                                          "
		+"	   distractor_1,                                    "
		+"	   distractor_2,                                    "
		+"	   distractor_3,                                    "
		+"	   distractor_4                                     "
		+"	FROM                                                "
		+"	   yfs_sm_word_question                             "
		+"	WHERE                                               "
		+"	   product_no = %d                                  "
		+"	   AND study_unit_code = %s                         "
		+"	ORDER BY                                            "
		+"	   question_order                                   ";
	
	// 틀린 문장 시험 문항을 조회함.
	public static final String SELECT_INCORRECT_SENTENCE_QUESTION =
		"	SELECT                                                        "
		+"	   sentence_question_no,                                      "
		+"	   question_order,                                            "
		+"	   sentence,                                                  "
		+"	   text_sentence                                              "
		+"	FROM                                                          "
		+"	   yfs_sm_sentence_question question ,                        "
		+"	   (   SELECT                                                 "
		+"	      sentence_question_no                                    "
		+"	   FROM                                                       "
		+"	      sbm_study_sentence_detail                               "
		+"	   WHERE                                                      "
		+"	      customer_no = %d                                        "
		+"	      AND product_no = %d                                     "
		+"	      AND study_unit_code = %s                                "
		+"		  AND re_study_cnt = %d 								  "
		+"        AND study_kind = 1									  "
		+"	      AND is_correct < 3) result                              "
		+"	WHERE                                                         "
		+"	   question.sm_sentence_question_no = sentence_question_no    "
		+"	ORDER BY                                                      "
		+"	   question_order			                                  ";
	
	// 문장 시험 문항을 조회함.
	public static final String SELECT_SENTENCE_QUESTION =
		"	SELECT                                                  "
		+"	   sm_sentence_question_no sentence_question_no,        "
		+"	   question_order,                                      "
		+"	   sentence,                                            "
		+"	   text_sentence                                        "
		+"	FROM                                                    "
		+"	   yfs_sm_sentence_question                             "
		+"	                                                        "
		+"	   WHERE                                                "
		+"	      product_no = %d                                "
		+"	      AND study_unit_code = %s                       "
		+"	  order by                                              "
		+"	   question_order                                       ";
	
	// 문장 시험 문항을 조회함.
	public static final String SELECT_VANISHING_QUESTION =
		"	SELECT                                                  "
		+"	   sm_pa_sentence_question_no sentence_question_no,        "
		+"	   question_order,                                      "
		+"	   sentence,                                            "
		+"	   text_sentence                                        "
		+"	FROM                                                    "
		+"	   yfs_sm_pa_sentence_question                             "
		+"	                                                        "
		+"	   WHERE                                                "
		+"	      product_no = %d                                "
		+"	      AND study_unit_code = %s                       "
		+"	  order by                                              "
		+"	   question_order                                       ";
	
	//복습 음원 듣기 목록을 조회함
	public static final String SELECT_REVEIW_CDN_AUDIO = 
			"SELECT 														" +
			" 	A.product_no, A.study_unit_code, " +
			"	wye_befly_study_unit.study_order, 						" +
			"	wye_befly_study_unit.series_no," +
			"	wye_befly_study_unit.series_name," +
			"	wye_befly_study_unit.item_no 							" +
			"FROM 															"+
			"( 																" +
			"	SELECT 														"+
			"		product_no,study_unit_code,study_finish_dt 					"+
			"	FROM 														"+
			"		sbm_study_result 										"+
			"	WHERE 														"+
			"		SUBSTR(study_finish_dt,1,10) > SUBSTR(DATETIME('NOW', '-30 day'), 1 , 10)"+                  
	        "	AND 														" +
	        "		customer_no = %d "+
			") AS A, wye_befly_study_unit 									" +
			"WHERE 															" +
			"	A.product_no = wye_befly_study_unit.product_no 				"+
	        "AND " +
	        "	A.study_unit_code = wye_befly_study_unit.study_unit_code " +
	        "AND  	"+
			"	wye_befly_study_unit.is_cdn_b_file = 1						" +
			"ORDER BY " +
			"	A.study_finish_dt desc ";
	
	//스마트 음원듣기를 조회함.
	public static final String SELECT_REVEIW_STUDY_AUDIO = 
			"SELECT 														" +
			" 	A.product_no, A.study_unit_code, " +
			"	wye_befly_study_unit.study_order, 						" +
			"	wye_befly_study_unit.series_no," +
			"	wye_befly_study_unit.series_name," +
			"	wye_befly_study_unit.item_no, 							" +
			"	A.audio_play_time, 							" +
			"	A.review_time, 							" +
			"	A.use_time, 							" +
			"	A.use_cnt, 							" +
			"	A.re_study_cnt 							" +
			"FROM 															"+
			"( 																" +
			"	SELECT 														"+
			"		sbm_study_result.product_no," +
			"		sbm_study_result.study_unit_code," +
			"		sbm_study_result.study_finish_dt," +
			"		sbm_study_result_review.audio_play_time, " +
			"		sbm_study_result_review.review_time, " +
			"		sbm_study_result_review.use_time, " +
			"		sbm_study_result_review.use_cnt,  					"+
			"		sbm_study_result.re_study_cnt  					"+
			"	FROM 														"+
			"		sbm_study_result 	" +
			"	LEFT OUTER JOIN				" +
			"		sbm_study_result_review			" +
			"	ON		" +
			"	sbm_study_result.product_no = sbm_study_result_review.product_no " +
			"	AND 		" +
			"	sbm_study_result.study_unit_code = sbm_study_result_review.study_unit_code 		" +
			"	AND				" +
			"	sbm_study_result.re_study_cnt = sbm_study_result_review.re_study_cnt			" +
			"	AND 			" +
			"	sbm_study_result.customer_no = sbm_study_result_review.customer_no						"+
			"	WHERE 														"+
			"		SUBSTR(study_finish_dt,1,10) > SUBSTR(DATETIME('NOW', '-30 day'), 1 , 10)"+                  
	        "	AND 														" +
	        "		sbm_study_result.customer_no = %d "+
			") AS A, wye_befly_study_unit 									" +
			"WHERE 															" +
			"	A.product_no = wye_befly_study_unit.product_no 				"+
	        "AND " +
	        "	A.study_unit_code = wye_befly_study_unit.study_unit_code " +
			"ORDER BY " +
			"	A.study_finish_dt desc ";

	public static final String SELECT_WEEK_CALENDAR_UNIT = ""
			+ "SELECT C.CALENDARYMD, "
			+ " CASE C.WEEKDAY WHEN 1 THEN 'SUN'  WHEN 2 THEN 'MON' WHEN 3 THEN 'TUE' WHEN 4 THEN 'WED' WHEN 5 THEN 'THU' WHEN 6 THEN 'FRI' WHEN 7 THEN 'SAT' END || strftime(' %d',C.CALENDARYMD) AS DIS_WD, "
			+ " WEEKDAY,";

	public static final String SELECT_WEEK_CALENDAR_UNIT_POSTFIX = ""
			+ " COUNT(DISTINCT SR.STUDY_UNIT_CODE) AS STUDY_UNIT_CNT, "
			+ " ( "
			+ "    SELECT COUNT(studyplanymd) "
			+ "      FROM tb_studyplan "
			+ "     WHERE studyplanymd BETWEEN '%s' AND '%s' "
			+ "       AND studyplanymd = C.CALENDARYMD "
			+ "     GROUP BY studyplanymd "
			+ " )  AS WEEK_STUDY_CNT "
			+ " FROM sbm_calendar C "
			+ " LEFT JOIN sbm_STUDY_RESULT SR ON C.CALENDARYMD = SUBSTR(SR.STUDY_FINISH_DT, 1 , 10) AND SR.STUDY_STATUS = 'SFN' AND SR.CUSTOMER_NO = %d "
			+ " LEFT JOIN tb_studyplan CC ON CC.mcode = %d "
			+ " WHERE C.CALENDARYMD BETWEEN '%s' AND '%s' "
			+ " GROUP BY WEEKDAY, C.calendarymd order by calendarymd asc";

	/**
	 * 범용 INSERT SQL
	 */
	public static final String COMMON_TABLE_INSERT = "INSERT OR REPLACE INTO %s (%s) values(%s)";
	
	/**
	 * 범용 UPDATE SQL
	 */
	public static final String COMMON_TABLE_UPDATE = "UPDATE %s SET %s";
	
	
	/**
	 * delete 클리어all
	 */
	public static final String COMMON_TABLE_DELETE = "DELETE FROM %s" ;
	
	
	/**
	 * 범용 SELECT SQL
	 */
	public static final String COMMON_TABLE_SELECT = "SELECT * FROM %s";
	
	/**
	 * 조건 - 고객 번호 조회용 
	 */
	public static final String COMMON_STR_CUSTOMER_NO = "customer_no = %d";
	
	/**
	 * 조건 - 업로드 유무
	 */
	public static final String COMMON_STR_CRUD = "crud = 'C' OR crud='R'";
	
	/**
	 * 조건 - 교재 
	 */
	public static final String COMMON_STR_STUDY_UNIT = "product_no = %d AND study_unit_code = %s AND re_study_cnt = %d";
	
	public static final String COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT = COMMON_STR_CUSTOMER_NO+" AND "+COMMON_STR_STUDY_UNIT;
	

	/**
	 * CRUD 클리어 SQL
	 */
	public static final String UPDATE_TABLE_CRUD_CLEAR = "UPDATE %s SET crud='' %s WHERE "+COMMON_STR_CUSTOMER_NO+" AND "+COMMON_STR_STUDY_UNIT;
	
	/**
	 * downloadSync에서 사용할 테이블 별 lastupdatedt를 구해옴 
	 */
	public static final String SELECT_LAST_UPDATE_DT = "SELECT IFNULL(MAX(last_update_dt), '') FROM %s";
		
	/**
	 * downloadSync에서 사용할 테이블 별 lastupdatedt를 구해옴 
	 */
	public static final String SELECT_LAST_UPDATE_DT_WHERE = SELECT_LAST_UPDATE_DT+" WHERE "+COMMON_STR_CUSTOMER_NO;
	
	
	/*--------------------------------------------------*/
	/*                     DB 동기화 용             */
	/*--------------------------------------------------*/
	public static final String UPDATE_TABLE_SYNC = COMMON_TABLE_UPDATE+" WHERE customer_no = %s";
	public static final String SELECT_TABLE_SYNC = COMMON_TABLE_SELECT+" WHERE "+COMMON_STR_CRUD;
	
	
	/*--------------------------------------------------*/
	/*                     sbm_network_used             */
	/*--------------------------------------------------*/
	public static final String INSERT_SBM_NETWORK_USE = "INSERT INTO sbm_network_used VALUES (%s, %s, %s, %d);";
	public static final String SELECT_SBM_NETWORK_USE = "SELECT * FROM sbm_network_used";
	public static final String SELECT_SBM_NETWORK_USE_GET_USED = "SELECT used FROM sbm_network_used WHERE ymd = %s and network_kind = %s;";
	public static final String DELETE_SBM_NETWORK_USE = "DELETE FROM sbm_network_used WHERE udid = %s AND ymd = %s AND network_kind = %s";
	public static final String UPDATE_SBM_NETWORK_USE_SET_USE = "UPDATE sbm_network_used SET used = %d WHERE ymd = %s AND network_kind = %s;";

	/*--------------------------------------------------*/
	/*                  sbm_page		                */
	/*--------------------------------------------------*/
	public static final String SELECT_PAGE_STUDY_UNIT = "SELECT type, page_order, page_no, is_used, text FROM sbm_page WHERE series_no = %d and real_book_no = %d and study_order = %d order by page_order;";

	/*--------------------------------------------------*/
	/*                     tb_studyplan             */
	/*--------------------------------------------------*/
	public static final String DELETE_OLD_STUDY_PLAN_BEFORE_180_DAY = "DELETE FROM tb_studyplan WHERE studyplanymd < SUBSTR(DATETIME('NOW', '-180 day'), 1 , 10) AND mcode = %d";
	public static final String DELETE_TB_STUDY_PLAN_WHERE_CUSTOMER_NO = "DELETE FROM tb_studyplan WHERE mcode = %d";
	public static final String UPDATE_TB_STDUY_PLAN_SET_STUDYPLANYMD_NOW = "UPDATE tb_studyplan SET studyplanymd = '%d'";	
	public static final String DELETE_RECENT_STUDY_PLAN_BEFORE_30_DAY = "DELETE FROM tb_studyplan WHERE studyplanymd >= SUBSTR(DATETIME('NOW', '-30 day'), 1 , 10) AND mcode = %d";
	
	
	/*--------------------------------------------------*/
	/*                       sbm_customer               */
	/*--------------------------------------------------*/
	public static final String INSERT_SBM_CUSTOMER = "INSERT OR REPLACE INTO sbm_customer(customer_no, user_id, user_password, customer_name) values(%d, %s, %s, %s)";
	public static final String DELETE_SBM_CUSTOMER = "DELETE FROM sbm_customer WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String UPDATE_SBM_CUSTOMER = "UPDATE sbm_customer SET user_id = %s, user_password= %s , customer_name = %s WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String UPDATE_SBM_CUSTOMER_SET_IS_STUDY_GUIDE = "UPDATE sbm_customer SET is_studyguide = %d WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String SELECT_SBM_CUSTOMER_GET_CNT = "SELECT count(*) FROM sbm_customer";
	public static final String SELECT_SBM_CUSTOMER_GET_CNT_WHERE = "SELECT count(*) FROM sbm_customer WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String SELECT_LOGING_CUSTOMER = "SELECT customer_no, user_id, user_password, customer_name, last_login_dt, "
								+ "is_studyguide FROM sbm_customer order by last_login_dt DESC";
	public static final String SELECT_SBM_CUSTOMER_GET_STUDY_OPTION = "SELECT is_auto_artt, is_caption, is_fast_rewind,  is_studyguide, is_dictation, one_type, three_type, "
								+ " is_review_book, repeat_cnt,is_dic_private,is_dic_answer FROM sbm_customer WHERE "+COMMON_STR_CUSTOMER_NO;
	
	/*--------------------------------------------------*/
	/*                  sbm_customer_config             */
	/*--------------------------------------------------*/
	public static final String DELETE_SBM_CUSTOMER_CONFIG_WHERE_CUSTOMER_NO = "DELETE FROM sbm_customer_config WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String GET_SBM_CUSTOMER_CONFIG_GET_CONF = "SELECT conf_name, conf_value FROM sbm_customer_config WHERE "+COMMON_STR_CUSTOMER_NO;
	
	/*--------------------------------------------------*/
	/*                  sbm_study_day_config             */
	/*--------------------------------------------------*/
	public static final String DELETE_SBM_STUDY_DAY_CONFIG_WHERE_CUSTOMER_NO = "DELETE FROM sbm_study_day_config WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String GET_RESERVE_TIME_WHERE_CUSTOMER_NO_AND_WEEKDAY = "SELECT reserve_time FROM sbm_study_day_config WHERE customer_no = %d AND weekday = %d";
	
	/*--------------------------------------------------*/
	/*                  sbm_study_result             */
	/*--------------------------------------------------*/
	public static final String DELETE_SBM_STUDY_RESULT_STATUS_IS_NULL = "DELETE FROM sbm_study_result WHERE study_status IS NULL OR length(study_status) = 0 ";
	public static final String DELETE_SBM_STUDY_RESULT_WHERE_CUSTOMER_NO = "DELETE FROM sbm_study_result WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String GET_STUDY_TIME_FROM_SBM_STUDY_RESULT = "SELECT review_book_start_dt, book_start_dt, study_finish_dt FROM sbm_study_result WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT;
	public static final String INSERT_SBM_STUDY_RESULT_IF_STUDY = "INSERT INTO sbm_study_result(customer_no, product_no, study_unit_code, re_study_cnt, last_update_dt, record_file_ymd, crud) values (%d, %d, %s, %d, %s, %s, %s)";
	public static final String INSERT_SBM_STUDY_RESULT_IF_RESTUDY = "INSERT INTO sbm_study_result(customer_no, product_no, study_unit_code, re_study_cnt, last_update_dt, record_file_ymd, crud, review_product_no, review_study_unit_code, review_re_study_cnt) values (%d, %d, %s, %d, %s, %s, %s, %d, %s, %d)";
//숲우영
//	public static final String INSERT_SBM_STUDY_RESULT_IF_STUDY = "INSERT INTO sbm_study_result(customer_no, product_no, study_unit_code, re_study_cnt, last_update_dt, record_file_ymd, agency_no, class_no, site_kind, crud) values (%d, %d, %s, %d, %s, %s, %d ,%s)";
//	public static final String INSERT_SBM_STUDY_RESULT_IF_RESTUDY = "INSERT INTO sbm_study_result(customer_no, product_no, study_unit_code, re_study_cnt, last_update_dt, record_file_ymd, agency_no, class_no, site_kind, crud, review_product_no, review_study_unit_code, review_re_study_cnt) values (%d, %d, %s, %d, %s, %s, %d, %d, %d,%s, %d, %s, %d)";
	public static final String UPDATE_SBM_STUDY_RESULT = "UPDATE sbm_study_result SET %s WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT;
	public static final String GET_BOOK_AUDIO_TOTAL_MIN_FROM_SBM_STUDY_RESULT = "SELECT book_start_dt, book_audio_totalmin FROM sbm_study_result WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT;
	public static final String GET_COUNT_SBM_STUDY_RESULT = "SELECT COUNT(*) FROM sbm_study_result WHERE "+	COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT;
	public static final String INSERT_SBM_STUDY_RESULT_AUDIO_PLAY_TIME = "UPDATE sbm_study_result SET book_check_dt = %s, crud='C' WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT;	
	public static final String SELECT_SBM_STUDY_RESULT_GET_STUDY_FINISH = "SELECT DISTINCT u.series_no, u.item_no, u.study_order FROM " 
			+ " wye_befly_study_unit u , sbm_study_result  r "
			+ " WHERE u.product_no = r.product_no AND u.study_unit_code = r.study_unit_code AND r.study_status = 'SFN' ORDER BY study_finish_dt";
	public static final String SELECT_SBM_STUDY_RESULT_GET_AUDIO_STUDY_TIME = "SELECT book_start_dt FROM sbm_study_result WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT;
	public static final String GET_CNT_SBM_STUDY_RESULT_GET_FINISH_NOT_UPLOAD = "SELECT count(*) FROM sbm_study_result WHERE study_status = 'SFN'  AND (crud = 'C' OR crud='R')";
	public static final String DELETE_SBM_STUDY_RESULT_ALL = "DELETE FROM sbm_study_result ";
	
	
	/*--------------------------------------------------*/
	/*                  sbm_study_word_detail             */
	/*--------------------------------------------------*/
	public static final String INSERT_SBM_STUDY_WORD_DETAIL = "INSERT OR REPLACE INTO sbm_study_word_detail(customer_no, product_no, study_unit_code, re_study_cnt, "
			+" study_kind, study_order, is_correct, correct_answer, customer_answer, last_update_dt, word_question_no, crud,study_result_no)" 
			+" values(%d, %d, %s, %d, %d, %d, %d, %s, %s, %s, %d, %s, %d)";
	//숲우영
//	public static final String INSERT_SBM_STUDY_WORD_DETAIL = "INSERT OR REPLACE INTO sbm_study_word_detail(customer_no, product_no, study_unit_code, re_study_cnt, "
//			+" study_kind, study_order, is_correct, correct_answer, customer_answer, last_update_dt, word_question_no, class_no, crud)" 
//			+" values(%d, %d, %s, %d, %d, %d, %d, %s, %s, %s, %d,  %d, %s)";
	public static final String DELETE_SBM_STUDY_WORD_DETAIL = "DELETE FROM sbm_study_word_detail";
	public static final String DELETE_SBM_STUDY_WORD_DETAIL_WHERE_CUSTOMER_NO = "DELETE FROM sbm_study_word_detail WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String SELECT_SBM_STUDY_WORD_DETAIL_GET_IS_CORRECT = "SELECT is_correct FROM sbm_study_word_detail WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT+" AND study_kind = 1";
	public static final String SELECT_SBM_STUDY_WORD_DETAIL_GET_CNT_WHERE_IS_UPLOAD = "SELECT count(*) FROM sbm_study_word_detail WHERE (crud = 'C' OR crud = 'R') "
			+ " AND NOT("+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT+")";
	  
	/*--------------------------------------------------*/
	/*                  sbm_study_sentence_detail             */
	/*--------------------------------------------------*/
	public static final String DELETE_SBM_STUDY_SETENCE_DETAIL_WHERE_CUSTOMER_NO = "DELETE FROM sbm_study_sentence_detail WHERE "+COMMON_STR_CUSTOMER_NO;
	public static final String GET_IS_CORRECT_FROM_SBM_STUDY_SETENCE_DETAIL = "SELECT is_correct FROM sbm_study_sentence_detail WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT+" AND study_kind = 1";
	
	public static final String INSERT_SBM_STUDY_SETENCE_DETAIL = "INSERT OR REPLACE INTO sbm_study_sentence_detail(customer_no, product_no, study_unit_code, re_study_cnt, " 
			+ "study_kind, study_order, is_correct, correct_answer, customer_answer, is_upload, sentence_question_no, last_update_dt, crud,study_result_no)" 
			+" values(%d, %d, %s, %d, %d, %d, %d, %s, %s, %d, %d, %s, %s,%d)";
	//숲우영
//	public static final String INSERT_SBM_STUDY_SETENCE_DETAIL = "INSERT OR REPLACE INTO sbm_study_sentence_detail(customer_no, product_no, study_unit_code, re_study_cnt, " 
//			+ "study_kind, study_order, is_correct, correct_answer, customer_answer, is_upload, sentence_question_no, last_update_dt, class_no, crud)" 
//			+" values(%d, %d, %s, %d, %d, %d, %d, %s, %s, %d, %d, %s,  %d, %s)";
	public static final String UPDATE_SBM_STUDY_SENTENCE_DETAIL_SET_IS_UPLOAD = "UPDATE sbm_study_sentence_detail SET is_upload = %d, last_update_dt = %s, crud = 'C'" +
			" WHERE customer_no = %s AND product_no = %s AND study_unit_code = %s AND re_study_cnt = %s AND study_kind = %d AND study_order = %s";
	public static final String GET_CNT_SBM_STUDY_SENTANCE_DETAIL_NOT_UPLOAD = "SELECT count(*) FROM sbm_study_sentence_detail WHERE (crud = 'C' OR crud = 'R') "
			+ " AND NOT("+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT+")";
	public static final String DELETE_SBM_STUDY_SENTANCE_DETAIL_ALL = "DELETE FROM sbm_study_sentence_detail";
	
	
	/*--------------------------------------------------*/
	/*                  sbm_study_pa_sentence_detail             */
	/*--------------------------------------------------*/
	//문단 연습 결과 조회
	public static final String GET_IS_CORRECT_FROM_SBM_STUDY_VANISHING_DETAIL = "SELECT is_correct FROM sbm_study_pa_sentence_detail WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT+" AND study_kind = 1";
	public static final String INSERT_SBM_STUDY_VANISHING_DETAIL = "INSERT OR REPLACE INTO sbm_study_pa_sentence_detail(customer_no, product_no, study_unit_code, re_study_cnt, " 
			+ "study_kind, study_order, is_correct, correct_answer, customer_answer, is_upload, retry_cnt ,sentence_question_no, last_update_dt, crud, study_result_no)" 
			+" values(%d, %d, %s, %d, %d, %d, %d, %s, %s, %d, %d, %d,%s, %s, %d)";

	public static final String DELETE_SBM_STUDY_VANISHING_DETAIL_ALL = "DELETE FROM sbm_study_pa_sentence_detail";
	public static final String UPDATE_SBM_STUDY_VANISHING_DETAIL_SET_IS_UPLOAD = "UPDATE sbm_study_pa_sentence_detail SET is_upload = %d, last_update_dt = %s, crud = 'C'" +
			" WHERE customer_no = %s AND product_no = %s AND study_unit_code = %s AND re_study_cnt = %s AND study_kind = %d AND study_order = %s";
	
	// 신규문항
	/*--------------------------------------------------*/
	/*                  sbm_study_exam_detail             */
	/*--------------------------------------------------*/
	public static final String INSERT_SBM_STUDY_EXAM_DETAIL = "INSERT OR REPLACE INTO sbm_study_exam_detail(customer_no, product_no, study_unit_code, re_study_cnt, " 
			+ "study_kind, study_order, tag_type, is_correct, is_upload, last_update_dt, question_order, question_no, customer_answer,study_result_no)" 
			+" values(%d, %d, %s, %d, %d, %d, %d, %d, %d, %s, %s, %s, %s)";
	public static final String INSERT_SBM_STUDY_EXAM_CRUD_DETAIL = "INSERT OR REPLACE INTO sbm_study_exam_detail(customer_no, product_no, study_unit_code, re_study_cnt, " 
			+ "study_kind, study_order, tag_type, is_correct, is_upload, last_update_dt, question_order, question_no, customer_answer, crud,study_result_no)" 
			+" values(%d, %d, %s, %d, %d, %d, %d, %d, %d, %s, %s, %s, %s, %s)";
	public static final String UPDATE_SBM_STUDY_EXAM_DETAIL_SET_IS_UPLOAD = "UPDATE sbm_study_exam_detail SET is_upload = %d, last_update_dt = %s, crud = 'C'" +
			" WHERE customer_no = %s AND product_no = %s AND study_unit_code = %s AND re_study_cnt = %s AND study_kind = %d AND study_order = %s AND question_no = %s AND question_order = %s";

	public static final String DELETE_SBM_STUDY_EXAM_DETAIL_ALL = "DELETE FROM sbm_study_exam_detail";
//	public static final String UPDATE_SBM_STUDY_EXAM_DETAIL_SET_IS_UPLOAD = "UPDATE sbm_study_exam_detail SET is_upload = %d, last_update_dt = %s, crud = 'C'" +
//			" WHERE customer_no = %s AND product_no = %s AND study_unit_code = %s AND re_study_cnt = %s AND study_kind = %d AND study_order = %s";

	
	
	/*--------------------------------------------------*/
	/*                  sbm_study_record_detail             */
	/*--------------------------------------------------*/
	public static final String INSERT_SBM_STUDY_RECORD_DETAIL_ = "INSERT OR REPLACE INTO sbm_study_record_detail(customer_no, product_no, study_unit_code, re_study_cnt, " 
			+ "study_kind, study_order, start_tag_name, start_tag_time, is_upload, crud)" 
			+" values(%s, %s, %s, %s, %d, %s, %s, %s, 1, 'C')";
	//클래스 아이디 추가
	public static final String INSERT_SBM_STUDY_RECORD_DETAIL_C = "INSERT OR REPLACE INTO sbm_study_record_detail(customer_no,product_no, study_unit_code, re_study_cnt, " 
			+ "study_kind, study_order, start_tag_name, start_tag_time, is_upload, crud, study_result_no, is_merge,rec_type,start_merge_time,end_merge_time,rec_len,rep_cnt,file_path,book_detail_id,student_book_id,end_tag_time)" 
			+" values(%s,%s, %s, %s,  %d, %s, %s, %s, 1, 'C',%s,%d,%s, %s, %s, %s,%d,%s,%d,%d,%s)";
	
	
	/*--------------------------------------------------*/
	/*                  wye_befly_study_unit             */
	/*--------------------------------------------------*/
	public static final String SELECT_ALL_WYE_BEFLY_STUDY_UNIT = "SELECT product_no, study_unit_code, study_order, series_no, series_name, item_no " 
			+ " FROM wye_befly_study_unit ORDER BY series_name, study_order";
	public static final String GET_CNT_STUDY_UNIT_DICTAION = "SELECT dictation_cnt FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	public static final String GET_CNT_STUDY_UNIT_DEBASE = "SELECT debate_cnt FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	public static final String GET_CNT_STUDY_UNIT_MOVIE = "SELECT movie_cnt FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	public static final String GET_STUDY_UNIT_IS_CDN = "SELECT is_cdn_b_file FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	public static final String GET_STUDY_UNIT_IS_CDN_MOVIE = "SELECT restudy_movie_cnt FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	public static final String GET_STUDY_UNIT_IS_CDN_AUDIO = "SELECT is_cdn_audio_file FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	public static final String GET_STUDY_UNIT_IS_CDN_ZIP = "SELECT is_exam_zip FROM wye_befly_study_unit WHERE product_no = %d AND study_unit_code = %s";
	
	
	/*--------------------------------------------------*/
	/*                  yfs_sm_word_question             */
	/*--------------------------------------------------*/
	public static final String SELECT_YFS_SM_WORD_QUESTION_LIST = "SELECT question,meaning FROM yfs_sm_word_question WHERE product_no = %d AND study_unit_code = %s";
	public static final String SELECT_YFS_SM_WORD_MEANING_LIST = "SELECT meaning FROM yfs_sm_word_question WHERE product_no = %d AND study_unit_code = %s";
	
	
	/*--------------------------------------------------*/
	/*                  yfs_sm_sentence_question             */
	/*--------------------------------------------------*/
	
	public static final String GET_CNT_YFS_SM_SENTENCE_QUESTION = "SELECT count(*) FROM yfs_sm_sentence_question WHERE product_no = %d AND study_unit_code = %s";
	
	
	/*--------------------------------------------------*/
	/*                  yfs_sm_pa_sentence_question             */
	/*--------------------------------------------------*/
	
	public static final String GET_CNT_YFS_SM_VANISHING_QUESTION = "SELECT count(*) FROM yfs_sm_pa_sentence_question WHERE product_no = %d AND study_unit_code = %s";
	
	/*--------------------------------------------------*/
	/*                  sbm_study_book_warning             */
	/*--------------------------------------------------*/
	
	public static final String DELETE_SBM_STUDY_BOOK_WARNNING = "DELETE FROM sbm_study_book_warning "
			+ "WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT+" AND book_check_status = %d";
	public static final String INSERT_SBM_STUDY_BOOK_WARNNING = "INSERT OR REPLACE INTO sbm_study_book_warning " 
			+ " (customer_no, product_no, study_unit_code, re_study_cnt, book_check_status, book_check_dt, crud,study_result_no) "
			+ " VALUES (%d, %d, %s, %d, %d, %s, 'C',%d);";
	
	/*--------------------------------------------------*/
	/*                  download_content             */
	/*--------------------------------------------------*/
	public static final String SELECT_DOWNLOAD_CONTENTS_UNIT = "SELECT study_unit_id, priority FROM download_content GROUP BY study_unit_id order by priority desc ";
	public static final String SELECT_DOWNLOAD_CONTENTS_UNIT_FILE = "SELECT study_unit_id, download_id, download_url, save_path FROM download_content"; 
	public static final String DELETE_DOWNLOAD_CONTESTS = "DELETE FROM download_content WHERE download_id = %d;";
	public static final String DELETE_DOWNLOAD_CONTESTS_ALL = "DELETE FROM download_content";
	public static final String UPDATE_DOWNLOAD_CONTENTS_SET_PRIORITY = "UPDATE download_content SET priority=0";
	public static final String INSERT_DOWNLOAD_CONTESTS = "INSERT OR REPLACE INTO download_content (study_unit_id, filename, download_url, save_path, priority) "
			+ " VALUES (%d, %s, %s, %s, %d);";
	
	
	/*--------------------------------------------------*/
	/*                  sync_info             */
	/*--------------------------------------------------*/
	public static final String SELECT_SYNC_INFO_GET_LAST_DT = "SELECT last_sync_dt FROM sync_info";
	public static final String INSERT_SYNC_INFO = "INSERT OR REPLACE INTO sync_info VALUES (1, %s)";
	
	
	
	/*--------------------------------------------------*/
	/*                  sbm_study_result_review             */
	/*--------------------------------------------------*/
	public static final String INSERT_SBM_STUDY_RESULT_REVIEW = "INSERT OR REPLACE INTO sbm_study_result_review  (customer_no, product_no, study_unit_code, re_study_cnt, " +
			"	audio_play_time, review_time, use_time, use_cnt) VALUES (%d, %d, %s, %d, %d, %d, %d, %d);";
	public static final String UPDATE_SBM_STUDY_RESULT_REVIEW = "UPDATE sbm_study_result_review SET use_time =%d, use_cnt = %d "
			+" WHERE "+COMMON_STR_CUSTOMER_NO_AND_STUDY_UNIT; 
	
	/*--------------------------------------------------*/
	/*                  sbm_study_record_detail             */
	/*--------------------------------------------------*/
	public static final String SELECT_SBM_STUDY_RECORD_ALL = "SELECT customer_no,product_no ,study_result_no,book_detail_id ,student_book_id,study_unit_code,re_study_cnt,study_kind ,study_order,rec_type,start_tag_name ,start_tag_time ,end_tag_time,start_merge_time  ,end_merge_time ,rec_len ,is_upload  ,is_merge,rep_cnt ,crud,file_path FROM  sbm_study_record_detail;";
	public static final String SELECT_SBM_STUDY_RECORD_DTAIL_MERGTIME = "SELECT DISTINCT start_merge_time FROM  sbm_study_record_detail WHERE is_merge=1;";
	public static final String SELECT_SBM_STUDY_RECORD_DTAIL_RECDATA = "SELECT customer_no,product_no ,study_result_no,book_detail_id ,student_book_id,study_unit_code,re_study_cnt,study_kind ,study_order,rec_type,start_tag_name ,start_tag_time ,end_tag_time,start_merge_time  ,end_merge_time ,rec_len ,is_upload  ,is_merge,rep_cnt ,crud,file_path FROM  sbm_study_record_detail WHERE start_merge_time=%s ORDER BY study_order" ;; 
	public static final String DELETE_SBM_STUDY_RECORD_DTAIL_RECDATA = "DELETE FROM  sbm_study_record_detail WHERE start_merge_time=%s AND is_merge=1 " ;


	public static final String INSERT_SBM_FAIL_STUDY_LOG = "INSERT INTO sbm_study_log VALUES (%s);";
	public static final String SELECT_SBM_FAIL_STUDY_LOG = "SELECT url FROM sbm_study_log";
	public static final String SELECT_SBM_FAIL_STUDY_LOG_CNT = "SELECT count(*) FROM sbm_study_log";
	public static final String DELETE_SBM_FAIL_STUDY_LOG = "DELETE FROM sbm_study_log";

}