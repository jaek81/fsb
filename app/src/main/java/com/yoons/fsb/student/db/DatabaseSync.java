package com.yoons.fsb.student.db;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.TagEventData;
import com.yoons.fsb.student.db.DatabaseData.TodayStudyUnit;
import com.yoons.fsb.student.db.DatabaseDefine.ColValue;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * db Sync 클래스
 *
 * @author ejlee
 */
public class DatabaseSync {
    private static final String TAG = "[DatabaseSync]";
    private Handler mResponseHandler = null; // request 결과를 받을 handler
    private ArrayList<String> mUploadSyncTableList = new ArrayList<String>(); // 업로드
    // 할
    // sync테이블
    // 리스트
    private ArrayList<ArrayList<ColValue>> mUploadSyncData = null; // 업로드할 컬럼
    // 리스트
    private ArrayList<String> mResultExcludeColumn = new ArrayList<String>(); // 업로드에서
    // 제외할
    // 컬럼
    // 리스트

    private Context mContext = null;
    private DatabaseUtil mDatabaseUtil = null;
    private int mCustomerId = 0; // 사용자 id
    boolean mIsSkipUploadDeviceError = false; // 업로드시 서버에서 sbm_study_result 업로드시
    // -6 오류(사용자가 등록되지 않은 곳에서 업로드 할때
    // 나오는 오류)가 왔을때 무시할 지 여부

    /**
     * 생성자
     *
     * @param context
     */
    public DatabaseSync(Context context) {
        mContext = context;
        mDatabaseUtil = DatabaseUtil.getInstance(mContext);
    }

    /**
     * downlod sync 실행 동기화할 테이블 리스트를 만듬 동기화 실행
     *
     * @param handler    : 결과를 받을 Handler
     * @param customerId : 사용자 Id
     * @throws UnsupportedEncodingException
     */
    public void downloadSyncStart(Handler handler, int customerId, int producNo, String studyUnit, String mBookdetailid, String mStudentofbookid) throws UnsupportedEncodingException {
        mResponseHandler = handler;
        mCustomerId = customerId;

        // 해당 사용자의 -180일 데이터 삭제.
        mDatabaseUtil.deleteOldStudyPlan(mCustomerId);

        // 동기화 실패시 원복하기위해 transaction을 걸어놓음.
        mDatabaseUtil.beginTransaction();

        // 변경된 학습 계획을 다시 받기 위해 -30일이후의 plan을 삭제함.
        mDatabaseUtil.deleteRecentStudyPlan(mCustomerId);
        downloadSyncAllTable(producNo, studyUnit, mBookdetailid, mStudentofbookid);
    }

    public void downloadPageDataStart(Handler handler, int seriesNo, int realBookNo) {
        mResponseHandler = handler;
        mDatabaseUtil.beginTransaction();
        downloadPageData(seriesNo, realBookNo);
    }

    /**
     * 리스트 목록 가져오기
     *
     * @param handler    : 결과를 받을 Handler
     * @param customerId : 사용자 Id
     */
    public void downloadList(Handler handler, int customerId) {
        mResponseHandler = handler;
        mCustomerId = customerId;

        StudyData.clear();

        downloadList();
    }

    private Handler mPageDownloadHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS: // 정상응답
                    try {
                        int result = ((JSONObject) msg.obj).getInt("out2");
                        if (result == 0) {
                            insertDownloadPageData((JSONObject) msg.obj);
                            sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS + 111);
                        } else {
                            sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL + 111);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL: // 응답 실패
                    sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL + 111);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    // download sync 메시지를 받는 handler
    private Handler mDownloadHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS: // 정상응답
                    try {
                        int result = ((JSONObject) msg.obj).getInt("out15");
                        if (result != 0) {
                            sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL);
                        } else {
                            mDatabaseUtil.deleteStudyResultAll();//잠시
                            mDatabaseUtil.deleteDownloadContentAll();//잠시
                            Crashlytics.log(msg.obj.toString());
                            insertDownloadSyncData((JSONObject) msg.obj);
                            sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorData.setErrorMsg(ErrorData.ERROR_DOWNLOAD_SYNC, e);
                        sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL);
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL: // 응답 실패
                    sendDownloadSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };
    // download sync 메시지를 받는 handler
    private Handler mlistHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS: // 정상응답
                    try {
                        int result = ((JSONObject) msg.obj).getInt("out2");
                        if (result != 0) {
                            sendListSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL);
                        } else {
                            insertListSyncData((JSONObject) msg.obj);
                            Crashlytics.log(((JSONObject) msg.obj).toString());
                            sendListSyncResult(ServiceCommon.MSG_DATABASE_NEW_DOWNLOAD_SYNC_SUCCESS);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorData.setErrorMsg(ErrorData.ERROR_DOWNLOAD_SYNC, e);
                        sendListSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL);
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL: // 응답 실패
                    sendListSyncResult(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * downloadSync result를 mResponseHandler에 전송
     *
     * @param result : sync 결과
     */
    private void sendDownloadSyncResult(int result) {
        try {
            if (result == ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS) {
                mDatabaseUtil.endTransaction(true); // 성공시 db commit
            } else {
                Log.e(TAG, "endTransaction 실패");
                mDatabaseUtil.endTransaction(false); // 실패시 db rollback
            }

        } catch (Exception e) {
            Log.e(TAG, "hy  " + e.toString());
        } finally {
            if (mResponseHandler != null) {
                mResponseHandler.sendMessage(mResponseHandler.obtainMessage(result));
            }
        }

    }

    /**
     * downloadSync result를 mResponseHandler에 전송
     *
     * @param result : sync 결과
     */
    private void sendListSyncResult(int result) {
        if (mResponseHandler != null) {
            mResponseHandler.sendMessage(mResponseHandler.obtainMessage(result));
        }
    }

    /**
     * 테이블의 last_update_dt를 구함 테이블에 내용이 없으면, 2010-01-01로 설정
     *
     * @param tableName : 조회할 테이블 이름
     * @return maxdt
     */
    private String getLastUpdateDt(String tableName) {
        String maxDate = mDatabaseUtil.selectLastUpdateDt(tableName);
        if (maxDate.equals("")) {
            maxDate = "2015-10-01";
        }

        try {
            return URLEncoder.encode(maxDate, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "2015-10-01";
    }

    /**
     * downloadSync 테이블을 조회하여 서버에 데이터 요청함.
     *
     * @throws UnsupportedEncodingException
     */
    private void downloadSyncAllTable(int productNo, String studyUnit, String mBookdetailid, String mStudentofbookid) throws UnsupportedEncodingException {
        ArrayList<String> params = new ArrayList<String>();
        if (CommonUtil.isCenter()) {
            params.add(Preferences.getForestCode(mContext));
            params.add(Preferences.getTeacherCode(mContext));
            params.add(Preferences.getLmsStatus(mContext));
        }
        params.add(String.valueOf(mCustomerId));
        params.add(getOneMonthAgoDate()); // studyplan_term
        params.add(getOneMonthAgoDate()); // studyunit_term
        params.add(getLastUpdateDt("yfs_sm_word_question")); // word_question_max_dt
        params.add(getLastUpdateDt("yfs_sm_sentence_question")); // sentence_question_max_dt
        params.add(getLastUpdateDt("sbm_study_result")); // study_result_max_dt
        params.add(getLastUpdateDt("sbm_study_word_detail")); // study_word_detail_max_dt
        params.add(getLastUpdateDt("sbm_study_sentence_detail")); // study_sentence_detail_max_dt
        // 신규문항
        params.add(getLastUpdateDt("sbm_study_pa_sentence_detail")); // study_pa_sentence_detail_max_dt
        params.add(getLastUpdateDt("sbm_study_exam_detail")); // study_exam_detail_max_dt
        params.add("2015-10-01"); // localquery_max_dt
        params.add(String.valueOf(productNo)); // localquery_max_dt
        params.add(String.valueOf(URLEncoder.encode(studyUnit, "UTF-8"))); //차시
        params.add(mBookdetailid);
        params.add(mStudentofbookid);
        params.add(CommonUtil.getMacAddress());
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestDownloadSyncAll(mDownloadHandler, (String[]) params.toArray(new String[0]));
    }

    private void downloadPageData(int seriesNo, int realBookNo) {
        ArrayList<String> params = new ArrayList<String>();
        params.add(String.valueOf(seriesNo));
        params.add(String.valueOf(realBookNo));

        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestPageData(mPageDownloadHandler, (String[]) params.toArray(new String[0]));
    }

    /**
     * downloadSync 테이블을 조회하여 서버에 데이터 요청함.
     */
    private void downloadList() {
        ArrayList<String> params = new ArrayList<String>();
        if (CommonUtil.isCenter()) {
            params.add(Preferences.getForestCode(mContext));
            params.add(Preferences.getTeacherCode(mContext));
            params.add(Preferences.getLmsStatus(mContext));
        }
        params.add(String.valueOf(mCustomerId));
        /*
         * params.add(getOneMonthAgoDate()); // studyplan_term
		 * params.add(getOneMonthAgoDate()); // studyunit_term
		 * params.add(getLastUpdateDt("yfs_sm_word_question")); //
		 * word_question_max_dt
		 * params.add(getLastUpdateDt("yfs_sm_sentence_question")); //
		 * sentence_question_max_dt
		 * params.add(getLastUpdateDt("sbm_study_result")); //
		 * study_result_max_dt
		 * params.add(getLastUpdateDt("sbm_study_word_detail")); //
		 * study_word_detail_max_dt
		 * params.add(getLastUpdateDt("sbm_study_sentence_detail")); //
		 * study_sentence_detail_max_dt // 신규문항
		 * params.add(getLastUpdateDt("sbm_study_pa_sentence_detail")); //
		 * study_pa_sentence_detail_max_dt
		 * params.add(getLastUpdateDt("sbm_study_exam_detail")); //
		 * study_exam_detail_max_dt params.add("2015-10-01"); //
		 * localquery_max_dt
		 */
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestListsyncAll(mlistHandler, (String[]) params.toArray(new String[0]));
    }

    /**
     * 한달전 날짜를 구함
     *
     * @return YYYY-MM-DD
     */
    private String getOneMonthAgoDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -30);

        return String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
    }

    private void insertDownloadPageData(JSONObject json) throws  JSONException, IOException {
        ArrayList<String> delteList = new ArrayList<String>();
        delteList.add("sbm_page");
        mDatabaseUtil.deleteCommonTable(delteList);
        mDatabaseUtil.insertOrUpdateSyncData("sbm_page", json.getJSONArray("out1"));
    }

    private void insertDownloadSyncData(JSONObject json) throws JSONException, IOException {
		/*
		 * String test2 = StringUtil.readAssetText(mContext,
		 * "json_select2.txt"); JSONArray jsonArray = new JSONArray(test2);
		 * System.out.println(jsonArray); for (int i = 0; i <
		 * jsonArray.length(); i++) { JSONObject obj =
		 * jsonArray.getJSONObject(i);
		 *
		 * TodayStudyUnit studyUnit = new TodayStudyUnit(obj);
		 * StudyData.setmStudyUnit(studyUnit);
		 */
		/*
		 * String test = StringUtil.readAssetText(mContext, "json_select6.txt");
		 * JSONObject jsonObject = new JSONObject(test);
		 * mDatabaseUtil.insertOrUpdateSyncData("sbm_customer",
		 * jsonObject.getJSONArray("out1"));
		 * mDatabaseUtil.insertOrUpdateSyncData("tb_studyplan",
		 * jsonObject.getJSONArray("out3"));
		 * mDatabaseUtil.insertOrUpdateSyncData("wye_befly_study_unit",
		 * jsonObject.getJSONArray("out4"));
		 * mDatabaseUtil.insertOrUpdateSyncData("tb_studyplan",
		 * jsonObject.getJSONArray("out3"));
		 * mDatabaseUtil.insertOrUpdateSyncData("yfs_sm_word_question",
		 * jsonObject.getJSONArray("out5"));
		 * mDatabaseUtil.insertOrUpdateSyncData("yfs_sm_sentence_question",
		 * jsonObject.getJSONArray("out6"));
		 * mDatabaseUtil.insertOrUpdateSyncData("yfs_sm_pa_sentence_question",
		 * jsonObject.getJSONArray("out7"));
		 */

        ArrayList<String> delteList = new ArrayList<String>();
        delteList.add("android_metadata");
		/*delteList.add("sbm_localquery");
		delteList.add("sync_info");
		delteList.add("sbm_customer");
		delteList.add("sbm_customer_config");
		delteList.add("sbm_study_day_config");*/
        delteList.add("sbm_study_result");
        delteList.add("sbm_study_result_review");
        delteList.add("sbm_study_book_warning");
        delteList.add("wye_befly_study_unit");
        delteList.add("yfs_sm_word_question");
        delteList.add("yfs_sm_sentence_question");
        delteList.add("yfs_sm_pa_sentence_question");
        delteList.add("sbm_study_past_history");
        delteList.add("sbm_sync_tables");
        delteList.add("download_content");
        delteList.add("sqlite_sequence");
        delteList.add("sbm_network_used");
        delteList.add("tb_studyplan");
        delteList.add("sbm_study_record_detail");
        delteList.add("sbm_study_word_detail");
        delteList.add("sbm_study_sentence_detail");
        delteList.add("sbm_study_pa_sentence_detail");
        delteList.add("sbm_study_exam_detail");
        mDatabaseUtil.deleteCommonTable(delteList);

        mDatabaseUtil.insertOrUpdateSyncData("sbm_customer", json.getJSONArray("out1"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_customer_config", json.getJSONArray("out2"));
        mDatabaseUtil.insertOrUpdateSyncData("tb_studyplan", json.getJSONArray("out3"));
        mDatabaseUtil.insertOrUpdateSyncData("wye_befly_study_unit", json.getJSONArray("out4"));
        mDatabaseUtil.insertOrUpdateSyncData("yfs_sm_word_question", json.getJSONArray("out5"));
        mDatabaseUtil.insertOrUpdateSyncData("yfs_sm_sentence_question", json.getJSONArray("out6"));
        mDatabaseUtil.insertOrUpdateSyncData("yfs_sm_pa_sentence_question", json.getJSONArray("out7"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_study_day_config", json.getJSONArray("out8"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_study_result", json.getJSONArray("out9"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_study_word_detail", json.getJSONArray("out10"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_study_sentence_detail", json.getJSONArray("out11"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_study_pa_sentence_detail", json.getJSONArray("out12"));
        mDatabaseUtil.insertOrUpdateSyncData("sbm_study_exam_detail", json.getJSONArray("out13"));
        try {
            TagEventData.setQuizJsonArray(json.getString("out16"));//숲용 quiz팝업
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void insertListSyncData(JSONObject json) throws JSONException, IOException {

		/*
		 * String test2 = StringUtil.readAssetText(mContext,
		 * "json_select2.txt"); JSONArray jsonArray = new
		 * JSONObject(test2).getJSONArray("out1");
		 */
        JSONArray jsonArray = json.getJSONArray("out1");
        System.out.println(jsonArray);
        StudyData.listClear();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);

            TodayStudyUnit studyUnit = new TodayStudyUnit(obj);
            StudyData.setmStudyUnit(studyUnit);

        }
    }

    // uploadHandler 업로드 결과를 받는 handler
    private Handler mUploadHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    try {
                        String sysdate = "";

                        String table = mUploadSyncTableList.get(0);

                        if (!mIsSkipUploadDeviceError) {
                            if (table.equals("sbm_study_result")) {
                                if (((JSONObject) msg.obj).get("out3").toString().length() > 2) {
                                    int result = ((JSONObject) msg.obj).getInt("out3");
                                    if (result == -6) { // sbm_study_result테이블 업로드시 -6이
                                        // 리턴될때 서버상 현 device에 등록되지 않은
                                        // 사용자의 결과가 업로드 되는 것임.
                                        sendUploadSyncResult(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_DEVICE_ERROR_FAIL);
                                        return;
                                    }
                                }
                            }
                        }

                        if (table.equals("sbm_network_used")) { // 네트워크 사용량은 응답이 오면
                            // 삭제
                            if (((JSONObject) msg.obj).getInt("out1") == 0) {
                                deleteNetworkUsed(mUploadSyncData.get(0));
                            }
                        } else {
                            if (((JSONObject) msg.obj).get("out3").toString().length() > 2 && ((JSONObject) msg.obj).getInt("out3") == 0) {
                                //							if (((JSONObject) msg.obj).getInt("out3") == 0) {
                                sysdate = ((JSONObject) msg.obj).getString("out2");
                            }

                            clearStudyResultCrud(table, mUploadSyncData.get(0), sysdate);
                        }

                        mUploadSyncData.remove(0);
                        uploadSyncTable();
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorData.setErrorMsg(ErrorData.ERROR_UPLOAD_SYNC, e);
                        sendUploadSyncResult(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL);
                    }
                    break;

                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    // 이건 호출한 곳에서 ErrorMsg를 기록함.
                    sendUploadSyncResult(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL);
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * upload sync시작
     *
     * @param handler : 응답 받을 handler
     */
    public void uploadSyncStart(Handler handler) {
        uploadSyncStart(handler, false, true);
    }

    /**
     * upload sync시작
     *
     * @param handler                 : 응답 받을 handler
     * @param isSkipUploadDeviceError : sbm_study_result 업로드에서 오류로 -6을 리턴할시 무시할지 여부
     * @param isDeleteInvalidResult   : 복습 쓰레기 데이터를 지울지 여부(복습 간지에서 업데이트도 하므로 적용)
     */
    public void uploadSyncStart(Handler handler, boolean isSkipUploadDeviceError, boolean isDeleteInvalidResult) {
        mResponseHandler = handler;
        mIsSkipUploadDeviceError = isSkipUploadDeviceError;

        // 이전학습 복습 중 중단할 경우 잘못된 데이터가 남을 수 있으므로 서버에 전송하지 않도록 삭제
        if (isDeleteInvalidResult) {
            mDatabaseUtil.deleteInvalidResult();
        }

        // uploadsync를 해야하는 table 설정
        //mUploadSyncTableList.add("sbm_network_used");
        //mUploadSyncTableList.add("sbm_study_result");
        //mUploadSyncTableList.add("sbm_study_word_detail");
        //	mUploadSyncTableList.add("sbm_study_sentence_detail");
        //mUploadSyncTableList.add("sbm_study_pa_sentence_detail");
        //mUploadSyncTableList.add("sbm_study_exam_detail");
        //mUploadSyncTableList.add("sbm_study_book_warning");
        //mUploadSyncTableList.add("sbm_study_record_detail");

        // mResultExcludeColumn이 각 테이블마다 다를 경우에는 따로 만들어준다.
        // 순서가 바뀔수가 있다면 순서가 넣어진 list로 관리하거나 query문에 넣는다.
        //mResultExcludeColumn.add("study_result_no");
        mResultExcludeColumn.add("last_update_dt");
        mResultExcludeColumn.add("book_check_dt");
        mResultExcludeColumn.add("crud");

        uploadSyncTable();
    }

    /**
     * uploadSync 진행
     */
    private void uploadSyncTable() {
        if (mUploadSyncTableList.size() == 0) {
            mDatabaseUtil.insertSyncInfo();

            sendUploadSyncResult(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS);
            return;
        }

        ArrayList<String> params = null;

        if (mUploadSyncData == null) {
            String table = mUploadSyncTableList.get(0);
            if (table.equals("sbm_network_used")) {
                mUploadSyncData = mDatabaseUtil.selectNetworkUsed();
            } else {
                mUploadSyncData = mDatabaseUtil.selectSyncStudyResult(mUploadSyncTableList.get(0));
            }
        }

        if (mUploadSyncData.size() == 0) { // 테이블의 컬럼을 모두 업로드 했을 때
            mUploadSyncTableList.remove(0);
            mUploadSyncData = null;
            uploadSyncTable();
        } else {
            params = makeUploadParams();

            HttpJSONRequest request = new HttpJSONRequest(mContext);
            String table = mUploadSyncTableList.get(0);
            if (table.equals("sbm_network_used")) {
                request.requestNetworkUsed(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_result")) {
                request.requestStudyResultFinish(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_word_detail")) {
                request.requestStudyWordDetail(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_sentence_detail")) {
                request.requestStudySentenceDetail(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_pa_sentence_detail")) {
                request.requestStudyVanishingDetail(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_exam_detail")) {
                request.requestStudyExamDetail(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_book_warning")) {
                request.requestStudyBookWarning(mUploadHandler, (String[]) params.toArray(new String[0]));
            } else if (table.equals("sbm_study_record_detail")) {
                request.requestStudyRecordDetail(mUploadHandler, (String[]) params.toArray(new String[0]));
            }
        }
    }

    /**
     * uploadSync request를 만들기 위한 param 생성
     *
     * @return sync inparam list
     */
    public ArrayList<String> makeUploadParams() {
        ArrayList<String> params = new ArrayList<String>();
        ArrayList<ColValue> colvalue = mUploadSyncData.get(0);
        String table = mUploadSyncTableList.get(0);

        for (int i = 0; i < colvalue.size(); i++) {
            ColValue column = colvalue.get(i);
            if (table.equals("sbm_study_book_warning") && column.mColumn.equals("book_check_dt")) {

            } else {
                if (mResultExcludeColumn.contains(column.mColumn)) {
                    continue;
                }
            }

            // 날짜 형식이면 encoding해준다.
            if (column.mColumn.toUpperCase().contains("_DT") || column.mColumn.toUpperCase().contains("_ANSWER")) {
                try {
                    params.add(URLEncoder.encode(column.mValue, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                params.add(column.mValue);
            }
        }

        if (table.equals("sbm_study_result")) {
            params.add(CommonUtil.getDeviceId(mContext));
        }

        if (table.equals("sbm_study_result")) {
            params.add(CommonUtil.getDeviceId(mContext));
        }

        return params;
    }

    /**
     * uploadSync 결과를 전달하는 함수
     *
     * @param result : 결과
     */
    private void sendUploadSyncResult(int result) {
        if (mResponseHandler != null) {
            if (result == ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_DEVICE_ERROR_FAIL) { // sbm_study_result에서만
                // 발생
                ArrayList<ColValue> colvalue = mUploadSyncData.get(0);

                int customerNo = 0;

                for (int i = 0; i < colvalue.size(); i++) {
                    ColValue column = colvalue.get(i);

                    if (column.mColumn.equals("customer_no")) {
                        customerNo = Integer.valueOf(column.mValue);
                        break;
                    }
                }

                mResponseHandler.sendMessage(mResponseHandler.obtainMessage(result, customerNo, 0));
            } else {
                mResponseHandler.sendMessage(mResponseHandler.obtainMessage(result));
            }
        }
    }

    /**
     * 네트워크 사용량 삭제하는 함수
     *
     * @param colvalue : 삭제해야하는 column과 value
     */
    private void deleteNetworkUsed(ArrayList<ColValue> colvalue) {
        String udid = "";
        String ymd = "";
        String networkKind = "";

        for (int i = 0; i < colvalue.size(); i++) {
            ColValue column = colvalue.get(i);

            if (column.mColumn.equalsIgnoreCase("udid")) {
                udid = column.mValue;
            } else if (column.mColumn.equalsIgnoreCase("ymd")) {
                ymd = column.mValue;
            } else if (column.mColumn.equalsIgnoreCase("network_kind")) {
                networkKind = column.mValue;
            }
        }

        mDatabaseUtil.deleteNetworkUsed(udid, ymd, networkKind);
    }

    /**
     * upload하고 나서 crud컬럼을 clear하는 함수
     *
     * @param tableName    : 테이블 이름
     * @param colvalue     : column과 value 리스트
     * @param lastupdateDt : 업데이트해야 할 date
     */
    private void clearStudyResultCrud(String tableName, ArrayList<ColValue> colvalue, String lastupdateDt) {
        int customerNo = 0;
        int productNo = 0;
        String studyUnitCode = "";
        int reStudyCnt = 0;
        int studyKind = 0;
        int studyOrder = 0;
        int bookCheckStatus = 0;
        // 신규문항
        String questionNo = "";
        String questionOrder = "";

        for (int i = 0; i < colvalue.size(); i++) {
            ColValue column = colvalue.get(i);

            if (column.mColumn.equalsIgnoreCase("customer_no")) {
                customerNo = Integer.valueOf(column.mValue);
            } else if (column.mColumn.equalsIgnoreCase("product_no")) {
                productNo = Integer.valueOf(column.mValue);
            } else if (column.mColumn.equalsIgnoreCase("study_unit_code")) {
                studyUnitCode = column.mValue;
            } else if (column.mColumn.equalsIgnoreCase("re_study_cnt")) {
                reStudyCnt = Integer.valueOf(column.mValue);
            } else if (column.mColumn.equalsIgnoreCase("study_kind")) {
                studyKind = Integer.valueOf(column.mValue);
            } else if (column.mColumn.equalsIgnoreCase("study_order")) {
                studyOrder = Integer.valueOf(column.mValue);
            } else if (column.mColumn.equalsIgnoreCase("question_no")) { // 신규문항
                questionNo = column.mValue;
            } else if (column.mColumn.equalsIgnoreCase("question_order")) {
                questionOrder = column.mValue;
                ;
            }

            if (tableName.equals("sbm_study_book_warning")) {
                if (column.mColumn.equalsIgnoreCase("book_check_status")) {
                    bookCheckStatus = Integer.valueOf(column.mValue);
                }
            }
        }

        if (tableName.equals("sbm_study_record_detail")) {
            mDatabaseUtil.deleteStudyRecordDetail(customerNo, productNo, studyUnitCode, reStudyCnt, studyKind, studyOrder);
        } else if (tableName.equals("sbm_study_book_warning")) {
            mDatabaseUtil.deleteStudyBookWarning(customerNo, productNo, studyUnitCode, reStudyCnt, bookCheckStatus);
        } else {
            mDatabaseUtil.updateStudyResultCrudClear(tableName, customerNo, productNo, studyUnitCode, reStudyCnt, studyKind, studyOrder, questionNo, questionOrder, lastupdateDt);
        }
    }
}
