package com.yoons.fsb.student.db;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.RecData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.ExamResult;
import com.yoons.fsb.student.data.StudyData.SentenceQuestion;
import com.yoons.fsb.student.data.StudyData.SentenceResult;
import com.yoons.fsb.student.data.StudyData.StudyResult;
import com.yoons.fsb.student.data.StudyData.VanishingQuestion;
import com.yoons.fsb.student.data.StudyData.VanishingResult;
import com.yoons.fsb.student.data.StudyData.WordQuestion;
import com.yoons.fsb.student.data.StudyData.WordResult;
import com.yoons.fsb.student.db.DatabaseData.DownloadContent;
import com.yoons.fsb.student.db.DatabaseData.DownloadStudyUnit;
import com.yoons.fsb.student.db.DatabaseData.LoginCustomer;
import com.yoons.fsb.student.db.DatabaseData.ReviewStudyAudio;
import com.yoons.fsb.student.db.DatabaseData.ReviewStudyAudioUnit;
import com.yoons.fsb.student.db.DatabaseData.ReviewStudyBook;
import com.yoons.fsb.student.db.DatabaseData.ReviewStudyBookUnit;
import com.yoons.fsb.student.db.DatabaseData.StudyOption;
import com.yoons.fsb.student.db.DatabaseData.StudyResultUnitMax;
import com.yoons.fsb.student.db.DatabaseData.StudyUnit;
import com.yoons.fsb.student.db.DatabaseData.TodayStudyUnit;
import com.yoons.fsb.student.db.DatabaseDefine.ColValue;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * db Util 함수
 * 
 * @author ejlee
 */
public class DatabaseUtil {
	static final String TAG = "[DatabaseUtil]";

	// 신규문항
	private static final int DATABASE_VERSION = 4; // DB Version

	private static DatabaseUtil mDatabaseUtil = null;
	private SQLiteDatabase mDb = null; // db instance
	private Context mContext = null;
	private boolean mIsBeginTransaction = false; // 데이터 동기화를 위해 사용함

	public static DatabaseUtil getInstance(Context context) {
		if (mDatabaseUtil == null) {
			mDatabaseUtil = new DatabaseUtil(context);
		}

		return mDatabaseUtil;
	}

	public static DatabaseUtil getInstance() {
		if (mDatabaseUtil == null) {
			Log.e(TAG, "error DatabaseUtil is not instance, call getInstance(Context context)");
		}

		return mDatabaseUtil;
	}

	public static void init() {
		mDatabaseUtil = null;
	}

	/**
	 * 생성자
	 * 
	 * @param context
	 */
	public DatabaseUtil(Context context) {
		mContext = context.getApplicationContext();

		// db파일이 없을 경우 asset의 db를 dbpath에 복사함.
		File file = new File(ServiceCommon.DB_PATH);
		/*
		 * if (!file.exists() || ServiceCommon.IS_EXPERIENCES) {
		 *
		 * CommonUtil.copyAssetsToSDCard(mContext, ServiceCommon.DB_FILE,
		 * ServiceCommon.DB_PATH); }
		 */

		OpenHelper dbHelper = new OpenHelper(mContext);

		try {
			mDb = dbHelper.getWritableDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// db버전이 업그레이드 될 경우 asset에서 복사하고, db를 close하고 다시 open함
		if (dbHelper.mIsUpgrade) {
			file = new File(ServiceCommon.DB_PATH);
			deleteDatabase(file);
			CommonUtil.copyAssetsToSDCard(mContext, ServiceCommon.DB_FILE, ServiceCommon.DB_PATH);

			mDb.close();

			dbHelper = new OpenHelper(mContext);
			try {
				mDb = dbHelper.getWritableDatabase();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * db파일 삭제
	 * 
	 * @param file
	 *            : 삭제할 db File
	 * @return 성공 여부
	 */
	public boolean deleteDatabase(File file) {
		if (file == null) {
			throw new IllegalArgumentException("file must not be null");
		}

		String save_file = ServiceCommon.DB_BACK_UP_PATH + file.getName().substring(0, file.getName().lastIndexOf(".")) + "_" + now2Str2() + ".db";
		Log.k("wusi12", "move file : " + save_file);
		copyFile(file, save_file);

		boolean deleted = false;
		deleted |= file.delete();
		deleted |= new File(file.getPath() + "-journal").delete();
		deleted |= new File(file.getPath() + "-shm").delete();
		deleted |= new File(file.getPath() + "-wal").delete();

		File dir = file.getParentFile();
		if (dir != null) {
			final String prefix = file.getName() + "-mj";
			final FileFilter filter = new FileFilter() {
				@Override
				public boolean accept(File candidate) {
					return candidate.getName().startsWith(prefix);
				}
			};
			for (File masterJournal : dir.listFiles(filter)) {
				deleted |= masterJournal.delete();
			}
		}
		return deleted;
	}

	private String now2Str2() {
		Calendar calendar = Calendar.getInstance();
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss", java.util.Locale.KOREA);
		return formatter.format(calendar.getTime());

	}

	/**
	 * 파일 복사
	 * 
	 * @param file
	 * @param save_file
	 * @return
	 */
	private boolean copyFile(File file, String save_file) {
		boolean result;
		if (file != null && file.exists()) {
			try {
				FileInputStream fis = new FileInputStream(file);
				FileOutputStream newfos = new FileOutputStream(save_file);
				int readcount = 0;
				byte[] buffer = new byte[1024];
				while ((readcount = fis.read(buffer, 0, 1024)) != -1) {
					newfos.write(buffer, 0, readcount);
				}
				newfos.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	/**
	 * openHelper
	 * 
	 * @author ejlee
	 */
	private static class OpenHelper extends SQLiteOpenHelper {
		private static final String DATABASE_NAME = ServiceCommon.DB_PATH;
		public boolean mIsUpgrade = false;

		/**
		 * 생성자 sd카드에서 db open
		 * 
		 * @param context
		 */
		public OpenHelper(Context context) {
			super(new ContextWrapper(context) {
				@Override
				public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory) {

					return SQLiteDatabase.openDatabase(DATABASE_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);
				}
			}, DATABASE_NAME, null, DATABASE_VERSION);
		}

		public void onCreate(SQLiteDatabase db) {
			createTable(db);
		}

		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log.e(TAG, "onUpgrade : " + oldVersion + " " + newVersion);

			mIsUpgrade = true;
		}

		/**
		 * 테이블 생성
		 * 
		 * @param db
		 */
		public void createTable(SQLiteDatabase db) {
			db.execSQL(DatabaseDefine.CREATE_SBM_LOCALQUERY);
			db.execSQL(DatabaseDefine.CREATE_SYNC_INFO);
			db.execSQL(DatabaseDefine.CREATE_SBM_CUSTOMER);
			db.execSQL(DatabaseDefine.CREATE_SBM_CUSTOMER_CONFIG);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_DAY_CONFIG);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_RESULT);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_RESULT_REVIEW);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_BOOK_WARNING);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_WORD_DETAIL);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_SENTENCE_DETAIL);
			// 신규문항
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_EXAM_DETAIL);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_VANISHING_DETAIL);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_RECORD_DETAIL);
			db.execSQL(DatabaseDefine.CREATE_WYE_BEFLY_STUDY_UNIT);
			db.execSQL(DatabaseDefine.CREATE_YFS_SM_WORD_QUESTION);
			db.execSQL(DatabaseDefine.CREATE_YFS_SM_SENTENCE_QUESTION);
			db.execSQL(DatabaseDefine.CREATE_YFS_SM_VANISHING_QUESTION);
			db.execSQL(DatabaseDefine.CREATE_SBM_STUDY_PAST_HISTORY);
			db.execSQL(DatabaseDefine.CREATE_SBM_SYNC_TABLES);
			db.execSQL(DatabaseDefine.CREATE_TB_STUDYPLAN);
			db.execSQL(DatabaseDefine.CREATE_DOWNLOAD_CONTENT);
			db.execSQL(DatabaseDefine.CREATE_SBM_NETWORK_USED);
			db.execSQL(DatabaseDefine.CREATE_SBM_FAIL_STUDY_LOG);
			db.execSQL(DatabaseDefine.CREATE_SBM_CALENDAR);
			db.execSQL(DatabaseDefine.CREATE_SBM_PAGE);
		}

		@Override
		public void onOpen(SQLiteDatabase db) {
			createTable(db);

			super.onOpen(db);
		}
	}

	/**
	 * transaction 시작 함수
	 */
	public void beginTransaction() {
		try {
			mDb.beginTransaction();
			mIsBeginTransaction = true;
		} catch (Exception e) {
			Log.e(TAG, e.toString());
			mIsBeginTransaction = false;
		}

	}

	/**
	 * transaction 종료 함수
	 * 
	 * @param isCommit
	 *            : commit할지 여부
	 */
	public void endTransaction(boolean isCommit) {
		if (isCommit && mIsBeginTransaction == true) {
			mDb.setTransactionSuccessful();
		}

		mDb.endTransaction();
		mIsBeginTransaction = false;
	}

	/**
	 * execSQL을 실행할지 결정하는 함수 contents_test에서는 download_content를 제외한 테이블에서는 기록을
	 * 하지 않도록 함.
	 * 
	 * @param query
	 */
	public void execSQL(String query) {
		if (ServiceCommon.IS_EXPERIENCES) {
			mDb.execSQL(query);
			return;
		} else if (ServiceCommon.IS_CONTENTS_TEST && !query.contains("download_content")) {
			Log.i(TAG, "IS_CONTENTS_TEST execSQL skip...");
			if (!query.contains("sbm_customer"))
				return;
		}
		// Log.e(TAG, query);
		mDb.execSQL(query);
	}

	/**
	 * downloadSync에서 받은 json데이터를 table에 insert하는 함수
	 * 
	 * @param tableName
	 *            : 테이블 이름
	 * @param jsonArray
	 *            : row 리스트
	 * @throws JSONException
	 * @throws SQLException
	 */
	public void insertOrUpdateSyncData(String tableName, JSONArray jsonArray) throws JSONException, SQLException {

		JSONObject jsonObj = null;
		String columns = "";
		String values = "";
		String setValues = "";
		String customerNo = "";
		boolean isInsert = true;

		// sbm_customer 테이블은 update 함.
		if (tableName.equals("sbm_customer")) {
			isInsert = false;
		}

		for (int i = 0; i < jsonArray.length(); i++) {
			jsonObj = (JSONObject) jsonArray.get(i);

			columns = "";
			values = "";
			Iterator iter = jsonObj.keys();
			while (iter.hasNext()) {
				String key = (String) iter.next();
				Object obj = jsonObj.opt(key);
				String value = "";
				if (obj instanceof String) {
					value = (String) obj;
					if (value.equals("null")) {
						value = "";
					}

					value = value.replace("\"", "\"\"");
                    try {
                        value = quote(value.trim());//공백제거
                    } catch (Exception e) {
                        e.printStackTrace();
                        value = quote(value);//예외상황시
                    }
				} else {
					value = String.valueOf(obj);

					if (value.equals("null")) {
						value = "";
						value = quote(value);
					}
				}

				if (isInsert) {
					columns += key;
					values += value;

					if (iter.hasNext()) {
						columns += ",";
						values += ",";
					}
				} else { // update
					if (key.equalsIgnoreCase("customer_no")) { // where 에 들어갈 customer_no
														// 추출
						customerNo = value;
					}

					if (key.equalsIgnoreCase("is_studyguide")) { // 학습 가이드는 update하지 않음.
						continue;
					}

					setValues += key + " = " + value;

					if (iter.hasNext()) {
						setValues += ",";
					}
				}

				// Log.e(TAG, key + ":" + value + " " +
				// obj.getClass().toString());
			}

			String query = "";

			if (isInsert) {
				query = String.format(DatabaseDefine.COMMON_TABLE_INSERT, tableName, columns, values);
			} else {
				query = String.format(DatabaseDefine.UPDATE_TABLE_SYNC, tableName, setValues, customerNo);
			}

			// Log.k("hy","hy2"+ query);

			execSQL(query);
		}
	}

	/**
	 * 이전학습 복습을 하다 중단했을 경우 남는 쓰레기 result 삭제
	 */
	public void deleteInvalidResult() {
		try {
			execSQL(DatabaseDefine.DELETE_SBM_STUDY_RESULT_STATUS_IS_NULL);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteInvalidResult Error : " + e);
		}
	}

	/**
	 * sbm_network_used 테이블 리스트 조회
	 * 
	 * @return column, value 리스트
	 */
	public ArrayList<ArrayList<ColValue>> selectNetworkUsed() {
		// String selectQuery = "SELECT * FROM sbm_network_used";
		Cursor cursor = null;
		ArrayList<ArrayList<ColValue>> list = new ArrayList<ArrayList<ColValue>>();

		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_SBM_NETWORK_USE, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					ArrayList<ColValue> column = new ArrayList<ColValue>();

					for (int i = 0; i < cursor.getColumnCount(); i++) {
						ColValue colvalue = new ColValue();
						String value = cursor.getString(i);

						colvalue.mColumn = cursor.getColumnName(i);
						colvalue.mValue = value == null ? "" : value;

						column.add(colvalue);
					}

					list.add(column);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectNetworkUsed Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return list;
	}

	public ArrayList<StudyData.PageInfo> selectPageInfo(int seriesNo, int realBookNo, int studyOrder) {
		Cursor cursor = null;
		String query = "";

		query = String.format(DatabaseDefine.SELECT_PAGE_STUDY_UNIT, seriesNo, realBookNo, studyOrder);
		Log.i("", "realBookNo => " + realBookNo);
		ArrayList<StudyData.PageInfo> pageList = new ArrayList<StudyData.PageInfo>();

		try {
			cursor = mDb.rawQuery(query, null);

			if(cursor != null && cursor.moveToFirst()) {
				do {
					StudyData.PageInfo page = new StudyData.PageInfo();
					page.mType = cursor.getString(0);
					page.mPageOrder = cursor.getInt(1);
					page.mPageNo = cursor.getInt(2);
					page.mIsUsed = cursor.getInt(3) == 1 ? true : false;
					page.mText = cursor.getString(4);
					if (page.mIsUsed)
						pageList.add(page);
				} while(cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			android.util.Log.e(TAG, "selectPageInfo Error : " + e);
		} finally {
			if(cursor != null) {
				cursor.close();
			}
		}

		return pageList;
	}

	/**
	 * uploadsync해야 할 컬럼 리스트 조회
	 * 
	 * @param tableName
	 *            : 테이블 이름
	 * @return update할 column, value리스트
	 */
	public ArrayList<ArrayList<ColValue>> selectSyncStudyResult(String tableName) {
		String selectQuery = String.format(DatabaseDefine.SELECT_TABLE_SYNC, tableName);
		Cursor cursor = null;
		ArrayList<ArrayList<ColValue>> list = new ArrayList<ArrayList<ColValue>>();

		try {
			cursor = mDb.rawQuery(selectQuery, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					ArrayList<ColValue> column = new ArrayList<ColValue>();

					for (int i = 0; i < cursor.getColumnCount(); i++) {
						ColValue colvalue = new ColValue();
						String value = cursor.getString(i);

						colvalue.mColumn = cursor.getColumnName(i);
						colvalue.mValue = value == null ? "" : value;

						column.add(colvalue);
					}

					list.add(column);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectSyncStudyResult Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return list;
	}

	/**
	 * (배본정책 변경으로 90일 -> 180일로 변경) 이전 tb_studyplan 데이터 삭제
	 * 
	 * @param customerNo
	 *            : 삭제할 customerNo
	 */
	public void deleteOldStudyPlan(int customerNo) {
		String query = String.format(DatabaseDefine.DELETE_OLD_STUDY_PLAN_BEFORE_180_DAY, customerNo);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteOldStudyPlan Error : " + e);
		}
	}

	/**
	 * -30부터 오늘까지 삭제. 새로 받는 데이터로 업데이트 하기위해, 학습 계획 날짜가 변경될수가 있어서 삭제를 해야함.
	 * 
	 * @param customerNo
	 *            : 삭제할 customerNo
	 */
	public void deleteRecentStudyPlan(int customerNo) {
		String query = String.format(DatabaseDefine.DELETE_RECENT_STUDY_PLAN_BEFORE_30_DAY, customerNo);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteRecentStudyPlan Error : " + e);
		}
	}

	/**
	 * downloadSync에서 사용할 테이블 별 lastupdatedt를 구해옴
	 * 
	 * @param tableName
	 *            : 테이블 이름
	 * @return lastupdatedt
	 */
	public String selectLastUpdateDt(String tableName) {
		String query = "";
		if (tableName.equals("sbm_study_result") || tableName.equals("sbm_study_word_detail") || tableName.equals("sbm_study_sentence_detail")) {
			query = String.format(DatabaseDefine.SELECT_LAST_UPDATE_DT_WHERE, tableName, Preferences.getCustomerNo(mContext));
		} else {
			query = String.format(DatabaseDefine.SELECT_LAST_UPDATE_DT, tableName);
		}

		Cursor cursor = null;
		String lastUpdateDt = "";

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				lastUpdateDt = cursor.getString(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectLastUpdateDt Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return lastUpdateDt;
	}

	/**
	 * db 사용자 삭제
	 * 
	 * @param customerNo
	 *            : 삭제할 customerNo
	 */
	public void deleteCustomer(int customerNo) {
		String deleteQuery[] = { DatabaseDefine.DELETE_SBM_CUSTOMER, DatabaseDefine.DELETE_SBM_STUDY_RESULT_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_SBM_CUSTOMER_CONFIG_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_SBM_STUDY_DAY_CONFIG_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_SBM_STUDY_WORD_DETAIL_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_SBM_STUDY_SETENCE_DETAIL_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_TB_STUDY_PLAN_WHERE_CUSTOMER_NO };

		try {
			for (int i = 0; i < deleteQuery.length; i++) {
				String query = String.format(deleteQuery[i], customerNo);
				execSQL(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteCustomer Error : " + e);
		}
	}

	/**
	 * 검수앱 db 사용자 삭제
	 * 
	 * @param customerNo
	 *            : 삭제할 customerNo
	 */
	public void deleteCustomerOnly(int customerNo) {
		String deleteQuery[] = { DatabaseDefine.DELETE_SBM_CUSTOMER, };

		try {
			for (int i = 0; i < deleteQuery.length; i++) {
				String query = String.format(deleteQuery[i], customerNo);
				execSQL(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteCustomer Error : " + e);
		}
	}

	/**
	 * 테이블 삭제
	 */
	public void deleteCommonTable(ArrayList<String> list) {

		try {
			for (String table : list) {
				String query = String.format(DatabaseDefine.COMMON_TABLE_DELETE, table);
				execSQL(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteCustomer Error : " + e);
		}
	}

	/**
	 * 학습 결과 삭제
	 * 
	 * @param customerNo
	 *            : 삭제할 customerNo
	 */
	public void deleteStudyResult(int customerNo) {
		String deleteQuery[] = { DatabaseDefine.DELETE_SBM_STUDY_RESULT_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_SBM_STUDY_WORD_DETAIL_WHERE_CUSTOMER_NO, DatabaseDefine.DELETE_SBM_STUDY_SETENCE_DETAIL_WHERE_CUSTOMER_NO };

		try {
			for (int i = 0; i < deleteQuery.length; i++) {
				String query = String.format(deleteQuery[i], customerNo);
				execSQL(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteStudyResult Error : " + e);
		}
	}

	/**
	 * 회원 추가
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param id
	 *            : 아이디
	 * @param password
	 *            : 비밀번호
	 * @param customerName
	 *            : 회원 이름
	 */
	public void insertCustomer(int customerNo, String id, String password, String customerName) {
		String query = "";

		try {
			// query = String.format(DatabaseDefine.INSERT_SBM_CUSTOMER,
			// customerNo, quote(id), quote(password), quote(customerName));
			query = String.format(DatabaseDefine.INSERT_SBM_CUSTOMER, customerNo, singleQuote(id), singleQuote(password), singleQuote(customerName));
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertCustomer Error : " + e);
		}
	}

	/**
	 * 회원 update
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param id
	 *            : 아이디
	 * @param password
	 *            : 비밀번호
	 * @param customerName
	 *            : 회원 이름
	 */
	public void updateCustomer(int customerNo, String id, String password, String customerName) {
		String query = "";

		try {
			// query = String.format(DatabaseDefine.UPDATE_SBM_CUSTOMER,
			// quote(id), quote(password), quote(customerName), customerNo);
			query = String.format(DatabaseDefine.UPDATE_SBM_CUSTOMER, singleQuote(id), singleQuote(password), singleQuote(customerName), customerNo);
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateCustomer Error : " + e);
		}
	}

	/**
	 * sbm_study_day_config 조회(교사가 설정한 학습 시간)
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @return 오늘 요일에 설정된 시간
	 */
	public String selectStudyDayConfig(int customerNo) {
		Cursor cursor = null;
		String query = "";
		String studyTime = "";

		query = String.format(DatabaseDefine.GET_RESERVE_TIME_WHERE_CUSTOMER_NO_AND_WEEKDAY, customerNo, CommonUtil.getWeekday());

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				studyTime = cursor.getString(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectStudyDayConfig Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return studyTime;
	}

	/**
	 * 학습 결과를 db에서 조회하는 함수
	 * 
	 * @return 학습 결과 data
	 */
	public StudyResult getStudyExamResult() {
		StudyData data = StudyData.getInstance();
		Cursor cursor = null;
		String query = "";
		StudyResult result = new StudyResult();

		result.mProductName = data.mProductName;

		query = String.format(DatabaseDefine.SELECT_SBM_STUDY_WORD_DETAIL_GET_IS_CORRECT, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					result.mWordTotal++;

					if (cursor.getInt(0) == 1) {
						result.mWordCorrect++;
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "getStudyExamResult Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		query = String.format(DatabaseDefine.GET_IS_CORRECT_FROM_SBM_STUDY_SETENCE_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					result.mSentenceTotal++;

					if (cursor.getInt(0) >= 3) {
						result.mSentenceCorrect++;
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "getStudyExamResult Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		query = String.format(DatabaseDefine.GET_IS_CORRECT_FROM_SBM_STUDY_VANISHING_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					result.mVanishingTotal++;

					if (cursor.getInt(0) >= 3) {
						result.mVanishingCorrect++;
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "getStudyExamResult Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		String startDt = "";
		String finishDt = "";

		query = String.format(DatabaseDefine.GET_STUDY_TIME_FROM_SBM_STUDY_RESULT, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				startDt = cursor.getString(0);
				if (startDt == null || startDt.length() < 10) {
					startDt = cursor.getString(1);
				}

				finishDt = cursor.getString(2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "getStudyExamResult Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		Date startDate = null;
		Date finishDate = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (startDt != null && startDt.length() > 0 && finishDt != null && finishDt.length() > 0) {
			try {
				startDate = sdf.parse(startDt);
				finishDate = sdf.parse(finishDt);

				result.mStudyTotalTime = (int) ((finishDate.getTime() - startDate.getTime()) / 1000 / 60);

			} catch (Exception e) {
				e.printStackTrace();
				result.mStudyTotalTime = 0;
			}
		} else {
			result.mStudyTotalTime = 0;
		}

		return result;
	}

	/**
	 * 현재 오디오 학습 진행 시간을 기록함. sbm_study_result의 book_check_dt에 기록함.
	 * 
	 * @param msec
	 *            : 오디오 학습 진행 시간
	 */
	public void updateStudyResultAudioPlayTime(int msec) {
		StudyData data = StudyData.getInstance();
		String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_RESULT_AUDIO_PLAY_TIME, quote(String.valueOf(msec)), data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateStudyResultAudioPlayTime Error : " + e);
		}
	}

	/**
	 * 학습 결과 테이블 crud clear
	 * 
	 * @param tableName
	 *            : 테이블 이름
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @param studyKind
	 *            : 학습 종류(본학습, 이전학습복습)
	 * @param studyOrder
	 *            : 차시 번호
	 * @param lastUpdateDt
	 *            : lastupdatedt
	 */
	public void updateStudyResultCrudClear(String tableName, int customerNo, int productNo, String studyUnitCode, int reStudyCnt, int studyKind, int studyOrder, String questionNo, String questionOrder, String lastUpdateDt) {
		String detailQuery = " AND study_kind = %d AND study_order = %d";
		String detailExamQuery = " AND question_no = %s AND question_order = %s";
		String query = "";
		String setString = "";

		if (lastUpdateDt != null && !lastUpdateDt.isEmpty()) {
			setString = ",last_update_dt=" + quote(lastUpdateDt);
		}

		if (tableName.equals("sbm_study_result")) {
			query = String.format(DatabaseDefine.UPDATE_TABLE_CRUD_CLEAR, tableName, setString, customerNo, productNo, quote(studyUnitCode), reStudyCnt);
		} else if (tableName.equals("sbm_study_exam_detail")) { // 신규문항
			query = String.format(DatabaseDefine.UPDATE_TABLE_CRUD_CLEAR + detailQuery + detailExamQuery, tableName, setString, customerNo, productNo, quote(studyUnitCode), reStudyCnt, studyKind, studyOrder, quote(questionNo), questionOrder);
		} else {
			query = String.format(DatabaseDefine.UPDATE_TABLE_CRUD_CLEAR + detailQuery, tableName, setString, customerNo, productNo, quote(studyUnitCode), reStudyCnt, studyKind, studyOrder);
		}

		// Log.i(TAG, "updateStudyResultCrudClear : " + query);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateStudyResultCrudClear Error : " + e);
		}
	}

	/**
	 * sbm_study_result에 학습 상태를 update함
	 * 
	 * @param step
	 *            : 학습 진행 상태
	 */
	public void updateStudyResultStatus(String step) {
		StudyData data = StudyData.getInstance();

		String query = "";
		String setString = "";
		String currentDt = CommonUtil.getCurrentDateTime();
		String recordFileDt = data.mRecordFileYmd;

		int customerNo = data.mCustomerNo;
		int productNo = 0;
		// int agency_no = 0;
		// int class_no = 0;
		// int site_kind = 0;
		String studyUnitCode = "";
		int reStudyCnt = 0;
		int reviewProductNo = 0;
		String reviewStudyUnitCode = "";
		int reviewReStudyCnt = 0;

		productNo = data.mProductNo;
		studyUnitCode = data.mStudyUnitCode;
		reStudyCnt = data.mReStudyCnt;

		reviewProductNo = data.mReviewProductNo;
		reviewStudyUnitCode = data.mReviewStudyUnitCode;
		reviewReStudyCnt = data.mReviewReStudyCnt;

		// 숲우영
		// agency_no = data.mAgencyCode;
		// class_no = data.mClassNo;
		// site_kind = data.mSiteKind;

		// 학습 결과 정보가 없으면 row생성
		// 2014-04-23
		// agency_no, class_no, site_kind 컬럼 추가
		if (!isExistStudyResult(customerNo, productNo, studyUnitCode, reStudyCnt)) {
			try {
				if (step.equals("S01")) {
					query = String.format(DatabaseDefine.INSERT_SBM_STUDY_RESULT_IF_STUDY, customerNo, productNo, quote(studyUnitCode), reStudyCnt, quote(currentDt), quote(recordFileDt), quote("C"));
				} else if (step.equals("R01")) {
					query = String.format(DatabaseDefine.INSERT_SBM_STUDY_RESULT_IF_RESTUDY, customerNo, productNo, quote(studyUnitCode), reStudyCnt, quote(currentDt), quote(recordFileDt), quote("C"), reviewProductNo, quote(reviewStudyUnitCode), reviewReStudyCnt);
				}
				execSQL(query);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}

		if (step.contains("S")) { // 본학습 일때
			setString = "study_status = " + quote(step);
		} else {
			setString = "review_status = " + quote(step);
		}

		setString += ", last_update_dt = " + quote(currentDt);

		if (step.equals("S01")) { // 학습 시작
			setString += ", is_auto_artt = " + booleanToInt(data.mIsAutoArtt);
			setString += ", is_caption = " + (data.mIsCaption ? "1" : "2");
		} else if (step.equals("S11")) { // 본학습 음원 듣기 시작
			setString += ", book_start_dt = " + quote(currentDt);
			setString += ", book_audio_totalmin = " + String.valueOf(data.mAudioTotalMin);
		} else if (step.equals("S12")) { // 본학습 음원 듣기 종료
			setString += ", book_end_dt = " + quote(currentDt); // 0: 정상 10: 빠름,
																// 11: 너무빠름, 20:
																// 느림, 21 : 너무
																// 느림
			setString += ", book_check_status_end = " + String.valueOf(getBookCheckStatus(currentDt));

			// 신규문항
			insertExamDetail();
		} else if (step.equals("S21")) { // 시험 준비 시작
			setString += ", exam_ready_start_dt = " + quote(currentDt);
		} else if (step.equals("S22")) { // 시험 준비 종료
			setString += ", exam_ready_end_dt = " + quote(currentDt);
		} else if (step.equals("S31")) { // 본학습 단어 시험 시작
			setString += ", word_exam_start_dt = " + quote(currentDt);
		} else if (step.equals("S32")) { // 본학습 단어 시험 종료
			setString += ", word_exam_end_dt = " + quote(currentDt);
			setString += ", word_cnt = " + data.mWordQuestion.size();
			setString += ", word_incorrect_cnt = " + data.mWrongWordQuestion.size();

			insertWordDetail(false);
		}

		else if (step.equals("S41")) { // 본학습 문장 시험 시작
			setString += ", sentence_start_dt = " + quote(currentDt);
		} else if (step.equals("S42")) { // 본학습 문장 시험 종료
			setString += ", sentence_end_dt = " + quote(currentDt);
			setString += ", is_voice_recoginition = " + booleanToInt(data.mIsVoiceRecoginition);
			setString += ", sentence_cnt = " + data.mSentenceQuestion.size();
			setString += ", sentence_incorrect_cnt = " + data.mWrongSentenceQuestion.size();

			insertSentenceDetail(false);
		}

		else if (step.equals("S71")) { // 본학습 문장 시험 시작
			setString += ", paragraph_start_dt = " + quote(currentDt);
		} else if (step.equals("S72")) { // 본학습 문장 시험 종료
			setString += ", paragraph_end_dt = " + quote(currentDt);
			// setString += ", is_voice_recoginition = " +
			// booleanToInt(data.mIsVoiceRecoginition);
			// setString += ", sentence_cnt = " + data.mSentenceQuestion.size();
			// setString += ", sentence_incorrect_cnt = " +
			// data.mWrongSentenceQuestion.size();

			insertVanishingDetail();
		}

		else if (step.equals("S51")) { // 본학습 받아쓰기 시작
			setString += ", dictation_start_dt = " + quote(currentDt);
		} else if (step.equals("S52")) { // 본학습 받아쓰기 종료
			setString += ", dictation_end_dt = " + quote(currentDt);
		} else if (step.equals("S61")) { // 본학습 문법 동영상 시작
			setString += ", study_movie_start_dt = " + quote(currentDt);
		} else if (step.equals("S62")) { // 본학습 문법 동영상 종료
			setString += ", study_movie_end_dt = " + quote(currentDt);
		} else if (step.equals("SFN")) { // 학습 종료
			setString += ", study_finish_dt = " + quote(currentDt);
		} else if (step.equals("R01")) { // 복습 학습 시작
			// status 이외에 따로 기록하는 것 없음.
		} else if (step.equals("R11")) { // 복습 본문 음원 듣기 시작
			setString += ", review_book_start_dt = " + quote(currentDt);
		} else if (step.equals("R12")) { // 복습 본문 음원 듣기 종료
			setString += ", review_book_end_dt = " + quote(currentDt);
		} else if (step.equals("R31")) { // 복습 단어 시험 시작
			setString += ", review_word_exam_start_dt = " + quote(currentDt);
		} else if (step.equals("R32")) { // 복습 단어 시험 종료
			setString += ", review_word_exam_end_dt = " + quote(currentDt);
			setString += ", review_word_cnt = " + data.mReviewWordQuestion.size();
			setString += ", review_word_incorrect_cnt = " + data.mWrongReviewWordQuestion.size();

			insertWordDetail(true);
		} else if (step.equals("R41")) { // 복습 문장 시험 시작
			setString += ", review_sentence_start_dt = " + quote(currentDt);
		} else if (step.equals("R42")) { // 복습 문장 시험 종료
			setString += ", review_sentence_end_dt = " + quote(currentDt);
			setString += ", is_review_voice_recoginition = " + booleanToInt(data.mIsReviewVoiceRecoginition);
			setString += ", review_sentence_cnt = " + data.mReviewSentenceQuestion.size();
			setString += ", review_sentence_incorrect_cnt = " + data.mWrongReviewSentenceQuestion.size();

			insertSentenceDetail(true);
		} else if (step.equals("R71")) { // 복습 문장 시험 시작
			// setString += ", review_sentence_start_dt = " + quote(currentDt);
		} else if (step.equals("R72")) { // 복습 문장 시험 종료
			// setString += ", review_sentence_end_dt = " + quote(currentDt);
			// setString += ", is_review_voice_recoginition = " +
			// booleanToInt(data.mIsReviewVoiceRecoginition);
			// setString += ", review_sentence_cnt = " +
			// data.mReviewSentenceQuestion.size();
			// setString += ", review_sentence_incorrect_cnt = " +
			// data.mWrongReviewSentenceQuestion.size();

			// insertSentenceDetail(true);
			insertVanishingDetail();

		}

		else if (step.equals("RFN")) { // 복습 종료
			setString += ", review_finish_dt = " + quote(currentDt);
		}

		setString += ", crud = 'C'";

		query = String.format(DatabaseDefine.UPDATE_SBM_STUDY_RESULT, setString, customerNo, productNo, quote(studyUnitCode), reStudyCnt);
		// Log.k("wusi12", "query : "+ query);
		try {
			if (!step.equals("R71") || !step.equals("R72")) {
				execSQL(query);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateStudyResultStatus Error : " + e);
		}
	}

	/**
	 * 오디오 학습 속도를 구함(서버에서 구하는 내용이므로 이 동작은 필요없음)
	 * 
	 * @param bookEndDt
	 *            : 오디오 학습 종료 시간
	 * @return 학습 상태 0: 정상 10: 빠름, 11: 너무빠름, 20: 느림, 21 : 너무 느림
	 */
	public int getBookCheckStatus(String bookEndDt) {
		// 0: 정상 10: 빠름, 11: 너무빠름, 20: 느림, 21 : 너무 느림
		StudyData data = StudyData.getInstance();
		String query = String.format(DatabaseDefine.GET_BOOK_AUDIO_TOTAL_MIN_FROM_SBM_STUDY_RESULT, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);
		Cursor cursor = null;
		String startDt = "";
		int bookTotalMin = 0;

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				startDt = cursor.getString(0);
				bookTotalMin = cursor.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistStudyResult Error : " + e);
			return 0;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return StudyDataUtil.getAudioStudySpeed(startDt, bookEndDt, bookTotalMin);
	}

	/**
	 * 단어 시험 결과 기록
	 * 
	 * @param isReview
	 *            : 이전학습 복습인지 여부
	 */
	public void insertWordDetail(boolean isReview) {
		StudyData data = StudyData.getInstance();
		String currentDt = CommonUtil.getCurrentDateTime();

		try {
			if (isReview) {
				for (int i = 0; i < data.mReviewWordResult.size(); i++) {
					WordResult result = data.mReviewWordResult.get(i);
					String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_WORD_DETAIL, data.mCustomerNo, data.mReviewProductNo, quote(data.mReviewStudyUnitCode), data.mReviewReStudyCnt, 2, result.mQuestionOrder, booleanToInt(result.mIsCorrect), quote(String.valueOf(result.mCorrectAnswer)), result.mCustomerAnswer == 0 ? "null" : quote(String.valueOf(result.mCustomerAnswer)), quote(currentDt), result.mWordQuestionNo, quote("C"), data.mStudyResultNo, data.mStudyResultNo);
					// 숲우영
					// String query =
					// String.format(DatabaseDefine.INSERT_SBM_STUDY_WORD_DETAIL,
					// data.mCustomerNo, data.mReviewProductNo,
					// quote(data.mReviewStudyUnitCode), data.mReviewReStudyCnt,
					// 2,
					// result.mQuestionOrder, booleanToInt(result.mIsCorrect),
					// quote(String.valueOf(result.mCorrectAnswer)),
					// result.mCustomerAnswer==
					// 0?"null":quote(String.valueOf(result.mCustomerAnswer)),
					// quote(currentDt), result.mWordQuestionNo, data.mClassNo,
					// quote("C"));
					// mCustomerAnswer 0이면 5초 동안 선택을 하지 않은 것이며 db에는 null값을 넣어줌.
					execSQL(query);
				}
			} else {
				for (int i = 0; i < data.mWordResult.size(); i++) {
					WordResult result = data.mWordResult.get(i);
					String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_WORD_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt, 1, result.mQuestionOrder, booleanToInt(result.mIsCorrect), quote(String.valueOf(result.mCorrectAnswer)), result.mCustomerAnswer == 0 ? "null" : quote(String.valueOf(result.mCustomerAnswer)), quote(currentDt), result.mWordQuestionNo, quote("C"), data.mStudyResultNo, data.mStudyResultNo);
					// 숲우영
					// String query =
					// String.format(DatabaseDefine.INSERT_SBM_STUDY_WORD_DETAIL,
					// data.mCustomerNo, data.mProductNo,
					// quote(data.mStudyUnitCode), data.mReStudyCnt, 1,
					// result.mQuestionOrder, booleanToInt(result.mIsCorrect),
					// quote(String.valueOf(result.mCorrectAnswer)),
					// result.mCustomerAnswer==
					// 0?"null":quote(String.valueOf(result.mCustomerAnswer)),
					// quote(currentDt), result.mWordQuestionNo, data.mClassNo,
					// quote("C"));
					execSQL(query);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertWordDetail Error : " + e);
		}
	}

	/**
	 * 문장 시험 결과 기록
	 * 
	 * @param isReview
	 *            : 이전 학습 복습 인지 여부
	 */
	public void insertSentenceDetail(boolean isReview) {
		StudyData data = StudyData.getInstance();
		String currentDt = CommonUtil.getCurrentDateTime();

		try {
			if (isReview) {
				for (int i = 0; i < data.mReviewSentenceResult.size(); i++) {
					SentenceResult result = data.mReviewSentenceResult.get(i);
					String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_SETENCE_DETAIL, data.mCustomerNo, data.mReviewProductNo, quote(data.mReviewStudyUnitCode), data.mReviewReStudyCnt, 2, result.mQuestionOrder, result.mIsCorrect, quote(result.mCorrectAnswer.replace("\"", "\"\"")), quote(result.mCustomerAnswer.replace("\"", "\"\"")), 0, result.mSentenceQuestionNo, quote(currentDt), quote("C"), data.mStudyResultNo);
					// 숲우영
					// String query =
					// String.format(DatabaseDefine.INSERT_SBM_STUDY_SETENCE_DETAIL,
					// data.mCustomerNo, data.mReviewProductNo,
					// quote(data.mReviewStudyUnitCode), data.mReviewReStudyCnt,u
					// 2,
					// result.mQuestionOrder, result.mIsCorrect,
					// quote(result.mCorrectAnswer.replace("\"", "\"\"")),
					// quote(result.mCustomerAnswer.replace("\"", "\"\"")),
					// 0, result.mSentenceQuestionNo, quote(currentDt),
					// data.mClassNo, quote("C"));

					execSQL(query);
				}
			} else {
				for (int i = 0; i < data.mSentenceResult.size(); i++) {
					SentenceResult result = data.mSentenceResult.get(i);
					String mAnswer = "";
					if (result.mCustomerAnswer != null) {
						mAnswer = result.mCustomerAnswer;
					}
					String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_SETENCE_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt, 1, result.mQuestionOrder, result.mIsCorrect, quote(result.mCorrectAnswer.replace("\"", "\"\"")), quote(mAnswer.replace("\"", "\"\"")), 0, result.mSentenceQuestionNo, quote(currentDt), quote("C"), data.mStudyResultNo);
					// 숲우영
					// String query =
					// String.format(DatabaseDefine.INSERT_SBM_STUDY_SETENCE_DETAIL,
					// data.mCustomerNo, data.mProductNo,
					// quote(data.mStudyUnitCode), data.mReStudyCnt, 1,
					// result.mQuestionOrder, result.mIsCorrect,
					// quote(result.mCorrectAnswer.replace("\"", "\"\"")),
					// quote(result.mCustomerAnswer.replace("\"", "\"\"")),
					// 0, result.mSentenceQuestionNo, quote(currentDt),
					// data.mClassNo, quote("C"));

					execSQL(query);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertSentenceDetail Error : " + e);
		}
	}

	// 신규문항
	/**
	 * popup 시험 결과 기록
	 */
	public void insertExamDetail() {
		StudyData data = StudyData.getInstance();
		String currentDt = CommonUtil.getCurrentDateTime();

		try {
			for (int i = 0; i < data.mExamResult.size(); i++) {
				ExamResult result = data.mExamResult.get(i);
				String query = "";

				// if (result.mTagType == ServiceCommon.TAG_TYPE_TOUCH_PLAY ||
				// result.mTagType == ServiceCommon.TAG_TYPE_ROLE_PLAY) {
				if (result.mMaxRecTime != 0) {
					query = String.format(DatabaseDefine.INSERT_SBM_STUDY_EXAM_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt, 1, result.mStudyOrder, result.mTagType, result.mIsCorrect, 0, quote(currentDt), result.mQuestionOrder, quote(result.mQuestionNo), quote(result.mCustomerAnswer), data.mStudyResultNo);
				} else {
					query = String.format(DatabaseDefine.INSERT_SBM_STUDY_EXAM_CRUD_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt, 1, result.mStudyOrder, result.mTagType, result.mIsCorrect, 0, quote(currentDt), result.mQuestionOrder, quote(result.mQuestionNo), quote(result.mCustomerAnswer), quote("C"), data.mStudyResultNo);
				}

				execSQL(query);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertExamDetail Error : " + e);
		}
	}

	/**
	 * 문단 시험 결과 기록
	 */
	public void insertVanishingDetail() {
		StudyData data = StudyData.getInstance();
		String currentDt = CommonUtil.getCurrentDateTime();

		try {
			for (int i = 0; i < data.mVanishingResult.size(); i++) {
				VanishingResult result = data.mVanishingResult.get(i);
				String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_VANISHING_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt, 1, result.mQuestionOrder, result.mIsCorrect, quote(result.mCorrectAnswer.replace("\"", "\"\"")), quote(result.mCustomerAnswer.replace("\"", "\"\"")), 0, result.mRetryCnt, result.mVanishingQuestionNo, quote(currentDt), quote("C"), data.mStudyResultNo);
				// Log.k("wusi12", "insertVanishingDetail query: " + query);
				execSQL(query);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertVanishingDetail Error : " + e);
		}
	}

	/**
	 * 학습 결과가 있는지 여부
	 * 
	 * @param customerNo
	 * @param productNo
	 * @param studyUnitCode
	 * @param reStudyCnt
	 * @return
	 */
	public boolean isExistStudyResult(int customerNo, int productNo, String studyUnitCode, int reStudyCnt) {
		Cursor cursor = null;

		String query = String.format(DatabaseDefine.GET_COUNT_SBM_STUDY_RESULT, customerNo, productNo, quote(studyUnitCode), reStudyCnt);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				if (cursor.getInt(0) != 0) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistStudyResult Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return false;
	}

	/**
	 * 녹음 파일 업로드시 호출
	 * 
	 * @param localFileName
	 *            : 업로드한 파일 이름
	 */
	public void updateRecordFileUpload(String localFileName) {
		if (!localFileName.contains("R1")) {
			if (localFileName.contains("SA") || localFileName.contains("RA")) {
				insertRecordDetail(localFileName);
			} else {
				if (localFileName.contains("VC")) {
					updateVanishingDetailIsUpload(localFileName);
				} else if (localFileName.contains("PU")) {
					updateExamDetailIsUpload(localFileName);
				} else {
					updateSentenceDetailIsUpload(localFileName);
				}

			}
		}
	}

	/**
	 * 오디오 학습 녹음 파일 업로드시 sbm_study_record_detail를 insert함
	 * 20130830_20004090_39692_01A_1_SA_1_G1_001122.mp3
	 * 
	 * @param localFileName
	 *            : 업로드한 파일 이름
	 */
	public void insertRecordDetail(String localFileName) {
		String query = "";
		String fileName = localFileName.replaceAll(".wav", "");
		fileName = fileName.replaceAll(".mp3", "");

		//tring[] token = fileName.split("_");
		String[] token = fileName.replace(ServiceCommon.CONTENTS_PATH, "").split("_");
		if (token.length != 10) {
			Log.e(TAG, "insertRecordDetail Error : fileName invalid " + fileName);
			return;
		}

		query = String.format(DatabaseDefine.INSERT_SBM_STUDY_RECORD_DETAIL_, token[1], token[2], quote(token[3]), token[4], token[5].contains("SA") ? 1 : 2, token[6], quote(token[7]), quote(token[8]));
		try {
			Log.i(TAG, "insertRecordDetail localFileName : " + localFileName + " query : " + query);
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertRecordDetail Error : " + e);
		}
	}

	/**
	 * 오디오 병합관련 녹음될때 저장 20130830_20004090_39692_01A_1_SA_1_G1_001122.mp3
	 * 
	 * @param localFileName
	 *            : 업로드한 파일 이름
	 */
	public void insertRecordDetailC(String localFileName, String rec_type, int start_merge_time, int end_merge_time, int rec_len, int rep_cnt, int is_merge, int book_detail_id, int student_book_id, int end_tag_time, boolean isRfile) {
		int i = isRfile ? 1 : 1000;
		String query = "";
		String path = localFileName;
		String fileName = localFileName.replaceAll(".wav", "");
		fileName = fileName.replaceAll(".mp3", "");

		String mStart_merge_time = String.valueOf(start_merge_time / i);
		String mEnd_merge_time = String.valueOf(end_merge_time / i);
		String mRec_len = String.valueOf(rec_len / i);
		String mEnd_tag_time = String.valueOf(end_tag_time / i);

		//String[] token = temp.split("_");
		String[] token = fileName.replace(ServiceCommon.CONTENTS_PATH, "").split("_");
		if (token.length != 10) {
			Log.e(TAG, "insertRecordDetail Error : fileName invalid " + fileName);
			return;
		}

		query = String.format(DatabaseDefine.INSERT_SBM_STUDY_RECORD_DETAIL_C, token[1], token[2], quote(token[3]), token[4], token[5].contains("SA") ? 1 : 2, token[6], quote(token[7]), quote(token[8]), token[9], is_merge, quote(rec_type), mStart_merge_time, mEnd_merge_time, mRec_len, rep_cnt, quote(path), book_detail_id, student_book_id, mEnd_tag_time);
		try {
			Log.i(TAG, "insertRecordDetail localFileName : " + localFileName + " query : " + query);
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertRecordDetail Error : " + e);
		}
	}

	/**
	 * 문장 시험 녹음 파일 업로드시 sbm_study_sentence_detail의 isupload를 업데이트함.
	 * 20130901_20004090_39692_02A_1_RS_3.wav
	 * 
	 * @param localFileName
	 *            : 업로드한 파일 이름
	 */
	public void updateSentenceDetailIsUpload(String localFileName) {
		String query = "";
		String fileName = localFileName.replaceAll(".wav", "");
		fileName = fileName.replaceAll(".mp3", "");

		//String[] token = fileName.split("_");
		String[] token = fileName.replace(ServiceCommon.CONTENTS_PATH, "").split("_");
		if (token.length != 7) {
			Log.e(TAG, "updateSentenceDetailUpload Error : fileName invalid " + fileName);
			return;
		}

		query = String.format(DatabaseDefine.UPDATE_SBM_STUDY_SENTENCE_DETAIL_SET_IS_UPLOAD, 1, quote(CommonUtil.getCurrentDateTime()), token[1], token[2], quote(token[3]), token[4], token[5].contains("SS") ? 1 : 2, token[6]);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateSentenceDetailIsUpload Error : " + e);
		}
	}

	/**
	 * 문단 시험 녹음 파일 업로드시 sbm_study_pa_sentence_detail의 isupload를 업데이트함.
	 * 20130901_20004090_39692_02A_1_RS_3.wav
	 * 
	 * @param localFileName
	 *            : 업로드한 파일 이름
	 */
	public void updateVanishingDetailIsUpload(String localFileName) {
		String query = "";
		String fileName = localFileName.replaceAll(".wav", "");
		fileName = fileName.replaceAll(".mp3", "");

		//String[] token = fileName.split("_");
		String[] token = fileName.replace(ServiceCommon.CONTENTS_PATH, "").split("_");
		if (token.length != 7) {
			Log.e(TAG, "updateVanishingDetailUpload Error : fileName invalid " + fileName);
			return;
		}

		query = String.format(DatabaseDefine.UPDATE_SBM_STUDY_VANISHING_DETAIL_SET_IS_UPLOAD, 1, quote(CommonUtil.getCurrentDateTime()), token[1], token[2], quote(token[3]), token[4], token[5].contains("VC") ? 1 : 2, token[6]);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateVanishingDetailIsUpload Error : " + e);
		}
	}

	// 신규문항
	/**
	 * popup 시험 녹음 파일 업로드시 sbm_study_exam_detail의 isupload를 업데이트함.
	 * 20150402_20003431_48086_01B_1000_PU_1_yoons20150120043102_1.mp3
	 * 
	 * @param localFileName
	 *            : 업로드한 파일 이름
	 */
	public void updateExamDetailIsUpload(String localFileName) {
		String query = "";
		String fileName = localFileName.replaceAll(".mp3", "");

		//String[] token = fileName.split("_");
		String[] token = fileName.replace(ServiceCommon.CONTENTS_PATH, "").split("_");
		if (token.length != 10) {
			Log.e(TAG, "updateExamDetailUpload Error : fileName invalid " + fileName);
			return;
		}

		query = String.format(DatabaseDefine.UPDATE_SBM_STUDY_EXAM_DETAIL_SET_IS_UPLOAD, 1, quote(CommonUtil.getCurrentDateTime()), token[1], token[2], quote(token[3]), token[4], token[5].contains("PU") ? 1 : 2, token[6], quote(token[7]), token[8]);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateExamDetailIsUpload Error : " + e);
		}
	}

	/**
	 * sbm_customer의 is_studyguide 값을 업데이트함.
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param isStudyGuide
	 *            : 학습 가이드 설정 여부
	 */
	public void updateLoginCustomerIsStudyGuide(int customerNo, boolean isStudyGuide) {

		String query = String.format(DatabaseDefine.UPDATE_SBM_CUSTOMER_SET_IS_STUDY_GUIDE, booleanToInt(isStudyGuide), customerNo);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateLoginCustomerIsStudyGuide Error : " + e);
		}
	}

	/**
	 * db상에 로그인 했던 회원 수를 구해옴
	 * 
	 * @return db상의 회원 수
	 */
	public int selectLoginCustomerCount() {
		Cursor cursor = null;
		int customerCount = 0;

		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_SBM_CUSTOMER_GET_CNT, null);

			if (cursor != null && cursor.moveToFirst()) {
				customerCount = cursor.getInt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectLoginCustomerCount Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return customerCount;
	}

	/**
	 * Customer테이블 조회
	 * 
	 * @return customer정보
	 */
	public ArrayList<LoginCustomer> selectLoginCustomer() {
		Cursor cursor = null;
		ArrayList<LoginCustomer> list = new ArrayList<LoginCustomer>();

		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_LOGING_CUSTOMER, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					LoginCustomer customer = new LoginCustomer();
					customer.mCustomerNo = cursor.getInt(0);
					customer.mUserId = cursor.getString(1);
					customer.mUserPassword = cursor.getString(2);
					customer.mCustomerName = cursor.getString(3);
					customer.mLastLogin = cursor.getString(4);
					customer.mIsStudyGuide = intToBoolean(cursor.getInt(5));
					list.add(customer);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectLoginCustomer Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return list;
	}

	/**
	 * Customer회원 조회
	 * 
	 * @return customer정보
	 */
	public boolean selectCustomerGetCntWhere(int customerId) {
		Cursor cursor = null;

		String query = String.format(DatabaseDefine.SELECT_SBM_CUSTOMER_GET_CNT_WHERE, customerId);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				if (cursor.getInt(0) != 0) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectLoginCustomer Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return false;
	}

	/**
	 * 오늘 다운로드 받아야할 차시 정보 조회
	 * 
	 * @param customerId
	 *            : customerId
	 * @return 차시 정보 리스트
	 */
	public ArrayList<DownloadStudyUnit> selectTodayDownloadStudyUnit(int customerId) {
		Cursor cursor = null;
		ArrayList<DownloadStudyUnit> studyUnitList = new ArrayList<DownloadStudyUnit>();
		String query = String.format(DatabaseDefine.SELECT_DOWNLOAD_STUDY_UNIT, customerId, customerId, customerId);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					DownloadStudyUnit studyUnit = new DownloadStudyUnit();
					studyUnit.mProductNo = cursor.getInt(0);
					studyUnit.mSeriesNo = cursor.getInt(1);
					studyUnit.mBookNo = cursor.getInt(2);
					studyUnit.mStudyUnitNo = cursor.getInt(3);
					studyUnit.mStudyUnitCode = cursor.getString(4);
					studyUnit.mDictationCnt = cursor.getInt(5);
					studyUnit.mIsCdnBFile = intToBoolean(cursor.getInt(6));
					studyUnit.mAniTalkCnt = cursor.getInt(7);
					studyUnit.mIsMovie = intToBoolean(cursor.getInt(8));
					studyUnit.mIsPriority = intToBoolean(cursor.getInt(9));
					studyUnit.mIsCdnVideoFile = intToBoolean(cursor.getInt(10));
					studyUnit.mWordQuestionList = selectWordQuestionWordList(studyUnit.mProductNo, studyUnit.mStudyUnitCode);
					studyUnit.mSentenceQuestionCnt = selectSentenceQuestionCount(studyUnit.mProductNo, studyUnit.mStudyUnitCode);
					studyUnit.mVanishingQuestionCnt = selectVanishingQuestionCount(studyUnit.mProductNo, studyUnit.mStudyUnitCode);
					studyUnit.mIsCdnAudio = intToBoolean(cursor.getInt(11));
					studyUnit.mIsCdnZip = intToBoolean(cursor.getInt(12));
					studyUnitList.add(studyUnit);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectTodayDownloadStudyUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return studyUnitList;
	}

	/**
	 * 오늘 학습할 수 있는 차시 조회
	 * 
	 * @param customerId
	 *            : customerId
	 * @param isToday
	 *            : 오늘의 학습인지 오늘 이후 7일인지 여부
	 * @return 차시 정보
	 */
	public ArrayList<TodayStudyUnit> selectTodayStudyUnit(int customerId, boolean isToday) {
		Cursor cursor = null;
		ArrayList<TodayStudyUnit> studyUnitList = new ArrayList<TodayStudyUnit>();
		String query = "";

		if (isToday) {
			query = String.format(DatabaseDefine.SELECT_TODAY_STUDY_UNIT, customerId);
		} else {
			query = String.format(DatabaseDefine.SELECT_NEXT_STUDY_UNIT, customerId);
		}

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					TodayStudyUnit studyUnit = new TodayStudyUnit();
					studyUnit.mProductNo = cursor.getInt(0);
					studyUnit.mSeriesNo = cursor.getInt(1);
					studyUnit.mSeriesName = cursor.getString(2);
					studyUnit.mBookNo = cursor.getInt(3);
					studyUnit.mStudyUnitNo = cursor.getInt(4);
					studyUnit.mStudyUnitCode = cursor.getString(5);
					studyUnit.mStudyStatus = cursor.getString(6);
					studyUnit.mReStudyCnt = cursor.getInt(7);
					studyUnit.mStudyPlanYmd = cursor.getString(8);
					studyUnit.mIsCdnBFile = intToBoolean(cursor.getInt(9));
					studyUnit.mIsMovie = intToBoolean(cursor.getInt(10));
					studyUnit.mBookCheckDt = cursor.getString(11);
					studyUnit.mRecordFileYmd = cursor.getString(12);
					studyUnit.mIsCdnVideoFile = intToBoolean(cursor.getInt(13));
					studyUnit.mIsCdnAudio = intToBoolean(cursor.getInt(14));
					studyUnit.mIsCdnZip = intToBoolean(cursor.getInt(15));
					// 숲우영
					// studyUnit.mAgencyCode = cursor.getInt(13);
					// studyUnit.mClassNo = cursor.getInt(14);
					// studyUnit.mSiteKind = cursor.getInt(15);
					// studyUnit.mIsCdnVideoFile =
					// intToBoolean(cursor.getInt(16));
					studyUnitList.add(studyUnit);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectTodayStudyUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return studyUnitList;
	}

	/**
	 * 선택한 차시 정보
	 * 
	 * @param customerId
	 *            : customerId
	 * @return 차시 정보
	 */
	public TodayStudyUnit selectNewTodayStudyUnit(int customerId, int itemcode, String studyUnit) {
		Cursor cursor = null;
		String query = "";
		TodayStudyUnit studyUnit1 = new TodayStudyUnit();
		query = String.format(DatabaseDefine.SELECT_NEW_TODAY_STUDY_UNIT, customerId, itemcode, quote(studyUnit));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					studyUnit1.mProductNo = cursor.getInt(0);
					studyUnit1.mSeriesNo = cursor.getInt(1);
					studyUnit1.mSeriesName = cursor.getString(2);
					studyUnit1.mBookNo = cursor.getInt(3);
					studyUnit1.mStudyUnitNo = cursor.getInt(4);
					studyUnit1.mStudyUnitCode = cursor.getString(5);
					studyUnit1.mStudyStatus = cursor.getString(6);
					studyUnit1.mReStudyCnt = cursor.getInt(7);
					studyUnit1.mStudyPlanYmd = cursor.getString(8);
					studyUnit1.mIsCdnBFile = intToBoolean(cursor.getInt(9));
					studyUnit1.mIsMovie = intToBoolean(cursor.getInt(10));
					studyUnit1.mBookCheckDt = cursor.getString(11);
					studyUnit1.mRecordFileYmd = cursor.getString(12);
					studyUnit1.mIsCdnVideoFile = intToBoolean(cursor.getInt(13));
					studyUnit1.mIsCdnAudio = intToBoolean(cursor.getInt(14));
					studyUnit1.mIsCdnZip = intToBoolean(cursor.getInt(15));
					studyUnit1.mBookDetailId = cursor.getInt(16);
					studyUnit1.mStudentBookId = cursor.getInt(17);
					studyUnit1.mStudyResultNo = cursor.getInt(18);
					studyUnit1.mReviewStudyStatus = cursor.getString(19);
					studyUnit1.mIsNewDictation = stringToBoolean(cursor.getString(20));
					studyUnit1.mProductType = cursor.getInt(21);
					studyUnit1.mRealBookNo = cursor.getInt(22);
					// 숲우영
					// studyUnit.mAgencyCode = cursor.getInt(13);
					// studyUnit.mClassNo = cursor.getInt(14);
					// studyUnit.mSiteKind = cursor.getInt(15);
					// studyUnit.mIsCdnVideoFile =
					// intToBoolean(cursor.getInt(16));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectTodayStudyUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return studyUnit1;
	}

	/**
	 * 학습 완료 정보중 해당 차시의 max 차시 조회 학습 계획에 있는 학습 결과에 없을때. 1~5까지 학습을 하였으나, result에는
	 * 345만있을 때 1,2는 무시해야하기 때문에 가장 높은 차시를 구해놓음.
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @return max차시 정보
	 */
	public ArrayList<StudyResultUnitMax> selectStudyResultMaxStudyUnitNo(int customerNo) {
		Cursor cursor = null;
		ArrayList<StudyResultUnitMax> studyUnitList = new ArrayList<StudyResultUnitMax>();
		String query = String.format(DatabaseDefine.SELECT_STUDY_RESULT_MAX_STUDY_UNIT_NO, customerNo);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					StudyResultUnitMax studyUnitMax = new StudyResultUnitMax();
					studyUnitMax.mProductNo = cursor.getInt(0);
					studyUnitMax.mStudyUnitNo = cursor.getInt(1);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectStudyResultMaxStudyUnitNo Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return studyUnitList;
	}

	/**
	 * 이전 학습 복습을 하기 위한 차시 조회
	 * 
	 * @param customerId
	 *            : customerId
	 * @return 차시 정보
	 */
	public StudyUnit selectRecentNoReviewUnit(int customerId) {
		Cursor cursor = null;
		String query = String.format(DatabaseDefine.SELECT_RECENT_NO_REVIEW_UNIT, customerId, customerId);
		StudyUnit unit = new StudyUnit();
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					unit.mProductNo = cursor.getInt(0);
					unit.mStudyUnitCode = cursor.getString(1);
					unit.mStudyUnitNo = cursor.getInt(2);
					unit.mReStudyCnt = cursor.getInt(3);
					unit.mSeriesNo = cursor.getInt(4);
					unit.mSeriesName = cursor.getString(5);
					unit.mBookNo = cursor.getInt(6);
					unit.mIsCdnBFile = intToBoolean(cursor.getInt(7));
					unit.mIsMovie = intToBoolean(cursor.getInt(8));
					unit.mStudyPlanYmd = cursor.getString(9);
					unit.mIsCdnVideoFile = intToBoolean(cursor.getInt(10));
					unit.mStudyResultNo = cursor.getInt(11);
					unit.mRealBookNo = cursor.getInt(12);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectRecentNoReview Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return unit;
	}

	/**
	 * contents test를 위해 모든 교재 정보 조회
	 * 
	 * @return 모든 교재 정보 리스트
	 */
	public ArrayList<ReviewStudyBook> selectReviewUnit() {
		ArrayList<ReviewStudyBook> reviewList = new ArrayList<ReviewStudyBook>();
		Cursor cursor = null;

		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_ALL_WYE_BEFLY_STUDY_UNIT, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					boolean isExistProductNo = false;

					ReviewStudyBook book = new ReviewStudyBook();
					book.mProductNo = cursor.getInt(0);
					book.mSeriesNo = cursor.getInt(3);
					book.mSeriesName = cursor.getString(4);
					book.mBookNo = cursor.getInt(5);

					for (int i = 0; i < reviewList.size(); i++) {
						ReviewStudyBook book2 = reviewList.get(i);
						if (book.mProductNo == book2.mProductNo) {
							book = book2;
							isExistProductNo = true;
							break;
						}
					}

					ReviewStudyBookUnit unit = new ReviewStudyBookUnit();
					unit.mStudyUnitCode = cursor.getString(1);
					unit.mStudyUnitNo = cursor.getInt(2);

					book.mUnitList.add(unit);

					if (!isExistProductNo) {
						reviewList.add(book);
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectReviewUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return reviewList;
	}

	/**
	 * 단어 시험 문제를 조회
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @param isIncorrectQuestion
	 *            true : 틀린 문제를 조회, false : 모든 문제 조회
	 * @return 단어 시험 정보
	 */
	public ArrayList<WordQuestion> selectWordQuestion(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, boolean isIncorrectQuestion) {
		Cursor cursor = null;
		String query = "";
		if (isIncorrectQuestion) {
			query = String.format(DatabaseDefine.SELECT_INCORRECT_WORD_QUESTION, customerNo, productNo, quote(studyUnitCode), reStudyCnt);
		} else {
			query = String.format(DatabaseDefine.SELECT_WORD_QUESTION, productNo, quote(studyUnitCode));
		}

		ArrayList<WordQuestion> questionList = new ArrayList<WordQuestion>();

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					WordQuestion question = new WordQuestion();
					question.mWordQuestionNo = cursor.getInt(0);
					question.mQuestionOrder = cursor.getInt(1);
					question.mQuestion = cursor.getString(2);
					question.mMeaning = cursor.getString(3);
					question.mAnswer = Integer.valueOf(cursor.getString(4));
					question.mDistractorList.add(cursor.getString(5));
					question.mDistractorList.add(cursor.getString(6));
					question.mDistractorList.add(cursor.getString(7));
					question.mDistractorList.add(cursor.getString(8));

					questionList.add(question);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectWordQuestion Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return questionList;
	}

	/**
	 * 문장 시험 문제를 조회
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @param isIncorrectQuestion
	 *            true : 틀린 문제를 조회, false : 모든 문제 조회
	 * @return 문장 시험 정보
	 */
	public ArrayList<SentenceQuestion> selectSentenceQuestion(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, boolean isIncorrectQuestion) {
		Cursor cursor = null;
		String query = "";
		if (isIncorrectQuestion) {
			query = String.format(DatabaseDefine.SELECT_INCORRECT_SENTENCE_QUESTION, customerNo, productNo, quote(studyUnitCode), reStudyCnt);
		} else {
			query = String.format(DatabaseDefine.SELECT_SENTENCE_QUESTION, productNo, quote(studyUnitCode));
		}

		ArrayList<SentenceQuestion> questionList = new ArrayList<SentenceQuestion>();

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					SentenceQuestion question = new SentenceQuestion();
					question.mSentenceQuestionNo = cursor.getInt(0);
					question.mQuestionOrder = cursor.getInt(1);
					question.mSentence = cursor.getString(2).replaceAll("`", "'");
					question.mTextSentence = cursor.getString(3).replaceAll("`", "'");

					questionList.add(question);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectSentenceQuestion Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return questionList;
	}

	/**
	 * 문장 시험 문제를 조회
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @return 문장 시험 정보
	 */
	public ArrayList<VanishingQuestion> selectVanishingQuestion(int customerNo, int productNo, String studyUnitCode, int reStudyCnt) {
		Cursor cursor = null;
		String query = "";

		query = String.format(DatabaseDefine.SELECT_VANISHING_QUESTION, productNo, quote(studyUnitCode));

		ArrayList<VanishingQuestion> questionList = new ArrayList<VanishingQuestion>();

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					VanishingQuestion question = new VanishingQuestion();
					question.mVanishingQuestionNo = cursor.getInt(0);
					question.mQuestionOrder = cursor.getInt(1);
					question.mSentence = cursor.getString(2).replaceAll("`", "'").replaceAll("</s>", "\n");
					question.mTextSentence = cursor.getString(3).replaceAll("`", "'");

					questionList.add(question);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectSentenceQuestion Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return questionList;
	}

	/**
	 * 단어 시험의 word 리스트 조회
	 * 
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return word 리스트
	 */
	public ArrayList<String> selectWordQuestionWordList(int productNo, String studyUnitCode) {
		Cursor cursor = null;
		ArrayList<String> wordList = new ArrayList<String>();

		String query = String.format(DatabaseDefine.SELECT_YFS_SM_WORD_QUESTION_LIST, productNo, quote(studyUnitCode));

		// 10차시 관련
		/**
		 * 아래 R이고 길이가 6이하일때로 한건 기존소스는 교실전용 소스여서 04R(스마트중학영어)밖에 없어서 넣으신거같은데 숲은
		 * Final Reivew,Review1등 차시가 R이들어간게 많아서 어쩔수 없이 하드코딩..
		 */
		/*if ((studyUnitCode.contains("R") && studyUnitCode.length() < 6)) {
			query = String.format(DatabaseDefine.SELECT_YFS_SM_WORD_MEANING_LIST, productNo, quote(studyUnitCode));
		}*/

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					String quesiton = cursor.getString(0);
					String meaning = cursor.getString(1);
					if (CommonUtil.isKorean(quesiton) || CommonUtil.isNum(quesiton)) {
						wordList.add(meaning);
					} else {
						wordList.add(quesiton);
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectWordQuestion Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return wordList;
	}

	/**
	 * 문장시험 개수를 조회
	 * 
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 문장시험 개수
	 */
	public int selectSentenceQuestionCount(int productNo, String studyUnitCode) {
		Cursor cursor = null;

		int count = 0;
		String query = String.format(DatabaseDefine.GET_CNT_YFS_SM_SENTENCE_QUESTION, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					count = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectSentenceQuestionCount Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return count;
	}

	/**
	 * 문단연습 개수를 조회
	 * 
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 문장시험 개수
	 */
	public int selectVanishingQuestionCount(int productNo, String studyUnitCode) {
		Cursor cursor = null;

		int count = 0;
		String query = String.format(DatabaseDefine.GET_CNT_YFS_SM_VANISHING_QUESTION, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					count = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectSentenceQuestionCount Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return count;
	}

	/**
	 * 받아쓰기 개수를 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 받아쓰기 개수
	 */
	public int selectDictationCount(int productNo, String studyUnitCode) {
		Cursor cursor = null;
		int count = 0;
		String query = String.format(DatabaseDefine.GET_CNT_STUDY_UNIT_DICTAION, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					count = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectDictationCount Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return count;
	}

	/**
	 * 애니톡 개수를 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 애니톡 개수
	 */
	public int selectAniTalkCount(int productNo, String studyUnitCode) {
		Cursor cursor = null;

		int count = 0;
		String query = String.format(DatabaseDefine.GET_CNT_STUDY_UNIT_DEBASE, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					count = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectAniTalkCount Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return count;
	}

	/**
	 * 문법 동영상 존재 여부 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 문법 동영상 존재 여부
	 */
	public boolean isExistMovie(int productNo, String studyUnitCode) {
		Cursor cursor = null;

		boolean isExist = false;

		String query = String.format(DatabaseDefine.GET_CNT_STUDY_UNIT_MOVIE, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					isExist = intToBoolean(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistMovie Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return isExist;
	}

	/**
	 * 학습음원 존재 여부 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 학습음원 존재 여부
	 */
	public boolean isExistAudio(int productNo, String studyUnitCode) {
		Cursor cursor = null;

		boolean isExist = false;

		String query = String.format(DatabaseDefine.GET_STUDY_UNIT_IS_CDN_AUDIO, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					isExist = intToBoolean(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistAudio Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return isExist;
	}

	/**
	 * popup학습 존재 여부 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return popup학습 존재 여부
	 */
	public boolean isExistZip(int productNo, String studyUnitCode) {
		Cursor cursor = null;

		boolean isExist = false;

		String query = String.format(DatabaseDefine.GET_STUDY_UNIT_IS_CDN_ZIP, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					isExist = intToBoolean(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistZip Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return isExist;
	}

	/**
	 * 이전 학습 본문 존재 여부 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 이전 학습 본문 존재 여부
	 */
	public boolean isExistBody(int productNo, String studyUnitCode) {
		Cursor cursor = null;
		boolean isExist = false;

		String query = String.format(DatabaseDefine.GET_STUDY_UNIT_IS_CDN, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					isExist = intToBoolean(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistBody Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return isExist;
	}

	/**
	 * 이전 학습 동영상 존재 여부 조회
	 * 
	 * @param productNo
	 *            : 교재 코드
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @return 이전 학습 본문 존재 여부
	 */
	public boolean isExistBodyVideo(int productNo, String studyUnitCode) {
		Cursor cursor = null;
		boolean isExist = false;

		String query = String.format(DatabaseDefine.GET_STUDY_UNIT_IS_CDN_MOVIE, productNo, quote(studyUnitCode));

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					isExist = intToBoolean(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExistBody Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return isExist;
	}

	/**
	 * 회원의 학습 옵션을 조회
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @return 옵션
	 */
	public StudyOption selectStudyOption(int customerNo) {
		Cursor cursor = null;

		String query = String.format(DatabaseDefine.SELECT_SBM_CUSTOMER_GET_STUDY_OPTION, customerNo);
		StudyOption option = new StudyOption();

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					option.mIsAutoArtt = intToBoolean(cursor.getInt(0));
					option.mIsCaption = cursor.getInt(1) == 1 ? true : false; // 1
																				// :
																				// 숙련자
																				// 하
																				// 모드
																				// 자막이
																				// 보임,
																				// 2
																				// :
																				// 숙련자
																				// 상
																				// 모드
																				// 자막이
																				// 안보임.
					option.mIsFastRewind = intToBoolean(cursor.getInt(2));
					option.mIsStudyGuide = intToBoolean(cursor.getInt(3));
					option.mIsDictation = intToBoolean(cursor.getInt(4));
					option.mMiddleRepeatCnt = cursor.getInt(5);
					option.mElementarytRepeatCnt = cursor.getInt(6);
					option.mIsReviewBook = intToBoolean(cursor.getInt(7));
					option.mRepeatCnt = cursor.getInt(8);
					option.mIsDicPirvate = stringToBoolean(cursor.getString(9));
					option.mIsDicAnswer = stringToBoolean(cursor.getString(10));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectStudyOption Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		// sbm_customer_config에서 is_record_upload조회
		query = String.format(DatabaseDefine.GET_SBM_CUSTOMER_CONFIG_GET_CONF, customerNo);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					String confName = cursor.getString(0);

					if (confName.equals("IS_RECORD_UPLOAD")) {
						option.isRecordUpload = intToBoolean(cursor.getInt(1));
					}

					if (confName.equals("IS_PARAGRAPH")) {
						option.isParagraph = intToBoolean(cursor.getInt(1));
					}

					if (confName.equals("IS_VIPSKIP")) {
						option.isVIPSkip = intToBoolean(cursor.getInt(1));
					}
					if (confName.equals("IS_TEXT_KOREAN")) {
						option.isTextKorean = intToBoolean(cursor.getInt(1));
					}

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectStudyOption Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return option;
}

	/**
	 * 체험 계정 사용자 인지 조회
	 * 
	 * @param customerNo
	 *            : 조회할 customerNo
	 * @return 체험계정인지 여부
	 */
	public boolean isExperienceCustomer(int customerNo) {
		// sbm_customer_config에서 IS_EXPERIENCE 조회
		Cursor cursor = null;

		String query = String.format(DatabaseDefine.GET_SBM_CUSTOMER_CONFIG_GET_CONF, customerNo);
		boolean isExperience = false;
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					String confName = cursor.getString(0);

					if (confName.equals("IS_EXPERIENCE")) {
						isExperience = intToBoolean(cursor.getInt(1));
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isExperienceCustomer Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return isExperience;
	}

	// 학습이 완료된 차시를 구해온다.(환경설정의 필요없는 차시를 삭제하기 위함.)
	/**
	 * 학습이 완료된 차시 조회 환경설정의 필요없는 차시를 삭제하기 위함
	 * 
	 * @return 차시 디랙토리 리스트
	 */
	public ArrayList<String> selectStudyFinishedUnitPath() {
		Cursor cursor = null;
		ArrayList<String> pathList = new ArrayList<String>();
		String selectQuery = "SELECT DISTINCT u.series_no, u.item_no, u.study_order FROM " + " wye_befly_study_unit u , sbm_study_result  r " + " WHERE u.product_no = r.product_no AND u.study_unit_code = r.study_unit_code AND r.study_status = 'SFN' ORDER BY study_finish_dt";
		StudyData data = StudyData.getInstance();

		try {
			cursor = mDb.rawQuery(selectQuery, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					int seriesNo = cursor.getInt(0);
					int bookNo = cursor.getInt(1);
					int studyUnitNo = cursor.getInt(2);

					// 현재 학습중인 차시는 제외(재학습 순서가 다를때 포함 될 가능성이 있음, 일반적인 상황은 아님)
					if ((seriesNo == data.mSeriesNo && bookNo == data.mBookNo && studyUnitNo == data.mStudyUnitNo) || (seriesNo == data.mReviewSeriesNo && bookNo == data.mReviewBookNo && studyUnitNo == data.mReviewStudyUnitNo)) {
						continue;
					}

					pathList.add(String.format(ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d", seriesNo, bookNo, studyUnitNo));
				} while (cursor.moveToNext());

				pathList.remove(pathList.size() - 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectStudyFinishUnitPath Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		for (int i = 0; i < pathList.size(); i++) {
			Log.e(TAG, "studyFinishUnitPath : " + pathList.get(i));
		}

		return pathList;
	}

	// 학습이 완료된 30일 이전의 차시를 구해온다.(동기화시 30일 이전의 콘텐츠를 삭제하기 위함.)
	/**
	 * 학습이 완료된 30일 이전의 차시 조회 동기화시 30일 이전의 콘텐츠를 삭제하기 위함
	 * 
	 * @return 차시 디랙토리 리스트
	 */
	public ArrayList<String> selectStudyFinished30DaysUnitPath() {
		Cursor cursor = null;
		ArrayList<String> pathList = new ArrayList<String>();
		String selectQuery = "SELECT DISTINCT u.series_no, u.item_no, u.study_order FROM " + " wye_befly_study_unit u , sbm_study_result  r , tb_studyplan p " + " WHERE u.product_no = r.product_no AND u.study_unit_code = r.study_unit_code AND r.study_status = 'SFN'" + "AND p.itemcode = u.product_no AND p.studyunitcode = u.study_unit_code AND p.studyplanymd <= SUBSTR(DATETIME('NOW', '-30 day'), 1 , 10)" + " ORDER BY study_finish_dt";
		StudyData data = StudyData.getInstance();

		try {
			cursor = mDb.rawQuery(selectQuery, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					int seriesNo = cursor.getInt(0);
					int bookNo = cursor.getInt(1);
					int studyUnitNo = cursor.getInt(2);

					// 현재 학습중인 차시는 제외(재학습 순서가 다를때 포함 될 가능성이 있음, 일반적인 상황은 아님)
					if ((seriesNo == data.mSeriesNo && bookNo == data.mBookNo && studyUnitNo == data.mStudyUnitNo) || (seriesNo == data.mReviewSeriesNo && bookNo == data.mReviewBookNo && studyUnitNo == data.mReviewStudyUnitNo)) {
						continue;
					}

					pathList.add(String.format(ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d", seriesNo, bookNo, studyUnitNo));
				} while (cursor.moveToNext());

				pathList.remove(pathList.size() - 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectStudyFinishUnitPath Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		for (int i = 0; i < pathList.size(); i++) {
			Log.e(TAG, "studyFinishUnitPath : " + pathList.get(i));
		}

		return pathList;
	}

	/**
	 * 현재 db상 유효한 차시를 조회 (배본정책 변경으로 90일 -> 180일로 변경)180일 이전의 데이터를 삭제하기 위해 유효한
	 * unit 구함. 동기화 이후에 값을 구해오게 되므로 180일 이전의 study_plan은 없는 상태임.
	 * 
	 * @return 차시 디랙토리 리스트
	 */
	public ArrayList<String> selectValidUnitPath() {
		Cursor cursor = null;
		ArrayList<String> pathList = new ArrayList<String>();
		String selectQuery = DatabaseDefine.SELECT_VALID_UNIT;

		try {
			cursor = mDb.rawQuery(selectQuery, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					pathList.add(String.format(ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d", cursor.getInt(0), cursor.getInt(1), cursor.getInt(2)));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectCurrentValidUnitPath Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		/*
		 * for(int i = 0; i < pathList.size(); i++) { Log.e(TAG,
		 * "validUnitPath : " + pathList.get(i)); }
		 */

		return pathList;
	}

	/**
	 * 오디오 학습 시작 시간 조회
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재 학습 순서
	 * @return 학습 시작 시간
	 */
	public String selectBookStartDt(int customerNo, int productNo, String studyUnitCode, int reStudyCnt) {
		Cursor cursor = null;

		String startDt = "";
		String query = String.format(DatabaseDefine.SELECT_SBM_STUDY_RESULT_GET_AUDIO_STUDY_TIME, customerNo, productNo, quote(studyUnitCode), reStudyCnt);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				startDt = cursor.getString(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectBookStartDt Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return startDt;
	}

	/**
	 * 오디오 학습 녹음 파일 업로드 삭제(sbm_study_record_detail)
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @param studyKind
	 *            : 학습 종류(1:본학습, 2:복습)
	 * @param studyOrder
	 *            : 순번
	 */
	public void deleteStudyRecordDetail(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, int studyKind, int studyOrder) {
		String deleteQuery = "DELETE FROM sbm_study_record_detail " + "WHERE customer_no = %d AND product_no = %d AND study_unit_code = %s AND re_study_cnt = %d AND study_kind = %d AND study_order = %d ";
		String query = String.format(deleteQuery, customerNo, productNo, quote(studyUnitCode), reStudyCnt, studyKind, studyOrder);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteStudyRecordDetail Error : " + e);
		}
	}

	/**
	 * 오디오 학습 warning 삭제(sbm_study_book_warning)
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @param bookCheckStatus
	 *            : status
	 */
	public void deleteStudyBookWarning(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, int bookCheckStatus) {
		String query = String.format(DatabaseDefine.DELETE_SBM_STUDY_BOOK_WARNNING, customerNo, productNo, quote(studyUnitCode), reStudyCnt, bookCheckStatus);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteStudyBookWarning Error : " + e);
		}
	}

	/**
	 * 오디오 학습 warning 기록(sbm_study_book_warning)
	 * 
	 * @param customerNo
	 *            : customerNo
	 * @param productNo
	 *            : 교재 번호
	 * @param studyUnitCode
	 *            : 차시 코드
	 * @param reStudyCnt
	 *            : 재학습 순서
	 * @param bookCheckStatus
	 *            : status
	 * @param bookCheckDt
	 *            : 발생 시간
	 */
	public void insertStudyBookWarning(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, int bookCheckStatus, String bookCheckDt, int resultStudyNo) {
		try {
			String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_BOOK_WARNNING, customerNo, productNo, quote(studyUnitCode), reStudyCnt, bookCheckStatus, quote(bookCheckDt), resultStudyNo);
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertStudyBookWarning Error : " + e);
		}
	}

	/**
	 * 업로드할 sync 데이터가 있는지 조회 현재 학습중인 것은 제외함. (학습 중 간지에서 환경설정을 띄울경우 항상 true가 되기
	 * 때문)
	 * 
	 * @return 데이터가 있는지 여부
	 */
	public boolean isRemindUploadSyncStudydata() {
		Cursor cursor = null;
		StudyData data = StudyData.getInstance();
		String query = "";

		// study_result 에서 status가 SFN인것중에 올려할 것이 있는지 확인.
		try {
			cursor = mDb.rawQuery(DatabaseDefine.GET_CNT_SBM_STUDY_RESULT_GET_FINISH_NOT_UPLOAD, null);

			if (cursor != null && cursor.moveToFirst()) {
				if (cursor.getInt(0) != 0) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isRemindUploadSyncStudydata Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		// word_detail에서 현재 학습중인 데이터가 아닌 업데이트할 내용이 있는지 확인.
		// 학습중이 아닐때도 문제 없음.
		query = String.format(DatabaseDefine.SELECT_SBM_STUDY_WORD_DETAIL_GET_CNT_WHERE_IS_UPLOAD, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				if (cursor.getInt(0) != 0) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isRemindUploadSyncStudydata Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		// word_detail에서 현재 학습중인 데이터가 아닌 업데이트할 내용이 있는지 확인.
		// 학습중이 아닐때도 문제 없음.
		query = String.format(DatabaseDefine.GET_CNT_SBM_STUDY_SENTANCE_DETAIL_NOT_UPLOAD, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt);

		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				if (cursor.getInt(0) != 0) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isRemindUploadSyncStudydata Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return false;
	}

	/**
	 * 다운로드 받아야할 교재 컨텐츠 조회
	 * 
	 * @return 컨텐츠 정보 리스트
	 */
	public ArrayList<DownloadContent> selectDownloadContent() {
		Cursor cursor = null;
		// String selectUnitQuery = "SELECT study_unit_id, priority FROM
		// download_content GROUP BY study_unit_id order by priority desc ";
		// String selectSubQuery = "SELECT study_unit_id, download_id,
		// download_url, save_path FROM download_content";
		ArrayList<DownloadContent> list = new ArrayList<DownloadContent>();

		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_DOWNLOAD_CONTENTS_UNIT, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					DownloadContent data = new DownloadContent(cursor.getInt(0), intToBoolean(cursor.getInt(1)));
					list.add(data);
				} while (cursor.moveToNext());
			}

			if (cursor != null) {
				cursor.close();
			}

			cursor = mDb.rawQuery(DatabaseDefine.SELECT_DOWNLOAD_CONTENTS_UNIT_FILE, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					int unitId = cursor.getInt(0);
					DownloadContent.UnitFile unitFile = new DownloadContent.UnitFile(cursor.getInt(1), cursor.getString(2), cursor.getString(3));

					for (int i = 0; i < list.size(); i++) {
						DownloadContent studyUnit = list.get(i);
						if (studyUnit.getStudyUnitId() == unitId) {
							studyUnit.addStudyUnit(unitFile);
						}
					}
				} while (cursor.moveToNext());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectDownloadContent Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return list;
	}

	/**
	 * 다운로드 받아야할 교재 컨텐츠 삭제
	 * 
	 * @param downloadId
	 *            : 삭제할 id
	 * @return 삭제 여부
	 */
	public boolean deleteDownloadContent(int downloadId) {
		try {
			String query = String.format(DatabaseDefine.DELETE_DOWNLOAD_CONTESTS, downloadId);
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteDownloadContent Error : " + e);

			return false;
		}

		return true;
	}

	/**
	 * 다운로드 받아야할 교재 컨텐츠 삭제
	 * @return 삭제 여부
	 */
	public boolean deleteDownloadContentAll() {
		try {
			execSQL(DatabaseDefine.DELETE_DOWNLOAD_CONTESTS_ALL);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteDownloadContent Error : " + e);

			return false;
		}

		return true;
	}

	/**
	 * 다운로드 우선순위를 모두 리셋 false로 설정
	 */
	public void updateDownloadContentNoPriority() {
		try {
			execSQL(DatabaseDefine.UPDATE_DOWNLOAD_CONTENTS_SET_PRIORITY);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateDownloadContentNoPriority Error : " + e);
		}
	}

	/**
	 * 다운로드 컨텐츠 추가
	 * 
	 * @param studyUnitId
	 *            : unitId
	 * @param downloadURL
	 *            : download URL
	 * @param savePath
	 *            : 로컬 저장 경로
	 * @param isPriority
	 *            : 우선 순위 여부
	 */
	public void insertDownloadContent(int studyUnitId, String downloadURL, String savePath, boolean isPriority) {
		Log.i(TAG, "insertDownloadContent  : " + "studyUnitId" + studyUnitId + ",downloadURL" + downloadURL + ",savePath" + savePath + ",isPriority" + isPriority);
		int index = savePath.lastIndexOf("/");
		String filename = "";

		if (index > 0) {
			filename = savePath.substring(index + 1);
		}

		try {
			String query = String.format(DatabaseDefine.INSERT_DOWNLOAD_CONTESTS, studyUnitId, quote(filename), quote(downloadURL), quote(savePath), booleanToInt(isPriority));
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertDownloadContent Error : " + e);
		}
	}

	/**
	 * 마지막 sync시간 조회
	 * 
	 * @return 시간 조회
	 */
	public String selectSyncInfo() {
		Cursor cursor = null;
		String lastUpdateDt = CommonUtil.getCurrentDateTime();

		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_SYNC_INFO_GET_LAST_DT, null);

			if (cursor != null && cursor.moveToFirst()) {
				lastUpdateDt = cursor.getString(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectSyncInfo Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return lastUpdateDt;
	}

	/**
	 * 싱크 시간 기록
	 */
	public void insertSyncInfo() {
		String currentTime = CommonUtil.getCurrentDateTime();
		String query = "";
		try {
			query = String.format(DatabaseDefine.INSERT_SYNC_INFO, quote(currentTime));
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertSyncInfo Error : " + e);
		}
	}

	/**
	 * 네트워크 사용량 삭제
	 * 
	 * @param udid
	 *            : deviceId
	 * @param ymd
	 *            : 날짜
	 * @param networkKind
	 *            : 네트워크 종류
	 */
	public void deleteNetworkUsed(String udid, String ymd, String networkKind) {
		try {
			String query = String.format(DatabaseDefine.DELETE_SBM_NETWORK_USE, quote(udid), quote(ymd), quote(networkKind));
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteNetworkUsed Error : " + e);
		}
	}

	/**
	 * 모바일 네트워크 사용량 추가
	 * 
	 * @param bytes
	 *            : 바이트수
	 * @return 성공 여부
	 */
	public boolean addNetworkUsed(int bytes) {
		return addNetworkUsed(bytes, true);
	}

	/**
	 * 네트워크 사용량 추가
	 * 
	 * @param bytes
	 *            : 바이트수
	 * @param isMobile
	 *            : 모바일 인지 여부
	 * @return 기록 여부
	 */
	public boolean addNetworkUsed(int bytes, boolean isMobile) {
		if (mIsBeginTransaction)
			return false;

		String TYPE_MOBILE = "M";
		String TYPE_ALL = "A";
		String day = CommonUtil.getCurrentYYYYMMDD2();
		// String selectQuery = "SELECT used FROM sbm_network_used WHERE ymd =
		// %s and network_kind = %s;";
		// String insertQuery = "INSERT INTO sbm_network_used VALUES (%s, %s,
		// %s, %d);";
		// String updateQuery = "UPDATE sbm_network_used SET used = %d WHERE ymd
		// = %s AND network_kind = %s;";
		String query = "";
		Cursor cursor = null;
		int usedBytes = bytes;

		try {
			query = String.format(DatabaseDefine.SELECT_SBM_NETWORK_USE_GET_USED, quote(day), quote(isMobile ? TYPE_MOBILE : TYPE_ALL));
			cursor = mDb.rawQuery(query, null);

			if (cursor.moveToFirst()) {
				usedBytes += cursor.getInt(0);
				query = String.format(DatabaseDefine.UPDATE_SBM_NETWORK_USE_SET_USE, usedBytes, quote(day), quote(isMobile ? TYPE_MOBILE : TYPE_ALL));
				execSQL(query);
			} else {
				query = String.format(DatabaseDefine.INSERT_SBM_NETWORK_USE, quote(CommonUtil.getDeviceId(mContext)), quote(day), quote(isMobile ? TYPE_MOBILE : TYPE_ALL), usedBytes);
				execSQL(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "addNeworkUsed Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return true;
	}

	/**
	 * string quote 함수
	 * 
	 * @param str
	 *            : quote할 string
	 * @return quote한 string
	 */
	public static String quote(String str) {
		return "\"" + str + "\"";
	}

	/**
	 * string single quote 함수
	 * 
	 * @param str
	 *            : quote할 string
	 * @return quote한 string
	 */
	public static String singleQuote(String str) {
		return "\'" + str + "\'";
	}

	/**
	 * boolean 값을 int로 변경
	 * 
	 * @param value
	 *            : 변환할 값
	 * @return int값
	 */
	public int booleanToInt(boolean value) {
		return value ? 1 : 0;
	}

	/**
	 * int 값을 boolean으로 변경
	 * 
	 * @param value
	 *            : 변환할 값
	 * @return 변환한 boolean 값
	 */
	public boolean intToBoolean(int value) {
		return (value == 0) ? false : true;
	}

	/**
	 * 가끔string으로 공백이 오는경우가 있어서...
	 * 
	 * @param value
	 *            : 변환할 값
	 * @return 변환한 boolean 값
	 */
	public boolean stringToBoolean(String value) {
		return (value == null || "".equals(value) || "0".equals(value)) ? false : true;
	}

	/**
	 * 복습 이전차시 음원 듣기 북 리스트 조회( 조지훈 )
	 * 
	 * @param customer_id
	 * @return
	 */
	public ArrayList<ReviewStudyBook> selectReviewBookList(int customer_id) {
		ArrayList<ReviewStudyBook> reviewList = new ArrayList<ReviewStudyBook>();
		Cursor cursor = null;
		String query = String.format(DatabaseDefine.SELECT_REVEIW_CDN_AUDIO, customer_id);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					boolean isExistProductNo = false;

					ReviewStudyBook book = new ReviewStudyBook();
					book.mProductNo = cursor.getInt(0);
					book.mSeriesNo = cursor.getInt(3);
					book.mSeriesName = cursor.getString(4);
					book.mBookNo = cursor.getInt(5);

					for (int i = 0; i < reviewList.size(); i++) {
						ReviewStudyBook book2 = reviewList.get(i);
						if (book.mProductNo == book2.mProductNo) {
							book = book2;
							isExistProductNo = true;
							break;
						}
					}

					ReviewStudyBookUnit unit = new ReviewStudyBookUnit();
					unit.mStudyUnitCode = cursor.getString(1);
					unit.mStudyUnitNo = cursor.getInt(2);

					book.mUnitList.add(unit);

					if (!isExistProductNo) {
						reviewList.add(book);
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectReviewUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return reviewList;
	}

	/**
	 * 음성 합칠 리스트
	 * 
	 * @param start_merge_time
	 * @return
	 */
	public ArrayList<RecData> selectMergRecList(int start_merge_time) {
		ArrayList<RecData> recList = new ArrayList<RecData>();
		Cursor cursor = null;
		String query = String.format(DatabaseDefine.SELECT_SBM_STUDY_RECORD_DTAIL_RECDATA, start_merge_time);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {

					RecData data = new RecData();
					data.customer_no = cursor.getInt(0);
					data.product_no = cursor.getInt(1);
					data.study_result_no = cursor.getInt(2);
					data.book_detail_id = cursor.getInt(3);
					data.student_book_id = cursor.getInt(4);
					data.study_unit_code = cursor.getString(5);
					data.re_study_cnt = cursor.getInt(6);
					data.study_kind = cursor.getInt(7);
					data.study_order = cursor.getInt(8);
					data.rec_type = cursor.getString(9);
					data.start_tag_name = cursor.getString(10);
					data.start_tag_time = cursor.getInt(11);
					data.end_tag_time = cursor.getString(12);
					data.start_merge_time = cursor.getInt(13);
					data.end_merge_time = cursor.getInt(14);
					data.rec_len = cursor.getInt(15);
					data.is_upload = cursor.getInt(16);
					data.is_merge = cursor.getInt(17);
					data.rep_cnt = cursor.getInt(18);
					data.crud = cursor.getString(19);
					data.file_path = cursor.getString(20);

					recList.add(data);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectReviewUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return recList;
	}

	/**
	 * 음성 합칠 리스트
	 * 
	 * @param
	 * @return
	 */
	public ArrayList<RecData> selectRecListAll() {
		ArrayList<RecData> recList = new ArrayList<RecData>();
		Cursor cursor = null;
		String query = String.format(DatabaseDefine.SELECT_SBM_STUDY_RECORD_ALL);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {

					RecData data = new RecData();
					data.customer_no = cursor.getInt(0);
					data.product_no = cursor.getInt(1);
					data.study_result_no = cursor.getInt(2);
					data.book_detail_id = cursor.getInt(3);
					data.student_book_id = cursor.getInt(4);
					data.study_unit_code = cursor.getString(5);
					data.re_study_cnt = cursor.getInt(6);
					data.study_kind = cursor.getInt(7);
					data.study_order = cursor.getInt(8);
					data.rec_type = cursor.getString(9);
					data.start_tag_name = cursor.getString(10);
					data.start_tag_time = cursor.getInt(11);
					data.end_tag_time = cursor.getString(12);
					data.start_merge_time = cursor.getInt(13);
					data.end_merge_time = cursor.getInt(14);
					data.rec_len = cursor.getInt(15);
					data.is_upload = cursor.getInt(16);
					data.is_merge = cursor.getInt(17);
					data.rep_cnt = cursor.getInt(18);
					data.crud = cursor.getString(19);
					data.file_path = cursor.getString(20);
					recList.add(data);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectReviewUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return recList;
	}

	public void deleteMergRecList(int start_merge_time) {
		try {
			String query = String.format(DatabaseDefine.DELETE_SBM_STUDY_RECORD_DTAIL_RECDATA, start_merge_time);
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteOldStudyPlan Error : " + e);
		}
	}

	/**
	 * 음성 합칠 리스트
	 * @return
	 */
	public ArrayList<Integer> selectMergStartTime() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		Cursor cursor = null;
		try {
			cursor = mDb.rawQuery(DatabaseDefine.SELECT_SBM_STUDY_RECORD_DTAIL_MERGTIME, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					int time = cursor.getInt(0);
					list.add(time);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectReviewUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return list;
	}

	/**
	 * 복습 이전차시 음원 듣기 북 리스트 조회( 조지훈 )
	 * 
	 * @param customer_id
	 * @return
	 */
	public ArrayList<ReviewStudyAudio> selectReviewStudyBookList(int customer_id) {
		ArrayList<ReviewStudyAudio> reviewList = new ArrayList<ReviewStudyAudio>();
		Cursor cursor = null;
		String query = String.format(DatabaseDefine.SELECT_REVEIW_STUDY_AUDIO, customer_id);
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					boolean isExistProductNo = false;

					ReviewStudyAudio book = new ReviewStudyAudio();
					book.mProductNo = cursor.getInt(0);
					book.mSeriesNo = cursor.getInt(3);
					book.mSeriesName = cursor.getString(4);
					book.mBookNo = cursor.getInt(5);
					book.mRestudyCnt = cursor.getInt(10);
					for (int i = 0; i < reviewList.size(); i++) {
						ReviewStudyAudio book2 = reviewList.get(i);
						if (book.mProductNo == book2.mProductNo) {
							book = book2;
							isExistProductNo = true;
							break;
						}
					}

					ReviewStudyAudioUnit unit = new ReviewStudyAudioUnit();
					unit.mStudyUnitCode = cursor.getString(1);
					unit.mStudyUnitNo = cursor.getInt(2);
					unit.mAudioPlayTime = cursor.getInt(6);
					unit.mReviewTime = cursor.getInt(7);
					unit.mUseTime = cursor.getInt(8);
					unit.mUseCnt = cursor.getInt(9);
					book.mUnitList.add(unit);

					if (!isExistProductNo) {
						reviewList.add(book);
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectReviewUnit Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return reviewList;
	}

	/**
	 * 홈화면 주간 달력 정보 조회
	 * @return 주간 달력에 표시할 정보
	 */
	public ArrayList<DatabaseData.WeekCalendarUnit> selectWeekCalendarUnit(int customerId, String startDate, String endDate) {
		ArrayList<DatabaseData.WeekCalendarUnit> wcList = new ArrayList<DatabaseData.WeekCalendarUnit>();
		Cursor cursor = null;
		String query = DatabaseDefine.SELECT_WEEK_CALENDAR_UNIT + String.format(DatabaseDefine.SELECT_WEEK_CALENDAR_UNIT_POSTFIX, startDate, endDate, customerId, customerId, startDate, endDate) ;

		android.util.Log.d("calad" , query);

		try {
			cursor = mDb.rawQuery(query, null);

			if(cursor != null && cursor.moveToFirst()) {
				do {
					DatabaseData.WeekCalendarUnit wc = new DatabaseData.WeekCalendarUnit();
					wc.mDate  = (cursor.getString(0)).substring(8);
					wc.mDisplayText  = (cursor.getString(1));
					wc.mWeekDay = cursor.getInt(2);
					wc.mStudyUnitCnt = cursor.getInt(3);
					wc.mWeekStudyCnt = cursor.getInt(4);
					wcList.add(wc);
				} while(cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			android.util.Log.e(TAG, "selectWeekCalendarUnit Error : " + e);
		} finally {
			if(cursor != null) {
				cursor.close();
			}
		}
		return wcList;
	}

	public void insertFailStudyLog(String url) {

		String query = String.format(DatabaseDefine.INSERT_SBM_FAIL_STUDY_LOG, quote(url));

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
//			Log.e(TAG, "insertFailStudyLog Error : " + e);
			Log.e("ss99km01", "ss99km01 insertFailStudyLog Error : " + e);
		}

	}

	public ArrayList<String> selectFailStudyLog() {
		Cursor cursor = null;
		ArrayList<String> pathList = new ArrayList<String>();
		String selectQuery = DatabaseDefine.SELECT_SBM_FAIL_STUDY_LOG;

		try {
			cursor = mDb.rawQuery(selectQuery, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					pathList.add(cursor.getString(0));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "selectFailStudyLog Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		/*
		 * for(int i = 0; i < pathList.size(); i++) { Log.e(TAG,
		 * "validUnitPath : " + pathList.get(i)); }
		 */

		return pathList;
	}

	public boolean isFailStudyLogCnt() {
		Cursor cursor = null;

		String query = DatabaseDefine.SELECT_SBM_FAIL_STUDY_LOG_CNT;
		try {
			cursor = mDb.rawQuery(query, null);

			if (cursor != null && cursor.moveToFirst()) {
				if (cursor.getInt(0) != 0) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "isFailStudyLogCnt Error : " + e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

		return false;
	}

	public void deleteFailStudyLog() {
		try {
			execSQL(DatabaseDefine.DELETE_SBM_FAIL_STUDY_LOG);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteFailStudyLog Error : " + e);
		}
	}

	/**
	 * 스마트 음원 다시 듣기에서 학습 재생시간, 횟수를 추가함
	 * 
	 * @param customerNo
	 * @param productNo
	 * @param studyUnitCode
	 * @param reStudyCnt
	 * @param audio_play_time
	 * @param review_time
	 * @param use_time
	 * @param use_cnt
	 */
	public void insertReviewStudyResult(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, int audio_play_time, int review_time, int use_time, int use_cnt) {

		String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_RESULT_REVIEW, customerNo, productNo, quote(studyUnitCode), reStudyCnt, audio_play_time, review_time, use_time, use_cnt);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "insertReviewStudyResult Error : " + e);
		}

	}

	/**
	 * 스마트 음원 다시 듣기에서 학습 재생시간, 횟수를 업데이트 함
	 * 
	 * @param customerNo
	 * @param productNo
	 * @param studyUnitCode
	 * @param reStudyCnt
	 * @param use_time
	 * @param use_cnt
	 */
	public void updateReviewStudyResult(int customerNo, int productNo, String studyUnitCode, int reStudyCnt, int use_time, int use_cnt) {
		String query = String.format(DatabaseDefine.UPDATE_SBM_STUDY_RESULT_REVIEW, use_time, use_cnt, customerNo, productNo, quote(studyUnitCode), reStudyCnt);

		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateReviewStudyResult Error : " + e);
		}

	}

	/**
	 * wusi12
	 */
	public void updateTbStudyPlanSetNow() {
		String query = String.format(DatabaseDefine.UPDATE_TB_STDUY_PLAN_SET_STUDYPLANYMD_NOW, now2Str());
		try {
			execSQL(query);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "updateDownloadContentNoPriority Error : " + e);
		}
	}

	public void deleteStudyResultAll() {
		try {
			execSQL(DatabaseDefine.DELETE_SBM_STUDY_RESULT_ALL);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteOldStudyPlan Error : " + e);
		}
	}

	public void deleteStudyWordDetailAll() {
		try {
			execSQL(DatabaseDefine.DELETE_SBM_STUDY_WORD_DETAIL);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteOldStudyPlan Error : " + e);
		}
	}

	public void deleteStudySentenceDetailAll() {

		try {
			execSQL(DatabaseDefine.DELETE_SBM_STUDY_SENTANCE_DETAIL_ALL);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteOldStudyPlan Error : " + e);
		}
	}

	public void deleteStudyVanishingDetailAll() {
		try {
			execSQL(DatabaseDefine.DELETE_SBM_STUDY_VANISHING_DETAIL_ALL);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "deleteOldStudyPlan Error : " + e);
		}
	}

	/**
	 * 지금 시간을 정해진 포맷으로 변경한다.
	 * 
	 * @return 변환된 포맷
	 */
	public String now2Str() {
		Calendar calendar = Calendar.getInstance();
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd", java.util.Locale.KOREA);
		return formatter.format(calendar.getTime());
	}
}
