package com.yoons.fsb.student.network;

import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * http 클라이언트 클래스
 * 
 * @author ejlee
 *
 */
public class HttpClientManager {
	private final String TAG = "[HttpClientManager]";
	private final int HTTPS_CONNECTION_TIMEOUT = 10000;
	private final int HTTPS_SOCKET_TIMEOUT = 20000;
	private final int HTTP_PORT = 80;
	private final int HTTPS_PORT = 443;

//	static public ThreadSafeClientConnManager mConnectionManager = null;

	/**
	 * 기본 설정된 httpClient을 구해옴
	 * 
	 * @return httpClient
	 */
	public DefaultHttpClient getClient() {
		DefaultHttpClient httpClient = null;

		try {
			// sets up parameters
			HttpParams params = new BasicHttpParams();

			// 연결 타임아웃 설정
			HttpConnectionParams.setConnectionTimeout(params, HTTPS_CONNECTION_TIMEOUT);

			// 통신 타임아웃 설정
			HttpConnectionParams.setSoTimeout(params, HTTPS_SOCKET_TIMEOUT);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, "utf-8");
			params.setParameter("http.protocol.expect-continue", false);

			// 컨넥션 개수 조정
			ConnManagerParams.setMaxConnectionsPerRoute(params, new ConnPerRouteBean(5));

			// registers schemes for both http and https
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), HTTP_PORT));
			registry.register(new Scheme("https", new TrustAllSSLSocketFactory(), HTTPS_PORT));

			/*if(mConnectionManager == null) {
				mConnectionManager = new ThreadSafeClientConnManager(params, registry);
			}
			
			mConnectionManager.closeExpiredConnections();*/

			httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
		} catch (Exception e) {
			Log.e(TAG, "HTTP getClient ERROR : " + e);
			return null;
		}

		return httpClient;
	}

	/**
	 * http Post Request
	 * 
	 * @param reqUri
	 *            : 요청 uri
	 * @param entity
	 *            : entity
	 * @return HttpResponse
	 */
	public HttpResponse httpPostRequest(String reqUri, String entity) {
		HttpRequestBase httpRequest = null;
		HttpResponse response = null;
		DefaultHttpClient httpClient = getClient();

		try {
			httpRequest = new HttpPost(reqUri);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (entity != null) {
			try {
				((HttpPost) httpRequest).setEntity(new StringEntity(entity));
			} catch (Exception e) {
				Log.e(TAG, "HTTP SEND ENTITY ERROR : " + e);
				httpClient.getConnectionManager().shutdown();
				return null;
			}
		}

		// Http Request
		try {
			response = httpClient.execute(httpRequest);
		} catch (Exception e) {
			Log.e(TAG, "HTTP SEND REQUEST ERROR : " + e);
			//HttpClient ShutDown
			httpClient.getConnectionManager().shutdown();
			ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_REQUEST, e);
		}

		if (response != null) {
			int httpStatusCode = response.getStatusLine().getStatusCode();
			Log.e(TAG, "[HTTP-STATUS-CODE]: " + httpStatusCode);

			if (httpStatusCode == 400 || httpStatusCode == 404) {
				Log.e(TAG, "[HTTP-ERROR]: 400-404 Error Retry!!!");

				try {
					Thread.sleep(200);
					response = httpClient.execute(httpRequest);
				} catch (Exception e) {
					Log.e(TAG, "HTTP SEND REQUEST ERROR : " + e);
					ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_REQUEST, e);
					return null;
				}
			}
		}

		return response;
	}

	/**
	 * http Post Request
	 * 
	 * @param reqUri
	 *            : 요청 uri
	 * @param jsonObject
	 *            : entity에 들어가는 jsonObject(사용하지 않음 항상 null)
	 * @return JSonObject
	 */
	public JSONObject httpPostRequestJSON(String reqUri, JSONObject jsonObject) {
		HttpResponse response = null;
		HttpEntity entity = null;

		if (jsonObject == null) {
			response = httpPostRequest(reqUri, null);
		} else {
			response = httpPostRequest(reqUri, jsonObject.toString());
		}

		if (response != null) {
			entity = response.getEntity();
			try {
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				entity.writeTo(os);
				if (response.getStatusLine().getStatusCode() == 200) {
					return new JSONObject(os.toString());
				}

			} catch (Exception e) {
				Log.e(TAG, "HTTP JSON REQUEST ERROR : " + e);
				e.printStackTrace();
				ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_REQUEST, e);
			}
		}

		return null;
	}
}
