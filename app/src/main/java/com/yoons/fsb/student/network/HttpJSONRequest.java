package com.yoons.fsb.student.network;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * was 서버 패키지 request 클래스
 *
 * @author ejlee
 */
public class HttpJSONRequest {
    public static final String TAG = "[HttpJSONRequest]";

    private Context mContext = null;

    private static final String PKG_NAME = "p_nm";

    private static final String IN_PARAM = "in";
    private static final String OUT_PARAM = "out";

    private static final String OUT_TYPE_INT = "int";
    private static final String OUT_TYPE_STRING = "string";
    private static final String OUT_TYPE_CURSOR = "cursor";
    private int mRetryCount = 0;
    private static final int MAX_RETRY_COUNT = 2;

    public HttpJSONRequest(Context context) {
        mContext = context;
    }

    /**
     * 패키지 이름 추가 함수
     *
     * @param name : 패키지 이름
     * @return
     */
    private String addPackageName(String name) {
        if (ServiceCommon.STUDY_SERVER_URL.length() == 0) {
            ServiceCommon.setServerUrl(mContext, "");
        }

        return ServiceCommon.STUDY_SERVER_URL + "?" + PKG_NAME + "=" + name;
    }

    /**
     * 패키지 이름 추가 함수
     *
     * @param name : 패키지 이름
     * @return
     */
    private String addFSBPackageName(String name) {
        return ServiceCommon.FSB_SERVER_URL + "?" + PKG_NAME + "=" + name;
    }

    private String addSBPackageName(String name) {
        return ServiceCommon.SB_SERVER_URL + "?" + PKG_NAME + "=" + name;
    }

    /**
     * 파라미터 생성
     *
     * @param params : 가변 스트링 파라미터 문자열
     * @return 스트링 배열
     */
    public String[] createParam(String... params) {
        String param[] = new String[params.length];
        for (int i = 0; i < param.length; i++) {
            param[i] = (String) params[i];
        }

        return param;
    }

    /**
     * In 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addInParam(String url, String param[]) {
        if (param.length == 0) {
            Log.e(TAG, "param size error");
            return url;
        }

        for (int i = 0; i < param.length; i++) {
            url += "&" + IN_PARAM + String.valueOf(i + 1) + "=" + param[i];
        }

        return url;
    }

    /**
     * Out 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addOutParam(String url, String param[]) {
        if (param.length == 0) {
            Log.e(TAG, "param size error");
            return url;
        }

        for (int i = 0; i < param.length; i++) {
            url += "&" + OUT_PARAM + String.valueOf(i + 1) + "=" + param[i];
        }

        return url;
    }

    /**
     * request
     *
     * @param url  : url
     * @param json : JSONObject
     */
    public final void request(final String url, final JSONObject json) {
        request(null, url, json);
    }

    /**
     * request
     *
     * @param handler : response 받을 handler
     * @param url     : url
     * @param json    : JSONObject
     */
    public final void request(final Handler handler, final String url, final JSONObject json) {
        request(handler, url, json, 0);
    }

    /**
     * request
     *
     * @param handler   : response 받을 handler
     * @param url       : url
     * @param json      : JSONObject
     * @param requestId : requesd Id(여러 request를 보내고 한 handler에서 response를 받을 경우 구분하기
     *                  위해 사용)
     */
    public final void request(final Handler handler, final String url, final JSONObject json, final int requestId) {
        if (ServiceCommon.IS_CONTENTS_TEST) {
            Log.i(TAG, "IS_CONTENTS_TEST HttpRequest skip...");
            return;
        } else if (ServiceCommon.IS_EXPERIENCES) {
            Log.i(TAG, "IS_EXPERIENCES HttpRequest skip...");
            if (handler != null) {
                handler.sendMessage(handler.obtainMessage(ServiceCommon.MSG_HTTP_REQUEST_FAIL, requestId, 0, null));
            }
            return;
        }

        Thread thread = new Thread("jsonrequest") {
            public void run() {
                Log.i(TAG, "HTTP REQUEST : " + url);
                HttpClientManager client = new HttpClientManager();
                JSONObject response = client.httpPostRequestJSON(url, json);

                if (response == null) { // http response가 200이 오지 않으면 null임
                    Log.i(TAG, "HTTP_REQUEST_FAIL : " + url);
                    if (handler != null) {
                        if (mRetryCount < MAX_RETRY_COUNT) { // 응답을 받을 핸들러가 있는 것만 retry
                            mRetryCount++;
                            Log.i(TAG, "RETRY " + mRetryCount + " : " + url);
                            request(handler, url, json, requestId);
                        } else {
                            if (CommonUtil.isCenter() && url.contains("QXNwX0xfU1RVRFlfUkVTVUxUX0Y=")) {
                                if (url.contains("&in5=SFN") || url.contains("&in6=START") || url.contains("&in6=END")) {
                                    Log.e("ss99km01", "ss99km01 MSG_HTTP_REQUEST_FAIL " + mRetryCount + " : " + url);
                                    DatabaseUtil.getInstance(mContext).insertFailStudyLog(url);
                                }
                            }
                            handler.sendMessage(handler.obtainMessage(ServiceCommon.MSG_HTTP_REQUEST_FAIL, requestId, 0, null));
                            Preferences.setServerRequestSuccess(mContext, false); // 성공 여부를 preferences에 기록
                        }
                    }
                } else {
                    Log.i(TAG, "HTTP_REQUEST_SUCCESS : " + url);
//                    if (CommonUtil.isCenter() && url.contains("QXNwX0xfU1RVRFlfUkVTVUxUX0Y=")) {
//                        DatabaseUtil.getInstance(mContext).insertFailStudyLog(url);
//                    }
                    if (response.toString().length() < 100) {
                        Log.i(TAG, "response : " + response.toString());
                    }
                    if (handler != null) {
                        handler.sendMessage(handler.obtainMessage(ServiceCommon.MSG_HTTP_REQUEST_SUCCESS, requestId, 0, response));
                        Preferences.setServerRequestSuccess(mContext, true); // 성공 여부를 preferences에 기록
                    }
                }
            }
        };

        thread.start();
    }

    /**
     * 집중,호출
     *
     * @param handler : response받을 handler
     */
    // dbo.Asp_L_GET_COMMAND
    public void requestNoti(Handler handler, int requestId) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfR0VUX0NPTU1BTkQ=");
        } else {
            url = addPackageName("ZGJvLkFzcF9MX0dFVF9DT01NQU5E");
        }

        String mac = "MAC";
        mac = CommonUtil.getMacAddress();

        url = addInParam(url, createParam(mac));
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_INT));

        request(handler, url, null, requestId);
    }

    /**
     * agencyNo검색(LMSstatus 추가  0:숲 1:우영)
     *
     * @param handler : response받을 handler
     */
    // dbo.Asp_L_GET_FOREST_INFO
    public void requestAgencyNo(Handler handler) {
        String url = addPackageName("ZGJvLkFzcF9MX0dFVF9GT1JFU1RfSU5GTw==");

        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 버전 조회
     *
     * @param handler : response받을 handler
     */
    // BS_PKG_M4SB_COMMON.SP_CHECK_VERSION_CENTER_INFO
    public void requestAppVersion(Handler handler, int requestId, String agencyNo, String teacherNo) {
        String url = addFSBPackageName("QlNfUEtHX000U0JfQ09NTU9OLlNQX0NIRUNLX1ZFUlNJT05fQ0VOVEVSX0lORk8=");
        String appKind = "SB2_F_YS20";


        url = addInParam(url, createParam(appKind, agencyNo, teacherNo));
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_INT));

        request(handler, url, null, requestId);
    }

    /**
     * 로그인
     *
     * @param handler        : response받을 handler
     * @param id             : 회원 id
     * @param password       : 회원 password
     * @param firstLoginDate : 최초 로그인 date
     * @param requestId      : requestId
     */
    // SBMUSER.SBM_PKG_C_COMMON.SP_L_LOGIN
    public void requestLogin(Handler handler, String id, String password, String firstLoginDate, int requestId) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX0NfQ09NTU9OLlNQX0xfTE9HSU4=");

        url = addInParam(url, createParam("M", id, password, firstLoginDate));
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_INT));

        request(handler, url, null, requestId);
    }

    /**
     * 컨텐츠 테스트 로그인
     *
     * @param handler
     * @param id
     * @param password
     * @param firstLoginDate
     * @param requestId
     */
    // SBMUSER.SBM_PKG_C_COMMON.SP_L_LOGIN
    public void requestContentsLogin(Handler handler, String id, String password, String firstLoginDate, int requestId) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX0NfQ09NTU9OLlNQX0xfTE9HSU4=");

        url = addInParam(url, createParam("A", id, password, firstLoginDate));
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_INT));

        requestContentsTest(handler, url, null, requestId);
    }

    /**
     * 단말 등록
     *
     * @param handler   : response받을 handler
     * @param params    : inParam
     * @param requestId : requestId
     */
    // SBMUSER.SBM_PKG_C_COMMON.SP_M_TERM_REGI
    public void requestRegi(Handler handler, String params[], int requestId) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX0NfQ09NTU9OLlNQX01fVEVSTV9SRUdJ");

        url = addInParam(url, params);
        url = addOutParam(url, createParam(OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null, requestId);
    }

    /**
     * 단말 등록 해지
     *
     * @param handler    : response받을 handler
     * @param customerNo : 회원 번호
     * @param requestId  : requestId
     */
    // SBMUSER.SBM_PKG_T_CUSTOMER.SP_D_CUSTOMER_DELREGI
    public void requestDeRegi(Handler handler, int customerNo, int requestId) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1RfQ1VTVE9NRVIuU1BfRF9DVVNUT01FUl9ERUxSRUdJ");

        url = addInParam(url, createParam(String.valueOf(customerNo), "SBS"));
        url = addOutParam(url, createParam(OUT_TYPE_INT));

        request(handler, url, null, requestId);
    }

    /**
     * download Sync
     *
     * @param handler
     *            : response받을 handler
     * @param customerNo
     *            : 회원 번호
     * @param tableName
     *            : 테이블 이름
     * @param maxDate
     *            : 마지막 업데이트 시간
     * @param term
     *            : 기간
     */
    // SBMUSER.SBM_PKG_SYNC.SP_SYNC_FROM_SERVER_CUSTOMER
    /*
     * public void requestDownloadSync(Handler handler, int customerNo, String
	 * tableName, String maxDate, String term) { String url = addPackageName(
	 * "U0JNVVNFUi5TQk1fUEtHX1NZTkMuU1BfU1lOQ19GUk9NX1NFUlZFUl9DVVNUT01FUg==");
	 * 
	 * url = addInParam(url, createParam(String.valueOf(customerNo), tableName,
	 * maxDate, term)); url = addOutParam(url, createParam(OUT_TYPE_CURSOR,
	 * OUT_TYPE_INT));
	 * 
	 * request(handler, url, null); }
	 */

    /**
     * download Sync 모든 테이블
     *
     * @param handler : response받을 Handler
     * @param params  테이블 별 maxdt 또는 Term
     */
    // SBMUSER.SBM_PKG_SYNC.SP_ALL_SYNC_FROM_CUSTOMER
    public void requestDownloadSyncAll(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU3luY19BbGw=");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NZTkMuU1BfQUxMX1NZTkNfRlJPTV9DVVNUT01FUg==");
        }

        url = addInParam(url, params);
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_CURSOR, OUT_TYPE_INT, OUT_TYPE_STRING));

        Crashlytics.log(url.toString());
        request(handler, url, null);
    }

    /**
     * download Sync 교재 목록 가져오기
     *
     * @param handler : response받을 Handler
     * @param params  테이블 별 maxdt 또는 Term
     */
    // SBMUSER.SBM_PKG_SYNC.SP_ALL_SYNC_FROM_CUSTOMER
    public void requestListsyncAll(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfR2V0X1N0dWR5X0xpc3Q=");
        } else {
            url = addPackageName("TE1TLmRiby5Bc3BfTF9HZXRfU3R1ZHlfTGlzdA==");
        }
        url = addInParam(url, params);
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_INT));

        request(handler, url, null);
    }

    public void requestWeekStudyResullt(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("U1BfTF9XRUVLX0NMQVNT");
        } else {
            url = addPackageName("");
        }

        url = addInParam(url, params);
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_CURSOR));

        request(handler, url, null);
    }

    public void requestPageData(Handler handler, String params[]) {
        String url = "";
        url = addSBPackageName("U0JNVVNFUi5TQk1fUEtHX0FQSS5TUF9MX1NCTV9QQUdF");

        url = addInParam(url, params);
        url = addOutParam(url, createParam(OUT_TYPE_CURSOR, OUT_TYPE_INT));

        request(handler, url, null);
    }

    public static boolean mIsStatusRequest = false;
    public static ArrayList<String> mStatusQueue = new ArrayList<String>();
    /**
     * 학습 상태 전송
     *
     * @param params : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_RESULT
    public void requestStudyStatus(String... params) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU1RVRFlfUkVTVUxUX0Y=");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9SRVNVTFQ=");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_INT));
        Log.e(TAG, "url:" + url);
        synchronized (mStatusQueue) {
            ArrayList<String> urlList = DatabaseUtil.getInstance(mContext).selectFailStudyLog();
            DatabaseUtil.getInstance(mContext).deleteFailStudyLog();
            Log.e("ss99km01", "ss99km01 requestStudyStatus synchronized (mStatusQueue)");

            for (int i = 0; i < urlList.size(); i++) {  // 실패한 학습 로그가 있는 경우 먼저 보낼수 있도록 추가함
                mStatusQueue.add(mStatusQueue.size(), urlList.get(i));
                Log.e("ss99km01", "ss99km01 requestStudyStatus " + "add :" + urlList.get(i));
            }
            mStatusQueue.add(mStatusQueue.size(), url);
        }

        requestStatus();
    }

    /**
     * 백업된 학습 상태 전송
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_RESULT
    public void requestBackupStudyStatus(Handler handler) {
        synchronized (mStatusQueue) {
            ArrayList<String> urlList = DatabaseUtil.getInstance(mContext).selectFailStudyLog();
            DatabaseUtil.getInstance(mContext).deleteFailStudyLog();
            Log.e("ss99km01", "ss99km01 requestBackupStudyStatus synchronized (mStatusQueue)");

            for (int i = 0; i < urlList.size(); i++) {  // 실패한 학습 로그가 있는 경우 먼저 보낼수 있도록 추가함
                mStatusQueue.add(mStatusQueue.size(), urlList.get(i));
                Log.e("ss99km01", "ss99km01 requestBackupStudyStatus " + "add :" + urlList.get(i));
            }
        }
        requestBackupStatus(handler);
    }

    /**
     * 학습 단계 동시 request로 인해 순서가 바뀌는 문제를 처리하기 위해 queue로 관리해서 request함
     */
    public void requestStatus() {
        synchronized (mStatusQueue) {
            if (!mIsStatusRequest && mStatusQueue.size() != 0) {
                mIsStatusRequest = true;

                String url = mStatusQueue.get(0);
                mStatusQueue.remove(0);

                request(mHandler, url, null);
            }
        }
    }

    /**
     * 학습 단계 동시 request로 인해 순서가 바뀌는 문제를 처리하기 위해 queue로 관리해서 request함
     * @param handler : response받을 Handler
     */
    public void requestBackupStatus(Handler handler) {
        synchronized (mStatusQueue) {
            if (!mIsStatusRequest && mStatusQueue.size() != 0) {
                mIsStatusRequest = true;

                String url = mStatusQueue.get(0);
                mStatusQueue.remove(0);

                request(handler, url, null);
            }
        }
    }

    /**
     * 학습 단계 전송 실패 시 queue의 값들 db에 기록
     */
    public void backupStatus() {
        synchronized (mStatusQueue) {
            for (int i = 0; i < mStatusQueue.size(); i++) {
                if (mStatusQueue.get(i).contains("&in5=SFN") || mStatusQueue.get(i).contains("&in6=START") || mStatusQueue.get(i).contains("&in6=END")) {
                    DatabaseUtil.getInstance(mContext).insertFailStudyLog(mStatusQueue.get(i));
                    Log.e("ss99km01", "ss99km01 backupStatus " + "insert :" + mStatusQueue.get(i));
                }
            }
            mStatusQueue.clear();
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    mIsStatusRequest = false;

                    requestStatus();
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    mIsStatusRequest = false;

                    backupStatus();
//                    requestStatus();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * 중앙화에서 반복구간에서만 사용하게 할 예정
     * 앱에 미리 심어둠
     *
     * @param params
     */
    //SBM_PKG_C_COMMON. SP_I_APP_SYSTEM_LOG
    public void requestAppSystemLog(String... params) {
        //String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX0NfQ09NTU9OLlNQX0lfQVBQX1NZU1RFTV9MT0c=");
        String url = addPackageName("QXNwX0xfU1lTVEVNX0xPRw==");//Asp_L_SYSTEM_LOG

        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_INT));

        request(url, null);
    }

    /**
     * 네트워크 사용량 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    // SBMUSER.SBM_PKG_C_COMMON.SP_M_NETWORK_USED
    public void requestNetworkUsed(Handler handler, String params[]) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX0NfQ09NTU9OLlNQX01fTkVUV09SS19VU0VE");
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 학습 결과 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_RESULT_FINISH_V4
    public void requestStudyResultFinish(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU1RVRFlfUkVTVUxUX0ZJTklTSA==");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9SRVNVTFRfRklOSVNIX1Y0");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 오디오 학습 상태를 5분마다 보냄
     *
     * @param params : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_RESULT_BOOK_CHK // 5분마다 호출함.
    public void requestStudyResultBookChk(String... params) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9SRVNVTFRfQk9PS19DSEs=");
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT));

        request(url, null);
    }

    /**
     * 오디오 학습 사이렌 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    //SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_BOOK_WARNING
    public void requestStudyBookWarning(Handler handler, String... params) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9CT09LX1dBUk5JTkc=");
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 오디오 학습 녹음 파일 정보
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    //SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_RECORD_DTL
    public void requestStudyRecordDetail(Handler handler, String... params) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU1RVRFlfUkVDT1JEX0RUTF9G");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9SRUNPUkRfRFRM");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 단어 시험 결과 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_WORDDTL
    public void requestStudyWordDetail(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU1RVRFlfV09SRERUTA==");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9XT1JERFRM");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 문장 시험 결과 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_SENTENCEDTL
    public void requestStudySentenceDetail(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU1RVRFlfU0VOVEVOQ0VEVExfRg==");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9TRU5URU5DRURUTA==");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 문단 연습 결과 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_STUDY_PA_SENTENCEDTL
    public void requestStudyVanishingDetail(Handler handler, String params[]) {

        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfU1RVRFlfUEFfU0VOVEVOQ0VEVExfRg==");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9QQV9TRU5URU5DRURUTA==");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    // 신규문항

    /**
     * Popup 학습 결과 전송
     *
     * @param handler : response받을 handler
     * @param params  : inParam
     */
    // SBMUSER.SBM_PKG_S_RT.SP_M_RT_EXAM_DTL
    public void requestStudyExamDetail(Handler handler, String params[]) {
        String url = "";
        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfRVhBTV9EVEw=");
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9FWEFNX0RUTA==");
        }
        url = addInParam(url, createParam(params));
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_STRING, OUT_TYPE_INT));

        request(handler, url, null);
    }

    /**
     * 서버시간 동기화 요청
     */
    //SBMUSER.SBM_PKG_COMMON.SP_GET_DATE
    public void requestServerTimeSync(Handler handler, int requestId) {
        String url = "";

        if (CommonUtil.isCenter()) {
            url = addPackageName("QXNwX0xfR0VUX0RBVEU=");
            url = addOutParam(url, createParam(OUT_TYPE_STRING));
        } else {
            url = addPackageName("U0JNVVNFUi5TQk1fUEtHX0NPTU1PTi5TUF9HRVRfREFURQ==");
            url = addOutParam(url, createParam(OUT_TYPE_STRING));
        }

        request(handler, url, null, requestId);

    }

    /**
     * 앱 업데이트 시 버전 정보 전송
     *
     * @param params : inParam
     */
    // SBMUSER.SBM_PKG_C_COMMON.SP_M_TERM_REGI
    public void requestUpgradeHistory(String params[]) {
        String url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NTTy5TUF9JX0FQUF9VUEdSQURF");

        url = addInParam(url, params);
        url = addOutParam(url, createParam(OUT_TYPE_INT, OUT_TYPE_INT));

        request(url, null);
    }

    public final void requestContentsTest(final Handler handler, final String url, final JSONObject json, final int requestId) {

        Thread thread = new Thread("jsonrequest") {
            public void run() {
                Log.i(TAG, "HTTP REQUEST : " + url);
                HttpClientManager client = new HttpClientManager();
                JSONObject response = client.httpPostRequestJSON(url, json);

                if (response == null) { // http response가 200이 오지 않으면 null임
                    Log.i(TAG, "HTTP_REQUEST_FAIL : " + url);
                    if (handler != null) {
                        if (mRetryCount < MAX_RETRY_COUNT) { // 응답을 받을 핸들러가 있는 것만 retry
                            mRetryCount++;
                            Log.i(TAG, "RETRY " + mRetryCount + " : " + url);
                            request(handler, url, json, requestId);
                        } else {
                            handler.sendMessage(handler.obtainMessage(ServiceCommon.MSG_HTTP_REQUEST_FAIL, requestId, 0, null));
                            Preferences.setServerRequestSuccess(mContext, false); // 성공 여부를 preferences에 기록
                        }
                    }
                } else {
                    Log.i(TAG, "HTTP_REQUEST_SUCCESS : " + url);
                    if (response.toString().length() < 100) {
                        Log.i(TAG, "response : " + response.toString());
                    }
                    if (handler != null) {
                        handler.sendMessage(handler.obtainMessage(ServiceCommon.MSG_HTTP_REQUEST_SUCCESS, requestId, 0, response));
                        Preferences.setServerRequestSuccess(mContext, true); // 성공 여부를 preferences에 기록
                    }
                }
            }
        };

        thread.start();
    }

}
