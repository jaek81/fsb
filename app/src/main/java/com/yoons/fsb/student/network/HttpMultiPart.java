package com.yoons.fsb.student.network;

import android.os.AsyncTask;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.RecData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * http 클라이언트 클래스
 *
 * @author ejlee
 */
public class HttpMultiPart extends AsyncTask<RecData, Integer, Integer> {
    /**
     * 학습 결과를 Server로 전송한다.
     */
    private final String TAG = "HttpMultiPart";
    private String lineEnd = "\r\n";
    private String twoHyphens = "--";
    private String boundary = "*****";
    private final String CRLF = "\r\n";
    private final String PREFIX = "--";
    private final String SUFFIX = PREFIX;
    private final String BOUNDARY = "--------multipart-boundary--------";

    private String mName = null;
    private String mPassword = null;
    private ArrayList<File> mFileList = null;

    private final int CONN_TIMEOUT = 10 * 1000;
    private final int READ_TIMEOUT = 10 * 1000;
    private final int BUFFER_SIZE = 1024 * 2;
    private final long UPLOAD_LIMIT_SIZE = 52428800; // 50MB(1024 * 1024 * 50)

    private boolean mNotEnoughSpace = false;
    private boolean mExceedUploadSize = false;

    private HttpURLConnection conn = null;
    private DataOutputStream dos = null;
    private InputStream is = null;
    private FileInputStream fileInputStream = null;

    private int mRetryCount = 0;
    private static final int MAX_RETRY_COUNT = 2;
    private static final String IN_PARAM = "in";
    private static final String OUT_PARAM = "out";
    private static final String OUT_TYPE_CURSOR = "cursor";

    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;

    @Override
    protected Integer doInBackground(RecData... params) {
        // TODO Auto-generated method stub

        RecData recData = params[0];
        if (CommonUtil.isCenter()) {//중앙화 모드
            try {

                CloudStorageAccount storageAccount = CloudStorageAccount.parse(ServiceCommon.storageConnectionString);
                CloudFileClient fileClient = storageAccount.createCloudFileClient();
                CloudFileShare share = fileClient.getShareReference("test");
                CloudFileDirectory rootDir = share.getRootDirectoryReference();

                // Get a reference to the sampledir directory
                CloudFileDirectory sampleDir = rootDir.getDirectoryReference("test");

                // 디렉토리 생성
                if (sampleDir.createIfNotExists()) {
                    System.out.println("sampledir created");
                } else {
                    System.out.println("sampledir already exists");
                }

                // 스토리지 로 업로드//해당 스토리지는 http로 다운이 안되서 blob로 변경

                CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

                CloudBlobContainer container = blobClient.getContainerReference("blobbasicscontainer");
                // 존재하지 않는 컨테이너를 만듭니다.
                container.createIfNotExists();
                // 사용 권한 개체 만들기
                BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
                // 사용 권한 개체에 공용 액세스 포함
                containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                // 컨테이너에 대한 권한 설정
                container.uploadPermissions(containerPermissions);

                String path = "";

                StudyData mStudyData = StudyData.getInstance();
                File file = new File(recData.file_path);

                path = "mp3/" + mStudyData.mStudyPlanYmd +"/"+ mStudyData.forestCode + "/" + mStudyData.teacherCode + "/" + mStudyData.mCustomerNo + "/" + file.getName();

                CloudBlockBlob blob = container.getBlockBlobReference(path);
                blob.uploadFromFile(recData.file_path);
                recData.blob_url = blob.getUri().toString();

                String url = recData.url;

                Map<String, String> inParam = new LinkedHashMap<String, String>();
                if (recData.url.contains("QXNwX0xfU1RVRFlfUkVDT1JEX0RUTF9G")) {//오디오학습
                    inParam.put("in1", String.valueOf(recData.customer_no));
                    inParam.put("in2", String.valueOf(recData.product_no));
                    inParam.put("in3", String.valueOf(recData.study_result_no));
                    inParam.put("in4", String.valueOf(recData.book_detail_id));
                    inParam.put("in5", String.valueOf(recData.student_book_id));
                    inParam.put("in6", String.valueOf(recData.study_order));
                    inParam.put("in7", String.valueOf(recData.study_kind));
                    inParam.put("in8", URLEncoder.encode(recData.rec_type,"utf-8"));
                    inParam.put("in9", String.valueOf(recData.start_tag_time));
                    inParam.put("in10", String.valueOf(recData.end_tag_time));
                    inParam.put("in11", String.valueOf(recData.rec_len));
                    inParam.put("in12", String.valueOf(recData.rep_cnt));
                    inParam.put("in13", URLEncoder.encode(recData.blob_url,"utf-8"));
                    inParam.put("in14", URLEncoder.encode(recData.study_unit_code,"utf-8"));

                } else if (recData.url.contains("QXNwX0xfU1RVRFlfU0VOVEVOQ0VEVExfRg==")) {//sp
                    inParam.put("in1", String.valueOf(recData.customer_no));
                    inParam.put("in2", String.valueOf(recData.product_no));
                    inParam.put("in3", String.valueOf(recData.study_unit_code));
                    inParam.put("in4", String.valueOf(recData.re_study_cnt));
                    inParam.put("in5", String.valueOf(recData.study_kind));
                    inParam.put("in6", String.valueOf(recData.study_order));
                    inParam.put("in7", String.valueOf(recData.is_correct));
                    inParam.put("in8", String.valueOf(URLEncoder.encode(recData.is_correct_answer,"utf-8")));
                    inParam.put("in9", String.valueOf(URLEncoder.encode(recData.is_customer_answer,"utf-8")));
                    inParam.put("in10", String.valueOf(recData.is_upload));
                    inParam.put("in11", String.valueOf(recData.sentence_question_no));
                    inParam.put("in12", String.valueOf(recData.study_result_no));
                    inParam.put("in13", String.valueOf(recData.blob_url));

                } else if (recData.url.contains("QXNwX0xfU1RVRFlfUEFfU0VOVEVOQ0VEVExfRg==")) {//vip
                    inParam.put("in1", String.valueOf(recData.customer_no));
                    inParam.put("in2", String.valueOf(recData.product_no));
                    inParam.put("in3", String.valueOf(recData.study_unit_code));
                    inParam.put("in4", String.valueOf(recData.re_study_cnt));
                    inParam.put("in5", String.valueOf(recData.study_kind));
                    inParam.put("in6", String.valueOf(recData.study_order));
                    inParam.put("in7", String.valueOf(recData.is_correct));
                    inParam.put("in8", String.valueOf(URLEncoder.encode(recData.is_correct_answer,"utf-8")));
                    inParam.put("in9", String.valueOf(URLEncoder.encode(recData.is_customer_answer,"utf-8")));
                    inParam.put("in10", String.valueOf(recData.is_upload));
                    inParam.put("in10", String.valueOf(recData.reTryCnt));
                    inParam.put("in11", String.valueOf(recData.sentence_question_no));
                    inParam.put("in12", String.valueOf(recData.study_result_no));
                    inParam.put("in13", String.valueOf(recData.blob_url));
                }

                url = addInParam(url, inParam);
                url = addOutParam(url, createParam(OUT_TYPE_CURSOR));

                Log.i(TAG, "URL: " + url);


                HttpClientManager client = new HttpClientManager();
                JSONObject response = client.httpPostRequestJSON(url, null);//결과값


                while (response == null && mRetryCount < MAX_RETRY_COUNT) {
                    mRetryCount++;
                    Log.i(TAG, "RETRY " + mRetryCount + " : " + url);
                    response = client.httpPostRequestJSON(url, null);
                }

                if (response != null) {
                    Log.i(TAG, response.toString());
                    ObjectMapper mapper = new ObjectMapper();
                  /*  try {
                        //결과 맵에 담기
                        JSONObject out1 = new JSONObject(response.toString()).getJSONObject("out1");

                    *//*    Iterator<String> keysItr = out1.keys();
                        while (keysItr.hasNext()) {
                            String key = keysItr.next();
                            String value = out1.getString(key);
                            result.put(key.toLowerCase(), value);
                        }
                        result.put("ret_code", "0000");//
                    } catch (Exception e) {
                        e.printStackTrace();
                        result.put("ret_code", "3001");//호출실패*//*
                    }*/

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {

                // open a URL connection to the Servlet

                URL url = new URL(recData.url);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                //conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            /*
             * conn.setRequestProperty("uploaded_file", fileName);
			 * conn.setRequestProperty("audiotype", "vip");
			 */

                ///

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(setValue("I_CUSTOMER_NO", String.valueOf(recData.customer_no)));
                dos.writeBytes(setValue("I_PRODUCT_NO", String.valueOf(recData.product_no)));
                dos.writeBytes(setValue("I_STUDY_UNIT_CODE", URLEncoder.encode(recData.study_unit_code, "UTF-8")));
                dos.writeBytes(setValue("I_RE_STUDY_CNT", String.valueOf(recData.re_study_cnt)));
                dos.writeBytes(setValue("I_STUDY_KIND", String.valueOf(recData.study_kind)));
                dos.writeBytes(setValue("I_STUDY_ORDER", String.valueOf(recData.study_order)));
                dos.writeBytes(setValue("I_IS_CORRECT", String.valueOf(recData.is_correct)));
                dos.writeBytes(setValue("I_CORRECT_ANSWER", recData.is_correct_answer));
                dos.writeBytes(setValue("I_CUSTOMER_ANSWER", recData.is_customer_answer == null ? "" : recData.is_customer_answer));
                dos.writeBytes(setValue("I_IS_UPLOAD", String.valueOf(recData.is_upload)));
                dos.writeBytes(setValue("I_RE_STUDY_CNT", String.valueOf(recData.re_study_cnt)));
                dos.writeBytes(setValue("I_SENTENCE_QUESTION_NO", String.valueOf(recData.sentence_question_no)));
                dos.writeBytes(setValue("I_CLASS_ID", String.valueOf(recData.study_result_no)));

                dos.writeBytes(setValue("I_BOOK_DETAIL_ID", String.valueOf(recData.book_detail_id)));
                dos.writeBytes(setValue("I_STUDENT_OF_BOOK_ID", String.valueOf(recData.student_book_id)));
                dos.writeBytes(setValue("I_STUDY_RESULT_NO", String.valueOf(recData.study_result_no)));
                dos.writeBytes(setValue("I_REC_TYPE", String.valueOf(recData.rec_type)));
                dos.writeBytes(setValue("I_START_TIME", String.valueOf(recData.start_tag_time)));
                dos.writeBytes(setValue("I_END_TIME", String.valueOf(recData.end_tag_time)));
                dos.writeBytes(setValue("I_REC_LEN", String.valueOf(recData.rec_len)));
                dos.writeBytes(setValue("I_RETRY_CNT", String.valueOf(recData.reTryCnt)));
                dos.writeBytes(setValue("I_REP_CNT", String.valueOf(recData.rep_cnt)));

                Log.e(TAG, recData.toString());
            /* if (new File(recData.file_path).isFile()) { */
                fileInputStream = new FileInputStream(recData.file_path);

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + recData.file_path + "\"" + lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }
                dos.writeBytes(lineEnd);

                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                dos.flush();

                is = conn.getInputStream();

                // retrieve the response from server
                int ch;

                StringBuffer b = new StringBuffer();
                while ((ch = is.read()) != -1) {
                    b.append((char) ch);
                }
                String s = b.toString();
                Log.i("Response", s);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    if (null != dos) {
                        dos.close();
                    }
                    if (null != is) {
                        is.close();
                    }
                    if (null != fileInputStream) {
                        fileInputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (null != conn) {
                    conn.disconnect();
                    conn = null;
                }
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);

    }

    /**
     * Map 형식으로 Key와 Value를 셋팅한다.
     *
     * @param key   : 서버에서 사용할 변수명
     * @param value : 변수명에 해당하는 실제 값
     * @return
     */

    private String setValue(String key, String value) {

        String result = "";

        result = result.concat(twoHyphens + boundary + lineEnd);
        result = result.concat("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
        result = result.concat(lineEnd);
        result = result.concat(value);
        result = result.concat(lineEnd);

        return result;
    }

    /**
     * In 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addInParam(String url, Map<String, String> param) {
        if (param.size() == 0) {
            Log.e(TAG, "param size error");
            return url;
        }
        Object[] str_param = param.values().toArray();
        for (int i = 0; i < str_param.length; i++) {
            url += "&" + IN_PARAM + String.valueOf(i + 1) + "=" + str_param[i];
        }
        return url;
    }

    /**
     * Out 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addOutParam(String url, String param[]) {
        if (param.length == 0) {
            Log.e(TAG, "param size error");
            return url;
        }

        for (int i = 0; i < param.length; i++) {
            url += "&" + OUT_PARAM + String.valueOf(i + 1) + "=" + param[i];
        }

        return url;
    }

    /**
     * 파라미터 생성
     *
     * @param params : 가변 스트링 파라미터 문자열
     * @return 스트링 배열
     */
    public String[] createParam(String... params) {
        String param[] = new String[params.length];
        for (int i = 0; i < param.length; i++) {
            param[i] = (String) params[i];
        }

        return param;
    }


}
