package com.yoons.fsb.student.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class RequestPhpGet extends AsyncTask<Map<String, String>, Integer, Map<String, String>> {

    private final String TAG = "RequestPhpGet";

    HttpURLConnection conn = null;
    InputStream is = null;

    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;

    private Context mContext;

    private String mcode = "";
    private String macAddress = "";
    private String userNo = "";

    public RequestPhpGet(Context context) {

        mContext = context;
        mcode = String.valueOf(Preferences.getCustomerNo(mContext));
        macAddress = String.valueOf(CommonUtil.getMacAddress());
        userNo = String.valueOf(Preferences.getCustomerNo(mContext));
    }

    @Override
    protected Map<String, String> doInBackground(Map<String, String>... paramList) {

        Map<String, String> result = new LinkedHashMap<String, String>();

        if (!CommonUtil.isCenter()) {
            try {

                String urlstr = ServiceCommon.SEC_SERVER_URL;
                urlstr += "?REQ_ID=";
                urlstr += paramList[0].get("REQ_ID");
                urlstr += "&MCODE=";
                urlstr += mcode;

                Log.e(TAG, urlstr);

                URL url = new URL(urlstr);

                // Open a HTTP connection to the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "text/html");

                is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String line = br.readLine();// RET_CODE=0000<br/>POINT_NAME=IT개발팀<br/>POINT_IP=10.1.2.226<br/>POINT_URL=10.1.2.226
                if (line != null && !"".equals(line)) {
                    String[] array = line.split("<br/>");
                    for (String str : array) {
                        String[] valArray = str.split("=");
                        if (valArray.length == 2) {
                            result.put(valArray[0].toLowerCase().trim(), valArray[1].trim());
                        } else {// step=""인경우가 잇어서
                            result.put(valArray[0].toLowerCase().trim(), "");
                        }
                    }

                }
                if (line != null)
                    Log.e(TAG, line);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (null != is) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (null != conn) {
                    conn.disconnect();
                    conn = null;
                }
            }
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     *
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // 프로그레스바
        super.onProgressUpdate(values);
    }

    public void getWeekStudyResult() {

        Map<String, String> params = new HashMap<String, String>();

        params.put("REQ_ID", ServiceCommon.GWSI);
        //params.put("MCODE", mcode);

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }
}
