package com.yoons.fsb.student.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * http 클라이언트 클래스(LMS)
 *
 * @author ejlee
 */
public class RequestPhpPost extends AsyncTask<Map<String, String>, Integer, Map<String, String>> {
    /**
     * 학습 결과를 Server로 전송한다.
     */
    private static final String SP_API_M_ALGIN = "U1BfQVBJX01fQUxHSU4=";//
    private static final String SP_API_M_AUTH = "U1BfQVBJX01fQVVUSA==";//기기의 MAC 어드레스로 MCODE를 얻어 옴
    private static final String SP_API_M_BDIF = "U1BfQVBJX01fQkRJRg==";//차시 정보를 얻어옴
    private static final String SP_API_M_BINF = "U1BfQVBJX01fQklORg==";//교재 정보를 얻어 옴
    private static final String SP_API_M_CASE = "U1BfQVBJX01fQ0FTRQ==";//
    private static final String SP_API_M_CHECK_NO = "U1BfQVBJX01fQ0hFQ0tfTk8=";//
    private static final String SP_API_M_CREATE_NO = "U1BfQVBJX01fQ1JFQVRFX05P";//
    private static final String SP_API_M_DBTEST = "U1BfQVBJX01fREJURVNU";//
    private static final String SP_API_M_DEL_NO = "U1BfQVBJX01fREVMX05P";//
    private static final String SP_API_M_DINF = "U1BfQVBJX01fRElORg==";//학습기의 정보 (자리 번호 등)를 얻어 옴
    private static final String SP_API_M_GETS = "U1BfQVBJX01fR0VUUw==";//"사용자가 학습할 다수의 교재 정보를 받아옴 제2교시 추가로 인해 마지막 차시에 대한 정보를 받아옴"
    private static final String SP_API_M_GHSI = "U1BfQVBJX01fR0hTSQ==";//"중단된 학습의 경우 가장 마지막 전송된 로그를 다시 받아와서 학습을 이어 간다."
    private static final String SP_API_M_HEAD = "U1BfQVBJX01fSEVBRA==";//공통부분을 송수신 한다
    private static final String SP_API_M_LGIN = "U1BfQVBJX01fTEdJTg==";//사용자가 로그인하여 스스로 인증함
    private static final String SP_API_M_LGINAPP = "U1BfQVBJX01fTEdJTkFQUA==";//
    private static final String SP_API_M_LSIF = "U1BfQVBJX01fTFNJRg==";//바로 직전에 학습한 차시 정보를 얻어 옴
    private static final String SP_API_M_MCQI = "U1BfQVBJX01fTUNRSQ==";//중앙영어교재에 사용되는 추가활동정보를 얻어 옴
    private static final String SP_API_M_MERR = "U1BfQVBJX01fTUVSUg==";//
    private static final String SP_API_M_PQAI = "U1BfQVBJX01fUFFBSQ==";//팝퀴즈의 내용 및 사용자 답안을 LMS 서버에 입력한다.
    private static final String SP_API_M_QGET = "U1BfQVBJX01fUUdFVA==";//2교시 학습시작 가능여부
    private static final String SP_API_M_QINF = "U1BfQVBJX01fUUlORg==";//2교시 학습시작
    private static final String SP_API_M_QLOG = "U1BfQVBJX01fUUxPRw==";//2교시 학습 문제 정오답 로그를 기록한다.
    private static final String SP_API_M_QORE = "U1BfQVBJX01fUU9SRQ==";//2교시 음성인식
    private static final String SP_API_M_QPRO = "U1BfQVBJX01fUVBSTw==";//2교시 진행사항을 기록하여 불완전 종료 여부를 확인 한다.
    private static final String SP_API_M_QTLG = "U1BfQVBJX01fUVRMRw==";//일간 주간 평가 문항에 대한 답안…
    private static final String SP_API_M_QUST = "U1BfQVBJX01fUVVTVA==";//2교시 학습시 문제 출력 (단어/구, 문장 함께제공)을 확인 할수 있다.
    private static final String SP_API_M_RCOP = "U1BfQVBJX01fUkNPUA==";//학습기에서 수행할 명령을 받아온다.
    private static final String SP_API_M_RCOPINS = "U1BfQVBJX01fUkNPUElOUw==";//
    private static final String SP_API_M_RESET = "U1BfQVBJX01fUkVTRVQ=";//
    private static final String SP_API_M_RFUL = "U1BfQVBJX01fUkZVTA==";//오디오 학습 중 자동 ARTT로 녹음하는 파일을 업로드 한다.
    private static final String SP_API_M_ROOM = "U1BfQVBJX01fUk9PTQ==";//
    private static final String SP_API_M_SLOG = "U1BfQVBJX01fU0xPRw==";//학습 로그 기록
    private static final String SP_API_M_Soaf_Check = "U1BfQVBJX01fU29hZl9DaGVjaw==";//
    private static final String SP_API_M_SSQI = "U1BfQVBJX01fU1NRSQ==";//해당 차시의 특정 문장 녹음 학습의 정보를 얻어 옴
    private static final String SP_API_M_STAT = "U1BfQVBJX01fU1RBVA==";//학습기에서 주기적으로 학습기 상태를 서버에 전송
    private static final String SP_API_M_STRT = "U1BfQVBJX01fU1RSVA==";//수신된 학습목록 중 회원이 선택하여 진행할 학습을 서버에 전송
    private static final String SP_API_M_TIME = "U1BfQVBJX01fVElNRQ==";//현재 시간을 얻어옴
    private static final String SP_API_M_UINF = "U1BfQVBJX01fVUlORg==";//사용자 정보를 얻어 옴
    private static final String SP_API_M_VCQI = "U1BfQVBJX01fVkNRSQ==";//해당 차시의 특정 문장 녹음 학습의 정보를 얻어 옴
    private static final String SP_API_M_VCRE = "U1BfQVBJX01fVkNSRQ==";//VC녹음파일 서버 전송 후 음성인식을 실시 결과값은 VRRC로 받는다.
    private static final String SP_API_M_VORE = "U1BfQVBJX01fVk9SRQ==";//녹음파일 서버 전송 후 음성인식을 실시 결과값은 VRRC로 받는다.
    private static final String SP_API_M_VREX = "U1BfQVBJX01fVlJFWA==";//음성인식을 실행하는 프로그램에 인자로 넘겨준다.
    private static final String SP_API_M_VRRC = "U1BfQVBJX01fVlJSQw==";//VC녹음파일 서버 전송 후 음성인식을 실시 결과값은 VRRC로 받는다.
    private static final String SP_API_M_VRRS = "U1BfQVBJX01fVlJSUw==";//음성인식을 실행하는 프로그렘에서 결과값을 DB에 저장한다.
    private static final String SP_API_M_WAIF = "U1BfQVBJX01fV0FJRg==";//특정 차시의 오답 정보를 얻어옴
    private static final String SP_API_M_WSAI = "U1BfQVBJX01fV1NBSQ==";//해당 차시의 단어 학습의 답안을 입력
    private static final String SP_API_M_WSQI = "U1BfQVBJX01fV1NRSQ==";//해당 차시의 특정 단어 학습의 정보를 얻어 옴


    private final String TAG = "RequestPhpPost";
    private String lineEnd = "\r\n";
    private String twoHyphens = "--";
    private String boundary = "*****";
    private final String CRLF = "\r\n";
    private final String PREFIX = "--";
    private final String SUFFIX = PREFIX;
    private final String BOUNDARY = "--------multipart-boundary--------";

    HttpURLConnection conn = null;
    DataOutputStream dos = null;
    InputStream is = null;

    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;

    private Context mContext;

    private String mcode = "";
    private String macAddress = "";
    private String userNo = "";

    //중앙화
    private int mRetryCount = 0;
    private static final int MAX_RETRY_COUNT = 2;
    private static final String IN_PARAM = "in";
    private static final String OUT_PARAM = "out";
    private static final String PKG_NAME = "p_nm";
    private static final String REQ_ID = "REQ_ID";
    private static final String OUT_TYPE_INT = "int";
    private static final String OUT_TYPE_STRING = "string";
    private static final String OUT_TYPE_CURSOR = "cursor";
    //!중앙화

    public RequestPhpPost(Context context) {

        mContext = context;
        mcode = String.valueOf(Preferences.getCustomerNo(mContext));
        macAddress = String.valueOf(CommonUtil.getMacAddress());
        userNo = String.valueOf(Preferences.getCustomerNo(mContext));

    }

    @Override
    protected Map<String, String> doInBackground(Map<String, String>... paramList) {

        Map<String, String> result = new LinkedHashMap<String, String>();

        if (CommonUtil.isCenter()) {//중앙화 모드

            String url = addPackageName(paramList[0].get(PKG_NAME));
            try {


                if (SP_API_M_QORE.equals(paramList[0].get(PKG_NAME))) {
                    CloudStorageAccount storageAccount = CloudStorageAccount.parse(ServiceCommon.storageConnectionString);
                    CloudFileClient fileClient = storageAccount.createCloudFileClient();
                    CloudFileShare share = fileClient.getShareReference("test");
                    CloudFileDirectory rootDir = share.getRootDirectoryReference();

                    // Get a reference to the sampledir directory
                    CloudFileDirectory sampleDir = rootDir.getDirectoryReference("test");

                    // 디렉토리 생성
                    if (sampleDir.createIfNotExists()) {
                        System.out.println("sampledir created");
                    } else {
                        System.out.println("sampledir already exists");
                    }

                    // 스토리지 로 업로드//해당 스토리지는 http로 다운이 안되서 blob로 변경

                    CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

                    CloudBlobContainer container = blobClient.getContainerReference("blobbasicscontainer");
                    // 존재하지 않는 컨테이너를 만듭니다.
                    container.createIfNotExists();
                    // 사용 권한 개체 만들기
                    BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
                    // 사용 권한 개체에 공용 액세스 포함
                    containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                    // 컨테이너에 대한 권한 설정
                    container.uploadPermissions(containerPermissions);

                    String path = "";

                    SecStudyData mStudyData = SecStudyData.getInstance();
                    File file = new File(paramList[0].get("REC_FILE"));

                    path = "mp3/" + mStudyData.getClass_study_ymd() + "/" + mStudyData.getForestCode() + "/" + mStudyData.getTeacherCode() + "/" + mStudyData.getMcode() + "/" + file.getName();

                    CloudBlockBlob blob = container.getBlockBlobReference(path);
                    blob.uploadFromFile(paramList[0].get("REC_FILE"));


                    paramList[0].put("REC_FILE", URLEncoder.encode(blob.getUri().toString(), "utf-8"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            paramList[0].remove(PKG_NAME);
            paramList[0].remove(REQ_ID);

            url = addInParam(url, paramList[0]);
            url = addOutParam(url, createParam(OUT_TYPE_CURSOR));

            Log.i(TAG, "URL: " + url);

            HttpClientManager client = new HttpClientManager();
            JSONObject response = client.httpPostRequestJSON(url, null);//결과값


            while (response == null && mRetryCount < MAX_RETRY_COUNT) {
                mRetryCount++;
                Log.i(TAG, "RETRY " + mRetryCount + " : " + url);
                response = client.httpPostRequestJSON(url, null);
            }
            if (response != null) {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    //결과 맵에 담기
                    JSONObject out1 = new JSONObject(response.toString()).getJSONArray("out1").getJSONObject(0);

                    Iterator<String> keysItr = out1.keys();
                    while (keysItr.hasNext()) {
                        String key = keysItr.next();
                        String value = out1.getString(key);
                        result.put(key.toLowerCase(), value);
                    }
                    result.put("ret_code", "0000");//
                } catch (Exception e) {
                    e.printStackTrace();
                    result.put("ret_code", "3001");//호출실패
                }
            } else {
                result.put("ret_code", "3001");//호출실패
            }

        } else {//로컬 모드일때
            try {

                // open a URL connection to the Servlet

                URL url = new URL(ServiceCommon.SEC_SERVER_URL);

                // Open a HTTP connection to the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                ///

                Map<String, String> params = new LinkedHashMap<String, String>();
                params.putAll(paramList[0]);

                Log.e(TAG, params.toString());

                dos = new DataOutputStream(conn.getOutputStream());
                Iterator<String> iterator = params.keySet().iterator();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if ("REC_FILE".equals(key)) {// 파일일때)}
                        fileWrite(params.get(key));
                    } else {
                        dos.writeBytes(setValue(key, params.get(key)));
                    }

                }

                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                dos.flush();

                is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String line = br.readLine();// RET_CODE=0000<br/>POINT_NAME=IT개발팀<br/>POINT_IP=10.1.2.226<br/>POINT_URL=10.1.2.226
                if (line != null && !"".equals(line)) {
                    String[] array = line.split("<br/>");
                    for (String str : array) {
                        String[] valArray = str.split("=");
                        if (valArray.length == 2) {
                            result.put(valArray[0].toLowerCase().trim(), valArray[1].trim());
                        } else {// step=""인경우가 잇어서
                            result.put(valArray[0].toLowerCase().trim(), "");
                        }
                    }

                }
                if (line != null)
                    Log.e(TAG, line);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (null != dos) {
                        dos.close();
                    }
                    if (null != is) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (null != conn) {
                    conn.disconnect();
                    conn = null;
                }
            }
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     *
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // 프로그레스바
        super.onProgressUpdate(values);
    }

    /*
     * 2교시학습시작 (QINF)
     */
    public void getQINF(int last_book_detail_id) {

        /**
         * 여긴 나중에 삭제해야함(아직 중간 통신이 없어서 넣어둠)나중에 이게 처음이 아니라면 여기보다 앞으로 가야함
         */
        // mcode = "2121212";//나중에

        /**
         *
         */

        Map<String, String> params = new LinkedHashMap<String, String>();

        params.put(REQ_ID, ServiceCommon.SEC_QINF);


        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_QINF);
            params.put("i_ForestCode", Preferences.getForestCode(mContext));
            params.put("i_TeacherCode", Preferences.getTeacherCode(mContext));
        } else {
            params.put("LAST_BOOK_DETAIL_ID", String.valueOf(last_book_detail_id));
        }

        params.put("MCODE", mcode);
        params.put("MAC_ADDR", macAddress);
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

    }

    /**
     * 2교시에 학습해야할 문장, 단어/구를 출력한다.
     */
    public void getGETS() {

        Map<String, String> params = new LinkedHashMap<String, String>();

        params.put(REQ_ID, ServiceCommon.SEC_GETS);
        params.put("MCODE", mcode);

        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_GETS);
        }

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

        // params.put("MCODE", "31729364");
        // params.put("CLASS_SECOND_ID", "31729364");
        // params.put("LAST_BOOK_DETAIL_ID", "31729364");

    }

    /**
     * 2교시 학습시작이 가능한지에 대한 여부를 알 수 있다.
     */
    public void getQGET() {


        Map<String, String> params = new LinkedHashMap<String, String>();

        params.put(REQ_ID, ServiceCommon.SEC_QGET);


        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_QGET);
            params.put("i_ForestCode", Preferences.getForestCode(mContext));
            params.put("i_TeacherCode", Preferences.getTeacherCode(mContext));
        }
        params.put("MCODE", mcode);
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

        // params.put("MCODE", "31729364");
        // params.put("CLASS_SECOND_ID", "31729364");
        // params.put("LAST_BOOK_DETAIL_ID", "31729364");

    }

    /**
     * Map 형식으로 Key와 Value를 셋팅한다.
     *
     * @param key   : 서버에서 사용할 변수명
     * @param value : 변수명에 해당하는 실제 값
     * @return
     * @throws IOException
     */
    private String setValue(String key, String value) throws IOException {

        String result = "";

        result = result.concat(twoHyphens + boundary + lineEnd);
        result = result.concat("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
        result = result.concat(lineEnd);
        result = result.concat(value);
        result = result.concat(lineEnd);

        return result;
    }

    public void sendQORE(int book_id, int book_detail_id, int class_second_id, int quiz_no, String rec_file,
                         int study_type) {

        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(REQ_ID, ServiceCommon.SEC_QORE);
        params.put("MAC_ADDR", macAddress);
        params.put("MCODE", mcode);
        if (!CommonUtil.isCenter()) {
            params.put("BOOK_ID", String.valueOf(book_id));
        }
        params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        params.put("QUIZ_NO", String.valueOf(quiz_no));
        params.put("REC_FILE", rec_file);
        params.put("CLASS_SECOND_ID", String.valueOf(class_second_id));
        params.put("STUDY_TYPE", String.valueOf(study_type));
        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_QORE);
        }

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

    }

    private void fileWrite(String file_path) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file_path);
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"userfile\";filename=\"" + file_path + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            // create a buffer of maximum size
            bytesAvailable = fileInputStream.available();

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }
            dos.writeBytes(lineEnd);
        } catch (IOException e) {
            throw e;
        } finally {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
        }
        // dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

    }

    public void sendQLOG(int user_no, int book_detail_id, int quiz_no, int answer, String filename, int class_second_id,
                         int study_type) {

        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(REQ_ID, ServiceCommon.SEC_QLOG);
        params.put("MAC_ADDR", macAddress);
        params.put("USER_NO", String.valueOf(user_no));
        params.put("MCODE", mcode);
        params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        params.put("QUIZ_NO", String.valueOf(quiz_no));
        params.put("ANSWER", String.valueOf(answer));
        params.put("FILENAME", filename);
        params.put("CLASS_SECOND_ID", String.valueOf(class_second_id));
        params.put("STUDY_TYPE", String.valueOf(study_type));

        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_QLOG);
        }
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }

    public void getBDIF(int book_id, int book_detail_id, String study_unit_code) {

        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(REQ_ID, ServiceCommon.SEC_BDIF);
        params.put("BOOK_ID", String.valueOf(book_id));
        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_BDIF);
            try {
                params.put("BOOK_DETAIL_NAME", URLEncoder.encode(study_unit_code, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                params.put("BOOK_DETAIL_NAME", study_unit_code);
            }
        } else {
            params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        }
        params.put("PART_GB", String.valueOf(1));


        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

    }

    public void sendQTLG(int book_detail_id, int quiz_no, int answer, int class_id) {

        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(REQ_ID, ServiceCommon.SEC_QTLG);
        params.put("MAC_ADDR", macAddress);
        if (!CommonUtil.isCenter()) {
            params.put("USER_NO", String.valueOf(userNo));
        }
        params.put("MCODE", mcode);
        params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        params.put("QUIZDW_NO", String.valueOf(quiz_no));
        params.put("ANSWER", String.valueOf(answer));
        params.put("CLASS_ID", String.valueOf(class_id));

        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_QTLG);
        }


        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }

    public void sendQPRO(int book_id, int class_second_id, int book_detail_id, String step, int play_time, String status, int pos, String time) {

        Map<String, String> params = new LinkedHashMap<String, String>();

        params.put(REQ_ID, ServiceCommon.SEC_QPRO);
        params.put("MCODE", mcode);
        params.put("BOOK_ID", String.valueOf(book_id));
        params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        params.put("CLASS_SECOND_ID", String.valueOf(class_second_id));
        params.put("STEP", step);
        params.put("PLAY_TIME", String.valueOf(play_time));
        params.put("STATUS", status);
        params.put("POS", String.valueOf(pos));
        params.put("TIME", time);
        params.put("MAC_ADDR", macAddress);

        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_QPRO);
        }

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }

    public void sendSLOG(int book_id, int book_detail_id, int class_id, String step, int play_time, String status, int pos, String time, int step_duration, int rec_a_cnt, int rec_a_time, int rec_m_cnt, int rec_m_time, int rec_r_time) {

        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(REQ_ID, ServiceCommon.SEC_SLOG);
        params.put("MCODE", mcode);
        params.put("BOOK_ID", String.valueOf(book_id));
        params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        params.put("CLASS_ID", String.valueOf(class_id));
        params.put("STEP", step);
        params.put("PLAY_TIME", String.valueOf(play_time));
        params.put("STATUS", status);
        params.put("POS", String.valueOf(pos));
        params.put("TIME", time);
        params.put("STEP_DURATION", String.valueOf(step_duration));
        params.put("MAC_ADDR", macAddress);
        params.put("REC_A_CNT", String.valueOf(rec_a_cnt));
        params.put("REC_A_TIME", String.valueOf(rec_a_time));
        params.put("REC_M_CNT", String.valueOf(rec_m_cnt));
        params.put("REC_M_TIME", String.valueOf(rec_m_time));
        params.put("REC_R_TIME", String.valueOf(rec_r_time));

        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_SLOG);
        }

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }

    /**
     * 사용자가 로그인하여 스스로 인증함
     *
     * @param user_no 1236
     * @param ex_lgin 1:로그인 연장, 0 : default (ys20은 0으로만 )
     */
    public void getLGIN(String user_no, String ex_lgin) {


        String agency_no = Preferences.getForestCode(mContext);
        Map<String, String> params = new LinkedHashMap<String, String>();

        params.put(REQ_ID, ServiceCommon.LGIN);
        params.put("MAC_ADDR", macAddress);
        if (CommonUtil.isCenter()) {
            try {
                params.put("USER_NO", URLEncoder.encode(user_no, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                params.put("USER_NO", user_no);
            }
            params.put(PKG_NAME, SP_API_M_LGIN);
        } else {
            params.put("USER_NO", user_no);
        }


        params.put("EX_LGIN", ex_lgin);
        params.put("i_ForestCode", Preferences.getForestCode(mContext));
        params.put("i_TeacherCode", Preferences.getTeacherCode(mContext));
        params.put("i_LMSStatus", Preferences.getLmsStatus(mContext));


        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

        // params.put("MCODE", "31729364");
        // params.put("CLASS_SECOND_ID", "31729364");
        // params.put("LAST_BOOK_DETAIL_ID", "31729364");

    }

    /***
     * 로그인 비활성화 기능에 따른 로그인CASE값(2 또는 3) 학습이 마친 상태에서 서버측에 전달한다.
     *
     * @param class_id
     * @param mCase
     */
    public void sendCASE(String class_id, String mCase) {

        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put(REQ_ID, ServiceCommon.CASE);
        params.put("MCODE", mcode);
        params.put("CLASS_ID", class_id);
        params.put("CASE", mCase);
        params.put("MAC_ADDR", macAddress);

        if (CommonUtil.isCenter()) {
            params.put(PKG_NAME, SP_API_M_CASE);
        }

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }


    /**
     * In 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addInParam(String url, Map<String, String> param) {
        if (param.size() == 0) {
            Log.e(TAG, "param size error");
            return url;
        }
        Object[] str_param = param.values().toArray();
        for (int i = 0; i < str_param.length; i++) {
            url += "&" + IN_PARAM + String.valueOf(i + 1) + "=" + str_param[i];
        }
        return url;
    }

    /**
     * Out 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addOutParam(String url, String param[]) {
        if (param.length == 0) {
            Log.e(TAG, "param size error");
            return url;
        }

        for (int i = 0; i < param.length; i++) {
            url += "&" + OUT_PARAM + String.valueOf(i + 1) + "=" + param[i];
        }

        return url;
    }

    /**
     * 패키지 이름 추가 함수
     *
     * @param name : 패키지 이름
     * @return
     */
    private String addPackageName(String name) {
        if (ServiceCommon.STUDY_SERVER_URL.length() == 0) {
            ServiceCommon.setServerUrl(mContext, "");
        }

        return ServiceCommon.STUDY_SERVER_URL + "?" + PKG_NAME + "=" + name;
    }

    /**
     * 파라미터 생성
     *
     * @param params : 가변 스트링 파라미터 문자열
     * @return 스트링 배열
     */
    public String[] createParam(String... params) {
        String param[] = new String[params.length];
        for (int i = 0; i < param.length; i++) {
            param[i] = (String) params[i];
        }

        return param;
    }

}
