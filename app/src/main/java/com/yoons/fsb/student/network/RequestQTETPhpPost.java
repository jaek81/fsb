package com.yoons.fsb.student.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData.OneWeekData;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * http 클라이언트 클래스
 *
 * @author ejlee
 */
public class RequestQTETPhpPost extends AsyncTask<Map<String, String>, Integer, ArrayList<OneWeekData>> {
    /**
     * 학습 결과를 Server로 전송한다.
     */
    private static final String SP_API_M_QTET = "U1BfQVBJX01fUVRFVA==";//일간/ 주간 평가 문항
    private final String TAG = "RequestPhpPost";
    private String lineEnd = "\r\n";
    private String twoHyphens = "--";
    private String boundary = "*****";
    private final String CRLF = "\r\n";
    private final String PREFIX = "--";
    private final String SUFFIX = PREFIX;
    private final String BOUNDARY = "--------multipart-boundary--------";

    private HttpURLConnection conn = null;
    private DataOutputStream dos = null;
    private InputStream is = null;


    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;


    //중앙화
    private int mRetryCount = 0;
    private static final int MAX_RETRY_COUNT = 2;
    private static final String IN_PARAM = "in";
    private static final String OUT_PARAM = "out";
    private static final String PKG_NAME = "p_nm";
    private static final String REQ_ID = "REQ_ID";
    private static final String OUT_TYPE_INT = "int";
    private static final String OUT_TYPE_STRING = "string";
    private static final String OUT_TYPE_CURSOR = "cursor";

    private int maxCnt = 0;

    private Context mContext;

    public RequestQTETPhpPost(Context context) {

        mContext = context;
    }

    @Override
    protected ArrayList<OneWeekData> doInBackground(Map<String, String>... paramList) {

        ArrayList<OneWeekData> result = new ArrayList<OneWeekData>();

        for (int i = 1; i < maxCnt + 1; i++) {
            if (CommonUtil.isCenter()) {//중앙화 모드
                String url = addPackageName(SP_API_M_QTET);

                paramList[0].remove(REQ_ID);

                paramList[0].put("QUIZ_SEQ", String.valueOf(i));

                url = addInParam(url, paramList[0]);
                url = addOutParam(url, createParam(OUT_TYPE_CURSOR));

                Log.i(TAG, "URL: " + url);

                HttpClientManager client = new HttpClientManager();
                JSONObject response = client.httpPostRequestJSON(url, null);//결과값

                Log.i(TAG, "response: " + response.toString());

                ObjectMapper om = new ObjectMapper();
                OneWeekData oneWeekData = null;
                try {
                    oneWeekData = om.readValue(recursiveJsonKeyConverterToLower(response.getJSONArray("out1").getJSONObject(0)).toString(), OneWeekData.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                result.add(oneWeekData);

            } else {
                try {

                    // open a URL connection to the Servlet

                    URL url = new URL(ServiceCommon.SEC_SERVER_URL);

                    // Open a HTTP  connection to  the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    ///

                    Map<String, String> params = new HashMap<String, String>();
                    params.putAll(paramList[0]);
                    params.put("QUIZ_SEQ", String.valueOf(i));

                    Log.e(TAG, params.toString());

                    dos = new DataOutputStream(conn.getOutputStream());
                    Iterator<String> iterator = params.keySet().iterator();
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        dos.writeBytes(setValue(key, params.get(key)));

                    }

                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    dos.flush();

                    is = conn.getInputStream();


                    BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    Map<String, String> map_result = new HashMap<String, String>();
                    String line = br.readLine();//RET_CODE=0000<br/>POINT_NAME=IT개발팀<br/>POINT_IP=10.1.2.226<br/>POINT_URL=10.1.2.226
                    if (line != null && !"".equals(line)) {
                        String[] array = line.split("<br/>");
                        for (String str : array) {
                            String[] valArray = str.split("=");
                            if (valArray.length == 2) {
                                map_result.put(valArray[0].toLowerCase(), valArray[1]);
                            } else {//step=""인경우가 잇어서
                                map_result.put(valArray[0].toLowerCase(), "");
                            }
                        }

                    }
                    Log.e(TAG, line);

                    //vo setting
                    ObjectMapper om = new ObjectMapper();
                    OneWeekData oneWeekData = null;

                    oneWeekData = om.readValue(om.writeValueAsString(map_result), OneWeekData.class);
                    result.add(oneWeekData);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (null != dos) {
                            dos.close();
                        }
                        if (null != is) {
                            is.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (null != conn) {
                        conn.disconnect();
                        conn = null;
                    }
                }

            }
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     *
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // 프로그레스바
        super.onProgressUpdate(values);
    }

    /**
     * Map 형식으로 Key와 Value를 셋팅한다.
     *
     * @param key   : 서버에서 사용할 변수명
     * @param value : 변수명에 해당하는 실제 값
     * @return
     * @throws IOException
     */
    private String setValue(String key, String value) throws IOException {

        String result = "";

        result = result.concat(twoHyphens + boundary + lineEnd);
        result = result.concat("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
        result = result.concat(lineEnd);
        result = result.concat(value);
        result = result.concat(lineEnd);

        return result;
    }

    public void sendQTET(String user_no, int book_detail_id, int maxCnt, String book_id, String book_detail_names) {
        String mcode = String.valueOf(Preferences.getCustomerNo(mContext));
        String macAddress = String.valueOf(CommonUtil.getMacAddress());
        this.maxCnt = maxCnt;

        Map<String, String> params = new LinkedHashMap<String, String>();
        if (CommonUtil.isCenter()) {
            params.put("BOOK_ID", book_id);
            try {
                params.put("BOOK_DETAIL_NAMES", URLEncoder.encode(book_detail_names, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                params.put("BOOK_DETAIL_NAMES", book_detail_names);
            }

        } else {
            params.put("REQ_ID", ServiceCommon.SEC_QTET);
            params.put("MAC_ADDR", macAddress);
            params.put("USER_NO", user_no);
            params.put("MCODE", mcode);
            params.put("BOOK_DETAIL_ID", String.valueOf(book_detail_id));
        }


        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
    }

    /**
     * 패키지 이름 추가 함수
     *
     * @param name : 패키지 이름
     * @return
     */
    private String addPackageName(String name) {
        if (ServiceCommon.STUDY_SERVER_URL.length() == 0) {
            ServiceCommon.setServerUrl(mContext, "");
        }

        return ServiceCommon.STUDY_SERVER_URL + "?" + PKG_NAME + "=" + name;
    }

    /**
     * In 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addInParam(String url, Map<String, String> param) {
        if (param.size() == 0) {
            Log.e(TAG, "param size error");
            return url;
        }
        Object[] str_param = param.values().toArray();
        for (int i = 0; i < str_param.length; i++) {
            url += "&" + IN_PARAM + String.valueOf(i + 1) + "=" + str_param[i];
        }
        return url;
    }

    /**
     * Out 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addOutParam(String url, String param[]) {
        if (param.length == 0) {
            Log.e(TAG, "param size error");
            return url;
        }

        for (int i = 0; i < param.length; i++) {
            url += "&" + OUT_PARAM + String.valueOf(i + 1) + "=" + param[i];
        }

        return url;
    }


    /**
     * 파라미터 생성
     *
     * @param params : 가변 스트링 파라미터 문자열
     * @return 스트링 배열
     */
    public String[] createParam(String... params) {
        String param[] = new String[params.length];
        for (int i = 0; i < param.length; i++) {
            param[i] = (String) params[i];
        }

        return param;
    }

    public JSONObject recursiveJsonKeyConverterToLower(JSONObject jsonObject) throws JSONException
    {
        JSONObject resultJsonObject = new JSONObject();
        @SuppressWarnings("unchecked") Iterator<String> keys = jsonObject.keys();
        while(keys.hasNext())
        {
            String key = keys.next();
            Object value = null;
            try
            {
                JSONObject nestedJsonObject = jsonObject.getJSONObject(key);
                value = this.recursiveJsonKeyConverterToLower(nestedJsonObject);
            }
            catch(JSONException jsonException)
            {
                value = jsonObject.get(key);
            }

            resultJsonObject.put(key.toLowerCase(), value);
        }

        return resultJsonObject;
    }


}
