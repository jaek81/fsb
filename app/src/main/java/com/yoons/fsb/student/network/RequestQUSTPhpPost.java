package com.yoons.fsb.student.network;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.PartData;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.service.AgentEvent;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ContentsUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.YdaConverter;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * http 클라이언트 클래스
 *
 * @author ejlee
 */
public class RequestQUSTPhpPost extends AsyncTask<Map<String, String>, Integer, SecStudyData> {
    /**
     * 학습 결과를 Server로 전송한다.
     */

    private final String TAG = "RequestQUSTPhpPost";
    private String lineEnd = "\r\n";
    private String twoHyphens = "--";
    private String boundary = "*****";
    private final String CRLF = "\r\n";
    private final String PREFIX = "--";
    private final String SUFFIX = PREFIX;
    private final String BOUNDARY = "--------multipart-boundary--------";

    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;

    private Context mContext;

    private int part1_cnt = 0;//part1카운트
    private int part2_cnt = 0;//part2 카운트
    private int stopStudy = 1;//중도학습 1:처음 2:중도
    private String step = "";//"", W1, W2, W3, S1, S2, S3, S4, N1(오답노트)

    private final int CONN_TIMEOUT = 10 * 3000;
    private final int READ_TIMEOUT = 10 * 3000;
    private boolean mIsCanceled = false;
    private File mFile = null;
    private int mFailReason = AgentEvent.REASON_NO_ERROR;
    private int mResCode = HttpURLConnection.HTTP_OK;
    private String mResInFo = "";
    private String LOG_TAG = "RequestQUSTPhpPost";
    private Exception mExceptionToBeThrown;
    int int_progress = 0;
    int quiz_seq = 1;

    //중앙화
    private int mRetryCount = 0;
    private static final int MAX_RETRY_COUNT = 2;
    private static final String IN_PARAM = "in";
    private static final String OUT_PARAM = "out";
    private static final String PKG_NAME = "p_nm";
    private static final String REQ_ID = "REQ_ID";
    private static final String OUT_TYPE_INT = "int";
    private static final String OUT_TYPE_STRING = "string";
    private static final String OUT_TYPE_CURSOR = "cursor";
    private static final String SP_API_M_QUST = "U1BfQVBJX01fUVVTVA==";//2교시 학습시 문제 출력 (단어/구, 문장 함께제공)을 확인 할수 있다.
    //!중앙화

    public RequestQUSTPhpPost(Context context) {

        mContext = context;
    }

    @Override
    protected SecStudyData doInBackground(Map<String, String>... paramList) {

        SecStudyData result = new SecStudyData();
        ArrayList<PartData> part1_list = new ArrayList<PartData>();//part1 문제
        ArrayList<PartData> part2_list = new ArrayList<PartData>();//part2 문제


        /**
         * PART1 데이터 만들기 20%
         */
        publishProgress(int_progress);
        if (CommonUtil.isCenter()) {//중앙화 모드
            result.setClass_study_ymd(paramList[0].get("CLASS_STUDY_YMD"));
            paramList[0].remove("CLASS_STUDY_YMD");


            for (int i = 1; i < part1_cnt + 1; i++) {//1부터시작
                // open a URL connection to the Servlet
                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                InputStream is = null;
                Map<String, String> mapResult = new LinkedHashMap<String, String>();
                try {
                    paramList[0].put(PKG_NAME, SP_API_M_QUST);
                    String url = addPackageName(paramList[0].get(PKG_NAME));

                    paramList[0].remove(PKG_NAME);
                    paramList[0].remove(REQ_ID);

                    ///
                    Map<String, String> params = new LinkedHashMap<>();

                    params.putAll(paramList[0]);
                    params.put("PART_GB", String.valueOf(ServiceCommon.PART_GB_PART1));
                    params.put("QUIZ_SEQ", String.valueOf(i));


                    url = addInParam(url, params);
                    url = addOutParam(url, createParam(OUT_TYPE_CURSOR));

                    Log.i(TAG, "URL: " + url);

                    HttpClientManager client = new HttpClientManager();
                    JSONObject response = client.httpPostRequestJSON(url, null);//결과값

                    ObjectMapper mapper = new ObjectMapper();
                    JSONObject out1 = null;
                    try {
                        //결과 맵에 담기
                        out1 = new JSONObject(response.toString()).getJSONArray("out1").getJSONObject(0);
                        Iterator<String> keysItr = out1.keys();
                        while (keysItr.hasNext()) {
                            String key = keysItr.next();
                            String value = out1.getString(key);
                            mapResult.put(key.toLowerCase(), value);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //vo setting
                    ObjectMapper om = new ObjectMapper();
                    PartData part1 = null;

                    part1 = om.readValue(om.writeValueAsString(mapResult), PartData.class);

                    String filePath;
                    String wordLastPath;
                    String wordLocalPath = String.format(ContentsUtil.WORD_LOCAL_PATH, part1.getBookserise(), part1.getBookvolume(), part1.getBookseq());

                    if (CommonUtil.isKorean(part1.getWord()) || CommonUtil.isNum(part1.getWord())) {
                        wordLastPath = part1.getMeans() + ContentsUtil.WORD_POSTFIX;
                        filePath = wordLocalPath + part1.getMeans() + ContentsUtil.WORD_POSTFIX;
                    } else {
                        wordLastPath = part1.getWord() + ContentsUtil.WORD_POSTFIX;
                        filePath = wordLocalPath + part1.getWord() + ContentsUtil.WORD_POSTFIX;
                    }
                    part1.setAudio_path(filePath);
                    part1.setDown_url(ContentsUtil.WORD_URL + wordLastPath);
                    part1.setRecord_path(String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_SP1_%d.wav", CommonUtil.getCurrentYYYYMMDDHHMMSS(), Integer.valueOf(paramList[0].get("MCODE")), Integer.valueOf(paramList[0].get("CLASS_SECOND_ID")) , part1.getBook_detail_id(), 1, i));
                    part1.setQuestionnumber(i);
                    part1_list.add(part1);
                } catch (Exception e) {
                    mExceptionToBeThrown = e;
                } finally {

                    try {
                        if (null != dos) {
                            dos.close();
                        }
                        if (null != is) {
                            is.close();
                        }
                        if (null != conn) {
                            conn.disconnect();
                            conn = null;
                        }
                    } catch (IOException e) {
                        mExceptionToBeThrown = e;
                    } finally {
                        if (null != conn) {
                            conn.disconnect();
                            conn = null;
                        }
                    }
                }

                maxTWpersent(0, i, part1_cnt);
            }

            /**
             * PART2 데이터 만들기 40%
             */
            for (int i = 1; i < part2_cnt + 1; i++) {
                // open a URL connection to the Servlet
                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                InputStream is = null;
                Map<String, String> mapResult = new LinkedHashMap<String, String>();
                try {
                    paramList[0].put(PKG_NAME, SP_API_M_QUST);
                    String url = addPackageName(paramList[0].get(PKG_NAME));

                    paramList[0].remove(PKG_NAME);
                    paramList[0].remove(REQ_ID);

                    ///
                    Map<String, String> params = new LinkedHashMap<>();

                    params.putAll(paramList[0]);
                    params.put("PART_GB", String.valueOf(ServiceCommon.PART_GB_PART2));
                    params.put("QUIZ_SEQ", String.valueOf(i));


                    url = addInParam(url, params);
                    url = addOutParam(url, createParam(OUT_TYPE_CURSOR));


                    Log.i(TAG, "URL: " + url);


                    HttpClientManager client = new HttpClientManager();
                    JSONObject response = client.httpPostRequestJSON(url, null);//결과값

                    ObjectMapper mapper = new ObjectMapper();
                    JSONObject out1 = null;
                    try {
                        //결과 맵에 담기
                        out1 = new JSONObject(response.toString()).getJSONArray("out1").getJSONObject(0);
                        Iterator<String> keysItr = out1.keys();
                        while (keysItr.hasNext()) {
                            String key = keysItr.next();
                            String value = out1.getString(key);
                            mapResult.put(key.toLowerCase(), value);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //vo setting
                    ObjectMapper om = new ObjectMapper();
                    PartData part2 = null;

                    part2 = om.readValue(om.writeValueAsString(mapResult), PartData.class);
                    String contentUrl = String.format(ServiceCommon.CONTENT_URL, part2.getBookserise(), part2.getBookvolume(), part2.getBookserise(), part2.getBookvolume(), part2.getBookseq());

                    String localPath = String.format(ContentsUtil.CONTENT_LOCAL_FILE_PREFIX, part2.getBookserise(), part2.getBookvolume(), part2.getBookseq(), part2.getBookserise(), part2.getBookvolume(), part2.getBookseq());
                    String filePath = String.format(localPath + ContentsUtil.SECOND_LOCAL_POSTFIX, part2.getSeq());

                    part2.setAudio_path(filePath);
                    part2.setDown_url(String.format(contentUrl + ContentsUtil.SECOND_POSTFIX, part2.getSeq()));
                    part2.setRecord_path(String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_SP2_%d.wav", CommonUtil.getCurrentYYYYMMDD(), Integer.valueOf(paramList[0].get("MCODE")), Integer.valueOf(paramList[0].get("CLASS_SECOND_ID")) , part2.getBook_detail_id(), 1, i));
                    if (part2.getVcautosentence().length() > 2) {
                        int cur_cnt = result.getVip_cnt();
                        result.setVip_cnt(cur_cnt + 1);
                    }

                    part2.setQuestionnumber(i);
                    part2_list.add(part2);

                } catch (Exception e) {
                    mExceptionToBeThrown = e;
                } finally {

                    try {
                        if (null != dos) {
                            dos.close();
                        }
                        if (null != is) {
                            is.close();
                        }
                    } catch (IOException e) {
                        mExceptionToBeThrown = e;
                    }
                    if (null != conn) {
                        conn.disconnect();
                        conn = null;
                    }

                }
                maxTWpersent(20, i, part2_cnt);
            }

        } else {
            for (int i = 1; i < part1_cnt + 1; i++) {//1부터시작
                // open a URL connection to the Servlet
                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                InputStream is = null;
                try {
                    URL url = new URL(ServiceCommon.SEC_SERVER_URL);

                    // Open a HTTP  connection to  the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    ///
                    Map<String, String> params = new HashMap<String, String>();

                    params.putAll(paramList[0]);
                    params.put("QUIZ_SEQ", String.valueOf(i));
                    params.put("PART_GB", String.valueOf(ServiceCommon.PART_GB_PART1));

                    Log.e(TAG, params.toString());

                    dos = new DataOutputStream(conn.getOutputStream());
                    Iterator<String> iterator = params.keySet().iterator();
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        dos.writeBytes(setValue(key, params.get(key)));
                    }

                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    dos.flush();

                    is = conn.getInputStream();

                    BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    String line = br.readLine();//RET_CODE=0000<br/>POINT_NAME=IT개발팀<br/>POINT_IP=10.1.2.226<br/>POINT_URL=10.1.2.226

                    Log.e(TAG, line);

                    Map<String, String> map_result = new HashMap<String, String>();
                    if (line != null && !"".equals(line)) {
                        String[] array = line.split("<br/>");
                        for (String str : array) {
                            String[] valArray = str.split("=");
                            if (valArray.length == 2) {
                                String val1 = valArray[0].toLowerCase().trim();//파라미터 소문자
                                String val2 = valArray[1].trim();
                                map_result.put(val1, val2);
                            }
                        }

                    }

                    //vo setting
                    ObjectMapper om = new ObjectMapper();
                    PartData part1 = null;

                    part1 = om.readValue(om.writeValueAsString(map_result), PartData.class);

                    String filePath;
                    String wordLastPath;
                    String wordLocalPath = String.format(ContentsUtil.WORD_LOCAL_PATH, part1.getBookserise(), part1.getBookvolume(), part1.getBookseq());

                    if (CommonUtil.isKorean(part1.getWord()) || CommonUtil.isNum(part1.getWord())) {
                        wordLastPath = part1.getMeans() + ContentsUtil.WORD_POSTFIX;
                        filePath = wordLocalPath + part1.getMeans() + ContentsUtil.WORD_POSTFIX;
                    } else {
                        wordLastPath = part1.getWord() + ContentsUtil.WORD_POSTFIX;
                        filePath = wordLocalPath + part1.getWord() + ContentsUtil.WORD_POSTFIX;
                    }
                    part1.setAudio_path(filePath);
                    part1.setDown_url(ContentsUtil.WORD_URL + wordLastPath);
                    part1.setRecord_path(String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_SP1_%d.wav", CommonUtil.getCurrentYYYYMMDDHHMMSS(), Integer.valueOf(paramList[0].get("MCODE")), part1.getBook_id(), part1.getBook_detail_id(), 1, i));
                    part1.setQuestionnumber(i);
                    part1_list.add(part1);
                } catch (Exception e) {
                    mExceptionToBeThrown = e;
                } finally {

                    try {
                        if (null != dos) {
                            dos.close();
                        }
                        if (null != is) {
                            is.close();
                        }
                        if (null != conn) {
                            conn.disconnect();
                            conn = null;
                        }
                    } catch (IOException e) {
                        mExceptionToBeThrown = e;
                    } finally {
                        if (null != conn) {
                            conn.disconnect();
                            conn = null;
                        }
                    }
                }

                maxTWpersent(0, i, part1_cnt);
            }
            /**
             * PART2 데이터 만들기 40%
             */
            for (int i = 1; i < part2_cnt + 1; i++) {
                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                InputStream is = null;

                try {
                    URL url = new URL(ServiceCommon.SEC_SERVER_URL);

                    // Open a HTTP  connection to  the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    ///
                    Map<String, String> params = new HashMap<String, String>();
                    params.putAll(paramList[0]);
                    params.put("QUIZ_SEQ", String.valueOf(i));
                    params.put("PART_GB", String.valueOf(ServiceCommon.PART_GB_PART2));
                    dos = new DataOutputStream(conn.getOutputStream());
                    Iterator<String> iterator = params.keySet().iterator();
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        dos.writeBytes(setValue(key, params.get(key)));
                    }

                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    dos.flush();

                    is = conn.getInputStream();

                    BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    String line = br.readLine();//RET_CODE=0000<br/>POINT_NAME=IT개발팀<br/>POINT_IP=10.1.2.226<br/>POINT_URL=10.1.2.226

                    Log.e(TAG, line);

                    Map<String, String> map_result = new HashMap<String, String>();
                    if (line != null && !"".equals(line)) {
                        String[] array = line.split("<br/>");
                        for (String str : array) {
                            String[] valArray = str.split("=");
                            if (valArray.length == 2) {
                                String val1 = valArray[0].toLowerCase().trim();//파라미터 소문자
                                String val2 = valArray[1].trim();
                                map_result.put(val1, val2);
                            }
                        }

                    }
                    //vo setting
                    ObjectMapper om = new ObjectMapper();
                    PartData part2 = null;

                    part2 = om.readValue(om.writeValueAsString(map_result), PartData.class);
                    String contentUrl = String.format(ServiceCommon.CONTENT_URL, part2.getBookserise(), part2.getBookvolume(), part2.getBookserise(), part2.getBookvolume(), part2.getBookseq());

                    String localPath = String.format(ContentsUtil.CONTENT_LOCAL_FILE_PREFIX, part2.getBookserise(), part2.getBookvolume(), part2.getBookseq(), part2.getBookserise(), part2.getBookvolume(), part2.getBookseq());
                    String filePath = String.format(localPath + ContentsUtil.SECOND_LOCAL_POSTFIX, part2.getSeq());

                    part2.setAudio_path(filePath);
                    part2.setDown_url(String.format(contentUrl + ContentsUtil.SECOND_POSTFIX, part2.getSeq()));
                    part2.setRecord_path(String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_SP2_%d.wav", CommonUtil.getCurrentYYYYMMDD(), Integer.valueOf(paramList[0].get("MCODE")), part2.getBook_id(), part2.getBook_detail_id(), 1, i));
                    if (part2.getVcautosentence().length() > 2) {
                        int cur_cnt = result.getVip_cnt();
                        result.setVip_cnt(cur_cnt + 1);
                    }

                    part2.setQuestionnumber(i);
                    part2_list.add(part2);

                } catch (Exception e) {
                    mExceptionToBeThrown = e;
                } finally {

                    try {
                        if (null != dos) {
                            dos.close();
                        }
                        if (null != is) {
                            is.close();
                        }
                    } catch (IOException e) {
                        mExceptionToBeThrown = e;
                    }
                    if (null != conn) {
                        conn.disconnect();
                        conn = null;
                    }

                }
                maxTWpersent(20, i, part2_cnt);
            }
        }


        /**
         * part1 다운로드 60%
         */
        for (

                int i = 0; i < part1_list.size(); i++) {
            PartData part1 = part1_list.get(i);
            download(part1.getAudio_path(), part1.getDown_url());
            maxTWpersent(40, i, part1_list.size());
        }
        /*for (PartData part1 : part1_list) {
            download(part1.getAudio_path(), part1.getDown_url());
		}*/
        /**
         * part2 다운로드 80%
         */

        for (int i = 0; i < part2_list.size(); i++) {
            PartData part2 = part2_list.get(i);
            download(part2.getAudio_path(), part2.getDown_url());
            maxTWpersent(60, i, part1_list.size());
        }

		/*for (PartData part2 : part2_list) {
            download(part2.getAudio_path(), part2.getDown_url());
		}*/

        /**
         * part2 yda convert 90%
         */
        for (int i = 0; i < part2_list.size(); i++) {
            PartData part2 = part2_list.get(i);
            YdaConverter mYdaConvert = new YdaConverter(part2.getAudio_path());
            mYdaConvert.start();
            part2.setAudio_path(part2.getAudio_path().replace("yda", "ogg"));
            maxTWpersent(80, i, part1_list.size());
        }
        /*for (PartData part2 : part2_list) {
            YdaConverter mYdaConvert = new YdaConverter(part2.getAudio_path());
			mYdaConvert.start();
			part2.setAudio_path(part2.getAudio_path().replace("yda", "ogg"));
		}*/
        try {
            result.setMcode(Integer.valueOf(paramList[0].get("MCODE")));
            result.setClass_second_id(Integer.valueOf(paramList[0].get("CLASS_SECOND_ID")));
            if (!CommonUtil.isCenter()) {
                result.setUser_no(Integer.valueOf(paramList[0].get("USER_NO")));
                result.setBook_detail_id(Integer.valueOf(paramList[0].get("BOOK_DETAIL_ID")));
                result.setBook_id(Integer.valueOf(paramList[0].get("BOOK_ID")));
            } else {
                result.setForestCode(Preferences.getForestCode(mContext));
                result.setTeacherCode(Preferences.getTeacherCode(mContext));

            }
            result.setPart1_list(part1_list);
            result.setPart2_list(part2_list);
            //result.init_Status_list(stopStudy == 1 ? false : true); 나중에
            //result.setIs_halt_study(stopStudy);
            result.setStep(step);//중도학습
            result.init_Status_list(step);//학습순서 생성
            result.setQuiz_seq(quiz_seq);//중도 학습  index
        } catch (Exception e) {
            mExceptionToBeThrown = e;
        }

        result.setException(mExceptionToBeThrown);//에러 확인용

        return result;

    }

    /**
     *
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        // 프로그레스바
        super.onProgressUpdate(values);
    }

    /**
     * 2교시에 학습해야할 문장, 단어/구를 출력한다.
     */
    public void getQUST(Map<String, String> result) throws Exception {

        Map<String, String> params = new LinkedHashMap<String, String>();

        if (CommonUtil.isCenter()) {
            params.put("CLASS_STUDY_YMD", result.get("class_study_ymd"));
            params.put("MCODE", String.valueOf(Preferences.getCustomerNo(mContext)));
            params.put("MAC_ADDR", CommonUtil.getMacAddress());
        } else {
            params.put("REQ_ID", ServiceCommon.SEC_QUST);
            params.put("MCODE", String.valueOf(Preferences.getCustomerNo(mContext)));
            params.put("MAC_ADDR", CommonUtil.getMacAddress());
            params.put("USER_NO", Preferences.getUserNo(mContext));
            params.put("BOOK_ID", result.get("book_id"));
            params.put("BOOK_DETAIL_ID", result.get("book_detail_id"));
        }
        params.put("CLASS_SECOND_ID", result.get("class_second_id"));

        part1_cnt = Integer.valueOf(result.get("part1_count"));
        part2_cnt = Integer.valueOf(result.get("part2_count"));
        //stopStudy = Integer.valueOf(result.get(""));
        step = result.get("step");
        quiz_seq = Integer.valueOf(result.get("quiz_seq"));

        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

        //params.put("MCODE", "31729364");
        //params.put("CLASS_SECOND_ID", "31729364");
        //params.put("LAST_BOOK_DETAIL_ID", "31729364");

    }

    /**
     * Map 형식으로 Key와 Value를 셋팅한다.
     *
     * @param key   : 서버에서 사용할 변수명
     * @param value : 변수명에 해당하는 실제 값
     * @return
     */
    private String setValue(String key, String value) {

        String result = "";

        result = result.concat(twoHyphens + boundary + lineEnd);
        result = result.concat("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
        result = result.concat(lineEnd);
        result = result.concat(value);
        result = result.concat(lineEnd);

        return result;
    }

    /**
     * 설정된 위치에 폴더를 생성함.
     *
     * @param fullPath 생성할 폴더의 전체 경로(마지막 항목이 파일이면 파일명으로 폴더가 생기지 않도록 함, 마지막 항목 제외함)
     * @return true이면 정상 처리됨을 의미함.
     */
    boolean mkdirs(String fullPath) {

        if (null == fullPath)
            return false;

        StringBuffer sb = new StringBuffer();
        List<String> itr = (Uri.parse(fullPath)).getPathSegments();

        int cnt = itr.size();
        for (int i = 0; i < cnt; i++) {
            String s = itr.get(i);

            // 마지막 항목이 파일이면 제외
            if (i == (cnt - 1)) {
                if (!s.contains(".")) {
                    sb.append(File.separator);
                    sb.append(s);
                }
            } else {
                sb.append(File.separator);
                sb.append(s);
            }
        }

        if (0 >= sb.length())
            return false;

        File f = new File(sb.toString());
        if (f.exists())
            return true;

        return f.mkdirs();
    }

    /**
     * 1교시,2교시 다운로드
     *
     * @param audio_path
     * @param down_url
     */
    private void download(String audio_path, String down_url) {
        HttpURLConnection con = null;
        FileOutputStream fos = null;
        BufferedInputStream bis = null;
        InputStream is = null;
        Log.e(TAG, "[audio_path=" + audio_path + "]" + "[down_url" + down_url + "]");
        try {

            File curr = new File(audio_path);
            if (curr.exists()) {//있으면 삭제
                curr.delete();
            }

            URL url = new URL(new URI(null, down_url, null).toASCIIString());
            con = (HttpURLConnection) url.openConnection();
            if (null == con) {
                mExceptionToBeThrown = new NullPointerException();
            }

            con.setConnectTimeout(CONN_TIMEOUT);
            con.setReadTimeout(READ_TIMEOUT);
            con.setUseCaches(false);
            // OutputStream으로 GET 데이터를 넘겨주겠다는 옵션.
            con.setDoOutput(false);

            mResCode = con.getResponseCode();
            if (HttpURLConnection.HTTP_OK != mResCode) {
                mExceptionToBeThrown = new Exception(String.valueOf(mResCode));
            }

            is = con.getInputStream();
            bis = new BufferedInputStream(is);
            mkdirs(audio_path);

            if (!mIsCanceled) {

                int readSize = 0;
                byte[] buff = new byte[2048];

                fos = new FileOutputStream(mFile = new File(audio_path));

                while (!mIsCanceled && (readSize = bis.read(buff)) != -1)
                    fos.write(buff, 0, readSize);

                Log.e(LOG_TAG, "mIsCanceled : " + mIsCanceled);
                if (!mIsCanceled) {
                    fos.flush();
                    fos.getFD().sync();
                    fos.close();
                    fos = null;
                    mFile = null;
                }
            }

        } catch (Exception e) {
            mExceptionToBeThrown = e;
        } finally {
            try {
                if (null != bis)
                    bis.close();
                if (null != is)
                    is.close();
                if (null != fos)
                    fos.close();
            } catch (IOException e) {
                mExceptionToBeThrown = e;
            } finally {
                if (null != con)
                    con.disconnect();
            }
        }
    }

    /**
     * 퍼센트가 20맥스로 더하게
     *
     * @param cur_num
     * @param for_num
     */
    private void maxTWpersent(int max_num, int cur_num, int for_num) {
        int_progress = max_num + (int) (20.0 * (((double) cur_num / (double) for_num)));

        if (int_progress < 0 || int_progress > 100) {//수치 넘어버리면
            int_progress = 100;
        }
        publishProgress(int_progress);

    }

    /**
     * In 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addInParam(String url, Map<String, String> param) {
        if (param.size() == 0) {
            Log.e(TAG, "param size error");
            return url;
        }
        Object[] str_param = param.values().toArray();
        for (int i = 0; i < str_param.length; i++) {
            url += "&" + IN_PARAM + String.valueOf(i + 1) + "=" + str_param[i];
        }
        return url;
    }

    /**
     * Out 파라미터 추가
     *
     * @param url   : 파라미터 추가할 url
     * @param param : 파라미터 배열
     * @return 파라미터 추가된 url
     */
    private String addOutParam(String url, String param[]) {
        if (param.length == 0) {
            Log.e(TAG, "param size error");
            return url;
        }

        for (int i = 0; i < param.length; i++) {
            url += "&" + OUT_PARAM + String.valueOf(i + 1) + "=" + param[i];
        }

        return url;
    }

    /**
     * 패키지 이름 추가 함수
     *
     * @param name : 패키지 이름
     * @return
     */
    private String addPackageName(String name) {
        if (ServiceCommon.STUDY_SERVER_URL.length() == 0) {
            ServiceCommon.setServerUrl(mContext, "");
        }

        return ServiceCommon.STUDY_SERVER_URL + "?" + PKG_NAME + "=" + name;
    }

    /**
     * 파라미터 생성
     *
     * @param params : 가변 스트링 파라미터 문자열
     * @return 스트링 배열
     */
    public String[] createParam(String... params) {
        String param[] = new String[params.length];
        for (int i = 0; i < param.length; i++) {
            param[i] = (String) params[i];
        }

        return param;
    }
}
