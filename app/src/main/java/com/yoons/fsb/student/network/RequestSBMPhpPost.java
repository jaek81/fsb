package com.yoons.fsb.student.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * http 클라이언트 클래스(관제)
 * 
 * @author ejlee
 *
 */
public class RequestSBMPhpPost extends AsyncTask<Map<String, String>, Integer, Map<String, String>> {
	/**
	 * 학습 결과를 Server로 전송한다.
	 * 
	 */
	private final String TAG = "RequestPhpPost";
	private final String STR_YS20 = "YS20";
	private String lineEnd = "\r\n";
	private String twoHyphens = "--";
	private String boundary = "*****";
	private final String CRLF = "\r\n";
	private final String PREFIX = "--";
	private final String SUFFIX = PREFIX;
	private final String BOUNDARY = "--------multipart-boundary--------";

	HttpURLConnection conn = null;
	DataOutputStream dos = null;
	InputStream is = null;

	int bytesRead, bytesAvailable, bufferSize;
	byte[] buffer;
	int maxBufferSize = 1 * 1024 * 1024;

	private Context mContext;

	private String mcode = "";
	private String macAddress = "";
	private String userNo = "";
	private String mAcode = "";

	public RequestSBMPhpPost(Context context) {

		mContext = context;
		mcode = String.valueOf(Preferences.getCustomerNo(mContext));
		macAddress = String.valueOf(CommonUtil.getMacAddress());
		userNo = String.valueOf(Preferences.getCustomerNo(mContext));
		mAcode = String.valueOf(Preferences.getForestCode(mContext));

	}

	@Override
	protected Map<String, String> doInBackground(Map<String, String>... paramList) {

		Map<String, String> result = new HashMap<String, String>();

		try {

			// open a URL connection to the Servlet

			URL url = new URL(ServiceCommon.SBM_SERVER_URL);

			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			///

			Map<String, String> params = new HashMap<String, String>();
			params.putAll(paramList[0]);

			Log.e(TAG, params.toString());

			dos = new DataOutputStream(conn.getOutputStream());
			Iterator<String> iterator = params.keySet().iterator();
			while (iterator.hasNext()) {
				String key = (String) iterator.next();
				if ("REC_FILE".equals(key)) {// 파일일때)}
					fileWrite(params.get(key));
				} else {
					dos.writeBytes(setValue(key, params.get(key)));
				}

			}

			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			dos.flush();

			is = conn.getInputStream();


			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line = br.readLine();// RET_CODE=0000<br/>POINT_NAME=IT개발팀<br/>POINT_IP=10.1.2.226<br/>POINT_URL=10.1.2.226
			if (line != null && !"".equals(line)) {
				String[] array = line.split("<br/>");
				for (String str : array) {
					String[] valArray = str.split("=");
					if (valArray.length == 2) {
						result.put(valArray[0].toLowerCase().trim(), valArray[1].trim());
					} else {// step=""인경우가 잇어서
						result.put(valArray[0].toLowerCase().trim(), "");
					}
				}

			}
			if (line != null)
				Log.e(TAG, line);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != dos) {
					dos.close();
				}
				if (null != is) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (null != conn) {
				conn.disconnect();
				conn = null;
			}
		}
		return result;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	/**
	 * Map 형식으로 Key와 Value를 셋팅한다.
	 * 
	 * @param key
	 *            : 서버에서 사용할 변수명
	 * @param value
	 *            : 변수명에 해당하는 실제 값
	 * @return
	 * @throws IOException
	 */
	private String setValue(String key, String value) throws IOException {

		String result = "";

		result = result.concat(twoHyphens + boundary + lineEnd);
		result = result.concat("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
		result = result.concat(lineEnd);
		result = result.concat(value);
		result = result.concat(lineEnd);

		return result;
	}

	private void fileWrite(String file_path) throws IOException {
		FileInputStream is = null;
		try {
			is = new FileInputStream(file_path);
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"userfile\";filename=\"" + file_path + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			// create a buffer of maximum size
			bytesAvailable = is.available();

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = is.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {

				dos.write(buffer, 0, bufferSize);
				bytesAvailable = is.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = is.read(buffer, 0, bufferSize);

			}
			dos.writeBytes(lineEnd);

			// dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		} catch (IOException e) {
			throw e;
		} finally {
			is.close();
		}
	}

	/**
	 * 
	 */
	@Override
	protected void onProgressUpdate(Integer... values) {
		// 프로그레스바
		super.onProgressUpdate(values);
	}

	/**
	 * 
	 * @param serialNumber
	 *            민트패드 시리얼 번호 (ex: 122200084)
	 * @param studentName
	 *            회원이름
	 * @param userNumber
	 *            회원번호
	 * @param mCase
	 *            로그인유형의 case 값 (1~3)
	 */
	public void sendMLGNL(String serialNumber, String studentName, String userNumber, String mCase) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("REQ_ID", ServiceCommon.MLGNL);
		params.put("ACODE", mAcode);
		params.put("SerialNumber", serialNumber);
		try {
			params.put("StudentName", URLEncoder.encode(studentName, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		params.put("UserNumber", userNumber);
		params.put("MCODE", mcode);
		params.put("CASE", mCase);
		params.put("DEVIES", STR_YS20);

		executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	}

	/*
	 * 2교시학습시작 (QINF)
	 */
	public void getMLGNB() {

		/**
		 * 여긴 나중에 삭제해야함(아직 중간 통신이 없어서 넣어둠)나중에 이게 처음이 아니라면 여기보다 앞으로 가야함
		 */
		// mcode = "2121212";//나중에

		/**
		 * 
		 */

		Map<String, String> params = new HashMap<String, String>();

		params.put("REQ_ID", ServiceCommon.MLGNB);
		params.put("MCODE", mcode);
		params.put("ACODE", mAcode);

		executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);

	}

	public void getServerInfo() {

		Map<String, String> params = new HashMap<String, String>();

		params.put("REQ_ID", "CENTER");
		params.put("MAC_ADDR", macAddress);

		execute(params);

	}

}
