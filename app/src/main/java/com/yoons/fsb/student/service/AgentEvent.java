package com.yoons.fsb.student.service;




public class AgentEvent {

	public static final String ACTION_SERVICE_AGENT = "com.yoons.fsb.student.service.ACTION_SERVICE_AGENT";
	public static final String ACTION_RECEIVE_AGENT_EVENT = "com.yoons.fsb.student.service.ACTION_RECEIVE_AGENT_EVENT";

	/** 기능 요청을 위한 구분 값 DOWNLOAD_START ~ DOWNLOAD_COMPLETE */  
	public static final String EVENT_TYPE = "event_type";

	/** 다운로드 실패 원인 REASON_NO_ERROR ~ REASON_NOT_AVAILABLE_NETWORKS */
	public static final String FAIL_REASON = "fail_reason";

	/** 다운로드 요청에 대한 네트워크 반환값 */
	public static final String RESPONSE_CODE = "response_code";
	/** 다운로드 요청에 대한 네트워크 반환값의 추가정보 */
	public static final String RESPONSE_INFO = "response_info";

	/** 다운로드 전체 상태(n/n) */
	public static final String TOT_POS = "total_position"; 
	public static final String TOT_CNT = "total_count";
	
	/** 다운로드 우선순위 상태(n/n) */
	public static final String PRI_POS = "priority_position"; 
	public static final String PRI_CNT = "priority_count"; 

	public static final int BASE = 128;

	/** 학습 콘텐츠 다운로드 시작 */
	public static final int DOWNLOAD_START = BASE;

	/** 학습 콘텐츠 다운로드 중지 */
	public static final int DOWNLOAD_STOP = BASE + 1;

	/** 학습 시작 */
	public static final int START_LEARNING = BASE + 2;

	/** 망 트래픽 체크(사용량) */
	public static final int CHECK_TRAFFIC = BASE + 3;
	
	/** Update */
	public static final int CONTENTS_UPDATE = BASE + 4;

	/** download state(progress n/n) */
	public static final int DOWNLOAD_STATE = BASE + 5;
	
	/** 다운로드 실패 */
	public static final int DOWNLOAD_FAIL = BASE + 6;

	/** 다운로드 완료 */
	public static final int DOWNLOAD_COMPLETE = BASE + 7;

	/** 다운로드 실패 원인(오류 아님) */
	public static final int REASON_NO_ERROR = 0;
	/** 다운로드 실패 원인(storage unmount) */
	public static final int REASON_UNMOUNTED_STORAGE = -1;
	/** 다운로드 실패 원인(저장공간 부족) */
	public static final int REASON_NOT_ENOUGH_SPACE = -2;
	/** 다운로드 실패 원인(네트워크 오류) */
	public static final int REASON_NET_ERROR = -3;
	/** 다운로드 실패 원인(일반 오류) */
	public static final int REASON_UNKNOWN_ERROR = -4;
	/** 사용 가능한 네트워크 없음. */
	public static final int REASON_NOT_AVAILABLE_NETWORKS = -5;

	public AgentEvent() {
	}
}
