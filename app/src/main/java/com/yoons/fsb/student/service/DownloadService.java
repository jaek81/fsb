package com.yoons.fsb.student.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

/**
 * 기본 기능은 학습 콘텐츠를 다운받는데 활용할 목적으로 개발하였고,
 * sleep(idle) mode 진입 시 notification 설정/해재 및 전원 유지, 
 * Wi-Fi 활성 유지 등의 기능을 추가적으로 관리함.
 * @author dckim
 */
public class DownloadService extends Service { 
	private static final String LOG_TAG = "[DownloadService]";
	
	private WifiLock mWifiLock = null;
	private WakeLock mWakeLock = null;

	private static final int SERVICE_NOTIFICATION_ID = 1;

	private HandlerThread worker;
	private DownloadServiceAgent agentHandler;
	private final IBinder binder = new LocalBinder();
	
    public class LocalBinder extends Binder {
    	public DownloadServiceAgent getServiceHandler() {
            return agentHandler;
        }
    }

	@Override
	public void onCreate() {
		super.onCreate();
		
		// 서비스의 메인 스레드를 시작 시킨다. 
        worker = new HandlerThread("DownloadServiceThread");
        worker.start();

        Looper looper = worker.getLooper();
        agentHandler = new DownloadServiceAgent(looper, this);
        agentHandler.registReceiver();
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();

		LockRelease();
	    worker.quit();
	    worker = null;
	    agentHandler.unregistReceiver();
	    agentHandler.stopCheckingTraffic();
	    agentHandler = null;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// onBind, onUnbind는 프로세스 시작되고 처음 MainActivity에서 bind될 때만 호출되고 두 번째 부터는 호출 안된다.
		// ServiceConnection 객체에서 onServiceConnected() 만 호출된다. 
		return binder;
	} 

	@Override
	public boolean onUnbind(Intent intent) {
       return super.onUnbind(intent);
    }

	@Override
    public int onStartCommand(Intent intent, int flags, int startId){
		super.onStartCommand(intent, flags, startId);
		return Service.START_STICKY;  
	}


	/**
	 * 연결 유지
	 * sleep(idle) mode 진입 시 전원 관리 및 Wi-Fi 연결 유지
	 */
	public void LockAcquire() {

		if (mWifiLock == null) {
		    mWifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE)).createWifiLock("wifilock");
		    mWifiLock.setReferenceCounted(true);
		    if (!mWifiLock.isHeld())
		    	mWifiLock.acquire();
		}
		
		if (null == mWakeLock) {
			mWakeLock = ((PowerManager) getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOG_TAG);
		    if (!mWakeLock.isHeld())
		    	mWakeLock.acquire();
		}
	}
	
	/**
	 * 연결 해지 
	 * 전원 및 Wi-Fi 관리 기능 해제함 
	 */
	public void LockRelease() {
		
		if (null != mWakeLock) {
			if (mWakeLock.isHeld())
				mWakeLock.release();
			mWakeLock = null;
		}

		if (mWifiLock != null) {
			if (mWifiLock.isHeld())
				mWifiLock.release();
			mWifiLock = null;
		}
	}
	
	
}
