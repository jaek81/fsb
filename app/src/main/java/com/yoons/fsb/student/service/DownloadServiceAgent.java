package com.yoons.fsb.student.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.TrafficStats;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.db.DatabaseData.DownloadContent;
import com.yoons.fsb.student.db.DatabaseData.DownloadContent.UnitFile;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StorageUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dckim HTTP다운로드, (업로더, 추후에 정해지면 개발) [구현 요구사항] 서비스 형태의 다운로드 매니저 DB에
 *         다운로드 받을 파일의 차시 별 리스트가 있으며, 먼저 다운로드를 받아야 할 리스트가 있습니다. 다운로드를 받게 되면 이벤트를
 *         줘야합니다. (broadcast같은) 어플리케이션이 실행되면, db에서 다운로드 파일을 조회하여 다운로드를
 *         시작합니다.(자동시작) DB는 생성되어있지 않으므로 임의로 리스트를 만들어 구현해야 합니다. cdn의 리스트를 긁어오면
 *         되겠지요.
 * 
 *         network 구분의 상태에 따른 동작(mobile network 사용 유무) 수행 - connected 상태가 되면
 *         다운로드 요청
 * 
 *         storage mount/unmount 상태에 따른 다운로드 수행
 * 
 *         교육 과정 중 다운로드 가능/불가능 구간의 분류에 따라 다운로드 수행
 * 
 *         UI -> Service 기능 수행 요청(다운로드 시작, 다운로드 중지, 다운로드 목록 갱신)
 */
public class DownloadServiceAgent extends Handler implements OnSharedPreferenceChangeListener {

	private static final String LOG_TAG = "[DownloadService]";

	private final int MSG_DELAY_MILLIS = 100;
	private final int MSG_TRAFFIC_INTERVAL = 10000;
	private final int MINIMUM_FREE_SPACE = 1024 * 1024 * 40; // 40 MegaByte
	private DownloadService mMainService = null;
	private DatabaseUtil mDbUtil = null;
	private WifiManager mWifiMgr = null;

	/** 사용자 취소 */
	private boolean mUserCanceled = false;

	/** 현재 다운로드 여부 */
	private boolean mIsDownloading = false;

	/** 다운로드 객체 */
	private DownTask mDownTask = null;

	/** 3G/LTE 사용량 체크를 위함 */
	int mUID = android.os.Process.myUid();
	long mTX = TrafficStats.UNSUPPORTED;
	long mRX = TrafficStats.UNSUPPORTED;
	long mMobileTX = TrafficStats.UNSUPPORTED;
	long mMobileRX = TrafficStats.UNSUPPORTED;

	/** 네트워크 트래픽(사용량) 체크 */
	private final int NET_TRAFFIC_NONE = 0;
	private final int NET_TRAFFIC_ALL = 0x01;
	private final int NET_TRAFFIC_MOBILE = 0x02;
	//private final int NET_TRAFFIC_WIFI = 0x04;

	private int mNetTrafficType = (NET_TRAFFIC_ALL | NET_TRAFFIC_MOBILE);

	/** 네트워크 타입 */
	private int mNetType = -1;
	private NetworkInfo.State mNetState = NetworkInfo.State.UNKNOWN;

	/** 다운로드 받을 목록 저장 */
	private ArrayList<DownloadContent> mList = new ArrayList<DownloadContent>();

	private int mInxContent = -1;
	private int mInxUnit = -1;
	private UnitFile mCurrUnit = null;

	/** 콘텐츠 다운로드 중 재 시도요구 */
	private final int MAX_RETRY_COUNT = 1; // 1로 변경 더해봤자 잘안됨.
	private int mRetryCount = 0;

	/** WiFi 상태 변화 감시 */
	protected WifiStateReceiver mWifiSeateReceiver = null;

	/** Storage state(Mount/Unmount) 감시 */
	protected StorageStateReceiver mStorageStateReceiver = null;

	/** UI로부터 기능 수행 요청 감지 */
	protected RequestAgentEventReceiver mRequestAgentEvent = null;

	public DownloadServiceAgent(Looper looper, DownloadService service) {
		super(looper);

		mMainService = service;
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
			mDbUtil = DatabaseUtil.getInstance(mMainService);

		mWifiMgr = (WifiManager) mMainService.getSystemService(Context.WIFI_SERVICE);
		mWifiMgr.startScan();
	}

	/**
	 * 다운로드 시작 요청(UI >> Service). UI의 BroadcastReceiver로 완료 시점 전달함.
	 * 
	 * @brief Action : AgentEvent.ACTION_RECEIVE_AGENT_EVENT
	 *        getIntExtra(AgentEvent.EVENT_TYPE, 0) == AgentEvent.DOWNLOAD_START
	 * @param ctx
	 */
	public static void RequestStart(Context ctx) {
		if (null != ctx)
			ctx.sendBroadcast(getIntent(AgentEvent.DOWNLOAD_START, true));
	}

	/**
	 * 다운로드 중지 요청(UI >> Service) UI의 BroadcastReceiver로 완료 시점 전달함.
	 * 
	 * @brief Action : AgentEvent.ACTION_RECEIVE_AGENT_EVENT
	 *        getIntExtra(AgentEvent.EVENT_TYPE, 0) == AgentEvent.DOWNLOAD_STOP
	 * @param ctx
	 */
	public static void RequestStop(Context ctx) {
		if (null != ctx)
			ctx.sendBroadcast(getIntent(AgentEvent.DOWNLOAD_STOP, true));
	}

	/**
	 * 다운로드 목록 갱신 요청(UI >> Service) UI의 BroadcastReceiver로 완료 시점 전달함.
	 * 
	 * @brief Action : AgentEvent.ACTION_RECEIVE_AGENT_EVENT
	 *        getIntExtra(AgentEvent.EVENT_TYPE, 0) ==
	 *        AgentEvent.CONTENTS_UPDATE
	 * @param ctx
	 */
	public static void RequestUpdate(Context ctx) {
		if (null != ctx)
			ctx.sendBroadcast(getIntent(AgentEvent.CONTENTS_UPDATE, true));
	}

	/**
	 * 다운로드 상태 정보 요청(UI >> Service) UI의 BroadcastReceiver로 완료 시점 전달함.
	 * 
	 * @brief Action : AgentEvent.ACTION_RECEIVE_AGENT_EVENT
	 *        getIntExtra(AgentEvent.EVENT_TYPE, 0) == AgentEvent.DOWNLOAD_STATE
	 *        getIntExtra(AgentEvent.TOT_POS, 0); 전체 중에서의 현재 위치(index by)
	 *        getIntExtra(AgentEvent.TOT_CNT, 0); 전체 갯수
	 *        getIntExtra(AgentEvent.PRI_POS, 0); 우선 순위 현재 위치(index by)
	 *        getIntExtra(AgentEvent.PRI_CNT, 0); 우선 순위 전체 갯수
	 * @param Context
	 */
	public static void RequestState(Context ctx) {
		if (null != ctx)
			ctx.sendBroadcast(getIntent(AgentEvent.DOWNLOAD_STATE, true));
	}

	/**
	 * @brief 다운로드 시작됨을 통지(Service >> UI)
	 */
	private void ResponseStart() {

		if (null == mMainService)
			return;

		if (!IsDownloading())
			mMainService.sendBroadcast(getIntent(AgentEvent.DOWNLOAD_START, false));
	}

	/**
	 * @brief 다운로드 중지됨을 통지(Service >> UI)
	 */
	private void ResponseStop() {

		if (null == mMainService)
			return;

		mMainService.sendBroadcast(getIntent(AgentEvent.DOWNLOAD_STOP, false));
	}

	/**
	 * @brief 학습할 콘텐츠 다운로드 완료 통지:학습시작(Service >> UI)
	 */
	private void ResponseStartLearning() {

		if (null == mMainService)
			return;

		mMainService.sendBroadcast(getIntent(AgentEvent.START_LEARNING, false));
	}

	/**
	 * @brief 다운로드 목록의 갱신의 완료 통지(Service >> UI)
	 */
	private void ResponseUpdate() {

		if (null == mMainService)
			return;

		mMainService.sendBroadcast(getIntent(AgentEvent.CONTENTS_UPDATE, false));
	}

	/**
	 * @brief 다운로드 완료 통지(Service >> UI)
	 */
	private void ResponseDownloadComplete() {

		if (null == mMainService)
			return;

		mMainService.sendBroadcast(getIntent(AgentEvent.DOWNLOAD_COMPLETE, false));
	}

	/**
	 * @brief 다운로드 실패하여 임시 중지됨 통지(Service >> UI)
	 */
	private void ResponseFail(int reason, int responseCode, String responseInfo) {

		if (null == mMainService)
			return;


		Intent intent = getIntent(AgentEvent.DOWNLOAD_FAIL, false);
		intent.putExtra(AgentEvent.FAIL_REASON, reason);

		// network 오류 일 경우 코드 전달
		if (AgentEvent.REASON_NET_ERROR == reason) {
			intent.putExtra(AgentEvent.RESPONSE_CODE, responseCode);
			intent.putExtra(AgentEvent.RESPONSE_INFO, responseInfo);
		}
		mMainService.sendBroadcast(intent);
	}

	/**
	 * @brief 다운로드 항목 변경 통지(Service >> UI)
	 */
	private void ResponseState() {

		if (null == mMainService)
			return;

		CountInfo t = getDownloadState(false);
		CountInfo p = getDownloadState(true);

		Intent i = getIntent(AgentEvent.DOWNLOAD_STATE, false);
		i.putExtra(AgentEvent.TOT_POS, t.curr);
		i.putExtra(AgentEvent.TOT_CNT, t.total);
		i.putExtra(AgentEvent.PRI_POS, p.curr);
		i.putExtra(AgentEvent.PRI_CNT, p.total);
		mMainService.sendBroadcast(i);
	}

	private static Intent getIntent(int type, boolean isRequest) {
		Intent intent = new Intent((isRequest) ? AgentEvent.ACTION_SERVICE_AGENT : AgentEvent.ACTION_RECEIVE_AGENT_EVENT);
		intent.putExtra(AgentEvent.EVENT_TYPE, type);
		return intent;
	}

	/**
	 * 다운로드 시작
	 */
	private void startDownload() {
		removeMessages(AgentEvent.BASE);
		Log.w(LOG_TAG, "startDownload()");

		// 다운 받을 콘텐츠 유무 확인
		if (!hasDownloadContent()) {
			Log.w(LOG_TAG, "has not download content!");
			mIsDownloading = false;
			return;
		}

		// 의도적인 정지 상태
		if (mUserCanceled) {
			Log.e(LOG_TAG, "mUserCanceled:" + mUserCanceled);
			mIsDownloading = false;
			return;
		}

		// 사용가능한 네트워크 없음
		if (!CommonUtil.isAvailableNetwork(mMainService, true)) {
			Log.e(LOG_TAG, "not exist activate network");
			mIsDownloading = false;
			ResponseFail(AgentEvent.REASON_NOT_AVAILABLE_NETWORKS, 0, "");
			return;
		}

		if (!isRunningDownTask()) {
			sendMessageDelayed(obtainMessage(AgentEvent.BASE, AgentEvent.DOWNLOAD_START, 0, null), MSG_DELAY_MILLIS);
			ResponseStart();
		} else {
			Log.e(LOG_TAG, "mDownTask is Running!!!");
		}
	}

	/**
	 * 다운로드 중지
	 */
	private void stopDownload() {
		removeMessages(AgentEvent.BASE);

		Log.w(LOG_TAG, "stopDownload()");
		if (isRunningDownTask())
			mDownTask.cancel();

		mIsDownloading = false;
		ResponseStop();
	}

	/**
	 * 다운로드 받을 콘텐츠 설정
	 */
	private void next() {

		if (null == mList)
			return;

		int cnt = mList.size();
		if (mInxContent >= cnt)
			return;

		DownloadContent dc = mList.get(mInxContent);
		if ((mInxUnit + 1) < dc.mUnitFileList.size()) {
			mInxUnit++;
			return;
		}

		// 다음 차시가 존재하면 다음 차시 콘텐츠로 이동
		if (mInxContent + 1 < cnt) {

			boolean cleanedPriority = false;

			if (dc.mPriority) {
				DownloadContent tmp = mList.get(mInxContent + 1);
				// 다음 차시에 우선순위 정보가 없으면... 
				// 학습 할 콘텐츠 다운로드 완료로 판단
				if (!tmp.mPriority) {
					// 항목 삭제
					try {
						for (int i = mInxContent; 0 <= i; i--)
							mList.remove(i);

						cleanedPriority = true;
						mInxContent = 0;
					} catch (Exception e) {
						e.printStackTrace();
					}

					// UI에 학습 시작 이벤트 전달
					ResponseDownloadComplete();
					ResponseStartLearning();
				}
			}

			if (!cleanedPriority)
				mInxContent++;

			mInxUnit = 0;
			return;
		}

		ResponseDownloadComplete();

		// 우선순위 정보 콘테츠로만으로 구성된 경우
		if (dc.mPriority) {
			// 학습 할 콘텐츠 다운로드 완료되어 UI에 학습 시작 이벤트 전달
			ResponseStartLearning();
		}

		//전체 다운로드 완료
		removeAll();
	}

	/**
	 * 다운로드 항목 전체 제거(all items)
	 */
	private void removeAll() {
		stopDownload();

		if (null == mList)
			return;

		for (int i = 0; i < mList.size(); i++) {
			DownloadContent studyUnit = mList.get(i);
			if (null != studyUnit)
				studyUnit.mUnitFileList.clear();
		}

		mList.clear();
		mInxContent = -1;
		mInxUnit = -1;
		mCurrUnit = null;
		mIsDownloading = false;
	}

	/**
	 * 다운로드 목록의 상태 조회
	 * 
	 * @param priorityOnly
	 *            : 우선순위 콘텐츠 조회 여부
	 * @return CountInfo : 전체 갯수와 현재 위치의 정보
	 */
	private CountInfo getDownloadState(boolean priorityOnly) {
		int tot = 0, curr = 0, loop = 0;

		for (DownloadContent dc : mList) {

			if (priorityOnly && !dc.mPriority)
				break;

			int cnt = dc.mUnitFileList.size();
			tot += cnt;

			if (loop == mInxContent) {
				curr += (mInxUnit + 1);
			} else if (loop < mInxContent) {
				curr += cnt;
			}

			loop++;
		}

		return new CountInfo(tot, curr);
	}

	/**
	 * 다운로드 받을 콘텐츠 정보 조회
	 * 
	 * @return UnitFile(null이면 정보 없음.)
	 */
	private UnitFile getCurrent() {

		UnitFile unitFile = null;

		if (null == mList)
			return unitFile;

		if (0 <= mInxContent && mInxContent < mList.size()) {
			DownloadContent studyUnit = mList.get(mInxContent);

			if (0 <= mInxUnit && mInxUnit < studyUnit.mUnitFileList.size()) {
				mCurrUnit = studyUnit.mUnitFileList.get(mInxUnit);
			}
		}

		return mCurrUnit;
	}

	/**
	 * 다운로드 받을 콘텐츠 유무 조회
	 * 
	 * @return boolean false이면 더이상 다운로드할 콘텐츠 없음.
	 */
	private boolean hasDownloadContent() {

		if (null == mList)
			return false;

		if (0 <= mInxContent && mInxContent < mList.size()) {
			DownloadContent studyUnit = mList.get(mInxContent);
			if (0 <= mInxUnit && mInxUnit < studyUnit.mUnitFileList.size())
				return true;
		}

		return false;
	}

	/**
	 * 다운로드 시작
	 */
	private void start() {
		mRetryCount = 0;
		mUserCanceled = false;
		startDownload();
	}

	/**
	 * 다운로드 중지
	 */
	private void stop() {
		mUserCanceled = true;
		stopDownload();
	}

	/**
	 * 목록 갱신(다운 받고 있던 항목이 있을 경우 "다운로드 중지 > 전체 목록 삭제 > 목록 새로 구성"으로 처리된다.)
	 */
	private void update() {

		removeAll();

		if (null == mDbUtil)
			return;

		mList = mDbUtil.selectDownloadContent();
		if (0 < mList.size()) {
			DownloadContent studyUnit = mList.get(0);
			if (0 < studyUnit.mUnitFileList.size()) {
				mInxContent = 0;
				mInxUnit = 0;
				ResponseState();
				ResponseUpdate();
				return;
			}
		}

		ResponseState();
	}

	/**
	 * 다운로드 중인지 확인
	 * 
	 * @return true이면 다운로드 중임.
	 */
	public boolean IsDownloading() {
		return mIsDownloading;
	}

	/**
	 * 학습 콘텐츠 다운로드 모듈 동작여부 확인
	 * 
	 * @return true이면 다운로드 Task 실행 중임.
	 */
	private boolean isRunningDownTask() {
		return (null != mDownTask) ? true : false;
	}

	@Override
	public void handleMessage(Message msg) {

		switch (msg.what) {
		case AgentEvent.BASE:
			agentEventProc(msg.arg1);
			break;
		case AgentEvent.CHECK_TRAFFIC:
			checkTraffic(mNetType, mNetState, true);
			break;
		default:
			Log.e(LOG_TAG, "handleMessage:" + msg.what);
			break;
		}
	}

	private void agentEventProc(int param) {
		switch (param) {
		case AgentEvent.DOWNLOAD_START:
			mIsDownloading = true;
			Log.w(LOG_TAG, "agentEventProc=AGENT_EVENT_START");

			if (!isRunningDownTask()) {
				UnitFile unit = getCurrent();
				if (null != unit) {
					mDownTask = new DownTask();
					mDownTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, unit);
				}
			}
			break;
		case AgentEvent.DOWNLOAD_STOP:
			mIsDownloading = false;
			Log.w(LOG_TAG, "agentEventProc=AGENT_EVENT_STOP");
			break;
		default:
			break;
		}
	}

	/**
	 * regist Receiver
	 */
	protected void registReceiver() {

		if (null == mMainService)
			return;

		// 외부 저장소 상태를 감시하기 위한 Receiver 등록
		if (null == mStorageStateReceiver) {
			mStorageStateReceiver = new StorageStateReceiver();
			IntentFilter filter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
			filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
			filter.addAction(Intent.ACTION_MEDIA_EJECT);
			filter.addAction(Intent.ACTION_MEDIA_REMOVED);
			filter.addDataScheme("file");
			mMainService.registerReceiver(mStorageStateReceiver, filter);
		}

		/** UI로부터 기능 수행 요청 감지 */
		if (null == mRequestAgentEvent) {
			mRequestAgentEvent = new RequestAgentEventReceiver();
			mMainService.registerReceiver(mRequestAgentEvent, new IntentFilter(AgentEvent.ACTION_SERVICE_AGENT));
		}

		/** 환경설정 값 변경 감시 */
		SharedPreferences prefs = Preferences.getPref(mMainService);
		if (null != prefs)
			prefs.registerOnSharedPreferenceChangeListener(this);

		/** 무선 네트워크 상태를 수신하기 위한 Receiver 등록 */
		if (null == mWifiSeateReceiver) {

			IntentFilter filter = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
			filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
			filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
			mWifiSeateReceiver = new WifiStateReceiver();
			mMainService.registerReceiver(mWifiSeateReceiver, filter);

			// 초기값 설정
			Intent i = new Intent(WifiManager.RSSI_CHANGED_ACTION);
			i.putExtra(WifiManager.EXTRA_NEW_RSSI, mWifiMgr.getConnectionInfo().getRssi());
			mWifiSeateReceiver.onReceive(mMainService, i);
		}
	}

	/**
	 * unregist Receiver
	 */
	protected void unregistReceiver() {

		if (null == mMainService)
			return;

		if (null != mStorageStateReceiver) {
			mMainService.unregisterReceiver(mStorageStateReceiver);
			mStorageStateReceiver = null;
		}

		if (null != mRequestAgentEvent) {
			mMainService.unregisterReceiver(mRequestAgentEvent);
			mRequestAgentEvent = null;
		}

		if (null != mWifiSeateReceiver) {
			mMainService.unregisterReceiver(mWifiSeateReceiver);
			mWifiSeateReceiver = null;
		}

		SharedPreferences prefs = Preferences.getPref(mMainService);
		if (null != prefs)
			prefs.unregisterOnSharedPreferenceChangeListener(this);

		mMainService = null;
	}

	/**
	 * stop Checking TrafficState(앱이 사용하는 DataNetwork 사용량 수집을 중지)
	 */
	protected void stopCheckingTraffic() {
		checkTraffic(mNetType, mNetState, false);
	}

	/**
	 * Wi-Fi 및 3G/LTE 상태 감시(Wi-Fi 감도, Wi-Fi/3G/LTE on/off)
	 * 
	 * @author dckim
	 */
	public class WifiStateReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (null == mMainService)
				return;

//			Log.e(LOG_TAG, "WifiStateReceiver onReceive Action : " + intent.getAction());
//			Log.e(LOG_TAG, "WifiStateReceiver onReceive State : " + intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1));
//			Log.e(LOG_TAG, "WifiStateReceiver onReceive RSSI : " + intent.getIntExtra(WifiManager.EXTRA_NEW_RSSI, 0));
//			Log.e(LOG_TAG, "WifiStateReceiver onReceive RSSI level : " +  WifiManager.calculateSignalLevel(intent.getIntExtra(WifiManager.EXTRA_NEW_RSSI, 0), 5));

			if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {

				if (WifiManager.WIFI_STATE_ENABLED != intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1)) {
					Preferences.setWifiSignalLevel(mMainService, 0);
				}

			} else if (WifiManager.RSSI_CHANGED_ACTION.equals(intent.getAction())) {

				if (!isValidWifi()) {
					Preferences.setWifiSignalLevel(mMainService, 0);
					return;
				}

				// RSSI 는 -100에 가까울수록 안좋고 0에 가까울수록 좋음
				int level = WifiManager.calculateSignalLevel(intent.getIntExtra(WifiManager.EXTRA_NEW_RSSI, 0), 5);

				if (level != Preferences.getWifiSignalLevel(mMainService))
					Preferences.setWifiSignalLevel(mMainService, level);

			} else if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {

				NetworkInfo ni = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
				if (null == ni || !ni.isAvailable()) {
					Preferences.setWifiSignalLevel(mMainService, 0);
					return;
				}

				if (ConnectivityManager.TYPE_WIFI == ni.getType() && NetworkInfo.State.CONNECTED != ni.getState()) {
					Preferences.setWifiSignalLevel(mMainService, 0);
				} else if (ConnectivityManager.TYPE_MOBILE == ni.getType()) {
					// 예외적으로 3G/LTE의 경우 -1을 설정하고 GUI상으로 아이콘이 나타나지 안도록 함이다.
					Preferences.setWifiSignalLevel(mMainService, NetworkInfo.State.CONNECTED == ni.getState() ? -1 : 0);
					return;
				}
			}
		}

		/**
		 * 와이파이 사용의 유효성 체크
		 * 
		 * @return true이면 사용가능한 상태임
		 */
		public boolean isValidWifi() {

			if (null == mWifiMgr)
				return false;

			WifiInfo wInfo = mWifiMgr.getConnectionInfo();
			if (null == wInfo)
				return false;

			// 연결상태 확인
			if (0 != wInfo.getIpAddress() && null != wInfo.getSSID() && wInfo.getSupplicantState().toString().equals("COMPLETED")) {
				DetailedState ds = WifiInfo.getDetailedStateOf(wInfo.getSupplicantState());
				if (ds == DetailedState.CONNECTED || ds == DetailedState.OBTAINING_IPADDR)
					return true;
			}

			return false;
		}
	}

	/**
	 * 학습 콘텐츠 중 하나의 파일을 다운로드 한다.<br>
	 * (반복 사용함으로 다수의 콘텐츠를 다운로드함)
	 * 
	 * @author dckim
	 */
	private class DownTask extends AsyncTask<UnitFile, Integer, Integer> {

		private final int CONN_TIMEOUT = 10 * 3000;
		private final int READ_TIMEOUT = 10 * 3000;
		private boolean mIsCanceled = false;
		private File mFile = null;
		private UnitFile mUnit = null;
		private int mFailReason = AgentEvent.REASON_NO_ERROR;
		private int mResCode = HttpURLConnection.HTTP_OK;
		private String mResInFo = "";

		protected void onPreExecute() {

			if (null != mMainService) {
				mMainService.LockAcquire();
			}
		}

		protected Integer doInBackground(UnitFile... params) {

			Integer result = -1;

			if (1 > params.length) {
				mFailReason = AgentEvent.REASON_UNKNOWN_ERROR;
				return result;
			}

			mUnit = params[0];

			//SD카드의 현재상태 조사
			if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
				Log.e(LOG_TAG, "ExternalStorageState : MEDIA_UNMOUNTED");
				mFailReason = AgentEvent.REASON_UNMOUNTED_STORAGE;
				return result;
			}

			/* if(mUnit.mDownloadUrl.equals("http://yoons.beflys.gscdn.com/smartbefly/contents/studyExt/HomeStudy/5882/1/01A.zip")){
				mUnit.mDownloadUrl="http://yoons.beflys.gscdn.com/smartbefly/contents/studyExt/HomeStudy/5881/1/01A.zip";
			}*/
			Log.e(LOG_TAG, "Download URL : " + mUnit.mDownloadUrl);
			Log.e(LOG_TAG, "Download file : " + mUnit.mSavePath);
			// 파일이 이미 존재한다면, 삭제한다.
			File curr = new File(mUnit.mSavePath);
			if (curr.exists())
				curr.delete();

			long freeBytes = StorageUtil.getStorageFreeSpace(StorageUtil.getStorageDirectoryPath(StorageUtil.INTERNAL_STORAGE));
			if (0 >= freeBytes) {
				// Removed storage 
				mFailReason = (0 == freeBytes) ? AgentEvent.REASON_NOT_ENOUGH_SPACE : AgentEvent.REASON_UNMOUNTED_STORAGE;
				return result;
			} else if (MINIMUM_FREE_SPACE > freeBytes) {
				// Not enough storage
				mFailReason = AgentEvent.REASON_NOT_ENOUGH_SPACE;
				return result;
			}

			HttpURLConnection con = null;
			FileOutputStream fos = null;
			BufferedInputStream bis = null;
			InputStream is = null;

			try {
				URL url = new URL(new URI(null, mUnit.mDownloadUrl, null).toASCIIString());
				con = (HttpURLConnection) url.openConnection();
				if (null == con) {
					mFailReason = AgentEvent.REASON_NET_ERROR;
					return result;
				}

				con.setConnectTimeout(CONN_TIMEOUT);
				con.setReadTimeout(READ_TIMEOUT);
				con.setUseCaches(false);
				// OutputStream으로 GET 데이터를 넘겨주겠다는 옵션.
				con.setDoOutput(false);

				mResCode = con.getResponseCode();
				if (HttpURLConnection.HTTP_OK != mResCode && mUnit.mDownloadUrl.contains("contents/answer")) {//구교재 일경우 빈파일 생성
					mkdirs(mUnit.mSavePath);
					fos = new FileOutputStream(mFile = new File(mUnit.mSavePath));
					fos.flush();
					fos.getFD().sync();
					fos.close();
					fos = null;
					mFile = null;

					return 0;

				} else if (HttpURLConnection.HTTP_OK != mResCode) {
					Log.i(LOG_TAG, "RESPONSE CODE:" + mResCode);
					con.disconnect();
					mFailReason = AgentEvent.REASON_NET_ERROR;
					if (mResCode == HttpURLConnection.HTTP_NOT_FOUND) {
						mResInFo = mUnit.mDownloadUrl;
					}

					return result;
				}

				is = con.getInputStream();
				bis = new BufferedInputStream(is);
				mkdirs(mUnit.mSavePath);

				if (!mIsCanceled) {

					int readSize = 0;
					byte[] buff = new byte[2048];

					fos = new FileOutputStream(mFile = new File(mUnit.mSavePath));

					while (!mIsCanceled && (readSize = bis.read(buff)) != -1)
						fos.write(buff, 0, readSize);

					Log.e(LOG_TAG, "mIsCanceled : " + mIsCanceled);
					if (!mIsCanceled) {
						fos.flush();
						fos.getFD().sync();
						fos.close();
						fos = null;
						mFile = null;

						result = 0;
						mDbUtil.deleteDownloadContent(mUnit.mDownloadId);
					}
				}

			} catch (Exception e) {
				result = -1;
				e.printStackTrace();
				File failFile = new File(mUnit.mSavePath);
				if (failFile.exists())
					failFile.delete();

				ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, e);
			} finally {
				try {
					if (null != bis)
						bis.close();
					if (null != is)
						is.close();
					if (null != fos)
						fos.close();

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (null != con)
						con.disconnect();
				}
			}

			return result;
		}

		protected void onProgressUpdate(Integer... progress) {
		}

		protected void onPostExecute(Integer result) {

			if (null != mMainService)
				mMainService.LockRelease();

			mDownTask = null;

			if (result.equals(0)) {
				// success
				mRetryCount = 0;
				next();
				ResponseState();
				if (hasDownloadContent()) {
					startDownload();
				} else {
					stopDownload();
				}
			} else if (mRetryCount < MAX_RETRY_COUNT && hasDownloadContent()) {
				mRetryCount++;
				Log.i(LOG_TAG, "RETRY COUNT:" + mRetryCount);
				startDownload();
			} else {
				// retry in case of failure
				mRetryCount = 0;
				ResponseFail(mFailReason, mResCode, mResInFo);
			}
		}

		protected void onCancelled() {
			super.onCancelled();
		}

		protected void cancel() {
			mIsCanceled = true;

			if (!mDownTask.isCancelled()) {
				super.cancel(true);

				try {
					if (null != mFile && mFile.exists()) {
						mFile.delete();

						mFile = null;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			ResponseStop();

			if (null != mMainService)
				mMainService.LockRelease();

			mDownTask = null;
			mIsDownloading = false;
		}
	}

	/**
	 * Storage State Receiver Class
	 */
	public class StorageStateReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			String strAction = intent.getAction();
			if (null == strAction)
				return;

			Log.w(LOG_TAG, "strAction:" + strAction);
			if (strAction.equalsIgnoreCase(Intent.ACTION_MEDIA_MOUNTED)) {
				// SD Card Mounted
				Uri uri = intent.getData();
				if (null != uri) {
					// INTERNAL_STORAGE가 mounted되면 다운로드 재개
					String path = StorageUtil.getStorageDirectoryPath(StorageUtil.INTERNAL_STORAGE);
					if (null != path && path.equalsIgnoreCase(uri.getPath()))
						startDownload();
				}
			} else if (strAction.equalsIgnoreCase(Intent.ACTION_MEDIA_EJECT)) {
				// SD Card 탈착
				//stopDownload();
			} else if (strAction.equalsIgnoreCase(Intent.ACTION_MEDIA_UNMOUNTED)) {
				// 외장 스토리지 삽입시 종료되는 무제로 인해 getData의 값을 검사함.				 
				// SD Card UnMounted
				if (null != mMainService && intent.getData().toString().contains("/mnt/sdcard")) {
					// DB가 External Storage에 있으므로 저장소가 제거되면 문제 심각
					mMainService.stopService(new Intent(mMainService, DownloadService.class));
					// app 종료
					mMainService.sendBroadcast(new Intent(BaseActivity.ACTIVITY_LOG_OUT_EVENT));
				}
			}
		}
	}

	/**
	 * RequestAgentEvent Receiver Class
	 */
	public class RequestAgentEventReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			String strAction = intent.getAction();
			if (null == strAction)
				return;

			if (!strAction.equalsIgnoreCase(AgentEvent.ACTION_SERVICE_AGENT))
				return;

			switch (intent.getIntExtra(AgentEvent.EVENT_TYPE, 0)) {
			case AgentEvent.DOWNLOAD_START:
				start();
				break;
			case AgentEvent.DOWNLOAD_STOP:
				stop();
				break;
			case AgentEvent.CONTENTS_UPDATE:
				update();
				break;
			case AgentEvent.DOWNLOAD_STATE:
				ResponseState();
				break;
			}
		}
	}

	/**
	 * 네트워크 사용량 체크 [ 각각의 사용량 구분(wi-fi of 3g/lte) 및 기록 ] 1. 위 두 함수를 이용하여 해당 앱이
	 * 사용하는 네트워크 사용량을 얻음. 2. 1에서 얻은 값을 저장(이전 값이 있으면 "현재 값 - 이전 값" 한 주기 동안의 사용량으로
	 * 기록(전체 네트워크 사용량) 3. 현재 Mobile(data network)에 연결되어 있다면 2번과 동일하게 계산하여 사용량으로
	 * 기록(Data Network의 사용량(3G/Lte)) 4. 네트워크가 끊어지거나, 네트워크 상태가 변경되면 그때까지 취합된
	 * 값(한번의 주기 동안)을 기록함. (이 경우는 체크가 반복되는 한번의 주기가 돌아오기 전에 끊어진 경우를 체크하기 위함)
	 * 
	 * @param netType
	 *            접속 타입(wi-fi, mobile)
	 * @param state
	 *            접속 형태(connected, disconnected)
	 * @param loop
	 *            주기적으로 반복하여 체크 할 경우 true, 한번만 사용할 경우 false 설정
	 */
	private synchronized void checkTraffic(int netType, NetworkInfo.State state, boolean loop) {

		removeMessages(AgentEvent.CHECK_TRAFFIC);

		if (NET_TRAFFIC_NONE == mNetTrafficType)
			return;

		long backupTx = TrafficStats.UNSUPPORTED, backupRx = TrafficStats.UNSUPPORTED;
		long usedMobileBytes = 0, usedBytes = 0;
		long lTx = 0, lRx = 0;

		// CONNECTED
		if (NetworkInfo.State.CONNECTED == state) {

			lTx = TrafficStats.getUidTxBytes(mUID);
			lRx = TrafficStats.getUidRxBytes(mUID);

			// 전체 네트워크
			if (0 < (mNetTrafficType & NET_TRAFFIC_ALL)) {
				// ALL NETWORK (send)
				if (TrafficStats.UNSUPPORTED == mTX) {
					mTX = lTx;
				} else {
					if (TrafficStats.UNSUPPORTED != lTx) {
						backupTx = mTX;
						usedBytes += lTx - mTX;
						mTX = lTx;
					}
				}

				// receive
				if (TrafficStats.UNSUPPORTED == mRX) {
					mRX = lRx;
				} else {
					if (TrafficStats.UNSUPPORTED != lRx) {
						backupRx = mRX;
						usedBytes += lRx - mRX;
						mRX = lRx;
					}
				}

				// size append (usedBytes)
				if (0 < usedBytes && null != mDbUtil) {
					Log.w(LOG_TAG, "usedBytes(all):" + usedBytes);
					if (!mDbUtil.addNetworkUsed((int) usedBytes, false)) {
						// 저장 안 되었을 경우 이전 값으로 재 설정(다음에 누적값으로 저장하기 위함)
						mRX = backupRx;
						mTX = backupTx;
					}
				}
			}

			// 모바일(DATA NETWORK) 네트워크
			if (0 < (mNetTrafficType & NET_TRAFFIC_MOBILE) && ConnectivityManager.TYPE_MOBILE == netType) {
				// send
				if (TrafficStats.UNSUPPORTED == mMobileTX) {
					mMobileTX = lTx;
				} else {
					if (TrafficStats.UNSUPPORTED != lTx) {
						backupTx = mMobileTX;
						usedMobileBytes += lTx - mMobileTX;
						mMobileTX = lTx;
					}
				}

				// receive
				if (TrafficStats.UNSUPPORTED == mMobileRX) {
					mMobileRX = lRx;
				} else {
					if (TrafficStats.UNSUPPORTED != lRx) {
						backupRx = mMobileRX;
						usedMobileBytes += lRx - mMobileRX;
						mMobileRX = lRx;
					}
				}

				// size append (usedBytes)
				if (0 < usedMobileBytes && null != mDbUtil) {
					Log.w(LOG_TAG, "usedBytes(mobile):" + usedMobileBytes);
					if (!mDbUtil.addNetworkUsed((int) usedMobileBytes)) {
						// 저장 안 되었을 경우 이전 값으로 재 설정(다음에 누적값으로 저장하기 위함)
						mMobileRX = backupRx;
						mMobileTX = backupTx;
					}
				}
			}

			if (loop) {
				sendEmptyMessageDelayed(AgentEvent.CHECK_TRAFFIC, MSG_TRAFFIC_INTERVAL);
			}

			return;
		}

		// DISCONNECTED
		// 전체 네트워크
		if (0 < (mNetTrafficType & NET_TRAFFIC_ALL)) {
			// DATA NETWORK => DISCONNECTED
			if (TrafficStats.UNSUPPORTED != mTX) {
				// 사용량 적용(누적할 값)
				if (0 == lTx)
					lTx = TrafficStats.getUidTxBytes(mUID);

				if (TrafficStats.UNSUPPORTED != lTx) {
					backupTx = mTX;
					usedBytes += lTx - mTX;
					//Log.e(LOG_TAG, "lTx:"+usedBytes);
				}

				mTX = TrafficStats.UNSUPPORTED;
			}

			if (TrafficStats.UNSUPPORTED != mRX) {
				// 사용량 적용(누적할 값)
				if (0 == lRx)
					lRx = TrafficStats.getUidRxBytes(mUID);

				if (TrafficStats.UNSUPPORTED != lRx) {
					backupRx = mRX;
					usedBytes += lRx - mRX;
					//Log.e(LOG_TAG, "lRx:"+usedBytes);
				}

				mRX = TrafficStats.UNSUPPORTED;
			}

			// size append (usedBytes) 
			if (0 < usedBytes && null != mDbUtil) {
				if (!mDbUtil.addNetworkUsed((int) usedBytes, false)) {
					// 저장 안 되었을 경우 이전 값으로 재 설정(다음에 누적값으로 저장하기 위함)
					Log.w(LOG_TAG, "usedBytes(all):" + usedBytes);
					mTX = backupTx;
					mRX = backupRx;
				}
			}
		}

		// 모바일(DATA NETWORK) 네트워크
		if (0 < (mNetTrafficType & NET_TRAFFIC_MOBILE)) {
			if (TrafficStats.UNSUPPORTED != mMobileTX) {
				// 사용량 적용(누적할 값)
				if (0 == lTx)
					lTx = TrafficStats.getUidTxBytes(mUID);

				if (TrafficStats.UNSUPPORTED != lTx) {
					backupTx = mMobileTX;
					usedMobileBytes += lTx - mMobileTX;
					//Log.e(LOG_TAG, "lTx:"+usedBytes);
				}

				mMobileTX = TrafficStats.UNSUPPORTED;
			}

			if (TrafficStats.UNSUPPORTED != mMobileRX) {
				// 사용량 적용(누적할 값)
				if (0 == lRx)
					lRx = TrafficStats.getUidRxBytes(mUID);

				if (TrafficStats.UNSUPPORTED != lRx) {
					backupRx = mMobileRX;
					usedMobileBytes += lRx - mMobileRX;
					//Log.e(LOG_TAG, "lRx:"+usedBytes);
				}

				mMobileRX = TrafficStats.UNSUPPORTED;
			}

			// size append (usedBytes) 
			if (0 < usedMobileBytes && null != mDbUtil) {
				if (!mDbUtil.addNetworkUsed((int) usedMobileBytes)) {
					// 저장 안 되었을 경우 이전 값으로 재 설정(다음에 누적값으로 저장하기 위함)
					Log.w(LOG_TAG, "usedBytes(mobile):" + usedMobileBytes);
					mMobileTX = backupTx;
					mMobileRX = backupRx;
				}
			}
		}
	}

	/** 환경설정 값 변경 감시 */
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

		if (null == key)
			return;

		if (Preferences.KEY_USE_DATA_NETWORK.equals(key)) {

			// 이동통신망(3G/LTE) 사용 여부
			//Log.w(LOG_TAG, "SharedPreferenceChanged() key:" + key);
			if (!Preferences.getUseDataNetwork(prefs)) {
				stopDownload();
				return;
			}

			startDownload();
		} else if (Preferences.KEY_DOWNLOAD_ON_DATA_NETWORK.equals(key)) {

			// 이동통신망(3G/LTE) 컨텐츠 다운로드
			//Log.w(LOG_TAG, "SharedPreferenceChanged() key:" + key);	
			if (!Preferences.getDownloadOnDataNetwork(prefs)) {
				stopDownload();
				return;
			}

			startDownload();
		} else if (Preferences.KEY_STUDYING.equals(key)) {

			// 학습 중 데이터 다운로드 허용
			if (Preferences.getStudying(prefs)) {
				//Log.w(LOG_TAG, "SharedPreferenceChanged() studying=true");	
				return;
			}

			//Log.w(LOG_TAG, "SharedPreferenceChanged() studying=false");
			startDownload();
		} else if (Preferences.KEY_ALLOW_DOWNLOADING_OF_STUDYING.equals(key)) {

			// 학습 중 데이터 다운로드 허용
			//boolean value = Settings.getAllowDownloadingOfStudying(prefs);
			//Log.w(LOG_TAG, "SharedPreferenceChanged() key:" + key);	
		} else {

			//Log.w(LOG_TAG, "SharedPreferenceChanged() key:" + key);	
		}
	}

	/**
	 * 설정된 위치에 폴더를 생성함.
	 * 
	 * @param fullPath
	 *            생성할 폴더의 전체 경로(마지막 항목이 파일이면 파일명으로 폴더가 생기지 않도록 함, 마지막 항목 제외함)
	 * @return true이면 정상 처리됨을 의미함.
	 */
	boolean mkdirs(String fullPath) {

		if (null == fullPath)
			return false;

		StringBuffer sb = new StringBuffer();
		List<String> itr = (Uri.parse(fullPath)).getPathSegments();

		int cnt = itr.size();
		for (int i = 0; i < cnt; i++) {
			String s = itr.get(i);

			// 마지막 항목이 파일이면 제외
			if (i == (cnt - 1)) {
				if (!s.contains(".")) {
					sb.append(File.separator);
					sb.append(s);
				}
			} else {
				sb.append(File.separator);
				sb.append(s);
			}
		}

		if (0 >= sb.length())
			return false;

		File f = new File(sb.toString());
		if (f.exists())
			return true;

		return f.mkdirs();
	}

	/**
	 * 상태 값(2개)을 저장하기 위한 객체 total(전체 갯수), curr(현재 순차)
	 * 
	 * @author dckim
	 */
	public class CountInfo {

		public int total = 0;
		public int curr = 0;

		public CountInfo() {
		}

		public CountInfo(int total, int curr) {
			this.total = total;
			this.curr = curr;
		}
	}
}
