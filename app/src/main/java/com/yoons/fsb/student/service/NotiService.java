package com.yoons.fsb.student.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotiService extends Service {
	private String TAG = "NotiService";
	private HttpJSONRequest request;
	private Thread callTread;
	private int callCheckTime = 0;
	//private boolean first_Check=true;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		request = new HttpJSONRequest(this); //

		Log.e(TAG, "onCreate()");
		startBookCheckThread();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy()");
		callTread.interrupt();
		callTread = null;
	}

	/**
	 * 5분 마다 StudyDataUtil.requestStudyResultBookCheck()를 수행하는 Thread를 시작함
	 */
	private void startBookCheckThread() {
		callTread = new Thread("bookCheckThread") {
			public void run() {
				while (null != callTread && !callTread.isInterrupted()) {
					SystemClock.sleep(1000);

					callCheckTime += 1000;

					// 30초
					if (10000 == callCheckTime/* &&first_Check */) {
						//mHandler.sendEmptyMessage(ServiceCommon.MSG_WHAT_CALL_NOTI);
						callCheckTime = 0;
						request.requestNoti(mHandler, ServiceCommon.MSG_WHAT_CALL_NOTI);
					}
				}
			}
		};

		callTread.start();
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Log.e(TAG, msg.toString());

			String status = "";

			if (msg.obj != null) {
				JSONObject obj = (JSONObject) (msg.obj);
				try {
					JSONArray array = new JSONArray(obj.getString("out1"));

					for (int i = 0; i < array.length(); i++) {
							status = array.getJSONObject(i).getString("command");
						if (status.equals("ATTENTION")) {//집중일때
							sendBroadcast(new Intent(ServiceCommon.ACTION_NOTI_FOUCS));
						} else if (status.equals("MANAGE")) {//호출일때
							StudyData.getInstance().call = true;

						} else if (status.equals("KEEPGOING")) {//취소일때
							StudyData.getInstance().call = false;
						} else if (status.equals("INITIALIZE")) {//중복로그인
							sendBroadcast(new Intent(ServiceCommon.ACTION_NOTI_INIT));
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				/* first_Check=false; */
			}
		}
	};
}
