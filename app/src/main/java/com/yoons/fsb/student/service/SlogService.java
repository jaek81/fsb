package com.yoons.fsb.student.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SlogService extends Service {

	public static final int TERM_MSEC = 10000;
	private String TAG = "SlogService";
	private HttpJSONRequest request;
	private Thread callTread;
	private int callCheckTime = 0;
	private String curStatus = ServiceCommon.STATUS_START;
	private Context mContext = null;
	private long StartTime = 0;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		StartTime = System.currentTimeMillis();

		request = new HttpJSONRequest(this); //
		mContext = this;
		Log.e(TAG, "onCreate()");
		startBookCheckThread();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy()");
		callTread.interrupt();
		callTread = null;
	}

	/**
	 * 5분 마다 StudyDataUtil.requestStudyResultBookCheck()를 수행하는 Thread를 시작함
	 */
	private void startBookCheckThread() {
		callTread = new Thread("bookCheckThread") {
			public void run() {
				while (null != callTread && !callTread.isInterrupted()) {
					SystemClock.sleep(1000);

					callCheckTime += 1000;
					StudyData data = StudyData.getInstance();
//					if (!(data.audioPlaying.equals(ServiceCommon.STATUS_PAUSE) && curStatus.equals(ServiceCommon.STATUS_PAUSE)) && !(data.audioPlaying.equals(ServiceCommon.STATUS_STOP) && curStatus.equals(ServiceCommon.STATUS_STOP)) || !data.mCurrentStatus.equalsIgnoreCase("S11")) {//pause랃 stop 은 계속 안보내기

					// 10초
					if (TERM_MSEC == callCheckTime) {
						callCheckTime = 0;
						//데이터 다시확인
						int audioTime = 0;
						int playingTime = 0;
						String playStatus = "";
						int stepDuration = 0;
						int aCnt = 0;
						int aTime = 0;
						int mCnt = 0;
						int mTime = 0;
						int mResultNo = 0;
						int mRtime=0;

						if (data.mCurrentStatus.contains("S")) {
							mResultNo = data.mStudyResultNo;
							stepDuration = (int) ((System.currentTimeMillis() - StartTime) / 1000);
						} else if (data.mCurrentStatus.contains("R")) {
							mResultNo = data.mStudyResultNo;
							data.mReviewDuration = data.mReviewDuration + SlogService.TERM_MSEC / 1000;
							stepDuration = data.mReviewDuration;
						}

						audioTime = data.mAudioTotalMin / 1000;
						playingTime = data.mAudioStudyTime / 1000;
						playStatus = data.audioPlaying;
						curStatus = data.audioPlaying;
						//stepDuration = data.mAudioStudyTime / 1000;
						aCnt = (data.mAudioRecordIndex - data.mRecMCnt) < 0 ? 0 : (data.mAudioRecordIndex - data.mRecMCnt);//녹음은 전체 녹음 카운트 하기 때문에 빼야한다.
						aTime = data.mTotalRecTime;
						mCnt = data.mRecMCnt;
						mTime = data.mRecMTime ;
						mRtime= data.mRecRTime * data.mRTagRepeatCnt;

						if (!data.mCurrentStatus.equals("S11")) {
							playStatus = "PLAY";
						}

						if (!"".equals(playStatus)&&!"SFN".equals(data.mCurrentStatus)) {
							try {
								request.requestStudyStatus(String.valueOf(String.valueOf(data.mCustomerNo)), //MCODE
										String.valueOf(data.mProductNo), //교재
										String.valueOf(URLEncoder.encode(data.mStudyUnitCode, "UTF-8")), //차시
										String.valueOf(data.mReStudyCnt), //몇회
										String.valueOf(data.mCurrentStatus), //단계
										String.valueOf(playStatus), //플레이 상태
										String.valueOf(CommonUtil.getCurrentDateTime2()), //시간
										String.valueOf(stepDuration), //단계진행시간
										String.valueOf(audioTime), //총 오디오 시간
										String.valueOf(playingTime), //위치
										String.valueOf(aCnt), //자동 녹음 누적 횟수(전체-수동)
										String.valueOf(aTime), //자동 녹음 누적 시간
										String.valueOf(mCnt), //수동 녹음 누적 횟수
										String.valueOf(mTime), //수동 녹음 누적 시간
										String.valueOf(mRtime), //권장 녹음 시간
										String.valueOf(data.mRecordFileYmd),
										String.valueOf(CommonUtil.getMacAddress()), //MAC 주소
										String.valueOf(mResultNo));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else if (callCheckTime > 10000) {
						callCheckTime = 0;
					}

				}
			}
		};

		callTread.start();
	}

	/*
	 * private Handler mHandler = new Handler() {
	 * 
	 * @Override public void handleMessage(Message msg) { Log.e(TAG,
	 * msg.toString()); switch (msg.what) {
	 * 
	 * case ServiceCommon.MSG_WHAT_SLOG: if (msg.arg1 == 1) //이거 나중에 정해지면 변경
	 * sendBroadcast(new Intent(ServiceCommon.ACTION_SLOG));
	 * 
	 * break;
	 * 
	 * default: super.handleMessage(msg); } } };
	 */
}
