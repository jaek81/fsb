package com.yoons.fsb.student.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomVideoPlayer;
import com.yoons.fsb.student.custom.CustomVorbisPlayer;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.ui.base.BaseStudyActivity;
import com.yoons.fsb.student.ui.control.BothSideRoundProgressBar;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.sec.study.OneWeekTestActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.hssb.student.custom.CustomMp3Recorder;

import java.io.File;

/**
 * 복습(본문 듣기) 화면
 *
 * @author jaek
 */
public class AudioReStudyActivity extends BaseStudyActivity implements OnClickListener, OnCompletionListener, OnPreparedListener, OnTouchListener {

    private RelativeLayout mParentLayout = null;
    private ImageView mPlay = null, mSeekBack = null, mSeekFwd = null;
    private ImageView mRecord = null, mWrite = null;
    private TextView mPlayTimeView, mEndTimeView = null;
    private SeekBar mProgress = null;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // Igse
            setContentView(R.layout.igse_audio_restudy_main);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                setContentView(R.layout.w_audio_restudy_main);
            else // 숲
                setContentView(R.layout.f_audio_restudy_main);
        }

        mStudyData = StudyData.getInstance();
        mRecordFilePath = ServiceCommon.CONTENTS_PATH + ServiceCommon.AUDIO_RECORD_FILE_NAME + ServiceCommon.MP3_FILE_TAIL;

        if (!checkValidStudyData())
            return;

        setWidget();

        // TitleView
        tBookName.setText(mStudyData.mReviewProductName);
        tCategory.setText(R.string.string_restudy);
        tCategory2.setText(R.string.string_audio_restudy);

        //setReStudyStatus();

        readyStudy();

        Crashlytics.log(getString(R.string.string_ga_AudioReStudyActivity));
    }

    @Override
    protected void onPause() {
        // 학습 중 화면 잠금, 꺼짐 시 간지로 재시작
        if (!isFinishing()) {
            if (mIsReStart) {
                startActivity(new Intent(this, ReStudyTitlePaperActivity.class).putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true).putExtra(ServiceCommon.TARGET_ACTIVITY, ServiceCommon.STATUS_REVIEW_AUDIO));
            }

            setFinish();
            finish();
        }

        super.onPause();
    }

    /**
     * 화면 구성 완료 후 호출됨 학습 가이드의 Width, Height 값을 저장함
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            View tooltip = findViewById(R.id.tooltip_record_text);
            if (null != tooltip) {
                mTooltipHeight = tooltip.getHeight();
                mTooltipWidth = tooltip.getWidth();
                mParentLayout.removeView(tooltip);
            }
        }
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                    try {
                        if (mVorbisPlayer.singleProcessChecker() || mVorbisPlayer.isSeeking())
                            return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return true;
                    }
                    boolean isExcute = false;
                    int btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;

                    if (mSeekBack.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                        mSeekBack.setPressed(true);
                        mSeekBack.playSoundEffect(SoundEffectConstants.CLICK);
                        btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
                        isExcute = true;
                    } else if (mSeekFwd.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                        mSeekFwd.setPressed(true);
                        mSeekFwd.playSoundEffect(SoundEffectConstants.CLICK);
                        btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
                        isExcute = true;
                    }

                    if (isExcute)
                        mVorbisPlayer.playerSeekPrecede(btnTag);
                } else if (View.VISIBLE == findViewById(R.id.animation_study_layout).getVisibility()) {
                    int a = 0;
                    if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                        // 앞으로 감기
                        if (mVideoPlayer != null && mVideoPlayer.mPlayer != null) {
                            a = mVideoPlayer.mPlayer.getCurrentPosition() + 5000; // 현재 재생되는 시간 +  500
                            mVideoPlayer.mPlayer.seekTo(a);
                        }
                    } else if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                        // 뒤로 감기
                        if (mVideoPlayer != null && mVideoPlayer.mPlayer != null) {
                            a = mVideoPlayer.mPlayer.getCurrentPosition() - 5000; // 현재 재생되는 시간 - 500
                            mVideoPlayer.mPlayer.seekTo(a);
                        }
                    }
                }

                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                    if (mPlay.isEnabled())
                        mPlay.setPressed(true);
                    else if (mWrite.isEnabled() && mIsWriting)
                        mWrite.setPressed(true);
                    else if (mRecord.isEnabled() && mMp3Recorder.isRecording())
                        mRecord.setPressed(true);
                } else if (View.VISIBLE == findViewById(R.id.animation_study_layout).getVisibility()) {
                    if (null != mVideoPlayer && mVideoPlayer.isPrepared())
                        mVideoPlayer.setPlayPressed(true);
                }

                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_STOP:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                    if (mRecord.isEnabled() && !mMp3Recorder.isRecording())
                        mRecord.setPressed(true);
                }

                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                    boolean isExcute = false;
                    int btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;

                    if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                        mSeekBack.setPressed(false);
                        if (mSeekBack.isEnabled()) {
                            btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
                            isExcute = true;
                        }
                    } else if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                        mSeekFwd.setPressed(false);
                        if (mSeekFwd.isEnabled()) {
                            btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
                            isExcute = true;
                        }
                    }

                    if (isExcute)
                        mVorbisPlayer.playerSeekExcute(btnTag);
                }

                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                try {
                    if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                        mPlay.setPressed(false);
                        mWrite.setPressed(false);
                        mRecord.setPressed(false);
                        if (mPlay.isEnabled()) {
                            if (mVorbisPlayer.isPlaying())
                                playerPause();
                            else
                                playerResume();
                        } else if (mWrite.isEnabled() && mIsWriting) {
                            writeEnd();
                        } else if (mRecord.isEnabled() && mMp3Recorder.isRecording()) {
                            recordEnd();
                        }
                    } else if (View.VISIBLE == findViewById(R.id.animation_study_layout).getVisibility()) {
                        if (null != mVideoPlayer && mVideoPlayer.isPrepared()) {
                            mVideoPlayer.setPlayPressed(false);
                            if (mVideoPlayer.isPlaying())
                                mVideoPlayer.pauseBtn();
                            else
                                mVideoPlayer.startBtn();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_STOP:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                    mRecord.setPressed(false);
                    if (mRecord.isEnabled()) {
                        if (!mMp3Recorder.isRecording())
                            recordReady();
                    }
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (null != mVorbisPlayer && mVorbisPlayer.isSeeking()) {
            v.setPressed(false);
            return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {

        if (!mIsStudyStart)
            return;

        int id = v.getId();

        if (singleProcessChecker())
            return;

        switch (id) {
            case R.id.audio_study_play_btn:
                if (null != mVorbisPlayer && mVorbisPlayer.isPlaying())
                    playerPause();
                else
                    playerResume();
                break;

            case R.id.audio_study_record_btn:
                if (null != mMp3Recorder && !mMp3Recorder.isRecording())
                    recordReady();
                else
                    recordEnd();
                break;

            case R.id.audio_study_write_btn:
                if (!mIsWriting)
                    writeStart();
                else
                    writeEnd();
                break;
        }
    }

    /**
     * StudyData의 유효성을 검사함
     */
    private boolean checkValidStudyData() {
        boolean isValid = true;

        if (null == mStudyData) {
            isValid = false;
            Log.e("", "checkValidStudyData studyData is null !!");
        } else {
            if (mStudyData.mReviewISBodyVideoExist) {
                File fileMovie = new File(mStudyData.mReviewBodyVideoFile);
                if (!fileMovie.exists())
                    isValid = false;
                Log.e("", "checkValidStudyData mReviewMoveFile => " + mStudyData.mReviewBodyVideoFile);
            } else {
                File fileYda = new File(mStudyData.mReviewBodySoundFile);
                if (!fileYda.exists())
                    isValid = false;
                Log.e("", "checkValidStudyData mAudioSoundFile => " + mStudyData.mReviewBodySoundFile);
            }

        }

        if (!isValid)
            Toast.makeText(this, getString(R.string.string_common_study_file_error), Toast.LENGTH_SHORT).show();

        return isValid;
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        if (mStudyData.mReviewISBodyVideoExist) {
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
            findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);
            findViewById(R.id.title_view).setVisibility(View.GONE);
            findViewById(R.id.animation_study_layout).setVisibility(View.VISIBLE);
        } else {
            mParentLayout = (RelativeLayout) findViewById(R.id.audio_restudy_parent_layout);
            mPlay = (ImageView) findViewById(R.id.audio_study_play_btn);
            mSeekBack = (ImageView) findViewById(R.id.audio_study_seek_back_btn);
            mSeekFwd = (ImageView) findViewById(R.id.audio_study_seek_fwd_btn);
            mRecord = (ImageView) findViewById(R.id.audio_study_record_btn);
            mWrite = (ImageView) findViewById(R.id.audio_study_write_btn);
            mPlayTimeView = (TextView) findViewById(R.id.audio_study_position_text);
            mEndTimeView = (TextView) findViewById(R.id.audio_study_position_text2);
            mProgress = (SeekBar) findViewById(R.id.audio_study_progressbar);

            // seek 동작을 위한 버튼 구별 tag
            mSeekBack.setTag(ServiceCommon.BUTTONTAG_SEEK_BACK);
            mSeekFwd.setTag(ServiceCommon.BUTTONTAG_SEEK_FWD);

            mSeekBack.setEnabled(false);
            mSeekFwd.setEnabled(false);

            mRecord.setOnClickListener(this);
            mPlay.setOnClickListener(this);
            mWrite.setOnClickListener(this);

            mPlay.setOnTouchListener(this);
            mWrite.setOnTouchListener(this);
            mRecord.setOnTouchListener(this);

            setTitlebarCategory(getString(R.string.string_titlebar_category_restudy));
            setTitlebarText(mStudyData.mReviewProductName);
            // WiFi 감도 아이콘 설정
            setPreferencesCallback();

            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);
            tooltip.setVisibility(View.INVISIBLE);
            TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));
            tipText.setText(getResources().getString(R.string.string_record_tool_tip));
            mParentLayout.addView(tooltip);
        }
    }

    /**
     * 복습 화면 상단 Status를 설정함
     */
    private void setReStudyStatus() {
        final int reStudy = 0x01, audio = 0x02, word = 0x04, sentence = 0x08;
        // 모든 복습
        final int allReStudy = reStudy | audio | word | sentence;
        // 본문 + 단어보충
        final int audioWordReStudy = reStudy | audio | word;
        // 본문 + 문장보충
        final int audioSentenceReStudy = reStudy | audio | sentence;
        // 본문
        final int audioReStudy = reStudy | audio;

        int reStudyStatus = reStudy;

        if (mStudyData.mReviewIsBodyExist)
            reStudyStatus |= audio;

        if (mStudyData.mReviewISBodyVideoExist)
            reStudyStatus |= audio;

        if (0 < mStudyData.mReviewWordQuestion.size())
            reStudyStatus |= word;

        if (0 < mStudyData.mReviewSentenceQuestion.size())
            reStudyStatus |= sentence;

        switch (reStudyStatus) {
            case allReStudy:
                findViewById(R.id.layout_restudy_status3).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.audio_restudy_title3)).setTextColor(getResources().getColor(R.color.color_black));
                break;

            case audioWordReStudy:
                findViewById(R.id.layout_restudy_status2_1).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.audio_restudy_title2_1)).setTextColor(getResources().getColor(R.color.color_black));
                break;

            case audioSentenceReStudy:
                findViewById(R.id.layout_restudy_status2_2).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.audio_restudy_title2_2)).setTextColor(getResources().getColor(R.color.color_black));
                break;

            case audioReStudy:
                findViewById(R.id.layout_restudy_status1_1).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.audio_restudy_title1_1)).setTextColor(getResources().getColor(R.color.color_black));
                break;
        }
    }

    /**
     * Player, Recorder를 설정하여 학습을 준비함
     */
    private void readyStudy() {
        if (mStudyData.mReviewISBodyVideoExist) {

        } else {
            setPlayerAndRecorder();
        }

        StudyDataUtil.setCurrentStudyStatus(this, "R01");
        StudyDataUtil.setCurrentStudyStatus(this, "R11");

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * 학습을 시작함
     */
    private void startStudy() {

        if (mStudyData.mReviewISBodyVideoExist) {
            startVideoPlay();
        } else {
            if (mVorbisPlayer.getState() == AudioTrack.STATE_UNINITIALIZED) {
                Log.e("", "startStudy Retry cuz AudioTrack Uninitialized !!");
                mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 100);
                return;
            }

            playerResume();
            if (!mIsStudyStart)
                mIsStudyStart = true;
        }
    }

    /**
     * Player, Recorder를 설정함
     */
    private void setPlayerAndRecorder() {
        mMp3Recorder = new CustomMp3Recorder(mHandler, ServiceCommon.RECORD_SAMPLE_RATE);

        mVorbisPlayer = new CustomVorbisPlayer(mHandler, mStudyData.mReviewBodySoundFile, mStudyData.mIsFastRewind);

        // 실제 seek 동작 리스너
        mSeekBack.setOnTouchListener(mVorbisPlayer.seekTouchListener);
        mSeekFwd.setOnTouchListener(mVorbisPlayer.seekTouchListener);

        mProgress.setProgress(0);
        mProgress.setMax(mVorbisPlayer.getDuration());

        //mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(0)).append(getString(R.string.string_common_margin_slash)).append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());
        mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition))));
        mEndTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());
    }

    /**
     * Player를 Resume하고 버튼의 리소스 및 활성화를 변경함
     */
    private void playerResume() {

        //mPlay.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_player_pause_d));

        if (CommonUtil.isCenter()) // Igse
            mPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_pause));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_pause));
            else // 숲
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_pause));
        }

        mPlay.setEnabled(true);
        mSeekBack.setEnabled(true);
        mSeekFwd.setEnabled(mStudyData.mIsFastRewind ? true : false);
        mWrite.setEnabled(true);
        mRecord.setEnabled(true);

        mVorbisPlayer.play();
    }

    /**
     * 화면상 버튼의 리소스 및 활성화를 변경함
     */
    private void playerPauseAndStop() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);

        if (CommonUtil.isCenter()) // Igse
            mPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_play));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_play));
            else // 숲
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_play));
        }

        mSeekBack.setEnabled(false);
        mSeekFwd.setEnabled(false);
        mWrite.setEnabled(false);
        mRecord.setEnabled(false);
    }

    /**
     * Player를 일시정지함
     */
    private void playerPause() {
        mVorbisPlayer.pause();
        playerPauseAndStop();
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        if (mStudyData.mReviewISBodyVideoExist) {

        } else {
            mVorbisPlayer.stop();
            playerPauseAndStop();
            mVorbisPlayer.release();
            mVorbisPlayer = null;
        }

    }

    /**
     * Player를 종료하고 다음 단계로 이동함
     */
    private void playerComplete() {
        playerEnd();
        goNextStatus();
    }

    /**
     * Progress 및 재생시간을 업데이트하여 표시함
     *
     * @param pos 현재 재생 시간
     */
    private void playerProgressUpdate(int pos) {
        mCurPosition = pos;

        if (mCurPosition > mVorbisPlayer.getDuration()) {
            Log.e("Log", "mVorbisPlayer mCurPosition ee => " + CommonUtil.convMsecToMinSecMsec(mCurPosition));
            return;
        }

        mProgress.setProgress(mCurPosition);
        //mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition)).append(getString(R.string.string_common_margin_slash)).append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());

        mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition))));
        mEndTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());
    }

    /**
     * 다음 단계로 이동 처리함
     */
    private void goNextStatus() {
        if (isNext) {
            isNext = false;
            if (!ServiceCommon.IS_CONTENTS_TEST) {
                int targetActivity = 0;
                if (0 < mStudyData.mReviewWordQuestion.size())
                    targetActivity = ServiceCommon.STATUS_REVIEW_WORD;
                else if (0 < mStudyData.mReviewSentenceQuestion.size())
                    targetActivity = ServiceCommon.STATUS_REVIEW_SENTENCE;
                else
                    targetActivity = ServiceCommon.STATUS_REVIEW_END;

                StudyDataUtil.setCurrentStudyStatus(this, "R12");

                // 본문 듣기에서 복습이 끝나는 경우
                if (targetActivity == ServiceCommon.STATUS_REVIEW_END) {

                    if (mStudyData.mIsAudioExist) {
                        StudyDataUtil.setCurrentStudyStatus(this, "RFN");
                        startActivity(new Intent(this, SmartStudyTitlePaperActivity.class));
                    } else {
                        StudyDataUtil.setCurrentStudyStatus(this, "S01");
                        if (0 < mStudyData.mWordQuestion.size() || 0 < mStudyData.mSentenceQuestion.size() || (mStudyData.mIsParagraph && 0 < mStudyData.mVanishingQuestion.size())) { // 단어시험이나 문장 시험이 있으면 시험준비
                            startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
                        } else if (mStudyData.mIsDictation && 0 < mStudyData.mDictation.size()) { // 받아쓰기가 있으면받아쓰기로
                            startActivity(new Intent(this, DictationTitlePaperActivity.class));
                        } else if ((mStudyData.isNewDication && mStudyData.mIsDicPirvate)) {
                            startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                        } else if (mStudyData.mIsMovieExist) {
                            startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                        } else if (mStudyData.mOneWeekData.size() > 0) {
                            startActivity(new Intent(this, OneWeekTestActivity.class));
                        } else {
                            startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
                        }
                    }

                } else
                    startActivity(new Intent(this, ReStudyTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, targetActivity));
            }

            finish();
        }
    }

    /**
     * 학습 가이드를 표시함
     *
     * @param tip         학습 가이드 내용
     * @param buttonIndex 3개의 버튼 위치 중 현재 위치 인덱스값
     */
    private void showToolTip(String tip, int buttonIndex) {
        if (!mStudyData.mIsStudyGuide)
            return;

        int textWidth = getResources().getDimensionPixelSize(R.dimen.dimen_395);

        if (0 < mTooltipWidth)
            textWidth = mTooltipWidth;

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);

        int x = 0, y = 0;
        LinearLayout tbpl = ((LinearLayout) findViewById(R.id.audio_study_three_btn_parent_layout));
        //TextView margin2 = ((TextView) findViewById(R.id.audio_study_three_btn_margin2));
        //TextView margin3 = ((TextView) findViewById(R.id.audio_study_three_btn_margin3));
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));

        tipText.setText(tip);
        mParentLayout.addView(tooltip);

        int centerWidth = tbpl.getWidth() / 2;

		/*if (buttonIndex == 0)
			x = (centerWidth - (mWrite.getWidth() / 2) - (mPlay.getWidth() / 2) - margin2.getWidth()) - (textWidth / 2);
		else if (buttonIndex == 1)
			x = centerWidth - (textWidth / 2);
		else
			x = (centerWidth + (mWrite.getWidth() / 2) + (mRecord.getWidth() / 2) + margin3.getWidth()) - (textWidth / 2);*/

        y = mParentLayout.getHeight() - tbpl.getHeight() - (mTooltipHeight / 2);

        ViewGroup.MarginLayoutParams vm = (MarginLayoutParams) tooltip.getLayoutParams();
        vm.leftMargin = x;
        vm.topMargin = y;
    }

    /**
     * 학습 가이드를 지움
     */
    private void hideToolTip() {
        if (null != findViewById(R.id.tooltip_record_text))
            mParentLayout.removeView(findViewById(R.id.tooltip_record_text));
    }

    // --- Recording && Write Animation effect ----

    /**
     * 녹음 진행 효과 시작 Runnable
     */
    private Runnable recordEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 쓰기 진행 효과 시작 Runnable
     */
    private Runnable writeEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mWrite).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과 종료 Runnable
     */
    private Runnable recordEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 쓰기 진행 효과 종료 Runnable
     */
    private Runnable writeEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mWrite).getDrawable();
                aniDraw.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 쓰기,녹음 진행 효과를 시작함
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void startIngEffect(boolean isRecord) {
        Runnable run = null;
        if (isRecord) {
            run = recordEffectStartRunnable;
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_record_volume));
        } else {
            run = writeEffectStartRunnable;
            mWrite.setImageDrawable(getResources().getDrawable(R.drawable.animation_list_write_ing));
        }

        mIngEffectThread = new Thread(run);
        mIngEffectThread.start();
    }

    /**
     * 쓰기,녹음 진행 효과를 종료함
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void stopIngEffect(boolean isRecord) {
        if (null != mIngEffectThread) {
            mIngEffectThread.interrupt();
            mIngEffectThread = null;
        }

        mIngEffectThread = new Thread(isRecord ? recordEffectStopRunnable : writeEffectStopRunnable);
        mIngEffectThread.start();
    }

    /**
     * 쓰기,녹음 완료 효과를 1초간 진행함
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void completeEffect(final boolean isRecord) {
        Runnable run = null;
        if (isRecord) {
            run = recordEffectStartRunnable;

            if (CommonUtil.isCenter()) // igse
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_seq_learning_progress));
            else {
                if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_seq_learning_progress));
                else // 숲
                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_seq_learning_progress));
            }
        } else {
            run = writeEffectStartRunnable;
            mWrite.setImageDrawable(getResources().getDrawable(R.drawable.animation_list_write_complete));
        }

        mCompleteEffectThread = new Thread(run);
        mCompleteEffectThread.start();

        Runnable runnable = null;
        if (isRecord) {
            runnable = new Runnable() {
                public void run() {
                    if (null == mContext)
                        return;

                    if (null != mCompleteEffectThread) {
                        mCompleteEffectThread.interrupt();
                        mCompleteEffectThread = null;
                    }

                    try {
                        AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                        aniDraw.stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        recordPlay();
                    }
                }
            };
            mRecord.postDelayed(runnable, 1000);
        } else {
            runnable = new Runnable() {
                public void run() {
                    if (null == mContext)
                        return;

                    if (null != mCompleteEffectThread) {
                        mCompleteEffectThread.interrupt();
                        mCompleteEffectThread = null;
                    }

                    try {
                        AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mWrite).getDrawable();
                        aniDraw.stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        writeComplete();
                    }
                }
            };
            mWrite.postDelayed(runnable, 1000);
        }
    }

    // ------------------- Recording----------------------

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordNotiCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            recordNotiEnd();
            recordStart();
        }
    };

    /**
     * 녹음 시작 알림음을 재생함
     */
    private void recordNotiPlay() {
        if (null != mRecordNotiPlayer)
            return;

        mRecordNotiPlayer = MediaPlayer.create(getBaseContext(), R.raw.ding);
        if (null != mRecordNotiPlayer) {
            mRecordNotiPlayer.setOnCompletionListener(mRecordNotiCompletion);
            mRecordNotiPlayer.start();
        }
    }

    /**
     * 녹음 시작 알림음을 종료함
     */
    private void recordNotiEnd() {
        if (null == mRecordNotiPlayer)
            return;

        if (mRecordNotiPlayer.isPlaying())
            mRecordNotiPlayer.stop();
        mRecordNotiPlayer.release();
        mRecordNotiPlayer = null;
    }

    /**
     * 버튼 비활성화 및 Player를 일시정지하고 녹음 시작 알림음을 재생시켜 녹음을 준비함
     */
    private void recordReady() {
        if (mMp3Recorder.isRecording())
            return;

        if (mVorbisPlayer.isPlaying())
            playerPause();

        mPlay.setEnabled(false);
        mWrite.setEnabled(false);
        mRecord.setEnabled(false);

        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_READY, 0), 100);
    }

    /**
     * 녹음을 시작하고 1초 뒤 녹음 완료를 진행 할 수 있도록 녹음 버튼을 활성화 함
     */
    private void recordStart() {
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_start));
        mMp3Recorder.setFilePath(mRecordFilePath);
        mMp3Recorder.start();

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect(true);
                showToolTip(getResources().getString(R.string.string_record_tool_tip), 2);
                mRecord.setEnabled(true);
            }
        };
        mRecord.postDelayed(runnable, 1000);
    }

    /**
     * 녹음을 종료함
     */
    private void recordEnd() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
        if (mMp3Recorder.isRecording()) {
            mMp3Recorder.stop();
            mRecord.setEnabled(false);
            stopIngEffect(true);
            hideToolTip();
            completeEffect(true);
        }
    }

    /**
     * 에코를 재생함
     */
    private void recordPlay() {
        if (null != mRecordPlayer && mRecordPlayer.isPlaying())
            return;

        File recordPlayFile = new File(mRecordFilePath);
        if (!recordPlayFile.exists()) {
            recordPlayEnd();
            return;
        }

        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo2_red_btn_bg));

        try {
            mRecordPlayer = new MediaPlayer();
            mRecordPlayer.reset();
            mRecordPlayer.setDataSource(mRecordFilePath);
            mRecordPlayer.setOnCompletionListener(this);
            mRecordPlayer.setOnPreparedListener(this);
            mRecordPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 에코 재생을 종료하고 에코 볼륨을 원복함
     */
    private void recordPlayEnd() {
        // 녹음 재생 후 종료
        if (null != mRecordPlayer) {
            if (mRecordPlayer.isPlaying())
                mRecordPlayer.stop();
            mRecordPlayer.release();
            mRecordPlayer = null;
            releaseEchoVolume();
        }

        File fileRecord = new File(mRecordFilePath);
        if (fileRecord.exists())
            fileRecord.delete();

        mWrite.setEnabled(true);
        mRecord.setEnabled(true);
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));

        playerResume();
    }

    /**
     * 에코 플레이어의 준비 완료시 에코 볼륨 설정 및 재생을 시작함
     */
    public void onPrepared(MediaPlayer mp) {
        if (null != mRecordPlayer) {
            setEchoVolume();
            mRecordPlayer.start();
        }
    }

    /**
     * 에코 재생 완료시 에코 재생을 종료함
     */
    public void onCompletion(MediaPlayer mp) {
        recordPlayEnd();
    }

    // ------------------- Writing----------------------

    /**
     * Player를 일시정지하고 쓰기를 시작함
     */
    private void writeStart() {
        if (mIsWriting)
            return;

        if (mVorbisPlayer.isPlaying())
            playerPause();

        mWrite.setEnabled(false);
        mPlay.setEnabled(false);
        mRecord.setEnabled(false);

        mWrite.setImageDrawable(getResources().getDrawable(R.drawable.btn_write_start));

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect(false);
                showToolTip(getResources().getString(R.string.string_write_tool_tip), 1);
                mIsWriting = true;
                mWrite.setEnabled(true);
            }
        };
        mWrite.postDelayed(runnable, 1000);
    }

    /**
     * 쓰기를 종료하고 완료 효과를 표시함
     */
    private void writeEnd() {
        if (mIsWriting) {
            mWrite.setEnabled(false);
            stopIngEffect(false);
            hideToolTip();
            completeEffect(false);
        }
    }

    /**
     * 쓰기를 완료하고 Player를 Resume함
     */
    private void writeComplete() {
        if (mIsWriting) {
            mWrite.setImageDrawable(getResources().getDrawable(R.drawable.selector_write_btn_bg));
            mIsWriting = false;
            mWrite.setEnabled(true);
            mRecord.setEnabled(true);
            playerResume();
        }
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            if (null == mVorbisPlayer && !mStudyData.mReviewISBodyVideoExist)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START)
                        startStudy();
                    break;

                case ServiceCommon.MSG_WHAT_PLAYER:
                    if (msg.arg1 == ServiceCommon.MSG_PROGRESS_UPDATE) {
                        playerProgressUpdate(msg.arg2);
                    } else if (msg.arg1 == ServiceCommon.MSG_PROGRESS_COMPLETION) {
                        playerComplete();
                    } else if (msg.arg1 == ServiceCommon.MSG_SEEK_FWD_ABLE) {
                        if (mVorbisPlayer.isPlaying() && !mSeekFwd.isEnabled())
                            mSeekFwd.setEnabled(true);
                    } else if (msg.arg1 == ServiceCommon.MSG_SEEK_FWD_DISABLE) {
                        if (mVorbisPlayer.isPlaying() && mSeekFwd.isEnabled())
                            mSeekFwd.setEnabled(false);
                    }
                    break;

                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (msg.arg1 == ServiceCommon.MSG_REC_READY)
                        recordNotiPlay();
                    else if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END)
                        recordEnd();
                    break;
                case ServiceCommon.MSG_WHAT_VIDEO_PLAYER:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_CONFIRM) {
                        showVideoPlayConfirmDialog();
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_COMPLETION) {
                        endVideoPlay();
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void setVideoPlayer(String path) {
        if (0 < ((LinearLayout) findViewById(R.id.animation_study_layout)).getChildCount())
            ((LinearLayout) findViewById(R.id.animation_study_layout)).removeAllViewsInLayout();

        mVideoPlayer = new CustomVideoPlayer(this, mHandler, path, true, mStudyData.mIsFastRewind);
        ((LinearLayout) findViewById(R.id.animation_study_layout)).addView(mVideoPlayer);
    }

    private void startVideoPlay() {
        mIsStudyStart = true;
        setVideoPlayer(mStudyData.mReviewBodyVideoFile);

    }

    private void endVideoPlay() {
        playerComplete();
    }

    private void showVideoPlayConfirmDialog() {
        if (null == mContext)
            return;

        mMsgBox = new MessageBox(this, 0, R.string.string_msg_ani_talk_confirm);
        mMsgBox.setConfirmText(R.string.string_common_retry);
        mMsgBox.setCancelText(R.string.string_common_ok);
        mMsgBox.setOnDialogDismissListener(mVideoPlayer.mDialogDismissListener);
        mMsgBox.show();
    }
}
