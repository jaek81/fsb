package com.yoons.fsb.student.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.service.AgentEvent;
import com.yoons.fsb.student.service.DownloadServiceAgent;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.control.BothSideRoundProgressBar;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import java.io.File;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * 교재 다운로드 화면
 *
 * @author ejlee
 */
public class BookDownloadActivity extends BaseActivity implements OnDialogDismissListener {
    public static final String TAG = "[BookDownloadActivity]";
    private TextView mDownloadProgressText = null;
    private boolean mIsStartSudyBookSelect = false; // 교재 선택했는지 여부
    // private DownloadServiceAgent mServiceAgent = null; // 다운로드 서비스 agent
    private BothSideRoundProgressBar mProgressBar = null; // 프로그래스 바
    private TextView mInfoText = null;
    private boolean mIsWifiSetting = false;

    private static final int REQUESTCODE_UPLOAD = 128; // upload requestcode

    private int MSGBOX_ID_SYNC_UPDOWN_NETWORK_ERROR = 0; // msgbox id

    // 서비스 connection
    /*
     * private ServiceConnection serviceConnection = new ServiceConnection() {
	 * 
	 * @Override public void onServiceConnected(ComponentName name, IBinder
	 * service) { DownloadService.LocalBinder binder =
	 * (DownloadService.LocalBinder)service; mServiceAgent =
	 * binder.getServiceHandler(); Log.i(TAG, "onServiceConnected"); }
	 * 
	 * @Override public void onServiceDisconnected(ComponentName name) {
	 * mServiceAgent = null; } };
	 */

    /**
     * onCreate 함수
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = this;

        if (CommonUtil.isCenter()) {
            // Igse
            setContentView(R.layout.igse_book_download);
        } else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) {
                //우영
                setContentView(R.layout.w_book_download);
            } else {
                // 숲
                setContentView(R.layout.f_book_download);
            }
        }

        setCancelable(false);

        if (ServiceCommon.VOC_MODE) { //voc 대응용으로 넣어둠
            Log.startLog(mContext);
        }
        Crashlytics.setUserIdentifier(String.valueOf(Preferences.getCustomerNo(mContext)));//크래시 리포트 사용자 ID 추가


        setTitlebarText(getString(R.string.string_common_book_downloading));

        mDownloadProgressText = (TextView) findViewById(R.id.download_progress_textview);
        mProgressBar = (BothSideRoundProgressBar) findViewById(R.id.progress);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(100);

        mInfoText = (TextView) findViewById(R.id.info_textview);

		/*
         * registerReceiver(mDownloadReceiver, new
		 * IntentFilter(AgentEvent.ACTION_RECEIVE_AGENT_EVENT)); // 다운로드 서비스 이벤트
		 * 등록
		 */
        super.onCreate(savedInstanceState);

        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        if (CommonUtil.isAvailableNetwork(this, true)) {
            mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_FILE_UPLOAD); // 파일 업로드 진행

        } else if (CommonUtil.isAvailableNetwork(this, false)) { // 네트워크 환경 확인후, 다음 진행이 불가능 할때.교재 선택화면으로 이동
            mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_OLD_FILE_DELETE_START);

        } else {
            Preferences.setSyncSuccess(mContext, false);
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
        Crashlytics.log(getString(R.string.string_ga_BookDownloadActivity));
    }

    protected void onResume() {
        super.onResume();

        if (mIsWifiSetting) {
            mIsWifiSetting = false;
            if (CommonUtil.isAvailableNetwork(this, true)) {
                mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_FILE_UPLOAD); // 파일
                // 업로드
                // 진행
            } else if (CommonUtil.isAvailableNetwork(this, false)) { // 네트워크 환경  확인 후 다음 진행이  불가능 할때. 교재 선택 화면으로 이동
                mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_OLD_FILE_DELETE_START);

            } else {
                Preferences.setSyncSuccess(mContext, false);
                ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
                showSyncUpDownNetworkErrorDialog();
            }
        }
    }

    ;

    /**
     * Upload Activity Result를 받는 함수
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUESTCODE_UPLOAD == requestCode) {

            switch (resultCode) {
                case HttpURLConnection.HTTP_OK:
                case UploadActivity.ERR_EMPTY_LIST:
                    // success
            }

            Log.i("", "resultCode:" + resultCode);

            if (RESULT_CANCELED == resultCode) {
                finish();
                return;
            }

            mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_OLD_FILE_DELETE_START); // 오래된
            // 음성
            // 파일
            // 삭제
            // 실행
        }
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Intent intent = null;

            switch (msg.what) {
                case ServiceCommon.MSG_RECORDING_FILE_UPLOAD: // 음성 파일 업로드 시작
                    startActivityForResult(new Intent(mContext, UploadActivity.class), REQUESTCODE_UPLOAD);
                    break;

                case ServiceCommon.MSG_RECORDING_OLD_FILE_DELETE_START: // 오래된 음성 파일
                    // 삭제 시작
                    updateProgress(30);
                    deleteOldRecordFile();
                    break;

                case ServiceCommon.MSG_RECORDING_OLD_FILE_DELETE_END: // 오래된 음성파일 삭제
                    // 완료
                    updateProgress(40);
                    if (CommonUtil.isCenter() && DatabaseUtil.getInstance(mContext).isFailStudyLogCnt()) {
                        mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_BACKUP_UPLOAD_SYNC_START);
                    } else {
                        mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
                    }
                    break;

                case ServiceCommon.MSG_DATABASE_BACKUP_UPLOAD_SYNC_START:
                    Log.e("ss99km01", "ss99km01 BookDownloadActivity MSG_DATABASE_BACKUP_UPLOAD_SYNC_START");
                    HttpJSONRequest request = new HttpJSONRequest(mContext);
                    request.requestBackupStudyStatus(mBackupHandler);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START: // upload sync
                    // 시작(학습 결과에 대한
                    // 업로드)
                    Preferences.setStudying(mContext, true);
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS: // upload sync
                    // 성공
                    updateProgress(50);
                    //mInfoText.setText(R.string.string_book_download_progress1);
                    syncList();
                    // syncDownloadDatabaseTable(); // 업로드 싱크가 끝나면, 다운로드 싱크를 함.
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL: // upload sync 실패
                    // 오프라인으로 판단하여 시작함.
                    showSyncUpDownNetworkErrorDialog();
                    Preferences.setSyncSuccess(mContext, false);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_DEVICE_ERROR_FAIL: // upload sync시 등록된 device가아닐때오류
                    int customerNo = msg.arg1;
                    DatabaseUtil.getInstance(mContext).deleteCustomer(customerNo);
                    startActivity(new Intent(BookDownloadActivity.this, LoginActivity.class));
                    Toast.makeText(mContext, "단말에 해지된 사용자가 있어 로그인 화면으로 이동합니다", Toast.LENGTH_SHORT).show();
                    finish();
                    break;

                case ServiceCommon.MSG_DATABASE_NEW_DOWNLOAD_SYNC_SUCCESS: // download
                    // sync 성공
                    updateProgress(90);
                    Preferences.setStudying(mContext, false);

                    if (!CommonUtil.isQ7Device()) {
                        intent = new Intent(ServiceCommon.ACTION_SET_ALARM); // 학습
                        // 계획이
                        // 바뀌었을수도
                        // 있으므로
                        // 보내줌.
                        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        sendBroadcast(intent);
                    }

                    // 온라인으로 판단하여 시작함.
                    deleteOldContentsFile();

                    Preferences.setSyncSuccess(mContext, true);
                    break;

                case ServiceCommon.MSG_OLD_CONTENTS_FILE_DELETE_COMPLETE: // 오래된 콘텐츠
                    // 음원 파일
                    // 삭제 완료
                    updateProgress(100);
                    makeDownloadContentList();
                    startBookSelectActivity();
                    break;

                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL: // download sync
                    // 실패
                    if (!CommonUtil.isQ7Device()) {
                        intent = new Intent(ServiceCommon.ACTION_SET_ALARM);
                        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        sendBroadcast(intent);
                    }

                    // 오프라인으로 판단하여 시작함.
                    showSyncUpDownNetworkErrorDialog();
                    Preferences.setSyncSuccess(mContext, false);
                    break;

                case ServiceCommon.MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE: // 다운로드 파일 목록 생성 완료
                /* DownloadServiceAgent.RequestUpdate(mContext); */
                    //mInfoText.setText(R.string.string_book_download_progress2);
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private Handler mBackupHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            HttpJSONRequest request = new HttpJSONRequest(mContext);
            HttpJSONRequest.mIsStatusRequest = false;

            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:

                    if (HttpJSONRequest.mStatusQueue.size() != 0) {
                        Log.e("ss99km01", "ss99km01 BookDownloadActivity  mBackupHandler requestBackupStatus");
                        request.requestBackupStatus(mBackupHandler);
                    } else {
                        mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
                        Log.e("ss99km01", "ss99km01 BookDownloadActivity  MSG_DATABASE_UPLOAD_SYNC_START");
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    HttpJSONRequest.mIsStatusRequest = false;
                    request.backupStatus();
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
                    Log.e("ss99km01", "ss99km01 BookDownloadActivity MSG_HTTP_REQUEST_FAIL MSG_DATABASE_UPLOAD_SYNC_START");
                    break;
            }
            super.handleMessage(msg);
        }
    };


    /**
     * db upload sync 실행
     */
    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mHandler);
        } else {
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * db download sync 실행
     */
    public void syncDownloadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.downloadList(mHandler, Preferences.getCustomerNo(this));
        } else {
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * 교재 list sync 실행
     */
    public void syncList() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.downloadList(mHandler, Preferences.getCustomerNo(this));
        } else {
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * 에러 메시지 박스 응답
     */
    @Override
    public void onDialogDismiss(int result, int dialogId) {
        if (dialogId == MSGBOX_ID_SYNC_UPDOWN_NETWORK_ERROR) {
            if (result == BaseDialog.DIALOG_CONFIRM || CommonUtil.isSMT330Device()) {
                startBookSelectActivity();
            } else {
                mExitInBackground = false;
                mIsWifiSetting = true;
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                intent.putExtra("extra_prefs_show_button_bar", true);
                intent.putExtra("extra_prefs_set_next_text", "");
                intent.putExtra("extra_prefs_set_back_text", getString(R.string.string_common_close)); // 닫기
                startActivity(intent);
            }
        }

    }

    /**
     * 교재 다운로드 시 오류가 발생시 띄우는 에러 다이얼로그
     */
    private void showSyncUpDownNetworkErrorDialog() {
        if (ErrorData.getErrorCode() == ErrorData.ERROR_HTTP_FILE_NOT_FOUND) {
            mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_file_not_found));
        } else {
            if (CommonUtil.isSMT330Device()) {
                mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_newwork_error3));
            } else {
                // if (isStorageFreeSpace() < 0) {
                // mMsgBox = new MessageBox(this, 0,
                // getString(R.string.string_msg_not_exist_storage_or_using_usb_disk));
                // } else {
                if (ErrorData.getErrorMsg().contains("UNMOUNTED_STORAGE") || ErrorData.getErrorMsg().contains("NOT ENOUGH SPACE")) {
                    mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_not_exist_storage_or_using_usb_disk));
                } else {
                    mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_newwork_error2) + "\n" + ErrorData.getErrorMsg());
                    mMsgBox.setCancelText(R.string.string_network_setting);
                }
            }
        }

        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_SYNC_UPDOWN_NETWORK_ERROR);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 다운로드 받을 컨텐츠 파일 리스트를 만드는 task
     *
     * @author ejlee
     */

    private void makeDownloadContentList() {
        Thread thread = new Thread("makeDownloadContentList") {
            public void run() {
                if (mContext == null) {
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE);
                    return;
                }

				/*
                 * DatabaseUtil databaseUtil =
				 * DatabaseUtil.getInstance(mContext);
				 * ArrayList<DownloadStudyUnit> unitList =
				 * databaseUtil.selectTodayDownloadStudyUnit(Preferences.
				 * getCustomerNo(mContext));
				 * 
				 * for (int i = 0; i < unitList.size(); i++) { DownloadStudyUnit
				 * unit = unitList.get(i); Map<String, String> list =
				 * ContentsUtil.getDownloadContentList(unit);
				 * 
				 * Iterator it = list.entrySet().iterator(); while
				 * (it.hasNext()) { Map.Entry pairs = (Map.Entry) it.next();
				 * databaseUtil.insertDownloadContent(unit.mSeriesNo * 1000 +
				 * unit.mBookNo * 100 + unit.mStudyUnitNo, (String)
				 * pairs.getKey(), (String) pairs.getValue(), unit.mIsPriority);
				 * } }
				 */

                mHandler.sendEmptyMessage(ServiceCommon.MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE);
            }
        };

        thread.start();
    }

    /**
     * 다운로드 서비스 event receiver
     */
    BroadcastReceiver mDownloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AgentEvent.ACTION_RECEIVE_AGENT_EVENT.equals(intent.getAction())) {
                int type = intent.getIntExtra(AgentEvent.EVENT_TYPE, 0);
                switch (type) {
                    case AgentEvent.DOWNLOAD_START:
                        Log.e(TAG, "DOWNLOAD_START");
                        break;

                    case AgentEvent.DOWNLOAD_STOP:
                        Log.e(TAG, "DOWNLOAD_STOP");
                        break;

                    case AgentEvent.DOWNLOAD_FAIL:
                        Log.e(TAG, "DOWNLOAD_FAIL");
                        // 네트워크가 설정에 다운을 받을수 없음.
                        int error = intent.getIntExtra(AgentEvent.FAIL_REASON, AgentEvent.REASON_NO_ERROR);

                        switch (error) {
                            case AgentEvent.REASON_NO_ERROR:
                                break;

                            case AgentEvent.REASON_UNMOUNTED_STORAGE:
                                ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "UNMOUNTED_STORAGE");
                                break;

                            case AgentEvent.REASON_NOT_ENOUGH_SPACE:
                                ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT ENOUGH SPACE");
                                break;

                            case AgentEvent.REASON_NET_ERROR:
                                ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_NOT_FOUND, "resInfo");
                                break;

                            case AgentEvent.REASON_UNKNOWN_ERROR:
                                break;

                            case AgentEvent.REASON_NOT_AVAILABLE_NETWORKS:
                                ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT_AVAILABLE_NETWORKS");
                                break;
                        }

                        showSyncUpDownNetworkErrorDialog();
                        break;

                    case AgentEvent.START_LEARNING:
                        Log.e(TAG, "START_LEARNING");
                        break;

                    case AgentEvent.DOWNLOAD_COMPLETE:
                        Log.e(TAG, "DOWNLOAD_COMPLETE");
                        break;

                    case AgentEvent.CONTENTS_UPDATE:
                        Log.e(TAG, "CONTENTS_UPDATE");
                        DownloadServiceAgent.RequestStart(BookDownloadActivity.this);
                        break;

                    case AgentEvent.DOWNLOAD_STATE: {
                        Log.e(TAG, "DOWNLOAD_STATE " + String.format("Tot(%d/%d), Pri(%d/%d)", intent.getIntExtra(AgentEvent.TOT_POS, 0), intent.getIntExtra(AgentEvent.TOT_CNT, 0), intent.getIntExtra(AgentEvent.PRI_POS, 0), intent.getIntExtra(AgentEvent.PRI_CNT, 0)));

                        int priorityTotal = intent.getIntExtra(AgentEvent.PRI_CNT, 0);
                        int priorityPos = intent.getIntExtra(AgentEvent.PRI_POS, 0);
                        // String progressText = String.format("%d/%d", priorityPos,
                        // priorityTotal);
                        if (priorityTotal != 0) {
                            // updateProgress(progressText);
                            updateProgress(priorityPos, priorityTotal);
                        }

                        if (priorityPos == 0 && priorityTotal == 0) {
                            startBookSelectActivity();
                        }
                    }
                    break;
                }
            }
        }
    };

    /**
     * 교재 선택화면으로 이동
     */
    private void startBookSelectActivity() {
        if (mIsStartSudyBookSelect) {
            return;
        } else {
            mIsStartSudyBookSelect = true;
        }

        if (CommonUtil.isHSSBForeground(this)) {
            try {
                mContext.startActivity(new Intent(this, NewHomeActivity.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        finish();
    }

    /**
     * Progress bar 업데이트
     *
     * @param percent : 퍼센트
     */
    private void updateProgress(int percent) {
        mProgressBar.setProgress(percent);
        mDownloadProgressText.setText(String.valueOf(percent) + "% 다운로드 중입니다.");
    }

    /**
     * Progress bar 업데이트
     *
     * @param pos   : 현재 진행중인 position
     * @param total : 총 개수
     */
    private void updateProgress(int pos, int total) {
        mProgressBar.setProgress(pos * 100 / total);
        mDownloadProgressText.setText(String.valueOf(pos * 100 / total) + "%");
    }

    @Override
    protected void onDestroy() {
        Preferences.setStudying(this, false);

        super.onDestroy();

		/*
         * try { unregisterReceiver(mDownloadReceiver); } catch (Exception e) {
		 * }
		 * 
		 * mServiceAgent = null;
		 */
    }

    /**
     * 바로파일삭제
     *
     * @author ejlee
     */
    private void deleteOldRecordFile() {
        Thread thread = new Thread("oldRecordFileDelete") {
            public void run() {
                deleteOldRecordFile(ServiceCommon.AUDIO_RECORD_PATH);
                deleteOldRecordFile(ServiceCommon.EXAM_RECORD_PATH);
                deleteOldRecordFile(ServiceCommon.DB_BACK_UP_PATH);
                deleteOldRecordFile(ServiceCommon.EXAM_RECORD_UPLOAD_PATH);

                mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_OLD_FILE_DELETE_END);
            }

            private void deleteOldRecordFile(String path) {
                File dir = new File(path);
                if (!dir.exists())
                    return;

                File[] arrFile = dir.listFiles();
                if (arrFile == null)
                    return;

                for (File file : arrFile) {
                    file.delete();
					/*
					 * if (file.isDirectory()) { if
					 * (isOldAudioDir(file.getName())) {
					 * CommonUtil.deleteDirectory(file); } }
					 */
                }
            }

            private boolean isOldAudioDir(String dirTime) {
                String currentTime = CommonUtil.getCurrentYYYYMMDD();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                Calendar currentCal = Calendar.getInstance();
                Calendar dirCal = Calendar.getInstance();

                try {
                    currentCal.setTime(sdf.parse(currentTime));
                    dirCal.setTime(sdf.parse(dirTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                    return false;
                }

                // 디렉토리 날짜에 30일을 더한 값이, 오늘 날짜보다 작다면 30일 이전의 날짜에 저장된 디렉토리임.
                dirCal.add(Calendar.DAY_OF_MONTH, 30);

                if (currentCal.compareTo(dirCal) > 0) {
                    return true;
                }

                return false;
            }
        };

        thread.start();
    }

    /**
     * (배본정책 변경으로 90일 -> 180일로 변경)180일 이전의 콘텐츠 파일을 삭제하는 Thread
     *
     * @author ejlee
     */
    private void deleteOldContentsFile() {
        Thread thread = new Thread("oldContentsFileDelete") {
            private ArrayList<String> mValidUnitPathList = new ArrayList<String>();
            private ArrayList<String> mFinished30DaysUnitPathList = new ArrayList<String>();

            public void run() {
                if (mContext != null) {
                    DatabaseUtil mDatabaseUtil = DatabaseUtil.getInstance(mContext);
                    mValidUnitPathList = mDatabaseUtil.selectValidUnitPath();
                    // 학습이 완료된 30일 이전의 차시를 구해온다.(동기화시 30일 이전의 콘텐츠를 삭제하기 위함.)
                    mFinished30DaysUnitPathList = mDatabaseUtil.selectStudyFinished30DaysUnitPath();

					/*
					 * for(int i = 0; i < mValidUnitPathList.size(); i++) {
					 * Log.e(TAG, "validPath : " + mValidUnitPathList.get(i)); }
					 */

                    deleteOldContentPath(ServiceCommon.STUDY_CONTENT_PATH);
                }

                mHandler.sendEmptyMessage(ServiceCommon.MSG_OLD_CONTENTS_FILE_DELETE_COMPLETE);
            }

            public void deleteOldContentPath(String path) {
                File dir = new File(path);
                if (!dir.exists())
                    return;

                File[] arrFile = dir.listFiles();
                if (arrFile == null)
                    return;

                for (File file : arrFile) {
                    if (file.isDirectory()) {
                        String unitPath = file.getPath().replace(ServiceCommon.STUDY_CONTENT_PATH, "");
                        String split[] = unitPath.split("[/]");
                        if (split.length == 3) {
                            boolean isValid = false;
                            boolean isFinished = false;
                            for (int i = 0; i < mValidUnitPathList.size(); i++) {
                                if (file.getPath().equals(mValidUnitPathList.get(i))) {
                                    isValid = true;
                                }
                            }
                            for (int i = 0; i < mFinished30DaysUnitPathList.size(); i++) {
                                if (file.getPath().equals(mFinished30DaysUnitPathList.get(i))) {
                                    isFinished = true;
                                }
                            }

                            if (!isValid || isFinished) {
                                deleteDirectory(file);
                            }
                        } else {
                            deleteOldContentPath(file.getPath());
                        }
                    }
                }
            }

            private void deleteDirectory(File path) {
                // Log.e(TAG, "deleteDirectory : " + path.getPath());

                String unitPath = path.getPath();
                String bookPath = unitPath.substring(0, unitPath.lastIndexOf("/"));
                String seriesPath = bookPath.substring(0, bookPath.lastIndexOf("/"));

                CommonUtil.deleteDirectory(path);
                deleteDirectory(bookPath);
                deleteDirectory(seriesPath);
            }

            // 비어있지 않으면 삭제 안함.
            public boolean deleteDirectory(String path) {
                File path2 = new File(path);
                if (!path2.exists()) {
                    return false;
                }

                File[] files = path2.listFiles();
                if (files.length != 0) {
                    return false;
                }

                return path2.delete();
            }
        };

        thread.start();
    }

    /**
     * 내부저장소 남은 용량 확인
     *
     * @return 남은 용량 여부 -2: 저장공간 부족, -1: storage unmount, 0: 저장가능 (10MB 이상인경우)
     */
    // private int isStorageFreeSpace() {
    // long freeBytes =
    // StorageUtil.getStorageFreeSpace(CommonUtil.getInternalStoragePath());
    //
    // int MINIMUM_FREE_SPACE = 1024 * 1024 * 10; // 10 MegaByte
    //
    // if (0 >= freeBytes) {
    // // Removed storage
    // return (0 == freeBytes) ? AgentEvent.REASON_NOT_ENOUGH_SPACE :
    // AgentEvent.REASON_UNMOUNTED_STORAGE ;
    // } else if (MINIMUM_FREE_SPACE > freeBytes) {
    // // Not enough storage
    // return AgentEvent.REASON_NOT_ENOUGH_SPACE;
    // }
    //
    // return AgentEvent.REASON_NO_ERROR;
    // }
}
