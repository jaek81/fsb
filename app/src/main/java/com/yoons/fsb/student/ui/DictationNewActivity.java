package com.yoons.fsb.student.ui;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.SentenceQuestion;
import com.yoons.fsb.student.data.StudyData.WordQuestion;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.control.StepStatusBar;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.popup.ScrollBox;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * 받아쓰기 화면
 *
 * @author dckim
 */
public class DictationNewActivity extends BaseActivity implements OnClickListener, OnCompletionListener,
        OnSeekCompleteListener, OnPreparedListener, OnDialogDismissListener {

    public static final int MSG_START_PLAY = 128;
    //study state
    private static final int WP_STATE = 1;
    private static final int SP_STATE = 2;
    //view state
    private static final int PIC_STATE = 3;
    private static final int NUM_STATE = 4;

    private StepStatusBar mStepStatusBar = null;
    private ImageView mBtnPrev = null, mBtnNext = null, mBtnPlayPause = null;
    private TextView mTooltip = null;
    private int mCursor = 0;
    private int mTotCnt = 0, mItemW = 0, mCursorW = 0;
    private int mMarginLeft = 0, mMarginRight = 0;
    private int[][] mArrBG = {{R.drawable.red_01, R.drawable.red_02, R.drawable.red_03},
            {R.drawable.blue_01, R.drawable.blue_02, R.drawable.blue_03},
            {R.drawable.white_01, R.drawable.white_02, R.drawable.white_03}};

    private ArrayList<DictationInfo> mDictation = new ArrayList<DictationInfo>();
    private MediaPlayer mPlayer = null;
    private boolean mStarted = false;
    private int mCurState = WP_STATE;//처음 단어 스테이트
    private int mViewState = PIC_STATE;//커서 이미지, 숫자
    private StudyData mStudydata;
    //private LinearLayout layout_pic, layout_num;
    //private TextView txt_dic_ment;
    //private TextView txt_dic_num;
    private  TextView txt_dic_cnt;

    private TextView tBookName = null, tCategory = null, tCategory2 = null;

    private ImageView mListen = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_dictation_new);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                setContentView(R.layout.w_dictation_new);
            else // 숲
                setContentView(R.layout.f_dictation_new);
        }

        mStudydata = StudyData.getInstance();
        initview();

        // TitleView
        tBookName.setText(mStudydata.mProductName);
        tCategory.setText(R.string.string_common_dictation);
        tCategory2.setText(R.string.string_listen_and_dictate_word);

        // 타이틀 바의 속성 설정
        setTitlebarText(mStudydata.mProductName);
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        readyView();
        if (mTotCnt == 0) {//단어가 0이면
            mCurState = SP_STATE;
        }
        readyStudy();
        btn_Check();

        // requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);
        Crashlytics.log(getString(R.string.string_ga_DictationActivity));
    }

    @Override
    protected void onStart() {
        super.onStart();
        CommonUtil.conSlogService(this, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        CommonUtil.conSlogService(this, false);
    }

    /**
     * 받아쓰기 현재 상태를 기록/관리한다.
     *
     * @author dckim
     */
    public class DictationInfo {

        public String fileName = "";
        public boolean wasStudy = false;
        private int playCnt = 0;//음원재생횟수

        public DictationInfo() {
        }

        public DictationInfo(String fileName) {
            this.fileName = fileName;
        }

        public DictationInfo(String fileName, boolean wasStudy) {
            this.fileName = fileName;
            this.wasStudy = wasStudy;
        }
    }

    /**
     * 받아쓰기 음성파일 위치를 반환한다.
     *
     * @param cursor 순번
     * @return String 음성파일 패스
     */
    private String getFileName(int cursor) {

        if (null == mDictation || cursor >= mDictation.size())
            return "";

        DictationInfo info = mDictation.get(cursor);
        return info.fileName;
    }

    /**
     * 받아쓰기 수행 여부 확인
     *
     * @param cursor 순번
     * @return true이면 해당 순번의 받아쓰기를 수행한 것임
     */
    private boolean getWasStudy(int cursor) {

        if (null == mDictation || cursor >= mDictation.size())
            return false;

        DictationInfo info = mDictation.get(cursor);
        return info.wasStudy;
    }

    /**
     * 받아쓰기 수행 여부 기록
     *
     * @param cursor 순번
     */
    private void setWasStudy(int cursor) {

        if (null == mDictation || cursor >= mDictation.size())
            return;

        DictationInfo info = mDictation.get(cursor);
        info.wasStudy = true;
    }

    /**
     * 받아쓰기 내용을 한번씩은 모두 수행하였는가 여부
     *
     * @return true이면 모두 수행한 것임
     */
    private boolean isFinishedStudy() {

        if (null == mDictation)
            return false;

        for (DictationInfo info : mDictation) {
            if (!info.wasStudy)
                return false;
        }

        return true;
    }

    private void requestServerTimeSync(int type) {
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestServerTimeSync(mNetworkHandler, type);
    }

    /**
     * Slog
     */
    private void readyStudy() {
        StudyDataUtil.setCurrentStudyStatus(this, "S51");
        // mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
        // ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
        mStarted = true;
        mHandler.sendEmptyMessageDelayed(MSG_START_PLAY, 1000);
    }

    /**
     * 항목의 전체 갯수 및 현재 위치를 설정하여 화면 구성함.
     *
     * @param cursor   현재 위치
     * @param applyCnt 전체 갯수
     */
    public void setCursor(int cursor, int applyCnt) {
        txt_dic_cnt.setText((cursor + 1) + " / " + mTotCnt);
        LinearLayout icons = (LinearLayout) findViewById(R.id.step_icon_list);
        int iconsCnt = icons.getChildCount();
        for (int i = 0; (i < applyCnt && i < iconsCnt); i++) {
            ImageView v = (ImageView) icons.getChildAt(i);
            if (null == v)
                continue;

            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
            lp.leftMargin = (i == 0) ? 0 : -mMarginLeft;
            lp.rightMargin = (i == applyCnt - 1) ? 0 : -mMarginRight;
            v.setLayoutParams(lp);
            v.setVisibility((i == cursor) ? View.VISIBLE : View.INVISIBLE);
        }

        LinearLayout progress = (LinearLayout) findViewById(R.id.step_progress_list);
        int progsCnt = progress.getChildCount();
        for (int i = 0; (i < applyCnt && i < progsCnt); i++) {
            TextView v = (TextView) progress.getChildAt(i);
            if (null == v)
                continue;

            // (comp) ? red : ((comp) ? blue : white);
            int kind = (i == cursor) ? 0 : ((getWasStudy(i) ? 1 : 2));
            v.setBackgroundResource(
                    (0 == i) ? mArrBG[kind][0] : ((i == mTotCnt - 1) ? mArrBG[kind][2] : mArrBG[kind][1]));

            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
            lp.width = mItemW;
            v.setLayoutParams(lp);
            v.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                mBtnPlayPause.setPressed(true);
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                mBtnPrev.setPressed(true);
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                mBtnNext.setPressed(true);
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                mBtnPlayPause.setPressed(false);
                playPause();
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                mBtnPrev.setPressed(false);
                prev();
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                mBtnNext.setPressed(false);

                if (mCursor >= (mTotCnt - 1) && isFinishedStudy() && ((mDictation.get(mCursor).playCnt >= 3) || ServiceCommon.VOC_MODE)) {//마지막 커서 && 학습을 다함 && 현재 &&(3번다 들었을때 || vocmode일때)
                    showFinishConfirm();
                    return false;
                }

                next();
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == R.id.btn_play_pause) {
            playPause();
        } else if (id == R.id.btn_prev) {
            prev();
        } else if (id == R.id.btn_next) {
            if (mCursor >= (mTotCnt - 1) && isFinishedStudy() && mDictation.get(mCursor).playCnt >= 3) {
                showFinishConfirm();
                return;
            }
            next();
        } else {
        }
    }

    /**
     * 받아쓰기 항목을 모두 학습하고 다음 단계로 이동
     */
    private void onOk() {

        playerEnd();
        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_START_PLAY)
                startPlay();

            super.handleMessage(msg);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playerEnd();
    }

    /**
     * 현재보다 이전 음원을 재생한다.
     */
    private synchronized void prev() {

        if (!isFinishedStudy() && mCursor == 0) {
            return;
        }
        if (!mBtnPrev.isEnabled()) {//비활성화 상태
            return;
        }
        playerEnd();

        mCursor--;
        if (0 > mCursor)
            mCursor = mTotCnt - 1;
        mTooltip.setVisibility((mCursor == (mTotCnt - 1) && isFinishedStudy() && mDictation.get(mCursor).playCnt >= 3) ? View.VISIBLE : View.INVISIBLE);
        startPlay();
    }

    /**
     * 현재보다 다음 음원을 재생한다.
     */
    private synchronized void next() {

        if (!mBtnNext.isEnabled()) {//비활성화 상태
            return;
        }
        playerEnd();

        mCursor++;
        mStudydata.mRTagRepeatCnt = mCursor;
        if (mCursor >= mTotCnt)
            mCursor = 0;
        mTooltip.setVisibility((mCursor == (mTotCnt - 1) && isFinishedStudy() && mDictation.get(mCursor).playCnt >= 3) ? View.VISIBLE : View.INVISIBLE);
        startPlay();
    }

    /**
     * 음원을 재생한다.<br>
     * (GUI의 포커스 위치를 설정하며, OK버튼을 조건에 따라 활성화한다.)
     */
    private synchronized void startPlay() {

        if (null == mDictation || 0 >= mDictation.size())
            return;

        setWasStudy(mCursor);
        if (mViewState == NUM_STATE) {//view init
            //txt_dic_num.setText(mCursor + 1 + "/" + mTotCnt);
            txt_dic_cnt.setText(mCursor + 1 + "/" + mTotCnt);
        } else {
            setCursor(mCursor, mTotCnt);
        }
        Log.i("hy", "startPlay | " + (mCursor + 1) + "/" + mTotCnt);
        setPlayer(getFileName(mCursor));

    }

    @Override
    protected void onPause() {
        Log.i("hy", "onPause");

        playerEnd();

        if (!isFinishing()) {
            if (mIsReStart) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class).putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
                finish();
            }
        }

        super.onPause();
    }

    /**
     * 오디오 준비 onPrepared -> onCompletion
     */
    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.i("hy", "onPrepared");
        if (null != mPlayer) {
            if (!mPlayer.isPlaying()) {
                mHandler.removeCallbacksAndMessages(null);
                mPlayer.start();
                if (CommonUtil.isCenter()) // igse
                    mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_pause_n));
                else {
                    if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                        mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_pause_n));
                    else // 숲
                        mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_pause_n));
                }
                btn_Check();
            }
        }
    }

    /**
     * 재생완료 onCompletion -> onSeekComplete
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.i("hy", "onCompletion");
        if (null != mPlayer) {
            playerEnd();
            mDictation.get(mCursor).playCnt++;
            btn_Check();
            mHandler.sendEmptyMessageDelayed(MSG_START_PLAY, 2000);
        }
    }

    /**
     * 음원 재생완료 onSeekComplete -> onCompletion
     */
    @Override
    public void onSeekComplete(MediaPlayer mp) {
        Log.i("hy", "onSeekComplete");
        mTooltip.setVisibility((mCursor == (mTotCnt - 1) && isFinishedStudy() && mDictation.get(mCursor).playCnt >= 3) ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * 일시정지/재게 기능을 수행함.
     */
    protected void playPause() {
        Log.i("hy", "playPause");
        if (null == mPlayer)
            return;

        if (mPlayer.isPlaying()) {
            mPlayer.pause();
            if (CommonUtil.isCenter()) // igse
                mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_play_n));
            else {
                if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                    mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_play_n));
                else // 숲
                    mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_play_n));
            }
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_activity_listen_01));
            return;
        }
        mHandler.removeCallbacksAndMessages(null);
        mPlayer.start();
        if (CommonUtil.isCenter()) // igse
            mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_pause_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_pause_n));
            else // 숲
                mBtnPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_pause_n));
        }
        mListen.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_activity_listen));
    }

    /**
     * 음원을 재생하도록 설정한다.
     *
     * @param mp3FilePath 음원의 로컬(스토리지) 위치
     */
    private synchronized void setPlayer(String mp3FilePath) {
        try {
            mPlayer = new MediaPlayer();
            mPlayer.reset();
            mPlayer.setDataSource(mp3FilePath);
            mPlayer.setOnSeekCompleteListener(this);
            mPlayer.setOnCompletionListener(this);
            mPlayer.setOnPreparedListener(this);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 재생 중이라면 정지시킨 후 리소스를 해제한다.
     */
    private synchronized void playerEnd() {
        if (null != mPlayer) {
            if (mPlayer.isPlaying())
                mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    /**
     * 메시지 박스를 발생시킨다.
     */
    /*private void showFinishConfirm() {
        if (isShowingMsgBox())
			return;
	
		playerEnd();
		mMsgBox = new MessageBox(this, 0, R.string.string_dictation_finish_message);
		mMsgBox.setId(1);
		mMsgBox.setConfirmText(R.string.string_common_yes);
		mMsgBox.setCancelText(R.string.string_common_no);
		mMsgBox.setOnDialogDismissListener(this);
		mMsgBox.show();
	}*/
    private void showFinishConfirm() {
        Log.i("hy", "showFinishConfirm");
        playerEnd();
        if (mCurState == WP_STATE) {//단어문장 완료시
            mMsgBox = new MessageBox(this, 0, R.string.string_dictation_wp_finish_message);
            mMsgBox.setId(1);
        } else if (mStudydata.mIsDicAnswer) {//SP문항 완료시&&정답 옵션 설정 되 잇을때
            mMsgBox = new MessageBox(this, 0, R.string.string_dictation_sp_finish_message);
            mMsgBox.setId(2);
        } else {//SP문항 완료시&&정답 옵션 설정 안되 잇을때
            mMsgBox = new MessageBox(this, 0, R.string.string_dictation_sp_finish_message2);
            mMsgBox.setId(3);
        }
        mMsgBox.setConfirmText(R.string.string_common_yes);
        mMsgBox.setCancelText(R.string.string_common_no);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();

    }

    /**
     * 메시지 박스의 결과를 반환 받는다.
     */
    public void onDialogDismiss(int result, int dialogId) {
        Log.i("hy", "onDialogDismiss=" + result);
        if (dialogId == 1) {//단어문장 완료시
            if (BaseDialog.DIALOG_CANCEL == result) {
                next();
                return;
            }
            mCurState = SP_STATE;
            readyView();
            startPlay();

        } else if (dialogId == 2) {//SP문항 완료시&&정답 옵션 설정 되 잇을때
            if (BaseDialog.DIALOG_CANCEL == result) {
                next();
                return;
            }
            String mResult = "Dictation_Word \n";

            for (WordQuestion word : mStudydata.mWordQuestion) {
                mResult += word.mQuestionOrder + ".  ";
                if (CommonUtil.isKorean(word.mQuestion)) {//문제가 한글이면
                    mResult += word.mMeaning + "\n";
                } else {
                    mResult += word.mQuestion + "\n";
                }
            }

            mResult += "Dictation_Sentence  \n";

            for (SentenceQuestion sentence : mStudydata.mSentenceQuestion) {
                mResult += sentence.mQuestionOrder + ".  ";
                mResult += sentence.mTextSentence + "\n";
            }

            showAnswerPop(mResult);

        } else if (dialogId == 3) {//SP문항 완료시&&정답 옵션 설정 안되 잇을때
            if (BaseDialog.DIALOG_CANCEL == result) {
                next();
            } else {
                onOk();
            }
        }

    }

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "--- S51 -----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, DictationNewActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            readyStudy();
                        }
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "-------S52-----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, DictationNewActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            goNextStatus();
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        readyStudy();
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        goNextStatus();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * 다음 단계로 이동 처리함
     */
    private void goNextStatus() {
        Log.i("hy", "goNextStatus");
        if (isNext) {
            isNext = false;
            StudyDataUtil.setCurrentStudyStatus(this, "S52");
            if (StudyData.getInstance().mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (StudyData.getInstance().mOneWeekData.size() > 0) {
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            } else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }

            finish();
        }
    }

    /**
     * wp,sp 정보 diction에 추가
     */
    private void initState() {
        if (mCurState == WP_STATE) {
            Log.i("hy", "initState" + "WP_STATE");
            mTotCnt = mStudydata.mWordQuestion.size();
            for (WordQuestion word : mStudydata.mWordQuestion) {
                mDictation.add(new DictationInfo(word.mSoundFile));
            }
        } else {
            Log.i("hy", "initState" + "SP_STATE");
            mTotCnt = mStudydata.mSentenceQuestion.size();
            mDictation.clear();
            for (SentenceQuestion senetence : mStudydata.mSentenceQuestion) {
                mDictation.add(new DictationInfo(senetence.mSoundFile));
            }
        }

    }

    /**
     * view 초기화
     */
    private void initview() {
        /*mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);
        if (null != mStepStatusBar)
            mStepStatusBar.setHighlights(StepStatusBar.STATUS_DICTATION);*/

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        mBtnPrev = (ImageView) findViewById(R.id.btn_prev);
        mBtnPrev.setOnClickListener(this);
        mBtnNext = (ImageView) findViewById(R.id.btn_next);
        mBtnNext.setOnClickListener(this);

        mListen = (ImageView) findViewById(R.id.dictation_listen_img);

        mBtnPlayPause = (ImageView) findViewById(R.id.btn_play_pause);
        mBtnPlayPause.setOnClickListener(this);

        mTooltip = (TextView) findViewById(R.id.tooltip_dict_text);
        mTooltip.setVisibility(View.INVISIBLE);
        mTooltip.setText(R.string.string_dictation_fast_forward_for_finish);

        //layout_pic = (LinearLayout) findViewById(R.id.layout_dic_pic);
        //layout_num = (LinearLayout) findViewById(R.id.layout_dic_num);
        //txt_dic_num = (TextView) findViewById(R.id.txt_dic_num);
        txt_dic_cnt = (TextView) findViewById(R.id.dictation_count_text);

        setTitlebarCategory(getString(R.string.string_titlebar_category_study));
    }

    /**
     * wp,sp view 설정
     */
    private void readyView() {
        initState();

        if (mCurState == SP_STATE) {//문장훈련 일떄 글씨변경
            mCursor = 0;
            //txt_dic_ment.setText(getString(R.string.string_listen_and_dictate_sentence));
            tCategory2.setText(R.string.string_listen_and_dictate_sentence);

            //layout_pic.removeAllViews();
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflater.inflate(R.layout.layout_dictation_status, layout_pic);
            mTooltip.setVisibility(View.INVISIBLE);

        }

        if (mTotCnt > 8) {//전체개수 판단
            mViewState = NUM_STATE;
        } else {
            mViewState = PIC_STATE;
        }

        if (mViewState == NUM_STATE) {//view init
            //layout_pic.setVisibility(View.GONE);
            //layout_num.setVisibility(View.VISIBLE);

            //txt_dic_num.setText("1/" + mTotCnt);
            txt_dic_cnt.setText(1 + " / " + mTotCnt);
        } else {
            mCursor = 0;
            //layout_pic.setVisibility(View.VISIBLE);
            //layout_num.setVisibility(View.GONE);

            mCursorW = getResources().getDimensionPixelSize(R.dimen.dimen_50);
            mItemW = getResources().getDimensionPixelSize(R.dimen.dimen_552) / mTotCnt;
            int interval = mCursorW - mItemW;
            mMarginLeft = mMarginRight = interval / 2;
            if (0 != interval % 2) {
                mMarginRight += interval % 2;
            }
            setCursor(mCursor, mTotCnt);

        }

    }

    /**
     * 커서별 버튼 설정
     */
    private void btn_Check() {
        if (mDictation.get(mCursor).playCnt >= 3 || ServiceCommon.VOC_MODE) {//voc 일 경우 스킵가능하게
            mBtnNext.setEnabled(true);
        } else {
            mBtnNext.setEnabled(false);
        }
    }

    /**
     * 정답확인
     *
     * @param txt
     */
    private void showAnswerPop(String txt) {

        ScrollBox mMsgBox = new ScrollBox(this, getString(R.string.string_answer_complete), txt);
        mMsgBox.setConfirmText(R.string.string_common_complete);
        mMsgBox.setOnDialogDismissListener(new OnDialogDismissListener() {

            @Override
            public void onDialogDismiss(int result, int dialogId) {
                // TODO Auto-generated method stub
                onOk();
            }
        });
        mMsgBox.setMessage(txt);
        mMsgBox.show();
    }
}
