package com.yoons.fsb.student.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.control.BothSideRoundProgressBar;
import com.yoons.fsb.student.ui.control.StepStatusBar;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.vanishing.VanishingTitlePaperActivity;

/**
 * 시험준비 화면
 * 
 * @author jaek
 */
public class ExamPreparingActivity extends BaseActivity implements OnClickListener {

	private StepStatusBar mStepStatusBar = null;

	private Thread mExamPreparingThread = null;

	private BothSideRoundProgressBar mProgress = null;

	private TextView mPreparingTimeView = null, mPreparingExtraTimeView = null;

	private Button mNextStatus = null;

	private StudyData mStudyData = null;

	private int mCurPosition = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mStudyData = StudyData.getInstance();

		if (mStudyData != null && !mStudyData.mIsAudioExist) {
			goNextStatus();
			return;
		}

		setContentView(R.layout.exam_preparing);

		StudyDataUtil.setCurrentStudyStatus(this, "S21");

		setWidget();

		runExamPreparingThread();

		Crashlytics.log(getString(R.string.string_ga_ExamPreparingActivity));

	}

	@Override
	protected void onStart() {
		super.onStart();
		//CommonUtil.conSlogService(this, true);
	}

	@Override
	protected void onStop() {
		super.onStop();
		//CommonUtil.conSlogService(this, false);
	}

	@Override
	protected void onPause() {
		// 학습 중 화면 잠금, 꺼짐 시 간지로 재시작 
		if (!isFinishing()) {
			if (mIsReStart) {
				startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class)
						.putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
			}

			finish();
		}

		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopExamTimerThread();
	}

	/**
	 * for bluetooth 7/24
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
			mNextStatus.setPressed(true);
			break;

		default:
			return super.onKeyDown(keyCode, event);
		}

		return false;
	}

	/**
	 * for bluetooth 7/24
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
			mNextStatus.setPressed(false);
			goNextStatus();
			break;

		default:
			return super.onKeyUp(keyCode, event);
		}

		return false;
	}

	@Override
	public void onClick(View view) {
		if (singleProcessChecker())
			return;

		int id = view.getId();
		if (id == R.id.exam_preparing_completion_next_btn) {
			goNextStatus();
		} else {
		}
	}

	/**
	 * layout을 설정함
	 */
	private void setWidget() {
		mPreparingTimeView = (TextView) findViewById(R.id.exam_preparing_position_text);
		mPreparingExtraTimeView = (TextView) findViewById(R.id.exam_preparing_extratime_text);
		mProgress = (BothSideRoundProgressBar) findViewById(R.id.exam_preparing_progressbar);
		mNextStatus = (Button) findViewById(R.id.exam_preparing_completion_next_btn);
		mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);

		mNextStatus.setOnClickListener(this);

		mProgress.setMax(ServiceCommon.MAX_EXAM_PREPARING_TIME);

		setTitlebarCategory(getString(R.string.string_titlebar_category_study));
		setTitlebarText(mStudyData.mProductName);
		// WiFi 감도 아이콘 설정
		setPreferencesCallback();

		mStepStatusBar.setHighlights(StepStatusBar.STATUS_EXAM_PREPARING);

		setTimeView();
	}

	/**
	 * 시험 준비 시간 업데이트 Tread를 시작함
	 */
	private void runExamPreparingThread() {
		mExamPreparingThread = new Thread("examPreparingThread") {
			public void run() {
				while (null != mExamPreparingThread && !mExamPreparingThread.isInterrupted()) {
					SystemClock.sleep(1000);
					mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_UPDATE, 0));
				}
			}
		};

		mExamPreparingThread.start();
	}

	/**
	 * 시험 준비 시간 업데이트 Tread를 종료함
	 */
	private void stopExamTimerThread() {
		if (null != mExamPreparingThread) {
			mExamPreparingThread.interrupt();
			mExamPreparingThread = null;
		}
	}

	/**
	 * 시험 준비 시간을 표시함
	 */
	private void setTimeView() {
		mPreparingTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition))
				.append(getString(R.string.string_common_margin_slash)).append(CommonUtil.convMsecToMinSec(ServiceCommon.MAX_EXAM_PREPARING_TIME))).toString());
		mPreparingExtraTimeView.setText((new StringBuilder().append(getString(R.string.string_exam_preparing_extratime))
				.append(getString(R.string.string_common_space)).append(CommonUtil.convMsecToMinSec(ServiceCommon.MAX_EXAM_PREPARING_TIME - mCurPosition))).toString());
	}

	/**
	 * 시험 준비 시간을 업데이트함
	 */
	private void preparingProgressUpdate() {
		mCurPosition += 1000;
		if (mCurPosition <= ServiceCommon.MAX_EXAM_PREPARING_TIME) {
			mProgress.setProgress(mCurPosition);
			setTimeView();

			if (mCurPosition == ServiceCommon.MAX_EXAM_PREPARING_TIME) {
				mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
				mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, 0));
			}
		}
	}

	/**
	 * 다음 단계로 이동 처리함
	 */
	private void goNextStatus() {
		if (isNext) {
			isNext = false;
			if (0 < mStudyData.mWordQuestion.size())
				startActivity(new Intent(this, WordTitlePaperActivity.class));
			else if (0 < mStudyData.mSentenceQuestion.size())
				startActivity(new Intent(this, SentenceTitlePaperActivity.class));
			else if (0 < mStudyData.mVanishingQuestion.size())
				startActivity(new Intent(this, VanishingTitlePaperActivity.class));
			else if (mStudyData.isNewDication&&mStudyData.mIsDicPirvate) {
				startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
			}else{
				startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
			}
			StudyDataUtil.setCurrentStudyStatus(this, "S22");

			finish();
		}
	}

	/**
	 * Handler
	 */
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (null == mContext)
				return;

			switch (msg.what) {
			case ServiceCommon.MSG_WHAT_STUDY:
				if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_UPDATE)
					preparingProgressUpdate();
				else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION)
					stopExamTimerThread();
				break;

			default:
				super.handleMessage(msg);
			}
		}
	};
}