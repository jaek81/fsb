package com.yoons.fsb.student.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.ui.base.TitlePaperBaseActivity;
import com.yoons.fsb.student.ui.control.BothSideRoundProgressBar;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

/**
 * 시험준비 간지 화면
 *
 * @author dckim
 */
public class ExamPreparingTitlePaperActivity extends TitlePaperBaseActivity {

    private BothSideRoundProgressBar mProgress = null;
    private int mCurPosition = 0;
    private Thread mExamPreparingThread = null;
    private TextView mPreparingExtraTimeView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) { // igse
            setContentView(R.layout.igse_exam_preparing_title_paper);
            findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.igse_btn_color_01));
        } else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) { // 우영
                setContentView(R.layout.w_exam_preparing_title_paper);
                findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.w_btn_color_01));
            } else { // 숲
                setContentView(R.layout.f_exam_preparing_title_paper);
                findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.f_btn_color_01));
            }
        }

        initTitlePaper(TP_TYPE_EXAM_PREPARING);

        Crashlytics.log(getString(R.string.string_ga_ExamPreparingTitlePaperActivity));

        setWidget();
        runExamPreparingThread();
    }

    private void setWidget() {
        mPreparingExtraTimeView = (TextView) findViewById(R.id.exam_preparing_extratime_text);
        mProgress = (BothSideRoundProgressBar) findViewById(R.id.exam_preparing_progressbar);
        mProgress.setMax(ServiceCommon.MAX_EXAM_PREPARING_TIME);
        setTimeView();
    }

    private void setTimeView() {
        mPreparingExtraTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(ServiceCommon.MAX_EXAM_PREPARING_TIME - mCurPosition))).toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopExamTimerThread();
    }

    /**
     * 시험 준비 시간을 업데이트함
     */
    private void preparingProgressUpdate() {
        mCurPosition += 1000;
        if (mCurPosition <= ServiceCommon.MAX_EXAM_PREPARING_TIME) {
            mProgress.setProgress(mCurPosition);
            setTimeView();

            if (mCurPosition == ServiceCommon.MAX_EXAM_PREPARING_TIME) {
                mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
                mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, 0));
            }
        }
    }

    /**
     * 시험 준비 시간 업데이트 Tread를 종료함
     */
    private void stopExamTimerThread() {
        if (null != mExamPreparingThread) {
            mExamPreparingThread.interrupt();
            mExamPreparingThread = null;
            findViewById(R.id.btn_ok).callOnClick();
        }
    }

    /**
     * 시험 준비 시간 업데이트 Tread를 시작함
     */
    private void runExamPreparingThread() {
        mExamPreparingThread = new Thread("examPreparingThread") {
            public void run() {
                while (null != mExamPreparingThread && !mExamPreparingThread.isInterrupted()) {
                    SystemClock.sleep(1000);
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_UPDATE, 0));
                }
            }
        };

        mExamPreparingThread.start();
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_UPDATE)
                        preparingProgressUpdate();
                    else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION)
                        stopExamTimerThread();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };
}
