package com.yoons.fsb.student.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.RequestSBMPhpPost;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.BlobUtil;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.hssb.student.custom.CustomMp3Recorder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * 인트로 화면
 *
 * @author ejlee
 */
public class IntroActivity extends BaseActivity implements OnDialogDismissListener {
    private String TAG = "[IntroActivity]";

    private String mUpdateUrl = ""; // 업데이트 파일을 다운로드 받을 url
    private String mUpdatePath = ServiceCommon.CONTENTS_PATH + "update/"; // 업데이트 파일 저장 path
    private String mUpdateFileName = CommonUtil.getCurrentYYYYMMDDHHMMSS() + "_hssb.apk"; // 업데이트 파일 이름
    private String mUpdateFullPath = mUpdatePath + mUpdateFileName; // 업데이트 파일

    // full path
    private int MSGBOX_ID_UPDATE_NOTI = 0; // 업데이트 메시지 박스 id
    private int MSGBOX_ID_UNMOUNTED_STORAGE = 1; // unmount 메시지 박스 id
    private int MSGBOX_ID_INCORRECT_TIME = 2; // 시간 오류 메시지 박스 id
    private int MSGBOX_ID_NOT_SUPPORT_DEVICE = 3; // 지원되지 않은 단말 오류 메시지 박스 id
    private int MSGBOX_ID_NOT_SUPPORT_ONLY_DEVICE = 4; // 전용 단말로 추측되는 단말 오류 메시지 박스 id
    private int MSGBOX_ID_UPDATE_FILE_UPLOAD_FAILED = 5; // 음성파일 업로드, 실패시 진행 할것인지 여부
    private int MSGBOX_ID_UPDATE_UPLOAD_SYNC_FAILED = 6; // uploadSync 실패시 진행 할것인지 여부
    private int MSGBOX_ID_UPDATE_UPLOAD_SYNC_RETRY_FAILED = 7; // uploadSync 재시도 실패시 진행 할것인지 여부
    private int MSGBOX_ID_LOGIN_FAILED = 8; // 전용단말에서 로그인 정보 없는 오류 메세지 박스id
    private int MSGBOX_ID_STOPPED_STUDY_FAILED = 9; // 중단된 학습이지만 학습단계가 잘못되었을 경우
    private int MSGBOX_ID_PERMISSION_FAILED = 10;
    private String mAlarmPackageName = "HSSBAlarmService.apk"; // assets 알람 서비스 apk이름
    private String mAlarmPackagePath = ServiceCommon.CONTENTS_PATH + "update/"; // 알람 서비스 저장할 로컬 경로
    private String mAlarmPackageFileName = mAlarmPackagePath + mAlarmPackageName; // 알람 서비스 파일 이름
    private LoadingDialog mProgressDialog = null; // progress dialog
    private DatabaseUtil mDatabaseUtil = null;
    private static final int REQUESTCODE_UPLOAD = 128;
    private int mUpdateRetryCnt = 0; // 업데이트 재시도 횟수 2회 이후 실패하면 로그인 화면으로 이동
    // 앱 업데이트시 시 버전 정보 전송에 사용
    private int mCustomerNo = 0;
    private String mCurrentVersion = "";
    private String mInstallVersion = "";
    private CustomMp3Recorder mMp3Recorder;

    private final int REQUEST_PERMISSIONS_CODE = 3322;

    /**
     * onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        checkPermissions();
    }

    private void init() {
        //로그 Blob 올리기
        if (ServiceCommon.VOC_MODE == true) {
            blobLogUpload();
        }

        checkFolders();
        DatabaseUtil.init();

        mDatabaseUtil = DatabaseUtil.getInstance(this);
        deleteDB();
        overridePendingTransition(0, 0);

        mMp3Recorder = new CustomMp3Recorder(mHandler, ServiceCommon.RECORD_SAMPLE_RATE);// mp3하는거 때문에 어쩔수없이

        if (ServiceCommon.SERVER == ServiceCommon.REAL_SERVER) {
            ServiceCommon.SBM_SERVER_URL = "http://sbms.yoons.com/api.php";
        } else {
            ServiceCommon.SBM_SERVER_URL = "http://sbms.yoons.or.kr/api.php";
        }

        RequestSBMPhpPost req = new RequestSBMPhpPost(this) {
            @Override
            protected void onPostExecute(Map<String, String> stringStringMap) {
                super.onPostExecute(stringStringMap);

                //login 정보 받기
                if ("0000".equals(stringStringMap.get("ret_code"))) {
                    Preferences.setCustomerNo(mContext, 0);
                    Preferences.setForestCode(mContext, "");
                    Preferences.setUserNo(mContext, "0");
                    Preferences.setCustomerName(mContext, "");
                    Preferences.setCaseStatus(mContext, "0");

                    Log.i("", "sjk forestcode => " + stringStringMap.get("i_forestcode"));
                    Log.i("", "sjk teachercode => " + stringStringMap.get("i_teachercode"));
                    Log.i("", "sjk lmsstatus => " + stringStringMap.get("i_lmsstatus"));
                    Log.i("", "sjk centralization => " + stringStringMap.get("centralization"));

                    Preferences.setForestCode(mContext, stringStringMap.get("i_forestcode"));
                    Preferences.setTeacherCode(mContext, stringStringMap.get("i_teachercode"));
                    Preferences.setLmsStatus(mContext, stringStringMap.get("i_lmsstatus"));
                    Preferences.setCenter(mContext, stringStringMap.get("centralization"));
                } else {
                    showError();
                    return;
                }

                //initServer();

                Log.e("Center", Preferences.getCenter(mContext));
                //중앙화모드
                if ("0".equals(Preferences.getCenter(mContext))) {
                    ServiceCommon.SERVER_MODE = ServiceCommon.LOCAL_MODE;
                } else {
                    ServiceCommon.SERVER_MODE = ServiceCommon.CENTER_MODE;
                }

                Intent i = getIntent();

                Log.i("", "sjk test url => " + i.getStringExtra(ServiceCommon.KEY_TEST_URL));

                //개발자모드
                String testUrl = "";
                if (i != null) {
                    testUrl = i.getStringExtra(ServiceCommon.KEY_TEST_URL);
                    if (testUrl != null) {// 4skill파라미터 보낼때 싸용
                        ServiceCommon.DEV_SERVER_URL = testUrl;
                        Log.e(TAG, "개발자모드!");
                    }
                }

                ServiceCommon.setServerUrl(mContext, testUrl);

                CommonUtil.makeNomedia(ServiceCommon.CONTENTS_PATH); // 특정 단말에서 .nomedia파일이 생성 안되는 문제가  있어서 intro에서 확인함.

                Preferences.setStudying(mContext, false);

                if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) { // device
                    // unmount상태인지
                    showUnmout();
                    return;
                }

                String mAgencyNo = Preferences.getForestCode(mContext);
                String mTeacherNo = Preferences.getTeacherCode(mContext);

                HttpJSONRequest request = new HttpJSONRequest(mContext); // 버전요청
                request.requestAppVersion(mHandler, 0, mAgencyNo, mTeacherNo);

                Crashlytics.log(getString(R.string.string_ga_IntroActivity));
            }
        };
        req.getServerInfo();
    }

    private void checkPermissions() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.RECORD_AUDIO};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(permissions, REQUEST_PERMISSIONS_CODE);
            } else {
                init();
            }
        } else {
            init();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_CODE:
                boolean retval = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        continue;
                    } else {
                        mMsgBox = new MessageBox(this, 0, "권한 동의 후 사용가능합니다.");
                        mMsgBox.setConfirmText(R.string.string_common_confirm);
                        mMsgBox.setId(MSGBOX_ID_PERMISSION_FAILED);
                        mMsgBox.setOnDialogDismissListener(this);
                        mMsgBox.show();
                        retval = false;
                        break;
                    }
                }
                if (retval) {
                    init();
                }
                break;
        }
    }

    private void showUnmout() {
        // 확인
        mMsgBox = new MessageBox(this, 0, R.string.string_msg_not_exist_storage_or_using_usb_disk);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.setId(MSGBOX_ID_UNMOUNTED_STORAGE);
        mMsgBox.show();
    }

    private void showError() {
        mMsgBox = new MessageBox(this, 0, "잘못된 접근입니다. 등록정보를 확인해주세요.");
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.setId(MSGBOX_ID_UNMOUNTED_STORAGE);
        mMsgBox.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        overridePendingTransition(0, 0);
    }

    /**
     * 화면 설정
     */
    private void initLayout() {
        TextView versionTextView = (TextView) findViewById(R.id.version);
        String versionText = "";
        String version = CommonUtil.getAppVersion(mContext);
        versionText = "ⓒYOONS ENGLISH SCHOOL.                     " + version;
        versionTextView.setText(versionText);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS: // 버전 요청 성공
                    versionAndTimeCheck((JSONObject) msg.obj);
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL: // 버전 요청 실패
                    MessageBox mMsgBox = new MessageBox(mContext, 0, R.string.string_msg_can_not_connect);
                    // }

                    mMsgBox.setConfirmText(R.string.string_common_confirm);
                    mMsgBox.setOnDialogDismissListener(new OnDialogDismissListener() {

                        @Override
                        public void onDialogDismiss(int result, int dialogId) {
                            finish();
                        }
                    });
                    if (mContext != null) {
                        mMsgBox.show();
                    }
                    // startLogin();
                    break;

                case ServiceCommon.MSG_UPDATE_DOWNLOAD_SUCCESS: // 업데이트 파일 다운로드 성공
                    // sendUdateHistory();
                    installUpdatedApp();
                    break;

                case ServiceCommon.MSG_UPDATE_DOWNLOAD_FAIL: // 업데이트 파일 다운로드 실패
                    hideLoading();
                    startLogin();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * 로그인 화면으로 이동
     */
    public void startLogin() {

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * app 버전 및 시간 체크
     *
     * @param obj : 서버에서 받은 버전 및 현재 서버시간 정보
     */
    private void versionAndTimeCheck(JSONObject obj) {
        String appVersion = "";
        String serverVersion = "";
        String serverDate = "";

        try {
            PackageInfo i = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = i.versionName;

            JSONArray out1 = obj.getJSONArray("out1");
            JSONObject versionObject = out1.getJSONObject(0);
            serverVersion = versionObject.getString("app_version");
            mUpdateUrl = versionObject.getString("app_url");
            serverDate = versionObject.getString("server_date");

            mCurrentVersion = appVersion;
            mInstallVersion = serverVersion;

            Log.e(TAG, "version : " + serverVersion + " updateUrl : " + mUpdateUrl + " serverDate : " + serverDate);

            if (timeCheck(serverDate)) { // 시간 먼저 체크함.
                // 스트링 비교로 처리할 수 없을수도 있음.(1.9.1과 1.10.1일 경우)
                // 따라서 값을 int로 바꿔서 처리.
                int appVersionValue = CommonUtil.getVersionValue(appVersion);
                int serverVersionValue = CommonUtil.getVersionValue(serverVersion);

                if (appVersionValue == -1 || serverVersionValue == -1) {
                    startLogin();
                    return;
                }

                if (appVersionValue < serverVersionValue) {
                    // startLogin();//잠깐
                    showUpdateNotiDialog();
                } else {
                    startLogin();
                }
            } else {
                showTimeSyncErrorDialog();
            }
        } catch (Exception e) {
            e.printStackTrace();
            startLogin();
        }
    }

    /**
     * 시간이 맞지 않을 때 에러 메시지 박스 띄움
     */
    private void showTimeSyncErrorDialog() {
        mMsgBox = new MessageBox(this, 0, R.string.string_msg_time_sync_error);
        mMsgBox.setId(MSGBOX_ID_INCORRECT_TIME);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 업데이트 메세지 박스 띄움
     */
    private void showUpdateNotiDialog() {
        mMsgBox = new MessageBox(this, 0, R.string.string_msg_exist_latest_version);
        mMsgBox.setConfirmText(R.string.string_msg_update);
        // mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setId(MSGBOX_ID_UPDATE_NOTI);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 업데이트 실패시 사용자에게 재시도를 진행할 것인지 박스 띄움
     */
    private void showUpdateRetryDialog(int id) {
        if (mUpdateRetryCnt == 2) {
            mMsgBox = new MessageBox(this, 0, R.string.string_msg_update_sync_retry_error);
            mMsgBox.setConfirmText(R.string.string_common_confirm);
            mMsgBox.setId(MSGBOX_ID_UPDATE_UPLOAD_SYNC_RETRY_FAILED);
            mMsgBox.setOnDialogDismissListener(this);
            mMsgBox.show();
        } else {
            mMsgBox = new MessageBox(this, 0, R.string.string_msg_update_sync_error);
            mMsgBox.setConfirmText(R.string.string_msg_update_retry);
            mMsgBox.setCancelText(R.string.string_common_cancel);
            mMsgBox.setId(id);
            mMsgBox.setOnDialogDismissListener(this);
            mMsgBox.show();
        }
    }

    /**
     * 오류 메시지 박스 확인 메시지를 받음
     */
    @Override
    public void onDialogDismiss(int result, int dialogId) {
        if (MSGBOX_ID_UPDATE_NOTI == dialogId) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                if (mUpdateUrl.length() <= 0) {
                    startLogin();
                    return;
                }

                showLoading();
                mSyncHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS);
            }
            if (BaseDialog.DIALOG_CANCEL == result) {
                startLogin();
            }
        } else if (MSGBOX_ID_UNMOUNTED_STORAGE == dialogId || MSGBOX_ID_LOGIN_FAILED == dialogId) {
            System.exit(0);
            finish();
        } else if (MSGBOX_ID_INCORRECT_TIME == dialogId) {
            Intent intent = new Intent(Settings.ACTION_DATE_SETTINGS);
            intent.putExtra("extra_prefs_show_button_bar", true);
            intent.putExtra("extra_prefs_set_next_text", "");
            intent.putExtra("extra_prefs_set_back_text", getString(R.string.string_common_close)); // 닫기
            startActivity(intent);
            finish();
        } else if (MSGBOX_ID_NOT_SUPPORT_DEVICE == dialogId) {
            finish();
        } else if (MSGBOX_ID_NOT_SUPPORT_ONLY_DEVICE == dialogId) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse("http://app.yoons.com/sbh/q7.html");
                i.setData(u);
                startActivity(i);
                finish();
            } else {
                finish(); // 업데이트에서 취소를 눌렀을때. 종료인지 확인.
            }
        } else if (MSGBOX_ID_UPDATE_UPLOAD_SYNC_FAILED == dialogId) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                showLoading();
                mSyncHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
                mUpdateRetryCnt++;
            } else {
                startLogin();
            }
        } else if (MSGBOX_ID_UPDATE_FILE_UPLOAD_FAILED == dialogId) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                showLoading();
                mSyncHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_FILE_UPLOAD);
                mUpdateRetryCnt++;
            } else {
                startLogin();
            }
        } else if (MSGBOX_ID_UPDATE_UPLOAD_SYNC_RETRY_FAILED == dialogId) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                startLogin();
            }
        } else if(MSGBOX_ID_PERMISSION_FAILED == dialogId) {
            finish();
        }
    }

    /**
     * 로딩 화면을 보여줌
     */
    private void showLoading() {
        mProgressDialog = LoadingDialog.show(this, R.string.string_common_loading, R.string.string_updating);
    }

    /**
     * 로딩 화면을 숨김
     */
    private void hideLoading() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * 업데이트 파일 다운로드
     *
     * @author ejlee
     */

    private void appUpdate() {
        Thread thread = new Thread("appUpdate") {
            public void run() {
                HttpURLConnection connection = null;
                FileOutputStream fos = null;
                InputStream is = null;

                try {
                    URL url = new URL(mUpdateUrl);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(false);
                    connection.connect();

                    File updateFileDir = new File(mUpdatePath);
                    if (!updateFileDir.exists())
                        updateFileDir.mkdirs();

                    String[] updateFileNameList = updateFileDir.list();
                    for (int i = 0; i < updateFileNameList.length; i++) {
                        Log.i("", "updateFileName " + i + " => " + updateFileNameList[i]);
                        if (updateFileNameList[i].contains("_hssb.apk")) {
                            File oldUpdateFile = new File(updateFileDir, updateFileNameList[i]);
                            oldUpdateFile.delete();
                        }
                    }

                    File updateFile = new File(updateFileDir, mUpdateFileName);

                    int responseCode = connection.getResponseCode();
                    Log.e(TAG, "appUpdateThread responseCode => " + responseCode);

                    if (HttpURLConnection.HTTP_OK == responseCode) {
                        fos = new FileOutputStream(updateFile);

                        is = connection.getInputStream();

                        byte[] buffer = new byte[1024];
                        int len = 0;
                        while ((len = is.read(buffer)) != -1)
                            fos.write(buffer, 0, len);

                        mHandler.sendEmptyMessage(ServiceCommon.MSG_UPDATE_DOWNLOAD_SUCCESS);
                    } else
                        mHandler.sendEmptyMessage(ServiceCommon.MSG_UPDATE_DOWNLOAD_FAIL);
                } catch (Exception e) {
                    Log.e(TAG, "appUpdate Exception !");
                    e.printStackTrace();
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_UPDATE_DOWNLOAD_FAIL);
                } finally {
                    try {
                        if (null != fos)
                            fos.close();

                        if (null != is)
                            is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (null != connection) {
                        connection.disconnect();
                        connection = null;
                    }
                }
            }
        };

        thread.start();
    }

    /**
     * update 파일 설치
     */
    private void installUpdatedApp() {
        if (CommonUtil.isQ7Device()) {
            /* if (CommonUtil.isSMT330Device()) { */
            Intent intent = new Intent(Intent.ACTION_VIEW);
            File file = new File(mUpdateFullPath);
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            IntroActivity.this.startActivity(intent);
            IntroActivity.this.finish();
        } else {
            hideLoading();

            CommonUtil.installLocalPackage(this, mUpdateFullPath);

            finish();
        }
    }

    /**
     * 서버 시간과 로컬 시간 비교 5분이상 차이나면 시간이 안맞는 것으로 간주함.
     *
     * @param serverTime : 서버시간
     * @return 시간 오류가 있는지 여부
     */
    private boolean timeCheck(String serverTime) {
        String localTime = CommonUtil.getCurrentDateTime();
        Date serverDate = null;
        Date localDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            serverDate = sdf.parse(serverTime);
            localDate = sdf.parse(localTime);

            Log.e(TAG, "timeCheck serverTime : " + serverDate.getTime() / 1000 + " localTime : "
                    + localDate.getTime() / 1000);

            if (Math.abs(serverDate.getTime() - localDate.getTime()) / 1000 > 300) {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }

        return true;
    }

    /**
     * sync handler
     */
    public Handler mSyncHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case ServiceCommon.MSG_RECORDING_FILE_UPLOAD: // 음성 파일 업로드 시작
                    // startActivityForResult(new Intent(mContext,
                    // UploadActivity.class), REQUESTCODE_UPLOAD);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START: // upload sync 시작
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS: // upload sync
                    // 완료
                    appUpdate();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL:
                    hideLoading();
                    showUpdateRetryDialog(MSGBOX_ID_UPDATE_UPLOAD_SYNC_FAILED);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_DEVICE_ERROR_FAIL: // upload  sync시 등록되지 않은 device에서 학습 결과를 업로드 했을 때 발생

                    int customerNo = msg.arg1;
                    mDatabaseUtil.deleteCustomer(customerNo);
                    syncUploadDatabaseTable();
                    break;
            }
        }
    };

    /**
     * upload Sync 시작
     */
    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mSyncHandler);
        } else {
            Log.e(TAG, "SyncEnd syncUploadDatabaseTable");
            mHandler.sendEmptyMessage(ServiceCommon.MSG_UPDATE_DOWNLOAD_FAIL);
        }
    }


    private void checkFolders() {

        CommonUtil.deleteDirectory(new File(ServiceCommon.AUDIO_RECORD_PATH));
        CommonUtil.deleteDirectory(new File(ServiceCommon.EXAM_RECORD_PATH));
        CommonUtil.deleteDirectory(new File(ServiceCommon.EXAM_RECORD_UPLOAD_PATH));

        CommonUtil.makeDirs(ServiceCommon.CONTENTS_PATH); // db파일 디랙토리 생성
        CommonUtil.makeDirs(ServiceCommon.AUDIO_RECORD_PATH);
        CommonUtil.makeDirs(ServiceCommon.EXAM_RECORD_PATH);
        CommonUtil.makeDirs(ServiceCommon.EXAM_RECORD_UPLOAD_PATH);
        CommonUtil.makeDirs(ServiceCommon.DB_BACK_UP_PATH);

        File file = new File(ServiceCommon.DB_PATH);

        if (!file.exists() || ServiceCommon.IS_EXPERIENCES) {
            CommonUtil.copyAssetsToSDCard(mContext, ServiceCommon.DB_FILE, ServiceCommon.DB_PATH);
        }
    }

    /**
     * db삭제
     */
    private void deleteDB() {
        ArrayList<String> delteList = new ArrayList<String>();
        delteList.add("android_metadata");
        delteList.add("sbm_localquery");
        delteList.add("sync_info");
        delteList.add("sbm_customer");
        delteList.add("sbm_customer_config");
        delteList.add("sbm_study_day_config");
        delteList.add("sbm_study_result");
        delteList.add("sbm_study_result_review");
        delteList.add("sbm_study_book_warning");
        delteList.add("wye_befly_study_unit");
        delteList.add("yfs_sm_word_question");
        delteList.add("yfs_sm_sentence_question");
        delteList.add("yfs_sm_pa_sentence_question");
        delteList.add("sbm_study_past_history");
        delteList.add("sbm_sync_tables");
        delteList.add("download_content");
        delteList.add("sqlite_sequence");
        delteList.add("sbm_network_used");
        delteList.add("tb_studyplan");
        delteList.add("sbm_study_record_detail");
        delteList.add("sbm_study_word_detail");
        delteList.add("sbm_study_sentence_detail");
        delteList.add("sbm_study_pa_sentence_detail");
        delteList.add("sbm_study_exam_detail");
        mDatabaseUtil.deleteCommonTable(delteList);
    }

    /**
     * --개발자 모드 &서버 설정--
     */
    private void initServer() {
        Intent i = getIntent();
        if (i == null || i.getStringExtra(ServiceCommon.KEY_FOREST_CODE) == null) {
            Toast.makeText(this, "잘못된 접근입니다. 런처에서 실행해주세요.", Toast.LENGTH_LONG).show();
            sendLogOutEvent2();
            return;
        }

        initIntent(i);

        Log.e("Center", Preferences.getCenter(mContext));
        //중앙화모드
        if (i != null && "0".equals(Preferences.getCenter(mContext))) {
            ServiceCommon.SERVER_MODE = ServiceCommon.LOCAL_MODE;

        } else {
            ServiceCommon.SERVER_MODE = ServiceCommon.CENTER_MODE;

        }

        //개발자모드
        String testUrl = "";
        if (i != null) {
            testUrl = i.getStringExtra(ServiceCommon.KEY_TEST_URL);
            if (testUrl != null) {// 4skill파라미터 보낼때 싸용
                ServiceCommon.DEV_SERVER_URL = testUrl;
                Log.e(TAG, "개발자모드!");
            }
        }


        ServiceCommon.setServerUrl(this, testUrl);
    }

    /**
     * 런처에서 받는 중앙화여부,센터번호,원장번호,LMS구분(0:영어숲, 1:우영, 2: IGSE)
     *
     * @param i
     */
    private void initIntent(Intent i) {

        Preferences.setCustomerNo(mContext, 0);
        Preferences.setForestCode(mContext, "");
        Preferences.setUserNo(mContext, "0");
        Preferences.setCustomerName(this, "");
        Preferences.setCaseStatus(this, "0");

        String str_forest_code = i.getStringExtra(ServiceCommon.KEY_FOREST_CODE);
        String str_techer_code = i.getStringExtra(ServiceCommon.KEY_TEACHER_CODE);
        String str_lms_status = i.getStringExtra(ServiceCommon.KEY_LMS_STATUS);
        String str_center = i.getStringExtra(ServiceCommon.KEY_CENTER);


        Preferences.setForestCode(mContext, str_forest_code);
        Preferences.setTeacherCode(mContext, str_techer_code);
        Preferences.setLmsStatus(mContext, str_lms_status);
        Preferences.setCenter(mContext, str_center);
        /*
        Preferences.setForestCode(mContext, "5794");
        Preferences.setTeacherCode(mContext, "0");
        Preferences.setLmsStatus(mContext, "0");
        Preferences.setCenter(mContext, "1");
*/
    }

    /**
     * voc대응용 blob 로그 업로드
     */
    private void blobLogUpload() {

        try {
            File[] list = Log.getLogFiles();///fsbdata/log/안에 있는 파일 찾음
            if (list != null) {
                for (File file : list) {

                    BlobUtil.FileUpload(BlobUtil.LOG_PATH + Preferences.getForestCode(mContext) + "/" + Preferences.getTeacherCode(mContext) + "/", file, true);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
