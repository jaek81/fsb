package com.yoons.fsb.student.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.db.DatabaseData.LoginCustomer;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.network.RequestSBMPhpPost;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.control.LoginInputItem;
import com.yoons.fsb.student.ui.control.Indicator;
import com.yoons.fsb.student.ui.control.TitleView;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 로그인 화면
 *
 * @author ejlee
 */
public class LoginActivity extends BaseActivity implements OnClickListener, OnDialogDismissListener {
    private static final String TAG = "[LoginActivity]";

    private static final int REQUEST_ID_LOGIN = 100; // 서버 request구분을 위한 id
    private static final int REQUEST_ID_REGI = 101;

    private static final String NETWOR_RESULT_OK = "0000";// 네트워크 결과 정상 코들

    private Button mBaseLoginButton = null;
    /* private Button mEasyLoginButton = null; */
    private LinearLayout mBaseLoginLayout = null;

    private LinearLayout close;
    // 기본 로그인 레이아웃
    /*
     * private LinearLayout mEasyLoginLayout = null; // 간편 로그인 레이아웃
     */ /* private LinearLayout mBaseLoginGuideLayout = null; */ // 기본 로그인 하단 가이드
    // layout
    /*
     * private
     * LinearLayout
     * mEasyLoginGuideLayout
     * = null; // 간편
     * 로그인 하단 가이드
     * layout
     */
    private LinearLayout mEasyLoginLayout1 = null; // 간편 로그인 1명 레이아웃
    private LinearLayout mEasyLoginLayout2 = null; // 간편 로그인 2명 레이아웃

    /*
     * private EditText mIdEditText = null; private EditText mPasswordEditText =
     * null;
     */

    /*
     * private ImageView mIdCancel = null; private ImageView mPasswordCancel =
     * null;
     */
    private TextView mLoginButton = null;

    private DatabaseUtil mDatabaseUtil = null;
    private ArrayList<LoginCustomer> mCustomerList = null; // db상에 있는 회원 list

    private String mId = "";
    private String mPassword = "";
    private int mCustomerNo = 0;
    private String mCustomerName = "";
    private String mUdid = "";
    private String mAgencyNo = "";
    private String mUserNo = "0";//사용자번호
    private int mSelectCustomer = 0;
    private int mLuncherSelectCustomer = 0;
    private LoadingDialog mProgressDialog = null;
    private boolean mIsAutoLogin = true;
    private LoginInputItem mNum1, mNum2, mNum3, mNum4;
    private int selNum = 0;
    private RequestPhpPost reqLGIN;

    /**
     * onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // Igse
            setContentView(R.layout.igse_login);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                setContentView(R.layout.w_login);
            else // 숲
                setContentView(R.layout.f_login);
        }

        findViewById(R.id.paper_title_view).setVisibility(View.GONE);
        findViewById(R.id.login_title_view).setVisibility(View.VISIBLE);

        Preferences.setStudying(this, false);
        initLayout();

        mDatabaseUtil = DatabaseUtil.getInstance(this);

        mCustomerList = mDatabaseUtil.selectLoginCustomer();
        setPreferencesCallback();
        /*
         * if (CommonUtil.isQ7Device() && ServiceCommon.IS_LAUNCHER) {
         * setContentView(R.layout.intro); launcherLogin(); } else {
         * setLoginData(true); }
         */
        if (ServiceCommon.SERVER != ServiceCommon.TEST_SERVER) {//테스트버전 소리 조정 x
            setVolume();
        }
        //중앙화 표시

        TextView txt_center = (TextView) findViewById(R.id.txt_center);
        if (CommonUtil.isCenter()) {
            txt_center.setText("(C)");
        } else {
            txt_center.setText("(L)");
        }

        //keepScreen(false);
        overridePendingTransition(0, 0);
        guidePlay(R.raw.p_login);
        Crashlytics.log(getString(R.string.string_ga_LoginActivity));

        mNum1.setSelected(true);
        mNum2.setSelected(false);
        mNum3.setSelected(false);
        mNum4.setSelected(false);
    }
    /*
     * @Override protected void onStart() { // TODO Auto-generated method stub
     * super.onStart(); FlurryAgent.onStartSession(this,
     * ServiceCommon.FLURRY_API_KEY); }
     *
     * @Override protected void onStop() { // TODO Auto-generated method stub
     * super.onStop(); FlurryAgent.onEndSession(this); }
     */

    /**
     * 로그인 화면에서는 로그 오프 이벤트 받아 화면이 닫히는 경우가 없도록 함.(빈 메소드)
     */
    @Override
    protected void setLogOutReceiver() {
        registerReceiver(mFinishEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, final Intent intent) {

                String action = intent.getAction();
                if (action.equalsIgnoreCase(ACTIVITY_LOG_OUT_EVENT) && intent.getBooleanExtra("finishall", false)) {
                    finish(); // Activity 종료 처리
                }
            }
        }, new IntentFilter(ACTIVITY_LOG_OUT_EVENT));
    }

    /**
     * 볼륨이 최대 볼륨의 2/3이하일때 2/3으로 올려줌
     */
    private void setVolume() {

        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
        int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int userVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        int volume = maxVolume * 66 / 100;

        if (userVolume < maxVolume * 66 / 100) {
            am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
        }
    }

    public interface LoginItemEvent {
        public void onSelect(int sel, boolean isLeft);

        public void onUpDown(boolean upDown);
    }

    private LoginItemEvent mItemEvent = new LoginItemEvent() {
        @Override
        public void onSelect(int sel, boolean isLeft) {
            selectTextTouch(sel, isLeft);
        }

        @Override
        public void onUpDown(boolean upDown) {
            cntEdit(upDown);
        }
    };


    /**
     * 화면 초기화
     */
    private void initLayout() {
        setTitlebarText("");//getString(R.string.string_common_login));

        mBaseLoginLayout = (LinearLayout) findViewById(R.id.base_login_layout);
        // 기본 로그인 View
        mLoginButton = (TextView) findViewById(R.id.login_btn);

        mNum1 = (LoginInputItem) findViewById(R.id.num1_edit);
        mNum2 = (LoginInputItem) findViewById(R.id.num2_edit);
        mNum3 = (LoginInputItem) findViewById(R.id.num3_edit);
        mNum4 = (LoginInputItem) findViewById(R.id.num4_edit);

        mNum1.setInterface(mItemEvent);
        mNum2.setInterface(mItemEvent);
        mNum3.setInterface(mItemEvent);
        mNum4.setInterface(mItemEvent);

        LinearLayout body_layout = findViewById(R.id.login_body_layout);
        body_layout.setAlpha(0.6f);

        mNum1.setText("0");
        mNum2.setText("0");
        mNum3.setText("0");
        mNum4.setText("0");
        /*		mNum1EditText.setText("1");
        mNum2EditText.setText("3");
		mNum3EditText.setText("0");
		mNum4EditText.setText("5");
		*/
        // 키보드안올라오게
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mLoginButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        mLoginButton.setEnabled(true);

    }

    /**
     * 로그인 타입에 의한 화면 설정
     *
     * @param isBaseLogin    : 기본 로그인 인지 여부
     * @param easyLoginCount : 간편 로그인 명수
     */
    private void setLoginType(boolean isBaseLogin, int easyLoginCount) {
        if (!isBaseLogin && (easyLoginCount < 0 || easyLoginCount > 2)) {
            Log.e(TAG, "easyLoginCount Error : " + easyLoginCount);
            return;
        }

        mBaseLoginButton.setSelected(isBaseLogin);

        mBaseLoginLayout.setVisibility(isBaseLogin ? View.VISIBLE : View.GONE);

        if (!isBaseLogin) {
            mEasyLoginLayout1.setVisibility((easyLoginCount == 1) ? View.VISIBLE : View.GONE);
            mEasyLoginLayout2.setVisibility((easyLoginCount != 1) ? View.VISIBLE : View.GONE);
        }

        if (!isBaseLogin && easyLoginCount != 1) {
            mBaseLoginButton.setEnabled(false);
        } else {
            mBaseLoginButton.setEnabled(true);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.login_btn) {
            if (loginInputCheck()) {
                mSelectCustomer = -1;
                login();
                mIsAutoLogin = false;
            }
        }
    }


    /**
     * 교재 다운로드 화면 실행
     */
    private void startBookDownloadActivity() {

        // showProgressDialog(false);
        if (mProgressDialog == null || (mProgressDialog != null && !mProgressDialog.mIsStop)) {
            if (ServiceCommon.IS_EXPERIENCES) {
                startActivity(new Intent(this, StudyBookSelectActivity.class));
            } else {
                startActivity(new Intent(this, BookDownloadActivity.class));
            }
        }

        try {
            if (mCustomerName.length() == 0) {
                mCustomerName = mCustomerList.get(mSelectCustomer).mCustomerName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mCustomerName.length() != 0 && !ServiceCommon.IS_LAUNCHER) {
            Toast.makeText(this, mCustomerName + "님 환영합니다.", Toast.LENGTH_SHORT).show();
        }

        Preferences.setCustomerName(this, mCustomerName);

        finish();
    }

    /**
     * 로그인 id,password 입력 체크
     *
     * @return
     */
    private boolean loginInputCheck() {
        /*
         * if (mId.length() == 0) { // 입력 오류 체크는 상세히 확인
         * showErrorMessageBox(888); return false; } else
         */
        String num1 = mNum1.getText().toString();
        String num2 = mNum2.getText().toString();
        String num3 = mNum3.getText().toString();
        String num4 = mNum4.getText().toString();
        if ("".equals(num1) & "".equals(num2) & "".equals(num3) & "".equals(num4)) {
            showErrorMessageBox(889);
            return false;
        }

        return true;
    }

    /**
     * 로그인 할 id와 pasword를 설정
     */
    private void setLoginIdPassword() {
        int index = mSelectCustomer;

        if (mCustomerList.size() <= index) {
            Log.e(TAG, "Error Customer list");
        }

        LoginCustomer customer = mCustomerList.get(index);
        mId = customer.mUserId;
        mPassword = customer.mUserPassword;
    }

    /**
     * 로그인 요청. 권한 확인 및 네트워크 체크
     */
    private void login() {
        /*
         * if (CommonUtil.isQ7Device() && !checkAuthPassHomeSBAdmin()) {
         * showErrorMessageBox(991); return; }
         */

        if (!ServiceCommon.IS_LAUNCHER) {
            showProgressDialog(true);
        }

        CommonUtil.isAvailableNetwork(this, true, mNetworkCheckHandler, REQUEST_ID_LOGIN);
    }

    public Handler mNetworkCheckHandler = new Handler() {
        public void handleMessage(Message msg) {
            int id = msg.arg1;

            switch (msg.what) {
                case ServiceCommon.MSG_NETWORK_CHECK_CONNECTED:
                    if (id == REQUEST_ID_LOGIN) {
                        //HttpJSONRequest request = new HttpJSONRequest(mContext);
                        mPassword = mNum1.getText().toString() + mNum2.getText().toString() + mNum3.getText().toString() + mNum4.getText().toString();
                        reqLGIN = new RequestPhpPost(mContext) {
                            protected void onPostExecute(java.util.Map<String, String> result) {
                                Log.e(TAG, result.toString());

                                try {
                                    String mCase = "0";//로그인 케이스
                                    String mRet_Code = "";//통신결과

                                    if (result.get("case") != null) {
                                        mCase = result.get("case").trim();
                                    }

                                    if (result.get("ret_code") != null) {
                                        mRet_Code = result.get("ret_code").trim();
                                    }

                                    if (mRet_Code.equals(NETWOR_RESULT_OK)) {//네트워크 결과 일때

                                        mCustomerNo = Integer.valueOf(result.get("mcode"));
                                        mCustomerName = result.get("studentname");
                                        mAgencyNo = result.get("acode");
                                        mUserNo = result.get("usernumber");//
                                        final String serialNumber = result.get("serialnumber");//

                                        Crashlytics.log(mAgencyNo + "|" + Preferences.getTeacherCode(mContext) + "|" + mCustomerNo + "|" + mUserNo + "|" + mCustomerName + "|" + CommonUtil.getMacAddress() + "|");

                                        Preferences.setCustomerNo(mContext, mCustomerNo);
                                        Preferences.setUserNo(mContext, mUserNo);
                                        Preferences.setCaseStatus(mContext, mCase);
                                        if (!CommonUtil.isCenter()) {
                                            Preferences.setForestCode(mContext, mAgencyNo);
                                        }
                                        Log.i("", "case => " + mCase);

                                        if (mCase.equals("1")) {//MLGNL,MLGNB 실행

                                            RequestSBMPhpPost reqMLGNL = new RequestSBMPhpPost(mContext);
                                            reqMLGNL.sendMLGNL(serialNumber, mCustomerName, mUserNo, mCase);

                                            RequestSBMPhpPost reqMLGNB = new RequestSBMPhpPost(mContext) {
                                                protected void onPostExecute(java.util.Map<String, String> result) {

                                                    String useyn = result.get("useyn");
                                                    if (useyn == null) {//lms에서 값 안줄때
                                                        mMsgBox = new MessageBox(mContext, 0, getString(R.string.string_Login_no_user));
                                                    } else if (useyn.equals("0")) {
                                                        mMsgBox = new MessageBox(mContext, 0, getString(R.string.string_Login_Case1_disable_message));
                                                    } else if (useyn.equals("1")) {
                                                        mMsgBox = new MessageBox(mContext, 0, getString(R.string.string_Login_Case1_enable_message));

                                                        mMsgBox.setConfirmText(R.string.string_common_confirm);
                                                        mMsgBox.setId(-7);
                                                        mMsgBox.setOnDialogDismissListener(LoginActivity.this);
                                                        RequestSBMPhpPost reqMLGNL3 = new RequestSBMPhpPost(mContext);
                                                        reqMLGNL3.sendMLGNL(serialNumber, mCustomerName, mUserNo, "3");
                                                        Preferences.setCaseStatus(mContext, "3");//1로들어왔을때 재수강이면 case3이라고 보내준다.
                                                    }
                                                    showProgressDialog(false);
                                                    try {
                                                        mMsgBox.show();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                ;
                                            };

                                            reqMLGNB.getMLGNB();
                                            return;
                                        } else if (mCase.equals("2")) {

                                            RequestSBMPhpPost reqMLGNL = new RequestSBMPhpPost(mContext);
                                            reqMLGNL.sendMLGNL(serialNumber, mCustomerName, mUserNo, mCase);

                                            mMsgBox = new MessageBox(mContext, 0, getString(R.string.string_Login_Case2_message));
                                            mMsgBox.setConfirmText(R.string.string_common_confirm);
                                            mMsgBox.setId(-7);
                                            mMsgBox.setOnDialogDismissListener(LoginActivity.this);
                                            mMsgBox.show();

                                            return;
                                        }
                                        //case0,5는 그냥넘어감

                                        if (!mDatabaseUtil.selectCustomerGetCntWhere(mCustomerNo)) {
                                            mDatabaseUtil.insertCustomer(mCustomerNo, mId, mPassword, mCustomerName);
                                        }
                                        startBookDownloadActivity();

                                    } else {
                                        showProgressDialog(false);
                                        showErrorMessageBox(-1);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    showProgressDialog(false);
                                    mMsgBox = new MessageBox(mContext, 0, getString(R.string.string_Login_no_user));
                                    mMsgBox.show();
                                }
                            }

                            ;
                        };

                        //
                        reqLGIN.getLGIN(mPassword, "0");
                        //ys20용 로그인
                        //request.requestLogin(mHandler, mId, mPassword, "2010-06-10", REQUEST_ID_LOGIN);
                    } else if (id == REQUEST_ID_REGI) {

                    }
                    break;

                case ServiceCommon.MSG_NETWORK_CHECK_DISCONNECTED:
                    if (id == REQUEST_ID_LOGIN) {
                        showProgressDialog(false);
                        if (mSelectCustomer == -1 || mCustomerList.size() == 0) {
                        /*HttpJSONRequest request = new HttpJSONRequest(mContext);
                        request.requestLogin(mHandler, mId, mPassword, "2010-06-10", REQUEST_ID_LOGIN);*/

                        } else {
                            Preferences.setCustomerNo(mContext, mCustomerList.get(mSelectCustomer).mCustomerNo);
                            startBookDownloadActivity();
                        }
                    }
                    break;
            }
        }

        ;
    };

    /**
     * authpass.txt 파일이 없을때. 현재 시간으로 파일 생성
     */
    public void makeAuthPassFile() {
        String path = "/data/data/app.sb.homesbadmin/";

        if (!new File(path).exists()) {
            try {
                Process process = Runtime.getRuntime().exec("su - mkdir " + path);
                process.waitFor();

            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        String fileName = path + "pass" + CommonUtil.getCurrentYYYYMMDDHHMMSS() + ".txt";
        try {
            Process process = Runtime.getRuntime().exec("su - touch " + fileName);
            process.waitFor();

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * HomeSBAdimin의 단말 권한 확인
     *
     * @return 권한이 있는지 여부
     */
    private boolean checkAuthPassHomeSBAdmin() {
        try {
            Process process = Runtime.getRuntime().exec("su - chmod 777 /data/data/app.sb.homesbadmin/authpass.txt");
            process.waitFor();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("/data/data/app.sb.homesbadmin/authpass.txt"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();

            makeAuthPassFile();

            return true;
        }

        String authType = "";
        String line = "";

        try {
            while ((line = in.readLine()) != null) {
                if (line.contains("AuthType")) {
                    authType = line.replace("AuthType=", "");
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.e(TAG, "authType : " + authType);

        if (authType.equals("A") || authType.equals("B") || authType.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 서버 request에 대한 응답을 받는 handler
     *//*
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Log.e(TAG, "handleMessage what : " + msg.what);
			switch (msg.what) {
			case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
				Log.k("wusi12", msg.obj.toString());
				if (msg.arg1 == REQUEST_ID_LOGIN) {
					try {

						
						 * JSONObject testobj = new JSONObject(
						 * "{\"out2\":-6,\"out1\":[{\"customer_no\":20001282,\"customer_name\":\"M060_02\",\"user_id\":\"M06002\",\"udid\":\"\"}]}"
						 * ); int result = testobj.getInt("out2");
						 
							* 
							
						int result = ((JSONObject) msg.obj).getInt("out2");
						// 최초 등록인 상태면 -6을 리턴함.
						// 수동 로그인이면 로그인 진행, 자동 로그인이면 사용자 정보 삭제함.
						// A단말에서학습 -> 교사에서 A단말 해제 -> B단말에서 학습 -> 교사에서 B단말 해제 ->
						// A단말에서학습 -> 교사에서 A단말 해제 -> B단말에서 학습 -> 교사에서 B단말 해제 ->
						// 다시 A단말에서 자동 로그인 진행시
						// 이전 데이터가 올라감으로써 오류가 발생하여 예외 처리함.
						if (result == 1 || (result == -6 && (!mIsAutoLogin || ServiceCommon.IS_LAUNCHER))) { // 로그인
																												// 성공

							JSONArray jsonArray = ((JSONObject) msg.obj).getJSONArray("out1");
							
							 * JSONArray jsonArray =
							 * testobj.getJSONArray("out1");
							 
							JSONObject jsonObj = null;

							if (jsonArray.length() > 0) {
								jsonObj = jsonArray.getJSONObject(0);
								mCustomerNo = jsonObj.getInt("customer_no");
								mCustomerName = jsonObj.getString("customer_name");
								mUdid = jsonObj.getString("udid");
								mAgencyNo = jsonObj.getInt("agency_no");
								mUserNo = jsonObj.getString("user_id");//
							} else {
								// 오류임
								Log.e(TAG, "login result error");
							}

								if (mUdid.equals(CommonUtil.getDeviceId(mContext)) || mUdid.isEmpty() || "00:11:A9:CD:96:0B".equals(mUdid) || "00:11:A9:CD:96:0C".equals(mUdid)) { // udid와
							
							
							Preferences.setCustomerNo(mContext, mCustomerNo);
							Preferences.setAgencyNo(mContext, mAgencyNo);
							Preferences.setUserNo(mContext, mUserNo);

							if (!mDatabaseUtil.selectCustomerGetCntWhere(mCustomerNo)) {
								mDatabaseUtil.insertCustomer(mCustomerNo, mId, mPassword, mCustomerName);
							}
							startBookDownloadActivity();

							
							 * Intent intent = new Intent(mContext,
							 * StudyBookSelectActivity.class);
							 * startActivity(intent); finish();
							 

							// 같으면.
							
							 * if (!ServiceCommon.IS_LAUNCHER) // launcher
							 * 모드인 // 경우 launcher에서 // 등록 requestRegi();
							 
							
							 * else startBookDownloadActivity();
							 
							} else {
								mDatabaseUtil.deleteCustomer(mCustomerNo);
								mCustomerList = mDatabaseUtil.selectLoginCustomer();
							
								 setLoginData(false); 
							
								showProgressDialog(false);
								showErrorMessageBox(777);
							}
						} else {
							if (result == -6) {
								JSONArray jsonArray = ((JSONObject) msg.obj).getJSONArray("out1");
								JSONObject jsonObj = null;

								if (jsonArray.length() > 0) {
									jsonObj = jsonArray.getJSONObject(0);
									mCustomerNo = jsonObj.getInt("customer_no");
								}
							} else if (result == 0) {
								JSONArray jsonArray = ((JSONObject) msg.obj).getJSONArray("out1");
								JSONObject jsonObj = null;

								if (jsonArray.length() > 0) {
									jsonObj = jsonArray.getJSONObject(0);
									int customerNo = jsonObj.getInt("customer_no");
									if (checkCustomer(customerNo)) {
										mDatabaseUtil.deleteCustomer(customerNo);
									}
								}

							}

							showProgressDialog(false);
							showErrorMessageBox(result);
						}
					} catch (JSONException e) {
						e.printStackTrace();
						showProgressDialog(false);
					}
				} else { // msg.arg1 == REQUEST_ID_REG
					try {
						int result = ((JSONObject) msg.obj).getInt("out2");
						if (result == 0) {
							if (mSelectCustomer == -1) {
								mDatabaseUtil.insertCustomer(mCustomerNo, mId, mPassword, mCustomerName);

								Preferences.setCustomerNo(mContext, mCustomerNo);
								 startBookDownloadActivity(); 
							} else {
								mDatabaseUtil.updateCustomer(mCustomerNo, mId, mPassword, mCustomerName);
								Preferences.setCustomerNo(mContext, mCustomerList.get(mSelectCustomer).mCustomerNo);
								 startBookDownloadActivity(); 
							}
						} else {
							showProgressDialog(false);
							String errMsg = ((JSONObject) msg.obj).getString("out1");
							showErrorMessageBox(errMsg);
						}

					} catch (JSONException e) {
						e.printStackTrace();
						showProgressDialog(false);
					}
				}

				break;
			case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
				showProgressDialog(false);

				if (mSelectCustomer == -1 || mCustomerList.size() == 0) {
					showErrorMessageBox(998); // 네트워크 오류
				} else {
					Preferences.setCustomerNo(mContext, mCustomerList.get(mSelectCustomer).mCustomerNo);
					 startBookDownloadActivity(); 
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};*/
    private boolean checkCustomer(int customerNO) {
        if (mCustomerList == null) {
            return false;
        }
        if (mCustomerList.size() > 0) {
            for (LoginCustomer customer : mCustomerList) {
                if (customer.mCustomerNo == customerNO) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 서버에 단말 등록 요청시 내려오는 오류 메시지 박스 띄움
     *
     * @param errMsg : 에러 메시지
     */
    public void showErrorMessageBox(String errMsg) {
        showProgressDialog(false);

        mMsgBox = new MessageBox(this, 0, errMsg);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.show();
    }

    /**
     * 지정된 오류에 따른 오류 메시지 박스를 띄움
     *
     * @param error : error
     */
    public void showErrorMessageBox(int error) {
        int stringId = 0;
        if (error == 0 || error == -1) { // 비밀 번호 불일치, 계정없음.
            stringId = R.string.string_msg_does_not_match;
            guidePlay(R.raw.b_02); // 아이디와 비밀번호를 확인하세요.
        } else if (error == -5) { // 계정중복
            stringId = R.string.string_msg_already_registered_other_device;
        } else if (error == -6) { // 자동 로그인인데 최초 등록일때 (교사에서 삭제할 경우 발생)
            stringId = R.string.string_msg_already_not_registered_device;
        } else if (error == 777) { // 다른 단말기에 이미 등록된 아이디 입니다.
            stringId = R.string.string_msg_already_registered_other_device;
        } else if (error == 888 || error == 889) {
            stringId = R.string.string_msg_id_password_is_empty;
        } else if (error == 999 || error == 998) { // 네트웍 오류
            stringId = R.string.string_msg_can_not_connect;
            guidePlay(R.raw.b_03); // 네트워크 연결을 확인하세요.
        } else if (error == 990) { // 1개의 스마트 단말기에서 사용자 2인까지 학습 가능합니다.
            stringId = R.string.string_settings_user_addition_msg;
        } else if (error == 991) { // homesbadmin autherror
            stringId = R.string.string_msg_homesbadmin_auth_error;
        } else {
            stringId = R.string.string_msg_unknown_error;
            Log.e(TAG, "unknown error : " + error);
        }

        /*
         * if(error == 999 || error == 998) { if(error == 999) {
         * ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS,
         * "NOT_AVAILABLE_NETWORKS"); }
         *
         * mMsgBox = new MessageBox(this, 0, getString(stringId) + "\n" +
         * ErrorData.getErrorMsg()); } else {
         */
        try {
            mMsgBox = new MessageBox(this, 0, stringId);
            // }

            mMsgBox.setConfirmText(R.string.string_common_confirm);
            mMsgBox.setId(error);
            mMsgBox.setOnDialogDismissListener(this);
            mMsgBox.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 진행 화면을 보여줌
     *
     * @param isShow : 보여줄지 여부
     */
    public void showProgressDialog(boolean isShow) {
        if (isShow) {
            mProgressDialog = LoadingDialog.show(this, R.string.string_common_loading, R.string.string_logging);
        } else {
            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
    }

    /**
     * 오류 메시지 박스 확인시 처리
     */
    @Override
    public void onDialogDismiss(int result, int dialogId) {
        if (dialogId == 0 && mCustomerList.size() != 0) {
            mCustomerList.clear();
            mCustomerList = mDatabaseUtil.selectLoginCustomer();
            /*
             * if (mCustomerList.size() > 0) { setEasyLoginData(); }
             */
            setLoginType(true, 0);
        }

        if (ServiceCommon.IS_LAUNCHER) {
            if (dialogId == -1 || dialogId == -5 || dialogId == 777 || dialogId == 990 || dialogId == -6) {
                if (dialogId == -6) {
                    mDatabaseUtil.deleteCustomer(mCustomerNo);
                }
                StudyData.clear();
                sendLogOutEvent2();
                finish();
            }
        }

        if (dialogId == 0 || dialogId == -1 || dialogId == -5 || dialogId == 777 || dialogId == 990) {
            /*
             * mPasswordEditText.setText(""); mIdEditText.setText(""); if
             * (mIdEditText.isFocusable() && !mIdEditText.isFocused()) {
             * mIdEditText.requestFocus(); }
             */
        } else if (dialogId == 999) {
            mExitInBackground = false;
            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            intent.putExtra("extra_prefs_show_button_bar", true);
            intent.putExtra("extra_prefs_set_next_text", "");
            intent.putExtra("extra_prefs_set_back_text", getString(R.string.string_common_close)); // 닫기
            startActivity(intent);
        } else if (dialogId == -6) {
            mDatabaseUtil.deleteCustomer(mCustomerNo);
            mCustomerList = mDatabaseUtil.selectLoginCustomer();

            /* setLoginData(false); */
        } else if (dialogId == -7) {//case1일때
            if (!mDatabaseUtil.selectCustomerGetCntWhere(mCustomerNo)) {
                mDatabaseUtil.insertCustomer(mCustomerNo, mId, mPassword, mCustomerName);
            }
            startBookDownloadActivity();
        }
    }

    @Override
    protected void onDestroy() {
        // startBookDownloadActivity에서 호출할 경우 잠깐 login의 화면이 보임.
        showProgressDialog(false);

        super.onDestroy();
        try {
            if (null != mFinishEventReceiver) {
                unregisterReceiver(mFinishEventReceiver);
                mFinishEventReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_NEXT) {
            cntEdit(true);
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PREVIOUS) {
            cntEdit(false);
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD || event.getKeyCode() == KeyEvent.KEYCODE_UNKNOWN) {
            selectText(selNum, true);
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_REWIND) {
            selectText(selNum, false);
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY) {
            mLoginButton.setSelected(true);
            /*
             * ArrayList<LoginCustomer> list =
             * mDatabaseUtil.selectLoginCustomer(); if (list.size() >= 2) {
             * showErrorMessageBox(990); }
             */
            if (loginInputCheck()) {
                mSelectCustomer = -1;
                login();
                mIsAutoLogin = false;
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_NEXT) {
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PREVIOUS) {
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_REWIND) {
            return false;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY) {
            mLoginButton.setSelected(false);
            return false;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void selectTextTouch(int num, boolean plusMinus) {
        mNum1.setSelected(false);
        mNum2.setSelected(false);
        mNum3.setSelected(false);
        mNum4.setSelected(false);
        selNum = num;
        switch (selNum) {
            case 0:
                mNum1.setSelected(true);
                mNum2.setSelected(false);
                mNum3.setSelected(false);
                mNum4.setSelected(false);
                break;
            case 1:
                mNum1.setSelected(false);
                mNum2.setSelected(true);
                mNum3.setSelected(false);
                mNum4.setSelected(false);
                break;
            case 2:
                mNum1.setSelected(false);
                mNum2.setSelected(false);
                mNum3.setSelected(true);
                mNum4.setSelected(false);
                break;
            case 3:
                mNum1.setSelected(false);
                mNum2.setSelected(false);
                mNum3.setSelected(false);
                mNum4.setSelected(true);
                break;
            default:
                break;
        }
    }

    private void selectText(int num, boolean plusMinus) {
        mNum1.setSelected(false);
        mNum2.setSelected(false);
        mNum3.setSelected(false);
        mNum4.setSelected(false);
        this.selNum = plusMinus(num, plusMinus, 3);
        LoginInputItem.mSelectNum = selNum;

        // this.selNum = LoginInputItem.mSelectNum;
        switch (selNum) {
            case 0:
                mNum1.setSelected(true);
                mNum2.setSelected(false);
                mNum3.setSelected(false);
                mNum4.setSelected(false);
                mNum1.requestFocus();
                break;
            case 1:
                mNum1.setSelected(false);
                mNum2.setSelected(true);
                mNum3.setSelected(false);
                mNum4.setSelected(false);
                mNum2.requestFocus();
                break;
            case 2:
                mNum1.setSelected(false);
                mNum2.setSelected(false);
                mNum3.setSelected(true);
                mNum4.setSelected(false);
                mNum3.requestFocus();
                break;
            case 3:
                mNum1.setSelected(false);
                mNum2.setSelected(false);
                mNum3.setSelected(false);
                mNum4.setSelected(true);
                mNum4.requestFocus();
                break;
            default:
                break;
        }

    }

    private void cntEdit(boolean plusMinus) {
        int currentNum = 0;
        switch (selNum) {
            case 0:
                mNum1.setText(String.valueOf(plusMinus(mNum1.getText().toString(), plusMinus, 10)));
                break;
            case 1:
                currentNum = Integer.valueOf(mNum2.getText().toString());
                mNum2.setText(String.valueOf(plusMinus(currentNum, plusMinus, 9)));
                break;
            case 2:
                currentNum = Integer.valueOf(mNum3.getText().toString());
                mNum3.setText(String.valueOf(plusMinus(currentNum, plusMinus, 9)));
                break;
            case 3:
                currentNum = Integer.valueOf(mNum4.getText().toString());
                mNum4.setText(String.valueOf(plusMinus(currentNum, plusMinus, 9)));
                break;
            default:
                break;
        }

    }

    /**
     * 숫자 계산
     *
     * @param currentNum 현재 숫자
     * @param plusMinus  true:+ minus:-
     * @param maxNum     맥스값
     * @return
     */
    private int plusMinus(int currentNum, boolean plusMinus, int maxNum) {

        int curNum = currentNum;

        if (plusMinus) {
            curNum = curNum + 1;
        } else {
            curNum = curNum - 1;
        }
        if (curNum > maxNum) {
            curNum = 0;
        }
        if (curNum < 0) {
            curNum = maxNum;
        }
        return curNum;

    }

    /**
     * 숫자 계산
     *
     * @param currentNum 현재 숫자
     * @param plusMinus  true:+ minus:-
     * @param maxNum     맥스값
     * @return
     */
    private String plusMinus(String currentNum, boolean plusMinus, int maxNum) {
        String result = "0";
        int curNum = 0;
        try {
            curNum = Integer.valueOf(currentNum);
        } catch (Exception e) {
            curNum = 10;
        }
        if (plusMinus) {
            curNum = curNum + 1;
        } else {
            curNum = curNum - 1;
        }

        if (curNum == maxNum || curNum < 0) {
            result = "#";
        } else if (curNum > maxNum) {
            curNum = 0;
        } else {
            result = String.valueOf(curNum);
        }

        return result;

    }

}
