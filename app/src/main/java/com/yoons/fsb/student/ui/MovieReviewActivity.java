package com.yoons.fsb.student.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomVideoPlayer;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.base.BaseStudyActivity;
import com.yoons.fsb.student.ui.control.StepStatusBar;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.StudyDataUtil;

public class MovieReviewActivity extends BaseStudyActivity implements OnDialogDismissListener {

	private StepStatusBar mStepStatusBar = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.movie_review_main);

		StudyDataUtil.setCurrentStudyStatus(this, "S61");

		mStudyData = StudyData.getInstance();

		mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);
		mStepStatusBar.setHighlights(StepStatusBar.STATUS_MOVIE_REVIEW);
		setTitlebarCategory(getString(R.string.string_titlebar_category_study));
		setTitlebarText(mStudyData.mProductName);
		// WiFi 감도 아이콘 설정
		setPreferencesCallback();

		startMovieReview();
		Crashlytics.log(getString(R.string.string_ga_MovieReviewActivity));

	}

	@Override
	protected void onPause() {
		// 학습 중 화면 잠금, 꺼짐 시 간지로 재시작 
		if (!isFinishing()) {
			if (mIsReStart) {
				startActivity(new Intent(this, MovieReviewTitlePaperActivity.class)
						.putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
			}

			setFinish();
			finish();
		}

		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);

		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
			if (null != mVideoPlayer && mVideoPlayer.isPrepared())
				mVideoPlayer.setPlayPressed(true);
			break;

		default:
			return super.onKeyDown(keyCode, event);
		}

		return false;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);

		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
			if (null != mVideoPlayer && mVideoPlayer.isPrepared()) {
				mVideoPlayer.setPlayPressed(false);
				if (mVideoPlayer.isPlaying())
					mVideoPlayer.pauseBtn();
				else
					mVideoPlayer.startBtn();
			}
			break;

		default:
			return super.onKeyUp(keyCode, event);
		}

		return false;
	}

	private void setVideoPlayer() {
//      로컬 파일 재생 방식에서 url로 변경됨 2013.11.4 
//		mStudyData.mMovieFile 형태 
//		"http://vod.yoons.gscdn.com:8080/Anitalk/001_1AB_High.mp4";

		String path = mStudyData.mMovieFile;

		Log.i("", "MovieReview path => " + path);

		mVideoPlayer = new CustomVideoPlayer(this, mHandler, path, true, true);
		((LinearLayout) findViewById(R.id.movie_review_layout)).addView(mVideoPlayer);
	}

	private void startMovieReview() {
		setVideoPlayer();
		findViewById(R.id.movie_review_layout).setVisibility(View.VISIBLE);
	}

	private void endMovieReview() {
		goNextStatus();
	}

	private void goNextStatus() {
		if (isNext) {
			isNext = false;
			if (StudyData.getInstance().mOneWeekData.size() > 0) {
				startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
			} else
				startActivity(new Intent(this, StudyOutcomeActivity.class));

			StudyDataUtil.setCurrentStudyStatus(this, "S62");
			finish();
		}
	}

	private void showMovieReviewConfirmDialog() {
		if (null == mContext)
			return;

		mMsgBox = new MessageBox(this, 0, R.string.string_msg_movie_review_confirm);
		mMsgBox.setConfirmText(R.string.string_common_retry);
		mMsgBox.setCancelText(R.string.string_common_ok);
		mMsgBox.setOnDialogDismissListener(mVideoPlayer.mDialogDismissListener);
		mMsgBox.show();
	}

	private void showwMovieReviewNetworkErrorDialog(int errorWhat, int errorExtra) {
		if (null == mContext)
			return;

		String errorMsg = getString(R.string.string_msg_network_error_video_end);
		if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_IO)
			errorMsg += "\nMEDIA_ERROR_IO";
		else if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_TIMED_OUT)
			errorMsg += "\nMEDIA_ERROR_TIMED_OUT";
		else if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_UNSUPPORTED)
			errorMsg = "\nMEDIA_ERROR_UNSUPPORTED";
		else if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_MALFORMED)
			errorMsg += "\nMEDIA_ERROR_MALFORMED";
		else
			errorMsg += "\n" + errorExtra;

		String error = String.format("[ %d ] %s", errorWhat, errorMsg);
		mMsgBox = new MessageBox(this, 0, error);
		mMsgBox.setConfirmText(R.string.string_common_confirm);
		mMsgBox.setOnDialogDismissListener(this);
		mMsgBox.show();
	}

	@Override
	public void onDialogDismiss(int result, int dialogId) {
		endMovieReview();
	}

	/**
	 * Handler
	 */
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (null == mContext)
				return;

			switch (msg.what) {
			case ServiceCommon.MSG_WHAT_VIDEO_PLAYER:
				if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_CONFIRM)
					showMovieReviewConfirmDialog();
				else if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_COMPLETION)
					endMovieReview();
				break;

			case ServiceCommon.MSG_WHAT_VIDEO_PLAYER_ERROR:
				showwMovieReviewNetworkErrorDialog(msg.arg1, msg.arg2);
				break;

			default:
				super.handleMessage(msg);
			}
		}
	};
}
