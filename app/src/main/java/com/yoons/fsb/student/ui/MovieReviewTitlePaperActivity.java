package com.yoons.fsb.student.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ui.base.TitlePaperBaseActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

public class MovieReviewTitlePaperActivity extends TitlePaperBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) { // igse
            setContentView(R.layout.igse_movie_review_title_paper);
            findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.igse_btn_color_01));
        } else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) { // 우영
                setContentView(R.layout.w_movie_review_title_paper);
                findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.w_btn_color_01));
            } else { // 숲
                setContentView(R.layout.f_movie_review_title_paper);
                findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.f_btn_color_01));
            }
        }

        initTitlePaper(TP_TYPE_MOVIE_REVIEW);

        Crashlytics.log(getString(R.string.string_ga_MovieReviewTitlePaperActivity));

    }
}
