package com.yoons.fsb.student.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.PeriodicSync;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.TagEventData;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.db.DatabaseData;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.RequestPhpGet;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.network.RequestQTETPhpPost;
import com.yoons.fsb.student.network.RequestQUSTPhpPost;
import com.yoons.fsb.student.service.AgentEvent;
import com.yoons.fsb.student.service.DownloadService;
import com.yoons.fsb.student.service.DownloadServiceAgent;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.data.StudyBookSelectListItem;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.popup.ShowPopup;
import com.yoons.fsb.student.ui.sec.SecIntroActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ContentsUtil;
import com.yoons.fsb.student.util.ImageDownloader;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.util.YdaConverter;
import com.yoons.fsb.student.vanishing.VanishingTitlePaperActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

public class NewHomeActivity extends BaseActivity implements BaseDialog.OnDialogDismissListener {
    private static final String TAG = "[NewHomeActivity]";

    private static final int MSGBOX_ID_STOPPED_STUDY_NOTI = 1;    // 중단학습 메시지 박스 id
    private static final int MSGBOX_ID_UPDOWN_REFRESH = 2;        // 새로 고침 메시지 박스 id
    private static final int MSGBOX_ID_UPDOWN_COMPLETE = 3;    // 새로 고침 성공 메시지 박스 id
    private static final int MSGBOX_ID_BOOK_SELECT = 4;        // 새로 고침 성공 메시지 박스 id
    private static final int MSGBOX_ID_UPDATE = 7;
    private static final int MSGBOX_ID_SEC_ERROR = 8;
    private static final int MSGBOX_ID_CONTENT_ERROR = 9;
    private static final int REQUEST_ID_VERSION = 104;

    private ImageView mIvMondayCheck = null;
    private ImageView mIvTuedayCheck = null;
    private ImageView mIvWendayCheck = null;
    private ImageView mIvThudayCheck = null;
    private ImageView mIvFridayCheck = null;
    private ImageView mIvSatdayCheck = null;
    private ImageView mIvSundayCheck = null;

    private RelativeLayout mStudyLayout = null;
    private RelativeLayout mNoStudyLayout = null;
    private LinearLayout mLayoutMonday = null;
    private LinearLayout mLayoutTuesday = null;
    private LinearLayout mLayoutWendsday = null;
    private LinearLayout mLayoutThursday = null;
    private LinearLayout mLayoutFriday = null;
    private LinearLayout mLayoutSaturday = null;
    private LinearLayout mLayoutSunday = null;

    private LinearLayout mCircle = null;
    private LinearLayout mCount = null;

    private LinearLayout mLayoutHome = null;
    private LinearLayout mLayoutNotify = null;

    private DatabaseUtil mDatabaseUtil = null;
    private ArrayList<DatabaseData.TodayStudyUnit> mStudyUnit = null;           // 오늘 학습 교재 정보
    private ArrayList<DatabaseData.WeekCalendarUnit> mWeekCalendarUnit = null;  // 주간 달력 정보
    private ArrayList<DatabaseData.LoginCustomer> mCustomerList = null;         // db상에 있는 회원 list
    private ArrayList<StudyBookSelectListItem.ItemInfo> mInfoList = new ArrayList<StudyBookSelectListItem.ItemInfo>();

    private DatabaseData.LoginCustomer mCustomer = null;
    //private DatabaseData.CustomerConfig mConfig = null;

    private static final String[] mWeek = {"일", "월", "화", "수", "목", "금", "토"};
    private JSONArray mWeekStudyResult = null;
    private ArrayList<Integer> mWeekStudyR = null;

    private TextView mTvStudentName = null, mTvSeriesName = null, mTvStudyUnitName = null;
    private TextView mTvMon = null, mTvTue = null, mTvWed = null, mTvThu = null, mTvFri = null, mTvSat = null, mTvSun = null;
    private TextView mTvMonText = null, mTvTueText = null, mTvWedText = null, mTvThuText = null, mTvFriText = null, mTvSatText = null, mTvSunText = null;
    private TextView mTvSpeakingbusCnt = null, mTvProtfolioCnt = null, mTvLibraryCnt = null;
    private TextView mSeriesName = null, mStudyUnitName = null;
    private TextView mLetsStart = null;

    private TextView mSelect, mTotal = null;

    private ImageView mIvLeftButton = null;
    private ImageView mIvRighButton = null;

    private ImageView mIvNavigatorFirst = null;
    private ImageView mIvNavigatorSecond = null;
    private ImageView mIvNavigatorThird = null;
    private ImageView mIvNavigatorFourth = null;
    private ImageView mIvNavigatorFifth = null;

    private CardView mSkillCardView = null;
    private CardView mSecStudyCardView = null;

    private LoadingDialog mProgressDialog = null;
    private boolean mIsStudyStopped = false;                // 중단된 학습 인지 여부
    private int mStudyStoppedIndex = 0;                    // 중단된 학습 index
    private YdaConverter mYdaConvert = null;                // yda->ogg converter
    private DownloadServiceAgent mServiceAgent = null;        // 다운로드 서비스 agent
    private int mDownloadingUnitPos = -1;                    // 다운로드 진행중인 교재 position
    private int mUnitDownloadedCnt = 0;                    // 다운로드 받는 count
    private int mUnitTotalCnt = 0;                            // 다운로드 받아야할 총 count
    private boolean mIsUnitDownload = false;                // 교재 다운로드 중인지 여부
    private ImageDownloader mImageDownloader = null;

    private ArrayList<StudyData.OneWeekData> tempOneWeekData = null;

    private boolean mIsNextPackage = false;                                     // 파닉스, 말하기 패키지 모두 선택된 경우 false :처음 유닛 true :다음 유닛

    private ImageView mTodayBadge = null;
    private TextView mTodayBadgeText = null;

    private ImageView mCoverThum = null;

    private int mSelectIndex = 0;

    private boolean isStudyEnd = false;
    private boolean mIsSecExist = false;

    private static final int REQUESTCODE_UPLOAD = 128;    // upload requestcode
    private static final int REQUESTCODE_GUIDE = 129;

    private RequestQTETPhpPost reqQTET;
    private RequestPhpPost reqQinf;
    private int mSecCurCnt = 0, mSecMaxCnt = 0;

    private Map<String, String> qGetMap = null;

    private boolean mExtraDownload = false;

    /**
     * 다운로드 서비스 connection
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DownloadService.LocalBinder binder = (DownloadService.LocalBinder) service;
            mServiceAgent = binder.getServiceHandler();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceAgent = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // Igse
            setContentView(R.layout.igse_new_home);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                setContentView(R.layout.w_new_home);
            else // 숲
                setContentView(R.layout.f_new_home);
        }

        overridePendingTransition(0, 0);

        initData();

        mDatabaseUtil = DatabaseUtil.getInstance(this);
        mCustomerList = mDatabaseUtil.selectLoginCustomer();

        if (mCustomerList.size() > 0) {
            mCustomer = mCustomerList.get(0);
            ServiceCommon.CUSTOMER_NO = mCustomer.mCustomerNo;
        }

        mImageDownloader = new

                ImageDownloader(this);
        mImageDownloader.setOnDownloadComplete(new ImageDownloader.DownloadCompleteListener() {
            @Override
            public void onDownloadComplete(ImageView imageView, Bitmap bitmap) {
                try {
                    if (bitmap == null) {
                        imageView.setImageResource(R.drawable.book_thumbnail_default);
                    } else {
                        imageView.setImageBitmap(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        if (Preferences.getShowGuide(mContext)) {
            ShowPopup.getInstance().ShowMainTutorial(this);
        }

        // 서비스 바인딩.
        if (mServiceAgent == null) {
            bindService(new Intent(this, DownloadService.class), serviceConnection, Context.BIND_AUTO_CREATE);
        }

        if (CommonUtil.isCenter()) {
            ArrayList<String> params = new ArrayList<String>();
            params.add(Preferences.getForestCode(mContext));
            params.add(String.valueOf(ServiceCommon.CUSTOMER_NO));
            HttpJSONRequest request = new HttpJSONRequest(mContext);
            request.requestWeekStudyResullt(mWeekInfoHandler, (String[]) params.toArray(new String[0]));
        } else {
            RequestPhpGet req = new RequestPhpGet(mContext) {
                protected void onPostExecute(java.util.Map<String, String> result) {
                    Object[] vals = result.values().toArray();
                    if (vals != null) {
                        mWeekStudyR = new ArrayList<Integer>();
                        for (int i = 1; i < vals.length; i++) {
                            String val = vals[i].toString();
                            mWeekStudyR.add(Integer.valueOf(val));
                        }
                    }
                    mWeekInfoHandler.sendEmptyMessage(44433);
                }
            };

            req.getWeekStudyResult();
        }

        Crashlytics.log("홈 화면");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("REFRESH")) {
            isStudyEnd = true;
        }

        if (isStudyEnd) {
            downloadList();
        } else {
            startHomeSelect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mDownloadReceiver, new IntentFilter(AgentEvent.ACTION_RECEIVE_AGENT_EVENT));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDownloadReceiver != null) {
            unregisterReceiver(mDownloadReceiver);
        }
    }

    private void startHomeSelect() {
        init();
        if (CommonUtil.isCenter())
            sendQGet();
        else {
            if (!"1".equals(Preferences.getLmsStatus(this))) {
                sendQGet();
            }
        }
    }

    private void downloadList() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            if (DatabaseUtil.getInstance(mContext).isFailStudyLogCnt()) {
                HttpJSONRequest request = new HttpJSONRequest(mContext);
                request.requestBackupStudyStatus(mBackupHandler);
            } else {
                if (mProgressDialog == null) {
                    mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_book_download_progress1);
                }
                DatabaseSync databaseSync = new DatabaseSync(this);
                databaseSync.downloadList(mHandler, Preferences.getCustomerNo(this));
            }
        } else {
            showDownloadNetworkErrorPopup();
        }
    }

    private Handler mWeekInfoHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.obj != null) {
                        try {
                            JSONObject jbo = (JSONObject) msg.obj;
                            JSONArray result = jbo.getJSONArray("out2");
                            int reultNo = result.getJSONObject(0).getInt("RESULT");
                            if (reultNo == 0) {
                                JSONArray jarr = jbo.getJSONArray("out1");
                                mWeekStudyResult = jarr;
                            }

                            mWeekInfoHandler.sendEmptyMessage(44433);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    break;

                case 44433:
                    Calendar oCalendar = Calendar.getInstance();
                    int nTodayCnt = oCalendar.get(Calendar.DAY_OF_WEEK);

                    checkTodayWhite(oCalendar.get(Calendar.DAY_OF_WEEK));

                    checkRoundToday(mWeek[oCalendar.get(Calendar.DAY_OF_WEEK) - 1]);

                    setCalendar();
                    break;
            }
        }
    };

    private Handler mBackupHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            com.yoons.fsb.student.util.Log.e(TAG, "handleMessage what : " + msg.what);
            HttpJSONRequest request = new HttpJSONRequest(mContext);
            HttpJSONRequest.mIsStatusRequest = false;

            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (HttpJSONRequest.mStatusQueue.size() != 0) {
                        request.requestBackupStatus(mBackupHandler);
                    } else {
                        downloadList();
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    HttpJSONRequest.mIsStatusRequest = false;
                    request.backupStatus();
                    showDownloadNetworkErrorPopup();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private void showExit() {
        mMsgBox = new MessageBox(mContext, 0, R.string.string_msg_close_app);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setOnDialogDismissListener(new BaseDialog.OnDialogDismissListener() {

            @Override
            public void onDialogDismiss(int result, int dialogId) {
                if (BaseDialog.DIALOG_CONFIRM == result) {
                    StudyData data = StudyData.getInstance();
                    //학습앱 종료
                    Crashlytics.log(mContext.getString(R.string.string_common_close));
                    RequestPhpPost reqFCASE = new RequestPhpPost(mContext);
                    reqFCASE.sendCASE(String.valueOf(data.mStudyResultNo), "EX3");
                    ((Activity) mContext).finish();
                }
            }
        });
        if (!mMsgBox.isShowing()) {
            mMsgBox.show();
        }
    }

    /**
     * 선택된 교재 다운로드 할지 확인
     *
     * @param position : 선택된 교재 position
     */
    private void startContentsDownload(int position) {
        try {
            mIsUnitDownload = true;
            StudyBookSelectListItem.ItemInfo info = mInfoList.get(position);
            DatabaseData.TodayStudyUnit unit = mDatabaseUtil.selectNewTodayStudyUnit(Preferences.getCustomerNo(mContext), info.mProductNo, info.mStudyUnitCode);
            int fileCnt[] = new int[2];
            int reviewFileCnt[] = new int[2];
            int percent = ContentsUtil.getUnitContentDownlaodPercent(mContext, unit.mProductNo, unit.mSeriesNo, unit.mBookNo, unit.mRealBookNo, unit.mStudyUnitNo, unit.mStudyUnitCode, fileCnt, false);
            int reviewPercent = 100;
            DatabaseData.StudyUnit review = mDatabaseUtil.selectRecentNoReviewUnit(Preferences.getCustomerNo(mContext));
            //StudyUnit review = StudyData.getmStudyUnit().get(position);

            //구교재정보
            unit.mIsOlndAnswer = isOldAnswer(unit.mSeriesNo);

            if (review.mProductNo != 0 && !unit.mReviewStudyStatus.equals("RFN")) {
                reviewPercent = ContentsUtil.getUnitContentDownlaodPercent(mContext, review.mProductNo, review.mSeriesNo, review.mBookNo, review.mRealBookNo, review.mStudyUnitNo, review.mStudyUnitCode, reviewFileCnt, true);
            }

            if ((percent == 100) && reviewPercent == 100) { // 백그라운드에서 완료가 됐을 수도 있으므로.
                makeStudyData(position);
                return;
            }

            // 다운로드가 완료된 차시일지라도 이전학습 복습을 위해 위의 사항을 체크해야함.
            if (!CommonUtil.isAvailableNetwork(mContext, false)) {
                ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT_AVAILABLE_NETWORKS");
                showDownloadNetworkErrorPopup();
                return;
            }

            mUnitTotalCnt = fileCnt[0] + reviewFileCnt[0];

            mUnitDownloadedCnt = fileCnt[1] + reviewFileCnt[1];
            setDownloadItemInfo(position);
            mDatabaseUtil.updateDownloadContentNoPriority();

            //일일/주간평가

            if (tempOneWeekData != null) {
                int i = 1;//순서 데이터따로없음
                for (StudyData.OneWeekData owData : tempOneWeekData) {
                    if (owData.question_type.equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_2)) {
                        owData.audio_path = ContentsUtil.getOneWeekFilePath(unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, i, true);
                        owData.audio_path2 = ContentsUtil.getOneWeekFilePath(unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, i, false);
                    }
                    i++;
                }
            }

            if (unit.mProductNo != 0) {

                DatabaseData.DownloadStudyUnit downloadUnit = new DatabaseData.DownloadStudyUnit();
                downloadUnit.mProductNo = unit.mProductNo;
                downloadUnit.mSeriesNo = unit.mSeriesNo;
                downloadUnit.mBookNo = unit.mBookNo;
                downloadUnit.mRealBookNo = unit.mRealBookNo;
                downloadUnit.mStudyUnitNo = unit.mStudyUnitNo;
                downloadUnit.mStudyUnitCode = unit.mStudyUnitCode;
                downloadUnit.mDictationCnt = mDatabaseUtil.selectDictationCount(unit.mProductNo, unit.mStudyUnitCode);
                downloadUnit.mIsCdnBFile = unit.mIsCdnBFile;
                downloadUnit.mWordQuestionList = mDatabaseUtil.selectWordQuestionWordList(unit.mProductNo, unit.mStudyUnitCode);
                downloadUnit.mSentenceQuestionCnt = mDatabaseUtil.selectSentenceQuestionCount(unit.mProductNo, unit.mStudyUnitCode);
                downloadUnit.mVanishingQuestionCnt = mDatabaseUtil.selectVanishingQuestionCount(unit.mProductNo, unit.mStudyUnitCode);
                downloadUnit.mAniTalkCnt = mDatabaseUtil.selectAniTalkCount(unit.mProductNo, unit.mStudyUnitCode);
                downloadUnit.mIsMovie = unit.mIsMovie;
                downloadUnit.mIsPriority = true;
                downloadUnit.mIsCdnVideoFile = unit.mIsCdnVideoFile;
                downloadUnit.mIsCdnAudio = unit.mIsCdnAudio;
                downloadUnit.mIsCdnZip = unit.mIsCdnZip;
                downloadUnit.mOneWeekList = tempOneWeekData;
                downloadUnit.mIsOlndAnswer = unit.mIsOlndAnswer;

                insertDownloadContentList(downloadUnit);
            }
            if (review.mProductNo != 0 && !unit.mReviewStudyStatus.equals("RFN")) {
                DatabaseData.DownloadStudyUnit reviewDownloadUnit = new DatabaseData.DownloadStudyUnit();
                reviewDownloadUnit.mProductNo = review.mProductNo;
                reviewDownloadUnit.mSeriesNo = review.mSeriesNo;
                reviewDownloadUnit.mBookNo = review.mBookNo;
                reviewDownloadUnit.mStudyUnitNo = review.mStudyUnitNo;
                reviewDownloadUnit.mStudyUnitCode = review.mStudyUnitCode;
                reviewDownloadUnit.mDictationCnt = mDatabaseUtil.selectDictationCount(review.mProductNo, review.mStudyUnitCode);
                reviewDownloadUnit.mIsCdnBFile = review.mIsCdnBFile;
                reviewDownloadUnit.mWordQuestionList = mDatabaseUtil.selectWordQuestionWordList(review.mProductNo, review.mStudyUnitCode);
                reviewDownloadUnit.mSentenceQuestionCnt = mDatabaseUtil.selectSentenceQuestionCount(review.mProductNo, review.mStudyUnitCode);
                reviewDownloadUnit.mVanishingQuestionCnt = mDatabaseUtil.selectVanishingQuestionCount(review.mProductNo, review.mStudyUnitCode);
                reviewDownloadUnit.mAniTalkCnt = mDatabaseUtil.selectAniTalkCount(review.mProductNo, review.mStudyUnitCode);
                reviewDownloadUnit.mIsMovie = review.mIsMovie;
                reviewDownloadUnit.mIsPriority = true;
                reviewDownloadUnit.mIsCdnVideoFile = review.mIsCdnVideoFile;

                insertDownloadContentList(reviewDownloadUnit);
            }

            mDownloadingUnitPos = position;
            DownloadServiceAgent.RequestUpdate(mContext);
        } catch (Exception e) {
            e.printStackTrace();
            showContentErrorDialog();
        }
    }

    /**
     * 다운로드 content리스트에 추가함(db에 추가)
     *
     * @param downloadUnit : 다운로드할 차시 정보
     */
    private void insertDownloadContentList(DatabaseData.DownloadStudyUnit downloadUnit) throws Exception {
        Map<String, String> list = ContentsUtil.getDownloadContentList(downloadUnit);

        Iterator it = list.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            mDatabaseUtil.insertDownloadContent(downloadUnit.mSeriesNo * 1000 + downloadUnit.mBookNo * 100 + downloadUnit.mStudyUnitNo, (String) pairs.getKey(), (String) pairs.getValue(), downloadUnit.mIsPriority);
        }

        // 다운로드 받아야할 페이지 리스트 생성
        list = ContentsUtil.getDownloadPageList(mContext, downloadUnit, downloadUnit.mRealBookNo);
        if (!list.isEmpty()) {
            it = list.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                mDatabaseUtil.insertDownloadContent(downloadUnit.mSeriesNo * 1000 + downloadUnit.mRealBookNo * 100, (String) pairs.getKey(), (String) pairs.getValue(), downloadUnit.mIsPriority);
            }
        }
    }

    /**
     * 학습 데이터 생성
     *
     * @param index : 리스트상 선택된 index
     */
    public void makeStudyData(int index) {

        StudyBookSelectListItem.ItemInfo itemInfo = mInfoList.get(index);
        DatabaseData.StudyUnit review = mDatabaseUtil.selectRecentNoReviewUnit(Preferences.getCustomerNo(mContext));

        StudyData.clear(); // 클리어함.

        StudyData data = StudyData.getInstance();

        int customerNo = Preferences.getCustomerNo(mContext);

        // 학습 옵션
        DatabaseData.StudyOption option = mDatabaseUtil.selectStudyOption(customerNo);
        data.mIsAutoArtt = option.mIsAutoArtt;
        data.mIsCaption = option.mIsCaption;
        data.mIsStudyGuide = option.mIsStudyGuide;

        data.mIsFastRewind = option.mIsFastRewind;
        data.mIsDictation = option.mIsDictation;
        data.mRTagRepeatCnt = option.mRepeatCnt;
        data.mIsAudioRecordUpload = option.isRecordUpload;
        data.mIsParagraph = option.isParagraph;
        data.mIsVIPSkip = option.isVIPSkip;
        data.mIsTextKorean = option.isTextKorean;
        data.mIsDicPirvate = option.mIsDicPirvate;
        data.mIsDicAnswer = option.mIsDicAnswer;

        //		mStudyUnit = mDatabaseUtil.selectTodayStudyUnit(Preferences.getCustomerNo(this), true);

        // 본학습 data
        data.mCustomerNo = customerNo;
        //		TodayStudyUnit selectUnit = mStudyUnit.get(itemInfo.mStudyUnitIndex);
        DatabaseData.TodayStudyUnit selectUnit = mDatabaseUtil.selectNewTodayStudyUnit(data.mCustomerNo, itemInfo.mProductNo, itemInfo.mStudyUnitCode);
        data.mStudyPlanYmd = selectUnit.mStudyPlanYmd;
        data.mRecordFileYmd = selectUnit.mRecordFileYmd.length() == 0 ? selectUnit.mStudyPlanYmd : selectUnit.mRecordFileYmd;
        data.mCurrentStatus = selectUnit.mStudyStatus;
        data.mProductNo = selectUnit.mProductNo;
        data.mStudyUnitCode = selectUnit.mStudyUnitCode;
        data.mStudyUnitNo = selectUnit.mStudyUnitNo;
        data.mReStudyCnt = selectUnit.mReStudyCnt;
        data.mProductName = String.format("%s %d권 - %s", selectUnit.mSeriesName.replace("u0027", "'"), selectUnit.mRealBookNo, selectUnit.mStudyUnitCode);
        data.mSeriesNo = selectUnit.mSeriesNo;
        data.mSeriesName = selectUnit.mSeriesName;
        data.mBookNo = selectUnit.mBookNo;
        data.mRealBookNo = selectUnit.mRealBookNo;
        data.mPracticeCnt = 5;// 5로 고정됨. selectUnit.mRepeatCount.equals("1")?option.mMiddleRepeatCnt:option.mElementarytRepeatCnt;
        data.mIsAudioExist = selectUnit.mIsCdnAudio;
        data.mIsZipExist = selectUnit.mIsCdnZip;
        data.mBookDetailId = selectUnit.mBookDetailId;
        data.mStudentBookId = selectUnit.mStudentBookId;
        data.mStudyResultNo = selectUnit.mStudyResultNo;
        data.mReviewStudyStatus = selectUnit.mReviewStudyStatus;
        data.mIsOldAnswerExist = isOldAnswer(selectUnit.mSeriesNo);//일부러 db에 안넣음 구교재꺼라서 나중에 제거될꺼
        //new Dictation (테스트 용으로 만든거 나중에 빼기만 하면됨)
        data.isNewDication = selectUnit.mIsNewDictation;
        data.isNewDication = selectUnit.mIsNewDictation;

        //팝퀴즈
        data.mProductType = selectUnit.mProductType;

        // V차시인 경우 VIP 진행
        if (data.mStudyUnitCode.contains("V")) {
            data.mIsParagraph = true;
        }
        // R차시인 경우 학습모드 하(caption 보여줌)로 진행
        if (data.mStudyUnitCode.contains("R") && data.mStudyUnitCode.length() < 6) {
            data.mIsCaption = true;
        }
        //		data.mAgencyCode = selectUnit.mAgencyCode;
        //		data.mClassNo = selectUnit.mClassNo;
        //		data.mSiteKind = selectUnit.mSiteKind;

        int videoCnt = 0;
        try {
            videoCnt = ContentsUtil.getVTag(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        } catch (IOException e1) {
            e1.printStackTrace();
            showContentErrorDialog();
        }
        for (int i = 1; i <= videoCnt; i++) {
            data.mVideoFile.add(ContentsUtil.getVideoFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i));
        }
        /*if (videoCnt > 0) {
			data.mVideoFile = ContentsUtil.getVideoFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
		} else {
			data.mVideoFile = "";
		}
		*/
        try {
            data.mAudioStudyTime = selectUnit.mBookCheckDt.equals("") ? 0 : Integer.valueOf(selectUnit.mBookCheckDt);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        data.mPageInfo = mDatabaseUtil.selectPageInfo(selectUnit.mSeriesNo, selectUnit.mRealBookNo, selectUnit.mStudyUnitNo);

        data.mWordQuestion = mDatabaseUtil.selectWordQuestion(customerNo, selectUnit.mProductNo, selectUnit.mStudyUnitCode, selectUnit.mReStudyCnt, false);
        data.mSentenceQuestion = mDatabaseUtil.selectSentenceQuestion(customerNo, selectUnit.mProductNo, selectUnit.mStudyUnitCode, selectUnit.mReStudyCnt, false);

        //vanishing
        data.mVanishingQuestion = mDatabaseUtil.selectVanishingQuestion(customerNo, selectUnit.mProductNo, selectUnit.mStudyUnitCode, selectUnit.mReStudyCnt);

        data.mIsMovieExist = selectUnit.mIsMovie;

        StudyDataUtil.initAudioRecordPath();

        for (int i = 0; i < data.mSentenceQuestion.size(); i++) {
            StudyData.SentenceQuestion question = data.mSentenceQuestion.get(i);
            question.mRecordFile = String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_SS_%d.wav", data.mRecordFileYmd.replaceAll("-", ""), data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, question.mQuestionOrder);
        }

        for (int i = 0; i < data.mVanishingQuestion.size(); i++) {
            StudyData.VanishingQuestion question = data.mVanishingQuestion.get(i);
            question.mRecordFile = String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_VC_%d.wav", data.mRecordFileYmd.replaceAll("-", ""), data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, question.mQuestionOrder);
        }

        // 중단 학습이 아니고 이전학습복습 데이터가 있을때
        // 이전학습 복습 데이터를 넣어줌.
        if (!mIsStudyStopped && review.mProductNo != 0 && !data.mReviewStudyStatus.equals("RFN")) {
            data.mIsReviewExist = true;
            data.mReviewStudyPlanYmd = review.mStudyPlanYmd.equals("") ? selectUnit.mStudyPlanYmd : review.mStudyPlanYmd;//record_file_ymd이게 지금 ""으로 옴
            data.mReviewProductNo = review.mProductNo;
            data.mReviewStudyUnitCode = review.mStudyUnitCode;
            data.mReviewReStudyCnt = review.mReStudyCnt;
            data.mReviewStudyUnitNo = review.mStudyUnitNo;
            data.mReviewProductName = String.format("%s %d권 - %s", review.mSeriesName.replace("u0027", "'"), review.mRealBookNo, review.mStudyUnitCode);
            data.mReviewSeriesNo = review.mSeriesNo;
            data.mReviewSeriesName = review.mSeriesName;
            data.mReviewBookNo = review.mBookNo;
            data.mReStudyResultNo = review.mStudyResultNo;

            if (option.mIsReviewBook) {
                data.mReviewIsBodyExist = review.mIsCdnBFile;
                data.mReviewISBodyVideoExist = review.mIsCdnVideoFile;
            } else {
                data.mReviewIsBodyExist = false;
            }

            data.mReviewPracticeCnt = 5;// 5로 고정됨.  review.mRepeatCount.equals("1")?option.mMiddleRepeatCnt:option.mElementarytRepeatCnt;

            data.mReviewWordQuestion = mDatabaseUtil.selectWordQuestion(customerNo, review.mProductNo, review.mStudyUnitCode, review.mReStudyCnt, true);
            data.mReviewSentenceQuestion = mDatabaseUtil.selectSentenceQuestion(customerNo, review.mProductNo, review.mStudyUnitCode, review.mReStudyCnt, true);

            for (int i = 0; i < data.mReviewSentenceQuestion.size(); i++) {
                StudyData.SentenceQuestion question = data.mReviewSentenceQuestion.get(i);
                question.mRecordFile = String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_RS_%d.wav", data.mReviewStudyPlanYmd.replaceAll("-", ""), data.mCustomerNo, data.mReviewProductNo, data.mReviewStudyUnitCode, data.mReviewReStudyCnt, question.mQuestionOrder);
            }

            // 본문 듣기가 없고, 틀린 단어나 문장이 없을때는 review_status = FN으로 하고 review가 없는 것으로 함.
            if (!data.mReviewIsBodyExist && !data.mReviewISBodyVideoExist && data.mReviewWordQuestion.size() == 0 && data.mReviewSentenceQuestion.size() == 0) {
                data.mIsReviewExist = false;

                StudyDataUtil.setCurrentStudyStatus(this, "RFN");
            }

            if (data.mReviewStudyUnitCode.contains("R") && data.mStudyUnitCode.length() < 6) {
                data.mIsReviewExist = false;
            }
        }
        //숲용 팝업퀴즈 선별
        try {
            data.mPopupQuizList = StudyDataUtil.getQuizPopList(TagEventData.getQuizJsonArray());
            TagEventData.setQuizJsonArray(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //일일/주간평가
        if (tempOneWeekData != null) {
            data.mOneWeekData = tempOneWeekData;
        }

        //data.mIsDicAnswer = true;*
        convertYdaToOgg();

        //중앙화관련추가
        data.forestCode = Preferences.getForestCode(mContext);
        data.teacherCode = Preferences.getTeacherCode(mContext);
        data.lmsStatus = Preferences.getLmsStatus(mContext);
    }

    /**
     * yda->ogg를 진행할 파일 리스트를 생성 및 실행
     */
    private void convertYdaToOgg() {
        StudyData data = StudyData.getInstance();

        ArrayList<String> ydaFileList = new ArrayList<String>();
//		ydaFileList.addAll(ContentsUtil.getUnitContentListYda(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, data.mSentenceQuestion.size(),
//				mDatabaseUtil.selectDictationCount(data.mProductNo, data.mStudyUnitCode), false, false));
        ydaFileList.addAll(ContentsUtil.getUnitContentListYda(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, data.mSentenceQuestion.size(), data.mVanishingQuestion.size(),
                mDatabaseUtil.selectDictationCount(data.mProductNo, data.mStudyUnitCode), false, false, data.mIsAudioExist));

        if (data.mIsReviewExist) {
            ydaFileList.addAll(ContentsUtil.getUnitContentListYda(data.mReviewSeriesNo, data.mReviewBookNo,
                    data.mReviewStudyUnitNo, mDatabaseUtil.selectSentenceQuestionCount(data.mReviewProductNo, data.mReviewStudyUnitCode), 0, true, data.mReviewIsBodyExist));
        }

//		for(int i = 0; i < ydaFileList.size(); i++) {
//			Log.e(TAG, "ydaFileList : " + ydaFileList.get(i));
//		}

        if (ydaFileList.size() > 0) {
            mYdaConvert = new YdaConverter(mHandler, (String[]) ydaFileList.toArray(new String[0]));
            mYdaConvert.start();
        } else {
//			setWordSoundFilePath();	//WP학습만 있는 경우 yda파일이 존재하지 않아 별도 path 지정 추가
            checkStoppedStudy();
        }
    }

    private void showContentErrorDialog() {

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_content_error));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_CONTENT_ERROR);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private boolean isOldAnswer(int mSeriesNo) {
        boolean result = false;
        //구교재정보
        for (int sNom : ServiceCommon.OLD_BOOK_LIST) {
            if (sNom == mSeriesNo) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * 다운로드 받는 교재 item에 대한 정보 설정
     *
     * @param position : 다운로드 받는 item Position
     */
    private void setDownloadItemInfo(int position) {
        StudyBookSelectListItem.ItemInfo info = null;
        boolean isDown = false;

        for (int i = 0; i < mInfoList.size(); i++) {
            info = mInfoList.get(i);

            if (i == position) {
                isDown = true;
                info.mIsDownloading = true;
                info.mDownloadPercent = (int) ((mUnitDownloadedCnt / (double) mUnitTotalCnt) * 100);
            } else {
                info.mIsBlackDimmed = true;
            }
        }

        //mListAdapter.notifyDataSetChanged();
    }

    /**
     * 다운로드 progress 업데이트
     *
     * @param percent : 퍼센트
     */
    private void updateDownloadProgress(int percent) {
        if (percent > 100) {
            percent = 100;
        }

        StudyBookSelectListItem.ItemInfo info = mInfoList.get(mDownloadingUnitPos);
        info.mDownloadPercent = percent;

        //mListAdapter.notifyDataSetChanged();
    }

    /**
     * 다운로드 실패시 item 설정
     */
    private void setDownloadCancel() {
        StudyBookSelectListItem.ItemInfo info = null;
        mDownloadingUnitPos = -1;

        for (int i = 0; i < mInfoList.size(); i++) {
            info = mInfoList.get(i);
            info.mIsDownloading = false;
            info.mIsBlackDimmed = false;
        }

        //mListAdapter.notifyDataSetChanged();
    }

    /**
     * 학습 데이터에 따라 학습 시작 화면 선정
     */
    private void studyStart() {
        StudyData data = StudyData.getInstance();
        String status = data.mCurrentStatus;

        if (data.mIsReviewExist) { //이전학습 복습이 존재하면(중단학습일때는 이전학습 복습이 존재하지 않는 것으로 진행)
            int targetActivity = 0;
            if (data.mReviewIsBodyExist)
                targetActivity = ServiceCommon.STATUS_REVIEW_AUDIO;
            else if (data.mReviewISBodyVideoExist)
                targetActivity = ServiceCommon.STATUS_REVIEW_AUDIO;
            else if (0 < data.mReviewWordQuestion.size())
                targetActivity = ServiceCommon.STATUS_REVIEW_WORD;
            else if (0 < data.mReviewSentenceQuestion.size())
                targetActivity = ServiceCommon.STATUS_REVIEW_SENTENCE;

            startActivity(new Intent(this, ReStudyTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, targetActivity)); // 이전학습 복습 간지
        } else if (status.equals("")) { // 중단학습이 아니면
            if (data.mIsAudioExist) {
                startActivity(new Intent(this, SmartStudyTitlePaperActivity.class)); // 오디오 학습 간지
            } else {
                StudyDataUtil.setCurrentStudyStatus(this, "S01");
                if (0 < data.mWordQuestion.size() || 0 < data.mSentenceQuestion.size() || (data.mIsParagraph && 0 < data.mVanishingQuestion.size())) {// 단어 시험이나 문장 시험이 있으면 시험 준비
                    startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
                } else if (data.mIsDictation && 0 < data.mDictation.size()) { // 받아쓰기가 있으면 받아쓰기로
                    startActivity(new Intent(this, DictationTitlePaperActivity.class));
                } else if (data.isNewDication && data.mIsDicPirvate) {
                    startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                } else if (data.mIsMovieExist) {
                    startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                } else if (data.mOneWeekData.size() > 0)
                    startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
                else {
                    startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
                }
            }

        }
        // 중단 학습일 경우 본학습 학습상태 코드(S01:학습시작, S11:본학습_음원듣기시작,S12:종료,
        //S21:시험준비,S22:시험 준비 완료 , S31,S32:본학습_단어, S41,S42:본학습_문장, S51,S52:본학습_받아쓰기,S61,S62:본학습_문법동영상,SFN:학습종료)
        else if (status.equals("S01") || status.equals("S11") || status.equals("RFN")) {
            //startActivity(new Intent(this, SmartStudyTitlePaperActivity.class)); // 오디오 학습 간지
            if (data.mIsAudioExist) {
                startActivity(new Intent(this, SmartStudyTitlePaperActivity.class)); // 오디오 학습 간지
            } else {
                StudyDataUtil.setCurrentStudyStatus(this, "S01");
                if (0 < data.mWordQuestion.size() || 0 < data.mSentenceQuestion.size() || (data.mIsParagraph && 0 < data.mVanishingQuestion.size())) {// 단어 시험이나 문장 시험이 있으면 시험 준비
                    startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
                } else if (data.mIsDictation && 0 < data.mDictation.size()) { // 받아쓰기가 있으면 받아쓰기로
                    startActivity(new Intent(this, DictationTitlePaperActivity.class));
                } else if (data.isNewDication && data.mIsDicPirvate) {
                    startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                } else if (data.mIsMovieExist) {
                    startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                } else if (data.mOneWeekData.size() > 0)
                    startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
                else {
                    startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
                }
            }
        } else if (status.equals("S12") || status.equals("S21")) {
            if (0 < data.mWordQuestion.size() || 0 < data.mSentenceQuestion.size() || (data.mIsParagraph && 0 < data.mVanishingQuestion.size())) {// 단어 시험이나 문장 시험이 있으면 시험 준비
                startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
            } else if (data.mIsDictation && 0 < data.mDictation.size()) { // 받아쓰기가 있으면 받아쓰기로
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
            }
        } else if (status.equals("S22")) { // 시험준비가 들어온것은 둘중 하나는 있는 것임
            if (0 < data.mWordQuestion.size()) {
                startActivity(new Intent(this, WordTitlePaperActivity.class)); // 단어 시험 간지로
            } else if (0 < data.mSentenceQuestion.size()) {
                startActivity(new Intent(this, SentenceTitlePaperActivity.class)); // 문장 시험 간지로
            } else if (data.mIsParagraph && 0 < data.mVanishingQuestion.size()) {
                startActivity(new Intent(this, VanishingTitlePaperActivity.class)); // 문장 시험 간지로
            } else if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S31")) {
            startActivity(new Intent(this, WordTitlePaperActivity.class)); // 단어 시험 간지로
        } else if (status.equals("S32") || status.equals("S41")) {
            if (0 < data.mSentenceQuestion.size()) {
                startActivity(new Intent(this, SentenceTitlePaperActivity.class));
            } else if (data.mIsParagraph && 0 < data.mVanishingQuestion.size()) {
                startActivity(new Intent(this, VanishingTitlePaperActivity.class));
            } else if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S42") || status.equals("S71")) {
            if (data.mIsParagraph && 0 < data.mVanishingQuestion.size()) {
                startActivity(new Intent(this, VanishingTitlePaperActivity.class));
            } else if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S51") || status.equals("S72")) {
            if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S52") || status.equals("S61")) { //
            if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class)); // 결과 페이지로
            }
        } else if (status.equals("S62")) {
            startActivity(new Intent(this, StudyOutcomeActivity.class)); // 결과 페이지로
        }

        finish();
    }

    /**
     * 메시지 박스에 대한 응답
     */
    @Override
    public void onDialogDismiss(int result, int dialogId) {
        com.yoons.fsb.student.util.Log.e(TAG, "result:" + result + "   dialogID" + dialogId);
        if (dialogId == MSGBOX_ID_STOPPED_STUDY_NOTI) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                studyStart();
            }
        } else if (dialogId == MSGBOX_ID_UPDOWN_REFRESH) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                if (!mIsUnitDownload)
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_FILE_UPLOAD);
            }
        } else if (dialogId == MSGBOX_ID_UPDOWN_COMPLETE) {
            hideProgressDialog();
        } else if (dialogId == MSGBOX_ID_BOOK_SELECT) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                showProgressDialog();
                syncDownloadDatabaseTable();
                //startContentsDownload(mSelectedIndex);
            } else {
            }
        } else if (dialogId == MSGBOX_ID_UPDATE) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                startActivity(new Intent(NewHomeActivity.this, IntroActivity.class));
                sendLogOutEvent2();
                finish();
            } else {
                startHomeSelect();
            }
        } else if (dialogId == MSGBOX_ID_CONTENT_ERROR) {
            downloadList();
        } else if (dialogId == MSGBOX_ID_SEC_ERROR) {
            //viewList();
            hideProgressDialog();
            //changeLayout();
        }
    }

    /**
     * 중단 학습인지 여부
     */
    private void checkStoppedStudy() {
        StudyData data = StudyData.getInstance();

        if (mIsStudyStopped) {
            if (!Preferences.getShowGuide(mContext)) {
                showStoppedStudyNotiDialog(data.mProductName + "의\n 중단된 학습을 이어서 진행합니다.");
            }
        } else {
            studyStart();
        }
    }

    /**
     * yda->ogg 결과 및 sync handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_CONVERT: {
                    com.yoons.fsb.student.util.Log.e(TAG, "Convert Complete Success : " + msg.arg1);
                    if (msg.arg1 == ServiceCommon.MSG_SUCCESS) {
                        // success 일때만 start
                        setStudySoundFilePath();
                        checkStoppedStudy();
                    } else {
                        String message = getString(R.string.string_common_study_file_convert_error) + msg.arg2;
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }

                    break;
                }

                case ServiceCommon.MSG_RECORDING_FILE_UPLOAD: // 음성 파일 업로드 시작
                    if (mProgressDialog == null) {
                        mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_book_download_progress1);
                    }
                    startActivityForResult(new Intent(mContext, UploadActivity.class), REQUESTCODE_UPLOAD);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START: // upload sync 시작(학습 결과에 대한 업로드)
                    Preferences.setStudying(mContext, true);
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS: // upload sync 성공
                    syncDownloadDatabaseTable(); // 업로드 싱크가 끝나면, 다운로드 싱크를 함.
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL: // upload sync 실패
                    showSyncUpDownNetworkErrorDialog();
                    //					Preferences.setSyncSuccess(mContext, false);
                    break;

                case ServiceCommon.MSG_DATABASE_NEW_DOWNLOAD_SYNC_SUCCESS: // download sync 성공
                    Preferences.setStudying(mContext, false);
                    Preferences.setSyncSuccess(mContext, true);
                    startHomeSelect();
                    if (isStudyEnd) {//처음시작할때 흐름이면
                        isStudyEnd = false;
                        hideProgressDialog();
                    }
                    break;

                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL: // download sync 실패
                    Preferences.setStudying(mContext, false);
                    showSyncUpDownNetworkErrorDialog();
                    Preferences.setSyncSuccess(mContext, false);
                    break;
                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS: // download sync성공
                    //hideProgressDialog();
                    DatabaseSync databaseSync = new DatabaseSync(mContext);
                    StudyBookSelectListItem.ItemInfo info = mInfoList.get(mSelectIndex);
                    databaseSync.downloadPageDataStart(mHandler, info.mSeriseNo, info.mRealBookNo);
                    break;

                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS + 111:
                    startContentsDownload(mSelectIndex);
                    break;

                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL + 111:
                    Preferences.setStudying(mContext, false);
                    showSyncUpDownNetworkErrorDialog();
                    Preferences.setSyncSuccess(mContext, false);
                    break;

                case ServiceCommon.MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE:    // 다운로드 파일 목록 생성 완료
                    DownloadServiceAgent.RequestUpdate(mContext);
                    break;
            }

            super.handleMessage(msg);
        }
    };

    /**
     * 다운로드 받을 컨텐츠 파일 리스트를 만드는 task
     *
     * @author ss99km01
     */
    private void makeExtraDownloadContentList() {
        Thread thread = new Thread("makeDownloadContentList") {
            public void run() {
                if (mContext == null) {
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE);
                    return;
                }

                DatabaseUtil databaseUtil = DatabaseUtil.getInstance(mContext);
                ArrayList<DatabaseData.DownloadStudyUnit> unitList = databaseUtil.selectTodayDownloadStudyUnit(Preferences.getCustomerNo(mContext));

                for (int i = 0; i < unitList.size(); i++) {
                    DatabaseData.DownloadStudyUnit unit = unitList.get(i);
                    Map<String, String> list = ContentsUtil.getExtraDownloadContentList(unit);
                    Iterator it = list.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pairs = (Map.Entry) it.next();
                        databaseUtil.insertDownloadContent(unit.mSeriesNo * 1000 + unit.mBookNo * 100 + unit.mStudyUnitNo, (String) pairs.getKey(), (String) pairs.getValue(), unit.mIsPriority);
                    }
                }

                mHandler.sendEmptyMessage(ServiceCommon.MSG_INSERT_DOWNLOAD_CONTENT_LIST_COMPLETE);
            }
        };

        thread.start();
    }

    private void sendQGet() {
        RequestPhpPost reqQget = new RequestPhpPost(mContext) {
            protected void onPreExecute() {
                qGetMap = null;
            }

            @Override
            protected void onPostExecute(Map<String, String> result) {
                super.onPostExecute(result);
                try {
                    qGetMap = result;
                    mSecCurCnt = Integer.valueOf(result.get("is_class_count"));
                    mSecMaxCnt = Integer.valueOf(result.get("total_count"));

                    ((TextView) findViewById(R.id.sec_cnt)).setText(String.valueOf(mSecMaxCnt - mSecCurCnt));

                    /*txt_month.setText(CommonUtil.curMonth() + "월 ");
                    txt_cnt.setText(result.get("is_class_count") + "회  / " + result.get("total_count") + "회 ");
                    txt_total_cnt.setText(result.get("total_count"));*/
                    if ("0".equals(result.get("is_class_count")) && "0".equals(result.get("total_count"))) {
                        mIsSecExist = false;
                    } else {
                        mIsSecExist = true;
                    }
                } catch (Exception e) {//qget이 없다면 안보이게
                    e.printStackTrace();
                    mIsSecExist = false;
                }
            }
        };

        reqQget.getQGET();
    }

    /**
     * 교재 다운로드 네트워크 에러 팝업 띄움
     */
    public void showDownloadNetworkErrorPopup() {
        hideProgressDialog();

        if (ErrorData.getErrorCode() == ErrorData.ERROR_HTTP_FILE_NOT_FOUND) {
            mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_file_not_found));
        } else {
            if (ErrorData.getErrorMsg().contains("UNMOUNTED_STORAGE") || ErrorData.getErrorMsg().contains("NOT ENOUGH SPACE")) {
                mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_not_exist_storage_or_using_usb_disk));
            } else {
                mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_is_not_connected) + "\n" + ErrorData.getErrorMsg());
            }
        }

        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.show();
    }

    /**
     * 새로 고침시 오류 발생시 띄우는 에러 다이얼로그
     */
    private void showSyncUpDownNetworkErrorDialog() {
        hideProgressDialog();

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_error) + "\n" + ErrorData.getErrorMsg());
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_REFRESH);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }


    /**
     * 새로 고침 성공 다이얼로그
     */
    private void showSyncUpDownCompleteDialog() {
        hideProgressDialog();

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_complete));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_COMPLETE);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private void showLTEDownloadDialog() {
        hideProgressDialog();
//        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_lte_download_check));
//        mMsgBox.setConfirmText(R.string.string_common_confirm);
//        mMsgBox.setCancelText(R.string.string_common_cancel);
//        mMsgBox.setId(MSGBOX_ID_LTE_DOWNLOAD);
//        mMsgBox.setOnDialogDismissListener(this);
//        mMsgBox.show();
    }

    /**
     * Upload Activity Result를 받는 함수
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUESTCODE_UPLOAD == requestCode) {

            switch (resultCode) {
                case HttpURLConnection.HTTP_OK:
                case UploadActivity.ERR_EMPTY_LIST:
                    // success
            }

            Log.i("", "resultCode:" + resultCode);

            if (RESULT_CANCELED == resultCode) {
                finish();
                return;
            }

            mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
        } else if (REQUESTCODE_GUIDE == requestCode) {
            //mIsResume = true;
        }
    }

    /**
     * db upload sync 실행
     */
    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mHandler);
        } else {
            Preferences.setStudying(mContext, false);
            Preferences.setSyncSuccess(mContext, false);
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * db download sync 실행
     */
    public void syncDownloadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            final DatabaseSync databaseSync = new DatabaseSync(this);
            if (mSelectIndex >= mInfoList.size()) {
                mSelectIndex = 0;
                Preferences.setStudying(mContext, false);
                Preferences.setSyncSuccess(mContext, false);
                showSyncUpDownNetworkErrorDialog();
                return;

            }
            final StudyBookSelectListItem.ItemInfo info = mInfoList.get(mSelectIndex);
            /**
             * 일주간평가문항
             */
            reqQTET = new RequestQTETPhpPost(mContext) {
                @Override
                protected void onPostExecute(ArrayList<StudyData.OneWeekData> result) {
                    tempOneWeekData = result;
                    try {
                        databaseSync.downloadSyncStart(mHandler, Preferences.getCustomerNo(mContext), info.mProductNo, info.mStudyUnitCode, String.valueOf(info.mBookDetailid), String.valueOf(info.mStudentBookId));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Preferences.setStudying(mContext, false);
                        Preferences.setSyncSuccess(mContext, false);
                        showSyncUpDownNetworkErrorDialog();
                    }

                }
            };

            /**
             * 일일/주간 문항개수 때문에
             */
            RequestPhpPost reqBDIF = new RequestPhpPost(mContext) {

                @Override
                protected void onPostExecute(Map<String, String> result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);
                    int i = 0;
                    try {
                        i = Integer.valueOf(result.get("cnt_test"));
                    } catch (Exception e) {//일일 주간평가 6월에 우영 적용한다고 함
                        e.printStackTrace();
                        i = 0;
                    }
                    if (i == 0) {//일일/주간평가 없을때
                        try {
                            //sync_all
                            databaseSync.downloadSyncStart(mHandler, Preferences.getCustomerNo(mContext), info.mProductNo, info.mStudyUnitCode, String.valueOf(info.mBookDetailid), String.valueOf(info.mStudentBookId));
                        } catch (UnsupportedEncodingException e) {
                            Preferences.setStudying(mContext, false);
                            Preferences.setSyncSuccess(mContext, false);
                            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
                            showSyncUpDownNetworkErrorDialog();
                        }

                    } else {//있을대
                        reqQTET.sendQTET(Preferences.getUserNo(mContext), info.mBookDetailid, i, String.valueOf(info.mProductNo), info.mStudyUnitCode);
                    }
                }
            };

            reqBDIF.getBDIF(info.mProductNo, info.mBookDetailid, info.mStudyUnitCode);

        } else {
            Preferences.setStudying(mContext, false);
            Preferences.setSyncSuccess(mContext, false);
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * 다운로드 서비스 이벤트 receiver
     */
    BroadcastReceiver mDownloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AgentEvent.ACTION_RECEIVE_AGENT_EVENT.equals(intent.getAction())) {
                int type = intent.getIntExtra(AgentEvent.EVENT_TYPE, 0);
                switch (type) {
                    case AgentEvent.DOWNLOAD_START:
                        Log.e(TAG, "DOWNLOAD_START");
                        break;

                    case AgentEvent.DOWNLOAD_STOP:
                        Log.e(TAG, "DOWNLOAD_STOP");
                        break;

//                    case AgentEvent.CHECK_LTE:
//                        Log.e(TAG, "CHECK_LTE");
//                        if(!mIsLTECheck) {
//                            mIsLTECheck = true;
//                            showLTEDownloadDialog();
//                        }
//                        break;

                    case AgentEvent.DOWNLOAD_COMPLETE:
                        Log.e(TAG, "DOWNLOAD_COMPLETE");
                        if (!mExtraDownload) {
                            mExtraDownload = true;
                            makeExtraDownloadContentList();
                        }
                        break;

                    case AgentEvent.DOWNLOAD_FAIL:
                        Log.e(TAG, "DOWNLOAD_FAIL");
                        // 네트워크가 설정에 다운을 받을수 없음.
                        // 오류 팝업
                        if (mIsUnitDownload) {
                            int error = intent.getIntExtra(AgentEvent.FAIL_REASON, AgentEvent.REASON_NO_ERROR);

                            switch (error) {
                                case AgentEvent.REASON_NO_ERROR:
                                    break;

                                case AgentEvent.REASON_UNMOUNTED_STORAGE:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "UNMOUNTED_STORAGE");
                                    break;

                                case AgentEvent.REASON_NOT_ENOUGH_SPACE:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT ENOUGH SPACE");
                                    break;

                                case AgentEvent.REASON_NET_ERROR:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_NOT_FOUND, "");
                                    break;

                                case AgentEvent.REASON_UNKNOWN_ERROR:
                                    break;

                                case AgentEvent.REASON_NOT_AVAILABLE_NETWORKS:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT_AVAILABLE_NETWORKS");
                                    break;
                            }
                            showDownloadNetworkErrorPopup();
                            mIsUnitDownload = false;
                            setDownloadCancel();
                        }

                        break;

                    case AgentEvent.START_LEARNING:
                        Log.e(TAG, "START_LEARNING");
                        break;

                    case AgentEvent.CONTENTS_UPDATE:
                        Log.e(TAG, "CONTENTS_UPDATE");
                        DownloadServiceAgent.RequestStart(NewHomeActivity.this);
                        mIsUnitDownload = true;
                        break;

                    case AgentEvent.DOWNLOAD_STATE: {
                        Log.e(TAG, "DOWNLOAD_STATE " + String.format("Tot(%d/%d), Pri(%d/%d)",
                                intent.getIntExtra(AgentEvent.TOT_POS, 0),
                                intent.getIntExtra(AgentEvent.TOT_CNT, 0),
                                intent.getIntExtra(AgentEvent.PRI_POS, 0),
                                intent.getIntExtra(AgentEvent.PRI_CNT, 0)));

                        int priorityTotal = intent.getIntExtra(AgentEvent.PRI_CNT, 0);
                        int priorityPos = intent.getIntExtra(AgentEvent.PRI_POS, 0);
                        if (priorityTotal != 0 && mDownloadingUnitPos != -1) {
                            updateDownloadProgress((int) (((mUnitDownloadedCnt + priorityPos) / (double) mUnitTotalCnt) * 100));
                        }

                        if (priorityPos == 0 && priorityTotal == 0 && mDownloadingUnitPos != -1) {
                            //다운로드 완료
                            if (mIsUnitDownload) {
                                //makeStudyData(mDownloadingUnitPos);
                                startContentsDownload(mSelectIndex);
                                mIsUnitDownload = false;
                            }
                        }
                    }
                    break;
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        //FlurryAgent.onStartSession(this, ServiceCommon.FLURRY_API_KEY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        overridePendingTransition(0, 0);

        hideProgressDialog();

        // 서비스 언바인딩.
        if (mServiceAgent != null) {
            unbindService(serviceConnection);
            mServiceAgent = null;
        }

        if (mMsgBox != null) {
            mMsgBox.dismiss();
            mMsgBox = null;
        }

        mServiceAgent = null;
    }

    /**
     * 오늘까지 학습해야할 차시 개수를 계산함
     */
    public void setStudyUnitCount() {
        DatabaseData.TodayStudyUnit unit = null;
        DatabaseData.TodayStudyUnit unit2 = null;
        StudyBookSelectListItem.ItemInfo info = null;

        int count = 0;

        for (int i = 0; i < mInfoList.size(); i++) {
            info = mInfoList.get(i);
            unit = mStudyUnit.get(info.mStudyUnitIndex);
            count = 0;

            for (int j = 0; j < mStudyUnit.size(); j++) {
                unit2 = mStudyUnit.get(j);

                if (unit.mProductNo == unit2.mProductNo && unit.mReStudyCnt == unit2.mReStudyCnt && unit2.mStudyPlanYmd.compareTo(CommonUtil.getCurrentYYYYMMDD2()) <= 0) {
                    count++;
                }
            }

            info.mStudyUnitCount = count;
        }
    }

    /**
     * 리스트에 등록되어 있는 교재인지 여부
     *
     * @param productNo  : 교재 코드
     * @param reStudyCnt : 재 학습 순서
     * @return
     */
    public boolean isExistProductNo(int productNo, int reStudyCnt) {
        for (int i = 0; i < mInfoList.size(); i++) {
            DatabaseData.TodayStudyUnit unit = mStudyUnit.get(mInfoList.get(i).mStudyUnitIndex);

            if (unit.mProductNo == productNo && unit.mReStudyCnt == reStudyCnt) {
                return true;
            }
        }

        return false;
    }

    /**
     * 오늘 학습 리스트 생성
     */
    private void makeTodayStudyLIst() {
        //중단학습이 있는지 체크
        for (int i = 0; i < mStudyUnit.size(); i++) {
            DatabaseData.TodayStudyUnit unit = mStudyUnit.get(i);
            if (!unit.mStudyStatus.equals("SFN") && !unit.mStudyStatus.equals("")) {
                mIsStudyStopped = true;
                mStudyStoppedIndex = i;
            }
        }

        mInfoList.clear();

        ArrayList<DatabaseData.StudyResultUnitMax> resultMaxList = mDatabaseUtil.selectStudyResultMaxStudyUnitNo(Preferences.getCustomerNo(mContext));

        for (int i = 0; i < mStudyUnit.size(); i++) {
            DatabaseData.TodayStudyUnit unit = mStudyUnit.get(i);
            boolean bContinue = false;

            StudyBookSelectListItem.ItemInfo info = new StudyBookSelectListItem.ItemInfo();
            info.mBookNo = String.format("%d권 - %s", unit.mRealBookNo, unit.mStudyUnitCode);
            info.mBookName = String.format("%s %s", unit.mSeriesName, info.mBookNo);
            info.mProductNo = unit.mProductNo;
            info.mSeriseNo = unit.mSeriesNo;
            info.mStudyUnitCode = unit.mStudyUnitCode;
            info.mIsStudyStopped = !unit.mStudyStatus.equals("");

            info.mBookDetailid = unit.mBookDetailId;
            info.mStudentBookId = unit.mStudentBookId;

            if (unit.mStudyPlanYmd.replaceAll("-", "").compareTo(CommonUtil.getCurrentYYYYMMDD()) > 0) {
                info.mIsTodayStudy = false;
            } else {
                info.mIsTodayStudy = true;
            }

            if (!ServiceCommon.IS_EXPERIENCES) {
                info.mThumbnail = ServiceCommon.THUMBNAIL_URL + String.format("%d_%d.jpg", unit.mSeriesNo, unit.mRealBookNo);
            }
//            info.mIsDownloadComplete = true;
//            info.mStudyUnitIndex = i;

            info.mRealBookNo = unit.mRealBookNo;

            if (mIsStudyStopped) { // 중단 학습일 경우
                if (i != mStudyStoppedIndex)
                    continue;
            } else { // 중단 학습이 있으면 이 부분 패스
                for (int j = 0; j < resultMaxList.size(); j++) { // 학습 결과가
                    DatabaseData.StudyResultUnitMax resultMax = resultMaxList.get(j);
                    if (unit.mProductNo == resultMax.mProductNo && unit.mStudyUnitNo <= resultMax.mStudyUnitNo) {
                        bContinue = true;
                        break;
                    }
                }

                if (bContinue)
                    continue;
            }

            if (!isExistProductNo(unit.mProductNo, unit.mReStudyCnt)) {
                try {
                    info.mIsDownloadComplete = ContentsUtil.getUnitContentDownlaodPercent(mContext, unit.mProductNo,
                            unit.mSeriesNo, unit.mBookNo, unit.mRealBookNo, unit.mStudyUnitNo, unit.mStudyUnitCode, null, false) == 100 ? true : false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                info.mStudyUnitIndex = i;

                mInfoList.add(info);
            }
        }

        setStudyUnitCount();

        if (mIsStudyStopped) { // 중단 학습 데이터로 시작함.
            if (mInfoList.size() != 1) {
                // 중단된 학습인데 학습 데이터가 없다는 메시지가 필요함.
            } else {
                mStudyLayout.setVisibility(View.VISIBLE);
                mNoStudyLayout.setVisibility(View.GONE);
                // 중단된 학습인데 콘텐츠가 모두 있는 경우.
                if (mInfoList.get(mSelectIndex).mIsDownloadComplete) {
//					makeStudyData(0);
                    startContentsDownload(mSelectIndex);
                }
            }
        } else if (mInfoList.size() == 0) { //학습할 차시가 없을때
            mStudyLayout.setVisibility(View.GONE);
            mNoStudyLayout.setVisibility(View.VISIBLE);
        } else {
            mStudyLayout.setVisibility(View.VISIBLE);
            mNoStudyLayout.setVisibility(View.GONE);

            if (CommonUtil.isBluetoothEnabled(this)) {
                selectItem(0);
            }
        }
    }

    /**
     * 아이탬 선택 (블루투스 키보드)
     *
     * @param index : 선택할 index
     */
    private void selectItem(int index) {
        if (mInfoList.size() <= 0) {
            return;
        }

        try {
            mInfoList.get(mSelectIndex).mIsSelect = false;

            mSelectIndex = index;

            mInfoList.get(mSelectIndex).mIsSelect = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 선택 아이탬 변경
     *
     * @param isUpKey : up키인지 down인키인지 여부
     */
    private void changeSelectItem(boolean isUpKey) {
        int itemSize = mInfoList.size();

        if (itemSize == 1) {
            return;
        }

        if (isUpKey) {
            if (mSelectIndex == 0) {
                selectItem(itemSize - 1);
            } else {
                selectItem(mSelectIndex - 1);
            }
        } else {
            if (mSelectIndex == itemSize - 1) {
                selectItem(0);
            } else {
                selectItem(mSelectIndex + 1);
            }
        }
    }

    /**
     * 아이탬 press 설정
     *
     * @param isPress : press여부
     */
    private void setPress(boolean isPress) {
        mInfoList.get(mSelectIndex).mIsPress = isPress;
    }

    // for bluetooth 7/24

    /**
     * 블루투스 키 이벤트
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (mIsUnitDownload)
            return true;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND: // left key
                if (mIvLeftButton.isEnabled()) {
                    mIvLeftButton.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD: // right key
                if (mIvRighButton.isEnabled()) {
                    mIvRighButton.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                //mBookListView.playSoundEffect(SoundEffectConstants.CLICK);
                if (mInfoList.size() > 0) {
                    setPress(true);
                }
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    // for bluetooth 7/24

    /**
     * 블루투스 키 이벤트
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (mIsUnitDownload)
            return true;

        switch (keyCode) {

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND: // left key
                mIvLeftButton.setPressed(false);
                mIvLeftButton.performClick();
               break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD: // right key
                mIvRighButton.setPressed(false);
                mIvRighButton.performClick();
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mInfoList.size() > 0) {
                    setPress(false);
                    mLetsStart.performClick();
                    //startContentsDownload(mSelectedIndex);
//                    if (mInfoList.size() == 1) {
//                        startContentsDownload(mSelectIndex);
//                    } else {
//                        StudyBookSelectListItem.ItemInfo info = mInfoList.get(mSelectIndex);
//                        showBookSelectDialog(info.mBookName);
//                    }
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    private void versionCheck(JSONObject obj) {
        hideProgressDialog();
        String appVersion = "";
        String serverVersion = "";

        try {
            PackageInfo i = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = i.versionName;

            JSONArray out1 = obj.getJSONArray("out1");
            JSONObject versionObject = out1.getJSONObject(0);
            serverVersion = versionObject.getString("app_version");

            // 스트링 비교로 처리할 수 없을수도 있음.(1.9.1과 1.10.1일 경우)
            // 따라서 값을 int로 바꿔서 처리.
            int appVersionValue = CommonUtil.getVersionValue(appVersion);
            int serverVersionValue = CommonUtil.getVersionValue(serverVersion);

            if (appVersionValue < serverVersionValue) {
                mMsgBox = new MessageBox(this, 0, R.string.string_msg_not_newest_version);
                mMsgBox.setConfirmText(R.string.string_msg_update);
                mMsgBox.setCancelText(R.string.string_common_cancel);
                mMsgBox.setId(MSGBOX_ID_UPDATE);
                mMsgBox.setOnDialogDismissListener(this);
                mMsgBox.show();
            } else {
                startHomeSelect();
            }
        } catch (Exception e) {
//            CommonUtil.sendLog(mContext, mLogger, e.fillInStackTrace().toString());
            e.printStackTrace();
        }
    }

    /**
     * 학습 데이터에 soundfile(ogg)를 설정함.
     */
    public void setStudySoundFilePath() {
        StudyData data = StudyData.getInstance();

        // 본학습 파일 세팅
        data.mAudioTagFile = ContentsUtil.getTagFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        data.mAudioSoundFile = ContentsUtil.getAudioFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        //data.mVideoFile = ContentsUtil.getVideoFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        // 신규문항
        data.mPopupStudyPath = ContentsUtil.getPopupContentsPath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);

        for (int i = 0; i < data.mWordQuestion.size(); i++) {
            StudyData.WordQuestion question = data.mWordQuestion.get(i);
            // 10차시 관련
            if ((data.mStudyUnitCode.contains("R") && data.mStudyUnitCode.length() < 6) || CommonUtil.isKorean(question.mQuestion) || CommonUtil.isNum(question.mQuestion)) {
                question.mSoundFile = ContentsUtil.getWordFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, question.mMeaning);
            } else {
                question.mSoundFile = ContentsUtil.getWordFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, question.mQuestion);
            }
        }

        for (int i = 0; i < data.mSentenceQuestion.size(); i++) {
            StudyData.SentenceQuestion question = data.mSentenceQuestion.get(i);
            question.mSoundFile = ContentsUtil.getSentenceFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1);
        }

        for (int i = 0; i < data.mVanishingQuestion.size(); i++) {
            StudyData.VanishingQuestion question = data.mVanishingQuestion.get(i);
            question.mSoundFile = ContentsUtil.getVanishingFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1);
        }

        int dictationCnt = mDatabaseUtil.selectDictationCount(data.mProductNo, data.mStudyUnitCode);
        for (int i = 0; i < dictationCnt; i++) {
            data.mDictation.add(ContentsUtil.getDictationFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1));
        }

        int aniTalkCnt = mDatabaseUtil.selectAniTalkCount(data.mProductNo, data.mStudyUnitCode);
        for (int i = 0; i < aniTalkCnt; i++) {
            data.mAniTalk.add(ContentsUtil.getAniTalkFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1));
        }

        if (data.mIsMovieExist) {
            data.mMovieFile = ContentsUtil.getMovieFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        }
        if (data.mIsOldAnswerExist) {
            data.mOldAnswerFile = ContentsUtil.getOLDFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        }

        // 이전학습 복습 파일 세팅
        if (data.mIsReviewExist) {
            if (data.mReviewIsBodyExist) {
                data.mReviewBodySoundFile = ContentsUtil.getBodyFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo);
            }

            if (data.mReviewISBodyVideoExist) {
                data.mReviewBodyVideoFile = ContentsUtil.getBodyVideoFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo);
            }

            for (int i = 0; i < data.mReviewWordQuestion.size(); i++) {
                StudyData.WordQuestion question = data.mReviewWordQuestion.get(i);
                if ((data.mReviewStudyUnitCode.contains("R") && data.mReviewStudyUnitCode.length() < 6) || CommonUtil.isKorean(question.mQuestion) || CommonUtil.isNum(question.mQuestion)) {
                    question.mSoundFile = ContentsUtil.getWordFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, question.mMeaning);
                } else {
                    question.mSoundFile = ContentsUtil.getWordFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, question.mQuestion);
                }
            }

            for (int i = 0; i < data.mReviewSentenceQuestion.size(); i++) {
                StudyData.SentenceQuestion question = data.mReviewSentenceQuestion.get(i);
                question.mSoundFile = ContentsUtil.getSentenceFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, question.mQuestionOrder);
            }
        }

        // 신규문항
        boolean bUnzip = false;
        String filepath = "";

        if (data.mIsZipExist) {
            try {
                filepath = ContentsUtil.getPopupContentsFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo) + ContentsUtil.POPUPSTUDY_POSTFIX;

                if (!filepath.isEmpty()) {
                    bUnzip = CommonUtil.unzip(filepath, data.mPopupStudyPath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bUnzip) {
                    File file = new File(filepath);
                    if (!file.exists())
                        return;

                    file.delete();
                }
            }
        }
    }

    private void initData() {
        mTvMon = (TextView) findViewById(R.id.tv_day_mon);
        mTvTue = (TextView) findViewById(R.id.tv_day_tue);
        mTvWed = (TextView) findViewById(R.id.tv_day_wed);
        mTvThu = (TextView) findViewById(R.id.tv_day_thu);
        mTvFri = (TextView) findViewById(R.id.tv_day_fri);
        mTvSat = (TextView) findViewById(R.id.tv_day_sat);
        mTvSun = (TextView) findViewById(R.id.tv_day_sun);

        mTvMonText = (TextView) findViewById(R.id.tv_text_mon);
        mTvTueText = (TextView) findViewById(R.id.tv_text_tue);
        mTvWedText = (TextView) findViewById(R.id.tv_text_wed);
        mTvThuText = (TextView) findViewById(R.id.tv_text_thu);
        mTvFriText = (TextView) findViewById(R.id.tv_text_fri);
        mTvSatText = (TextView) findViewById(R.id.tv_text_sat);
        mTvSunText = (TextView) findViewById(R.id.tv_text_sun);

        mTvStudentName = (TextView) findViewById(R.id.tv_student_name);
//        mTvSeriesName = (TextView) findViewById(R.id.tv_series_name);
//        mTvStudyUnitName = (TextView) findViewById(R.id.tv_studyunit_name);
        mSelect = (TextView) findViewById(R.id.navigator_select);
        mTotal = (TextView) findViewById(R.id.navigator_total);

        mIvMondayCheck = (ImageView) findViewById(R.id.iv_monday_check);
        mIvTuedayCheck = (ImageView) findViewById(R.id.iv_tueday_check);
        mIvWendayCheck = (ImageView) findViewById(R.id.iv_wedday_check);
        mIvThudayCheck = (ImageView) findViewById(R.id.iv_thuday_check);
        mIvFridayCheck = (ImageView) findViewById(R.id.iv_friday_check);
        mIvSatdayCheck = (ImageView) findViewById(R.id.iv_satday_check);
        mIvSundayCheck = (ImageView) findViewById(R.id.iv_sunday_check);

        mLayoutMonday = (LinearLayout) findViewById(R.id.layout_monday);
        mLayoutTuesday = (LinearLayout) findViewById(R.id.layout_tuesday);
        mLayoutWendsday = (LinearLayout) findViewById(R.id.layout_wendsday);
        mLayoutThursday = (LinearLayout) findViewById(R.id.layout_thursday);
        mLayoutFriday = (LinearLayout) findViewById(R.id.layout_friday);
        mLayoutSaturday = (LinearLayout) findViewById(R.id.layout_saturday);
        mLayoutSunday = (LinearLayout) findViewById(R.id.layout_sunday);

        mLayoutHome = (LinearLayout) findViewById(R.id.layout_home);
        //mLayoutNotify = (LinearLayout) findViewById(R.id.layout_notify);

        mTodayBadge = (ImageView) findViewById(R.id.today_badge);
        mTodayBadgeText = (TextView) findViewById(R.id.today_badge_text);

        mSeriesName = (TextView) findViewById(R.id.tv_series_name);
        mStudyUnitName = (TextView) findViewById(R.id.tv_studyunit_name);

        mCoverThum = ((ImageView) findViewById(R.id.iv_home_book_cover));

        mIvLeftButton = (ImageView) findViewById(R.id.home_left);
        mIvRighButton = (ImageView) findViewById(R.id.home_right);

        mIvNavigatorFirst = (ImageView) findViewById(R.id.iv_navigator_first);
        mIvNavigatorSecond = (ImageView) findViewById(R.id.iv_navigator_second);
        mIvNavigatorThird = (ImageView) findViewById(R.id.iv_navigator_third);
        mIvNavigatorFourth = (ImageView) findViewById(R.id.iv_navigator_fourth);
        mIvNavigatorFifth = (ImageView) findViewById(R.id.iv_navigator_fifth);

        mNoStudyLayout = (RelativeLayout) findViewById(R.id.no_study);
        mStudyLayout = (RelativeLayout) findViewById(R.id.study);

        mLetsStart = (TextView) findViewById(R.id.btn_start_learn);

        mCircle = (LinearLayout) findViewById(R.id.new_home_navigator_circle);
        mCount = (LinearLayout) findViewById(R.id.new_home_navigator_count);

        guidePlay(R.raw.b_09);// 교재를 선택하세요
    }

    private void init() {

        int customerNo = Preferences.getCustomerNo(mContext);
        // 체험 계정일때 학습 결과는 모두 삭제함.
        if (mDatabaseUtil.isExperienceCustomer(customerNo) || ServiceCommon.IS_EXPERIENCES) {
            mDatabaseUtil.deleteStudyResult(customerNo);
        }

        mStudyUnit = StudyData.getmStudyUnit();//mDatabaseUtil.selectTodayStudyUnit(Preferences.getCustomerNo(this), true);

//        if(mStudyUnit.size() == 0) {
//            mStudyUnit = mDatabaseUtil.selectTodayStudyUnit(Preferences.getCustomerNo(this), false);
//        }

        mSeriesName.setText("");
        mStudyUnitName.setText("");

        mLetsStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudyBookSelectListItem.ItemInfo info = mInfoList.get(mSelectIndex);
//                if(info.mIsBlackDimmed || info.mIsDownloading) {
//                    return;
//                }

                if (mInfoList.size() == 1) {
                    showProgressDialog();
                    syncDownloadDatabaseTable();
                    //startContentsDownload(mSelectIndex);
                } else {
                    showBookSelectDialog(info.mBookName);
                }
            }
        });

        mSkillCardView = (CardView) findViewById(R.id.menu_cardview_skill);
        mSkillCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchApp(ServiceCommon.FOURSKILL_PACKAGE, "market://details?id=" + ServiceCommon.FOURSKILL_PACKAGE);
            }
        });

        mSecStudyCardView = (CardView) findViewById(R.id.menu_cardview_sec);
        if (mSecStudyCardView != null) {
            mSecStudyCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIsSecExist) {
                        startSecStudy();
                    } else {
                        Toast.makeText(mContext, "학습 할 스마트 훈련이 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        mIvLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsNextPackage = false;
                mSelectIndex--;
                if (mSelectIndex <= 0) {
                    mSelectIndex = 0;
                }
                init();
            }
        });

        mIvRighButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsNextPackage = true;
                mSelectIndex++;
                if (mSelectIndex >= mInfoList.size() - 1) {
                    mSelectIndex = mInfoList.size() - 1;
                }
                init();
            }
        });

        //학습 만료 여부 학습 종료 여부
        mLayoutHome.setVisibility(View.VISIBLE);
        //mLayoutNotify.setVisibility(View.INVISIBLE);

        findViewById(R.id.iv_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExit();
            }
        });

        findViewById(R.id.iv_home_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewHomeActivity.this, BookDownloadActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        });

        makeTodayStudyLIst();

        if (mInfoList.size() <= 5) {
            mCircle.setVisibility(View.VISIBLE);

            if (mInfoList.size() <= 1) {
                mCircle.setVisibility(View.GONE);
            } else if (mInfoList.size() == 2) {
                mIvNavigatorThird.setVisibility(View.GONE);
                mIvNavigatorFourth.setVisibility(View.GONE);
                mIvNavigatorFifth.setVisibility(View.GONE);
            } else if (mInfoList.size() == 3) {
                mIvNavigatorFourth.setVisibility(View.GONE);
                mIvNavigatorFifth.setVisibility(View.GONE);
            } else if (mInfoList.size() == 4) {
                mIvNavigatorFifth.setVisibility(View.GONE);
            }

            Drawable shape_n = null;
            Drawable shape_d = null;

            if (CommonUtil.isCenter()) { // Igse
                shape_n = ContextCompat.getDrawable(getApplicationContext(), R.drawable.igse_navigator_shape_n);
                shape_d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.igse_navigator_shape_d);
            } else {
                if ("1".equals(Preferences.getLmsStatus(this))) { //우영
                    shape_n = ContextCompat.getDrawable(getApplicationContext(), R.drawable.w_navigator_shape_n);
                    shape_d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.w_navigator_shape_d);
                } else { // 숲
                    shape_n = ContextCompat.getDrawable(getApplicationContext(), R.drawable.f_navigator_shape_n);
                    shape_d = ContextCompat.getDrawable(getApplicationContext(), R.drawable.f_navigator_shape_d);
                }
            }
            switch (mSelectIndex + 1) {
                case 1:
                    mIvNavigatorFirst.setImageDrawable(shape_n);
                    mIvNavigatorSecond.setImageDrawable(shape_d);
                    mIvNavigatorThird.setImageDrawable(shape_d);
                    mIvNavigatorFourth.setImageDrawable(shape_d);
                    mIvNavigatorFifth.setImageDrawable(shape_d);
                    break;
                case 2:
                    mIvNavigatorFirst.setImageDrawable(shape_d);
                    mIvNavigatorSecond.setImageDrawable(shape_n);
                    mIvNavigatorThird.setImageDrawable(shape_d);
                    mIvNavigatorFourth.setImageDrawable(shape_d);
                    mIvNavigatorFifth.setImageDrawable(shape_d);
                    break;
                case 3:
                    mIvNavigatorFirst.setImageDrawable(shape_d);
                    mIvNavigatorSecond.setImageDrawable(shape_d);
                    mIvNavigatorThird.setImageDrawable(shape_n);
                    mIvNavigatorFourth.setImageDrawable(shape_d);
                    mIvNavigatorFifth.setImageDrawable(shape_d);
                    break;
                case 4:
                    mIvNavigatorFirst.setImageDrawable(shape_d);
                    mIvNavigatorSecond.setImageDrawable(shape_d);
                    mIvNavigatorThird.setImageDrawable(shape_d);
                    mIvNavigatorFourth.setImageDrawable(shape_n);
                    mIvNavigatorFifth.setImageDrawable(shape_d);
                    break;
                case 5:
                    mIvNavigatorFirst.setImageDrawable(shape_d);
                    mIvNavigatorSecond.setImageDrawable(shape_d);
                    mIvNavigatorThird.setImageDrawable(shape_d);
                    mIvNavigatorFourth.setImageDrawable(shape_d);
                    mIvNavigatorFifth.setImageDrawable(shape_n);
                    break;
            }
        } else {
            mCount.setVisibility(View.VISIBLE);
            mSelect.setText(String.valueOf(mSelectIndex + 1));
            mTotal.setText(String.valueOf(mInfoList.size()));
        }

        if (mInfoList.size() > 1) {
            mIvRighButton.setVisibility(View.VISIBLE);
            mIvLeftButton.setVisibility(View.VISIBLE);
            if (mSelectIndex > 0) {
                mIvRighButton.setEnabled(true);
                mIvLeftButton.setEnabled(true);
                if (mSelectIndex == mInfoList.size() - 1) {
                    mIvRighButton.setEnabled(false);
                    mIvLeftButton.setEnabled(true);
                }
            } else {
                mIvRighButton.setEnabled(true);
                mIvLeftButton.setEnabled(false);
            }
        } else {
            mIvRighButton.setVisibility(View.INVISIBLE);
            mIvLeftButton.setVisibility(View.INVISIBLE);
        }

        SpannableStringBuilder bulder = new SpannableStringBuilder();
        bulder.append("Hello, ");

        String name = "";
        if (mCustomer != null) {
            name = mCustomer.mCustomerName;
        }
        SpannableStringBuilder spanSB = new SpannableStringBuilder(name);
        if (CommonUtil.isCenter()) // Igse
            spanSB.setSpan(new ForegroundColorSpan(Color.parseColor("#9858cf")), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                spanSB.setSpan(new ForegroundColorSpan(Color.parseColor("#f79432")), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            else // 숲
                spanSB.setSpan(new ForegroundColorSpan(Color.parseColor("#ac6543")), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        bulder.append(spanSB);
        bulder.append("!");

        mTvStudentName.setText(bulder);

        if (mInfoList != null && mInfoList.size() > 0) {
            mNoStudyLayout.setVisibility(View.GONE);
            mStudyLayout.setVisibility(View.VISIBLE);

            String bookName = mInfoList.get(mSelectIndex).mBookName.trim();
            String bookNo = mInfoList.get(mSelectIndex).mBookNo.trim();

            mSeriesName.setText(bookName.replace(bookNo, "").trim());
            mStudyUnitName.setText(bookNo.trim());
            String data = mSeriesName.getText().toString();

            ViewGroup.LayoutParams vl = mSeriesName.getLayoutParams();
            int oneLineMax = 12;
            if (vl != null && vl.width > 0) {
                oneLineMax = mSeriesName.getPaint().breakText(data, true, vl.width, null);
            }
            com.yoons.fsb.student.util.Log.i("", "onelie => " + oneLineMax);
            boolean isTablet = false;
            if ((getResources().getConfiguration().screenLayout
                    & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                isTablet = true;
            }

            if (data.length() > oneLineMax * 3) {
                mSeriesName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, isTablet ? 27 : 14);
                mStudyUnitName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, isTablet ? 25 : 12);
            } else if (data.length() > oneLineMax * 2) {
                mSeriesName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, isTablet ? 30 : 19);
                mStudyUnitName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, isTablet ? 27 : 17);
            } else {
                mSeriesName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, isTablet ? 33 : 24);
                mStudyUnitName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, isTablet ? 29 : 22);
            }

            ((RelativeLayout) mTodayBadgeText.getParent()).setVisibility(mInfoList.get(mSelectIndex).mStudyUnitCount == 0 ? View.GONE : View.VISIBLE);

            mTodayBadgeText.setText(String.valueOf(mInfoList.get(mSelectIndex).mStudyUnitCount));

            setTodayStudyThumbnail();
        } else {
            //오늘의 학습이 없는 경우
            mNoStudyLayout.setVisibility(View.VISIBLE);
            mStudyLayout.setVisibility(View.GONE);
        }
    }

    /**
     * 설치되어 있는 경우 앱 실행
     * 미설치시 앱 다운로드 및 설치
     */
    private void launchApp(String package_name, String app_url) {
        if (CommonUtil.checkInstalledPackage(mContext, package_name)) {
            startApp(package_name);
        } else {
            if (CommonUtil.isAvailableNetwork(this, false)) {
                if (app_url.contains("market://")) {
                    Uri uri = Uri.parse("market://details?id=" + package_name);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else {
                    //showLoading();
                    // appUpdate(package_name, app_url);
                }
            }
        }
    }

    private void startApp(final String app_package) {

        if (CommonUtil.checkInstalledPackage(mContext, app_package)) {
            Intent intent = getPackageManager().getLaunchIntentForPackage(app_package);
            String mainActivity = intent.getComponent().getClassName();
            String packageString = intent.getComponent().getPackageName();
            Intent launchIntent;
            launchIntent = new Intent(Intent.ACTION_MAIN);
            launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            launchIntent.setComponent(new ComponentName(packageString, mainActivity));
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            //launchIntent.addFlags(flags)(Intent.FLAG_ACTIVITY_NEW_TASK);

            if (app_package.equals(ServiceCommon.FOURSKILL_PACKAGE)) {
                String mAgencyNo = Preferences.getForestCode(mContext);
                String mTeacherNo = Preferences.getTeacherCode(mContext);

                launchIntent.putExtra("AgencyNo", mAgencyNo);
                launchIntent.putExtra("TeacherNo", mTeacherNo);
                launchIntent.putExtra(ServiceCommon.KEY_LMS_STATUS, Preferences.getLmsStatus(mContext));
                launchIntent.putExtra(ServiceCommon.KEY_CENTER, Preferences.getCenter(mContext));
            }

            startActivity(launchIntent);
//			sendLogOutEvent2();

        } else {
            Toast.makeText(this, "앱이" + getString(R.string.string_app_not_installed), Toast.LENGTH_SHORT).show();
        }
    }

    private void showBookSelectDialog(String bookName) {
        hideProgressDialog();

        StringBuffer buffer = new StringBuffer();
        buffer = buffer.append(bookName).append("\n").append("학습을 시작하시겠습니까?");

        mMsgBox = new MessageBox(this, 0, buffer.toString());
        mMsgBox.setConfirmText("확인");
        mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setId(MSGBOX_ID_BOOK_SELECT);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private void setTodayStudyThumbnail() {
        Log.i("", "thumb url => " + mInfoList.get(mSelectIndex).mThumbnail);
        mImageDownloader.download(mInfoList.get(mSelectIndex).mThumbnail, mCoverThum);
    }

    private void checkTodayWhite(int nIndex) {

        switch (nIndex) {

            case 1: {    //  일
                mLayoutMonday.setBackgroundColor(Color.WHITE);
                mLayoutTuesday.setBackgroundColor(Color.WHITE);
                mLayoutWendsday.setBackgroundColor(Color.WHITE);
                mLayoutThursday.setBackgroundColor(Color.WHITE);
                mLayoutFriday.setBackgroundColor(Color.WHITE);
                mLayoutSaturday.setBackgroundColor(Color.WHITE);
                mLayoutSunday.setBackgroundColor(Color.WHITE);
            }
            break;
            case 2: {    //  월
                mLayoutMonday.setBackgroundColor(Color.WHITE);
            }
            break;
            case 3: {    //  화
                mLayoutMonday.setBackgroundColor(Color.WHITE);
                mLayoutTuesday.setBackgroundColor(Color.WHITE);
            }
            break;
            case 4: {    //  수
                mLayoutMonday.setBackgroundColor(Color.WHITE);
                mLayoutTuesday.setBackgroundColor(Color.WHITE);
                mLayoutWendsday.setBackgroundColor(Color.WHITE);
            }
            break;
            case 5: {    //  목
                mLayoutMonday.setBackgroundColor(Color.WHITE);
                mLayoutTuesday.setBackgroundColor(Color.WHITE);
                mLayoutWendsday.setBackgroundColor(Color.WHITE);
                mLayoutThursday.setBackgroundColor(Color.WHITE);
            }
            break;
            case 6: {    //  금
                mLayoutMonday.setBackgroundColor(Color.WHITE);
                mLayoutTuesday.setBackgroundColor(Color.WHITE);
                mLayoutWendsday.setBackgroundColor(Color.WHITE);
                mLayoutThursday.setBackgroundColor(Color.WHITE);
                mLayoutFriday.setBackgroundColor(Color.WHITE);

            }
            break;
            case 7: {    //  토
                mLayoutMonday.setBackgroundColor(Color.WHITE);
                mLayoutTuesday.setBackgroundColor(Color.WHITE);
                mLayoutWendsday.setBackgroundColor(Color.WHITE);
                mLayoutThursday.setBackgroundColor(Color.WHITE);
                mLayoutFriday.setBackgroundColor(Color.WHITE);
                mLayoutSaturday.setBackgroundColor(Color.WHITE);
//                mLayoutSunday.setBackgroundColor(Color.WHITE);
            }
            break;

            default: {
                Log.d(TAG, "날짜 에러--------");

            }
            break;
        }
    }

    @SuppressLint("ResourceType")
    private void checkRoundToday(String strDay) {
        mIvMondayCheck.setVisibility(View.INVISIBLE);
        mIvTuedayCheck.setVisibility(View.INVISIBLE);
        mIvWendayCheck.setVisibility(View.INVISIBLE);
        mIvThudayCheck.setVisibility(View.INVISIBLE);
        mIvFridayCheck.setVisibility(View.INVISIBLE);
        mIvSatdayCheck.setVisibility(View.INVISIBLE);
        mIvSundayCheck.setVisibility(View.INVISIBLE);

        mLayoutMonday.setBackgroundResource(R.drawable.c_round_ui_shape_white_);

        if (strDay.equals("월")) {
            mIvMondayCheck.setVisibility(View.VISIBLE);
            mLayoutMonday.setBackgroundResource(R.drawable.c_circle_ui_shape_white);
        } else if (strDay.equals("화")) {
            mIvTuedayCheck.setVisibility(View.VISIBLE);
            mLayoutTuesday.setBackgroundResource(R.drawable.c_round_ui_shape_white);
        } else if (strDay.equals("수")) {
            mIvWendayCheck.setVisibility(View.VISIBLE);
            mLayoutWendsday.setBackgroundResource(R.drawable.c_round_ui_shape_white);
        } else if (strDay.equals("목")) {
            mIvThudayCheck.setVisibility(View.VISIBLE);
            mLayoutThursday.setBackgroundResource(R.drawable.c_round_ui_shape_white);
        } else if (strDay.equals("금")) {
            mIvFridayCheck.setVisibility(View.VISIBLE);
            mLayoutFriday.setBackgroundResource(R.drawable.c_round_ui_shape_white);
        } else if (strDay.equals("토")) {
            mIvSatdayCheck.setVisibility(View.VISIBLE);
            mLayoutSaturday.setBackgroundResource(R.drawable.c_round_ui_shape_white);
        } else if (strDay.equals("일")) {
            mIvSundayCheck.setVisibility(View.VISIBLE);
            mLayoutSunday.setBackgroundResource(R.drawable.c_round_ui_shape_white);
        }
    }

    private void setCalendar() {
        Calendar curCal = Calendar.getInstance();
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();
        String strStart = "", strEnd = "";

        if (curCal.get(Calendar.DAY_OF_WEEK) == 1) {
            startCal.add(startCal.DATE, -6);
        } else {
            startCal.add(startCal.DATE, 2 - startCal.get(Calendar.DAY_OF_WEEK));
            endCal.add(endCal.DATE, 8 - endCal.get(Calendar.DAY_OF_WEEK));
        }

        strStart = CommonUtil.getCurrentYYYYMMDD2(startCal);
        strEnd = CommonUtil.getCurrentYYYYMMDD2(endCal);

        //날짜별 학습양/결과
        mWeekCalendarUnit = mDatabaseUtil.selectWeekCalendarUnit(mCustomer.mCustomerNo, strStart, strEnd);
        Log.d(TAG, "----------주간 달력 정보 size:" + mWeekCalendarUnit.size());
        if (mWeekCalendarUnit.size() < 7)
            return;

        DatabaseData.WeekCalendarUnit mon = mWeekCalendarUnit.get(0);
        DatabaseData.WeekCalendarUnit tue = mWeekCalendarUnit.get(1);
        DatabaseData.WeekCalendarUnit wed = mWeekCalendarUnit.get(2);
        DatabaseData.WeekCalendarUnit thu = mWeekCalendarUnit.get(3);
        DatabaseData.WeekCalendarUnit fri = mWeekCalendarUnit.get(4);
        DatabaseData.WeekCalendarUnit sat = mWeekCalendarUnit.get(5);
        DatabaseData.WeekCalendarUnit sun = mWeekCalendarUnit.get(6);

        //스마일 셋팅
        setSmileMark(mon, mTvMon, mTvMonText, 0);
        setSmileMark(tue, mTvTue, mTvTueText, 1);
        setSmileMark(wed, mTvWed, mTvWedText, 2);
        setSmileMark(thu, mTvThu, mTvThuText, 3);
        setSmileMark(fri, mTvFri, mTvFriText, 4);
        setSmileMark(sat, mTvSat, mTvSatText, 5);
        setSmileMark(sun, mTvSun, mTvSunText, 6);
    }

    private void setSmileMark(DatabaseData.WeekCalendarUnit unit, TextView date, TextView text, int index) {
        //스마일 도장 텍스트 초기화
        date.setBackground(null);
        date.setText("");
        text.setText("");
/*
        Calendar curCal = Calendar.getInstance();
        //요일 일요일 :1, 월요일 :2 ... 토요일 :7
        int todayIndex = curCal.get(Calendar.DAY_OF_WEEK);

        //일요일일 때만 일요일을 8로 변경하여 처리
        if (unit.mWeekDay == 1) {
            unit.mWeekDay = 8;
        }
        if (todayIndex == 1)
            todayIndex = 8;
*/

        boolean isStudyDone = false;
        if (CommonUtil.isCenter()) {
            try {
                if (mWeekStudyResult != null && mWeekStudyResult.getJSONObject(index).getInt("IS_CLASS") > 0) {
                    isStudyDone = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (mWeekStudyR != null && mWeekStudyR.get(index) > 0) {
                isStudyDone = true;
            }
        }

        if (isStudyDone) {
            if (CommonUtil.isCenter()) // Igse
                date.setBackground(getResources().getDrawable(R.drawable.igse_img_main_stamp_01));
            else {
                if ("1".equals(Preferences.getLmsStatus(this))) {
                    date.setBackground(getResources().getDrawable(R.drawable.w_img_main_stamp_01));
                } else {
                    date.setBackground(getResources().getDrawable(R.drawable.f_img_main_stamp_01));
                }
            }
            text.setText(unit.mDisplayText);
        } else {
            date.setText(unit.mDate);
            text.setText(unit.mDisplayText.substring(0, 3));
        }

/*
        // 오늘 이후의 날에 학습이 있는 경우
        if (todayIndex != 1 && todayIndex < unit.mWeekDay && unit.mWeekStudyCnt > 0) {
            date.setBackground(getResources().getDrawable((R.drawable.img_main_stamp_04)));
            text.setText(unit.mDisplayText);
        }
        // 오늘 이후의 날에 학습이 없는 경우
        else if (todayIndex != 1 && todayIndex < unit.mWeekDay && unit.mWeekStudyCnt == 0) {
            date.setText(unit.mDate);
            text.setText(unit.mDisplayText.substring(0, 3));
        }
        //오늘 및 이전날 처리
        else if (todayIndex != 1 && todayIndex >= unit.mWeekDay) {
            // 학습 완료(학습일아닐때 학습한경우 or 학습날 모두 학습)
            if ((unit.mWeekStudyCnt == 0 && unit.mStudyUnitCnt > 0) || (unit.mWeekStudyCnt > 0 && unit.mWeekStudyCnt <= unit.mStudyUnitCnt)) {
                date.setBackground(getResources().getDrawable((R.drawable.img_main_stamp_01)));
                text.setText(unit.mDisplayText);
            }
            // 학습 부분 완료(학습양이 있고,  학습완료를 다 못함)
            //else if (unit.mWeekStudyCnt > 0 && unit.mStudyUnitCnt > 0 && unit.mWeekStudyCnt > unit.mStudyUnitCnt) {
            else if (unit.mWeekStudyCnt > 0 && unit.mStudyUnitCnt > 0 && unit.mWeekStudyCnt > unit.mStudyUnitCnt) {
                date.setBackground(getResources().getDrawable((R.drawable.img_main_stamp_02)));
                text.setText(unit.mDisplayText);
            }
            // 학습 미완료(학습양이 있고 완료가 없음)
            else if (unit.mWeekStudyCnt > 0 && unit.mStudyUnitCnt == 0) {
                if( todayIndex == unit.mWeekDay )//오늘일때는 회색
                {
                    date.setBackground(getResources().getDrawable((R.drawable.img_main_stamp_04)));
                    text.setText(unit.mDisplayText);
                }
                else {
                    date.setBackground(getResources().getDrawable((R.drawable.img_main_stamp_03)));
                    text.setText(unit.mDisplayText);
                }
            }
            // 학습날이 아닌날
            else {
                date.setText(unit.mDate);
                text.setText(unit.mDisplayText.substring(0, 3));
            }
        } else {
            date.setText(unit.mDate);
            text.setText(unit.mDisplayText.substring(0, 3));
        }
        */
    }

    /**
     * 중단 학습 메시지 박스
     *
     * @param msg : 메시지
     */
    private void showStoppedStudyNotiDialog(String msg) {
        hideProgressDialog();

        mMsgBox = new MessageBox(this, 0, msg);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_STOPPED_STUDY_NOTI);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 학습 진행 progress 화면 보여줌
     */
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_studying_file_preparing);
        }
    }

    /**
     * 학습 진행 progress 화면 숨김
     */
    private void hideProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * 2차시 시작(GETS->QINF)
     */
    private void startSecStudy() {

        if (mSecMaxCnt != 0 && mSecCurCnt != mSecMaxCnt) {//카운트가 0이아닌데 같다면
            /**
             * Qinf생성
             */
            reqQinf = new RequestPhpPost(mContext) {
                @Override
                protected void onPostExecute(Map<String, String> result) {
                    super.onPostExecute(result);
                    makeSecStudyData(result);
                }
            };
            /**
             * Gets생성
             */
            RequestPhpPost reqGets = new RequestPhpPost(mContext) {
                @Override
                protected void onPreExecute() {
                    // TODO Auto-generated method stub
                    super.onPreExecute();
                    if (mProgressDialog == null) {
                        mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_book_download_progress);
                    }
                }

                @Override
                protected void onPostExecute(Map<String, String> result) {
                    super.onPostExecute(result);
					/*	int last_detail_id = 0;
						try {
							last_detail_id = Integer.valueOf(result.get("last_book_detail_id"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						reqQinf.getQINF(last_detail_id);//마지막 차시//나중에
					*/

                    reqQinf.getQINF(0);//마지막 차시//민트패드에서는 0으로 넘김
                }
            };

            reqGets.getGETS();

            //req.onSyncPostExecute(result);
			/*startActivity(new Intent(this, SecondIntroActivity.class));
			finish();*/
        } else {
            showMaxSecondStudy();
        }
    }

    private void showMaxSecondStudy() {
        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sec_max_study));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_COMPLETE);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private void makeSecStudyData(Map<String, String> result) {

        RequestQUSTPhpPost reqQust = new RequestQUSTPhpPost(mContext) {
            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                try {
                    int val = values[0];
                    String ment = null;
                    if (0 <= val && val <= 39) {
                        ment = getString(R.string.string_book_download_progress3);
                    } else if (40 <= val && val <= 59) {
                        ment = getString(R.string.string_book_download_progress3);
                    } else if (60 <= val && val <= 79) {
                        ment = getString(R.string.string_book_download_progress3);
                    } else if (80 <= val) {
                        ment = getString(R.string.string_book_download_progress3);
                    }
                    mProgressDialog.setMessage(val + "%\n\n" + ment);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onPostExecute(SecStudyData result) {
                super.onPostExecute(result);
                try {
                    if (null != mProgressDialog && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                    if (result.getException() != null) {

                        if (ServiceCommon.CONTENT_MODE == ServiceCommon.REAL_CONTENT) {
                            Crashlytics.logException(result.getException());
                        }
                        showNotDownloadSecondStudy();
                        return;

                    }
                    //2교시 자료 생성
                    SecStudyData.getInstance().setInstance(result);

                    SecStudyData data = SecStudyData.getInstance();

                    // 학습 옵션
                    /*DatabaseData.StudyOption option = mDatabaseUtil.selectStudyOption(data.getMcode());
                    //data.set_caption(option.mIsCaption);
                    data.set_caption(true);*/
                    //시작
                    startActivity(new Intent(mContext, SecIntroActivity.class));
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (null != mProgressDialog && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                }
            }
        };
        try {
            result.putAll(qGetMap);
            reqQust.getQUST(result);
        } catch (Exception e) {
            e.printStackTrace();
            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
            showNotconfigSecondStudy();
        }
    }

    private void showNotDownloadSecondStudy() {
        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sec_enough_study));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_SEC_ERROR);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private void showNotconfigSecondStudy() {
        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sec_config_study));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_COMPLETE);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }
}
