package com.yoons.fsb.student.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ImageDownloader;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 복습 간지 화면
 *
 * @author jaek
 */
public class ReStudyTitlePaperActivity extends BaseActivity implements OnClickListener {

    private Button mConfirmBtn = null;
    private StudyData mStudyData = null;
    private TextView mActiveName = null;
    private int wordCount = 0, sentenceCount = 0;

    // 복습 간지에서 이동할 화면
    private int mTargetActivity = 0;

    private final static String TAG = "[ReStudyTitlePaperActivity]";
    private HttpJSONRequest request;

    private ImageDownloader mImageDownloader = null;
    private ImageView mThumbB = null, mThumbW = null, mThumbS = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*if (getIntent().getBooleanExtra(ServiceCommon.PARAM_GO_SETTING, false)) */
        /*startActivity(new Intent(this, SettingsActivity.class));*/

        if (CommonUtil.isCenter()) {
            // Igse
            setContentView(R.layout.igse_restudy_title_paper);
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) {
                //우영
                setContentView(R.layout.w_restudy_title_paper);
            } else {
                // 숲
                setContentView(R.layout.f_restudy_title_paper);
            }
        }

        mActiveName = (TextView) findViewById(R.id.active_name);

        mTargetActivity = getIntent().getIntExtra(ServiceCommon.TARGET_ACTIVITY, 0);

        if (!getIntent().getBooleanExtra(ServiceCommon.PARAM_MUTE_GUIDE, false)) {
            if (mTargetActivity == ServiceCommon.STATUS_REVIEW_AUDIO)
                setGuideMsg(R.raw.b_11);
            else if (mTargetActivity == ServiceCommon.STATUS_REVIEW_WORD)
                setGuideMsg(R.raw.b_12);
            else
                setGuideMsg(R.raw.b_20);
        }

        mStudyData = StudyData.getInstance();

        setReStudyStatusEnable();

        mSyncHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);

        //GA적용
        Crashlytics.log(getString(R.string.string_ga_ReStudyTitlePaperActivity));
        request = new HttpJSONRequest(this);
        request.requestNoti(mNotiHandler, ServiceCommon.MSG_WHAT_CALL_NOTI);
        //StudyDataUtil.setCurrentStudyStatus(this, "R01");

    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                mConfirmBtn.setPressed(true);
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                mConfirmBtn.setPressed(false);
                goNextStatus();
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (singleProcessChecker())
            return;

        int id = v.getId();

        if (id == R.id.restudy_title_paper_notitle_parent_layout) {
        } else if (id == R.id.restudy_title_paper_confirm_btn) {
            goNextStatus();
        }
    }

    /**
     * 복습 간지 화면을 설정함
     */
    private void setReStudyStatusEnable() {
        setTitlebarCategory(getString(R.string.string_titlebar_category_restudy));
        setTitlebarText(mStudyData.mReviewProductName);
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        wordCount = mStudyData.mReviewWordQuestion.size();
        sentenceCount = mStudyData.mReviewSentenceQuestion.size();

        if (mTargetActivity == ServiceCommon.STATUS_REVIEW_AUDIO) {
            mActiveName.setText(R.string.string_audio_restudy);

            if (wordCount > 0) {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_1)).setText(String.valueOf(wordCount));
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_1)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_alert));
            } else {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_1)).setText("");
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_1)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_off));
            }

            if (sentenceCount > 0) {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_1)).setText(String.valueOf(sentenceCount));
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_1)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_alert));
            } else {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_1)).setText("");
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_1)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_off));
            }

            //setWordRestudyDisable();
            //setSentenceRestudyDisable();
        } else if (mTargetActivity == ServiceCommon.STATUS_REVIEW_WORD) {

            if (sentenceCount > 0) {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_2)).setText(String.valueOf(sentenceCount));
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_2)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_alert));
            } else {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_2)).setText("");
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_2)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_off));
            }

            mActiveName.setText(R.string.string_word_restudy);
            ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_2)).setText(String.valueOf(wordCount));

            findViewById(R.id.restudy_first_layout).setVisibility(View.GONE);
            findViewById(R.id.restudy_word_layout).setVisibility(View.VISIBLE);

            if (mStudyData.mReviewIsBodyExist || mStudyData.mReviewISBodyVideoExist) {
                if (CommonUtil.isCenter()) {
                    // Igse
                    ((ImageView) (findViewById(R.id.restudy_word_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                } else {
                    if ("1".equals(Preferences.getLmsStatus(this))) {
                        //우영
                        ((ImageView) (findViewById(R.id.restudy_word_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.w_img_cover_chapter_complete));
                    } else {
                        // 숲
                        ((ImageView) (findViewById(R.id.restudy_word_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.f_img_cover_chapter_complete));
                    }
                }
            } else {
                ((ImageView) (findViewById(R.id.restudy_word_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_off));
            }

            //setAudiodRestudyDisable();
            //setSentenceRestudyDisable();
        } else if (mTargetActivity == ServiceCommon.STATUS_REVIEW_SENTENCE) {

            if (wordCount > 0) {
                if (CommonUtil.isCenter()) {
                    ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_3)).setBackground(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                } else {
                    if ("1".equals(Preferences.getLmsStatus(this))) {
                        ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_3)).setBackground(getResources().getDrawable(R.drawable.w_img_cover_chapter_complete));
                    } else {
                        ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_3)).setBackground(getResources().getDrawable(R.drawable.f_img_cover_chapter_complete));
                    }
                }
            } else {
                ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_3)).setBackground(getResources().getDrawable(R.drawable.c_img_cover_chapter_off));
            }

            mActiveName.setText(R.string.string_sentence_restudy);
            ((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text_3)).setText("");
            ((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text_3)).setText(String.valueOf(sentenceCount));
            findViewById(R.id.restudy_first_layout).setVisibility(View.GONE);
            findViewById(R.id.restudy_sentence_layout).setVisibility(View.VISIBLE);

            if (mStudyData.mReviewIsBodyExist || mStudyData.mReviewISBodyVideoExist) {
                if (CommonUtil.isCenter()) {
                    // Igse

                    ((ImageView) (findViewById(R.id.restudy_sentence_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                } else {
                    if ("1".equals(Preferences.getLmsStatus(this))) {
                        //우영
                        ((ImageView) (findViewById(R.id.restudy_sentence_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.w_img_cover_chapter_complete));
                    } else {
                        // 숲
                        ((ImageView) (findViewById(R.id.restudy_sentence_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.f_img_cover_chapter_complete));
                    }
                }
            } else {
                ((ImageView) (findViewById(R.id.restudy_sentence_layout).findViewById(R.id.first_point))).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_off));
            }

            //setAudiodRestudyDisable();
            //setWordRestudyDisable();
        }

        mThumbB = ((ImageView) findViewById(R.id.last_point_b));
        mThumbW = ((ImageView) findViewById(R.id.last_point_w));
        mThumbS = ((ImageView) findViewById(R.id.last_point_s));

        mImageDownloader = new ImageDownloader(this);
        mImageDownloader.clearCache();
        mImageDownloader.setOnDownloadComplete(new ImageDownloader.DownloadCompleteListener() {

            @Override
            public void onDownloadComplete(ImageView imageView, Bitmap bitmap) {
                try {
                    if(bitmap == null) {
                        mThumbB.setImageResource(R.drawable.book_thumbnail_default);
                        mThumbW.setImageResource(R.drawable.book_thumbnail_default);
                        mThumbS.setImageResource(R.drawable.book_thumbnail_default);
                    } else {
                        mThumbB.setImageBitmap(bitmap);
                        mThumbW.setImageBitmap(bitmap);
                        mThumbS.setImageBitmap(bitmap);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (CommonUtil.isSMT330Device()) {
            TextView tv1 = (TextView) findViewById(R.id.text_guide);

            if (mTargetActivity == ServiceCommon.STATUS_REVIEW_AUDIO) {
                tv1.setText(R.string.string_device_only_smart_restudy_message);
                tv1.setVisibility(View.VISIBLE);
            } else if (mTargetActivity == ServiceCommon.STATUS_REVIEW_WORD) {
                tv1.setText(R.string.string_device_only_reword_message);
                tv1.setVisibility(View.VISIBLE);
            }
        }

        //((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text)).setText(String.valueOf(mStudyData.mReviewWordQuestion.size()));
        //((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text)).setText(String.valueOf(mStudyData.mReviewSentenceQuestion.size()));

        //((ImageView) findViewById(R.id.last_point)).setImageResource(0);
        String thumbnail = ServiceCommon.THUMBNAIL_URL +  String.format("%d_%d.jpg", mStudyData.mSeriesNo, mStudyData.mBookNo);
        mImageDownloader.download(thumbnail, mThumbB);
        mImageDownloader.download(thumbnail, mThumbW);
        mImageDownloader.download(thumbnail, mThumbS);

        mConfirmBtn = (Button) findViewById(R.id.restudy_title_paper_confirm_btn);
        findViewById(R.id.restudy_title_paper_notitle_parent_layout).setClickable(true);
        findViewById(R.id.restudy_title_paper_notitle_parent_layout).setOnClickListener(this);
        mConfirmBtn.setOnClickListener(this);
    }

    /**
     * 본문 듣기를 비활성화 처리함
     */
    private void setAudiodRestudyDisable() {
        //((ImageView) findViewById(R.id.restudy_title_paper_audio_icon)).setImageDrawable(getResources().getDrawable(R.drawable.icon_word_restudy_dis));
        //((TextView) findViewById(R.id.restudy_title_paper_audio_text)).setTextColor(Color.parseColor("#CDCDCD"));
    }

    /**
     * 단어 보충을 비활성화 처리함
     */
    private void setWordRestudyDisable() {
        //((ImageView) findViewById(R.id.restudy_title_paper_word_icon)).setImageDrawable(getResources().getDrawable(R.drawable.icon_word_restudy_dis));
        // ((ImageView) findViewById(R.id.restudy_title_paper_wrong_word_count_bg)).setImageDrawable(getResources().getDrawable(R.drawable.wrong_count_bg_dis));
        //((TextView) findViewById(R.id.restudy_title_paper_word_text)).setTextColor(Color.parseColor("#CDCDCD"));
        //((TextView) findViewById(R.id.restudy_title_paper_wrong_word_count_text)).setTextColor(Color.parseColor("#90B03E"));
    }

    /**
     * 문장 보충을 비활성화 처리함
     */
    private void setSentenceRestudyDisable() {
        //((ImageView) findViewById(R.id.restudy_title_paper_sentence_icon)).setImageDrawable(getResources().getDrawable(R.drawable.icon_sentence_restudy_dis));
        //((ImageView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_bg)).setImageDrawable(getResources().getDrawable(R.drawable.wrong_count_bg_dis));
        //((TextView) findViewById(R.id.restudy_title_paper_sentence_text)).setTextColor(Color.parseColor("#C7C7C7"));
        //((TextView) findViewById(R.id.restudy_title_paper_wrong_sentence_count_text)).setTextColor(Color.parseColor("#ffffff"));
    }

    /**
     * 다음 단계로 이동 처리함
     */
    private void goNextStatus() {

        if (isNext) {
            isNext = false;
            if (mTargetActivity == ServiceCommon.STATUS_REVIEW_AUDIO)
                startActivity(new Intent(this, AudioReStudyActivity.class));
            else if (mTargetActivity == ServiceCommon.STATUS_REVIEW_WORD)
                startActivity(new Intent(this, WordReStudyActivity.class));
            else if (mTargetActivity == ServiceCommon.STATUS_REVIEW_SENTENCE)
                startActivity(new Intent(this, SentenceReStudyActivity.class));

            finish();
        }
    }

    /**
     * Upload Sync를 시작함
     */
    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mSyncHandler, true, false);
        }
    }

    /**
     * Sync Handler
     */
    public Handler mSyncHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START:
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS:
                    Log.e(TAG, "MSG_DATABASE_UPLOAD_SYNC_SUCCESS");
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL:
                    Log.e(TAG, "MSG_DATABASE_UPLOAD_SYNC_FAIL");
                    break;
            }

            super.handleMessage(msg);
        }
    };

    public Handler mNotiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.arg1) {
                case ServiceCommon.MSG_WHAT_CALL_NOTI:

                    String status = "";

                    if (msg.obj != null) {
                        JSONObject obj = (JSONObject) (msg.obj);
                        try {
                            JSONArray array = new JSONArray(obj.getString("out1"));

                            for (int i = 0; i < array.length(); i++) {

                                status = array.getJSONObject(i).getString("command");

                                if (status.equals("INITIALIZE")) {//중복로그인
                                    ((Activity) mContext).finish();
                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        /* first_Check=false; */
                    }

                    break;
            }

            super.handleMessage(msg);
        }
    };
}
