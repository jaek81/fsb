package com.yoons.fsb.student.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomWavRecoder;
import com.yoons.fsb.student.custom.CustomVorbisPlayer;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.base.BaseStudyActivity;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.FlipAnimation;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.vanishing.VanishingTitlePaperActivity;
import com.yoons.recognition.OnMSTT;
import com.yoons.recognition.VoiceRecognizerWeb;
import com.yoons.recognition.data.SentenceObj;
import com.yoons.recognition.util.NumberUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;

/**
 * 문장시험 화면
 *
 * @author jaek
 */
public class SentenceExamActivity extends BaseStudyActivity implements OnClickListener, OnCompletionListener, OnPreparedListener {

    private LinearLayout mNormallevel, mWrong = null;
    private RelativeLayout mParentLayout = null;
    //private StepStatusBar mStepStatusBar = null;
    private ImageView mListen = null, mRecord = null, mRecordPlay = null;
    private TextView mResultView = null, mResultView2 = null, mResultTime = null;
    private TextView mStudyView = null, mStudyCountView = null;
    private TextView mVoiceResult = null;
    private TextView mWrongStudyView = null, mWrongStudyCountView = null;
    private Button mResultConfirm = null, mNextStatus = null;
    private LoadingDialog mLoadingDialog = null;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;
    private FrameLayout fSentenceStudy = null;

    // 타이머
    private long startTime = 0L;
    long timeInMilliseconds = 0L;
    long updatedTime = 0L;

    /**
     * 2014-02-14 WCPM 적용 지역 변수 였으나 원어민 파일의 재생 시간이 필요 하여 멤버 변수로 선언
     */
    int playDuration = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_sentence_exam_main);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                setContentView(R.layout.w_sentence_exam_main);
            else // 숲
                setContentView(R.layout.f_sentence_exam_main);
        }

        mStudyData = StudyData.getInstance();
        mRecordFilePath = ServiceCommon.CONTENTS_PATH + ServiceCommon.WRONG_SENTENCE_RECORD_FILE_NAME + ServiceCommon.WAV_FILE_TAIL;

        if (!checkValidStudyData())
            return;

        setWidget();

        // TitleView
        tBookName.setText(mStudyData.mProductName);
        tCategory.setText(R.string.string_common_today_study);
        tCategory2.setText(R.string.string_restudy_wrong_sentence_title);
        ((TextView) findViewById(R.id.step_status_text)).setText(R.string.string_restudy_wrong_sentence_title);

        if (ServiceCommon.IS_CONTENTS_TEST) {
            readyStudy();
        } else {
            requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);
        }

//		requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);

//		StudyDataUtil.setCurrentStudyStatus(this, "S41");
//		mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);

        //GA적용
        Crashlytics.log(getString(R.string.string_ga_SentenceExamActivity));


    }

    @Override
    protected void onStart() {
        super.onStart();
        startTime = SystemClock.uptimeMillis();
    }

    @Override
    protected void onPause() {
        // 학습 중 화면 잠금, 꺼짐 시 간지로 재시작
        if (!isFinishing()) {
            if (mIsReStart) {
                StudyDataUtil.clearSentenceResult();
                startActivity(new Intent(this, SentenceTitlePaperActivity.class)
                        .putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
            }

            setFinish();
            finish();
        }

        super.onPause();
    }

    /**
     * 화면 구성 완료 후 호출됨 학습 가이드의 Width, Height 값을 저장함
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if (hasFocus) {
            View tooltip = findViewById(R.id.tooltip_record_text);
            if (null != tooltip) {
                mTooltipHeight = tooltip.getHeight();
                mTooltipWidth = tooltip.getWidth();
                mParentLayout.removeView(tooltip);
            }
        }
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.sentence_study_ing_layout).getVisibility()) {
                    if (mRecord.isEnabled() && mIsRecording)
                        mRecord.setPressed(true);
                } else if (View.VISIBLE == findViewById(R.id.sentence_study_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(true);
                } else if (View.VISIBLE == findViewById(R.id.sentence_study_wrong_layout).getVisibility()) {
                    if (View.VISIBLE == findViewById(R.id.sentence_study_wrong_next_status_layout).getVisibility())
                        mNextStatus.setPressed(true);
                    else {
                        if (mRecord.isEnabled() && mIsRecording)
                            mRecord.setPressed(true);
                    }
                }
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.sentence_study_ing_layout).getVisibility()) {
                    mRecord.setPressed(false);
                    if (mRecord.isEnabled() && mIsRecording)
                        recordEnd();
                } else if (View.VISIBLE == findViewById(R.id.sentence_study_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(false);
                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        goNextStatus();
                    } else {
                        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                    }
                } else if (View.VISIBLE == findViewById(R.id.sentence_study_wrong_layout).getVisibility()) {
                    if (View.VISIBLE == findViewById(R.id.sentence_study_wrong_next_status_layout).getVisibility()) {
                        mNextStatus.setPressed(false);
                        if (ServiceCommon.IS_CONTENTS_TEST) {
                            goNextStatus();
                        } else {
                            requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                        }
                    } else {
                        mRecord.setPressed(false);
                        if (mRecord.isEnabled() && mIsRecording)
                            recordEnd();
                    }
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (!mIsStudyStart)
            return;

        if (singleProcessChecker())
            return;

        int id = v.getId();

        switch (id) {
            case R.id.sentence_study_record_btn:
            case R.id.sentence_study_wrong_record_btn:
                if (mIsRecording)
                    recordEnd();
                else
                    recordReady(0);
                break;

            case R.id.sentence_study_result_confirm_btn:
            case R.id.sentence_study_wrong_next_status_btn:
                goNextStatus();
                break;
            /*
             * case R.id.titlebar_btn_setting: // 媛꾩?濡??ъ떆??
             * StudyDataUtil.clearSentenceResult(); startActivity(new Intent(this,
             * SentenceTitlePaperActivity.class)
             * .putExtra(ServiceCommon.PARAM_GO_SETTING, true)
             * .putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true)); break;
             */
        }
    }

    /**
     * StudyData의 유효성을 검사함
     */
    private boolean checkValidStudyData() {
        boolean isValid = true;

        if (null == mStudyData) {
            isValid = false;
            Log.e("", "checkValidStudyData studyData is null !!");
        } else {
            if (mStudyData.mSentenceQuestion.isEmpty()) {
                isValid = false;
                Log.e("", "checkValidStudyData mSentenceQuestion is empty !!");
            } else {
                for (int i = 0; i < mStudyData.mSentenceQuestion.size(); i++) {
                    StudyData.SentenceQuestion sq = mStudyData.mSentenceQuestion.get(i);
                    File fileYda = new File(sq.mSoundFile);
                    if (0 >= sq.mSentence.length() || !fileYda.exists()) {
                        isValid = false;
                        break;
                    }
                }
            }
        }

        if (!isValid)
            Toast.makeText(this, getString(R.string.string_common_study_data_error), Toast.LENGTH_SHORT).show();

        return isValid;
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {
        fSentenceStudy = (FrameLayout) findViewById(R.id.sentence_study);

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        mParentLayout = (RelativeLayout) findViewById(R.id.sentence_exam_parent_layout);

        mNormallevel = (LinearLayout) findViewById(R.id.sentence_study_normallevel_question_layout);
        mWrong = (LinearLayout) findViewById(R.id.sentence_study_wrong_question_layout);
        mStudyView = (TextView) findViewById(R.id.sentence_study_question_text);
        mStudyCountView = (TextView) findViewById(R.id.sentence_study_question_count_text);
        mListen = (ImageView) findViewById(R.id.sentence_study_listen_btn);
        mRecord = (ImageView) findViewById(R.id.sentence_study_record_btn);
        mRecordPlay = (ImageView) findViewById(R.id.sentence_study_recordplay_btn);
        //mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);
        mResultView = (TextView) findViewById(R.id.sentence_study_result_wrong_info_text);
        mResultView2 = (TextView) findViewById(R.id.sentence_study_result_wrong_info_text2);
        mResultConfirm = (Button) findViewById(R.id.sentence_study_result_confirm_btn);
        mWrongStudyView = (TextView) findViewById(R.id.sentence_study_wrong_question_text);
        mWrongStudyCountView = (TextView) findViewById(R.id.sentence_study_wrong_question_count_text);
        mNextStatus = (Button) findViewById(R.id.sentence_study_wrong_next_status_btn);
        mVoiceResult = (TextView) findViewById(R.id.voicerecognize_result);
        mResultTime = (TextView) findViewById(R.id.sentence_study_result_time_text);
        mRecord.setOnClickListener(this);
        mResultConfirm.setOnClickListener(this);
        mNextStatus.setOnClickListener(this);
        mListen.setEnabled(false);
        mRecord.setEnabled(false);
        mRecordPlay.setEnabled(false);
        mTotalStudyCount = mStudyData.mSentenceQuestion.size();

        setTitlebarCategory(getString(R.string.string_titlebar_category_study));
        setTitlebarText(mStudyData.mProductName);
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        //mStepStatusBar.setHighlights(StepStatusBar.STATUS_SENTENCE_EXAM);

        findViewById(R.id.sentence_study_highlevel_question_layout).setVisibility(mStudyData.mIsCaption ? View.GONE : View.VISIBLE);
        findViewById(R.id.sentence_study_normallevel_question_layout).setVisibility(mStudyData.mIsCaption ? View.VISIBLE : View.GONE);

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);
        tooltip.setVisibility(View.INVISIBLE);
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));
        tipText.setText(getResources().getString(R.string.string_record_tool_tip));
        mParentLayout.addView(tooltip);
    }

    /**
     * Recorder를 설정함
     *
     * @param wavFilePath 녹음 파일 저장 경로
     */
    private void setRecorder(String wavFilePath) {
        mWavRecorder = new CustomWavRecoder(wavFilePath);
    }

    /**
     * Player를 설정함
     *
     * @param oggFilePath 재생 파일 경로
     */
    private void setPlayer(String oggFilePath) {
        mVorbisPlayer = new CustomVorbisPlayer(mHandler, oggFilePath);
    }

    /**
     * Player를 Resume함
     */
    private void playerResume() {
        mVorbisPlayer.play();
        if (CommonUtil.isCenter()) // igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen2));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen2));
            else // 숲
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen2));
        }

        listenIngEffect();
    }

    private void listenIngEffect() {
        int duration = mVorbisPlayer.getDuration();
        AnimationDrawable ad = null;
        final LinearLayout parent = ((LinearLayout) mListen.getParent());
        if (CommonUtil.isCenter()) // igse
            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.w_seq_learning_progress);
            else // 숲
                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
        }
        final AnimationDrawable ing = new AnimationDrawable();
        final int frameTime = duration / ad.getNumberOfFrames();

        for (int i = 0; i < ad.getNumberOfFrames(); i++){
            Drawable draw =  ad.getFrame(i);
            ing.addFrame(draw, frameTime);
        }
        ing.setOneShot(true);
        parent.setBackground(ing);
        ((AnimationDrawable)parent.getBackground()).start();
    }

    private void playerPauseAndStop() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);
        LinearLayout parent = ((LinearLayout) mListen.getParent());
        try {
            AnimationDrawable aniDraw = (AnimationDrawable)parent.getBackground();
            aniDraw.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        parent.setBackground(null);
        if (CommonUtil.isCenter()) // igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen));
            else // 숲
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen));
        }
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        //Log.e("hy", "playerEnd");
        mVorbisPlayer.stop();
        playerPauseAndStop();
        mVorbisPlayer.release();
        mVorbisPlayer = null;
    }

    /**
     * Player 종료 후 녹음을 녹음을 마치면 OK를준비함
     */
    private void playerComplete() {
        if (findViewById(R.id.sentence_study_wrong_layout).getVisibility() == View.VISIBLE) {
            mCurPlayCount++;
            // 틀린 문장은 음성 2번 들려준 후 녹음 시작  => 1번으로 변경 6/21
//			if (2 >= mCurPlayCount) {
            if (1 >= mCurPlayCount) {
                mVorbisPlayer.pause();
                mVorbisPlayer.seekTo(0);
                mVorbisPlayer.play();
                return;
            }
        }

        /**
         * 2014-02-14 WCPM 적용
         */
        playDuration = mVorbisPlayer.getDuration();

        playerEnd();

        recordReady(getMaxRecordTime(ServiceCommon.MAX_REC_TIME_20SEC, playDuration));
    }

    /**
     * 문장 학습 Flip Animation Listener
     */
    private AnimationListener studyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            findViewById(R.id.sentence_study_question_count_text).setVisibility(View.VISIBLE);
            setStudy();
            playerResume();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 문제 전환용 애니메이션을 시작함
     */
    private void startStudyAni() {
        findViewById(R.id.sentence_study_question_count_text).setVisibility(View.GONE);
        Animation ani = new FlipAnimation(180f, 0f, mStudyView.getWidth() / 2, mStudyView.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(studyAniListener);
        mStudyView.startAnimation(ani);

        /*Animation ani = new FlipAnimation(180f, 0f, fSentenceStudy.getWidth() / 2, fSentenceStudy.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(studyAniListener);
        fSentenceStudy.startAnimation(ani);*/
    }

    /**
     * 학습을 시작함
     *
     * @param filePath 문장 파일 경로
     */
    private void startStudy(String filePath) {
        mStudyView.setText("");

        setPlayer(filePath);

        if (mStudyData.mIsCaption && 0 < mCurStudyIndex) {
            startStudyAni();
        } else {
            setStudy();
            playerResume();
            if (!mIsStudyStart)
                mIsStudyStart = true;
        }
    }

    /**
     * 문장 문제를 표시함
     */
    private void setStudy() {
        if (mStudyData.mIsCaption) {
            String sentenceQuestion = mStudyData.mSentenceQuestion.get(mCurStudyIndex).mSentence.trim();

            Log.i("", "setStudy sentenceQuestion length => " + sentenceQuestion.length());

            mStudyView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ServiceCommon.MAX_SENTENCE_LENGTH < sentenceQuestion.length() ? getResources().getDimensionPixelSize(R.dimen.dimen_22) : getResources().getDimensionPixelSize(R.dimen.dimen_34));

            mStudyView.setText(sentenceQuestion);
        }

        mStudyCountView.setText(mCurStudyIndex + 1 + "/" + mTotalStudyCount);
    }

    /**
     * 다음 문제를 설정함 마지막 문제 이후엔 음성 인식 채점을 요청함
     */
    private void nextStudy() {
        mCurStudyIndex++;
        if (mCurStudyIndex < mTotalStudyCount)
            startStudy(mStudyData.mSentenceQuestion.get(mCurStudyIndex).mSoundFile);
        else
            startCheckResultAddComplete();
    }

    /**
     * 학습 가이드를 표시함
     *
     * @param tip 학습 가이드 내용
     */
    private void showToolTip(String tip) {
        if (!mStudyData.mIsStudyGuide)
            return;

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);

        int x = 0, y = 0;
        LinearLayout tbpl = ((LinearLayout) findViewById(R.id.sentence_study_three_btn_parent_layout));
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));

        tipText.setText(tip);
        mParentLayout.addView(tooltip);

        int centerWidth = tbpl.getWidth() / 2;
        x = centerWidth - (mTooltipWidth / 2);

        y = mParentLayout.getHeight() - tbpl.getHeight() - (mTooltipHeight / 2);

        ViewGroup.MarginLayoutParams vm = (MarginLayoutParams) tooltip.getLayoutParams();
        vm.leftMargin = x;
        vm.topMargin = y;
    }

    /**
     * 학습 가이드를 지움
     */
    private void hideToolTip() {
        if (null != findViewById(R.id.tooltip_record_text))
            mParentLayout.removeView(findViewById(R.id.tooltip_record_text));
    }

//--- Recording && Write Animation effect ---- 

    /**
     * 녹음 진행 효과 시작 Runnable
     */
    private Runnable recordEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과 종료 Runnable
     */
    private Runnable recordEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) mRecord.getDrawable();
                aniDraw.stop();
                mRecord.setBackgroundResource(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과를 시작함
     */
    private void startIngEffect() {

        if (CommonUtil.isCenter()) // Igse
            ((LinearLayout)mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_btn_learning_record_bg_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                ((LinearLayout)mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.w_btn_learning_record_bg_n));
            else // 숲
                ((LinearLayout)mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_btn_learning_record_bg_n));
        }
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_record_volume));

        mIngEffectThread = new Thread(recordEffectStartRunnable);
        mIngEffectThread.start();
    }

    /**
     * 녹음 진행 효과를 종료함
     */
    private void stopIngEffect() {
       if (null != mIngEffectThread) {
           mIngEffectThread.interrupt();
           mIngEffectThread = null;
       }

       mIngEffectThread = new Thread(recordEffectStopRunnable);
       mIngEffectThread.start();
    }

    /**
     * 녹음 완료 효과를 1초간 진행함
     */
    private void completeEffect() {
        if (CommonUtil.isCenter()) { // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record2));
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_seq_learning_progress));
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) { // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record2));
                ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.w_seq_learning_progress));
            } else { // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record2));
                ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_seq_learning_progress));
            }
        }

        try {
            AnimationDrawable aniDraw = (AnimationDrawable)((LinearLayout) mRecord.getParent()).getBackground();
            aniDraw.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Runnable runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                try {
                    AnimationDrawable aniDraw = (AnimationDrawable)((LinearLayout) mRecord.getParent()).getBackground();
                    aniDraw.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    recordPlay();
                }
            }
        };
        ((LinearLayout) mRecord.getParent()).postDelayed(runnable, 1200);
    }

//------------------- Recording----------------------

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordNotiCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            recordNotiEnd();
            recordStart();
        }
    };

    /**
     * 녹음 시작 알림음을 재생함
     */
    private void recordNotiPlay() {
        if (null != mRecordNotiPlayer)
            return;

        mRecordNotiPlayer = MediaPlayer.create(getBaseContext(), R.raw.ding);
        if (null != mRecordNotiPlayer) {
            mRecordNotiPlayer.setOnCompletionListener(mRecordNotiCompletion);
            mRecordNotiPlayer.start();
        }
    }

    /**
     * 녹음 시작 알림음을 종료함
     */
    private void recordNotiEnd() {
        if (null == mRecordNotiPlayer)
            return;

        if (mRecordNotiPlayer.isPlaying())
            mRecordNotiPlayer.stop();
        mRecordNotiPlayer.release();
        mRecordNotiPlayer = null;
    }

    /**
     * 버튼 비활성화 및 Recorder를 설정하고 녹음 시작 알림음을 재생시켜 녹음을 준비함
     */
    private void recordReady(int maxRecTime) {
        if (mIsRecording)
            return;

        mIsRecording = true;

        mRecord.setEnabled(false);

        String recordPath = "";
        if (findViewById(R.id.sentence_study_wrong_layout).getVisibility() == View.VISIBLE)
            recordPath = mRecordFilePath;
        else
            recordPath = mStudyData.mSentenceQuestion.get(mCurStudyIndex).mRecordFile.trim();

        // AudioRecorderforWeb는 녹음파일경로에서 고정적인 .wav는 제외
        setRecorder(recordPath.replace(ServiceCommon.WAV_FILE_TAIL, ""));

        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);

        mMaxRecTime = maxRecTime;

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_READY, 0), 100);
    }

    /**
     * 녹음을 시작하고 1초 뒤 녹음 완료를 진행 할 수 있도록 녹음 버튼을 활성화 함
     */
    private void recordStart() {
        if (CommonUtil.isCenter()) // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record));
            else // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));
        }

        mWavRecorder.startRecording();

        if (0 < mMaxRecTime)
            mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), mMaxRecTime);

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect();
                showToolTip(getResources().getString(R.string.string_record_tool_tip));
                mRecord.setEnabled(true);
            }
        };
        mRecord.postDelayed(runnable, 1000);
    }

    /**
     * 녹음을 종료함
     */
    private void recordEnd() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
        if (mIsRecording) {
            mRecord.setEnabled(false);
            mWavRecorder.stopRecording();
            mIsRecording = false;
            if (findViewById(R.id.sentence_study_ing_layout).getVisibility() == View.VISIBLE) {
                requestVoiceRecognize();
            }
            stopIngEffect();
            hideToolTip();
            completeEffect();
        }
    }

    /**
     * 음성 인식 채점을 요청함
     */
    private void requestVoiceRecognize() {
        mVoiceScore = -99;
        mVoiceText = "";

        String fullRecordPath = mStudyData.mSentenceQuestion.get(mCurStudyIndex).mRecordFile.trim();

        File recordFile = new File(fullRecordPath);
        // 녹음 파일이 없는 경우
        if (!recordFile.exists()) {
            setRecordResult(mVoiceText, mVoiceScore);
            return;
        }

        // 음성 평가 전 네트워크 상태 불가능의 경우
        if (!CommonUtil.isAvailableNetwork(this, false)) {
            setRecordResult(mVoiceText, mVoiceScore);
            return;
        }

        mIsRecoginition = true;
        String[] recordPathArray = fullRecordPath.split("/");
        String recordFileName = recordPathArray[recordPathArray.length - 1];
        final String recordPath = fullRecordPath.replace(recordFileName, "").trim();
        if (!recordFileName.contains(ServiceCommon.WAV_FILE_TAIL))
            recordFileName += ServiceCommon.WAV_FILE_TAIL;

        try {
            VRUTask = new VoiceRecognizerWeb(getApplicationContext());

            VRUTask.mstt = new OnMSTT() {

                @Override
                public void getSentenceObj(SentenceObj senObj) {
                    if (senObj != null) {
                        setRecordResult(senObj.getRec_sentence(), NumberUtil.Half6(senObj.getTotal_score()));
                    }
                }

            };

            VRUTask.execute(recordFileName, recordPath, "16000",
                    mStudyData.mSentenceQuestion.get(mCurStudyIndex).mTextSentence.trim(), String.valueOf(mStudyData.mCustomerNo),
                    playDuration, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 음성 인식 결과를 설정함
     *
     * @param text  음성 인식된 텍스트
     * @param score 음성 인식 하여 채점된 점수 or 오류 코드
     */
    private void setRecordResult(String text, int score) {
        StudyDataUtil.addSentenceResult(text == null ? "" : text, score);
    }

    /**
     * 에코를 재생함
     */
    private void recordPlay() {
        if (null != mRecordPlayer && mRecordPlayer.isPlaying())
            return;

        String dataSource = findViewById(R.id.sentence_study_wrong_layout).getVisibility() == View.VISIBLE ? mRecordFilePath : mStudyData.mSentenceQuestion.get(mCurStudyIndex).mRecordFile;

        ((LinearLayout) mRecord.getParent()).setBackground(null);
        if (CommonUtil.isCenter()) { // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat2));
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) { // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record));
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat2));
            } else { // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat2));
            }
        }

        try {
            if (dataSource != null) {
                File f = new File(dataSource);
                if (f.exists()) {

                    mRecordPlayer = new MediaPlayer();
                    if (mRecordPlayer != null) {

                        int duration = 1200;

                        if (f.length() > 300) {
                            mRecordPlayer.setDataSource(this, Uri.fromFile(f));
                            mRecordPlayer.setOnCompletionListener(this);
                            mRecordPlayer.setOnPreparedListener(this);
                            mRecordPlayer.prepare();
                            duration = mRecordPlayer.getDuration();
                        }

                        final LinearLayout parent = ((LinearLayout) mRecordPlay.getParent());
                        AnimationDrawable ad = null;
                        if (CommonUtil.isCenter()) { // igse
                            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
                        } else {
                            if ("1".equals(Preferences.getLmsStatus(this))) { // 우영
                                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.w_seq_learning_progress);
                            } else { // 숲
                                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
                            }
                        }
                        final AnimationDrawable ing = new AnimationDrawable();
                        final int frameTime = duration / ad.getNumberOfFrames();
                        Log.i("",  "duration => " + duration + " frame time => " + frameTime);
                        ing.setOneShot(true);
                        for (int i = 0; i < ad.getNumberOfFrames(); i++){
                            Drawable draw =  ad.getFrame(i);
                            ing.addFrame(draw, frameTime);
                        }
                        parent.setBackground(ing);
                        ((AnimationDrawable)parent.getBackground()).start();

                        if (f.length() <= 300) {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    recordPlayEnd();
                                }
                            }, duration);
                        }

                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 에코 재생을 종료하고 에코 볼륨을 원복함
     */
    private void recordPlayEnd() {
        // 녹음 재생 후 종료
        if (null != mRecordPlayer) {
            mRecordPlayer.stop();
            mRecordPlayer.release();
            mRecordPlayer = null;

            if (CommonUtil.isCenter()) // igse
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat));
            else {
                if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                    mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat));
                else // 숲
                    mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat));
            }

            releaseEchoVolume();
        }

        final LinearLayout parent = (LinearLayout) mRecordPlay.getParent();

        try {
            AnimationDrawable aniDraw = (AnimationDrawable)((LinearLayout)parent).getBackground();
            aniDraw.stop();
            parent.setBackground(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        File fileRecord = new File(mRecordFilePath);
        if (fileRecord.exists())
            fileRecord.delete();

        if (findViewById(R.id.sentence_study_ing_layout).getVisibility() == View.VISIBLE)
            nextStudy();
        else
            nextWrongStudy();
    }

    /**
     * 에코 플레이어의 준비 완료시 에코 볼륨 설정 및 재생을 시작함
     */
    public void onPrepared(MediaPlayer mp) {
        if (null != mRecordPlayer) {
            setEchoVolume();
            mRecordPlayer.start();
        }
    }

    /**
     * 에코 재생 완료시 에코 재생을 종료함
     */
    public void onCompletion(MediaPlayer mp) {
        recordPlayEnd();
    }

// --- 문장 시험 결과 --

    /**
     * 음성 인식 채점 결과가 모두 완료 될때까지 1초 간격으로 체크함 완료 된 경우 문장 학습 결과 화면 표시 메세지를 전달함
     */
    private void startCheckResultAddComplete() {
        mLoadingDialog = LoadingDialog.show(this, R.string.string_common_loading, R.string.string_sentence_exam_outcomes_preparing);

        mResultAddCompleteCheckThread = new Thread("resultAddCompleteCheckThread") {
            public void run() {
                while (null != mResultAddCompleteCheckThread && !mResultAddCompleteCheckThread.isInterrupted()) {
                    // 최소 1sec 로딩 표시..
                    SystemClock.sleep(1000);
                    if (mStudyData.mSentenceQuestion.size() == mStudyData.mSentenceResult.size()) {
                        stopCheckResultAddComplete();
                        mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
                        mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, 0));
                    }
                }
            }
        };

        mResultAddCompleteCheckThread.start();
    }

    /**
     * mResultAddCompleteCheckThread를 종료함
     */
    private void stopCheckResultAddComplete() {
        if (null != mResultAddCompleteCheckThread) {
            mResultAddCompleteCheckThread.interrupt();
            mResultAddCompleteCheckThread = null;
        }
    }

    /**
     * 문장 학습 결과 화면을 표시함
     */
    private void setSentenceResult() {

        findViewById(R.id.sentence_study_ing_layout).setVisibility(View.GONE);
        findViewById(R.id.title_view).setVisibility(View.GONE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);
        findViewById(R.id.sentence_study_result_layout).setVisibility(View.VISIBLE);

        if (null != mLoadingDialog) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }

        mTotalWrongStudyCount = mStudyData.mWrongSentenceQuestion.size();

        // chImage = (ImageView) findViewById(R.id.sentence_study_result_ch_img);

        timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
        updatedTime = timeInMilliseconds;
        int secs = (int) (updatedTime / 1000);
        int mins = secs / 60;
        secs = secs % 60;

        if (0 < mTotalWrongStudyCount) {
            if (1 == mTotalWrongStudyCount) {
                //guidePlay(R.raw.b_16);
                guidePlay(R.raw.b_19);
                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_02));
            } else if (2 == mTotalWrongStudyCount) {
                //guidePlay(R.raw.b_17);
                guidePlay(R.raw.b_19);
                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_03));
            } else {
                //guidePlay(R.raw.b_18);
                guidePlay(R.raw.b_19);
                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_04));
            }
            mResultView.setText((new StringBuilder().append(mTotalStudyCount).toString()));
            mResultView2.setText((new StringBuilder().append(mTotalStudyCount - mTotalWrongStudyCount).append(getString(R.string.string_common_question))).toString());

        } else {
            guidePlay(R.raw.b_15);
            // chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_01));
            findViewById(R.id.sentence_study_result_info_text).setVisibility(View.GONE);
            findViewById(R.id.sentence_study_result_info_text2).setVisibility(View.GONE);
            mResultView.setText(getString(R.string.string_study_result_all_correct));
        }

        mResultTime.setText(" (소요 시간 : " + mins + "분  " + secs + "초)");
    }

// --- 틀린 문장 학습 --

    /**
     * 틀린 문장 학습 가이드 음성 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mWrongStudyGuideCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            startWrongStudy(mStudyData.mWrongSentenceQuestion.get(0).mSoundFile);
        }
    };

    /**
     * 틀린 문장 학습 Flip Animation Listener
     */
    private AnimationListener wrongStudyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            mCurPlayCount = 1;
            setWrongStudy();
            playerResume();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 틀린 문장 학습을 준비함
     */
    private void readyWrongStudy() {
        guideStop();
        findViewById(R.id.sentence_study_result_layout).setVisibility(View.GONE);
        findViewById(R.id.sentence_study_wrong_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.title_view).setVisibility(View.VISIBLE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.VISIBLE);

        tCategory.setText(R.string.string_common_sentence_exam);
        tCategory2.setText(R.string.string_study_wrong_sentence_title);

        // Sub Title 설정 (2차 문장 연습)
        //((TextView) findViewById(R.id.sentence_study_wrong_title_text)).setText(getString(R.string.string_study_wrong_sentence_title));

        mListen = (ImageView) findViewById(R.id.sentence_study_wrong_listen_btn);
        mRecord = (ImageView) findViewById(R.id.sentence_study_wrong_record_btn);
        mRecordPlay = (ImageView) findViewById(R.id.sentence_study_wrong_recordplay_btn);
        //mStudyCountView = (TextView) findViewById(R.id.sentence_study_wrong_question_count_text);

        mRecord.setOnClickListener(this);

        mListen.setEnabled(false);
        mRecord.setEnabled(false);
        mRecordPlay.setEnabled(false);

        guidePlay(R.raw.b_30, mWrongStudyGuideCompletion);
    }

    /**
     * 틀린 문장 문제를 표시함
     */
    private void setWrongStudy() {
        String wrongSentenceQuestion = mStudyData.mWrongSentenceQuestion.get(mCurWrongStudyIndex).mSentence.trim();

        Log.i("", "setWrongStudy wrongSentenceQuestion length => " + wrongSentenceQuestion.length());

        mWrongStudyView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ServiceCommon.MAX_SENTENCE_LENGTH < wrongSentenceQuestion.length() ? getResources().getDimensionPixelSize(R.dimen.dimen_22) : getResources().getDimensionPixelSize(R.dimen.dimen_34));

        mWrongStudyView.setText(wrongSentenceQuestion);

        mWrongStudyCountView.setText(mCurWrongStudyIndex + 1 + "/" + mTotalWrongStudyCount);
    }

    /**
     * 틀린 문장 문제 전환용 애니메이션을 시작함
     */
    private void startWrongStudyAni() {
        /*Animation ani = new FlipAnimation(180f, 0f, mWrongStudyView.getWidth() / 2, mWrongStudyView.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(wrongStudyAniListener);
        mWrongStudyView.startAnimation(ani);*/

        Animation ani = new FlipAnimation(180f, 0f, mWrong.getWidth() / 2, mWrong.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(wrongStudyAniListener);
        mWrong.startAnimation(ani);
    }

    /**
     * 틀린 문장 학습을 시작함
     *
     * @param filePath 틀린 문장 파일 경로
     */
    private void startWrongStudy(String filePath) {
        mWrongStudyView.setText("");
        setPlayer(filePath);

        if (0 < mCurWrongStudyIndex) {
            startWrongStudyAni();
        } else {
            mCurPlayCount = 1;
            setWrongStudy();
            playerResume();
        }
    }

    /**
     * 다음 틀린 문제를 설정함 마지막 문제 이후엔 다음 단계 이동 버튼을 표시함
     */
    private void nextWrongStudy() {
        mCurWrongStudyIndex++;
        if (mCurWrongStudyIndex < mTotalWrongStudyCount) {
            startWrongStudy(mStudyData.mWrongSentenceQuestion.get(mCurWrongStudyIndex).mSoundFile);
        } else {
            // 다음 단계 이동 버튼 표시
            findViewById(R.id.sentence_study_wrong_player_control_layout).setVisibility(View.GONE);
            findViewById(R.id.sentence_study_wrong_next_status_layout).setVisibility(View.VISIBLE);
        }
    }

    /**
     * 최대 녹음 시간(녹음 자동 종료 시간)을 계산하여 반환함
     *
     * @param baseMaxRecordTime 20초
     * @param playDuration      문장 재생 시간
     * @return 최대 녹음 시간
     */
    private int getMaxRecordTime(int baseMaxRecordTime, int playDuration) {
        Log.i("", "getMaxRecordTime playDuration => " + playDuration);

        int maxRecordTime = baseMaxRecordTime;
        int customMaxRecordTime = playDuration + (playDuration * 2);

        if (baseMaxRecordTime < customMaxRecordTime)
            maxRecordTime = playDuration * 2;
        else
            maxRecordTime = baseMaxRecordTime - playDuration;

        Log.i("", "getMaxRecordTime customMaxRecordTime => " + customMaxRecordTime + " maxRecordTime => " + maxRecordTime);

        return maxRecordTime;
    }

    /**
     * 다음 단계로 이동 처리함
     */
    private void goNextStatus() {
        if (findViewById(R.id.sentence_study_result_layout).getVisibility() == View.VISIBLE) {
            if (0 < mTotalWrongStudyCount) {
                readyWrongStudy();
                return;
            }
        }
        if (isNext) {
            isNext = false;
            mStudyData.mIsVoiceRecoginition = mIsRecoginition;
            StudyDataUtil.setCurrentStudyStatus(this, "S42");
            if (mStudyData.mIsParagraph && 0 < mStudyData.mVanishingQuestion.size()) {
                if (!mStudyData.mIsCaption) {
                    int percent = mTotalWrongStudyCount * 100 / mTotalStudyCount;
                    Log.k("wusi12", "Sentence wrong persent : " + percent);
                    if (percent > 50) {
                        startActivity(new Intent(this, VanishingTitlePaperActivity.class));
                    } else {
                        //vip 완료 한것으로 침
                        StudyDataUtil.setCurrentStudyStatus(this, "S71");
                        StudyDataUtil.setCurrentStudyStatus(this, "S72");
                        if (mStudyData.mIsDictation && 0 < mStudyData.mDictation.size())
                            startActivity(new Intent(this, DictationTitlePaperActivity.class));
                        else if (mStudyData.isNewDication && mStudyData.mIsDicPirvate)
                            startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                        else if (mStudyData.mIsMovieExist)
                            startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                        else if (mStudyData.mOneWeekData.size() > 0)
                            startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
                        else
                            startActivity(new Intent(this, StudyOutcomeActivity.class));
                    }
                } else {
                    startActivity(new Intent(this, VanishingTitlePaperActivity.class));
                }
            } else if (mStudyData.mIsDictation && 0 < mStudyData.mDictation.size())
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            else if (mStudyData.isNewDication && mStudyData.mIsDicPirvate)
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            else if (mStudyData.mIsMovieExist)
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            else if (mStudyData.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else
                startActivity(new Intent(this, StudyOutcomeActivity.class));

            finish();
        }
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START)
                        startStudy(mStudyData.mSentenceQuestion.get(0).mSoundFile);
                    else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION)
                        setSentenceResult();
                    break;

                case ServiceCommon.MSG_WHAT_PLAYER:
                    if (null == mVorbisPlayer)
                        return;

                    if (msg.arg1 == ServiceCommon.MSG_PROGRESS_COMPLETION)
                        playerComplete();
                    break;

                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (null == mWavRecorder)
                        return;

                    if (msg.arg1 == ServiceCommon.MSG_REC_READY) {
                        recordNotiPlay();
                    } else if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END) {
                        recordEnd();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void requestServerTimeSync(int type) {
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestServerTimeSync(mNetworkHandler, type);
    }

    private void readyStudy() {
        StudyDataUtil.setCurrentStudyStatus(this, "S41");
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "--- S41 -----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SentenceExamActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            readyStudy();
                            Log.k("wusi12", "--------------------");
                        }
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "-------S42-----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SentenceExamActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            goNextStatus();
                            Log.k("wusi12", "--------------------");
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        readyStudy();
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        goNextStatus();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * Handler
     */
    private Handler mUIHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    String result = String.format(getString(R.string.string_study_sentence_voicerecognize_result), mVoiceScore);
                    mVoiceResult.setVisibility(View.VISIBLE);
                    mVoiceResult.setText(result);
                    break;
            }
        }
    };
}
