package com.yoons.fsb.student.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.SensorEventListener;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomVideoPlayer;
import com.yoons.fsb.student.custom.CustomVorbisPlayer;
import com.yoons.fsb.student.custom.CustomVorbisPlayer.OnVorbisSeekCompleteListener;
import com.yoons.fsb.student.data.RecData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.ExamResult;
import com.yoons.fsb.student.data.TagEventData;
import com.yoons.fsb.student.data.TagEventData.TAG_EVENT_TYPE_ENUM;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.HttpMultiPart;
import com.yoons.fsb.student.service.SlogService;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.base.BaseStudyActivity;
import com.yoons.fsb.student.ui.control.BothSideRoundProgressBar;
import com.yoons.fsb.student.ui.control.StepStatusBar;
import com.yoons.fsb.student.ui.data.PlayListRecyclerItem;
import com.yoons.fsb.student.ui.popup.AnimationBox;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.popup.ScrollBox;
import com.yoons.fsb.student.ui.popup.ShowPopup;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ContentsUtil;
import com.yoons.fsb.student.util.IOUtils;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.util.TagConverter;
import com.yoons.hssb.student.custom.CustomMp3Recorder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 스마트 학습 화면
 *
 * @author jaek
 */
public class SmartStudyActivity extends BaseStudyActivity implements OnClickListener, OnCompletionListener, OnPreparedListener, OnTouchListener, OnDialogDismissListener, OnVorbisSeekCompleteListener {

    private TextView tBookName = null, tCategory = null, tCategory2 = null;
    private final String TAG = "SmartStudyActivity";
    private static final int REQUEST_ID_TIME_SYNC_START = 1001;
    private static final int REQUEST_ID_TIME_SYNC_END = 1002;

    private RelativeLayout mParentLayout = null;

    //private StepStatusBar mStepStatusBar = null;

    private ImageView mPlay = null, mSeekBack = null, mSeekFwd = null, mPagePlay = null, mPageSeekBack = null, mPageSeekFwd = null;
    private ImageView mRecord = null, mWrite = null;
    private TextView mNextStatus = null;
    private TextView mEndTimeView = null;
    private TextView mPlayTimeView = null, mPlayEndTimeView = null, mPagePlayTimeView = null, mPagePlayEndTimeView = null;
    private SeekBar mProgress = null, mPageProgress = null;
    private Thread mPlayTimeUpdateThread = null;
    private int mBookWarningCheckTime = 0, mBookCheckTime = 0, mPlayTimeUpdateTime = 0;

    // Tag 사용 여부 (StudyData.mIsAutoArttr값과 같으나 사용자에 의한 일시정지,쓰기시작,녹음시작을 한 경우
    // resume 전까지 False)
    private boolean mIsUseTag = false;

    // Tag 속성에 따른 RecordPlay(Echo) 생략 여부 (LG1,LG2 / R1,R2)
    private boolean mIsStagEchoPassByRtagArg = false, mIsGtagEchoPassByRtagArg = false, mIsLGtagEchoPassByLGtagArg = false;

    // G1,LG1 tag 수행 여부
    private boolean mIsGtagActive = false;

    private Dialog mShowPopup = null;

    // 신규문항
    // PQ, PQ1 tag 수행 여부
    private boolean mIsPQtagActive = false;
    // PT, PT1 tag 수행 여부
    private boolean mIsPTtagActive = false;
    // PR, PR1 tag 수행 여부
    private boolean mIsPRtagActive = false;

    // G2,LG2 tag에서 녹음 종료하고 에코하기 여부 (G1,LG1 수행 후 G2,LG2 진입 전 녹음 종료 누른 경우 True)
    private boolean mIsG2tagRecordEnd = false;

    // 구간반복 정보가 표시되고 있는지 여부
    private boolean mIsShowRepeat = false;

    // R1,R2 tag 반복
    private int mCurRepeatIndex = 0, mCurRepeatStartTime = 0, mCurRepeatCount = 1, mTotalRepeatCount = 1;
    //R1,R2 상황여부
    private boolean mIsRtagActive = false;
    //R1,R2시간
    private int rStartTime = 0;
    private int rEndTime = 0;
    private String rTAagArg = "";
    // 현재,다음 tag 인덱스
    private int mCurTagEventIndex = 0, mNextTagEventIndex = 0;

    // 현재 tag 타입
    private String mCurTagEventType = "";

    // 현재 V tag 인데스
    private int mCurVideoIndex = 0;
    // 현재 V1 tag 인데스
    private int mCurV1TagIndex = 0;

    // 신규문항
    // 현재 PQ, PT, PR tag 인데스
    private int mCurQuizIndex = 0;
    private int mPopupStudyOrder = 0;
    private boolean mResultComplete = false;

    // // 현재 PQ tag 인데스
    // private int mCurQuizIndex = 0;
    //
    // // 현재 PT tag 인데스
    // private int mCurTouchPlayIndex = 0;
    //
    // // 현재 PR tag 인데스
    // private int mCurRolePlayIndex = 0;

    private String mRecordPath = null;
    private String mMergePath = null;

    private ArrayList<TagEventData> mTagEventList = null;

    private TagConverter mTagConverter = null;

    private LoadingDialog mProgressDialog = null;

    private LinearSnapHelper mSnapHelper = null;

    private LinearLayoutManager mLayoutManager = null;

    // 신규문항
    private WebView mWebView = null;
    private MediaPlayer mWebViewRecordPlayer = null;
    private FrameLayout mTargetView = null;
    private FrameLayout mContentView = null;

    private WebChromeClient.CustomViewCallback mCustomViewCallback = null;
    private View mCustomView = null;
    private ExamResult mPopupStudyResult = null;
    private String mWebViewRecordPath = null;
    private ArrayList<ExamResult> mPopupRecordResult = new ArrayList<ExamResult>(); // Popup 학습 결과
    private ArrayList<PlayListRecyclerItem> mPageItems = new ArrayList<PlayListRecyclerItem>();

    private boolean mIsPageStudy = false;

    // 배속관련
    private String mMediaPath;
    private float mSpeed = 1.0f;
    private int mBaserate = 44100;

    private MediaExtractor mExtractor = null;
    private MediaCodec mMediaCodec = null;
    private AudioTrack mAudioTrack = null;
    private int mInputBufIndex = 0;

    boolean mIsForceStop = false;
    boolean mIsPause = false;
    boolean mChangeSpd = false;
    boolean mQuiz_Check = false;

    Thread mPlayThread;

    private RecyclerView mRecyclerView = null;
    private OffsetItemDecoration mDeco = null;
    private PageListAdapter mPageListAdapter = null;

    // 현재 패이지 tag 인덱스
    private int mPageTagIndex = 0;
    private int mPageTagIndexMax = 0;

    //  배속정보
    public static class AudioPlaybackRate {
        public float mSpeed = 1.0f; // 배속 속도
        public String mFilepath = ""; // 파일 경로
    }

    public interface SmartFuncImpl {
        public void onPlayResume();

        public void onWriteEnd();

        public void onRecordEnd();
    }

    public SmartStudyActivity.SmartFuncImpl mImpl = new SmartStudyActivity.SmartFuncImpl() {
        @Override
        public void onPlayResume() {
            playerResume();
        }

        @Override
        public void onWriteEnd() {
            writeEnd();
        }

        @Override
        public void onRecordEnd() {
            learningRecodeClick();
        }
    };

    private void learningRecodeClick() {
        if (null != mRecordPlayer && mRecordPlayer.isPlaying()) {
            recordPlayEnd();
        } else {
            if (!mIsG2tagRecordEnd)
                recordEnd();
        }
    }

    private BroadcastReceiver mNotiEventReceiver = null;
    private TimerTask timerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // Igse
            setContentView(R.layout.igse_smart_study_main);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                setContentView(R.layout.w_smart_study_main);
            else // 숲
                setContentView(R.layout.f_smart_study_main);
        }

        mStudyData = StudyData.getInstance();
        mRecordFilePath = ServiceCommon.CONTENTS_PATH + ServiceCommon.AUDIO_RECORD_FILE_NAME + ServiceCommon.MP3_FILE_TAIL;

        String testFilePath = getIntent().getStringExtra("testFilePath");
        if (null != testFilePath) {
            mStudyData.mAudioSoundFile = testFilePath + ".ogg";
            mStudyData.mAudioTagFile = testFilePath + ".tag";
            mStudyData.mProductName = "Test";
            mStudyData.mIsAutoArtt = true;
            mStudyData.mAniTalk.clear();
            mStudyData.mAniTalk.add("http://vod.yoons.gscdn.com:8080/Anitalk/001_1AB_High.mp4");
        }

        if (!checkValidStudyData())
            return;

        setWidget();

        Dialog dialog = null;

        if (mStudyData.mPageInfo != null && mStudyData.mPageInfo.size() > 0) {
            mIsPageStudy = true;
        }

        if (mIsPageStudy) {
            findViewById(R.id.page_audio_study_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
            if (Preferences.getShowLearningGuideForPageStudy(mContext) == true) {
                dialog = ShowPopup.getInstance().ShowLearningTutorialForPageStudy(this);
            }
        } else {
            findViewById(R.id.page_audio_study_layout).setVisibility(View.GONE);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.VISIBLE);
            if (Preferences.getShowLearningGuide(mContext) == true) {
                dialog = ShowPopup.getInstance().ShowLearningTutorial(this);
            }
        }

        if (dialog != null) {
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (mIsPageStudy) {
                        Preferences.setShowLearningGuideForPageStudy(mContext, false);
                    } else {
                        Preferences.setShowLearningGuide(mContext, false);
                    }
                    convertTagToData();
                }
            });
        } else {
            convertTagToData();
        }

        // TitleView
        tBookName.setText(mStudyData.mProductName);
        tCategory.setText(getString(R.string.string_common_today_study));
        tCategory2.setText(getString(R.string.string_common_smart_study));

        Crashlytics.log(getString(R.string.string_ga_SmartStudyActivity));

        // readyStudy();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.flags &= ~WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        //이어폰단자
        /* registerReceiver(mBroadcastReceiver, mIntentFilter); */

        getWindow().setAttributes(lp);
        setNotiReceiver();
        //setSlogReceiver();

//        if (Preferences.getShowLearningGuide(mContext)) {
//            Dialog dialog = ShowPopup.getInstance().ShowLearningTutorial(this);
//            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialog) {
//                    Preferences.setShowLearningGuide(mContext, false);
//                    convertTagToData();
//                    if (mVorbisPlayer != null && !mVorbisPlayer.isPlaying()) {
//                        mVorbisPlayer.play();
//                    }
//                }
//            });
//        } else {
//            convertTagToData();
//        }
    }

    private void setStudyLayout() {
        String pagePath = String.format(ContentsUtil.PAGE_LOCAL_PATH, mStudyData.mSeriesNo, mStudyData.mRealBookNo);

        // 첫번째 표지 이미지를 기본적으로 넣어줌 (DB 및 태그에는 존재하지 않음)
        StudyData.PageInfo pi = new StudyData.PageInfo();
        pi.mIsUsed = true;
        pi.mPageNo = 0;
        pi.mPageOrder = 0;
        pi.mTagTime = 0;
        pi.mType = "S";

        if (mIsPageStudy) {
            mRecyclerView = (RecyclerView) findViewById(R.id.page_list_layout);

            mLayoutManager = new LinearLayoutManager(this);

            ((LinearLayoutManager) mLayoutManager).setOrientation(LinearLayoutManager.HORIZONTAL);
            mRecyclerView.setLayoutManager(mLayoutManager);

            mPageListAdapter = new PageListAdapter(mPageItems);
            mRecyclerView.setAdapter(mPageListAdapter);

            mDeco = new OffsetItemDecoration(this);
            mRecyclerView.addItemDecoration(mDeco);

            mSnapHelper = new LinearSnapHelper();
            mSnapHelper.attachToRecyclerView(mRecyclerView);

            mRecyclerView.addOnItemTouchListener(mItemTouchListener);

            mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }
            });

            mPageSeekBack.setTag(ServiceCommon.BUTTONTAG_SEEK_BACK);
            mPageSeekFwd.setTag(ServiceCommon.BUTTONTAG_SEEK_FWD);

            mPageSeekBack.setEnabled(false);
            mPageSeekFwd.setEnabled(false);

            if (pi.mIsUsed == mStudyData.mPageInfo.get(0).mIsUsed &&
                    pi.mPageNo == mStudyData.mPageInfo.get(0).mPageNo &&
                    pi.mPageOrder == mStudyData.mPageInfo.get(0).mPageOrder &&
                    pi.mTagTime == mStudyData.mPageInfo.get(0).mTagTime &&
                    pi.mType.equalsIgnoreCase(mStudyData.mPageInfo.get(0).mType))
            {
                android.util.Log.d("","same data");
            } else {
                mStudyData.mPageInfo.add(0, pi);
            }

            for (int i = 0; i < mStudyData.mPageInfo.size(); i++) {
                String imagePath = String.format(pagePath + ContentsUtil.PAGE_POSTFIX, mStudyData.mSeriesNo, mStudyData.mRealBookNo, mStudyData.mPageInfo.get(i).mType, mStudyData.mPageInfo.get(i).mPageNo);
                mPageItems.add(new PlayListRecyclerItem(imagePath, mStudyData.mPageInfo.get(i).mPageNo));
            }

            Log.i("", "resume time => " + CommonUtil.convMsecToMinSecMsec(mStudyData.mAudioStudyTime));

            mPageListAdapter.notifyDataSetChanged();

        } else {
            // seek 동작을 위한 버튼 구별 tag
            mSeekBack.setTag(ServiceCommon.BUTTONTAG_SEEK_BACK);
            mSeekFwd.setTag(ServiceCommon.BUTTONTAG_SEEK_FWD);

            mSeekBack.setEnabled(false);
            mSeekFwd.setEnabled(false);
        }
    }

    private void requestServerTimeSync(int type) {
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestServerTimeSync(mNetworkHandler, type);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        /* registerReceiver(mBroadcastReceiver, mIntentFilter); */
        /* FlurryAgent.onStartSession(this, ServiceCommon.FLURRY_API_KEY); */
        // GA적용
        CommonUtil.conSlogService(this, true);
        CommonUtil.conNotiService(this, true);
        ///slogCheck();//slog 안될때 체크
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        callHiddenWebViewMethod("onStop");
        try {
            unregisterReceiver(mNotiEventReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        CommonUtil.conSlogService(this, false);
        CommonUtil.conNotiService(this, false);
        /* FlurryAgent.onEndSession(this); */
    }

    @Override
    protected void onPause() {
        // 학습 중 화면 잠금, 꺼짐 시 간지로 재시작
        if (mWebView != null) {
            mWebView.loadUrl("javascript:stopAudio()");
            callHiddenWebViewMethod("onPause");
            mIsForceStop = true;
            if (mPlayThread != null) {
                mPlayThread.interrupt();
            }
        }

        if (!isFinishing()) {
            if (mIsReStart) {
                startActivity(new Intent(this, SmartStudyTitlePaperActivity.class).putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
            }

            setFinish();
            if (null != mWebViewRecordPlayer) {
                if (mWebViewRecordPlayer.isPlaying())
                    mWebViewRecordPlayer.stop();
                mWebViewRecordPlayer.release();
                mWebViewRecordPlayer = null;
            }
            finish();
        }

        super.onPause();
    }

    private void playerPagePause() {
        mVorbisPlayer.pause();
    }

    private void playerSwipeResume(int pauseTime) {

        mCurPosition = pauseTime;
        mVorbisPlayer.seekTo((double) mCurPosition / 1000, true);

        //튜토리얼 때는 핸들러도 안먹게(팝업이 있으면 resume안되게)
        if (!ShowPopup.getInstance().isPopupLayer()) {
            playerResume();
        }
    }

    private synchronized void setPageIndex(int index) {
        if (index < 0) {
            index = 0;
        }

        mPageTagIndex = index;
        if (mPageTagIndexMax <= mPageTagIndex) {
            mPageTagIndexMax = mPageTagIndex;
        }

        if (mStudyData.mPageInfo.size() > 0) {
            Log.i("", " setpage => " + index);

            if (index == 0) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLayoutManager.scrollToPosition(0);
                    }
                }, 100);
            } else {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mDeco.mLeftX > 0) {
                            Log.i("", " why setpage => " + mPageTagIndex);
                            mLayoutManager.scrollToPositionWithOffset(mPageTagIndex, mDeco.mLeftX);
                        }
                    }
                }, 100);
            }

            TextView gap = new TextView(this);
            gap.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 23);

            for (int i = 0; i < mStudyData.mPageInfo.size(); i++) {
                PageListViewHolder item = (PageListViewHolder) mRecyclerView.findViewHolderForLayoutPosition(i);

                if (item != null) {
                    if (i == (mPageTagIndex)) {
                        item.mLinearLayout.setScaleX(1.0f);
                        item.mLinearLayout.setScaleY(1.0f);
                        item.mLinearLayout.setAlpha(1f);
                        ((MarginLayoutParams) item.mLinearLayout.getLayoutParams()).leftMargin = 0;
                        ((MarginLayoutParams) item.mLinearLayout.getLayoutParams()).rightMargin = (int) gap.getTextSize();
//                        if (CommonUtil.isCenter()) // Igse
//                            item.mLinearLayout.setBackground(getResources().getDrawable(R.drawable.igse_page_item_bg));
//                        else {
//                            if ("1".equals(Preferences.getLmsStatus(this))) //우영
//                                item.mLinearLayout.setBackground(getResources().getDrawable(R.drawable.w_page_item_bg));
//                            else // 숲
//                                item.mLinearLayout.setBackground(getResources().getDrawable(R.drawable.f_page_item_bg));
//                        }
                        if (CommonUtil.isCenter()) // Igse
                            ((CardView) item.mLinearLayout.findViewById(R.id.page_item)).setCardBackgroundColor(Color.parseColor("#9858cf"));
                        else {
                            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                                ((CardView) item.mLinearLayout.findViewById(R.id.page_item)).setCardBackgroundColor(Color.parseColor("#f79432"));
                            else // 숲
                                ((CardView) item.mLinearLayout.findViewById(R.id.page_item)).setCardBackgroundColor(Color.parseColor("#ac6543"));
                        }
                        item.mInfo.setVisibility(View.VISIBLE);
                    } else {
                        item.mLinearLayout.setScaleX(0.9f);
                        item.mLinearLayout.setScaleY(0.9f);
                        item.mLinearLayout.setAlpha(0.66f);
                        if (i == mPageTagIndex - 1) {
                            ((MarginLayoutParams) item.mLinearLayout.getLayoutParams()).rightMargin = (int) gap.getTextSize();
                        } else {
                            ((MarginLayoutParams) item.mLinearLayout.getLayoutParams()).rightMargin = (int) (gap.getTextSize() * 0.6f);
                        }
                        ((MarginLayoutParams) item.mLinearLayout.getLayoutParams()).leftMargin = 0;
                        ((CardView) item.mLinearLayout.findViewById(R.id.page_item)).setCardBackgroundColor(Color.parseColor("#ffffff"));
//                        if (CommonUtil.isCenter()) // Igse
//                            item.mLinearLayout.setBackground(getResources().getDrawable(R.drawable.igse_page_item_white_bg));
//                        else {
//                            if ("1".equals(Preferences.getLmsStatus(this))) //우영
//                                item.mLinearLayout.setBackground(getResources().getDrawable(R.drawable.w_page_item_white_bg));
//                            else // 숲
//                                item.mLinearLayout.setBackground(getResources().getDrawable(R.drawable.f_page_item_white_bg));
//                        }
                        item.mInfo.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }
    }

    private RecyclerView.OnItemTouchListener mItemTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            if (!StudyData.getInstance().mIsFastRewind) {
                return true;
            }

            if (singleProcessChecker()) {
                return true;
            }
            View childView = rv.findChildViewUnder(e.getX(), e.getY());
            int pos = rv.getChildAdapterPosition(childView);
            if (pos != RecyclerView.NO_POSITION) {
                playerPagePause();
                Log.i("", "setpage pos => " + pos + " time => " + CommonUtil.convMsecToMinSecMsec(mStudyData.mPageInfo.get(pos).mTagTime));
                playerSwipeResume(mStudyData.mPageInfo.get(pos).mTagTime);
                playerProgressUpdate(mStudyData.mPageInfo.get(pos).mTagTime);
                setPageIndex(pos);
            }
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

		/*if (null != mBookCheckThread) {
            mBookCheckThread.interrupt();
			mBookCheckThread = null;
		}

		if (null != mBookWarningCheckThread) {
			mBookWarningCheckThread.interrupt();
			mBookWarningCheckThread = null;
		}
		*/

        if (mRecyclerView != null) {
            if (mDeco != null) {
                mRecyclerView.removeItemDecoration(mDeco);
            }

            if (mItemTouchListener != null) {
                mRecyclerView.removeOnItemTouchListener(mItemTouchListener);
            }
        }

        if (timerTask != null) {
            timerTask.cancel();
        }
        if (null != mPlayTimeUpdateThread) {
            mPlayTimeUpdateThread.interrupt();
            mPlayTimeUpdateThread = null;
        }
    }

    /**
     * 화면 구성 완료 후 호출됨 학습 가이드의 Width, Height 값을 저장함
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            /*View tooltip = findViewById(R.id.tooltip_text);
            if (null != tooltip) {
                mTooltipHeight = tooltip.getHeight();
                mTooltipWidth = tooltip.getWidth();
                mParentLayout.removeView(tooltip);
            }*/
        }
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        if ((mIsPQtagActive || mIsPTtagActive || mIsPRtagActive)) {
            return false;
        }

        try {
            keyCode = getCorrectKeyCode(keyCode);

            switch (keyCode) {
                case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:

                    if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()
                            || View.VISIBLE == findViewById(R.id.page_audio_study_layout).getVisibility()) {
                        if (mVorbisPlayer.singleProcessChecker() || mVorbisPlayer.isSeeking())
                            return true;

                        boolean isExcute = false;
                        int btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;

//                        if (mSeekBack.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
//                            mSeekBack.setPressed(true);
//                            mSeekBack.playSoundEffect(SoundEffectConstants.CLICK);
//                            btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
//                            isExcute = true;
//                        } else if (mSeekFwd.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
//                            mSeekFwd.setPressed(true);
//                            mSeekFwd.playSoundEffect(SoundEffectConstants.CLICK);
//                            btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
//                            isExcute = true;
//                        }
                        if (mIsPageStudy) {
                            if (mPageSeekBack.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                                mPageSeekBack.setPressed(true);
                                mPageSeekBack.playSoundEffect(SoundEffectConstants.CLICK);
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
                                isExcute = true;
                            } else if (mPageSeekFwd.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                                mPageSeekFwd.setPressed(true);
                                mPageSeekFwd.playSoundEffect(SoundEffectConstants.CLICK);
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
                                isExcute = true;
                            }
                        } else {
                            if (mSeekBack.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                                mSeekBack.setPressed(true);
                                mSeekBack.playSoundEffect(SoundEffectConstants.CLICK);
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
                                isExcute = true;
                            } else if (mSeekFwd.isEnabled() && keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                                mSeekFwd.setPressed(true);
                                mSeekFwd.playSoundEffect(SoundEffectConstants.CLICK);
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
                                isExcute = true;
                            }
                        }

                        if (isExcute)
                            mVorbisPlayer.playerSeekPrecede(btnTag);
                    } else if (View.VISIBLE == findViewById(R.id.animation_study_layout).getVisibility()) {
                        int a = 0;
                        if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                            // 앞으로 감기
                            a = mVideoPlayer.mPlayer.getCurrentPosition() + 5000; // 현재
                            // 재생되는
                            // 시간
                            // +
                            // 500
                            mVideoPlayer.mPlayer.seekTo(a);
                        } else if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                            // 뒤로 감기
                            a = mVideoPlayer.mPlayer.getCurrentPosition() - 5000; // 현재
                            // 재생되는
                            // 시간
                            // -
                            // 500
                            mVideoPlayer.mPlayer.seekTo(a);
                        }

                    }

                    break;

                case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
                case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                    if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()
                            || View.VISIBLE == findViewById(R.id.page_audio_study_layout).getVisibility()) {
                        if (mPlay.isEnabled() || mPagePlay.isEnabled()) {
                            mPlay.setPressed(true);
                            mPagePlay.setPressed(true);
                        } else if (mWrite.isEnabled() && mIsWriting)
                            mWrite.setPressed(true);
                        else if (mRecord.isEnabled() && mMp3Recorder.isRecording())
                            mRecord.setPressed(true);
                    } else if (View.VISIBLE == findViewById(R.id.animation_study_layout).getVisibility()) {
                        if (null != mVideoPlayer && mVideoPlayer.isPrepared())
                            mVideoPlayer.setPlayPressed(true);
                    } else {
                        mNextStatus.setPressed(true);
                    }
                    break;

                case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_STOP:
                    if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                        if (mRecord.isEnabled() && !mMp3Recorder.isRecording())
                            mRecord.setPressed(true);
                    }
                    break;

                default:
                    return super.onKeyDown(keyCode, event);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        if ((mIsPQtagActive || mIsPTtagActive || mIsPRtagActive)) {
            return false;
        }

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()
                        || View.VISIBLE == findViewById(R.id.page_audio_study_layout).getVisibility()) {
                    boolean isExcute = false;
                    int btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;

//                    if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
//                        mSeekBack.setPressed(false);
//                        if (mSeekBack.isEnabled()) {
//                            btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
//                            isExcute = true;
//                        }
//                    } else if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
//                        mSeekFwd.setPressed(false);
//                        if (mSeekFwd.isEnabled()) {
//                            btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
//                            isExcute = true;
//                        }
//                    }

                    if (mIsPageStudy) {
                        if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                            mPageSeekBack.setPressed(false);
                            if (mPageSeekBack.isEnabled()) {
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
                                isExcute = true;
                            }
                        } else if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                            mPageSeekFwd.setPressed(false);
                            if (mPageSeekFwd.isEnabled()) {
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
                                isExcute = true;
                            }
                        }
                    } else {
                        if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND) {
                            mSeekBack.setPressed(false);
                            if (mSeekBack.isEnabled()) {
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_BACK;
                                isExcute = true;
                            }
                        } else if (keyCode == ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD) {
                            mSeekFwd.setPressed(false);
                            if (mSeekFwd.isEnabled()) {
                                btnTag = ServiceCommon.BUTTONTAG_SEEK_FWD;
                                isExcute = true;
                            }
                        }
                    }

                    if (isExcute)
                        mVorbisPlayer.playerSeekExcute(btnTag);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()
                        || View.VISIBLE == findViewById(R.id.page_audio_study_layout).getVisibility()) {
                    mPlay.setPressed(false);
                    mPagePlay.setPressed(false);
                    mWrite.setPressed(false);
                    mRecord.setPressed(false);
                    if (mPlay.isEnabled() || mPagePlay.isEnabled()) {
                        if (mVorbisPlayer != null)
                            if (mVorbisPlayer.isPlaying()) {
                                mIsUseTag = false;
                                playerPause();
                            } else {
                                playerResume();
                            }
                    } else if (mWrite.isEnabled() && mIsWriting) {
                        writeEnd();
                    } else if (mRecord.isEnabled() && mMp3Recorder.isRecording()) {
                        if (!mIsG2tagRecordEnd)
                            recordEnd();
                    }
                } else if (View.VISIBLE == findViewById(R.id.animation_study_layout).getVisibility()) {
                    if (null != mVideoPlayer && mVideoPlayer.isPrepared()) {
                        mVideoPlayer.setPlayPressed(false);
                        if (mVideoPlayer.isPlaying())
                            mVideoPlayer.pauseBtn();
                        else
                            mVideoPlayer.startBtn();
                    }
                } else {
                    mNextStatus.setPressed(false);
                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        goNextStatus();
                    } else {
                        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_STOP:
                if (View.VISIBLE == findViewById(R.id.audio_study_ing_layout).getVisibility()) {
                    mRecord.setPressed(false);
                    if (mRecord.isEnabled()) {
                        if (null != mRecordPlayer && mRecordPlayer.isPlaying()) {
                            recordPlayEnd();
                        } else {
                            if (!mMp3Recorder.isRecording()) {
                                mIsUseTag = false;
                                recordReady(true, ServiceCommon.MAX_REC_TIME_3MIN);
                            }
                        }
                    }
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (null != mVorbisPlayer && mVorbisPlayer.isSeeking()) {
            v.setPressed(false);
            return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (!mIsStudyStart)
            return;
        try {
            int id = v.getId();

            if (singleProcessChecker())
                return;

            switch (id) {
                case R.id.audio_study_play_btn:
                case R.id.page_list_play_btn:
                    if (mVorbisPlayer.isPlaying()) {
                        mIsUseTag = false;
                        playerPause();
                    } else {
                        playerResume();
                    }
                    break;

                case R.id.audio_study_write_btn:
                    if (!mIsWriting) {
                        mIsUseTag = false;
                        writeStart();
                    } else {
                        writeEnd();
                    }
                    break;

                case R.id.audio_study_record_btn:
                    if (null != mRecordPlayer && mRecordPlayer.isPlaying()) {
                        recordPlayEnd();
                    } else {
                        if (!mMp3Recorder.isRecording()) {
                            mIsUseTag = false;
                            recordReady(true, ServiceCommon.MAX_REC_TIME_3MIN);
                            mStudyData.mRecMCnt++;
                            mStudyData.mPassRcheck = true;
                        } else {
                            if (!mIsG2tagRecordEnd)
                                recordEnd();
                        }
                    }
                    break;

                case R.id.smart_study_completion_next_btn:
                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        goNextStatus();
                    } else {
                        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * StudyData의 유효성을 검사함
     */
    private boolean checkValidStudyData() {
        boolean isValid = true;

        if (null == mStudyData) {
            isValid = false;
            Log.e("", "checkValidStudyData studyData is null !!");
        } else {
            File fileYda = new File(mStudyData.mAudioSoundFile);
            if (!fileYda.exists())
                isValid = false;
            Log.e("", "checkValidStudyData mAudioSoundFile => " + mStudyData.mAudioSoundFile);
        }

        if (!isValid)
            Toast.makeText(this, getString(R.string.string_common_study_file_error), Toast.LENGTH_SHORT).show();

        return isValid;
    }

    public static class PageListViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mLinearLayout;
        public ImageView mImage;
        public TextView mInfo;

        public PageListViewHolder(LinearLayout v) {
            super(v);
            mLinearLayout = v;
            mImage = (ImageView) v.findViewById(R.id.page_image);
            mInfo = (TextView) v.findViewById(R.id.page_list_info);
        }
    }

    public class OffsetItemDecoration extends RecyclerView.ItemDecoration {

        private Context ctx;
        private int mLeftX = 0;

        public OffsetItemDecoration(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            //Log.i("", "width => " + ((LinearLayout.LayoutParams) parent.findViewById(R.id.page_item).getLayoutParams()).width);
            int itemWidth = ((LinearLayout.LayoutParams) parent.findViewById(R.id.page_item).getLayoutParams()).width;
            int offset = (int) (getScreenWidth() / (float) (2)) - itemWidth / 2;

            mLeftX = offset;

            int pos = parent.getChildLayoutPosition(view);
            Log.i("", "offset pos => " + pos);

            if (pos == 0) {
                ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = 0;
                setupOutRect(outRect, offset, true);
            } else if (pos == state.getItemCount() - 1) {
                ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin = 0;
                setupOutRect(outRect, offset, false);
            }

            setPageIndex(mPageTagIndex);
        }

        private void setupOutRect(Rect rect, int offset, boolean start) {
            if (start) {
                rect.left = offset;
            } else {
                rect.right = offset;
            }
        }

        private int getScreenWidth() {
            return getResources().getDisplayMetrics().widthPixels;
        }
    }

    public class PageListAdapter extends RecyclerView.Adapter<PageListViewHolder> {
        private ArrayList<PlayListRecyclerItem> mData = null;

        // Provide a suitable constructor (depends on the kind of dataset)
        public PageListAdapter(ArrayList<PlayListRecyclerItem> data) {
            mData = data;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public PageListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            LinearLayout v = null;
            if (CommonUtil.isCenter()) {
                v = (LinearLayout)LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.igse_page_audio_study_item, parent, false);
            } else {
                if ("1".equals(Preferences.getLmsStatus(mContext))) {
                    v = (LinearLayout)LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.w_page_audio_study_item, parent, false);
                } else {
                    v = (LinearLayout)LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.f_page_audio_study_item, parent, false);
                }
            }

            if ((getResources().getConfiguration().screenLayout
                    & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                CommonUtil.setSizeLayout(getApplicationContext(), v);
            }

            PageListViewHolder vh = new PageListViewHolder(v);

            return vh;
        }

        @Override
        public void onBindViewHolder(final PageListViewHolder holder, int position) {
            String image = mData.get(position).getImage();
            Log.i("", "image => " + image);
            Bitmap b = BitmapFactory.decodeFile(image);
            holder.mImage.setImageBitmap(b);
            holder.mInfo.setText(String.valueOf(mData.get(position).getPageNo()));
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        mParentLayout = (RelativeLayout) findViewById(R.id.smart_study_parent_layout);
        mPlay = (ImageView) findViewById(R.id.audio_study_play_btn);
        mPagePlay = (ImageView) findViewById(R.id.page_list_play_btn);
        mSeekBack = (ImageView) findViewById(R.id.audio_study_seek_back_btn);
        mSeekFwd = (ImageView) findViewById(R.id.audio_study_seek_fwd_btn);
        mPageSeekBack = (ImageView) findViewById(R.id.page_audio_study_seek_back_btn);
        mPageSeekFwd = (ImageView) findViewById(R.id.page_audio_study_seek_fwd_btn);
        mRecord = (ImageView) findViewById(R.id.audio_study_record_btn);
        mWrite = (ImageView) findViewById(R.id.audio_study_write_btn);
        mNextStatus = (TextView) findViewById(R.id.smart_study_completion_next_btn);
        mPlayTimeView = (TextView) findViewById(R.id.audio_study_position_text);
        mEndTimeView = (TextView) findViewById(R.id.audio_study_position_text2);
        mProgress = (SeekBar) findViewById(R.id.audio_study_progressbar);
        mPagePlayTimeView = (TextView) findViewById(R.id.page_audio_study_position_text);
        mPagePlayEndTimeView = (TextView) findViewById(R.id.page_audio_study_position_text2);
        mPageProgress = (SeekBar) findViewById(R.id.page_audio_study_progressbar);
        //mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);
        mWebView = (WebView) findViewById(R.id.web_View);
        mContentView = (FrameLayout) findViewById(R.id.main_content);
        mTargetView = (FrameLayout) findViewById(R.id.target_view);

        mProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser) {
                if (fromUser) {
                    mCurPosition = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                playerPause();
            }

            @Override
            public void onStopTrackingTouch(final SeekBar seekBar) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mVorbisPlayer.seekTo(seekBar.getProgress() / 1000);
                        playerResume();
                    }
                });
            }
        });

        mPageProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser) {
                if (fromUser) {
                    mCurPosition = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                playerPause();
            }

            @Override
            public void onStopTrackingTouch(final SeekBar seekBar) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mVorbisPlayer.seekTo(seekBar.getProgress() / 1000);
                        playerResume();
                    }
                });
            }
        });

        mProgress.setEnabled(false);
        mPageProgress.setEnabled(false);

        mNextStatus.setOnClickListener(this);
        mPlay.setOnClickListener(this);
        mPagePlay.setOnClickListener(this);
        mWrite.setOnClickListener(this);
        mRecord.setOnClickListener(this);

        mPlay.setOnTouchListener(this);
        mPagePlay.setOnTouchListener(this);
        mWrite.setOnTouchListener(this);
        mRecord.setOnTouchListener(this);

        setTitlebarCategory(getString(R.string.string_titlebar_category_study));
        setTitlebarText(mStudyData.mProductName);
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        //mStepStatusBar.setHighlights(StepStatusBar.STATUS_SMART_STUDY);

        TextView modeInfoTextView = (TextView) findViewById(R.id.audio_study_record_mode_info);

        Log.d("autoartt2", "" + mStudyData.mIsAutoArtt);

        if (ServiceCommon.IS_CONTENTS_TEST) {
            if (mStudyData.mIsAutoArtt)
                modeInfoTextView.setText(getString(R.string.string_tag_test_mode));
            modeInfoTextView.setVisibility(View.VISIBLE);
        } else {
            modeInfoTextView.setVisibility(mStudyData.mIsAutoArtt ? View.GONE : View.VISIBLE);
        }

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        /*View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);
        tooltip.setVisibility(View.INVISIBLE);
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_text));
        tipText.setText(getResources().getString(R.string.string_record_tool_tip));
        mParentLayout.addView(tooltip);*/

    }

    private void setVtagView() {
        if (0 == mStudyData.mAniTalk.size())
            return;

        int aniIndex = 0;
        if (0 < mStudyData.mAudioStudyTime) {
            for (int i = 0; i < mTagEventList.size(); i++) {
                TagEventData te = mTagEventList.get(i);
                if (te.getEventType().equalsIgnoreCase(TagEventData.TAG_V)) {
                    if (te.getEventTime() >= mStudyData.mAudioStudyTime) {
                        aniIndex = te.getVideoIndex();
                        break;
                    }
                }
            }
        }

        setVideoPlayer(mStudyData.mAniTalk.get(aniIndex), false);
    }

    /**
     * Tag File 변환을 시작함
     */
    private void convertTagToData() {
        mTagConverter = new TagConverter(mNetworkHandler, mStudyData.mAudioTagFile);
        mTagConverter.start();
    }

    /**
     * Tag 변환 결과를 설정함
     *
     * @param tagDataList Tag 변환 결과
     */
    @SuppressWarnings("unchecked")
    private void setTagEventData(Object tagDataList) {
        if (null == tagDataList)
            return;

        if (tagDataList instanceof ArrayList<?>) {
            mTagEventList = (ArrayList<TagEventData>) tagDataList;
        }

        setVtagView();
        setPageTag();
        setStartPage();
    }

    private int getPageIndexFromAudioTime(int time) {
        int index = 0;
        for (int i = 0; i < mStudyData.mPageInfo.size(); i++) {
            if ((i > 0) && mStudyData.mPageInfo.get(i).mTagTime == 0) {
                continue;
            }
            if (time >= mStudyData.mPageInfo.get(i).mTagTime) {
                index = i;
            }
        }
        return index;
    }

    private void setStartPage() {
        mPageTagIndex = getPageIndexFromAudioTime(mStudyData.mAudioStudyTime);

        if (mPageTagIndex > 0) {
            setPageIndex(mPageTagIndex);
        }
    }

    private void setPageTag() {
        ///////////////////////////////////////
        int pageIndex = 1;
        ///////////////////////////////////////

        boolean isNUM1TagActive = false;
        for (int i = 0; i < mTagEventList.size(); i++) {
            TagEventData te = mTagEventList.get(i);
            String type = te.getEventType();
            if (type.contains(TagEventData.TAG_NUM1) && !(type.equalsIgnoreCase(TagEventData.TAG_R1) || type.equalsIgnoreCase(TagEventData.TAG_PAGE1)  || type.equalsIgnoreCase(TagEventData.TAG_LG1))) {
                isNUM1TagActive = true;
            }
            if (type.contains(TagEventData.TAG_NUM2) && !(type.equalsIgnoreCase(TagEventData.TAG_R2) || type.equalsIgnoreCase(TagEventData.TAG_PAGE2) || type.equalsIgnoreCase(TagEventData.TAG_LG2))) {
                isNUM1TagActive = false;
            }

            if (!isNUM1TagActive && (te.getEventType().equalsIgnoreCase(TagEventData.TAG_PAGE0) || te.getEventType().equalsIgnoreCase(TagEventData.TAG_PAGE1))) {
                android.util.Log.d("", "---------------mStudyData.mPageInfo.size()-" + String.valueOf(mStudyData.mPageInfo.size()));

                if (pageIndex < mStudyData.mPageInfo.size()) {
                    StudyData.PageInfo pi = mStudyData.mPageInfo.get(pageIndex);
                    pi.mTagTime = te.getEventTime();
                    Log.i("", "page tag => " + pageIndex + " time => "+ CommonUtil.convMsecToMinSecMsec(pi.mTagTime));
                    pageIndex++;
                } else {
                    Toast.makeText(this, "학습 페이지 준비에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    }

    /**
     * Player, Recorder를 설정하여 학습을 준비함
     */
    private void readyStudy() {
        setPlayerAndRecorder();

        mIsUseTag = mStudyData.mIsAutoArtt;

        mTotalRepeatCount = mStudyData.mRTagRepeatCnt; // 교사가 설정한 반복 횟수 설정

        //StudyDataUtil.requestAppTotalRepeatLog(mContext);

        mStudyData.mAudioTotalMin = mVorbisPlayer.getDuration();

        /*
         * if (0 == mStudyData.mAudioStudyTime) {
         * StudyDataUtil.setCurrentStudyStatus(this, "S01");
         * StudyDataUtil.setCurrentStudyStatus(this, "S11"); }
         */

        StudyDataUtil.setCurrentStudyStatus(this, "S01");
        StudyDataUtil.setCurrentStudyStatus(this, "S11");

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * 학습을 시작함
     */
    private void startStudy() {
        if (mVorbisPlayer.getState() == AudioTrack.STATE_UNINITIALIZED) {
            Log.e("", "startStudy Retry cuz AudioTrack Uninitialized !!");
            mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 100);
            return;
        }

        playerResume(mStudyData.mAudioStudyTime);
        if (!mIsStudyStart)
            mIsStudyStart = true;
        startPlayTimeUpdateThread();
        //startBookCheckThread();
        //	startBookWarningCheckThread();

    }

    /**
     * Player, Recorder를 설정함
     */
    private void setPlayerAndRecorder() {
        mMp3Recorder = new CustomMp3Recorder(mHandler, ServiceCommon.RECORD_SAMPLE_RATE);

        mVorbisPlayer = new CustomVorbisPlayer(mHandler, mStudyData.mAudioSoundFile, mStudyData.mIsFastRewind);
        mVorbisPlayer.setOnVorbisSeekCompleteListener(this);

        if (mIsPageStudy) {
            mPageSeekBack.setOnTouchListener(mVorbisPlayer.seekTouchListener);
            mPageSeekFwd.setOnTouchListener(mVorbisPlayer.seekTouchListener);

            mPageProgress.setProgress(0);
            mPageProgress.setMax(mVorbisPlayer.getDuration());
            mPageProgress.setEnabled(false);
        } else {
            // 실제 seek 동작 리스너
            mSeekBack.setOnTouchListener(mVorbisPlayer.seekTouchListener);
            mSeekFwd.setOnTouchListener(mVorbisPlayer.seekTouchListener);

            mProgress.setProgress(0);
            mProgress.setMax(mVorbisPlayer.getDuration());
            mProgress.setEnabled(false);
        }

        //mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(0)).append(getString(R.string.string_common_margin_slash)).append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());

        mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition))));
        mEndTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());

        mPagePlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(0))));
        mPagePlayEndTimeView.setText(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()).toString());

        mCurTagEventType = "";
    }

    /**
     * 학습 중단 당시의 재생 시간에서 10초 전으로 Player를 Resume함
     *
     * @param pauseTime 학습 중단 당시 재생 시간
     */
    private void playerResume(int pauseTime) {
        int resumeTime = pauseTime - (10 * 1000);
        if (0 > resumeTime)
            resumeTime = 0;

        mCurPosition = resumeTime;
        mVorbisPlayer.seekTo((double) mCurPosition / 1000, true);
        playerResume();
    }

    /**
     * Player를 Resume하고 버튼의 리소스 및 활성화를 변경함
     */
    private void playerResume() {
        if (mStudyData.mIsAutoArtt) {
            playerTagResume();
            mIsUseTag = true;
        }

        if (CommonUtil.isCenter()) // igse
            mPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_pause_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_pause_n));
            else // 숲
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_pause_n));
        }

        mPagePlay.setImageDrawable(getResources().getDrawable(R.drawable.c_selector_page_list_pause));

        if (!mIsGtagActive) {
            mSeekBack.setEnabled(true);
            mSeekFwd.setEnabled(mStudyData.mIsFastRewind ? true : false);
            mPageSeekBack.setEnabled(true);
            mPageSeekFwd.setEnabled(mStudyData.mIsFastRewind);
            mPlay.setEnabled(true);
            mPagePlay.setEnabled(true);
            mWrite.setEnabled(true);
            mRecord.setEnabled(true);
        }

        if (mVorbisPlayer != null)
            mVorbisPlayer.play();
    }

    /**
     * 화면상 버튼의 리소스 및 활성화를 변경함
     */
    private void playerPauseAndStop() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);

        if (CommonUtil.isCenter()) // Igse
            mPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_player_play));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_player_play));
            else // 숲
                mPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_player_play));
        }

        mPagePlay.setImageDrawable(getResources().getDrawable(R.drawable.c_selector_page_list_play));

        mSeekBack.setEnabled(false);
        mSeekFwd.setEnabled(false);

        mPageSeekBack.setEnabled(false);
        mPageSeekFwd.setEnabled(false);

        if (!mIsGtagActive) {
            mWrite.setEnabled(false);
            mRecord.setEnabled(false);
        }
    }

    /**
     * Player를 일시정지함
     */
    private void playerPause() {
        mVorbisPlayer.pause();
        playerPauseAndStop();
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        mVorbisPlayer.stop();
        playerPauseAndStop();
        mVorbisPlayer.release();
        mVorbisPlayer = null;
    }

    /**
     * Player를 종료하고 오디오 학습 완료 화면을 표시함
     */
    private void playerComplete() {

        CommonUtil.conSlogService(this, false);        //end찍고 play가 찍히는 경우가 있어서

        playerEnd();
        guidePlay(R.raw.b_31);
        findViewById(R.id.smart_study_completion_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.title_view).setVisibility(View.GONE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);
        findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
        findViewById(R.id.page_audio_study_layout).setVisibility(View.GONE);

        AnimationDrawable ad = (AnimationDrawable) ((ImageView) findViewById(R.id.befly_complete_moving)).getDrawable();
        ad.start();

        mergeFiles();

    }

    /**
     * Player Resume시 Tag정보에 의한 Seek를 처리함
     */
    private void playerTagResume() {
        if (mIsUseTag && 0 < mCurTagEventType.length()) {
            boolean isChangePosition = false;

            // S1, W1, LS1, U1, J1,JF1,JW1의 다음 재생 위치 => 다음 TAG EVENT 시간 (S2, W2, LS2,U2, J2)
            //
            if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_S1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_W1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_LS1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_U1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_J1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_V1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_JF1) || (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_JW1) && "1".equals(Preferences.getLmsStatus(mContext))) || (mStudyData.mIsTextKorean && mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_O1))) {
                String nextEventType = mCurTagEventType.substring(0, mCurTagEventType.length() - 1) + TagEventData.TAG_NUM2;
                TagEventData nextTagData = mTagEventList.get(mNextTagEventIndex);
                if (null != nextTagData && nextTagData.getEventType().equalsIgnoreCase(nextEventType)) {
                    isChangePosition = true;
                    mCurPosition = nextTagData.getEventTime();
                }
            }

            // PQ1, PA1, PI1, PT1, PR1의 다음 재생 위치 => 다음 TAG EVENT 시간 (PQ2, PA2,
            // PI2, PT2, PR2)
            if (mQuiz_Check) {//팝업퀴즈를  싱행했다면
                if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_PQ1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_PA1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_PI1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_PT1) || mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_PR1)) {
                    String nextEventType = mCurTagEventType.substring(0, mCurTagEventType.length() - 1) + TagEventData.TAG_NUM2;

                    for (int i = mNextTagEventIndex; i < mTagEventList.size(); i++) {
                        TagEventData nextTagData = mTagEventList.get(i);

                        if (null != nextTagData && nextTagData.getEventType().equalsIgnoreCase(nextEventType)) {
                            isChangePosition = true;
                            mCurPosition = nextTagData.getEventTime();
                            break;
                        }
                    }
                }
            }

            // 설정된 반복 횟수 안에서 R2의 다음 재생 위치 => R1 TAG EVENT 시간
            if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_R2)) {
                if (0 < mCurRepeatStartTime && mTotalRepeatCount > mCurRepeatCount) {
                    isChangePosition = true;
                    mCurRepeatCount++;
                    mCurPosition = mCurRepeatStartTime;
                    showRepeatText(true);
                    StudyDataUtil.requestAppRepeatLog(mContext, mCurRepeatCount);
                } else {
                    showRepeatText(false);
                    mCurRepeatCount = 1;
                    //파일합병
                    /*
                     * TagEventData te = mTagEventList.get(mCurTagEventIndex);
                     * CommonUtil.meargeMp3(StudyDataUtil.getAudioRecordPath(
                     * TagEventData.TAG_M, te.getEventTime(),
                     * isChangePosition));
                     */
                }
            }

            if (isChangePosition)
                mVorbisPlayer.seekTo((double) mCurPosition / 1000);
        }
    }

    /**
     * Progress 및 재생시간을 업데이트하여 표시함
     *
     * @param pos 현재 재생 시간
     */
    private void playerProgressUpdate(int pos) {
        mCurPosition = pos;

        if (mCurPosition > mVorbisPlayer.getDuration()) {
            Log.e("", "mVorbisPlayer mCurPosition ee => " + CommonUtil.convMsecToMinSecMsec(mCurPosition));
            return;
        }

        mProgress.setProgress(mCurPosition);
        //mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition)).append(getString(R.string.string_common_margin_slash)).append(CommonUtil.convMsecToMinSec(mVorbisPlayer.getDuration()))).toString());

        if (mIsPageStudy) {
            mPageProgress.setProgress(mCurPosition);
            mPagePlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition))));
            if ( mPageProgress.getSecondaryProgress() < mCurPosition) {
                mPageProgress.setSecondaryProgress(mCurPosition);
            }
        } else {
            mProgress.setProgress(mCurPosition);
            mPlayTimeView.setText((new StringBuilder().append(CommonUtil.convMsecToMinSec(mCurPosition))));
            if ( mProgress.getSecondaryProgress() < mCurPosition) {
                mProgress.setSecondaryProgress(mCurPosition);
            }
        }

        checkTagEvent();
    }

    /**
     * 재생 시간을 Tag 시간에 비교 체크함
     */
    private void checkTagEvent() {
        if (!mStudyData.mIsAutoArtt)
            return;

        if (mVorbisPlayer.isSeeking()) {
            // mCurTagEventType 초기화
            if (0 < mCurTagEventType.length())
                mCurTagEventType = "";
            return;
        }

        boolean isCatchTag = false;

        for (int i = 0; i < mTagEventList.size(); i++) {
            isCatchTag = false;
            TagEventData te = mTagEventList.get(i);
            String tagType = te.getEventType();

            if (te.getEventTime() <= mCurPosition && te.getEventTime() + ServiceCommon.TAG_TIME_FIX_TERM_200 > mCurPosition)
                isCatchTag = true;

            if (isCatchTag) {
                // 동일 이벤트 타입의 연속 방지
                if (mCurTagEventType.equalsIgnoreCase(tagType) && mCurTagEventIndex == i)
                    return;

                mCurTagEventIndex = te.getEventIndex();
                mCurTagEventType = tagType;
                mNextTagEventIndex = i + 1;

                mCurVideoIndex = te.getVideoIndex();
                mCurV1TagIndex = te.getV1TagIndex();
                mCurQuizIndex = te.getQuizIndex();

                int repeatIndex = te.getRepeatIndex();
                int repeatStartTime = te.getRepeatStartTime();
                if (mCurRepeatIndex != repeatIndex)
                    mCurRepeatCount = 1;
                mCurRepeatIndex = repeatIndex;
                mCurRepeatStartTime = repeatStartTime;

                mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_TAG_EVENT, te.checkEventType(), te.getEventTime()));
                break;
            }
        }
    }

    /**
     * 재생 시간이 Tag 시간에 속한 경우 해당 Tag 기능을 수행함
     */
    private void executeTagEvent(TAG_EVENT_TYPE_ENUM event, int curEventTime) {
        if (ServiceCommon.IS_CONTENTS_TEST) {
            ((TextView) findViewById(R.id.audio_study_record_mode_info)).setText(mCurTagEventType + " (" + CommonUtil.convMsecToMinSecMsec(curEventTime) + ")  PlayTime (" + CommonUtil.convMsecToMinSecMsec(mCurPosition) + ")");
        }

        switch (event) {
            case TAG_EVENT_S0:
            case TAG_EVENT_LS0:
            case TAG_EVENT_LS1:
                recordReady(true, ServiceCommon.MAX_REC_TIME_3MIN);
                break;

            case TAG_EVENT_S1:
                recordReady(true, ServiceCommon.MAX_REC_TIME_20SEC);
                break;

            case TAG_EVENT_W0:
            case TAG_EVENT_W1:
                writeStart();
                break;

            case TAG_EVENT_G1:
                mIsGtagActive = true;
                setGtagVolume(true);
                recordReady(true, getMaxRecordTime(ServiceCommon.MAX_REC_TIME_20SEC, curEventTime));
                break;

            case TAG_EVENT_LG1:
                mIsGtagActive = true;
                setGtagVolume(true);
                checkRecordPlayPassTagArg();
                recordReady(true, getMaxRecordTime(ServiceCommon.MAX_REC_TIME_3MIN, curEventTime));
                break;

            case TAG_EVENT_G2:
            case TAG_EVENT_LG2:
                if (mIsGtagActive) {
                    playerPause();
                    mIsGtagActive = false;
                    setGtagVolume(false);
                    if (mShowPopup != null) {
                        //버튼리소스읽어와서 버튼 활성화 처리
                        final TextView tvCancel = mShowPopup.findViewById(R.id.tv_text_ok);
                        if (tvCancel != null) {
                            tvCancel.setClickable(true);
                            tvCancel.setEnabled(true);
                        }
                    }
                    if (mIsG2tagRecordEnd)
                        recordEnd();
                }
                break;

            case TAG_EVENT_U0:
            case TAG_EVENT_U1:
                playerPause();

                ShowPopup.getInstance().setCallBackListener(mImpl);
                ShowPopup.getInstance().ShowLeaningTextBook(SmartStudyActivity.this, "교재 활동을 마치면");
                break;

            case TAG_EVENT_J1:
                if (mStudyData.mIsTextKorean) {//igse용 한글구문 스킵기능
                    playerPause();
                    playerResume();
                }
                break;
            case TAG_EVENT_O1:
                playerPause();
                playerResume();
                break;

            case TAG_EVENT_R1:
                mIsRtagActive = true;//녹음 모음 때분에 구분
                TagEventData te = mTagEventList.get(mCurTagEventIndex);
                rStartTime = te.getEventTime();
                rTAagArg = te.getEventTypeArg();
                mStudyData.rRecType = te.getEventTypeArg();

                checkRecordPlayPassTagArg();
                if (mTotalRepeatCount > 1)
                    showRepeatText(true);
                break;

            case TAG_EVENT_R2:
                mIsRtagActive = false;
                TagEventData te2 = mTagEventList.get(mCurTagEventIndex);
                //			rEndTime = getTagTime(te2);
                rEndTime = te2.getEventTime();
                mStudyData.rRecType = "";
                checkRecordPlayPassTagArg();
                showRepeatText(false);
                playerPause();
                playerResume();

                rStartTime = 0;
                rEndTime = 0;
                rTAagArg = "";
                break;

            case TAG_EVENT_V:
                startAniTalk();
                break;
            case TAG_EVENT_V1:
                startVideoPlay();
                break;
            case TAG_EVENT_V2:
                break;

            // 신규문항
            case TAG_EVENT_PQ0:
            case TAG_EVENT_PQ1:
            case TAG_EVENT_PA0:
            case TAG_EVENT_PA1:
            case TAG_EVENT_PI0:
            case TAG_EVENT_PI1:
                mIsPQtagActive = true;
                playerPause();
                // mPopupStudyOrder++;
                popupWebView(event);
                break;
            case TAG_EVENT_PQ2:
            case TAG_EVENT_PA2:
            case TAG_EVENT_PI2:
                playerResume();
                break;
            case TAG_EVENT_PT0:
            case TAG_EVENT_PT1:
                mIsPTtagActive = true;
                playerPause();
                // mPopupStudyOrder++;
                popupWebView(event);
                break;
            case TAG_EVENT_PT2:
                // playerResume();
                break;
            case TAG_EVENT_PR0:
            case TAG_EVENT_PR1:
                mIsPRtagActive = true;
                playerPause();
                // mPopupStudyOrder++;
                popupWebView(event);
                break;
            case TAG_EVENT_PR2:
                // playerResume();
                break;

            case TAG_JF1:
                playerPause();
                playerResume();
                break;

            case TAG_JW1:
                if ("1".equals(Preferences.getLmsStatus(getApplicationContext()))) {//우영일때만 동작
                    playerPause();
                    playerResume();
                }
                break;
            case TAG_EVENT_PAGE0:
            case TAG_EVENT_PAGE1:
            case TAG_EVENT_PAGE2:
                TagEventData type = getTagType(curEventTime);
                Log.i("", "event => " + event + " time => " + CommonUtil.convMsecToMinSecMsec(curEventTime) + " page => " + type.getPageIndex() );
                setPageIndex(type.getPageIndex());
                break;
            default:
                break;
        }
    }

    /**
     * G1G2, LG1LG2 TAG를 이용하여 최대 녹음 시간(녹음 자동 종료 시간)을 계산 후 반환함
     *
     * @param baseMaxRecordTime 20초 또는 3분
     * @param curEventTime      현재 Tag의 시간
     * @return 최대 녹음 시간
     */
    private int getMaxRecordTime(int baseMaxRecordTime, int curEventTime) {
        int maxRecordTime = ServiceCommon.MAX_REC_TIME_3MIN;
        try {
            TagEventData nextTe = mTagEventList.get(mNextTagEventIndex);
            String nextEventType = mCurTagEventType.substring(0, mCurTagEventType.length() - 1) + TagEventData.TAG_NUM2;

            if (null == nextTe || !nextTe.getEventType().equalsIgnoreCase(nextEventType))
                return baseMaxRecordTime;

            Log.i("", "getMaxRecordTime curEventTime => " + CommonUtil.convMsecToMinSecMsec(curEventTime) + " nextEventTime() => " + CommonUtil.convMsecToMinSecMsec(nextTe.getEventTime()));

            int tagGap = nextTe.getEventTime() - curEventTime;
            int customMaxRecordTime = (int) (tagGap * 1.2);
            maxRecordTime = baseMaxRecordTime;

            if (baseMaxRecordTime < customMaxRecordTime)
                maxRecordTime = customMaxRecordTime;

            Log.i("", "getMaxRecordTime customMaxRecordTime => " + customMaxRecordTime + " maxRecordTime => " + maxRecordTime);
        } catch (Exception e) {//에러시 맥스값으로
            e.printStackTrace();
            return baseMaxRecordTime;
        }
        return maxRecordTime;
    }

    /**
     * Tag 속성에 의한 에코 패스 여부를 체크함
     */
    private void checkRecordPlayPassTagArg() {
        if (mIsUseTag && 0 < mCurTagEventType.length()) {
            TagEventData te = mTagEventList.get(mCurTagEventIndex);
            if (null == te)
                return;
            String tagTypeArg = te.getEventTypeArg();
            if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_LG1)) {
                if (tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_SPEAKGUIDE_I) || tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_TEXTSPEAKGUIDE_I) || tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_WORDSPEAKGUIDE_I))
                    mIsLGtagEchoPassByLGtagArg = true;
            } else if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_R1)) {
                if (tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_ROLEPLAY_I))
                    mIsStagEchoPassByRtagArg = true;
                else if (tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_ROLEPLAYGUIDE_I))
                    mIsGtagEchoPassByRtagArg = true;
            } else if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_R2)) {
                if (tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_ROLEPLAY_O))
                    mIsStagEchoPassByRtagArg = false;
                else if (tagTypeArg.equalsIgnoreCase(TagEventData.TAG_ARG_01_ROLEPLAYGUIDE_O))
                    mIsGtagEchoPassByRtagArg = false;
            }
        }
    }

    /**
     * 학습 가이드를 표시함
     *
     * @param tip         학습 가이드 내용
     * @param buttonIndex 3개의 버튼 위치 중 현재 위치 인덱스값
     */
    private void showToolTip(String tip, int buttonIndex) {
        if (!mStudyData.mIsStudyGuide)
            return;

        int textWidth = getResources().getDimensionPixelSize(R.dimen.dimen_395);

        if (0 < mTooltipWidth)
            textWidth = mTooltipWidth;

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);

        int x = 0, y = 0;
        LinearLayout tbpl = ((LinearLayout) findViewById(R.id.audio_study_three_btn_parent_layout));
        //TextView margin2 = ((TextView) findViewById(R.id.audio_study_three_btn_margin2));
        //TextView margin3 = ((TextView) findViewById(R.id.audio_study_three_btn_margin3));
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));

        tipText.setText(tip);
        mParentLayout.addView(tooltip);

        int centerWidth = tbpl.getWidth() / 2;

        /*if (buttonIndex == 0)
            x = (centerWidth - (mWrite.getWidth() / 2) - (mPlay.getWidth() / 2) - margin2.getWidth()) - (textWidth / 2);
        else if (buttonIndex == 1)
            x = centerWidth - (textWidth / 2);
        else
            x = (centerWidth + (mWrite.getWidth() / 2) + (mRecord.getWidth() / 2) + margin3.getWidth()) - (textWidth / 2);*/

        y = mParentLayout.getHeight() - tbpl.getHeight() - (mTooltipHeight / 2);

        ViewGroup.MarginLayoutParams vm = (MarginLayoutParams) tooltip.getLayoutParams();
        vm.leftMargin = x;
        vm.topMargin = y;
    }

    /**
     * 학습 가이드를 지움
     */
    private void hideToolTip() {
        if (null != findViewById(R.id.tooltip_record_text))
            mParentLayout.removeView(findViewById(R.id.tooltip_record_text));
    }

    // --- AniTalk ---

    private void setVideoPlayer(String path, boolean isFast) {
        // 로컬 파일 재생 방식에서 url로 변경됨 2013.11.4
        // mStudyData.mAniTalk.get(index) 형태
        // "http://vod.yoons.gscdn.com:8080/Anitalk/001_1AB_High.mp4";

        if (0 < ((LinearLayout) findViewById(R.id.animation_study_layout)).getChildCount())
            ((LinearLayout) findViewById(R.id.animation_study_layout)).removeAllViewsInLayout();

        mVideoPlayer = new CustomVideoPlayer(this, mHandler, path, true, isFast);
        ((LinearLayout) findViewById(R.id.animation_study_layout)).addView(mVideoPlayer);
    }

    private void startAniTalk() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);
        playerPause();

        if (0 == ((LinearLayout) findViewById(R.id.animation_study_layout)).getChildCount()) {
            if (0 <= mCurVideoIndex && 0 < mStudyData.mAniTalk.size()) {
                Log.i("", "Animation index => " + mCurVideoIndex + " path => " + mStudyData.mAniTalk.get(mCurVideoIndex));
                setVideoPlayer(mStudyData.mAniTalk.get(mCurVideoIndex), false);
            }
        }

        if (0 < ((LinearLayout) findViewById(R.id.animation_study_layout)).getChildCount()) {
            if (!CommonUtil.isAvailableNetwork(this)) {
                showAniTalkNetworkErrorDialog();
                return;
            }

            Log.i("", "Animation index => " + mCurVideoIndex + " path => " + mStudyData.mAniTalk.get(mCurVideoIndex));
            findViewById(R.id.page_audio_study_layout).setVisibility(View.GONE);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
            findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);
            //findViewById(R.id.setp_statusbar).setVisibility(View.GONE);
            findViewById(R.id.title_view).setVisibility(View.GONE);
            findViewById(R.id.animation_study_layout).setVisibility(View.VISIBLE);
        } else {
            playerResume();
        }
    }

    private void endAniTalk() {
        ((LinearLayout) findViewById(R.id.animation_study_layout)).removeAllViewsInLayout();
        findViewById(R.id.animation_study_layout).setVisibility(View.GONE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.VISIBLE);
        //findViewById(R.id.setp_statusbar).setVisibility(View.VISIBLE);
        findViewById(R.id.title_view).setVisibility(View.VISIBLE);
        if (mIsPageStudy) {
            findViewById(R.id.page_audio_study_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.page_audio_study_layout).setVisibility(View.GONE);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.VISIBLE);
        }
        playerResume();
    }

    private void showAniTalkConfirmDialog() {
        if (null == mContext)
            return;

        mMsgBox = new MessageBox(this, 0, R.string.string_msg_ani_talk_confirm);
        mMsgBox.setConfirmText(R.string.string_common_retry);
        mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setOnDialogDismissListener(mVideoPlayer.mDialogDismissListener);
        mMsgBox.show();
    }

    private void showAniTalkNetworkErrorDialog() {
        if (null == mContext)
            return;

        mMsgBox = new MessageBox(this, 0, R.string.string_msg_network_error_video_no_play);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private void showAniTalkNetworkErrorDialog(int errorWhat, int errorExtra) {
        if (null == mContext)
            return;

        String errorMsg = getString(R.string.string_msg_network_error_video_end);
        if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_IO)
            errorMsg += "\nMEDIA_ERROR_IO";
        else if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_TIMED_OUT)
            errorMsg += "\nMEDIA_ERROR_TIMED_OUT";
        else if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_UNSUPPORTED)
            errorMsg = "\nMEDIA_ERROR_UNSUPPORTED";
        else if (errorExtra == CustomVideoPlayer.MEDIA_ERROR_MALFORMED)
            errorMsg += "\nMEDIA_ERROR_MALFORMED";
        else
            errorMsg += "\n" + errorExtra;

        String error = String.format("[ %d ] %s", errorWhat, errorMsg);
        mMsgBox = new MessageBox(this, 0, error);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    @Override
    public void onDialogDismiss(int result, int dialogId) {
        endAniTalk();
    }

    // --- Recording && Write Animation effect ----

    /**
     * 녹음 진행 효과 시작 Runnable
     */
    private Runnable recordEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            /*try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    };

    /**
     * 쓰기 진행 효과 시작 Runnable
     */
    private Runnable writeEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            /*try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mWrite).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    };

    /**
     * 녹음 진행 효과 종료 Runnable
     */
    private Runnable recordEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

           /* try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    };

    /**
     * 쓰기 진행 효과 종료 Runnable
     */
    private Runnable writeEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

           /* try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mWrite).getDrawable();
                aniDraw.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    };

    /**
     * 쓰기,녹음 진행 효과를 시작함
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void startIngEffect(boolean isRecord) {
        Runnable run = null;
        if (isRecord) {
            run = recordEffectStartRunnable;
            //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_record_volume));
        } else {
            run = writeEffectStartRunnable;
            //mWrite.setImageDrawable(getResources().getDrawable(R.drawable.animation_list_write_ing));
        }

        mIngEffectThread = new Thread(run);
        mIngEffectThread.start();
    }

    /**
     * 쓰기,녹음 진행 효과를 종료함
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void stopIngEffect(boolean isRecord) {
        if (null != mIngEffectThread) {
            mIngEffectThread.interrupt();
            mIngEffectThread = null;
        }

        mIngEffectThread = new Thread(isRecord ? recordEffectStopRunnable : writeEffectStopRunnable);
        mIngEffectThread.start();
    }

    /**
     * 쓰기,녹음 완료 효과를 1초간 진행함
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void completeEffect(final boolean isRecord) {
        Runnable run = null;
        if (isRecord) {
            run = recordEffectStartRunnable;

            if (CommonUtil.isCenter()) // igse
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_seq_learning_progress));
            else {
                if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_seq_learning_progress));
                else // 숲
                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_seq_learning_progress));
            }
        } else {
            run = writeEffectStartRunnable;
            //mWrite.setImageDrawable(getResources().getDrawable(R.drawable.animation_list_write_complete));
        }

        mCompleteEffectThread = new Thread(run);
        mCompleteEffectThread.start();

        Runnable runnable = null;
        if (isRecord) {
            runnable = new Runnable() {
                public void run() {
                    if (null == mContext)
                        return;

                    if (null != mCompleteEffectThread) {
                        mCompleteEffectThread.interrupt();
                        mCompleteEffectThread = null;
                    }

                   /* try {
                        AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                        aniDraw.stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (!(mIsPQtagActive || mIsPTtagActive || mIsPRtagActive))
                            recordPlay();
                    }*/

                    if (!(mIsPQtagActive || mIsPTtagActive || mIsPRtagActive))
                        recordPlay();
                }
            };
            mRecord.postDelayed(runnable, 1000);
        } else {
            runnable = new Runnable() {

                public void run() {
                    if (null == mContext)
                        return;

                    if (null != mCompleteEffectThread) {
                        mCompleteEffectThread.interrupt();
                        mCompleteEffectThread = null;
                    }

                   /* try {
                        AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mWrite).getDrawable();
                        aniDraw.stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        writeComplete();
                    }*/

                    writeComplete();
                }
            };
            mWrite.postDelayed(runnable, 1000);
        }
    }

    // ------------------- Recording----------------------

    /**
     * WebView 녹음 재생 준비 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnPreparedListener mWebViewRecordPrepared = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            if (null == mContext)
                return;

            mWebViewRecordPlayer.start();
        }
    };

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordNotiCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            recordNotiEnd();
            recordStart();
        }
    };

    /**
     * 녹음 시작 알림음을 재생함
     */
    private void recordNotiPlay() {
        if (null != mRecordNotiPlayer)
            return;

        mRecordNotiPlayer = MediaPlayer.create(getBaseContext(), R.raw.ding);
        if (null != mRecordNotiPlayer) {
            mRecordNotiPlayer.setOnCompletionListener(mRecordNotiCompletion);
            mRecordNotiPlayer.start();
        }
    }

    /**
     * 녹음 시작 알림음을 종료함
     */
    private void recordNotiEnd() {
        if (null == mRecordNotiPlayer)
            return;

        if (mRecordNotiPlayer.isPlaying())
            mRecordNotiPlayer.stop();
        mRecordNotiPlayer.release();
        mRecordNotiPlayer = null;
    }

    /**
     * 버튼 비활성화 및 Player를 일시정지하고 녹음 시작 알림음을 재생시켜 녹음을 준비함
     */
    private void recordReady(boolean isPlayPause, int maxRecTime) {
        if (mMp3Recorder.isRecording())
            return;

        if (isPlayPause) {
            if (mVorbisPlayer == null)
                return;//맨 마지막에 레코드 호출하는 경우가 있음(팝업퀴즈로 예상)
            if (mVorbisPlayer.isPlaying())
                playerPause();
        }

        mPlay.setEnabled(false);
        mWrite.setEnabled(false);
        mRecord.setEnabled(false);

        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);

        mMaxRecTime = maxRecTime;

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_READY, 0), 100);
    }

    /**
     * 녹음을 시작하고 1초 뒤 녹음 완료를 진행 할 수 있도록 녹음 버튼을 활성화 함 현재 G1,LG1 Tag인 경우 Player를
     * Resume함
     */
    private void recordStart() {
        if (mIsGtagActive)
            playerResume();

        if (!(mIsPQtagActive || mIsPTtagActive || mIsPRtagActive))
            //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_start));

            mRecordPath = "";

        // 신규문항
        if (mIsPQtagActive || mIsPTtagActive || mIsPRtagActive) {
            if (mPopupStudyResult == null || mPopupRecordResult.size() == 0) { // 터치앤플레이나
                // 롤플레이
                // 태그
                // 사용중
                // 웹뷰에서
                // startDeviceRecord()
                // 없이
                // 녹음
                // 시작된
                // 경우
                Toast.makeText(this, getString(R.string.string_common_study_data_error), Toast.LENGTH_SHORT).show();
                mIsPQtagActive = false;
                mIsPTtagActive = false;
                mIsPRtagActive = false;

                if (mIsUseTag) {
                    TagEventData te = mTagEventList.get(mCurTagEventIndex);
                    mRecordPath = null == te ? StudyDataUtil.getAudioRecordPath("", 0) : StudyDataUtil.getAudioRecordPath(te.getEventType(), te.getEventTime());
                } else {
                    mRecordPath = mRecordFilePath;
                }
            } else {
                mRecordPath = StudyDataUtil.getPopupRecordPath(mPopupStudyResult.mStudyOrder, mPopupStudyResult.mQuestionNo, mPopupStudyResult.mQuestionOrder);
            }
        } else {
            if (mIsUseTag) {
                TagEventData te = mTagEventList.get(mCurTagEventIndex);
                mRecordPath = null == te ? StudyDataUtil.getAudioRecordPath("", 0) : StudyDataUtil.getAudioRecordPath(te.getEventType(), te.getEventTime());
            } else {
                mRecordPath = mRecordFilePath;
            }
        }

        // // 신규문항
        // if (mIsPTtagActive || mIsPRtagActive) {
        // mRecordPath = mRecordPath.replaceAll("SA", "PQ");
        // String []flag = mRecordPath.split("\\.");
        //
        // if ( flag.length != 0 && mPopupStudyResult != null)
        // {
        //// mRecordPath = flag[0] + "_" + mPopupStudyResult.mTagType + "_" +
        // mPopupStudyResult.mQuestionNo + "_" +
        //// mPopupStudyResult.mQuestionOrder + "." + flag[1];
        // mRecordPath = flag[0] + "_" + mPopupStudyResult.mQuestionNo + "_" +
        // mPopupStudyResult.mQuestionOrder + "." + flag[1];
        // }
        // }

        // Log.k("wusi12", "recordPath : "+ recordPath);

        mMp3Recorder.setFilePath(mRecordPath);
        mMp3Recorder.start();

        if (0 < mMaxRecTime)
            mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), mMaxRecTime);

        if ((mIsPQtagActive || mIsPTtagActive || mIsPRtagActive))
            return;

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect(true);
                //showToolTip(getResources().getString(R.string.string_record_tool_tip), 2);
                mRecord.setEnabled(true);
            }
        };
        mRecord.postDelayed(runnable, 1000);
    }

    /**
     * 녹음을 종료함 현재 G1,LG1 Tag인 경우 녹음 종료를 미수행함
     */
    private void recordEnd() {
        Log.e("hy", "recordEnd()1");
        if (mIsGtagActive) {
            Log.e("hy", "recordEnd()2");
            mIsG2tagRecordEnd = true;
            return;
        }

        Log.e("hy", "recordEnd()3");
        if (mMp3Recorder.isRecording()) {
            Log.e("hy", "recordEnd()4");
            mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
            mMp3Recorder.stop();
            //녹음시간 측정
            /*mStudyData.mRecEndTime = System.currentTimeMillis();//mp3Record 녹음 종료 시점
            if (mStudyData.mPassRcheck) {//수동일떄
				mStudyData.mRecMTime = mStudyData.mRecMTime + (int) ((mStudyData.mRecEndTime - mStudyData.mRecStartTime) / 1000.0);
			} else {//자동일때
				mStudyData.mTotalRecTime = mStudyData.mTotalRecTime + (int) ((mStudyData.mRecEndTime - mStudyData.mRecStartTime) / 1000.0);
			}

			mStudyData.mRecStartTime = 0;
			mStudyData.mRecEndTime = 0;*/
            Log.e("hy", "recordEnd()5");
            mRecord.setEnabled(false);
            mIsG2tagRecordEnd = false;
            if (!(mIsPQtagActive || mIsPTtagActive || mIsPRtagActive)) {
                /*Log.e("hy", "recordEnd()6");
                stopIngEffect(true);
                hideToolTip();
                completeEffect(true);
                Log.e("hy", "recordEnd()7");*/
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (mShowPopup != null) {
                    mShowPopup.dismiss();
                }
                mShowPopup = ShowPopup.getInstance().ShowLeaningListen(this, "잘 들어보세요.");
                popupComplete(true);
            }
        }
    }

    /**
     * 쓰기,녹음 완료시 처리
     *
     * @param isRecord 쓰기,녹음 구분
     */
    private void popupComplete(final boolean isRecord) {
        if (isRecord) {
            if (!(mIsPQtagActive || mIsPTtagActive || mIsPRtagActive))
                recordPlay();
        } else {
            writeComplete();
        }
    }

    // ------------ recordPlay (Echo) ---------

    /**
     * 에코 패스 플래그를 체크하여 패스 여부를 반환함
     *
     * @return 에코 패스 여부
     */
    private boolean checkRecordPlayPass() {
        boolean isRecordPlayPass = false;

        if (mIsUseTag && 0 < mCurTagEventType.length()) {
            // Stag와 Gtag는 R2에서 pass flag 해제함
            if (mIsStagEchoPassByRtagArg) {
                if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_S1))
                    isRecordPlayPass = true;
            }

            if (mIsGtagEchoPassByRtagArg) {
                if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_G2))
                    isRecordPlayPass = true;
            }

            if (mIsLGtagEchoPassByLGtagArg) {
                if (mCurTagEventType.equalsIgnoreCase(TagEventData.TAG_LG2)) {
                    isRecordPlayPass = true;
                    mIsLGtagEchoPassByLGtagArg = false;
                }
            }
        }

        return isRecordPlayPass;
    }

    /**
     * 에코를 재생함
     */
    private void recordPlay() {
        if (null != mRecordPlayer && mRecordPlayer.isPlaying())
            return;

        String recordPlayPath = "";
        if (mIsUseTag)
            recordPlayPath = StudyDataUtil.getAudioRecordPlayPath();
        else
            recordPlayPath = mRecordFilePath;

        File recordPlayFile = new File(recordPlayPath);
        if (!recordPlayFile.exists() || checkRecordPlayPass()) {
            recordPlayEnd();
            return;
        }

        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo2_red_btn_bg));

        try {
            mRecordPlayer = new MediaPlayer();
            mRecordPlayer.reset();
            mRecordPlayer.setDataSource(recordPlayPath);
            mRecordPlayer.setOnCompletionListener(this);
            mRecordPlayer.setOnPreparedListener(this);
            mRecordPlayer.prepare();
            int echoDuration = mRecordPlayer.getDuration();
            Log.i("", "recordPlay echoDuration => " + echoDuration);
            if (15000 <= echoDuration)
                setRecordPlayEndEnable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 에코가 15초 이상 진행시 에코 종료 가능하도록 버튼을 활성화 함
     */
    private void setRecordPlayEndEnable() {
        Runnable runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                if (null != mRecordPlayer && mRecordPlayer.isPlaying())
                    mRecord.setEnabled(true);
            }
        };
        mRecord.postDelayed(runnable, 15000);
    }

    /**
     * 에코 재생을 종료하고 에코 볼륨을 원복함
     */
    private void recordPlayEnd() {
        // 녹음 재생 후 종료
        int curRecTime = 0;
        try {
            if (mRecordPlayer != null) {
                curRecTime = mRecordPlayer.getDuration();
            } else if (mMp3Recorder != null && curRecTime == 0) {
                curRecTime = mMp3Recorder.getDuration();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mStudyData.mPassRcheck) {//수동녹음 일때
            mStudyData.mRecMTime = mStudyData.mRecMTime + (curRecTime / 1000);//수동녹음 합
            mStudyData.mPassRcheck = false;
        } else {//자동녹음일때
            mStudyData.mTotalRecTime = mStudyData.mTotalRecTime + (curRecTime / 1000);//자동녹음 합
        }

        if (null != mRecordPlayer) {
            if (mRecordPlayer.isPlaying())
                mRecordPlayer.stop();

            mRecordPlayer.release();
            mRecordPlayer = null;
            releaseEchoVolume();
        }
        //업로드가 false일때나 true일때 구간반복의 마지막이 아닐경우
        if (mIsUseTag && !mStudyData.mIsAudioRecordUpload || mIsUseTag && mStudyData.mIsAudioRecordUpload && mTotalRepeatCount != mCurRepeatCount && mIsRtagActive) {
            File fileRecord = new File(StudyDataUtil.getAudioRecordPlayPath());
            if (fileRecord.exists())
                fileRecord.delete();
        } else if (mIsRtagActive) {//머지파일 DB에 넣기
            //TagEventData te = mTagEventList.get(mCurTagEventIndex);
            int eventTime = 0;
            try {
                rEndTime = mTagEventList.get(mCurTagEventIndex + 2).getEventType().equals(TagEventData.TAG_R2) ? mTagEventList.get(mCurTagEventIndex + 2).getEventTime() : 0;
                eventTime = mTagEventList.get(mCurTagEventIndex + 1).getEventTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e(TAG, "rEndTime:" + rEndTime);
            if (!"".equals(mRecordPath) && !mRecordPath.equals(mRecordFilePath)) {
                DatabaseUtil.getInstance().insertRecordDetailC(StudyDataUtil.getAudioRecordPlayPath(), rTAagArg, rStartTime, rEndTime, curRecTime, mStudyData.mRTagRepeatCnt, 1, mStudyData.mBookDetailId, mStudyData.mStudentBookId, eventTime, true);
            }
        } else if (!mIsRtagActive) {//한개짜리 파일일때
            try {
                int eventTime = 0;
                TagEventData te = mTagEventList.get(mCurTagEventIndex);
                String tagTypeArg = te.getEventTypeArg();

                eventTime = mTagEventList.get(mCurTagEventIndex + 1).getEventTime();

                if (!"".equals(mRecordPath) && !mRecordPath.equals(mRecordFilePath)) {
                    DatabaseUtil.getInstance().insertRecordDetailC(StudyDataUtil.getAudioRecordPlayPath(), tagTypeArg, rStartTime, rEndTime, curRecTime, mStudyData.mRTagRepeatCnt, 0, mStudyData.mBookDetailId, mStudyData.mStudentBookId, eventTime, false);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Crashlytics.logException(e);
            }
        }

        File fileRecord = new File(mRecordFilePath);
        if (fileRecord.exists())
            fileRecord.delete();

//        mWrite.setEnabled(true);
//        mRecord.setEnabled(true);
//        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));

        if (mShowPopup != null) {
            mShowPopup.dismiss();
        }

        playerResume();
    }

    /**
     * 에코를 재생함
     */

    private void recordPlayWebView() {
        if (null != mWebViewRecordPlayer && mWebViewRecordPlayer.isPlaying())
            return;

        String path = mWebViewRecordPath;
        Log.e("ss99km01", "ss99km01 recordPlayWebView path = " + path);

        File fileRecord = new File(path);
        if (fileRecord.exists()) {
            String tempFilePath = ServiceCommon.CONTENTS_PATH + "temp_record" + ServiceCommon.MP3_FILE_TAIL;
            CommonUtil.removeEndNoise(path, tempFilePath);
        }

        File recordPlayFile = new File(path);
        if (!recordPlayFile.exists() || checkRecordPlayPass()) {
            recordPlayEndWebView();
            return;
        }

        try {
            mWebViewRecordPlayer = new MediaPlayer();
            mWebViewRecordPlayer.reset();
            mWebViewRecordPlayer.setDataSource(path);
            mWebViewRecordPlayer.setOnPreparedListener(this);
            mWebViewRecordPlayer.setOnPreparedListener(mWebViewRecordPrepared);
            mWebViewRecordPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 에코 재생을 종료하고 에코 볼륨을 원복함
     */
    private void recordPlayEndWebView() {
        // 녹음 재생 후 종료
        if (null != mWebViewRecordPlayer) {
            if (mWebViewRecordPlayer.isPlaying())
                mWebViewRecordPlayer.stop();
            mWebViewRecordPlayer.release();
            mWebViewRecordPlayer = null;
            releaseEchoVolume();
        }
    }

    /**
     * 에코 플레이어의 준비 완료시 에코 볼륨 설정 및 재생을 시작함
     */
    public void onPrepared(MediaPlayer mp) {
        if (null != mRecordPlayer) {
            setEchoVolume();
            mRecordPlayer.start();
        }
    }

    /**
     * 에코 재생 완료시 에코 재생을 종료함
     */
    public void onCompletion(MediaPlayer mp) {
        recordPlayEnd();
    }

    // ------------------- Writing----------------------

    /**
     * Player를 일시정지하고 쓰기를 시작함
     */
    private void writeStart() {
        if (mIsWriting)
            return;

        if (mVorbisPlayer.isPlaying())
            playerPause();

        mPlay.setEnabled(false);
        //mWrite.setEnabled(false);
        //mRecord.setEnabled(false);

        mIsWriting = true;
        ShowPopup.getInstance().setCallBackListener(mImpl);
        ShowPopup.getInstance().ShowLeaningOk(mContext, "     쓰기를 마치면 ");

        // mWrite.setImageDrawable(getResources().getDrawable(R.drawable.btn_write_start));

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect(false);
                //showToolTip(getResources().getString(R.string.string_write_tool_tip), 1);
                mIsWriting = true;
                mWrite.setEnabled(true);
            }
        };
        mWrite.postDelayed(runnable, 1000);
    }

    /**
     * 쓰기를 종료하고 완료 효과를 표시함
     */
    private void writeEnd() {
        if (mIsWriting) {
            mWrite.setEnabled(false);
            stopIngEffect(false);
            //hideToolTip();
            completeEffect(false);
        }
    }

    /**
     * 쓰기를 완료하고 Player를 Resume함
     */
    private void writeComplete() {
        if (mIsWriting) {
            //mWrite.setImageDrawable(getResources().getDrawable(R.drawable.selector_write_btn_bg));
            mIsWriting = false;
            mWrite.setEnabled(true);
            mRecord.setEnabled(true);
            playerResume();
        }
    }

    // ---- 재생 시간 기록 ---

    /**
     * 재생 시간을 DB에 업데이트함
     */
    private void updatePlayTimeDB() {
        DatabaseUtil dbUtil = DatabaseUtil.getInstance(this);
        dbUtil.updateStudyResultAudioPlayTime(mCurPosition);

        mStudyData.mAudioStudyTime = mCurPosition;
    }

    /**
     * 10초마다 updatePlayTimeDB()를 수행하는 Thread를 시작함
     */
    private void startPlayTimeUpdateThread() {
        mPlayTimeUpdateThread = new Thread("playTimeUpdateThread") {
            public void run() {
                while (null != mPlayTimeUpdateThread && !mPlayTimeUpdateThread.isInterrupted()) {
                    SystemClock.sleep(1000);

                    mPlayTimeUpdateTime += 1000;

                    if (null == mContext)
                        return;

                    // 10초
                    if (10000 == mPlayTimeUpdateTime) {
                        updatePlayTimeDB();
                        mPlayTimeUpdateTime = 0;
                    }
                }
            }
        };

        mPlayTimeUpdateThread.start();
    }

    // ---- 학습 시간 체크 ---

    /**
     * 5분 마다 StudyDataUtil.requestStudyResultBookCheck()를 수행하는 Thread를 시작함
     */
    /*private void startBookCheckThread() {
        mBookCheckThread = new Thread("bookCheckThread") {
			public void run() {
				while (null != mBookCheckThread && !mBookCheckThread.isInterrupted()) {
					SystemClock.sleep(1000);

					mBookCheckTime += 1000;

					if (null == mContext)
						return;

					// 5분
					if (300000 == mBookCheckTime) {
						mHandler.sendEmptyMessage(ServiceCommon.MSG_WHAT_BOOK);
						mBookCheckTime = 0;
					}
				}
			}
		};

		mBookCheckThread.start();
	}*/

    // ----- 학습 시간 경고 체크 ---

    /**
     * 2분 마다 StudyDataUtil.checkBookWarning()를 수행하는 Thread를 시작함
     */
/*	private void startBookWarningCheckThread() {
        mBookWarningCheckThread = new Thread("bookWarningCheckThread") {
			public void run() {
				while (null != mBookWarningCheckThread && !mBookWarningCheckThread.isInterrupted()) {
					SystemClock.sleep(1000);

					mBookWarningCheckTime += 1000;

					if (null == mContext)
						return;

					// 2분
					if (120000 == mBookWarningCheckTime) {
						mHandler.sendEmptyMessage(ServiceCommon.MSG_WHAT_BOOK_WARNING);
						mBookWarningCheckTime = 0;
					}
				}
			}
		};

		mBookWarningCheckThread.start();
	}
*/
    // ----- 다음 학습 이동 ---

    /**
     * 다음 단계로 이동 처리함
     */
    private void goNextStatus() {
        if (isNext) {
            isNext = false;
            if (0 < mStudyData.mWordQuestion.size() || 0 < mStudyData.mSentenceQuestion.size() || (mStudyData.mIsParagraph && 0 < mStudyData.mVanishingQuestion.size()))
                startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));

            else if (mStudyData.mIsDictation && 0 < mStudyData.mDictation.size())
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            else if (mStudyData.isNewDication && mStudyData.mIsDicPirvate)
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            else if (mStudyData.mIsMovieExist)
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            else if (mStudyData.mOneWeekData.size() > 0) {
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            } else
                startActivity(new Intent(this, StudyOutcomeActivity.class));

            StudyDataUtil.setCurrentStudyStatus(this, "S12");
            finish();
        }
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            if (null == mVorbisPlayer)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_VIDEO_PLAYER:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_CONFIRM) {
                        showAniTalkConfirmDialog();
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_VIDEO_PROGRESS_COMPLETION) {
                        if (mCurTagEventType.equals(TagEventData.TAG_V1)) {
                            endVideoPlay();
                        } else {
                            endAniTalk();
                        }
                    }

                    break;

                case ServiceCommon.MSG_WHAT_VIDEO_PLAYER_ERROR:
                    showAniTalkNetworkErrorDialog(msg.arg1, msg.arg2);
                    break;

                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START) {
                        startStudy();
                    }
                    break;

                case ServiceCommon.MSG_WHAT_TAG_WARNING:
                /*if (mStudyData.mIsAutoArtt)
                    StudyDataUtil.requestAppSystemLog(mContext);*/
                    break;

                case ServiceCommon.MSG_WHAT_TAG_EVENT:
                    executeTagEvent(TAG_EVENT_TYPE_ENUM.fromInt(msg.arg1), msg.arg2);
                    break;

                case ServiceCommon.MSG_WHAT_PLAYER:
                    if (msg.arg1 == ServiceCommon.MSG_PROGRESS_UPDATE) {
                        playerProgressUpdate(msg.arg2);
                    } else if (msg.arg1 == ServiceCommon.MSG_PROGRESS_COMPLETION) {
                        //playerComplete();

                        if (mStudyData.mIsOldAnswerExist) {//정답화면이 있는 구교재인경우
                            String answer = "";

                            StringBuffer buffer = new StringBuffer();

                            File file = new File(mStudyData.mOldAnswerFile);

                            try {
                                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "euc-kr"));

                                String str = reader.readLine();

                                while (str != null) {
                                    buffer.append(str + "\n");
                                    str = reader.readLine();//한 줄씩 읽어오기
                                }

                                answer = buffer.toString();
                                reader.close();

                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            if ("".equals(answer)) {//구교재 시리즈이지만 파일이 없는경우
                                playerComplete();
                            } else {
                                showAnswerPop(answer);
                            }

                        } else {//정답화면이 있는 구교재가 아닌경우
                            playerComplete();
                        }

                    } else if (msg.arg1 == ServiceCommon.MSG_SEEK_FWD_ABLE) {
                        if (mIsGtagActive)
                            return;

                        if (mVorbisPlayer.isPlaying() && !mSeekFwd.isEnabled()) {
                            if (mCurRepeatCount == 1) {
                                mSeekFwd.setEnabled(true);
                            }
                        }

                    } else if (msg.arg1 == ServiceCommon.MSG_SEEK_FWD_DISABLE) {
                        if (mIsGtagActive)
                            return;

                        if (mVorbisPlayer.isPlaying() && mSeekFwd.isEnabled())
                            mSeekFwd.setEnabled(false);
                    }
                    break;

                case ServiceCommon.MSG_WHAT_RECORDER:
                    /*Log.e("hy", "MSG_WHAT_RECORDER1" + msg.arg1);
                    if (msg.arg1 == ServiceCommon.MSG_REC_READY) {
                        Log.e("hy", "MSG_WHAT_RECORDER2");
                        recordNotiPlay();
                    }
                *//*else if (msg.arg1 == ServiceCommon.MSG_REC_WARNING)
                    StudyDataUtil.addRecordBookWarning(mContext);*//*
                    else if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END) {
                        Log.e("hy", "MSG_WHAT_RECORDER3");
                        if (!mIsG2tagRecordEnd) {
                            Log.e("hy", "MSG_WHAT_RECORDER4");
                            recordEnd();
                        }
                    }
                    break;*/
                    if (msg.arg1 == ServiceCommon.MSG_REC_READY) {
                        if (!(mIsPQtagActive || mIsPTtagActive || mIsPRtagActive)) {
                            ShowPopup popup = ShowPopup.getInstance();
                            popup.setCallBackListener(mImpl);
                            mShowPopup = popup.ShowLeaningRecord(mContext, " 녹음을 마치면 ", mCurTagEventType);
                        }

                        recordNotiPlay();
                    } else if (msg.arg1 == ServiceCommon.MSG_REC_WARNING)
                        StudyDataUtil.addRecordBookWarning(mContext);
                    else if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END) {
                        if (!mIsG2tagRecordEnd) {
                            recordEnd();
                        }
                    }
                    break;

                case ServiceCommon.MSG_WHAT_BOOK:
                    StudyDataUtil.requestStudyResultBookCheck(mContext);
                    break;

                case ServiceCommon.MSG_WHAT_BOOK_WARNING:
                    StudyDataUtil.checkBookWarning(mContext);
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_CONVERT:
                    Log.k("wusi12", "ServiceCommon.MSG_WHAT_CONVERT");
                    setStudyLayout();
                    setTagEventData(msg.obj);
                    mStudyData.mRecRTime = msg.arg2;//권장시간

                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        readyStudy();
                    } else {
                        showProgressDialog(true);
                        requestServerTimeSync(REQUEST_ID_TIME_SYNC_START);
                    }

                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "--- S11 -----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SmartStudyActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            showProgressDialog(false);
                            readyStudy();
                            Log.k("wusi12", "--------------------");
                        }
                    } else if (msg.arg1 == REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "-------S12-----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SmartStudyActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            goNextStatus();
                            Log.k("wusi12", "--------------------");
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == REQUEST_ID_TIME_SYNC_START) {
                        showProgressDialog(false);
                        readyStudy();
                    } else if (msg.arg1 == REQUEST_ID_TIME_SYNC_END) {
                        goNextStatus();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void startVideoPlay() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);
        playerPause();
        if (0 <= mCurV1TagIndex && 0 < mStudyData.mVideoFile.size()) {
            setVideoPlayer(mStudyData.mVideoFile.get(mCurV1TagIndex), mStudyData.mIsFastRewind);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
            findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);
            //findViewById(R.id.setp_statusbar).setVisibility(View.GONE);
            findViewById(R.id.title_view).setVisibility(View.GONE);
            findViewById(R.id.page_audio_study_layout).setVisibility(View.GONE);
            findViewById(R.id.animation_study_layout).setVisibility(View.VISIBLE);
        } else {
            playerResume();
        }
    }

    private void endVideoPlay() {
        ((LinearLayout) findViewById(R.id.animation_study_layout)).removeAllViewsInLayout();
        findViewById(R.id.animation_study_layout).setVisibility(View.GONE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.VISIBLE);
        //findViewById(R.id.setp_statusbar).setVisibility(View.VISIBLE);
        findViewById(R.id.title_view).setVisibility(View.VISIBLE);
        if (mIsPageStudy) {
            findViewById(R.id.page_audio_study_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.audio_study_ing_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.page_audio_study_layout).setVisibility(View.GONE);
        }

        playerResume();
    }

    private TagEventData getTagType(int time) {
        for (int i = 0; i < mTagEventList.size(); i++) {
            TagEventData te = mTagEventList.get(i);
            //String tagType = te.getEventType();
            if (te.getEventTime() > time){
                if(i== 0){
                    return null;
                } else {
                    return mTagEventList.get(i-1);
                }
            } else if (te.getEventTime() == time) {
                return mTagEventList.get(i);
            }
        }
        return null;
    }

    private int getTagIndex(int time) {
        for (int i = 0; i < mTagEventList.size(); i++) {
            TagEventData te = mTagEventList.get(i);
            // String tagType = te.getEventType();
            if (te.getEventTime() > time) {
                if (i == 0) {
                    return 0;
                } else {
                    return i - 1;
                }

            }
        }
        return 0;
    }

    /**
     * 진행 화면을 보여줌
     *
     * @param isShow : 보여줄지 여부
     */
    public void showProgressDialog(boolean isShow) {
        try {
            if (isShow) {
                mProgressDialog = LoadingDialog.show(this, R.string.string_common_loading, R.string.string_common_time_sync);
            } else {
                if (null != mProgressDialog && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (Exception e) {//예외처리
            e.printStackTrace();
            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
    }

    @Override
    public void OnSeekCompleteEvent(int time, int btnTag) {
        // TODO Auto-generated method stub
        TagEventData type = getTagType(time);
        if (type != null) {
            if (type.getEventType().equals(TagEventData.TAG_V1) || type.getEventType().equals(TagEventData.TAG_J1) || (mStudyData.mIsTextKorean && type.getEventType().equals(TagEventData.TAG_O1))) {
                mCurTagEventType = type.getEventType();
                mCurTagEventIndex = getTagIndex(time);
                mNextTagEventIndex = mCurTagEventIndex + 1;
                // startPopSong();
                mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_TAG_EVENT, type.checkEventType(), type.getEventTime()), 100);
                // mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
                // ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 100);
            }
            setPageIndex(type.getPageIndex());
        } else {

        }

        if (mIsShowRepeat) {
            Toast.makeText(this, getString(R.string.string_audio_study_repeat_warning_message), Toast.LENGTH_SHORT).show();
            showRepeatText(false);
            mIsRtagActive = false;
        }
    }

    /**
     * 현재 반복 횟수 및 총 반복 횟수 보여줌
     */
    public void showRepeatText(boolean isShow) {
        TextView repeatTextView = (TextView) findViewById(R.id.audio_study_repeat_text);
        TextView repeatCountView = (TextView) findViewById(R.id.audio_study_repeat_count);

        if (isShow) {
            mIsShowRepeat = true;
            findViewById(R.id.audio_study_repeat).setVisibility(View.VISIBLE);
            repeatTextView.setText(getString(R.string.string_audio_study_repeat_message));
            repeatCountView.setText(" " + mCurRepeatCount + " / " + mTotalRepeatCount);
        } else {
            mIsShowRepeat = false;
            repeatTextView.setText("");
            repeatCountView.setText("");
        }
    }

    private void setGtagVolume(boolean isStart) {
        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
        // int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int userVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
        int volume = 0;

        if (isStart) {
            volume = userVolume / 2;
        } else {
            volume = userVolume * 2;
        }

        if (volume > 0) {
            am.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
        }
    }

    // 신규문항

    /**
     * 웹뷰 사용을 위한 설정
     */

    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
    public void setupWebView() {
        // 웹뷰에서 자바스크립트실행가능 설정 셋팅
        mWebView.getSettings().setJavaScriptEnabled(true);
        // webview.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.addJavascriptInterface(new AndroidBridge(), "bridge");
        mWebView.getSettings().setDefaultTextEncodingName("utf-8");
        // webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setTextZoom(100);

        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                new AlertDialog.Builder(mContext).setTitle("AlertDialog").setMessage(message).setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                }).setCancelable(false).create().show();

                return true;
            }

            @Override
            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                final ImageButton close = new ImageButton(mContext);
                //close.setImageDrawable(mContext.getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cancel));
                FrameLayout.LayoutParams blp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.TOP);
                close.setLayoutParams(blp);
                close.setBackgroundColor(Color.TRANSPARENT);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onHideCustomView();
                    }
                });
                ((FrameLayout) view).addView(close);
                mCustomViewCallback = callback;
                mTargetView.addView(view);
                mCustomView = view;
                mTargetView.setVisibility(View.VISIBLE);
                mTargetView.bringToFront();
            }

            @Override
            public void onHideCustomView() {
                if (mCustomView == null)
                    return;

                mCustomView.setVisibility(View.GONE);
                mTargetView.removeView(mCustomView);
                mCustomView = null;
                mTargetView.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();
                mContentView.setVisibility(View.VISIBLE);
            }
        });
        /*mWebView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
				if (bottom > oldBottom) {

					mWebView.setScrollX(0);
					mWebView.setScrollY(0);
				}
			}
		});*/
        mWebView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }

    public void popupWebView(TAG_EVENT_TYPE_ENUM event) {
        setupWebView();
        String filename = "";

        mPopupStudyOrder = mCurQuizIndex;
        mResultComplete = true;

        mPlay.setEnabled(false);

        switch (event) {
            case TAG_EVENT_PQ0:
            case TAG_EVENT_PQ1:
                if (mCurQuizIndex < 10) {
                    filename = "quiz_0";
                } else {
                    filename = "quiz_";
                }
                filename += mCurQuizIndex;
                break;
            case TAG_EVENT_PA0:
            case TAG_EVENT_PA1:
                if (mCurQuizIndex < 10) {
                    filename = "activity_0";
                } else {
                    filename = "activity_";
                }
                filename += mCurQuizIndex;
                break;
            case TAG_EVENT_PI0:
            case TAG_EVENT_PI1:
                if (mCurQuizIndex < 10) {
                    filename = "info_0";
                } else {
                    filename = "info_";
                }
                filename += mCurQuizIndex;
                break;
            case TAG_EVENT_PT0:
            case TAG_EVENT_PT1:
                if (mCurQuizIndex < 10) {
                    filename = "touch&play_0";
                } else {
                    filename = "touch&play_";
                }
                filename += mCurQuizIndex;
                break;
            case TAG_EVENT_PR0:
            case TAG_EVENT_PR1:
                if (mCurQuizIndex < 10) {
                    filename = "roleplay_0";
                } else {
                    filename = "roleplay_";
                }
                filename += mCurQuizIndex;
                break;
            default:
                filename = "";
                break;
        }

        if (!filename.equals("")) {
            /**
             * 인터페이스디면 지우기
             */
            mQuiz_Check = false;//숲용인지 체크
            String[] quizList = mStudyData.mPopupQuizList;

            File f = new File(mStudyData.mPopupStudyPath + filename + ".html");
            String strHtml = IOUtils.getHTML(f);

            if (f.length() > 0 && quizList != null) {
                for (String quizNum : quizList) {
                    if (strHtml.contains(quizNum) || mStudyData.mProductType != 0 || ServiceCommon.CONTENT_MODE == ServiceCommon.TEST_CONTENT) {//숲에서 안보여야하는거 제외
                        guidePlay(R.raw.p_signal);
                        mWebView.loadUrl("file://" + mStudyData.mPopupStudyPath + filename + ".html?basedir=" + mStudyData.mPopupStudyPath);
                        Runnable runnable = null;
                        runnable = new Runnable() {
                            public void run() {
                                mWebView.setVisibility(View.VISIBLE);
                            }
                        };
                        mWebView.postDelayed(runnable, 1000);
                        mQuiz_Check = true;
                        break;
                    }
                }
                if (!mQuiz_Check) {
                    mIsPQtagActive = false;
                    mIsPTtagActive = false;
                    mIsPRtagActive = false;
                    playerResume();
                }
            } else {
                mIsPQtagActive = false;
                mIsPTtagActive = false;
                mIsPRtagActive = false;
                playerResume();
            }
        }

    }

    public void play() throws IOException {

        mIsForceStop = false;

        mPlayThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (!mPlayThread.isInterrupted()) {
                    decodeLoop();
                }
            }
        });

        mPlayThread.start();
    }

    private void decodeLoop() {
        //setting
        ByteBuffer[] codecInputBuffers;
        ByteBuffer[] codecOutputBuffers;

        mExtractor = new MediaExtractor();
        try {
            mExtractor.setDataSource(mMediaPath);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        MediaFormat format;
        String mime;
        try {
            format = mExtractor.getTrackFormat(0);
            mime = format.getString(MediaFormat.KEY_MIME);
            mMediaCodec = MediaCodec.createDecoderByType(mime);
        } catch (IOException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            return;
        }
        mMediaCodec.configure(format, null, null, 0);
        mMediaCodec.start();
        codecInputBuffers = mMediaCodec.getInputBuffers();
        codecOutputBuffers = mMediaCodec.getOutputBuffers();

        int sampleRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);

        Log.i("jyp", "mime " + mime);
        Log.i("jyp", "sampleRate " + sampleRate);

        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT), AudioTrack.MODE_STREAM);

        mBaserate = mAudioTrack.getPlaybackRate();

        Log.d("jyp", Integer.toString(mAudioTrack.getPlaybackRate()));
        Log.d("jyp", Integer.toString(mBaserate));
        mAudioTrack.setPlaybackRate((int) (mBaserate * mSpeed));

        mAudioTrack.play();
        mExtractor.selectTrack(0);

        final long kTimeOutUs = 10000;
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        boolean sawInputEOS = false;
        int noOutputCounter = 0;
        int noOutputCounterLimit = 50;

        //��� �÷��� ����
        while (!sawInputEOS && noOutputCounter < noOutputCounterLimit && !mIsForceStop) {
            if (!sawInputEOS) //������ ��������
            {
                if (mChangeSpd) {
                    mAudioTrack.setPlaybackRate((int) (mBaserate * mSpeed));
                    Log.d("jyp", Integer.toString(mAudioTrack.getPlaybackRate()));

                    mChangeSpd = !mChangeSpd;
                }
                if (mIsPause) //pause����
                {

                    continue;
                }

                noOutputCounter++;

                mInputBufIndex = mMediaCodec.dequeueInputBuffer(kTimeOutUs);
                if (mInputBufIndex >= 0) {
                    ByteBuffer dstBuf = codecInputBuffers[mInputBufIndex];

                    int sampleSize = mExtractor.readSampleData(dstBuf, 0);

                    long presentationTimeUs = 0;

                    if (sampleSize < 0) //��
                    {
                        Log.d("jyp", "saw input EOS.");
                        sawInputEOS = true;
                        sampleSize = 0;
                    } else //���
                    {
                        presentationTimeUs = mExtractor.getSampleTime();
                        //�������
                        //Log.d("jyp", "presentaionTime = " + (int) (presentationTimeUs / 1000 / 1000));
                    }

                    mMediaCodec.queueInputBuffer(mInputBufIndex, 0, sampleSize, presentationTimeUs, sawInputEOS ? MediaCodec.BUFFER_FLAG_END_OF_STREAM : 0);

                    if (!sawInputEOS) {
                        mExtractor.advance();
                    }
                } else {
                    Log.e("jyp", "inputBufIndex " + mInputBufIndex);
                }
            }
            try {
                int res = mMediaCodec.dequeueOutputBuffer(info, kTimeOutUs);

                if (res >= 0) {

                    if (info.size > 0) {
                        noOutputCounter = 0;
                    }

                    int outputBufIndex = res;
                    ByteBuffer buf = codecOutputBuffers[outputBufIndex];

                    final byte[] chunk = new byte[info.size];
                    buf.get(chunk);
                    buf.clear();
                    if (chunk.length > 0) {
                        mAudioTrack.write(chunk, 0, chunk.length);
                    }
                    mMediaCodec.releaseOutputBuffer(outputBufIndex, false);

                } else if (res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    codecOutputBuffers = mMediaCodec.getOutputBuffers();
                    Log.d("jyp", "output buffers have changed.");
                } else if (res == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    MediaFormat oformat = mMediaCodec.getOutputFormat();
                    Log.d("jyp", "output format has changed to " + oformat);
                } else {
                    Log.d("jyp", "dequeueOutputBuffer returned " + res);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Crashlytics.logException(e);
            }
        }

        Log.d("jyp", "the end...");
        mIsForceStop = true;

        if (noOutputCounter < noOutputCounterLimit) {
            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_AUDIO_PLAYBACK_RATE, ServiceCommon.MSG_AUDIO_PR_END, 0));
        }

        releaseResources(true);
    }

    private void releaseResources(Boolean release) {
        Log.d("jyp", "releaseResoutces");
        if (mPlayThread != null) {
            mPlayThread.interrupt();
        }

        if (mExtractor != null) {
            mExtractor.release();
            mExtractor = null;
        }

        if (mMediaCodec != null) {
            if (release) {
                mMediaCodec.stop();
                mMediaCodec.release();
                mMediaCodec = null;
            }
        }

        if (mAudioTrack != null) {
            mAudioTrack.release();
            mAudioTrack = null;
        }
    }

    private final Handler mWebViewHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    Log.e("hy", "MSG_WHAT_STUDY");
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_QUIZ_COMPLETION) {
                        Log.e("hy", "MSG_STUDY_QUIZ_COMPLETION");
                        mIsPQtagActive = false;
                        mIsPTtagActive = false;
                        mIsPRtagActive = false;
                        mPopupStudyResult = (ExamResult) msg.obj;
                        mPopupStudyResult.mStudyOrder = mPopupStudyOrder;
                        if (mPopupRecordResult.size() > 0) {
                            StudyDataUtil.addExamResult(mPopupRecordResult, mContext, mHandler);
                            mPopupRecordResult.clear();
                        } else {
                            StudyDataUtil.addExamResult(mPopupStudyResult, mContext, mHandler);
                        }
                        mPopupStudyResult = null;
                        closeWebView();
                        playerResume();

                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_TOUCHPLAY_COMPLETION) {
                        Log.e("hy", "MSG_STUDY_TOUCHPLAY_COMPLETION");
                        mIsPQtagActive = false;
                        mIsPTtagActive = false;
                        mIsPRtagActive = false;
                        if (mPopupRecordResult.size() > 0) {
                            StudyDataUtil.addExamResult(mPopupRecordResult, mContext, mHandler);
                            mPopupRecordResult.clear();
                        } else {
                            mPopupStudyResult = (ExamResult) msg.obj;
                            mPopupStudyResult.mStudyOrder = mPopupStudyOrder;
                            StudyDataUtil.addExamResult(mPopupStudyResult, mContext, mHandler);
                            mPopupStudyResult = null;
                        }
                        closeWebView();
                        playerResume();

                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_IDEAMAP_COMPLETION) {
                        Log.e("hy", "MSG_STUDY_IDEAMAP_COMPLETION");
                        mIsPQtagActive = false;
                        mIsPTtagActive = false;
                        mIsPRtagActive = false;
                        if (mPopupRecordResult.size() > 0) {
                            mPopupStudyResult = mPopupRecordResult.get(0);
                            mPopupStudyResult.mCustomerAnswer = ((ExamResult) msg.obj).mCustomerAnswer;
                            StudyDataUtil.addExamResult(mPopupStudyResult, mContext, mHandler);
                            mPopupRecordResult.clear();
                            mPopupStudyResult = null;
                        } else {
                            mPopupStudyResult = (ExamResult) msg.obj;
                            mPopupStudyResult.mStudyOrder = mPopupStudyOrder;
                            StudyDataUtil.addExamResult(mPopupStudyResult, mContext, mHandler);
                            mPopupStudyResult = null;
                        }
                        closeWebView();
                        playerResume();

                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_ROLEPLAY_COMPLETION) {
                        Log.e("hy", "MSG_STUDY_ROLEPLAY_COMPLETION");
                        mIsPQtagActive = false;
                        mIsPTtagActive = false;
                        mIsPRtagActive = false;
                        addRolePlayResult((ExamResult) msg.obj);
                        StudyDataUtil.addExamResult(mPopupRecordResult, mContext, mHandler);
                        mPopupRecordResult.clear();
                        closeWebView();
                        playerResume();

                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_ROLEPLAY_UPDATE) {
                        Log.e("hy", "MSG_STUDY_ROLEPLAY_UPDATE");
                        addRolePlayResult((ExamResult) msg.obj);
                        StudyDataUtil.addExamResult(mPopupRecordResult, mContext, mHandler);
                        mPopupRecordResult.clear();

                    }
                    break;
                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (msg.arg1 == ServiceCommon.MSG_REC_START) {
                        Log.e("hy", "MSG_WHAT_RECORDER");
                        mPopupStudyResult = (ExamResult) msg.obj;
                        mPopupStudyResult.mStudyOrder = mPopupStudyOrder;
                        mPopupRecordResult.add(mPopupStudyResult);
                        recordReady(true, mPopupStudyResult.mMaxRecTime * 2);
                    } else if (msg.arg1 == ServiceCommon.MSG_REC_END) {
                        Log.e("hy", "MSG_REC_END");
                        recordEnd();
                        mPopupStudyResult = null;
                        mWebView.loadUrl("javascript:fileNameFromApp('" + mRecordPath + "')");
                        mWebView.loadUrl("javascript:recordFromApp()");
                    } else if (msg.arg1 == ServiceCommon.MSG_REC_PLAY) {
                        recordPlayWebView();
                    } else if (msg.arg1 == ServiceCommon.MSG_REC_PLAY_END) {
                        recordPlayEndWebView();
                    }
                    break;
                case ServiceCommon.MSG_WHAT_AUDIO_PLAYBACK_RATE:
                    if (msg.arg1 == ServiceCommon.MSG_AUDIO_PR_START) {
                        mMediaPath = mStudyData.mPopupStudyPath + ((AudioPlaybackRate) msg.obj).mFilepath;
                        Log.e("hy", "hy MSG_AUDIO_PR_START mSpeed = " + mSpeed);

                        try {
                            play();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else if (msg.arg1 == ServiceCommon.MSG_AUDIO_PR_STOP) {
                        mIsForceStop = true;
                    } else if (msg.arg1 == ServiceCommon.MSG_AUDIO_PR_CHANGE) {
                        mChangeSpd = true;
                        mSpeed = ((AudioPlaybackRate) msg.obj).mSpeed;
                        Log.e("ss99km01", "ss99km01 MSG_AUDIO_PR_CHANGE mSpeed = " + mSpeed);
                    } else if (msg.arg1 == ServiceCommon.MSG_AUDIO_PR_PAUSE) {
                        if (!mIsPause)
                            mIsPause = true;
                        else
                            mIsPause = false;
                    } else if (msg.arg1 == ServiceCommon.MSG_AUDIO_PR_END) {
                        mWebView.loadUrl("javascript:endSpdPlayer(true)");
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * Role & Play 학습 후 선택된 역활 정보 업데이트
     *
     * @param result : 학습 결과
     */
    private void addRolePlayResult(ExamResult result) {
        for (int i = 0; i < mPopupRecordResult.size(); i++) {
            mPopupRecordResult.get(i).mCustomerAnswer = result.mCustomerAnswer;
        }
    }

    public void closeWebView() {
        mWebView.clearHistory();
        mWebView.clearCache(true);
        mWebView.clearView();
        mWebView.setVisibility(View.GONE);
    }

    private void callHiddenWebViewMethod(String name) {

        if (mWebView != null) {
            try {
                Method method = WebView.class.getMethod(name);
                method.invoke(mWebView);
            } catch (NoSuchMethodException e) {
                // TODO Auto-generated catch block
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
            } catch (InvocationTargetException e) {
                // TODO Auto-generated catch block
            } catch (Exception e) {
            }
        }
    }

    /**
     * @author jyp nexmore AndroidBridge
     * @JavascriptInterface code : 문항코드 type : 문제유형 - 1 : pop(선택형) - 2 :
     * pop(drag&drop) - 3 : pop(집중형) - 4 : Touch&Play - 5 :
     * Role&Play - 6 : pop(OX) answer : 문항의 정답 ( 숫자 또는 O X
     * ) userChk : 사용자 선택 값 ( ex : 3 or 1-2,2-4,3-1 )
     * playSeq,recordSeq : 녹음 문장번호|파일명 (ex :
     * W0004|test2.mp4,W0002|test.mp4, ) f_role : 선택한 캐릭터
     * (ex : 2 ) isEnded : 완전히 끝났는지에 대한 여부 String(true,
     * false)
     */
    // receive data
    private class AndroidBridge {

        // debug log or message
        @JavascriptInterface
        public void setMessage(final String fromMsg) {
            Log.e("hy", "setMessage" + fromMsg);
            // mWebViewHandler.post(new Runnable() {
            // public void run() {
            // Log.d("receive from html", fromMsg);
            // // btn2.performClick();|
            // }
            // });
        }

        // popquiz 결과
        // @JavascriptInterface target API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void sendQuizResult(final String q_arg1, final String q_arg2, final String q_arg3, final String q_arg4) {

            ExamResult result = new ExamResult();
            result.mQuestionNo = q_arg1;
            result.mTagType = Integer.valueOf(q_arg2);
            result.mIsCorrect = q_arg3.replaceAll(", ", ",").equals(q_arg4) ? 1 : 0;
            result.mCustomerAnswer = q_arg4;
            Log.e("hy", "sendQuizResult" + result.toString());
            if (mResultComplete) {
                mResultComplete = false;
                mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_QUIZ_COMPLETION, 0, result));
            }
        }

        // role&play 결과
        // @JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void sendRolePlayResult(final String rp_arg1, final String rp_arg2, final String rp_arg3, final String rp_arg4, final String rp_arg5) {
            ExamResult result = new ExamResult();
            result.mQuestionNo = rp_arg1;
            result.mTagType = Integer.valueOf(rp_arg2);
            result.mCustomerAnswer = rp_arg4;
            Log.e("hy", "sendRolePlayResult" + result.toString());
            if (rp_arg5.equals("true")) {
                mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_ROLEPLAY_COMPLETION, 0, result));
            } else {
                mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_ROLEPLAY_UPDATE, 0, result));
            }
        }

        // touch&play 결과
        // @JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void sendTouchAndPlayResult(final String tp_arg1, final String tp_arg2, final String tp_arg3) {
            ExamResult result = new ExamResult();
            result.mQuestionNo = tp_arg1;
            result.mTagType = Integer.valueOf(tp_arg2);
            // result.mQuestionOrder = Integer.valueOf(tp_arg3);
            Log.e("hy", "sendTouchAndPlayResult" + result.toString());

            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_TOUCHPLAY_COMPLETION, 0, result));
        }

        // ideamap 결과
        // @JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void sendTouchAndPlayResult(final String tp_arg1, final String tp_arg2, final String tp_arg3, final String tp_arg4) {
            ExamResult result = new ExamResult();
            result.mQuestionNo = tp_arg1;
            result.mTagType = Integer.valueOf(tp_arg2);
            result.mCustomerAnswer = tp_arg4;
            // result.mQuestionOrder = Integer.valueOf(tp_arg3);
            Log.e("hy", "sendTouchAndPlayResult" + result.toString());

            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_IDEAMAP_COMPLETION, 0, result));
        }

        // @JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void setTestMsg(final String msgs) {

            Log.e("hy", "setTestMsg+" + msgs);
        }

        @JavascriptInterface
        public void startRecordPlay(String filepath) {
            mWebViewRecordPath = filepath;
            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_PLAY, 0));
        }

        @JavascriptInterface
        public void stopRecordPlay() {
            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_PLAY_END, 0));
        }

        // 단말 녹음 중지
        // @JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void stopDeviceRecord() {
            Log.e("hy", "stopDeviceRecord1+");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.e("hy", "stopDeviceRecord2+");
            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_END, 0));
        }

        // 단말 녹음 시작
        // @JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void startDeviceRecord(final String fileName, final String studyType, final String studyCode) {
            Log.e("hy", "startDeviceRecord+");
            int maxRecTime = -1;
            int questionOrder = -1;
            int DEFAULT_REC_TIME = 20;
            int index1 = fileName.indexOf("path");
            int index2 = fileName.indexOf(".");

            if (fileName.contains("null")) {
                index1 = fileName.indexOf("_");

                maxRecTime = DEFAULT_REC_TIME;
                questionOrder = Integer.valueOf(fileName.substring(index1 + 1, index1 + 2));
            } else {
                mMediaPlayer = new MediaPlayer();

                if (mMediaPlayer != null) {
                    String filepath = mStudyData.mPopupStudyPath + "audio/" + fileName;
                    try {
                        mMediaPlayer.reset();
                        mMediaPlayer.setDataSource(filepath);
                        mMediaPlayer.prepare();
                        maxRecTime = mMediaPlayer.getDuration() * 2;
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        mMediaPlayer.release();
                        mMediaPlayer = null;
                    }
                }
                questionOrder = Integer.valueOf(fileName.substring(index1 + 4, index2));
            }

            ExamResult result = new ExamResult();
            result.mTagType = Integer.valueOf(studyType);
            result.mQuestionNo = studyCode;
            result.mQuestionOrder = questionOrder;
            result.mMaxRecTime = maxRecTime;

            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_START, 0, result));
        }

        // 오디오 실행 - 실행할 파일 패스와 배속 속도를 넘겨주면 단말에서 실행한다
        // 배속값은 0.5(최소), 0.6, 0.7, 0.8, 0.9, 1.0(기본), 1.2, 1.4, 1.6, 1.8, 2.0(최대)
//		@JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void startSpdPlayer(String filepath, String spd) {
            Log.e("hy", "startSpdPlayer+");
            AudioPlaybackRate apr = new AudioPlaybackRate();
            apr.mFilepath = filepath;
            apr.mSpeed = Float.parseFloat(spd);

            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_AUDIO_PLAYBACK_RATE, ServiceCommon.MSG_AUDIO_PR_START, 0, apr));
        }

        // 웹뷰를 닫거나 플레이를 멈출때 사용
//		@JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void stopSpdPlayer() {
            Log.e("hy", "stopSpdPlayer+");
            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_AUDIO_PLAYBACK_RATE, ServiceCommon.MSG_AUDIO_PR_STOP, 0));
        }

        // 음원 재생중 배속값이 변경될 경우 변경된 값을 보낸다. 단말은 변경된 값으로 변경하여 배속재생한다.
        // 배속값은 0.5(최소), 0.6, 0.7, 0.8, 0.9, 1.0(기본), 1.2, 1.4, 1.6, 1.8, 2.0(최대)
//		@JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void changeSpdPlayer(String spd) {
            Log.e("hy", "changeSpdPlayer+");
            AudioPlaybackRate apr = new AudioPlaybackRate();
            apr.mSpeed = Float.parseFloat(spd);

            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_AUDIO_PLAYBACK_RATE, ServiceCommon.MSG_AUDIO_PR_CHANGE, 0, apr));
        }

        // 플레이를 일시정지할때 사용
//		@JavascriptInterface API Level 17이상으로 변경시 필요
        @JavascriptInterface
        public void pauseSpdPlayer() {
            Log.e("hy", "pauseSpdPlayer+");
            mWebViewHandler.sendMessage(mWebViewHandler.obtainMessage(ServiceCommon.MSG_WHAT_AUDIO_PLAYBACK_RATE, ServiceCommon.MSG_AUDIO_PR_PAUSE, 0));
        }

    }

    /*
     * private String getTagTime(TagEventData te) { int tagTime =
     * te.getEventTime() / 1000;
     *
     * String tagTimeStr = String.format("00%02d%02d", tagTime / 60, tagTime %
     * 60);
     *
     * return tagTimeStr; }
     */

    private void mergeFiles() {
        DatabaseUtil databaseUtil = DatabaseUtil.getInstance();
        ArrayList<Integer> timeList = databaseUtil.selectMergStartTime();
        if (timeList.size() != 0) {//머지할께 하나라도 있을때
            for (int i = 0; i < timeList.size(); i++) {//하나의 머지값들
                ArrayList<RecData> recList = databaseUtil.selectMergRecList(timeList.get(i));
                try {
                    //						CommonUtil.meargeMp3(recList);
                    CommonUtil.mergeMp3Files(recList);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (timeList.get(i) != 0)
                    databaseUtil.deleteMergRecList(timeList.get(i));
            }

        }

        ArrayList<RecData> recList = databaseUtil.selectRecListAll();

        for (RecData data : recList) {
            HttpMultiPart httpMultiPart = new HttpMultiPart();

            if (CommonUtil.isCenter()) {
                data.url = StudyDataUtil.addPackageName("QXNwX0xfU1RVRFlfUkVDT1JEX0RUTF9G");
            } else {
                data.url = StudyDataUtil.addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9SRUNPUkRfRFRM");
            }
            httpMultiPart.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);
        }

    }

    /**
     * 모든 ACTIVITY를 종료 시키는 이벤트 수신
     */
    protected void setNotiReceiver() {
        // 이벤트 Receiver 생성
        IntentFilter iFilter = new IntentFilter(ServiceCommon.ACTION_NOTI_FOUCS);
        iFilter.addAction(ServiceCommon.ACTION_NOTI_INIT);//초기화 필터 추가

        registerReceiver(mNotiEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, final Intent intent) {

                String action = intent.getAction();
                if (action.equalsIgnoreCase(ServiceCommon.ACTION_NOTI_FOUCS)) {
                    ani_box = new AnimationBox(mContext, R.anim.ani_foucs);
                    ani_box.setConfirmText(R.string.string_common_confirm);

                    if (!ani_box.isShowing()) {

                        ani_box.show();
                        guidePlay(R.raw.p_focus);
                    }
                    if (null != mVorbisPlayer && mVorbisPlayer.isPlaying()) {//재생중이라면
                        playerPause();
                    }
                } else if (action.equalsIgnoreCase(ServiceCommon.ACTION_NOTI_INIT)) {
                    finish();
                }
            }
        }, iFilter);
    }

    /**
     * 구교재 정답 팝업 노출
     *
     * @param txt
     */
    private void showAnswerPop(String txt) {

        ScrollBox mMsgBox = new ScrollBox(this, getString(R.string.string_answer_check), txt);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setOnDialogDismissListener(new OnDialogDismissListener() {

            @Override
            public void onDialogDismiss(int result, int dialogId) {
                // TODO Auto-generated method stub
                playerComplete();
            }
        });
        mMsgBox.setMessage(txt);
        mMsgBox.show();
    }

    /**
     * slog가 안될경우 체크 시작 5초뒤 한번
     */
    private void slogCheck() {
        //SLOG 서비스가 안돌때가 있어서..
        timerTask = new TimerTask() {
            public void run() {

                if (!CommonUtil.isServiceRunningCheck(mContext, SlogService.class.getName())) {
                    try {
                        Intent intent = new Intent(mContext, SlogService.class);
                        mContext.startService(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                }
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, 5000);

    }
}
