
package com.yoons.fsb.student.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.KeyEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.OneWeekData;
import com.yoons.fsb.student.data.StudyData.SentenceQuestion;
import com.yoons.fsb.student.data.StudyData.VanishingQuestion;
import com.yoons.fsb.student.data.StudyData.WordQuestion;
import com.yoons.fsb.student.data.TagEventData;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.db.DatabaseData.DownloadStudyUnit;
import com.yoons.fsb.student.db.DatabaseData.StudyOption;
import com.yoons.fsb.student.db.DatabaseData.StudyUnit;
import com.yoons.fsb.student.db.DatabaseData.TodayStudyUnit;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.network.RequestQTETPhpPost;
import com.yoons.fsb.student.network.RequestQUSTPhpPost;
import com.yoons.fsb.student.service.AgentEvent;
import com.yoons.fsb.student.service.DownloadService;
import com.yoons.fsb.student.service.DownloadServiceAgent;
import com.yoons.fsb.student.ui.adapter.StudyBookSelectListAdapter;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.control.TitleView;
import com.yoons.fsb.student.ui.data.StudyBookSelectListItem;
import com.yoons.fsb.student.ui.data.StudyBookSelectListItem.ItemInfo;
import com.yoons.fsb.student.ui.data.StudyBookSelectListItem.OnBookItemClickListener;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.sec.SecIntroActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ContentsUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.util.YdaConverter;
import com.yoons.fsb.student.vanishing.VanishingTitlePaperActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * 교재 선택 화면
 *
 * @author ejlee
 */
public class StudyBookSelectActivity extends BaseActivity implements OnBookItemClickListener, OnDialogDismissListener {
    private static final String TAG = "[StudyBookSelectActivity]";

    private static final int MSGBOX_ID_STOPPED_STUDY_NOTI = 1; // 중단학습 메시지 박스 id
    private static final int MSGBOX_ID_UPDOWN_REFRESH = 2; // 새로 고침 메시지 박스 id
    private static final int MSGBOX_ID_UPDOWN_COMPLETE = 3; // 새로 고침 성공 메시지 박스 id
    private static final int MSGBOX_ID_BOOK_SELECT = 4; // 새로 고침 성공 메시지 박스 id
    private static final int MSGBOX_ID_UPDATE = 7;
    private static final int MSGBOX_ID_CONTENT_ERROR = 8;
    private static final int MSGBOX_ID_SEC_ERROR = 9;
    private static final int REQUEST_ID_VERSION = 104;

    public TitleView tView;

    private ListView mBookListView = null;
    private LinearLayout mStudyBookLayout = null; // 학습이 있을때 보여주는 화면
    private LinearLayout mNoStudyBookLayout = null; // 학습이 없을때 보여주는 화면
    private LinearLayout mSecondPeriodLayout = null; // 2차시
    private TextView mNoStudyBookTextView = null; // 학습이 없을때 문구
    private StudyBookSelectListAdapter mListAdapter = null;
    private ArrayList<StudyBookSelectListItem.ItemInfo> mInfoList = new ArrayList<StudyBookSelectListItem.ItemInfo>();
    private DatabaseUtil mDatabaseUtil;
    private ArrayList<TodayStudyUnit> mStudyUnit = null; // 오늘 학습 교재 정보
    private boolean mIsStudyStopped = false; // 중단된 학습 인지 여부
    private int mStudyStoppedIndex = 0; // 중단된 학습 index
    private LoadingDialog mProgressDialog = null;
    private YdaConverter mYdaConvert = null; // yda->ogg converter
    private DownloadServiceAgent mServiceAgent = null; // 다운로드 서비스 agent
    private int mDownloadingUnitPos = -1; // 다운로드 진행중인 교재 position
    private int mUnitDownloadedCnt = 0; // 다운로드 받는 count
    private int mUnitTotalCnt = 0; // 다운로드 받아야할 총 count
    private boolean mIsUnitDownload = false; // 교재 다운로드 중인지 여부
    private int mSelectedIndex = 0; // 현재 select된 index(블루투스 키보드에서 사용)
    private LinearLayout refresh_layout2 = null;

    private static final int REQUESTCODE_UPLOAD = 128; // upload requestcode
    private boolean isStudyEnd = false;
    private RequestPhpPost reqQinf;

    private ImageView img_refresh = null;

    //2교시
    private Map<String, String> qGetMap = null;
    private TextView txt_month, txt_total_cnt, txt_cnt = null; // 학습이 없을때 문구

    //일주간평가
    private ArrayList<OneWeekData> tempOneWeekData = null;

    private TextView txt_frist_preiod, txt_second_preiod;
    private RequestQTETPhpPost reqQTET;
    private TextView txt_ok = null;
    private int sec_cur_cnt = 0, sec_max_cnt = 0;//2교시 진행 카운트

    /**
     * 다운로드 서비스 connection
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DownloadService.LocalBinder binder = (DownloadService.LocalBinder) service;
            mServiceAgent = binder.getServiceHandler();
            Log.i(TAG, "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceAgent = null;
        }
    };

    /**
     * onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_book_select);
        initLayout();
        mDatabaseUtil = DatabaseUtil.getInstance(this);
        registerReceiver(mDownloadReceiver, new IntentFilter(AgentEvent.ACTION_RECEIVE_AGENT_EVENT));

        //keepScreen(false);

        overridePendingTransition(0, 0);

        // WiFi 감도 아이콘 설정
        setPreferencesCallback();
        Crashlytics.log(getString(R.string.string_ga_StudyBookSelectActivity));
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        //갱신 골라내기
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("REFRESH")) {
            isStudyEnd = true;
        }
        //
        if (isStudyEnd) {
            downloadList();
        } else {
            startStudyBookSelect();
        }
        /* checkAppUpdate(); */
    }

    private void downloadList() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            if (DatabaseUtil.getInstance(mContext).isFailStudyLogCnt()) {
                HttpJSONRequest request = new HttpJSONRequest(mContext);
                request.requestBackupStudyStatus(mBackupHandler);
            } else {
                if (mProgressDialog == null) {
                    mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_book_download_progress1);
                }
                DatabaseSync databaseSync = new DatabaseSync(this);
                databaseSync.downloadList(mHandler, Preferences.getCustomerNo(this));
            }
        } else {
            showDownloadNetworkErrorPopup();
        }
    }

    /**
     * 화면 초기 작업
     */
    private void changeLayout() {

        if (Preferences.getSyncSuccess(this)) {
            mNoStudyBookTextView.setText(R.string.string_no_study_book_message);
        } else {
            mNoStudyBookTextView.setText(R.string.string_no_study_book_message1);
        }
        makeTodayStudyLIst();
        sendQGet();
    }

    /**
     * 클릭 이벤트
     */
    @Override
    public void onClick(View view) {

        if (mIsUnitDownload) {
            return;
        }
        if (mProgressDialog != null) {
            return;
        }
        switch (view.getId()) {

            case R.id.refresh_imageview:
            case R.id.refresh_layout2:
                if (singleProcessChecker())
                    return;
                if (!mIsUnitDownload) {
                    downloadList();
                    /*
                     * if (!mIsUnitDownload)
                     * mHandler.sendEmptyMessage(ServiceCommon.
                     * MSG_RECORDING_FILE_UPLOAD );
                     */
                }

                break;
            case R.id.txt_frist_preiod:
                viewList();
                break;
            case R.id.txt_second_preiod:
                viewSec();
                break;
            case R.id.txt_ok:
                //Log.e("hy", "2교시시작");
                startSecStudy();

                break;
        }

        super.onClick(view);
    }

    /**
     * 교재 선택 이벤트
     */
    @Override
    public void onItemClick(View v) {
        if (singleProcessChecker())
            return;

        int position = mBookListView.getPositionForView(v);
        StudyBookSelectListItem.ItemInfo info = mInfoList.get(position);
        if (info.mIsBlackDimmed || info.mIsDownloading) {
            return;
        }

        if (mInfoList.size() == 1) {
            mSelectedIndex = position;
            showListProgressDialog();
            syncDownloadDatabaseTable();
        } else {
            mSelectedIndex = position;
            showBookSelectDialog(info.mBookName);
        }

        /*
         * if(info.mIsDownloadComplete) { makeStudyData(position); } else {
         * if(CommonUtil.isAvailableNetwork(mContext, true)) {
         * startContentsDownload(position); } else {
         * showDownloadNetworkErrorPopup(); } }
         */
    }

    private void showBookSelectDialog(String bookName) {
        StringBuffer buffer = new StringBuffer();
        buffer = buffer.append(bookName).append("\n").append("학습을 시작하시겠습니까?");

        mMsgBox = new MessageBox(this, 0, buffer.toString());
        mMsgBox.setConfirmText(R.string.string_common_start);
        mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setId(MSGBOX_ID_BOOK_SELECT);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 선택된 교재 다운로드 할지 확인
     *
     * @param position : 선택된 교재 position
     */
    private void startContentsDownload(int position) {
//        try {
//            mIsUnitDownload = true;
//            StudyBookSelectListItem.ItemInfo info = mInfoList.get(position);
//            TodayStudyUnit unit = mDatabaseUtil.selectNewTodayStudyUnit(Preferences.getCustomerNo(mContext), info.mProductNo, info.mStudyUnitCode);
//            int fileCnt[] = new int[2];
//            int reviewFileCnt[] = new int[2];
//            int percent = ContentsUtil.getUnitContentDownlaodPercent(mContext, unit.mProductNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, unit.mStudyUnitCode, fileCnt, s);
//            int reviewPercent = 100;
//            StudyUnit review = mDatabaseUtil.selectRecentNoReviewUnit(Preferences.getCustomerNo(mContext));
//            //StudyUnit review = StudyData.getmStudyUnit().get(position);
//
//            //구교재정보
//            unit.mIsOlndAnswer = isOldAnswer(unit.mSeriesNo);
//
//            if (review.mProductNo != 0 && !unit.mReviewStudyStatus.equals("RFN")) {
//                reviewPercent = ContentsUtil.getUnitContentDownlaodPercent(mContext, review.mProductNo, review.mSeriesNo, review.mBookNo, review.mStudyUnitNo, review.mStudyUnitCode, reviewFileCnt, true);
//            }
//
//            if ((percent == 100) && reviewPercent == 100) { // 백그라운드에서 완료가 됐을 수도 있으므로.
//                makeStudyData(position);
//                return;
//            }
//
//            // 다운로드가 완료된 차시일지라도 이전학습 복습을 위해 위의 사항을 체크해야함.
//            /*if (!CommonUtil.isAvailableNetwork(mContext, true)) {
//                ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT_AVAILABLE_NETWORKS");
//				showDownloadNetworkErrorPopup();
//				return;
//			}*/
//
//            mUnitTotalCnt = fileCnt[0] + reviewFileCnt[0];
//            mUnitDownloadedCnt = fileCnt[1] + reviewFileCnt[1];
//            setDownloadItemInfo(position);
//            mDatabaseUtil.updateDownloadContentNoPriority();
//
//            //일일/주간평가
//
//            if (tempOneWeekData != null) {
//                int i = 1;//순서 데이터따로없음
//                for (OneWeekData owData : tempOneWeekData) {
//                    if (owData.question_type.equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_2)) {
//                        owData.audio_path = ContentsUtil.getOneWeekFilePath(unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, i, true);
//                        owData.audio_path2 = ContentsUtil.getOneWeekFilePath(unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, i, false);
//                    }
//                    i++;
//                }
//            }
//
//            if (unit.mProductNo != 0) {
//
//                DownloadStudyUnit downloadUnit = new DownloadStudyUnit();
//                downloadUnit.mProductNo = unit.mProductNo;
//                downloadUnit.mSeriesNo = unit.mSeriesNo;
//                downloadUnit.mBookNo = unit.mBookNo;
//                downloadUnit.mStudyUnitNo = unit.mStudyUnitNo;
//                downloadUnit.mStudyUnitCode = unit.mStudyUnitCode;
//                downloadUnit.mDictationCnt = mDatabaseUtil.selectDictationCount(unit.mProductNo, unit.mStudyUnitCode);
//                downloadUnit.mIsCdnBFile = unit.mIsCdnBFile;
//                downloadUnit.mWordQuestionList = mDatabaseUtil.selectWordQuestionWordList(unit.mProductNo, unit.mStudyUnitCode);
//                downloadUnit.mSentenceQuestionCnt = mDatabaseUtil.selectSentenceQuestionCount(unit.mProductNo, unit.mStudyUnitCode);
//                downloadUnit.mVanishingQuestionCnt = mDatabaseUtil.selectVanishingQuestionCount(unit.mProductNo, unit.mStudyUnitCode);
//                downloadUnit.mAniTalkCnt = mDatabaseUtil.selectAniTalkCount(unit.mProductNo, unit.mStudyUnitCode);
//                downloadUnit.mIsMovie = unit.mIsMovie;
//                downloadUnit.mIsPriority = true;
//                downloadUnit.mIsCdnVideoFile = unit.mIsCdnVideoFile;
//                downloadUnit.mIsCdnAudio = unit.mIsCdnAudio;
//                downloadUnit.mIsCdnZip = unit.mIsCdnZip;
//                downloadUnit.mOneWeekList = tempOneWeekData;
//                downloadUnit.mIsOlndAnswer = unit.mIsOlndAnswer;
//
//                insertDownloadContentList(downloadUnit);
//            }
//            if (review.mProductNo != 0 && !unit.mReviewStudyStatus.equals("RFN")) {
//                DownloadStudyUnit reviewDownloadUnit = new DownloadStudyUnit();
//                reviewDownloadUnit.mProductNo = review.mProductNo;
//                reviewDownloadUnit.mSeriesNo = review.mSeriesNo;
//                reviewDownloadUnit.mBookNo = review.mBookNo;
//                reviewDownloadUnit.mStudyUnitNo = review.mStudyUnitNo;
//                reviewDownloadUnit.mStudyUnitCode = review.mStudyUnitCode;
//                reviewDownloadUnit.mDictationCnt = mDatabaseUtil.selectDictationCount(review.mProductNo, review.mStudyUnitCode);
//                reviewDownloadUnit.mIsCdnBFile = review.mIsCdnBFile;
//                reviewDownloadUnit.mWordQuestionList = mDatabaseUtil.selectWordQuestionWordList(review.mProductNo, review.mStudyUnitCode);
//                reviewDownloadUnit.mSentenceQuestionCnt = mDatabaseUtil.selectSentenceQuestionCount(review.mProductNo, review.mStudyUnitCode);
//                reviewDownloadUnit.mVanishingQuestionCnt = mDatabaseUtil.selectVanishingQuestionCount(review.mProductNo, review.mStudyUnitCode);
//                reviewDownloadUnit.mAniTalkCnt = mDatabaseUtil.selectAniTalkCount(review.mProductNo, review.mStudyUnitCode);
//                reviewDownloadUnit.mIsMovie = review.mIsMovie;
//                reviewDownloadUnit.mIsPriority = true;
//                reviewDownloadUnit.mIsCdnVideoFile = review.mIsCdnVideoFile;
//
//                insertDownloadContentList(reviewDownloadUnit);
//            }
//
//            mDownloadingUnitPos = position;
//            DownloadServiceAgent.RequestUpdate(mContext);
//        } catch (Exception e) {
//            e.printStackTrace();
//            showContentErrorDialog();
//        }
    }

    /**
     * 다운로드 content리스트에 추가함(db에 추가)
     *
     * @param downloadUnit : 다운로드할 차시 정보
     */
    private void insertDownloadContentList(DownloadStudyUnit downloadUnit) throws Exception {
        Map<String, String> list = ContentsUtil.getDownloadContentList(downloadUnit);

        Iterator it = list.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            mDatabaseUtil.insertDownloadContent(downloadUnit.mSeriesNo * 1000 + downloadUnit.mBookNo * 100 + downloadUnit.mStudyUnitNo, (String) pairs.getKey(), (String) pairs.getValue(), downloadUnit.mIsPriority);
        }
    }

    /**
     * 다운로드 받는 교재 item에 대한 정보 설정
     *
     * @param position : 다운로드 받는 item Position
     */
    private void setDownloadItemInfo(int position) {
        StudyBookSelectListItem.ItemInfo info = null;

        for (int i = 0; i < mInfoList.size(); i++) {
            info = mInfoList.get(i);

            if (i == position) {
                info.mIsDownloading = true;
                info.mDownloadPercent = (int) ((mUnitDownloadedCnt / (double) mUnitTotalCnt) * 100);
            } else {
                info.mIsBlackDimmed = true;
            }
        }

        mListAdapter.notifyDataSetChanged();
    }

    /**
     * 다운로드 progress 업데이트
     *
     * @param percent : 퍼센트
     */
    private void updateDownloadProgress(int percent) {
        if (percent > 100) {
            percent = 100;
        }

        StudyBookSelectListItem.ItemInfo info = mInfoList.get(mDownloadingUnitPos);
        info.mDownloadPercent = percent;

        mListAdapter.notifyDataSetChanged();
    }

    /**
     * 다운로드 실패시 item 설정
     */
    private void setDownloadCancel() {
        StudyBookSelectListItem.ItemInfo info = null;
        mDownloadingUnitPos = -1;

        for (int i = 0; i < mInfoList.size(); i++) {
            info = mInfoList.get(i);
            info.mIsDownloading = false;
            info.mIsBlackDimmed = false;
        }

        mListAdapter.notifyDataSetChanged();
    }

    /**
     * 오늘 학습 리스트 생성
     */
    private void makeTodayStudyLIst() {

        mInfoList.clear();

        for (int i = 0; i < mStudyUnit.size(); i++) {
            TodayStudyUnit unit = mStudyUnit.get(i);

            StudyBookSelectListItem.ItemInfo info = new StudyBookSelectListItem.ItemInfo();
            info.mBookName = String.format("%s - %d권 - %s", unit.mSeriesName.replace("u0027", "'"), unit.mBookNo, unit.mStudyUnitCode);
            info.mProductNo = unit.mProductNo;
            info.mSeriseNo = unit.mSeriesNo;
            info.mStudyUnitCode = unit.mStudyUnitCode;
            info.mBookNo = String.valueOf(unit.mBookNo);

            info.mBookDetailid = unit.mBookDetailId;
            info.mStudentBookId = unit.mStudentBookId;

            if (unit.mStudyPlanYmd.replaceAll("-", "").compareTo(CommonUtil.getCurrentYYYYMMDD()) > 0) {
                info.mIsTodayStudy = false;
            } else {
                info.mIsTodayStudy = true;
            }

            if (!ServiceCommon.IS_EXPERIENCES) {
                info.mThumbnail = ServiceCommon.THUMBNAIL_URL + String.format("%d_%d.jpg", unit.mSeriesNo, unit.mBookNo);
            }
            info.mIsDownloadComplete = true;
            info.mStudyUnitIndex = i;

            mInfoList.add(info);
        }

        setStudyUnitCount();

        if (mIsStudyStopped) { // 중단 학습 데이터로 시작함.
            if (mInfoList.size() != 1) {
                // 중단된 학습인데 학습 데이터가 없다는 메시지가 필요함.
            } else {
                mBookListView.setVisibility(View.VISIBLE);
                //mStudyBookLayout.setVisibility(View.VISIBLE);
                mNoStudyBookLayout.setVisibility(View.GONE);
                // 중단된 학습인데 콘텐츠가 모두 있는 경우.
                if (mInfoList.get(0).mIsDownloadComplete) {
                    //					makeStudyData(0);
                    startContentsDownload(0);
                } else {
                }
            }
        } else if (mInfoList.size() == 0) { //학습할 차시가 없을때
            mBookListView.setVisibility(View.GONE);
            mNoStudyBookLayout.setVisibility(View.VISIBLE);
        } else {
            //mStudyBookLayout.setVisibility(View.VISIBLE);
            mNoStudyBookLayout.setVisibility(View.GONE);

            if (CommonUtil.isBluetoothEnabled(this)) {
                selectItem(0);
            }

            mListAdapter.setItemInfoList(mInfoList);
            mListAdapter.notifyDataSetChanged();

            guidePlay(R.raw.b_09);// 교재를 선택하세요
        }
        /*mBookListView.setVisibility(View.VISIBLE);
        mNoStudyBookLayout.setVisibility(View.VISIBLE);
		mSecondPeriodLayout.setVisibility(View.GONE);*/
        /*txt_frist_preiod.setActivated(true);
        txt_second_preiod.setActivated(false);*/
    }

    /**
     * 아이탬 선택 (블루투스 키보드)
     *
     * @param index : 선택할 index
     */
    private void selectItem(int index) {
        if (mInfoList.size() <= 0) {
            return;
        }

        try {
            mInfoList.get(mSelectedIndex).mIsSelect = false;

            mSelectedIndex = index;

            mInfoList.get(mSelectedIndex).mIsSelect = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 선택 아이탬 변경
     *
     * @param isUpKey : up키인지 down인키인지 여부
     */
    private void changeSelectItem(boolean isUpKey) {
        int itemSize = mInfoList.size();

        if (itemSize == 1) {
            return;
        }

        if (isUpKey) {
            if (mSelectedIndex == 0) {
                selectItem(itemSize - 1);
            } else {
                selectItem(mSelectedIndex - 1);
            }
        } else {
            if (mSelectedIndex == itemSize - 1) {
                selectItem(0);
            } else {
                selectItem(mSelectedIndex + 1);
            }
        }

        mBookListView.smoothScrollToPosition(mSelectedIndex);

        mListAdapter.notifyDataSetChanged();
    }

    /**
     * 아이탬 press 설정
     *
     * @param isPress : press여부
     */
    private void setPress(boolean isPress) {
        mInfoList.get(mSelectedIndex).mIsPress = isPress;

        mListAdapter.notifyDataSetChanged();
    }

    // for bluetooth 7/24

    /**
     * 블루투스 키 이벤트
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mIsUnitDownload)
            return true;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT: // up key
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS: // down key
                mBookListView.playSoundEffect(SoundEffectConstants.CLICK);
                return true;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                mBookListView.playSoundEffect(SoundEffectConstants.CLICK);
                if (txt_frist_preiod.isActivated()) {//스마트 학습 쪽이면
                    if (mInfoList.size() > 0) {
                        setPress(true);
                    }
                } else {
                    txt_ok.setPressed(true);

                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (txt_second_preiod.getVisibility() == View.VISIBLE) {
                    if (mInfoList.size() != 0) {
                        mBookListView.setVisibility(View.VISIBLE);
                        mNoStudyBookLayout.setVisibility(View.GONE);
                    } else {
                        mBookListView.setVisibility(View.GONE);
                        mNoStudyBookLayout.setVisibility(View.VISIBLE);
                    }
                    mSecondPeriodLayout.setVisibility(View.GONE);
                    txt_frist_preiod.setActivated(true);
                    txt_second_preiod.setActivated(false);
                    img_refresh.setVisibility(View.VISIBLE);
                }

                break;
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (txt_second_preiod.getVisibility() == View.VISIBLE) {
                    mBookListView.setVisibility(View.GONE);
                    mNoStudyBookLayout.setVisibility(View.GONE);
                    mSecondPeriodLayout.setVisibility(View.VISIBLE);
                    txt_frist_preiod.setActivated(false);
                    txt_second_preiod.setActivated(true);
                    img_refresh.setVisibility(View.GONE);
                }
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    // for bluetooth 7/24

    /**
     * 블루투스 키 이벤트
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (mIsUnitDownload)
            return true;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                changeSelectItem(true);
                return true;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                changeSelectItem(false);
                return true;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (txt_frist_preiod.isActivated()) {//스마트 학습 쪽이면
                    if (mInfoList.size() > 0) {
                        setPress(false);
                        if (mInfoList.size() == 1) {
                            mSelectedIndex = 0;
                            showListProgressDialog();
                            syncDownloadDatabaseTable();
                        } else {
                            StudyBookSelectListItem.ItemInfo info = mInfoList.get(mSelectedIndex);
                            showBookSelectDialog(info.mBookName);
                        }
                    }
                } else {//훈련쪽이면
                    txt_ok.setPressed(false);
                    //Log.e("hy", "2교시시작");
                    startSecStudy();
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    /**
     * 오늘까지 학습해야할 차시 개수를 계산함
     */
    public void setStudyUnitCount() {
        TodayStudyUnit unit = null;
        TodayStudyUnit unit2 = null;
        ItemInfo info = null;

        int count = 0;

        for (int i = 0; i < mInfoList.size(); i++) {
            info = mInfoList.get(i);
            unit = mStudyUnit.get(info.mStudyUnitIndex);
            count = 0;

            for (int j = 0; j < mStudyUnit.size(); j++) {
                unit2 = mStudyUnit.get(j);

                if (unit.mProductNo == unit2.mProductNo && unit.mReStudyCnt == unit2.mReStudyCnt && unit2.mStudyPlanYmd.compareTo(CommonUtil.getCurrentYYYYMMDD2()) <= 0) {
                    count++;
                }
            }

            info.mStudyUnitCount = count;
        }
    }

    /**
     * 학습 데이터 생성
     *
     * @param index : 리스트상 선택된 index
     */
    public void makeStudyData(int index) {
        Preferences.setStudying(mContext, true);
        showProgressDialog();

        StudyBookSelectListItem.ItemInfo itemInfo = mInfoList.get(index);
        StudyUnit review = mDatabaseUtil.selectRecentNoReviewUnit(Preferences.getCustomerNo(mContext));

        StudyData.clear(); // 클리어함.

        StudyData data = StudyData.getInstance();

        int customerNo = Preferences.getCustomerNo(mContext);

        // 학습 옵션
        StudyOption option = mDatabaseUtil.selectStudyOption(customerNo);
        data.mIsAutoArtt = option.mIsAutoArtt;
        data.mIsCaption = option.mIsCaption;
        data.mIsStudyGuide = option.mIsStudyGuide;

        data.mIsFastRewind = option.mIsFastRewind;
        data.mIsDictation = option.mIsDictation;
        data.mRTagRepeatCnt = option.mRepeatCnt;
        data.mIsAudioRecordUpload = option.isRecordUpload;
        data.mIsParagraph = option.isParagraph;
        data.mIsVIPSkip = option.isVIPSkip;
        data.mIsTextKorean = option.isTextKorean;
        data.mIsDicPirvate = option.mIsDicPirvate;
        data.mIsDicAnswer = option.mIsDicAnswer;

        //		mStudyUnit = mDatabaseUtil.selectTodayStudyUnit(Preferences.getCustomerNo(this), true);

        // 본학습 data
        data.mCustomerNo = customerNo;
        //		TodayStudyUnit selectUnit = mStudyUnit.get(itemInfo.mStudyUnitIndex);
        TodayStudyUnit selectUnit = mDatabaseUtil.selectNewTodayStudyUnit(Preferences.getCustomerNo(mContext), itemInfo.mProductNo, itemInfo.mStudyUnitCode);
        data.mStudyPlanYmd = selectUnit.mStudyPlanYmd;
        data.mRecordFileYmd = selectUnit.mRecordFileYmd.length() == 0 ? selectUnit.mStudyPlanYmd : selectUnit.mRecordFileYmd;
        data.mCurrentStatus = selectUnit.mStudyStatus;
        data.mProductNo = selectUnit.mProductNo;
        data.mStudyUnitCode = selectUnit.mStudyUnitCode;
        data.mStudyUnitNo = selectUnit.mStudyUnitNo;
        data.mReStudyCnt = selectUnit.mReStudyCnt;
        data.mProductName = String.format("%s %d권 - %s", selectUnit.mSeriesName.replace("u0027", "'"), selectUnit.mBookNo, selectUnit.mStudyUnitCode);
        data.mSeriesNo = selectUnit.mSeriesNo;
        data.mSeriesName = selectUnit.mSeriesName;
        data.mBookNo = selectUnit.mBookNo;
        data.mPracticeCnt = 5;// 5로 고정됨. selectUnit.mRepeatCount.equals("1")?option.mMiddleRepeatCnt:option.mElementarytRepeatCnt;
        data.mIsAudioExist = selectUnit.mIsCdnAudio;
        data.mIsZipExist = selectUnit.mIsCdnZip;
        data.mBookDetailId = selectUnit.mBookDetailId;
        data.mStudentBookId = selectUnit.mStudentBookId;
        data.mStudyResultNo = selectUnit.mStudyResultNo;
        data.mReviewStudyStatus = selectUnit.mReviewStudyStatus;
        data.mIsOldAnswerExist = isOldAnswer(selectUnit.mSeriesNo);//일부러 db에 안넣음 구교재꺼라서 나중에 제거될꺼
        //new Dictation (테스트 용으로 만든거 나중에 빼기만 하면됨)
        data.isNewDication = selectUnit.mIsNewDictation;
        data.isNewDication = selectUnit.mIsNewDictation;

        //팝퀴즈
        data.mProductType = selectUnit.mProductType;

        // V차시인 경우 VIP 진행
        if (data.mStudyUnitCode.contains("V")) {
            data.mIsParagraph = true;
        }
        // R차시인 경우 학습모드 하(caption 보여줌)로 진행
        if (data.mStudyUnitCode.contains("R") && data.mStudyUnitCode.length() < 6) {
            data.mIsCaption = true;
        }
        //		data.mAgencyCode = selectUnit.mAgencyCode;
        //		data.mClassNo = selectUnit.mClassNo;
        //		data.mSiteKind = selectUnit.mSiteKind;

        int videoCnt = 0;
        try {
            videoCnt = ContentsUtil.getVTag(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            showContentErrorDialog();
        }
        for (int i = 1; i <= videoCnt; i++) {
            data.mVideoFile.add(ContentsUtil.getVideoFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i));
        }
        /*if (videoCnt > 0) {
			data.mVideoFile = ContentsUtil.getVideoFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
		} else {
			data.mVideoFile = "";
		}
		*/
        try {
            data.mAudioStudyTime = selectUnit.mBookCheckDt.equals("") ? 0 : Integer.valueOf(selectUnit.mBookCheckDt);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        data.mWordQuestion = mDatabaseUtil.selectWordQuestion(customerNo, selectUnit.mProductNo, selectUnit.mStudyUnitCode, selectUnit.mReStudyCnt, false);
        data.mSentenceQuestion = mDatabaseUtil.selectSentenceQuestion(customerNo, selectUnit.mProductNo, selectUnit.mStudyUnitCode, selectUnit.mReStudyCnt, false);

        //vanishing
        data.mVanishingQuestion = mDatabaseUtil.selectVanishingQuestion(customerNo, selectUnit.mProductNo, selectUnit.mStudyUnitCode, selectUnit.mReStudyCnt);

        data.mIsMovieExist = selectUnit.mIsMovie;

        StudyDataUtil.initAudioRecordPath();

        for (int i = 0; i < data.mSentenceQuestion.size(); i++) {
            SentenceQuestion question = data.mSentenceQuestion.get(i);
            question.mRecordFile = String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_SS_%d.wav", data.mRecordFileYmd.replaceAll("-", ""), data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, question.mQuestionOrder);
        }

        for (int i = 0; i < data.mVanishingQuestion.size(); i++) {
            VanishingQuestion question = data.mVanishingQuestion.get(i);
            question.mRecordFile = String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_VC_%d.wav", data.mRecordFileYmd.replaceAll("-", ""), data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, question.mQuestionOrder);
        }

        // 중단 학습이 아니고 이전학습복습 데이터가 있을때
        // 이전학습 복습 데이터를 넣어줌.
        if (!mIsStudyStopped && review.mProductNo != 0 && !data.mReviewStudyStatus.equals("RFN")) {
            data.mIsReviewExist = true;
            data.mReviewStudyPlanYmd = review.mStudyPlanYmd.equals("") ? selectUnit.mStudyPlanYmd : review.mStudyPlanYmd;//record_file_ymd이게 지금 ""으로 옴
            data.mReviewProductNo = review.mProductNo;
            data.mReviewStudyUnitCode = review.mStudyUnitCode;
            data.mReviewReStudyCnt = review.mReStudyCnt;
            data.mReviewStudyUnitNo = review.mStudyUnitNo;
            data.mReviewProductName = String.format("%s %d권 - %s", review.mSeriesName.replace("u0027", "'"), review.mBookNo, review.mStudyUnitCode);
            data.mReviewSeriesNo = review.mSeriesNo;
            data.mReviewSeriesName = review.mSeriesName;
            data.mReviewBookNo = review.mBookNo;
            data.mReStudyResultNo = review.mStudyResultNo;

            if (option.mIsReviewBook) {
                data.mReviewIsBodyExist = review.mIsCdnBFile;
                data.mReviewISBodyVideoExist = review.mIsCdnVideoFile;
            } else {
                data.mReviewIsBodyExist = false;
            }

            data.mReviewPracticeCnt = 5;// 5로 고정됨.  review.mRepeatCount.equals("1")?option.mMiddleRepeatCnt:option.mElementarytRepeatCnt;

            data.mReviewWordQuestion = mDatabaseUtil.selectWordQuestion(customerNo, review.mProductNo, review.mStudyUnitCode, review.mReStudyCnt, true);
            data.mReviewSentenceQuestion = mDatabaseUtil.selectSentenceQuestion(customerNo, review.mProductNo, review.mStudyUnitCode, review.mReStudyCnt, true);

            for (int i = 0; i < data.mReviewSentenceQuestion.size(); i++) {
                SentenceQuestion question = data.mReviewSentenceQuestion.get(i);
                question.mRecordFile = String.format(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + "%s_%d_%d_%s_%d_RS_%d.wav", data.mReviewStudyPlanYmd.replaceAll("-", ""), data.mCustomerNo, data.mReviewProductNo, data.mReviewStudyUnitCode, data.mReviewReStudyCnt, question.mQuestionOrder);
            }

            // 본문 듣기가 없고, 틀린 단어나 문장이 없을때는 review_status = FN으로 하고 review가 없는 것으로 함.
            if (!data.mReviewIsBodyExist && !data.mReviewISBodyVideoExist && data.mReviewWordQuestion.size() == 0 && data.mReviewSentenceQuestion.size() == 0) {
                data.mIsReviewExist = false;

                StudyDataUtil.setCurrentStudyStatus(this, "RFN");
            }

            if (data.mReviewStudyUnitCode.contains("R") && data.mStudyUnitCode.length() < 6) {
                data.mIsReviewExist = false;
            }
        }
        //숲용 팝업퀴즈 선별
        try {
            data.mPopupQuizList = StudyDataUtil.getQuizPopList(TagEventData.getQuizJsonArray());
            TagEventData.setQuizJsonArray(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //일일/주간평가
        if (tempOneWeekData != null) {
            data.mOneWeekData = tempOneWeekData;
        }

        //data.mIsDicAnswer = true;*
        convertYdaToOgg();

        //중앙화관련추가
        data.forestCode = Preferences.getForestCode(mContext);
        data.teacherCode = Preferences.getTeacherCode(mContext);
        data.lmsStatus = Preferences.getLmsStatus(mContext);
    }

    /**
     * yda->ogg를 진행할 파일 리스트를 생성 및 실행
     */
    private void convertYdaToOgg() {
        StudyData data = StudyData.getInstance();

        ArrayList<String> ydaFileList = new ArrayList<String>();
        ydaFileList.addAll(ContentsUtil.getUnitContentListYda(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, data.mSentenceQuestion.size(), data.mVanishingQuestion.size(), mDatabaseUtil.selectDictationCount(data.mProductNo, data.mStudyUnitCode), false, false, data.mIsAudioExist));

        if (data.mIsReviewExist) {
            ydaFileList.addAll(ContentsUtil.getUnitContentListYda(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, mDatabaseUtil.selectSentenceQuestionCount(data.mReviewProductNo, data.mReviewStudyUnitCode), 0, true, data.mReviewIsBodyExist));
        }

        if (ydaFileList.size() > 0) {
            mYdaConvert = new YdaConverter(mHandler, (String[]) ydaFileList.toArray(new String[0]));
            mYdaConvert.start();
        } else {
            checkStoppedStudy();
        }
    }

    /**
     * 학습 데이터에 soundfile(ogg)를 설정함.
     */
    public void setStudySoundFilePath() {
        StudyData data = StudyData.getInstance();

        // 본학습 파일 세팅
        data.mAudioTagFile = ContentsUtil.getTagFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        data.mAudioSoundFile = ContentsUtil.getAudioFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        //data.mVideoFile = ContentsUtil.getVideoFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        // 신규문항
        data.mPopupStudyPath = ContentsUtil.getPopupContentsPath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);

        for (int i = 0; i < data.mWordQuestion.size(); i++) {
            WordQuestion question = data.mWordQuestion.get(i);
            // 10차시 관련
            if ((data.mStudyUnitCode.contains("R") && data.mStudyUnitCode.length() < 6) || CommonUtil.isKorean(question.mQuestion) || CommonUtil.isNum(question.mQuestion)) {
                question.mSoundFile = ContentsUtil.getWordFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, question.mMeaning);
            } else {
                question.mSoundFile = ContentsUtil.getWordFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, question.mQuestion);
            }
        }

        for (int i = 0; i < data.mSentenceQuestion.size(); i++) {
            SentenceQuestion question = data.mSentenceQuestion.get(i);
            question.mSoundFile = ContentsUtil.getSentenceFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1);
        }

        for (int i = 0; i < data.mVanishingQuestion.size(); i++) {
            VanishingQuestion question = data.mVanishingQuestion.get(i);
            question.mSoundFile = ContentsUtil.getVanishingFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1);
        }

        int dictationCnt = mDatabaseUtil.selectDictationCount(data.mProductNo, data.mStudyUnitCode);
        for (int i = 0; i < dictationCnt; i++) {
            data.mDictation.add(ContentsUtil.getDictationFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1));
        }

        int aniTalkCnt = mDatabaseUtil.selectAniTalkCount(data.mProductNo, data.mStudyUnitCode);
        for (int i = 0; i < aniTalkCnt; i++) {
            data.mAniTalk.add(ContentsUtil.getAniTalkFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo, i + 1));
        }

        if (data.mIsMovieExist) {
            data.mMovieFile = ContentsUtil.getMovieFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        }
        if (data.mIsOldAnswerExist) {
            data.mOldAnswerFile = ContentsUtil.getOLDFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);
        }

        // 이전학습 복습 파일 세팅
        if (data.mIsReviewExist) {
            if (data.mReviewIsBodyExist) {
                data.mReviewBodySoundFile = ContentsUtil.getBodyFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo);
            }

            if (data.mReviewISBodyVideoExist) {
                data.mReviewBodyVideoFile = ContentsUtil.getBodyVideoFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo);
            }

            for (int i = 0; i < data.mReviewWordQuestion.size(); i++) {
                WordQuestion question = data.mReviewWordQuestion.get(i);
                if ((data.mReviewStudyUnitCode.contains("R") && data.mReviewStudyUnitCode.length() < 6) || CommonUtil.isKorean(question.mQuestion) || CommonUtil.isNum(question.mQuestion)) {
                    question.mSoundFile = ContentsUtil.getWordFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, question.mMeaning);
                } else {
                    question.mSoundFile = ContentsUtil.getWordFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, question.mQuestion);
                }
            }

            for (int i = 0; i < data.mReviewSentenceQuestion.size(); i++) {
                SentenceQuestion question = data.mReviewSentenceQuestion.get(i);
                question.mSoundFile = ContentsUtil.getSentenceFilePath(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo, question.mQuestionOrder);
            }
        }

        // 신규문항
        boolean bUnzip = false;
        String filepath = "";

        if (data.mIsZipExist) {
            try {
                filepath = ContentsUtil.getPopupContentsFilePath(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo) + ContentsUtil.POPUPSTUDY_POSTFIX;

                if (!filepath.isEmpty()) {
                    bUnzip = CommonUtil.unzip(filepath, data.mPopupStudyPath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bUnzip) {
                    File file = new File(filepath);
                    if (!file.exists())
                        return;

                    file.delete();
                }
            }
        }
    }

    /**
     * 학습 데이터에 따라 학습 시작 화면 선정
     */
    private void studyStart() {
        StudyData data = StudyData.getInstance();
        String status = data.mCurrentStatus;

        if (data.mIsReviewExist) { //이전학습 복습이 존재하면(중단학습일때는 이전학습 복습이 존재하지 않는 것으로 진행)
            int targetActivity = 0;
            if (data.mReviewIsBodyExist)
                targetActivity = ServiceCommon.STATUS_REVIEW_AUDIO;
            else if (data.mReviewISBodyVideoExist)
                targetActivity = ServiceCommon.STATUS_REVIEW_AUDIO;
            else if (0 < data.mReviewWordQuestion.size())
                targetActivity = ServiceCommon.STATUS_REVIEW_WORD;
            else if (0 < data.mReviewSentenceQuestion.size())
                targetActivity = ServiceCommon.STATUS_REVIEW_SENTENCE;

            startActivity(new Intent(this, ReStudyTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, targetActivity)); // 이전학습 복습 간지
        } else if (status.equals("")) { // 중단학습이 아니면
            if (data.mIsAudioExist) {
                startActivity(new Intent(this, SmartStudyTitlePaperActivity.class)); // 오디오 학습 간지
            } else {
                StudyDataUtil.setCurrentStudyStatus(this, "S01");
                if (0 < data.mWordQuestion.size() || 0 < data.mSentenceQuestion.size() || (data.mIsParagraph && 0 < data.mVanishingQuestion.size())) {// 단어 시험이나 문장 시험이 있으면 시험 준비
                    startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
                } else if (data.mIsDictation && 0 < data.mDictation.size()) { // 받아쓰기가 있으면 받아쓰기로
                    startActivity(new Intent(this, DictationTitlePaperActivity.class));
                } else if (data.isNewDication && data.mIsDicPirvate) {
                    startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                } else if (data.mIsMovieExist) {
                    startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                } else if (data.mOneWeekData.size() > 0)
                    startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
                else {
                    startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
                }
            }

        }
        // 중단 학습일 경우 본학습 학습상태 코드(S01:학습시작, S11:본학습_음원듣기시작,S12:종료,
        //S21:시험준비,S22:시험 준비 완료 , S31,S32:본학습_단어, S41,S42:본학습_문장, S51,S52:본학습_받아쓰기,S61,S62:본학습_문법동영상,SFN:학습종료)
        else if (status.equals("S01") || status.equals("S11") || status.equals("RFN")) {
            //startActivity(new Intent(this, SmartStudyTitlePaperActivity.class)); // 오디오 학습 간지
            if (data.mIsAudioExist) {
                startActivity(new Intent(this, SmartStudyTitlePaperActivity.class)); // 오디오 학습 간지
            } else {
                StudyDataUtil.setCurrentStudyStatus(this, "S01");
                if (0 < data.mWordQuestion.size() || 0 < data.mSentenceQuestion.size() || (data.mIsParagraph && 0 < data.mVanishingQuestion.size())) {// 단어 시험이나 문장 시험이 있으면 시험 준비
                    startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
                } else if (data.mIsDictation && 0 < data.mDictation.size()) { // 받아쓰기가 있으면 받아쓰기로
                    startActivity(new Intent(this, DictationTitlePaperActivity.class));
                } else if (data.isNewDication && data.mIsDicPirvate) {
                    startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                } else if (data.mIsMovieExist) {
                    startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                } else if (data.mOneWeekData.size() > 0)
                    startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
                else {
                    startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
                }
            }
        } else if (status.equals("S12") || status.equals("S21")) {
            if (0 < data.mWordQuestion.size() || 0 < data.mSentenceQuestion.size() || (data.mIsParagraph && 0 < data.mVanishingQuestion.size())) {// 단어 시험이나 문장 시험이 있으면 시험 준비
                startActivity(new Intent(this, ExamPreparingTitlePaperActivity.class));
            } else if (data.mIsDictation && 0 < data.mDictation.size()) { // 받아쓰기가 있으면 받아쓰기로
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class)); // 없다면 결과 페이지로
            }
        } else if (status.equals("S22")) { // 시험준비가 들어온것은 둘중 하나는 있는 것임
            if (0 < data.mWordQuestion.size()) {
                startActivity(new Intent(this, WordTitlePaperActivity.class)); // 단어 시험 간지로
            } else if (0 < data.mSentenceQuestion.size()) {
                startActivity(new Intent(this, SentenceTitlePaperActivity.class)); // 문장 시험 간지로
            } else if (data.mIsParagraph && 0 < data.mVanishingQuestion.size()) {
                startActivity(new Intent(this, VanishingTitlePaperActivity.class)); // 문장 시험 간지로
            } else if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S31")) {
            startActivity(new Intent(this, WordTitlePaperActivity.class)); // 단어 시험 간지로
        } else if (status.equals("S32") || status.equals("S41")) {
            if (0 < data.mSentenceQuestion.size()) {
                startActivity(new Intent(this, SentenceTitlePaperActivity.class));
            } else if (data.mIsParagraph && 0 < data.mVanishingQuestion.size()) {
                startActivity(new Intent(this, VanishingTitlePaperActivity.class));
            } else if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S42") || status.equals("S71")) {
            if (data.mIsParagraph && 0 < data.mVanishingQuestion.size()) {
                startActivity(new Intent(this, VanishingTitlePaperActivity.class));
            } else if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S51") || status.equals("S72")) {
            if (data.mIsDictation && 0 < data.mDictation.size()) {
                startActivity(new Intent(this, DictationTitlePaperActivity.class));
            } else if (data.isNewDication && data.mIsDicPirvate) {
                startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
            } else if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class));
            }
        } else if (status.equals("S52") || status.equals("S61")) { //
            if (data.mIsMovieExist) {
                startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
            } else if (data.mOneWeekData.size() > 0)
                startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
            else {
                startActivity(new Intent(this, StudyOutcomeActivity.class)); // 결과 페이지로
            }
        } else if (status.equals("S62")) {
            startActivity(new Intent(this, StudyOutcomeActivity.class)); // 결과 페이지로
        }

        finish();
    }

    /**
     * 메시지 박스에 대한 응답
     */
    @Override
    public void onDialogDismiss(int result, int dialogId) {
        Log.e(TAG, "result:" + result + "   dialogID" + dialogId);
        if (dialogId == MSGBOX_ID_STOPPED_STUDY_NOTI) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                studyStart();
            }
        } else if (dialogId == MSGBOX_ID_UPDOWN_REFRESH) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                if (!mIsUnitDownload)
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_FILE_UPLOAD);
            }
        } else if (dialogId == MSGBOX_ID_UPDOWN_COMPLETE) {
            hideProgressDialog();

        } else if (dialogId == MSGBOX_ID_BOOK_SELECT) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                showListProgressDialog();
                syncDownloadDatabaseTable();
                //startContentsDownload(mSelectedIndex);
            } else {
            }
        } else if (dialogId == MSGBOX_ID_UPDATE) {
            if (result == BaseDialog.DIALOG_CONFIRM) {
                startActivity(new Intent(StudyBookSelectActivity.this, IntroActivity.class));
                sendLogOutEvent2();
                finish();
            } else {
                startStudyBookSelect();
            }
        } else if (dialogId == MSGBOX_ID_CONTENT_ERROR) {
            downloadList();
        } else if (dialogId == MSGBOX_ID_SEC_ERROR) {
            viewList();
            hideListProgressDialog();
            changeLayout();

        }
    }

    /**
     * 중단 학습 메시지 박스
     *
     * @param msg : 메시지
     */
    private void showStoppedStudyNotiDialog(String msg) {
        hideProgressDialog();

        mMsgBox = new MessageBox(this, 0, msg);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_STOPPED_STUDY_NOTI);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 중단 학습인지 여부
     */
    private void checkStoppedStudy() {
        StudyData data = StudyData.getInstance();

        if (mIsStudyStopped)
            showStoppedStudyNotiDialog(data.mProductName + "의\n 중단된 학습을 이어서 진행합니다.");
        else
            studyStart();
    }

    /**
     * yda->ogg 결과 및 sync handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_CONVERT: {
                    Log.e(TAG, "Convert Complete Success : " + msg.arg1);
                    if (msg.arg1 == ServiceCommon.MSG_SUCCESS) {
                        // success 일때만 start
                        setStudySoundFilePath();
                        checkStoppedStudy();
                    } else {
                        String message = getString(R.string.string_common_study_file_convert_error) + msg.arg2;
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }

                    break;
                }

                case ServiceCommon.MSG_RECORDING_FILE_UPLOAD: // 음성 파일 업로드 시작
                    if (mProgressDialog == null) {
                        mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_book_download_progress1);
                    }
                    startActivityForResult(new Intent(mContext, UploadActivity.class), REQUESTCODE_UPLOAD);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START: // upload sync 시작(학습 결과에 대한 업로드)
                    Preferences.setStudying(mContext, true);
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS: // upload sync 성공
                    syncDownloadDatabaseTable(); // 업로드 싱크가 끝나면, 다운로드 싱크를 함.
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL: // upload sync 실패
                    showSyncUpDownNetworkErrorDialog();
                    //					Preferences.setSyncSuccess(mContext, false);
                    break;

                case ServiceCommon.MSG_DATABASE_NEW_DOWNLOAD_SYNC_SUCCESS: // download sync 성공

                    //					Toast.makeText(mContext, getString(R.string.string_msg_sync_updown_complete), Toast.LENGTH_SHORT).show();

                    Preferences.setStudying(mContext, false);
                    Preferences.setSyncSuccess(mContext, true);
                    mStudyUnit = StudyData.getmStudyUnit();
                    changeLayout();
                    if (isStudyEnd) {//처음시작할때 흐름이면
                        isStudyEnd = false;
                        hideProgressDialog();
                    } else {
                        showSyncUpDownCompleteDialog();
                    }
                    break;

                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL: // download sync 실패
                    Preferences.setStudying(mContext, false);
                    showSyncUpDownNetworkErrorDialog();
                    Preferences.setSyncSuccess(mContext, false);
                    break;
                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS: // download sync성공
                    hideListProgressDialog();
                    startContentsDownload(mSelectedIndex);
                    break;
            }

            super.handleMessage(msg);
        }
    };

    private Handler mBackupHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage what : " + msg.what);
            HttpJSONRequest request = new HttpJSONRequest(mContext);
            HttpJSONRequest.mIsStatusRequest = false;

            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:

                    if (HttpJSONRequest.mStatusQueue.size() != 0) {
                        Log.e("ss99km01", "ss99km01 StudyBookSelectActivity  mBackupHandler requestBackupStatus");
                        request.requestBackupStatus(mBackupHandler);
                    } else {
                        downloadList();
                        Log.e("ss99km01", "ss99km01 StudyBookSelectActivity  downloadList");
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    HttpJSONRequest.mIsStatusRequest = false;
                    request.backupStatus();
                    showDownloadNetworkErrorPopup();
                    Log.e("ss99km01", "ss99km01 StudyBookSelectActivity MSG_HTTP_REQUEST_FAIL showDownloadNetworkErrorPopup");
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * 학습 진행 progress 화면 보여줌
     */
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = LoadingDialog.show(this, R.string.string_common_loading, R.string.string_studying_file_preparing);
        }
    }

    /**
     * 학습 진행 progress 화면 숨김
     */
    private void hideProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * 학습 진행 progress 화면 보여줌
     */
    private void showListProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = LoadingDialog.show(this, R.string.string_common_loading, R.string.string_studying_List_preparing);
        }
    }

    /**
     * 학습 진행 progress 화면 숨김
     */
    private void hideListProgressDialog() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * 교재 다운로드 네트워크 에러 팝업 띄움
     */
    public void showDownloadNetworkErrorPopup() {
        String str_error = "";
        if (ErrorData.getErrorCode() == ErrorData.ERROR_HTTP_FILE_NOT_FOUND) {
            str_error = ErrorData.getErrorMsg();
        } else {
            if (ErrorData.getErrorMsg().contains("UNMOUNTED_STORAGE") || ErrorData.getErrorMsg().contains("NOT ENOUGH SPACE")) {
                str_error = getString(R.string.string_msg_not_exist_storage_or_using_usb_disk);
            } else {
                str_error = getString(R.string.string_msg_is_not_connected) + "\n" + ErrorData.getErrorMsg();
            }
        }

        if (ServiceCommon.CONTENT_MODE == ServiceCommon.REAL_CONTENT) {
            Crashlytics.logException(new FileNotFoundException(str_error));
        }

        mMsgBox = new MessageBox(this, 0, str_error);
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.show();
    }

    /**
     * 새로 고침시 오류 발생시 띄우는 에러 다이얼로그
     */
    private void showSyncUpDownNetworkErrorDialog() {
        hideProgressDialog();

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_error) + "\n" + ErrorData.getErrorMsg());
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setCancelText(R.string.string_common_cancel);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_REFRESH);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 새로 고침 성공 다이얼로그
     */
    private void showSyncUpDownCompleteDialog() {

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_updown_complete));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_COMPLETE);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 컨텐츠 실행실패시 다이얼로그
     */
    private void showContentErrorDialog() {

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sync_content_error));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_CONTENT_ERROR);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * Upload Activity Result를 받는 함수
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUESTCODE_UPLOAD == requestCode) {

            switch (resultCode) {
                case HttpURLConnection.HTTP_OK:
                    /* case UploadActivity.ERR_EMPTY_LIST : */
                    // success
            }

            Log.i("", "resultCode:" + resultCode);

            if (RESULT_CANCELED == resultCode) {
                finish();
                return;
            }

            mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
        }

        if (requestCode == ServiceCommon.MSG_STUDY_FINISH) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.downloadList(mHandler, Preferences.getCustomerNo(this));
        }
    }

    /**
     * db upload sync 실행
     */
    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mHandler);
        } else {
            Preferences.setStudying(mContext, false);
            Preferences.setSyncSuccess(mContext, false);
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * db download sync 실행
     */
    public void syncDownloadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            final DatabaseSync databaseSync = new DatabaseSync(this);
            if (mSelectedIndex >= mInfoList.size()) {
                mSelectedIndex = 0;
                Preferences.setStudying(mContext, false);
                Preferences.setSyncSuccess(mContext, false);
                showSyncUpDownNetworkErrorDialog();
                return;

            }
            final StudyBookSelectListItem.ItemInfo info = mInfoList.get(mSelectedIndex);
            /**
             * 일주간평가문항
             */
            reqQTET = new RequestQTETPhpPost(mContext) {
                @Override
                protected void onPostExecute(ArrayList<OneWeekData> result) {
                    tempOneWeekData = result;
                    try {
                        databaseSync.downloadSyncStart(mHandler, Preferences.getCustomerNo(mContext), info.mProductNo, info.mStudyUnitCode, String.valueOf(info.mBookDetailid), String.valueOf(info.mStudentBookId));
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        Preferences.setStudying(mContext, false);
                        Preferences.setSyncSuccess(mContext, false);
                        showSyncUpDownNetworkErrorDialog();
                    }

                }
            };

            /**
             * 일일/주간 문항개수 때문에
             */
            RequestPhpPost reqBDIF = new RequestPhpPost(mContext) {

                @Override
                protected void onPostExecute(Map<String, String> result) {
                    // TODO Auto-generated method stub
                    super.onPostExecute(result);
                    int i = 0;
                    try {
                        i = Integer.valueOf(result.get("cnt_test"));
                    } catch (Exception e) {//일일 주간평가 6월에 우영 적용한다고 함
                        e.printStackTrace();
                        i = 0;
                    }
                    if (i == 0) {//일일/주간평가 없을때
                        try {
                            //sync_all
                            databaseSync.downloadSyncStart(mHandler, Preferences.getCustomerNo(mContext), info.mProductNo, info.mStudyUnitCode, String.valueOf(info.mBookDetailid), String.valueOf(info.mStudentBookId));
                        } catch (UnsupportedEncodingException e) {
                            Preferences.setStudying(mContext, false);
                            Preferences.setSyncSuccess(mContext, false);
                            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
                            showSyncUpDownNetworkErrorDialog();
                        }

                    } else {//있을대
                        reqQTET.sendQTET(Preferences.getUserNo(mContext), info.mBookDetailid, i, String.valueOf(info.mProductNo), info.mStudyUnitCode);
                    }
                }
            };

            reqBDIF.getBDIF(info.mProductNo, info.mBookDetailid, info.mStudyUnitCode);

        } else {
            Preferences.setStudying(mContext, false);
            Preferences.setSyncSuccess(mContext, false);
            ErrorData.setErrorMsg(ErrorData.ERROR_NOT_AVAILABLE_NETWORKS, "NOT_AVAILABLE_NETWORKS");
            showSyncUpDownNetworkErrorDialog();
        }
    }

    /**
     * 다운로드 서비스 이벤트 receiver
     */
    BroadcastReceiver mDownloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (AgentEvent.ACTION_RECEIVE_AGENT_EVENT.equals(intent.getAction())) {
                int type = intent.getIntExtra(AgentEvent.EVENT_TYPE, 0);
                switch (type) {
                    case AgentEvent.DOWNLOAD_START:
                        Log.e(TAG, "DOWNLOAD_START");
                        break;

                    case AgentEvent.DOWNLOAD_STOP:
                        Log.e(TAG, "DOWNLOAD_STOP");
                        break;

                    case AgentEvent.DOWNLOAD_COMPLETE:
                        Log.e(TAG, "DOWNLOAD_COMPLETE");
                        break;

                    case AgentEvent.DOWNLOAD_FAIL:
                        Log.e(TAG, "DOWNLOAD_FAIL");
                        // 네트워크가 설정에 다운을 받을수 없음.
                        // 오류 팝업
                        if (mIsUnitDownload) {
                            int error = intent.getIntExtra(AgentEvent.FAIL_REASON, AgentEvent.REASON_NO_ERROR);

                            switch (error) {
                                case AgentEvent.REASON_NO_ERROR:
                                    break;

                                case AgentEvent.REASON_UNMOUNTED_STORAGE:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "UNMOUNTED_STORAGE");
                                    break;

                                case AgentEvent.REASON_NOT_ENOUGH_SPACE:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT ENOUGH SPACE");
                                    break;

                                case AgentEvent.REASON_NET_ERROR:
                                    String msg = "";
                                    if (intent.getStringExtra(AgentEvent.RESPONSE_INFO) != null) {
                                        msg = intent.getStringExtra(AgentEvent.RESPONSE_INFO);
                                    }
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_NOT_FOUND, msg);
                                    break;

                                case AgentEvent.REASON_UNKNOWN_ERROR:
                                    break;

                                case AgentEvent.REASON_NOT_AVAILABLE_NETWORKS:
                                    ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_DOWNLOAD, "NOT_AVAILABLE_NETWORKS");
                                    break;
                            }
                            showDownloadNetworkErrorPopup();
                            mIsUnitDownload = false;
                            setDownloadCancel();
                        }

                        break;

                    case AgentEvent.START_LEARNING:
                        Log.e(TAG, "START_LEARNING");
                        break;

                    case AgentEvent.CONTENTS_UPDATE:
                        Log.e(TAG, "CONTENTS_UPDATE");
                        DownloadServiceAgent.RequestStart(StudyBookSelectActivity.this);
                        mIsUnitDownload = true;
                        break;

                    case AgentEvent.DOWNLOAD_STATE: {

                        Log.e(TAG, "DOWNLOAD_STATE " + String.format("Tot(%d/%d), Pri(%d/%d)", intent.getIntExtra(AgentEvent.TOT_POS, 0), intent.getIntExtra(AgentEvent.TOT_CNT, 0), intent.getIntExtra(AgentEvent.PRI_POS, 0), intent.getIntExtra(AgentEvent.PRI_CNT, 0)));

                        int priorityTotal = intent.getIntExtra(AgentEvent.PRI_CNT, 0);
                        int priorityPos = intent.getIntExtra(AgentEvent.PRI_POS, 0);
                        if (priorityTotal != 0 && mDownloadingUnitPos != -1) {
                            updateDownloadProgress((int) (((mUnitDownloadedCnt + priorityPos) / (double) mUnitTotalCnt) * 100));
                        }

                        if (priorityPos == 0 && priorityTotal == 0 && mDownloadingUnitPos != -1) {
                            //다운로드 완료
                            if (mIsUnitDownload) {
                                //makeStudyData(mDownloadingUnitPos);
                                startContentsDownload(mSelectedIndex);
                                mIsUnitDownload = false;
                            }
                        }
                    }
                    break;
                }
            }
        }

    };

    @Override
    protected void onStart() {
        super.onStart();
        // 서비스 바인딩.
        if (mServiceAgent == null) {
            bindService(new Intent(this, DownloadService.class), serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 서비스 언바인딩.
        if (mServiceAgent != null) {
            unbindService(serviceConnection);
            mServiceAgent = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        overridePendingTransition(0, 0);

        hideProgressDialog();

        try {
            unregisterReceiver(mDownloadReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mServiceAgent = null;
    }

    private void startStudyBookSelect() {
        mStudyUnit = StudyData.getmStudyUnit();
        changeLayout();
    }

    /**
     * 2차시 시작(GETS->QINF)
     */
    private void startSecStudy() {

        if (sec_max_cnt != 0 && sec_cur_cnt != sec_max_cnt) {//카운트가 0이아닌데 같다면
            /**
             * Qinf생성
             */
            reqQinf = new RequestPhpPost(mContext) {
                @Override
                protected void onPostExecute(Map<String, String> result) {
                    super.onPostExecute(result);
                    makeSecStudyData(result);
                }
            };
            /**
             * Gets생성
             */
            RequestPhpPost reqGets = new RequestPhpPost(mContext) {
                @Override
                protected void onPreExecute() {
                    // TODO Auto-generated method stub
                    super.onPreExecute();
                    if (mProgressDialog == null) {
                        mProgressDialog = LoadingDialog.show(mContext, R.string.string_common_loading, R.string.string_book_download_progress);
                    }
                }

                @Override
                protected void onPostExecute(Map<String, String> result) {
                    super.onPostExecute(result);
					/*	int last_detail_id = 0;
						try {
							last_detail_id = Integer.valueOf(result.get("last_book_detail_id"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						reqQinf.getQINF(last_detail_id);//마지막 차시//나중에
					*/

                    reqQinf.getQINF(0);//마지막 차시//민트패드에서는 0으로 넘김
                }
            };

            reqGets.getGETS();

            //req.onSyncPostExecute(result);
			/*startActivity(new Intent(this, SecondIntroActivity.class));
			finish();*/
        } else {
            showMaxSecondStudy();
        }
    }

    /**
     * QUST
     *
     * @param result
     */
    private void makeSecStudyData(Map<String, String> result) {

        RequestQUSTPhpPost reqQust = new RequestQUSTPhpPost(mContext) {
            @Override
            protected void onProgressUpdate(Integer... values) {
                // TODO Auto-generated method stub
                super.onProgressUpdate(values);
                try {
                    int val = values[0];
                    String ment = null;
                    if (0 <= val && val <= 39) {
                        ment = getString(R.string.string_book_download_progress3);
                    } else if (40 <= val && val <= 59) {
                        ment = getString(R.string.string_book_download_progress3);
                    } else if (60 <= val && val <= 79) {
                        ment = getString(R.string.string_book_download_progress3);
                    } else if (80 <= val) {
                        ment = getString(R.string.string_book_download_progress3);
                    }
                    mProgressDialog.setMessage(val + "%\n\n" + ment);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onPostExecute(SecStudyData result) {
                super.onPostExecute(result);
                try {
                    if (null != mProgressDialog && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                    if (result.getException() != null) {

                        if (ServiceCommon.CONTENT_MODE == ServiceCommon.REAL_CONTENT) {
                            Crashlytics.logException(result.getException());
                        }
                        showNotDownloadSecondStudy();
                        Log.e(TAG, result.getException().toString());
                        return;

                    }
                    //2교시 자료 생성
                    SecStudyData.getInstance().setInstance(result);

                    SecStudyData data = SecStudyData.getInstance();


                    // 학습 옵션
                    StudyOption option = mDatabaseUtil.selectStudyOption(data.getMcode());
                    //data.set_caption(option.mIsCaption);
                    data.set_caption(true);
                    //시작
                    startActivity(new Intent(mContext, SecIntroActivity.class));
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (null != mProgressDialog && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                }
            }
        };
        try {
            result.putAll(qGetMap);
            reqQust.getQUST(result);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
            showNotconfigSecondStudy();
        }

    }

    //2교시 클릭하면 QGET실행
    private void sendQGet() {
        RequestPhpPost reqQget = new RequestPhpPost(mContext) {
            protected void onPreExecute() {
                qGetMap = null;
            }

            @Override
            protected void onPostExecute(Map<String, String> result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
                try {
                    qGetMap = result;
                    sec_cur_cnt = Integer.valueOf(result.get("is_class_count"));
                    sec_max_cnt = Integer.valueOf(result.get("total_count"));

                    txt_month.setText(CommonUtil.curMonth() + "월 ");
                    txt_cnt.setText(result.get("is_class_count") + "회  / " + result.get("total_count") + "회 ");
                    txt_total_cnt.setText(result.get("total_count"));
                    if ("0".equals(result.get("is_class_count")) && "0".equals(result.get("total_count"))) {
                        txt_second_preiod.setVisibility(View.GONE);
                    } else {
                        txt_second_preiod.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {//qget이 없다면 안보이게
                    e.printStackTrace();
                    txt_second_preiod.setVisibility(View.GONE);
                }
            }
        };

        reqQget.getQGET();
    }

    /**
     *
     */
    private void initLayout() {
        showUserName();
        setTitlebarText(getString(R.string.string_common_select_study));

        refresh_layout2 = (LinearLayout) findViewById(R.id.refresh_layout2);
        mStudyBookLayout = (LinearLayout) findViewById(R.id.study_book_layout);
        mNoStudyBookLayout = (LinearLayout) findViewById(R.id.no_study_book_layout);
        mSecondPeriodLayout = (LinearLayout) findViewById(R.id.second_period_layout);
        mNoStudyBookTextView = (TextView) findViewById(R.id.no_study_book_textview);
        txt_frist_preiod = (TextView) findViewById(R.id.txt_frist_preiod);
        txt_second_preiod = (TextView) findViewById(R.id.txt_second_preiod);
        txt_month = (TextView) findViewById(R.id.txt_month);
        txt_cnt = (TextView) findViewById(R.id.txt_cnt);
        txt_total_cnt = (TextView) findViewById(R.id.txt_total_cnt);
        mBookListView = (ListView) findViewById(R.id.book_listview);
        mListAdapter = new StudyBookSelectListAdapter(this, this, mInfoList);
        mBookListView.setAdapter(mListAdapter);
        img_refresh = (ImageView) findViewById(R.id.refresh_imageview);
        img_refresh.setOnClickListener(this);
        txt_frist_preiod.setOnClickListener(this);
        txt_second_preiod.setOnClickListener(this);
        refresh_layout2.setOnClickListener(this);
        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_ok.setOnClickListener(this);
        txt_frist_preiod.setActivated(true);
        txt_second_preiod.setActivated(false);

    }

    /**
     *
     */
    private void showMaxSecondStudy() {

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sec_max_study));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_COMPLETE);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 다운로드 실패
     */
    private void showNotDownloadSecondStudy() {

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sec_enough_study));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_SEC_ERROR);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    /**
     * 스마트훈련 관련 설정 문제
     */
    private void showNotconfigSecondStudy() {

        mMsgBox = new MessageBox(this, 0, getString(R.string.string_msg_sec_config_study));
        mMsgBox.setConfirmText(R.string.string_common_confirm);
        mMsgBox.setId(MSGBOX_ID_UPDOWN_COMPLETE);
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    private boolean isOldAnswer(int mSeriesNo) {
        boolean result = false;
        //구교재정보
        for (int sNom : ServiceCommon.OLD_BOOK_LIST) {
            if (sNom == mSeriesNo) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * 목록화면으로 전환
     */
    private void viewList() {
        if (mInfoList.size() != 0) {
            mBookListView.setVisibility(View.VISIBLE);
            mNoStudyBookLayout.setVisibility(View.GONE);
        } else {
            mBookListView.setVisibility(View.GONE);
            mNoStudyBookLayout.setVisibility(View.VISIBLE);
        }
        mSecondPeriodLayout.setVisibility(View.GONE);
        img_refresh.setVisibility(View.VISIBLE);
        txt_frist_preiod.setActivated(true);
        txt_second_preiod.setActivated(false);

    }

    /**
     * 스마트훈련화면으로 전환
     */
    private void viewSec() {
        mBookListView.setVisibility(View.GONE);
        mNoStudyBookLayout.setVisibility(View.GONE);
        mSecondPeriodLayout.setVisibility(View.VISIBLE);
        txt_frist_preiod.setActivated(false);
        txt_second_preiod.setActivated(true);
        img_refresh.setVisibility(View.GONE);

    }

}
