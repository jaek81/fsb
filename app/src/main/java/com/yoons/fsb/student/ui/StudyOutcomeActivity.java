package com.yoons.fsb.student.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.OneWeekData;
import com.yoons.fsb.student.data.StudyData.StudyResult;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ContentsUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;

import java.util.IllegalFormatException;

/**
 * 학습결과 화면
 *
 * @author dckim
 */
public class StudyOutcomeActivity extends BaseActivity implements OnClickListener, View.OnTouchListener {
    private static final String TAG = "[StudyOutcomeActivity]";
    private TextView mStudyContent = null;
    private TextView mWordTotalCnt = null;
    private TextView mWordRightCnt = null;
    private TextView mSentenceTotalCnt = null;
    private TextView mSentenceRightCnt = null;
    private TextView mTestTotalCnt = null;
    private TextView mTestRightCnt = null;
    private TextView mRunningTime = null;
    //private TextView m4SkillText = null;
    private TextView mNoStudylText = null;
    private LinearLayout mWordArea = null;
    private LinearLayout mSentenceArea = null;
    private LinearLayout mDurationArea = null;
    private LinearLayout mTestArea = null;

    private LinearLayout mVanishingArea = null;
    //	private TextView mVanishingTotalCnt = null;
    //	private TextView mVanishingRightCnt = null;

    private LoadingDialog mProgressDialog = null;

    private boolean mIsSyncEnd = false;

    private static final int REQUESTCODE_UPLOAD = 128;

    private TextView btn_moveFskill = null;
    private TextView btn_moveList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_study_outcome);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                setContentView(R.layout.w_study_outcome);
            else // 숲
                setContentView(R.layout.f_study_outcome);
        }

        findViewById(R.id.paper_title_view).setVisibility(View.GONE);
        findViewById(R.id.end_title_view).setVisibility(View.VISIBLE);

        // 타이틀 바의 속성 설정
        setTitlebarText(getString(R.string.string_learning_outcome));
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        mStudyContent = (TextView) findViewById(R.id.study_content);
        mWordTotalCnt = (TextView) findViewById(R.id.word_total_cnt);
        mWordRightCnt = (TextView) findViewById(R.id.word_right_cnt);
        mSentenceTotalCnt = (TextView) findViewById(R.id.sentence_total_cnt);
        mSentenceRightCnt = (TextView) findViewById(R.id.sentence_right_cnt);
        mTestTotalCnt = (TextView) findViewById(R.id.test_total_cnt);
        mTestRightCnt = (TextView) findViewById(R.id.test_right_cnt);
        mRunningTime = (TextView) findViewById(R.id.running_time);
        //m4SkillText = (TextView) findViewById(R.id.fourskill_befly_text);
        mNoStudylText = (TextView) findViewById(R.id.outcome_nostudy_text);
        btn_moveFskill = (TextView) findViewById(R.id.btn_fskill);
        btn_moveList = (TextView) findViewById(R.id.btn_list);
        btn_moveList.setFocusable(true);
        btn_moveList.setFocusableInTouchMode(true);
        btn_moveFskill.setFocusableInTouchMode(true);
        btn_moveFskill.setFocusable(true);
        btn_moveFskill.requestFocus();
        btn_moveFskill.setOnTouchListener(this);
        btn_moveList.setOnTouchListener(this);
        btn_moveFskill.setOnClickListener(this);
        btn_moveList.setOnClickListener(this);

        mSentenceArea = (LinearLayout) findViewById(R.id.outcome_sentence_area);
        mWordArea = (LinearLayout) findViewById(R.id.outcome_word_area);
        mDurationArea = (LinearLayout) findViewById(R.id.outcome_duration_area);
        mTestArea = (LinearLayout) findViewById(R.id.outcome_test_area);

        mVanishingArea = (LinearLayout) findViewById(R.id.outcome_vanishing_area);
        //btn_moveFskill.setActivated(true);
        //		mVanishingTotalCnt = (TextView)findViewById(R.id.vanishing_total_cnt);
        //		mVanishingRightCnt = (TextView)findViewById(R.id.vanishing_right_cnt);

        // 학습 진행 상태 코드를 업데이트(SFN: 학습 종료 화면에서 호출)
        StudyDataUtil.setCurrentStudyStatus(this, "SFN");

        showProgressDialog(true);

        StudyResult sr = StudyDataUtil.getStudyResult();
        mStudyContent.setText(sr.mProductName);
        mRunningTime.setText(getValue(sr.mStudyTotalTime));

        StudyData data = StudyData.getInstance();

        if (null != data) {
            if (0 < data.mWordQuestion.size()) {
                // 단어 시험 존재
                mWordTotalCnt.setText(getValue(sr.mWordTotal));
                mWordRightCnt.setText(getValue(sr.mWordCorrect));
                mWordArea.setVisibility(View.VISIBLE);
            }

            if (0 < data.mSentenceQuestion.size()) {
                // 문장 시험 존재
                mSentenceTotalCnt.setText(getValue(sr.mSentenceTotal));
                mSentenceRightCnt.setText(getValue(sr.mSentenceCorrect));
                mSentenceArea.setVisibility(View.VISIBLE);
            }

            /*if (0 < data.mVanishingQuestion.size() && data.mIsParagraph) {
                // 문단 시험 존재
                if (!data.mIsCaption && !data.mStudyUnitCode.contains("V")) {
                    if (sr.mSentenceTotal > 0) {
                        int percent = sr.mSentenceCorrect * 100 / sr.mSentenceTotal;
                        if (percent < 50) {
                            mVanishingArea.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    mVanishingArea.setVisibility(View.VISIBLE);
                }

                if (!data.mIsAudioExist) {
                    ((TextView) findViewById(R.id.vanishing_position_center)).setVisibility(View.VISIBLE);
                }
            }*/

            if ("0" != mRunningTime.getText() || null != mRunningTime.getText()) {
                // 학습 시간 존재
                mDurationArea.setVisibility(View.VISIBLE);
            }

            if (data.mWordQuestion.size() == 0 && data.mSentenceQuestion.size() == 0 && data.mVanishingQuestion.size() == 0 && !data.mIsAudioExist) {
                mDurationArea.setVisibility(View.VISIBLE);
                findViewById(R.id.learning_tag_time).setVisibility(View.GONE);
                //m4SkillText.setVisibility(View.INVISIBLE);
                //mNoStudylText.setVisibility(View.VISIBLE);
                //mStudyContent.setVisibility(View.INVISIBLE);
            }

            if (data.mOneWeekData != null && data.mOneWeekData.size() != 0) {
                int total = data.mOneWeekData.size();
                int right = 0;

                for (OneWeekData owData : data.mOneWeekData) {
                    if (owData.test_result)
                        right++;
                }

                mTestArea.setVisibility(View.VISIBLE);
                mTestTotalCnt.setText(getValue(total));
                mTestRightCnt.setText(getValue(right));
            }
        }

        deleteOggFile();

        if (ServiceCommon.IS_CONTENTS_TEST) {
            mIsSyncEnd = true;
            finish();
        } else {
            if (CommonUtil.isAvailableNetwork(this, true)) {
                mHandler.sendEmptyMessage(ServiceCommon.MSG_RECORDING_FILE_UPLOAD);
                Log.e(TAG, "UploadStart");
                //showProgressDialog(true);
            } else if (CommonUtil.isAvailableNetwork(this, false)) {
                mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
                //showProgressDialog(true);
            } else {
                mIsSyncEnd = true;
            }
        }

        String mcase = Preferences.getCaseStatus(mContext);
        if ("1".equals(mcase) || "2".equals(mcase) || "3".equals(mcase)) {//로그인 케이스1,2,3 경우
            RequestPhpPost reqCASE = new RequestPhpPost(mContext);
            reqCASE.sendCASE(String.valueOf(data.mStudyResultNo), "CASE" + mcase);
            
            //btn_moveList.setVisibility(View.INVISIBLE);
            btn_moveList.setVisibility(View.GONE);
        }

        setGuideMsg(R.raw.b_28);

        //이어하기 노출 case
        RequestPhpPost reqVCASE = new RequestPhpPost(mContext);
        reqVCASE.sendCASE(String.valueOf(data.mStudyResultNo), btn_moveList.getVisibility() == View.VISIBLE ? "YS1" : "YS2");

        //GA적용
        Crashlytics.log(getString(R.string.string_ga_StudyOutcomeActivity));
    }

    public boolean onTouch(View v, MotionEvent event) {
        // TODO Auto-generated method stub

        if (MotionEvent.ACTION_UP != event.getAction())
            return false;

        if (v.isFocusable() && v.isFocusableInTouchMode()) {
            if (v.isPressed() && !v.isFocused()) {
                v.performClick();
                return true;
            }
        }

        return false;
    }

    /**
     * 숫자 값을 문자 값으로 변환
     *
     * @param value 숫자 값
     * @return String 변환된 문자열
     */
    private String getValue(int value) {
        String rtnValue;

        try {
            rtnValue = String.valueOf(value);
        } catch (NullPointerException e) {
            e.printStackTrace();
            rtnValue = "0";
        } catch (IllegalFormatException e) {
            e.printStackTrace();
            rtnValue = "0";
        }

        return rtnValue;
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mIsSyncEnd)
                    getCurrentFocus().setPressed(true);
                    /*if (btn_moveFskill.isActivated()) {
                        btn_moveFskill.setPressed(true);

                    } else if (btn_moveList.isActivated()) {
                        btn_moveList.setPressed(true);
                    }*/
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mIsSyncEnd)
                    getCurrentFocus().setPressed(false);
                    /*if (btn_moveFskill.isActivated()) {
                        btn_moveFskill.setPressed(false);

                    } else if (btn_moveList.isActivated()) {
                        btn_moveList.setPressed(false);
                    }*/
                goNextStatus();
                break;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if (btn_moveList.getVisibility() == View.VISIBLE) {
                    //btn_moveList.setActivated(true);
                   // btn_moveFskill.setActivated(false);
                    btn_moveList.requestFocus();
                }

                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                if (btn_moveFskill.getVisibility() == View.VISIBLE) {
                   // btn_moveFskill.setActivated(true);
                   // btn_moveList.setActivated(false);
                    btn_moveFskill.requestFocus();
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        if (singleProcessChecker())
            return;

        int id = view.getId();
        if (id == R.id.btn_fskill) {
            //4스킬 클릭시 전송
            Crashlytics.log(getString(R.string.string_learning_4skill_befly));

            CommonUtil.startPackage(mContext, ServiceCommon.FOURSKILL_PACKAGE, Preferences.getForestCode(this), Preferences.getTeacherCode(this), Preferences.getCustomerNo(this),Preferences.getCenter(this));
            finish();

            if (mContext != null) {
                //4스킬 로그남기기
                RequestPhpPost reqFCASE = new RequestPhpPost(mContext);
                StudyData data = StudyData.getInstance();
                reqFCASE.sendCASE(String.valueOf(data.mStudyResultNo), "EX2");
            }

        } else if (id == R.id.btn_list) {
            StudyData.clear();

			/*
             * startActivityForResult(new Intent(this,
			 * StudyBookSelectActivity.class), ServiceCommon.MSG_STUDY_FINISH);
			 * finish();
			 */

            Intent mIntent = new Intent(this, NewHomeActivity.class);
            mIntent.putExtra("REFRESH", "TRUE");
            startActivity(mIntent);
            finish();

            if (mContext != null) {
                //목록가기 로그남기기
                RequestPhpPost reqFCASE = new RequestPhpPost(mContext);
                StudyData data = StudyData.getInstance();
                reqFCASE.sendCASE(String.valueOf(data.mStudyResultNo), "EX1");
            }

        }
    }

    private void goNextStatus() {
        if (btn_moveFskill.isActivated()) {
            CommonUtil.startPackage(mContext, ServiceCommon.FOURSKILL_PACKAGE, Preferences.getForestCode(this), Preferences.getTeacherCode(this), Preferences.getCustomerNo(this),Preferences.getCenter(this));
            finish();
        } else if (btn_moveList.isActivated()) {
            /*
			 * StudyData.clear(); startActivityForResult(new Intent(this,
			 * StudyBookSelectActivity.class), ServiceCommon.MSG_STUDY_FINISH);
			 */
            Intent mIntent = new Intent(this, NewHomeActivity.class);
            mIntent.putExtra("REFRESH", "TRUE");
            startActivity(mIntent);
            finish();
        }
    }

    public void deleteOggFile() {
        StudyData data = StudyData.getInstance();
        ContentsUtil.deleteOggFile(data.mSeriesNo, data.mBookNo, data.mStudyUnitNo);

        if (data.mIsReviewExist) {
            ContentsUtil.deleteOggFile(data.mReviewSeriesNo, data.mReviewBookNo, data.mReviewStudyUnitNo);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUESTCODE_UPLOAD == requestCode) {
            Log.i("", "resultCode:" + resultCode);
            if (RESULT_CANCELED == resultCode) {
                finish();
                return;
            }

            mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START);
        }
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case ServiceCommon.MSG_RECORDING_FILE_UPLOAD:
                    startActivityForResult(new Intent(mContext, UploadActivity.class), REQUESTCODE_UPLOAD);
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START:
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL:
                    Log.e(TAG, "SyncEnd " + msg.what);
                    showProgressDialog(false);
                    mIsSyncEnd = true;
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS:
                    //syncDownloadDatabaseTable();
                    mHandler.sendEmptyMessage(ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS);
                    break;

                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_FAIL:
                case ServiceCommon.MSG_DATABASE_DOWNLOAD_SYNC_SUCCESS:
                    Log.e(TAG, "SyncEnd " + msg.what);
                    showProgressDialog(false);
                    mIsSyncEnd = true;
                    break;
            }
            super.handleMessage(msg);
        }
    };

    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mHandler, true, true);
        } else {
            Log.e(TAG, "SyncEnd syncUploadDatabaseTable");
            //showProgressDialog(false);
        }
    }

	/*
	 * public void syncDownloadDatabaseTable() {
	 * if(CommonUtil.isAvailableNetwork(mContext, false)) { DatabaseSync
	 * databaseSync = new DatabaseSync(this);
	 * databaseSync.downloadSyncStart(mHandler,
	 * Preferences.getCustomerNo(this),info.mProductNo,info.mStudyUnitCode); }
	 * else { Log.e(TAG, "SyncEnd syncDownloadDatabaseTable");
	 * showProgressDialog(false); } }
	 */

    public void showProgressDialog(boolean isShow) {
        if (isShow) {
            mProgressDialog = LoadingDialog.show(this);
        } else {
            if (null != mProgressDialog && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        }
    }
}
