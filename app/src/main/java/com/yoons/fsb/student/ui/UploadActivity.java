package com.yoons.fsb.student.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.ErrorData;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.ui.base.BaseActivity;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.StorageUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 학습 중 녹취된 음원을 WAS에 전달함.(서버에 업로드 및 녹취 파일 백업 기능 수행)
 * 
 * @author dckim
 */
public class UploadActivity extends BaseActivity implements OnDialogDismissListener {

	private final String LOG_TAG = "[UploadActivity]";

	public static final int UPLOADTASK_MSG_ID = 128;

	/** 오류 없음 */
	public static final int ERR_NONE = 0;
	/** 알수 없는 오류 */
	public static final int ERR_UNKNOWN = -1;
	/** 업로드할 목록 없음 */
	public static final int ERR_EMPTY_LIST = -2;
	/** 사용가능 네트워크 없음 */
	public static final int ERR_NOT_AVAILABLE_NETWORKS = -3;
	/** 용량 초과 */
	public static final int ERR_UPLOAD_SIZE_OVERFLOW = -4; // WAS에서 50M(파일당) 용량제한이 설정되어 있습니다.
	/** 사용자 취소 */
	public static final int ERR_USER_CANCEL = -5;
	/** Zip 파일 생성공간 부족 */
	public static final int ERR_NOT_ENOUGH_SPACE = -6;

	/** 이하 네트워크 오류 HttpURLConnection.HTTP_OK ~ HttpURLConnection.HTTP_VERSION */

	private int mResult = ERR_EMPTY_LIST;
	private DatabaseUtil DbUtil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_empty);
		overridePendingTransition(0, 0);

		Log.d(LOG_TAG, "onCreate()");
		runUpload();
		Crashlytics.log(getString(R.string.string_ga_UploadActivity));

		DbUtil = DatabaseUtil.getInstance(this);
	}

	public Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (UPLOADTASK_MSG_ID == msg.what) {

				mResult = msg.arg1;
				finish();
			}

			super.handleMessage(msg);
		}
	};

	@Override
	public void finish() {
		setResult(mResult);
		super.finish();
		overridePendingTransition(0, 0);
	}

	public void onDialogDismiss(int result, int dialogId) {
		finish();
	}

	/**
	 * 학습 결과를 Server로 전송한다.
	 */
	public void runUpload() {

		Thread thread = new Thread("runUpload") {

			private final String CRLF = "\r\n";
			private final String PREFIX = "--";
			private final String SUFFIX = PREFIX;
			private final String BOUNDARY = "--------multipart-boundary--------";

			private String mName = null;
			private String mPassword = null;
			private ArrayList<File> mFileList = null;

			private final int CONN_TIMEOUT = 10 * 1000;
			private final int READ_TIMEOUT = 10 * 1000;
			private final int BUFFER_SIZE = 1024 * 2;
			private final long UPLOAD_LIMIT_SIZE = 52428800; // 50MB(1024 * 1024 * 50)

			private boolean mNotEnoughSpace = false;
			private boolean mExceedUploadSize = false;

			@Override
			public void run() {
				onResult(processing());
			};

			/**
			 * 주요 기능 처리
			 */
			protected int processing() {

				int result = ERR_NONE;

				if (null == mContext) // 오류가 아니므로 항목이 없는 것처럼 처리한다.
					return ERR_EMPTY_LIST;

				deleteZipFiles();

				mFileList = getUploadRes();

				if (mNotEnoughSpace) {
					deleteZipFiles();
					return ERR_NOT_ENOUGH_SPACE;
				}

				if (mExceedUploadSize) {
					deleteZipFiles();
					return ERR_UPLOAD_SIZE_OVERFLOW;
				}

				if (0 >= mFileList.size())
					return ERR_EMPTY_LIST;

				//mergeFiles();

				//audioUpload();

				mFileList = getUploadRes();

				if (!CommonUtil.isAvailableNetwork(UploadActivity.this) || ServiceCommon.IS_EXPERIENCES)
					return ERR_NOT_AVAILABLE_NETWORKS;

				try {
					DataOutputStream dos = null;
					if (ServiceCommon.STUDY_SERVER_URL.length() == 0) {
						ServiceCommon.setServerUrl(mContext, "");
					}

					URL connectURL = new URL(ServiceCommon.UPLOAD_SERVER_URL);

					HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
					conn.setConnectTimeout(CONN_TIMEOUT);
					conn.setReadTimeout(READ_TIMEOUT);
					conn.setDoInput(true);
					conn.setDoOutput(true);
					conn.setUseCaches(false);
					conn.setRequestMethod("POST");

					conn.setRequestProperty("Connection", "Keep-Alive");
					conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
					conn.connect();

					dos = new DataOutputStream(conn.getOutputStream());

					if (null != mName)
						writeFormField(dos, "login", mName);
					if (null != mPassword)
						writeFormField(dos, "password", mPassword);

					for (File f : mFileList) {
						FileInputStream fis = null;

						String ext = CommonUtil.getFileExt(f.getAbsolutePath());
						if (null == ext || 0 >= ext.length())
							continue;

						File newFile = (ext.equalsIgnoreCase("wav")) ? new File(f.getAbsolutePath().replace(".wav", ".zip")) : f;
						if (!newFile.exists())
							continue;

						try {
							fis = new FileInputStream(newFile);
						} catch (Exception e) {
							e.printStackTrace();
						}

						if (null == fis)
							continue;

						writeFileField(dos, "userfile[]", newFile.getName(), "application/octet-stream", fis);

						fis.close();
					}

					// final closing boundary line
					dos.writeBytes(PREFIX + BOUNDARY + SUFFIX + CRLF);

					dos.flush();
					dos.close();
					dos = null;

					result = conn.getResponseCode();

					InputStream is = conn.getInputStream();

					// retrieve the response from server
					int ch;

					StringBuffer b = new StringBuffer();
					while ((ch = is.read()) != -1) {
						b.append((char) ch);
					}
					String s = b.toString();
					Log.e(LOG_TAG, s);

					if (HttpURLConnection.HTTP_OK == result) {
						if (null != DbUtil) {

							deleteZipFiles();

							for (File f : mFileList)
								DbUtil.updateRecordFileUpload(f.getName());
						}
					}
				} catch (MalformedURLException mue) {
					mue.printStackTrace();
					Log.e(LOG_TAG, "error: " + mue.getMessage());
					result = HttpURLConnection.HTTP_BAD_REQUEST;
					ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_UPLOAD, mue);
				} catch (IOException ioe) {
					Log.e(LOG_TAG, "error: " + ioe.getMessage());
					result = HttpURLConnection.HTTP_SERVER_ERROR;
					ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_UPLOAD, ioe);
				} catch (Exception e) {
					Log.e(LOG_TAG, "error: " + e.getMessage());
					result = ERR_UNKNOWN;
					ErrorData.setErrorMsg(ErrorData.ERROR_HTTP_FILE_UPLOAD, e);
				}

				return result;
			}

			/**
			 * 처리 상태에 따라 결과 반영
			 * 
			 * @param result
			 *            처리 결과값
			 */
			protected void onResult(int result) {

				Log.i(LOG_TAG, "end Upload responseCode:" + result);

				if (null != mFileList && 0 < mFileList.size()) {

					if (HttpURLConnection.HTTP_OK == result) {

						for (File f : mFileList) {
							String nf = "";

							if (f.getName().contains("SA") || f.getName().contains("RA")) {
								nf = String.format("%s%s/%s", ServiceCommon.AUDIO_RECORD_PATH, f.getName().substring(0, 8), f.getName());
							} else {
								nf = String.format("%s%s/%s", ServiceCommon.EXAM_RECORD_PATH, f.getName().substring(0, 8), f.getName());
							}

							try {
								fileMove(new File(ServiceCommon.EXAM_RECORD_UPLOAD_PATH + f.getName()), new File(nf));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					mFileList.clear();
					mFileList = null;
				}

				mHandler.sendMessage(mHandler.obtainMessage(UPLOADTASK_MSG_ID, result, 0));
			}

			/**
			 * 원복 백업(원래 위치에는 녹취 정보가 없도록 함)
			 * 
			 * @param fOri
			 *            원본 파일 정보
			 * @param fTar
			 *            복사본 파일 정보
			 * @throws IOException
			 */
			protected void fileMove(File fOri, File fTar) throws IOException {

				if (!fTar.isFile()) {

					File fParent = new File(fTar.getParent());
					if (!fParent.exists())
						fParent.mkdir();
				}

				if (fOri.exists() && fTar.exists()) {
					if (!fOri.getAbsolutePath().equals(fTar.getAbsolutePath()))
						fTar.delete();
				}

				if (fOri.renameTo(fTar)) { // move
					return;
				}

				try { // delete after copy
					copy(fOri, fTar);
					if (fOri.exists())
						fOri.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			/**
			 * 파일 복사
			 * 
			 * @param fOrg
			 *            원본 파일 정보
			 * @param fTar
			 *            복사본 파일 정보
			 * @throws IOException
			 */
			protected void copy(File fOrg, File fTar) throws IOException {

				FileInputStream inputStream = new FileInputStream(fOrg);

				if (!fTar.isFile()) {
					File fParent = new File(fTar.getParent());
					if (!fParent.exists())
						fParent.mkdir();

					try {
						fTar.createNewFile();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				FileOutputStream outputStream = new FileOutputStream(fTar);
				FileChannel fcin = inputStream.getChannel();
				FileChannel fcout = outputStream.getChannel();

				long size = fcin.size();
				fcin.transferTo(0, size, fcout);

				fcout.close();
				fcin.close();
				outputStream.close();
				inputStream.close();
			}
			/*
			 * protected void cancel() {
			 * 
			 * if (!isCancelled()) { super.cancel(true); }
			 * 
			 * if (null != mFileList && 0 < mFileList.size()) {
			 * mFileList.clear(); mFileList = null; }
			 * 
			 * Log.e(LOG_TAG, "end Upload");
			 * mHandler.sendMessage(mHandler.obtainMessage(UPLOADTASK_MSG_ID,
			 * ERR_USER_CANCEL, 0)); }
			 */

			public void setAccount(String name, String password) {
				this.mName = name;
				this.mPassword = password;
			}

			/**
			 * write one form field to dataSream
			 * 
			 * @param fieldName
			 * @param fieldValue
			 */
			private void writeFormField(DataOutputStream dos, String fieldName, String fieldValue) {

				try {
					dos.writeBytes(String.format("%s%s%s", PREFIX, BOUNDARY, CRLF));
					dos.writeBytes(String.format("Content-Disposition: form-data; name=\"%s\"%s", fieldName, CRLF));
					dos.writeBytes(String.format("%s%s%s", CRLF, fieldValue, CRLF));
				} catch (Exception e) {
					Log.e(LOG_TAG, "Uploader.writeFormField: " + e.getMessage());
				}
			}

			/**
			 * write one file field to dataSream
			 * 
			 * @param fieldName
			 *            - name of file field
			 * @param fieldValue
			 *            - file name
			 * @param type
			 *            - mime type
			 * @param fis
			 *            - stream of bytes that get sent up
			 */
			private void writeFileField(DataOutputStream dos, String fieldName, String fieldValue, String type, FileInputStream fis) {

				try {
					// opening boundary line
					//Log.e(getClass().getSimpleName(), String.format("%s%s%s", PREFIX, BOUNDARY, CRLF));
					//Log.e(getClass().getSimpleName(), String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"%s", fieldName, fieldValue, CRLF));
					//Log.e(getClass().getSimpleName(), String.format("Content-Type: %s%s%s", type, CRLF, CRLF));
					dos.writeBytes(String.format("%s%s%s", PREFIX, BOUNDARY, CRLF));
					dos.writeBytes(String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"%s", fieldName, fieldValue, CRLF));
					dos.writeBytes(String.format("Content-Type: %s%s%s", type, CRLF, CRLF));

					// create a buffer of maximum size 
					int maxBufferSize = 1024;
					int bufferSize = Math.min(fis.available(), maxBufferSize);
					byte[] buffer = new byte[bufferSize];

					// read file and write it into form... 
					int bytesRead = fis.read(buffer, 0, bufferSize);
					while (bytesRead > 0) {
						dos.write(buffer, 0, bytesRead);

						//publishProgress(bytesRead);

						bufferSize = Math.min(fis.available(), maxBufferSize);
						bytesRead = fis.read(buffer, 0, bufferSize);
					}

					// closing CRLF
					dos.writeBytes(CRLF);
				} catch (Exception e) {
					Log.e(LOG_TAG, "Uploader.writeFormField: got: " + e.getMessage());
				}
			}

			/**
			 * 지정 위치(srcPath) 리소스를 Zip 파일로 생성한다.
			 * 
			 * @param srcPath
			 *            - 압축 대상
			 * @param output
			 *            - 저장 zip 파일 이름
			 * @return 정상실행 시 TRUE 반환
			 */
			public boolean makeZip(String srcPath, String output) {

				boolean res = false;

				// 압축 대상(sourcePath)이 디렉토리나 파일이 아니면 리턴한다.
				File srcFile = new File(srcPath);
				if (!srcFile.isFile() && !srcFile.isDirectory()) {
					return res;
				}

				FileOutputStream fos = null;
				BufferedOutputStream bos = null;
				ZipOutputStream zos = null;

				try {
					fos = new FileOutputStream(output);
					bos = new BufferedOutputStream(fos);
					zos = new ZipOutputStream(bos);
					zos.setLevel(Deflater.BEST_COMPRESSION);
					zipEntry(srcFile, srcPath, zos);
					zos.finish();
					res = true;
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if (zos != null)
							zos.close();

						if (bos != null)
							bos.close();

						if (fos != null)
							fos.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				return res;
			}

			/**
			 * 압축
			 * 
			 * @param srcFile
			 *            원본 파일
			 * @param srcPath
			 *            경로 정보
			 * @param zos
			 *            ZipOutputStream
			 * @throws Exception
			 */
			private void zipEntry(File srcFile, String srcPath, ZipOutputStream zos) throws Exception {
				// sourceFile 이 디렉토리인 경우 하위 파일 리스트 가져와 재귀호출
				if (srcFile.isDirectory()) {
					if (srcFile.getName().equalsIgnoreCase(".metadata")) // .metadata 디렉토리
						return;

					File[] fileArray = srcFile.listFiles(); // sourceFile 의 하위 파일 리스트

					for (int i = 0; i < fileArray.length; i++)
						zipEntry(fileArray[i], srcPath, zos); // 재귀 호출

					return;
				}

				BufferedInputStream bis = null;
				try {
					String zipEntryName = srcPath.substring(srcFile.getParent().length() + 1);

					bis = new BufferedInputStream(new FileInputStream(srcFile));
					ZipEntry zentry = new ZipEntry(zipEntryName);
					zentry.setTime(srcFile.lastModified());
					zos.putNextEntry(zentry);

					byte[] buffer = new byte[BUFFER_SIZE];
					int cnt = 0;
					while ((cnt = bis.read(buffer, 0, BUFFER_SIZE)) != -1)
						zos.write(buffer, 0, cnt);

					zos.closeEntry();
				} finally {
					if (bis != null)
						bis.close();
				}
			}

			/**
			 * 업로드 파일 목록을 반환한다.
			 * 
			 * @return ArrayList<File> 업로드할 파일 목록(파일이고 크기가 있는것)
			 */
			public ArrayList<File> getUploadRes() {

				ArrayList<File> list = new ArrayList<File>();

				if (!StorageUtil.getStorageState(StorageUtil.INTERNAL_STORAGE))
					return list;

				long freeSpace = StorageUtil.getStorageFreeSpace(ServiceCommon.EXAM_RECORD_UPLOAD_PATH);
				File f = new File(ServiceCommon.EXAM_RECORD_UPLOAD_PATH);
				if (!f.exists())
					return list;

				File[] arrFile = f.listFiles();
				if (null == arrFile)
					return list;

				long size = 0;

				for (File file : arrFile) {

					if (freeSpace < file.length()) {
						mNotEnoughSpace = true;
						list.clear(); // 일부 항목만을 전송하지 않도록 삭제함. 
						return list;
					}

					if (!validExtention(file))
						continue;

					// WAS에서 50MB 제한을 두고 있음(초과하지 않을 것으로 판단함)
					String ext = CommonUtil.getFileExt(file.getAbsolutePath());
					if (null == ext || 0 >= ext.length())
						continue;

					if (ext.equalsIgnoreCase("wav")) {

						String newNm = file.getAbsolutePath().replace(".wav", ".zip");
						makeZip(file.getAbsolutePath(), newNm);
						File newfile = new File(newNm);
						if (null != newfile && newfile.exists()) {

							long len = newfile.length();
							if (0 < len) {
								size += len;

								if (UPLOAD_LIMIT_SIZE >= size) {
									list.add(file);
								} else {
									mExceedUploadSize = true;
									list.clear(); // 일부 항목만을 전송하지 않도록 삭제함. 
									return list;
								}
							}
						}
					} else {

						long len = file.length();
						if (0 < len) {
							size += len;

							if (UPLOAD_LIMIT_SIZE >= size) {
								list.add(file);
							} else {
								mExceedUploadSize = true;
								list.clear(); // 일부 항목만을 전송하지 않도록 삭제함. 
								return list;
							}
						}
					}
				}

				return list;
			}

			public boolean validExtention(File f) {
				String name = f.getName();

				if (!f.isFile())
					return false;

				if (0 >= f.length())
					return false;

				if (name.toLowerCase().contains(".wav") || name.toLowerCase().contains(".mp3"))
					return true;

				return false;
			}

			public void deleteZipFiles() {
				File f = new File(ServiceCommon.EXAM_RECORD_UPLOAD_PATH);
				if (!f.exists())
					return;

				File[] arrFile = f.listFiles();
				if (null == arrFile)
					return;

				for (File file : arrFile) {
					String name = file.getName();

					if (name.contains(".zip")) {
						file.delete();
					}
				}
			}
		};
		thread.start();
	}

	/**
	 * 오디오 학습 업로드
	 *//*
		 * private void audioUpload() {
		 * 
		 * ArrayList<RecData> list = DbUtil.selectRecListAll();
		 * 
		 * for (RecData data : list) { try { String url =
		 * "http://localhost:8080/web/Uploader";
		 * 
		 * // 데이터 구분문자. 아무거나 정해도 상관없지만 꼭 나타날 수 없는 형태의 문자열로 정한다. String boundary
		 * = "^******^";
		 * 
		 * // 데이터 경계선 String delimiter = "\r\n--" + boundary + "\r\n";
		 * 
		 * StringBuffer postDataBuilder = new StringBuffer();
		 * 
		 * // 추가하고 싶은 Key & Value 추가 // key & value를 추가한 후 꼭 경계선을 삽입해줘야 데이터를 구분할
		 * 수 있다. postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("MCODE",
		 * String.valueOf(data.customer_no)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("BOOK_ID",
		 * String.valueOf(data.product_no))); postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("BOOK_DETAIL_ID",
		 * String.valueOf(data.book_detail_id)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("CLASS_ID",
		 * String.valueOf(data.study_result_no)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("STUDENT_BOOK_ID",
		 * String.valueOf(data.student_book_id)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("REC_TYPE",
		 * String.valueOf(data.rec_type))); postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("REC_POS_S",
		 * String.valueOf(data.start_tag_time)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("REC_POS_E",
		 * String.valueOf(data.end_tag_time)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("REC_LEN",
		 * String.valueOf(data.book_detail_id)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("REP_CNT",
		 * String.valueOf(data.re_study_cnt)));
		 * postDataBuilder.append(delimiter);
		 * postDataBuilder.append(setValue("REP_XTH",
		 * String.valueOf(data.re_study_cnt)));
		 * postDataBuilder.append(delimiter);
		 * 
		 * // 파일 첨부
		 * 
		 * postDataBuilder.append(setFile("uploadedFile", "temp.jpg"));
		 * postDataBuilder.append("\r\n");
		 * 
		 * 
		 * // 커넥션 생성 및 설정 HttpURLConnection conn = (HttpURLConnection) new
		 * URL(url).openConnection(); conn.setDoInput(true);
		 * conn.setDoOutput(true); conn.setUseCaches(false);
		 * conn.setRequestMethod("POST"); conn.setRequestProperty("Connection",
		 * "Keep-Alive"); conn.setRequestProperty("Content-Type",
		 * "multipart/form-data;boundary=" + boundary);
		 * 
		 * // 전송 작업 시작 FileInputStream in;
		 * 
		 * in = new FileInputStream(data.file_path);
		 * 
		 * DataOutputStream out = new DataOutputStream(new
		 * BufferedOutputStream(conn.getOutputStream()));
		 * 
		 * // 위에서 작성한 메타데이터를 먼저 전송한다. (한글이 포함되어 있으므로 UTF-8 메소드 사용)
		 * out.writeUTF(postDataBuilder.toString());
		 * 
		 * // 파일 복사 작업 시작 int maxBufferSize = 1024; int bufferSize =
		 * Math.min(in.available(), maxBufferSize); byte[] buffer = new
		 * byte[bufferSize];
		 * 
		 * // 버퍼 크기만큼 파일로부터 바이트 데이터를 읽는다. int byteRead = in.read(buffer, 0,
		 * bufferSize);
		 * 
		 * // 전송 while (byteRead > 0) { out.write(buffer); bufferSize =
		 * Math.min(in.available(), maxBufferSize); byteRead = in.read(buffer,
		 * 0, bufferSize); }
		 * 
		 * out.writeBytes(delimiter); // 반드시 작성해야 한다. out.flush(); out.close();
		 * in.close();
		 * 
		 * // 결과 반환 (HTTP RES CODE) conn.getInputStream(); conn.disconnect(); }
		 * catch (FileNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } }
		 * 
		 * }
		 */

	/**
	 * Map 형식으로 Key와 Value를 셋팅한다.
	 * 
	 * @param key
	 *            : 서버에서 사용할 변수명
	 * @param value
	 *            : 변수명에 해당하는 실제 값
	 * @return
	 */
	private String setValue(String key, String value) {
		return "Content-Disposition: form-data; name=\"" + key + "\"r\n\r\n" + value;
	}

	/**
	 * 업로드할 파일에 대한 메타 데이터를 설정한다.
	 * 
	 * @param key
	 *            : 서버에서 사용할 파일 변수명
	 * @param fileName
	 *            : 서버에서 저장될 파일명
	 * @return
	 */
	private String setFile(String key, String fileName) {
		return "Content-Disposition: form-data; name=\"" + key + "\";filename=\"" + fileName + "\"\r\n";
	}
}
