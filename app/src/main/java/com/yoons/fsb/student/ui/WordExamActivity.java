package com.yoons.fsb.student.ui;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.base.BaseStudyActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.FlipAnimation;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.vanishing.VanishingTitlePaperActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * 단어시험 화면
 *
 * @author jaek
 */
public class WordExamActivity extends BaseStudyActivity implements OnClickListener, OnCompletionListener, OnSeekCompleteListener {

    private LinearLayout mAnswer1 = null, mAnswer2 = null, mAnswer3 = null, mAnswer4 = null, normal_quest = null, high_quest = null, wrong_layout = null;
    private TextView mStudyView, mStudyCountView, mResultView, mResultView2, mResultTime = null;
    private TextView mWrongStudyView = null, mWrongStudyCountView = null, mWrongStudyAnswerView = null;
    private Button mResultConfirm = null, mNextStatus = null;
    //private StepStatusBar mStepStatusBar = null;
    private ArrayList<ImageView> mTimerViewList = null;
    private Drawable mTimerBuble = null;
    private Drawable mTimerBubleDis = null;
    private boolean mWrongStudy = false;
    //	private boolean mOnclick=true;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;

    // 타이머
    private long startTime = 0L;
    long timeInMilliseconds = 0L;
    long updatedTime = 0L;

    private boolean mTime = true;

    private int[] short_linear = {R.id.word_study_answer_1, R.id.word_study_answer_2, R.id.word_study_answer_3, R.id.word_study_answer_4};
    private int[] long_linear = {R.id.word_study_list_answer_1, R.id.word_study_list_answer_2, R.id.word_study_list_answer_3, R.id.word_study_list_answer_4};

    private int[] short_answer = {R.id.word_answer_1, R.id.word_answer_2, R.id.word_answer_3, R.id.word_answer_4};
    private int[] long_answer = {R.id.list_answer_1, R.id.list_answer_2, R.id.list_answer_3, R.id.list_answer_4};

    private ArrayList<LinearLayout> mAnswerList = null;

    private Drawable answer_on = null;
    private int btn_learning_answer_num_on, btn_learning_answer_num_off = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_word_exam_main);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                setContentView(R.layout.w_word_exam_main);
            else // 숲
                setContentView(R.layout.f_word_exam_main);
        }

        mStudyData = StudyData.getInstance();

        if (!checkValidStudyData())
            return;

        setWidget();

        // TitleView
        tBookName.setText(mStudyData.mProductName);
        tCategory.setText(R.string.string_word_restudy);
        tCategory2.setText(R.string.string_common_word_title);
        ((TextView) findViewById(R.id.step_status_text)).setText(R.string.string_common_word_title);

        if (ServiceCommon.IS_CONTENTS_TEST) {
            readyStudy();
        } else {
            requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);
        }
        // GA적용
        Crashlytics.log(getString(R.string.string_ga_WordExamActivity));

        // requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);

        // StudyDataUtil.setCurrentStudyStatus(this, "S31");
        // mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
        // ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startTime = SystemClock.uptimeMillis();
    }

    @Override
    protected void onPause() {
        // 학습 중 화면 잠금, 꺼짐 시 간지로 재시작
        if (!isFinishing()) {
            if (mIsReStart) {
                StudyDataUtil.clearWordResult();
                startActivity(new Intent(this, WordTitlePaperActivity.class).putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
            }

            setFinish();
            finish();
        }

        super.onPause();
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer1 && mAnswer1.isEnabled()) {
                        mAnswer1.setPressed(true);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer2 && mAnswer2.isEnabled())
                        mAnswer2.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer3 && mAnswer3.isEnabled()) {
                        mAnswer3.setPressed(true);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer4 && mAnswer4.isEnabled())
                        mAnswer4.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.word_study_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(true);
                } else {
                    if (View.VISIBLE == mNextStatus.getVisibility())
                        mNextStatus.setPressed(true);
                }
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer1 && mAnswer1.isEnabled()) {
                        mAnswer1.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_1);
                        //nextStudy();
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer2 && mAnswer2.isEnabled()) {
                        mAnswer2.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_2);
                        //nextStudy();
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer3 && mAnswer3.isEnabled()) {
                        mAnswer3.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_3);
                        //nextStudy();
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.word_study_ing_layout).getVisibility()) {
                    if (null != mAnswer4 && mAnswer4.isEnabled()) {
                        mAnswer4.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_4);
                        //nextStudy();
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.word_study_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(false);
                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        goNextStatus();
                    } else {
                        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                    }
                } else {
                    if (View.VISIBLE == mNextStatus.getVisibility()) {
                        mNextStatus.setPressed(false);
                        if (ServiceCommon.IS_CONTENTS_TEST) {
                            goNextStatus();
                        } else {
                            requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                        }
                    }
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        if (!mIsStudyStart)
            return;

        if (singleProcessChecker())
            return;

        int id = v.getId();
        switch (v.getId()) {
            case R.id.word_study_list_answer_1:
            case R.id.word_study_list_answer_2:
            case R.id.word_study_list_answer_3:
            case R.id.word_study_list_answer_4:
            case R.id.word_study_answer_1:
            case R.id.word_study_answer_2:
            case R.id.word_study_answer_3:
            case R.id.word_study_answer_4:
                int answer = (Integer) v.getTag();
                setWordResult(answer);
                //nextStudy();
                break;

            case R.id.word_study_result_confirm_btn:
            case R.id.word_study_wrong_next_status_btn:
                //			if (mOnclick) {
                //				mOnclick = false;
                if (ServiceCommon.IS_CONTENTS_TEST) {
                    goNextStatus();
                } else {
                    requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                }
                //			}
                break;
        }

    }

    /**
     * StudyData의 유효성을 검사함
     */
    private boolean checkValidStudyData() {
        boolean isValid = true;

        if (null == mStudyData) {
            isValid = false;
            Log.e("", "checkValidStudyData studyData is null !!");
        } else {
            if (mStudyData.mWordQuestion.isEmpty()) {
                isValid = false;
                Log.e("", "checkValidStudyData mWordQuestion is empty !!");
            } else {
                for (int i = 0; i < mStudyData.mWordQuestion.size(); i++) {
                    StudyData.WordQuestion wq = mStudyData.mWordQuestion.get(i);
                    /*if (!CommonUtil.isKorean(wq.mQuestion)) {*/
                    File fileYda = new File(wq.mSoundFile);
                    if (0 >= wq.mQuestion.length() || !fileYda.exists()) {
                        isValid = false;
                        break;
                    }
					/*} else {
						wq.mSoundFile = "";
					}*/
                }
            }
        }

        if (!isValid)
            Toast.makeText(this, getString(R.string.string_common_study_data_error), Toast.LENGTH_SHORT).show();

        return isValid;
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        if (CommonUtil.isCenter()) { // Igse
            answer_on = ContextCompat.getDrawable(getApplicationContext(), R.drawable.igse_btn_learning_answer_bg_on);
            btn_learning_answer_num_on = R.drawable.igse_btn_learning_answer_num_on;
            btn_learning_answer_num_off = R.drawable.igse_btn_learning_answer_num_off;
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) { //우영
                answer_on = ContextCompat.getDrawable(getApplicationContext(), R.drawable.w_btn_learning_answer_bg_on);
                btn_learning_answer_num_on = R.drawable.w_btn_learning_answer_num_on;
                btn_learning_answer_num_off = R.drawable.w_btn_learning_answer_num_off;
            } else {// 숲
                answer_on = ContextCompat.getDrawable(getApplicationContext(), R.drawable.f_btn_learning_answer_bg_on);
                btn_learning_answer_num_on = R.drawable.f_btn_learning_answer_num_on;
                btn_learning_answer_num_off = R.drawable.f_btn_learning_answer_num_off;
            }
        }

        wrong_layout = (LinearLayout) findViewById(R.id.word_study_wrong_question_layout);

        normal_quest = (LinearLayout) findViewById(R.id.word_study_normallevel_question_layout);
        high_quest = (LinearLayout) findViewById(R.id.word_study_highlevel_question_layout);

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        mStudyView = (TextView) findViewById(R.id.word_study_question_text);
        mStudyCountView = (TextView) findViewById(R.id.word_study_question_count_text);

        if (CommonUtil.isCenter()) // igse
            mTimerBuble = getResources().getDrawable(R.drawable.igse_img_learning_timer_on);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mTimerBuble = getResources().getDrawable(R.drawable.w_img_learning_timer_on);
            else // 숲
                mTimerBuble = getResources().getDrawable(R.drawable.f_img_learning_timer_on);
        }

        //mTimerBuble = getResources().getDrawable(R.drawable.timer_buble);
        //mTimerBubleDis = getResources().getDrawable(R.drawable.timer_buble_dis);
        mTimerBubleDis = getResources().getDrawable(R.drawable.c_img_learning_timer_off);

        //mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);

        mResultView = (TextView) findViewById(R.id.word_study_result_wrong_count_text);
        mResultView2 = (TextView) findViewById(R.id.word_study_result_wrong_count_text2);

        mResultTime = (TextView) findViewById(R.id.sentence_study_result_time_text);

        mResultConfirm = (Button) findViewById(R.id.word_study_result_confirm_btn);

        mWrongStudyView = (TextView) findViewById(R.id.word_study_wrong_question_text);
        mWrongStudyCountView = (TextView) findViewById(R.id.word_study_wrong_question_count_text);
        mWrongStudyAnswerView = (TextView) findViewById(R.id.word_study_wrong_correct_answer_text);
        mNextStatus = (Button) findViewById(R.id.word_study_wrong_next_status_btn);

        mResultConfirm.setOnClickListener(this);
        mNextStatus.setOnClickListener(this);

        mTimerViewList = new ArrayList<ImageView>();
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_1));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_2));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_3));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_4));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_5));

        mTotalStudyCount = mStudyData.mWordQuestion.size();

        setTitlebarCategory(getString(R.string.string_titlebar_category_study));
        setTitlebarText(mStudyData.mProductName);
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        //mStepStatusBar.setHighlights(StepStatusBar.STATUS_WORD_EXAM);

        high_quest.setVisibility(View.GONE);
        normal_quest.setVisibility(View.GONE);
    }

    /**
     * Player를 설정함
     *
     * @param mp3FilePath 재생 파일 경로
     */
    private void setPlayer(String mp3FilePath) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            if (!mp3FilePath.equals("")) {
                mMediaPlayer.setDataSource(mp3FilePath);
            } else {
                AssetFileDescriptor afd = getAssets().openFd("mute.mp3");
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            }

            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.prepare();
            if (!mWrongStudy) {
                if ((mStudyData.mStudyUnitCode.contains("R") && mStudyData.mStudyUnitCode.length() < 6) || CommonUtil.isKorean(mStudyData.mWordQuestion.get(mCurStudyIndex).mQuestion) || CommonUtil.isNum(mStudyData.mWordQuestion.get(mCurStudyIndex).mQuestion)) {
                    mMediaPlayer.setVolume(0, 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // --- 단어 시험 --

    /**
     * 단어 문제 전환용 Flip Animation Listener
     */
    private AnimationListener studyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            findViewById(R.id.word_study_count_time).setVisibility(View.VISIBLE);
            setStudy(false);
            startAnswerAni(mAnswer1);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 단어 문제의 보기 항목 전환용 Flip Animation Listener
     */
    private AnimationListener answerAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            nextAnswerAni();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 단어 학습 문제 전환용 애니메이션을 시작함
     */
    private void startStudyAni() {
        findViewById(R.id.word_study_count_time).setVisibility(View.GONE);
        Animation ani = new FlipAnimation(180f, 0f, normal_quest.getWidth() / 2, normal_quest.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(studyAniListener);
        normal_quest.startAnimation(ani);
    }

    /**
     * 단어 학습 문제의 보기 항목 전환용 애니메이션을 시작함
     *
     * @param view 보기 항목 View
     */
    private void startAnswerAni(LinearLayout view) {
        Animation ani = new FlipAnimation(180f, 0f, mAnswer1.getWidth() / 2, mAnswer1.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(answerAniListener);
        view.startAnimation(ani);
    }

    /**
     * 다음 문제의 보기 항목을 애니메이션과 함께 표시함 모든 보기 항목의 애니메이션이 끝난 경우 해당 문제를 재생함
     */
    private void nextAnswerAni() {
        boolean isListType = isListAnswer();
        ArrayList<String> distractorList = mStudyData.mWordQuestion.get(mCurStudyIndex).mDistractorList;
        mCurAnswerAniCount++;

        if (mCurAnswerAniCount == 2) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer1_text)).setText(distractorList.get(0));
            else
                ((TextView) findViewById(R.id.word_study_answer1_text)).setText(distractorList.get(0));

            startAnswerAni(mAnswer2);
        } else if (mCurAnswerAniCount == 3) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer2_text)).setText(distractorList.get(1));
            else
                ((TextView) findViewById(R.id.word_study_answer2_text)).setText(distractorList.get(1));

            startAnswerAni(mAnswer3);
        } else if (mCurAnswerAniCount == 4) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer3_text)).setText(distractorList.get(2));
            else
                ((TextView) findViewById(R.id.word_study_answer3_text)).setText(distractorList.get(2));

            startAnswerAni(mAnswer4);
        } else {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer4_text)).setText(distractorList.get(3));
            else
                ((TextView) findViewById(R.id.word_study_answer4_text)).setText(distractorList.get(3));

            mAnswer1.setEnabled(true);
            mAnswer2.setEnabled(true);
            mAnswer3.setEnabled(true);
            mAnswer4.setEnabled(true);

            mCurPlayCount = 1;
            mCurAnswerAniCount = 1;
            mIsTimerReady = true;
            if (mMediaPlayer != null)
                mMediaPlayer.start();
        }
    }

    /**
     * 단어 학습을 시작함 첫번재 문제는 애니메이션 없이 표시함
     *
     * @param filePath 재생 파일 경로
     */
    private void startStudy(String filePath) {
        mStudyView.setText("");

        initAnswers();

        setPlayer(filePath);

        if (0 < mCurStudyIndex) {
            if (mStudyData.mIsCaption) {
                startStudyAni();
            } else {
                setStudy(false);
                startAnswerAni(mAnswer1);
            }
        } else {
            setStudy(true);
            mCurPlayCount = 1;
            mIsTimerReady = true;
            mMediaPlayer.start();
            if (!mIsStudyStart) {
                runTimerThread();
                mIsStudyStart = true;
            }
        }
    }

    /**
     * 단어 문제를 설정함
     *
     * @param isShowAnswer 보기 설정 여부
     */
    private void setStudy(boolean isShowAnswer) {
        if (mStudyData.mIsCaption || CommonUtil.isKorean(mStudyData.mWordQuestion.get(mCurStudyIndex).mQuestion) || CommonUtil.isNum(mStudyData.mWordQuestion.get(mCurStudyIndex).mQuestion)) {//하모드 이거나 한글일때
            high_quest.setVisibility(View.GONE);
            normal_quest.setVisibility(View.VISIBLE);
            mStudyView.setText(mStudyData.mWordQuestion.get(mCurStudyIndex).mQuestion.replaceAll("`", "'"));
        } else {//상모드
            high_quest.setVisibility(View.VISIBLE);
            normal_quest.setVisibility(View.GONE);
        }
        mStudyCountView.setText(mCurStudyIndex + 1 + "/" + mTotalStudyCount);

        // 보기 설정
        if (isShowAnswer)
            setAnswers();
    }

    /**
     * 보기 항목의 표시 유형을 판단하여 반환함
     *
     * @return 보기 항목의 표시 유형(Grid / List)
     */
    private boolean isListAnswer() {
        ArrayList<String> distractorList = mStudyData.mWordQuestion.get(mCurStudyIndex).mDistractorList;

        boolean isListType = false;
        for (int i = 0; i < distractorList.size(); i++) {
            if (ServiceCommon.MAX_WORD_LENGTH < distractorList.get(i).length())
                isListType = true;
        }

        return isListType;
    }

    /**
     * 보기 항목의 View를 초기화함
     */
    private void initAnswers() {
        boolean isListType = isListAnswer();
        findViewById(R.id.word_study_lattice_word_select_layout).setVisibility(isListType ? View.GONE : View.VISIBLE);
        findViewById(R.id.word_study_list_word_select_layout).setVisibility(isListType ? View.VISIBLE : View.GONE);

        if (isListType) {
            mAnswer1 = (LinearLayout) findViewById(R.id.word_study_list_answer_1);
            mAnswer2 = (LinearLayout) findViewById(R.id.word_study_list_answer_2);
            mAnswer3 = (LinearLayout) findViewById(R.id.word_study_list_answer_3);
            mAnswer4 = (LinearLayout) findViewById(R.id.word_study_list_answer_4);

            ((TextView) findViewById(R.id.word_study_list_answer1_text)).setText("");
            ((TextView) findViewById(R.id.word_study_list_answer2_text)).setText("");
            ((TextView) findViewById(R.id.word_study_list_answer3_text)).setText("");
            ((TextView) findViewById(R.id.word_study_list_answer4_text)).setText("");
        } else {
            mAnswer1 = (LinearLayout) findViewById(R.id.word_study_answer_1);
            mAnswer2 = (LinearLayout) findViewById(R.id.word_study_answer_2);
            mAnswer3 = (LinearLayout) findViewById(R.id.word_study_answer_3);
            mAnswer4 = (LinearLayout) findViewById(R.id.word_study_answer_4);

            ((TextView) findViewById(R.id.word_study_answer1_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer2_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer3_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer4_text)).setText("");
        }

        mAnswerList = new ArrayList<LinearLayout>();
        mAnswerList.add(mAnswer1);
        mAnswerList.add(mAnswer2);
        mAnswerList.add(mAnswer3);
        mAnswerList.add(mAnswer4);

        mAnswer1.setOnClickListener(this);
        mAnswer2.setOnClickListener(this);
        mAnswer3.setOnClickListener(this);
        mAnswer4.setOnClickListener(this);

        mAnswer1.setTag(ServiceCommon.BUTTONTAG_ANSWER_1);
        mAnswer2.setTag(ServiceCommon.BUTTONTAG_ANSWER_2);
        mAnswer3.setTag(ServiceCommon.BUTTONTAG_ANSWER_3);
        mAnswer4.setTag(ServiceCommon.BUTTONTAG_ANSWER_4);

        mAnswer1.setEnabled(false);
        mAnswer2.setEnabled(false);
        mAnswer3.setEnabled(false);
        mAnswer4.setEnabled(false);
    }

    /**
     * 보기 항목를 설정함
     */
    private void setAnswers() {
        if (mStudyData.mWordQuestion.get(mCurStudyIndex).mDistractorList.isEmpty())
            return;

        ArrayList<String> distractorList = mStudyData.mWordQuestion.get(mCurStudyIndex).mDistractorList;

        if (isListAnswer()) {
            ((TextView) findViewById(R.id.word_study_list_answer1_text)).setText(distractorList.get(0));
            ((TextView) findViewById(R.id.word_study_list_answer2_text)).setText(distractorList.get(1));
            ((TextView) findViewById(R.id.word_study_list_answer3_text)).setText(distractorList.get(2));
            ((TextView) findViewById(R.id.word_study_list_answer4_text)).setText(distractorList.get(3));
        } else {
            ((TextView) findViewById(R.id.word_study_answer1_text)).setText(distractorList.get(0));
            ((TextView) findViewById(R.id.word_study_answer2_text)).setText(distractorList.get(1));
            ((TextView) findViewById(R.id.word_study_answer3_text)).setText(distractorList.get(2));
            ((TextView) findViewById(R.id.word_study_answer4_text)).setText(distractorList.get(3));
        }

        mAnswer1.setEnabled(true);
        mAnswer2.setEnabled(true);
        mAnswer3.setEnabled(true);
        mAnswer4.setEnabled(true);
    }

    /**
     * 다음 문제를 시작함 마지막 문제 이후엔 단어 학습의 완료 메세지를 전달함
     */
    private void nextStudy() {
        mCurStudyIndex++;
        mIsTimerReady = false;
        playerEnd();
        if (mCurStudyIndex < mTotalStudyCount) {
            resetTimer();
            startStudy(mStudyData.mWordQuestion.get(mCurStudyIndex).mSoundFile);
        } else {
            mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, 0));
        }
    }

    /**
     * 단어 시험의 결과를 저장함
     *
     * @param answer 사용자의 선택 항목
     */
    private void setWordResult(int answer) {
        if (mCurStudyIndex >= mTotalStudyCount)
            return;
        selectAni(answer);
        //StudyDataUtil.addWordResult(answer, mHandler, mContext);
    }

    /**
     * 선택했을때 부터 에니메이션
     */
    private void selectAni(final int answer) {
        mTime = false;
        if (answer != 0) {
            setBtnControl("KIND_ENABLED", answer);
            setBtnControl("KIND_SELECTED", answer);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //초기화
                mTime = true;
                if (answer != 0) {
                    setBtnAllControl("KIND_ACTIVED", false, answer);
                    setBtnAllControl("KIND_ENABLED", false, answer);
                    setBtnAllControl("KIND_PRESSED", false, answer);
                    setBtnAllControl("KIND_SELECTED", false, answer);
                }
                nextStudy();
            }
        }, 100);
        StudyDataUtil.addWordResult(answer, mHandler, mContext);
    }

    /**
     * 버튼 상태 변경
     */
    private void setBtnControl(String kind, int answer) {
        int i = 1;
        for (LinearLayout btn : mAnswerList) {
            if (kind.equals("KIND_ACTIVED")) {
                btn.setActivated(i == answer);
            } else if (kind.equals("KIND_PRESSED")) {
                btn.setPressed(i == answer);
            } else if (kind.equals("KIND_SELECTED")) {
                //btn.setSelected(i == answer);
                if (View.VISIBLE == findViewById(R.id.word_study_lattice_word_select_layout).getVisibility()) {
                    ((LinearLayout) findViewById(short_linear[answer - 1])).setBackground(answer_on);
                    ((TextView) findViewById(short_answer[answer - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_on));
                } else {
                    ((LinearLayout) findViewById(long_linear[answer - 1])).setBackground(answer_on);
                    ((TextView) findViewById(long_answer[answer - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_on));
                }
            } else if (kind.equals("KIND_ENABLED")) {
                btn.setEnabled(i == answer);
            }
            i++;

        }
    }

    /**
     * 버튼 상태 변경(전체)
     */
    private void setBtnAllControl(String kind, boolean bol, int answer) {
        int i = 1;

        for (LinearLayout btn : mAnswerList) {
            if (kind.equals("KIND_ACTIVED")) {
                btn.setActivated(bol);
            } else if (kind.equals("KIND_PRESSED")) {
                btn.setPressed(bol);
            } else if (kind.equals("KIND_SELECTED")) {
                btn.setSelected(bol);
            } else if (kind.equals("KIND_ENABLED")) {
                btn.setEnabled(bol);
            }
            if (View.VISIBLE == findViewById(R.id.word_study_lattice_word_select_layout).getVisibility()) {
                ((LinearLayout) findViewById(short_linear[answer - 1])).setBackground(getResources().getDrawable(R.drawable.c_btn_learning_answer_bg_off));
                ((TextView) findViewById(short_answer[answer - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_off));
            } else {
                ((LinearLayout) findViewById(long_linear[answer - 1])).setBackground(getResources().getDrawable(R.drawable.c_btn_learning_answer_bg_off));
                ((TextView) findViewById(long_answer[answer - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_off));
            }
            i++;
        }
    }

    /**
     * 단어 시험 제한시간인 5초 타이머 Thread를 시작함
     */
    private void runTimerThread() {
        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                while (null != mTimerThread && !mTimerThread.isInterrupted()) {
                    SystemClock.sleep(1000);
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_UPDATE, 0));
                }
            }
        };
        mTimerThread = new Thread(runnable);
        mTimerThread.start();
    }

    /**
     * 5초 타이머 Thread를 종료함
     */
    private void stopTimerThread() {
        if (null != mTimerThread) {
            mTimerThread.interrupt();
            mTimerThread = null;
        }
    }

    /**
     * 5초 타이머를 Reset함
     */
    private void resetTimer() {
        for (int i = 0; i < mTimerViewList.size(); i++)
            mTimerViewList.get(i).setImageDrawable(mTimerBuble);
        mTimerIndex = 4;
    }

    /**
     * 1초 단위로 경과 표시를 함 5초 경과 후 자동으로 답안을 설정하고 다음 문제를 시작함
     */
    private void setTimer() {
        if (mIsTimerReady) {
            if (0 > mTimerIndex) {
                setWordResult(0);
                return;
            }
            mTimerViewList.get(mTimerIndex).setImageDrawable(mTimerBubleDis);
            mTimerIndex--;
        }
    }

    private void stopTimer() {
        if (mTimerIndex < 0)
            mTimerIndex = 0;
        mTimerViewList.get(mTimerIndex).setImageDrawable(mTimerBubleDis);
    }

    // --- 단어 시험 결과 --

    /**
     * 단어 학습 결과를 설정함
     */
    private void setWordResult() {

        findViewById(R.id.word_study_result_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.word_study_ing_layout).setVisibility(View.GONE);
        findViewById(R.id.title_view).setVisibility(View.GONE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);

        mTotalWrongStudyCount = mStudyData.mWrongWordQuestion.size();

        //ImageView chImage = (ImageView) findViewById(R.id.word_study_result_ch_img);

        timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
        updatedTime = timeInMilliseconds;
        int secs = (int) (updatedTime / 1000);
        int mins = secs / 60;
        secs = secs % 60;

        if (0 < mTotalWrongStudyCount) {
            if (1 == mTotalWrongStudyCount) {
                // guidePlay(R.raw.b_16);
                guidePlay(R.raw.b_19);
                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_02));
            } else if (2 == mTotalWrongStudyCount) {
                // guidePlay(R.raw.b_17);
                guidePlay(R.raw.b_19);
                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_03));
            } else {
                // guidePlay(R.raw.b_18);
                guidePlay(R.raw.b_19);
                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_04));
            }

            //mResultView.setText((new StringBuilder().append(mTotalWrongStudyCount).append(getString(R.string.string_study_result_wrong_word_count))).toString());
            //mResultView.append("\n");
            //mResultView.append(getString(R.string.string_study_result_wrong_tip));
            mResultView.setText((new StringBuilder().append(mTotalStudyCount).toString()));
            mResultView2.setText((new StringBuilder().append(mTotalStudyCount - mTotalWrongStudyCount).append(getString(R.string.string_common_question))).toString());
        } else {
            guidePlay(R.raw.b_15);
            //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_01));

            findViewById(R.id.word_study_result_wrong_text).setVisibility(View.GONE);
            findViewById(R.id.word_study_result_wrong_text2).setVisibility(View.GONE);

            mResultView.setText(getString(R.string.string_study_result_all_correct));

            if (0 < mStudyData.mSentenceQuestion.size()) {
                //mResultView.append("\n");
                //mResultView.append("\n");
                mResultView.append(" ");
                mResultView.append(getString(R.string.string_study_result_all_correct_tip));
            }
        }

        mResultTime.setText(" (소요 시간 : " + mins + "분  " + secs + "초)");
    }

    // --- 틀린 단어 학습 --

    /**
     * 틀린 단어 학습 가이드 음원 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mWrongStudyGuideCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            startWrongStudy(mStudyData.mWrongWordQuestion.get(0).mSoundFile);
        }
    };

    /**
     * 틀린 단어 학습 문제 전환용 Flip Animation Listener
     */
    private AnimationListener wrongStudyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            setWrongStudy();
            mCurPlayCount = 1;

            if (mMediaPlayer != null) {
                mMediaPlayer.start();
            } else {
                if (findViewById(R.id.word_study_wrong_layout).getVisibility() == View.VISIBLE) {
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE), 2000);
                }
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 틀린 단어 학습을 준비함
     */
    private void readyWrongStudy() {
        guideStop();
        tCategory2.setText(R.string.string_study_wrong_word_title);
        findViewById(R.id.word_study_wrong_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.title_view).setVisibility(View.VISIBLE);
        findViewById(R.id.ganji_titlebar).setVisibility(View.VISIBLE);
        findViewById(R.id.word_study_result_layout).setVisibility(View.GONE);
        guidePlay(R.raw.b_13, mWrongStudyGuideCompletion);
    }

    /**
     * 틀린 단어 문제를 설정함
     */
    private void setWrongStudy() {
        mWrongStudyView.setText(mStudyData.mWrongWordQuestion.get(mCurWrongStudyIndex).mQuestion.replaceAll("`", "'"));
        mWrongStudyCountView.setText(mCurWrongStudyIndex + 1 + "/" + mTotalWrongStudyCount);
    }

    /**
     * 틀린 단어 문제 전환용 애니메이션을 시작함
     */
    private void startWrongStudyAni() {
        /*Animation ani = new FlipAnimation(180f, 0f, mWrongStudyView.getWidth() / 2, mWrongStudyView.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(wrongStudyAniListener);
        mWrongStudyView.startAnimation(ani);*/

        Animation ani = new FlipAnimation(180f, 0f, wrong_layout.getWidth() / 2, wrong_layout.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(wrongStudyAniListener);
        wrong_layout.startAnimation(ani);
    }

    /**
     * 틀린 단어 학습을 시작함
     *
     * @param filePath 재생 파일 경로
     */
    private void startWrongStudy(String filePath) {
        mWrongStudyView.setText("");
        mWrongStudyAnswerView.setText("");

        mWrongStudy = true;
        setPlayer(filePath);

        if (0 < mCurWrongStudyIndex) {
            startWrongStudyAni();
        } else {
            setWrongStudy();
            mCurPlayCount = 1;
            mMediaPlayer.start();
        }
    }

    /**
     * 다음 틀린 단어 문제를 시작함
     */
    private void nextWrongStudy() {
        mCurWrongStudyIndex++;
        playerEnd();
        if (mCurWrongStudyIndex < mTotalWrongStudyCount) {
            startWrongStudy(mStudyData.mWrongWordQuestion.get(mCurWrongStudyIndex).mSoundFile);
        } else {
            // 틀린 문제 전체 1회 완료 -> 다음 단계 이동 버튼 표시
            mNextStatus.setVisibility(View.VISIBLE);
            mCurWrongStudyPracticeCount++;
            if (mCurWrongStudyPracticeCount <= mStudyData.mPracticeCnt) {
                mCurWrongStudyIndex = 0;
                startWrongStudy(mStudyData.mWrongWordQuestion.get(0).mSoundFile);
            }
        }
    }

    /**
     * 틀린 단어 정답을 설정함
     */
    private void setWrongStudyAnswer() {
        mWrongStudyAnswerView.setText(mStudyData.mWrongWordQuestion.get(mCurWrongStudyIndex).mMeaning);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE_NEXT), 2000);
    }

    /**
     * 틀린 단어 문제 2번 재생 완료시 2초 후 정답 공개 메세지를 전달함
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.pause();
        mCurPlayCount++;
        // 음성 2번 들려준 후 2초 후 정답 공개
        if (2 >= mCurPlayCount) {
            mMediaPlayer.seekTo(0);
        } else {
            if (findViewById(R.id.word_study_wrong_layout).getVisibility() == View.VISIBLE) {
                mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE), 2000);
            }
        }
    }

    /**
     * 1번 재생 완료 이후 처음으로 Seek 완료 되어 재생을 시작함
     */
    @Override
    public void onSeekComplete(MediaPlayer mp) {
        mMediaPlayer.start();
    }

    /**
     * 다음 단계로 이동 처리함
     */
    private void goNextStatus() {
        if (findViewById(R.id.word_study_result_layout).getVisibility() == View.VISIBLE) {
            if (0 < mTotalWrongStudyCount) {
                readyWrongStudy();
                return;
            }
        }
        if (isNext) {
            isNext = false;
            mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
            playerEnd();

            if (!ServiceCommon.IS_CONTENTS_TEST) {
                if (0 < mStudyData.mSentenceQuestion.size())
                    startActivity(new Intent(this, SentenceTitlePaperActivity.class));
                else if (mStudyData.mIsParagraph && 0 < mStudyData.mVanishingQuestion.size()) {
                    startActivity(new Intent(this, VanishingTitlePaperActivity.class));
                } else if (mStudyData.mIsDictation && 0 < mStudyData.mDictation.size())
                    startActivity(new Intent(this, DictationTitlePaperActivity.class));
                else if (mStudyData.isNewDication && mStudyData.mIsDicPirvate)
                    startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
                else if (mStudyData.mIsMovieExist)
                    startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
                else if (mStudyData.mOneWeekData.size() > 0)
                    startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
                else
                    startActivity(new Intent(this, StudyOutcomeActivity.class));

            }

            StudyDataUtil.setCurrentStudyStatus(this, "S32");
            finish();
        }
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START) {
                        startStudy(mStudyData.mWordQuestion.get(0).mSoundFile);
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_UPDATE) {
                        if (mTime) {
                            setTimer();
                        } else {
                            stopTimer();
                        }
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION) {
                        stopTimerThread();
                        setWordResult();
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE) {
                        if (msg.arg2 == ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE)
                            setWrongStudyAnswer();
                        else
                            nextWrongStudy();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void requestServerTimeSync(int type) {
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestServerTimeSync(mNetworkHandler, type);
    }

    private void readyStudy() {
        StudyDataUtil.setCurrentStudyStatus(this, "S31");
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "--- S31 -----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, WordExamActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            readyStudy();
                            Log.k("wusi12", "--------------------");
                        }
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "-------S32-----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, WordExamActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            isNext = false;
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            isNext = false;
                        } finally {
                            goNextStatus();
                            Log.k("wusi12", "--------------------");
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        readyStudy();
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        goNextStatus();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };
}
