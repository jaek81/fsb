package com.yoons.fsb.student.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.yoons.fsb.student.ui.data.ReviewBookSelectListItem;

import java.util.ArrayList;

/**
 * content test 교재 목록 list adapter
 * @author ejlee
 */
public class ReviewBookSelectListAdapter extends BaseAdapter {
	private Context mContext = null;
	private ArrayList<ReviewBookSelectListItem.ItemInfo> 	mItemInfoList = null;
	private ListView mDisListView = null;
	private ReviewBookSelectListItem.OnItemClickListener mClickListener = null;
	
	public ReviewBookSelectListAdapter(Context context, ArrayList<ReviewBookSelectListItem.ItemInfo>  itemInfoList, 
			ReviewBookSelectListItem.OnItemClickListener listener) {
		mContext = context;
		mItemInfoList =	itemInfoList;
		mClickListener = listener;
	}
	
	public void setItemInfoList(ArrayList<ReviewBookSelectListItem.ItemInfo> list) {
		mItemInfoList = list;
	}
	public void setDisListView(ListView listView) {
		mDisListView = listView;
	}
	
	@Override
	public int getCount() {
		return mItemInfoList.size();
	}

	@Override
	public Object getItem(int position) {
		return mItemInfoList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ReviewBookSelectListItem viewItem = null;
		
		if(position < mItemInfoList.size()) {
			if (convertView == null) {			
				viewItem = new ReviewBookSelectListItem(mContext);
			} else {
				viewItem = (ReviewBookSelectListItem)convertView;
			}
			
			viewItem.setDisListView(mDisListView);
			viewItem.setOnItemClickListener(mClickListener);
			viewItem.setItemInfo(mItemInfoList.get(position));
		}

		return viewItem;
	}
}


