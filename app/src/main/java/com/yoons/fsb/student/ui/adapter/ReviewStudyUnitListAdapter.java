package com.yoons.fsb.student.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.yoons.fsb.student.ui.data.ReviewStudyUnitListItem;

import java.util.ArrayList;

/**
 * content test 차시 목록 list adapter
 * @author ejlee
 */
public class ReviewStudyUnitListAdapter extends BaseAdapter {
	private Context mContext = null;
	private ArrayList<ReviewStudyUnitListItem.ItemInfo> 	mItemInfoList = null;	
	
	
	public ReviewStudyUnitListAdapter(Context context) {
		mContext = context;
	}
	
	public void setItemInfoList(ArrayList<ReviewStudyUnitListItem.ItemInfo> list) {
		mItemInfoList = list;
		
	}
	
	@Override
	public int getCount() {
		return mItemInfoList.size();
	}

	@Override
	public Object getItem(int position) {
		return mItemInfoList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ReviewStudyUnitListItem viewItem = null;
		
		if(position < mItemInfoList.size()) {
			if (convertView == null) {			
				viewItem = new ReviewStudyUnitListItem(mContext);
				
			} else {
				viewItem = (ReviewStudyUnitListItem)convertView;				
			}
			
			viewItem.setItemInfo(mItemInfoList.get(position));
		}

		return viewItem;
	}
}


