package com.yoons.fsb.student.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.yoons.fsb.student.ui.data.StudyBookSelectListItem;
import com.yoons.fsb.student.ui.data.StudyBookSelectListItem.OnBookItemClickListener;

import java.util.ArrayList;

/**
 * 교재 선택화면 교재 listAdapter
 * @author ejlee
 */
public class StudyBookSelectListAdapter extends BaseAdapter {
	private Context mContext = null;
	private ArrayList<StudyBookSelectListItem.ItemInfo> mItemInfoList = null;
	private OnBookItemClickListener mItemClickListener = null;
	private ArrayList<View> mViewList = new ArrayList<View>(); // getview에서 아이템이 뒤집힐때가 있어, 교재 다운로드시 썸네일이 왔다갔다함.
	
	public StudyBookSelectListAdapter(Context context, OnBookItemClickListener listener, ArrayList<StudyBookSelectListItem.ItemInfo> itemInfoList) {
		mContext = context;
		mItemClickListener = listener;
		mItemInfoList =	itemInfoList;
	}
	
	public void setItemInfoList(ArrayList<StudyBookSelectListItem.ItemInfo> list) {
		mItemInfoList = list;
	}
	
	@Override
	public int getCount() {
		return mItemInfoList.size();
	}

	@Override
	public Object getItem(int position) {
		return mItemInfoList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		StudyBookSelectListItem viewItem = null;
		
		if(position < mItemInfoList.size()) {
			if (convertView == null) {	
				viewItem = new StudyBookSelectListItem(mContext);
				mViewList.add(position, viewItem);
			} else {
				try { 
					viewItem = (StudyBookSelectListItem)mViewList.get(position);	
				} catch (Exception e){ // index가 안맞는 때가 있음.
					viewItem = new StudyBookSelectListItem(mContext);
					mViewList.add(position, viewItem);
				}
			}
			
			viewItem.setItemInfo(mItemInfoList.get(position));
			viewItem.setOnItemClickListener(mItemClickListener);
		}

		return viewItem;
	}
	
}


