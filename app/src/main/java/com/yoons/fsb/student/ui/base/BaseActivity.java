package com.yoons.fsb.student.ui.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.yoons.fsb.student.MediaButtonReceiver;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.ui.IntroActivity;
import com.yoons.fsb.student.ui.control.Indicator;
import com.yoons.fsb.student.ui.popup.AnimationBox;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import java.util.Timer;
import java.util.TimerTask;

public class BaseActivity extends Activity implements OnClickListener {

	public static final String ACTIVITY_LOG_OUT_EVENT = "ACTIVITY_LOG_OUT_EVENT";
	public static final String LOG_TAG = "BaseActivity";

	protected Context mContext = null;
	private Indicator mTitleBar = null;
	private TextView bookname = null;
	private boolean mIsCancelable = true;
	private boolean mIsBackKeyDoublePress = true;
	private long mPrevTime = 0;
	private long mDoublePressInterval = 2000;

	protected boolean mExitInBackground = true;

	protected MessageBox mMsgBox = null;

	protected MediaPlayer mGuidePlayer = null;
	protected int mGuideMsg = 0;

	protected boolean mIsProcess = false;

	// 학습 중 화면 잠금,꺼짐으로 onPause()가 발생한 경우 간지로 재시작
	protected boolean mIsReStart = true;

	protected BroadcastReceiver mFinishEventReceiver = null;

	protected BroadcastReceiver mSlogEventReceiver = null;
	private AudioManager mAudioManager;
	private ComponentName mRemoteControlResponder;

	protected OnSharedPreferenceChangeListener mSharedPrefChangeListener = null;
	/** 네트워크 상태 변화 감시 */
	protected NetworkChangeReceiver mNetChangeReceiver = null;

	/** 네트워크 타입 */
	private int mNetType = -1;
	private NetworkInfo.State mNetState = NetworkInfo.State.UNKNOWN;
	// 리시버
	private EarPhoneReceiver earPhoneReceiver;

	protected AnimationBox ani_box;

	protected Handler mHandler = new Handler();

	protected boolean isNext = true;//다음 화면 넘어갈때 중복방지
	public static Typeface mTypeface = null, mTypeface2 = null;

	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);

		if (mTypeface == null) {
			mTypeface = Typeface.createFromAsset(getAssets(), "fonts/nanumsquareroundeb.ttf");
		}

		if (mTypeface2 == null) {
			mTypeface2 = Typeface.createFromAsset(getAssets(), "fonts/nanumsquareroundb.ttf");
		}

		CommonUtil.setGlobalFont(this, getWindow().getDecorView());

		CommonUtil.setSizeLayout(this, getWindow().getDecorView());

		mTitleBar = findViewById(R.id.ganji_titlebar);
		bookname = findViewById(R.id.book_name);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onBackPressed() {
		if (!mIsCancelable)
			return;

		if (mIsBackKeyDoublePress) {
			long t = System.currentTimeMillis();
			if (0 >= mPrevTime || mDoublePressInterval < (t - mPrevTime)) {
				if (!ServiceCommon.IS_CONTENTS_TEST)
					Toast.makeText(this, getString(R.string.string_backkey_toast_message), Toast.LENGTH_SHORT).show();
				mPrevTime = t;
				return;
			}

			cancelStudyingFlag();
			if (!ServiceCommon.IS_CONTENTS_TEST)
				StudyData.clear();
		}

		super.onBackPressed();
	}

	@Override
	public void onClick(View view) {

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//keepScreen(true);

		super.onCreate(savedInstanceState);

		mContext = this;

		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mRemoteControlResponder = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());

		mAudioManager.registerMediaButtonEventReceiver(mRemoteControlResponder);

		// 볼륨키 동작 시 무조건 미디어 볼륨 조절하도록 함.
		setMediaVolumeOnly();

		// Activity 종료 Receiver 등록
		setLogOutReceiver();
		//setNotiReceiver();
		if (mNetChangeReceiver == null) {
			mNetChangeReceiver = new NetworkChangeReceiver();
		}
		//registerReceiver(mPushReceiver, new IntentFilter(ServiceCommon.ACTION_PUSH_MESSAGE));
		registerReceiver(mNetChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

		if (earPhoneReceiver == null) {
			earPhoneReceiver = new EarPhoneReceiver();
		}
		IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
		registerReceiver(earPhoneReceiver, filter);
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (0 < mGuideMsg) {
			guidePlay(mGuideMsg);
			mGuideMsg = 0; // 한번만 실행
		}

		if (ServiceCommon.IS_LAUNCHER) {
			if (isCustomerChange() && !mContext.getClass().getName().contains("IntroActivity")) {
				cancelStudyingFlag();
				StudyData.clear();
				Intent intent = new Intent(this, IntroActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		mIsReStart = true;
	}

	/**
	 * Preferences에 저장된 고객번호와 login app에서 전달받는 고객번호 비교하여 틀릴 경우 다른 사용자 로그인으로 판단
	 */
	public boolean isCustomerChange() {
		Uri CONTENT_URI = Uri.parse("content://com.yoons.fsb.login/sbm_customer_login");
		Bundle bundle = null;
		boolean retVal = false;

		try {
			bundle = this.getContentResolver().call(CONTENT_URI, "getCustomer()", null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (bundle != null) {
			if (bundle.getInt("mCustomerNo") > 0) { // 로그인 정보 받은경우
				int customerNo = bundle.getInt("mCustomerNo");

				if (customerNo != Preferences.getCustomerNo(this))
					retVal = true;
			} else { // 로그인 된 사용자가 없는 경우(교사 및 서버에서 단말 해지)
				retVal = true;
			}
		}

		return retVal;
	}

	/**
	 * Homekey
	 */
	protected void onUserLeaveHint() {
		super.onUserLeaveHint();
		mIsReStart = false;
	}

	/**
	 * Override method (View.setKeepScreenOn)
	 * 
	 * @param isOn
	 *            true이면 화면 잠기지 않도록 설정함.
	 */
	protected void keepScreen(boolean isOn) {
		findViewById(android.R.id.content).setKeepScreenOn(isOn);
	}

	/**
	 * 환경설정 값 변경 감시 설정<br>
	 * 해, 구름 : Preferences.getServerRequestSuccess(context ctx) 값 true (WIFI감도에
	 * 따라 해, 구름 표시)<br>
	 * 비 : Preferences.getServerRequestSuccess(context ctx) 값 false
	 */
	protected void setPreferencesCallback() {

		if (null != mSharedPrefChangeListener)
			return;

		mSharedPrefChangeListener = new OnSharedPreferenceChangeListener() {

			public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

				if (null == key)
					return;

				if (Preferences.KEY_WIFI_LEVEL.equals(key) || Preferences.KEY_NETWORK_TYPE.equals(key))
					if (mTitleBar != null)
						mTitleBar.setSensitiveIcon();
			}
		};

		SharedPreferences prefs = Preferences.getPref(this);
		if (null != prefs) {
			prefs.registerOnSharedPreferenceChangeListener(mSharedPrefChangeListener);
			mSharedPrefChangeListener.onSharedPreferenceChanged(prefs, Preferences.KEY_WIFI_LEVEL);
			mSharedPrefChangeListener.onSharedPreferenceChanged(prefs, Preferences.KEY_NETWORK_TYPE);
		}

		// Wi-Fi 아이콘 설정 시 Bluetooth도 함께 설정하므로 여기에서 처리함.
		mTitleBar.reflectChangeStatus();
	}

	/**
	 * 환경설정 값 변경 감시 해제
	 */
	protected void unsetPreferencesCallback() {

		if (null != mSharedPrefChangeListener) {

			SharedPreferences prefs = Preferences.getPref(this);
			if (null != prefs) {
				prefs.unregisterOnSharedPreferenceChangeListener(mSharedPrefChangeListener);
				mSharedPrefChangeListener = null;
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mAudioManager.unregisterMediaButtonEventReceiver(mRemoteControlResponder);

		unsetPreferencesCallback();

		if (null != mContext)
			mContext = null;

		try {
			// Activity 종료 Receiver 해제
			if (null != mFinishEventReceiver) {
				unregisterReceiver(mFinishEventReceiver);
				mFinishEventReceiver = null;
			}
			if (null != mNetChangeReceiver) {
				unregisterReceiver(mNetChangeReceiver);
				mNetChangeReceiver = null;
			}
			if (null != earPhoneReceiver) {
				unregisterReceiver(earPhoneReceiver);
				earPhoneReceiver = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (isShowingMsgBox()) {
			mMsgBox.dismiss();
			mMsgBox.setOnDialogDismissListener(null);
			mMsgBox = null;
		}

		guideStop();
	}

	/**
	 * 모든 ACTIVITY를 종료 시키는 이벤트 수신
	 */
	protected void setLogOutReceiver() {
		// 이벤트 Receiver 생성
		registerReceiver(mFinishEventReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, final Intent intent) {

				String action = intent.getAction();
				if (action.equalsIgnoreCase(ACTIVITY_LOG_OUT_EVENT)) {
					finish(); // Activity 종료 처리
				}
			}
		}, new IntentFilter(ACTIVITY_LOG_OUT_EVENT));
	}

	/**
	 * 모든 ACTIVITY를 종료 시키는 이벤트 수신
	 */
	/*
	 * protected void setCallReceiver() { // 이벤트 Receiver 생성
	 * registerReceiver(mNotiEventReceiver = new BroadcastReceiver() { public
	 * void onReceive(Context context, final Intent intent) {
	 * 
	 * String action = intent.getAction(); if
	 * (action.equalsIgnoreCase(ServiceCommon.ACTION_NOTI_CALL)) {
	 * 
	 * ani_box = new AnimationBox(mContext, R.anim.ani_call);
	 * ani_box.setConfirmText(R.string.string_common_confirm);
	 * 
	 * if (!ani_box.isShowing()) { ani_box.show(); guidePlay(R.raw.p_call); } }
	 * } }, new IntentFilter(ServiceCommon.ACTION_NOTI_CALL)); }
	 * 
	 */ /**
		 * 모든 ACTIVITY를 종료 시키는 이벤트 수신
		 */
	protected void setSlogReceiver() {
		// 이벤트 Receiver 생성
		registerReceiver(mSlogEventReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, final Intent intent) {

				String action = intent.getAction();
				if (action.equalsIgnoreCase(ServiceCommon.ACTION_SLOG)) {

					//SLOG 받을때
				}
			}
		}, new IntentFilter(ServiceCommon.ACTION_SLOG));
	}

	/**
	 * Activity(LoginActivity 제외)를 종료 시키는 이벤트 발신(Broadcast)
	 */
	protected void sendLogOutEvent() {

		sendBroadcast(new Intent(ACTIVITY_LOG_OUT_EVENT));
	}

	/**
	 * Activity(LoginActivity 포함)를 종료 시키는 이벤트 발신(Broadcast)
	 */
	protected void sendLogOutEvent2() {
		Intent intent = new Intent(ACTIVITY_LOG_OUT_EVENT);
		intent.putExtra("finishall", true);

		sendBroadcast(intent);
	}

	/**
	 * 메시지 박스가 활성화되어 있는지 확인한다.
	 * 
	 * @return true이면 메시지 박스가 보이는 상태임
	 */
	public boolean isShowingMsgBox() {
		return null != mMsgBox && mMsgBox.isShowing();

	}

	/**
	 * 일정 시간(500mili) 동안 클래그 설정을 변경함<br>
	 * 재 진입을 막기 위함
	 * 
	 * @return
	 */
	protected boolean singleProcessChecker() {
		if (mIsProcess)
			return true;

		if (null == mContext)
			return true;

		mIsProcess = true;

		TimerTask timerTask = new TimerTask() {
			public void run() {
				mIsProcess = false;
			}
		};

		Timer timer = new Timer();
		timer.schedule(timerTask, 500);
		return false;
	}

	/**
	 * 학습 중 Flag 설정 취소(학습 중이 아님)
	 */
	private void cancelStudyingFlag() {
		try {
			new Thread(new Runnable() {
				@Override
				public void run() {
					Preferences.setStudying(BaseActivity.this, false);
				}
			}).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 볼륨 키 설정으로 미디어 볼륨만 제어하도록 설정
	 */
	protected void setMediaVolumeOnly() {
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	/**
	 * 가이드 음성,효과음 재생
	 * 
	 * @param soundResId
	 *            음원 리소스 id
	 */
	protected void guidePlay(int soundResId) {

		if (null != mGuidePlayer)
			guideStop();

		mGuidePlayer = MediaPlayer.create(getBaseContext(), soundResId);
		if (null != mGuidePlayer) {
			mGuidePlayer.start();
		}
	}

	/**
	 * 가이드 음성,효과음 재생
	 * 
	 * @param soundResId
	 *            음원 리소스 id
	 * @param listener
	 *            OnCompletionListener(재생 완료 통지)
	 */
	protected void guidePlay(int soundResId, OnCompletionListener listener) {

		if (null != mGuidePlayer)
			guideStop();

		mGuidePlayer = MediaPlayer.create(getBaseContext(), soundResId);
		if (null != mGuidePlayer) {
			mGuidePlayer.setOnCompletionListener(listener);
			mGuidePlayer.start();
		}
	}

	/**
	 * 가이드 음성,효과음 재생중지(리소스 해제)
	 */
	protected void guideStop() {

		if (null == mGuidePlayer)
			return;

		if (mGuidePlayer.isPlaying())
			mGuidePlayer.stop();
		mGuidePlayer.release();
		mGuidePlayer = null;
	}

	/**
	 * 가이드 음성,효과음 설정(onResume 시 사운드 재생)
	 * 
	 * @param guideResId
	 */
	protected void setGuideMsg(int guideResId) {
		mGuideMsg = guideResId;
	}

	@Override
	public void finish() {

		overridePendingTransition(0, R.anim.slide_in_right);
		super.finish();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {

		/*
		 * switch (keyCode) { case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
		 * case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS: return true; }
		 */

		return super.onKeyDown(keyCode, event);
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {

		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT: {
			AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
			am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			return true;
		}

		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS: {
			AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
			am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			return true;
		}
		}

		return super.onKeyUp(keyCode, event);
	}

	/**
	 * 취소 가능(Back Key) 여부를 설정함
	 * 
	 * @param flag
	 */
	protected void setCancelable(boolean flag) {
		mIsCancelable = flag;
	}

	/**
	 * 이중 Back Key 취소 가능 여부를 설정함
	 * 
	 * @param flag
	 */
	protected void setBackKeyDoublePress(boolean flag) {
		mIsBackKeyDoublePress = flag;
	}

	/**
	 * Back Key를 2번 사용으로 취소할 경우 두번의 시간 간격
	 * 
	 * @param long
	 *            시간 각격(Millis)
	 */
	protected void setDoublePressInterval(long millis) {
		mDoublePressInterval = millis;
	}

	/**
	 * 타이틀바의 중앙에 표시 할 내용을 설정함.
	 * 
	 * @param String
	 *            표시할 문자열
	 */
	public void setTitlebarText(String title) {
		/*if (null == mTitleBar || null == title)
			return;
		mTitleBar.setTitle(title);*/

		if (null == bookname || null == title)
			return;
		bookname.setText(title);
	}

	/**
	 * 타이틀바의 좌측에 표시 할 내용을 설정함.(예, "[학습]", "[복습]")
	 * 
	 * @param String
	 *            표시할 문자열
	 */
	public void setTitlebarCategory(String category) {

		if (null == mTitleBar || null == category)
			return;

		mTitleBar.showCategory(true, category);
	}

	/**
	 * 타이틀바의 좌측에 홈(HOME)아이콘 설정
	 * 
	 * @param listener
	 *            ClickListener
	 */
	public void showTitlebarHomeMenu(OnClickListener listener) {

		if (null == mTitleBar)
			return;

		mTitleBar.showHomeButton(true, listener);
	}

	/**
	 * 타이틀바의 좌측에 홈(HOME)아이콘 설정
	 */
	public void showTitlebarHomeMenu() {

		if (null == mTitleBar)
			return;

		mTitleBar.showHomeButton(true, null);
	}

	/**
	 * 타이틀바의 좌측에 사용자 이름을 나타냄
	 */
	public void showUserName() {

		if (null == mTitleBar)
			return;

		mTitleBar.setUserName(true);
	}

	private BroadcastReceiver mPushReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(ServiceCommon.ACTION_PUSH_MESSAGE)) {
				String message = intent.getStringExtra(ServiceCommon.EXTRA_PUSH_MESSAGE);

				mMsgBox = new MessageBox(context, 0, message);
				mMsgBox.setConfirmText(R.string.string_common_confirm);
				mMsgBox.show();

				abortBroadcast();
			}
		}
	};

	protected int getCorrectKeyCode(int keyCode) {
		if (CommonUtil.isQ7Device() && keyCode == 0) {
			return ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD;
		}

		return keyCode;
	}

	/**
	 * Network Change Receiver Class
	 */
	public class NetworkChangeReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (!ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction()))
				return;

			NetworkInfo ni = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            int type = ni.getType();

			if (ni.getState() == ni.getState().CONNECTED || ni.getState() == ni.getState().CONNECTING) {
                if(type == ConnectivityManager.TYPE_ETHERNET) {
                    Preferences.setNetworkType(context, 9);
                } else if(type == ConnectivityManager.TYPE_WIFI) {
                    Preferences.setNetworkType(context, 1);
                } else if(type == ConnectivityManager.TYPE_MOBILE) {
                    Preferences.setNetworkType(context, 0);
                }
            } else {
				Preferences.setNetworkType(context, -1);
			}
		}
	}

	private class EarPhoneReceiver extends BroadcastReceiver {
		boolean first_bol = true;

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
				if (!first_bol) {
					int state = intent.getIntExtra("state", -1);
					switch (state) {
					case 0:
						Toast.makeText(context, "이어폰이 제거 되었습니다.", Toast.LENGTH_SHORT).show();
						break;
					case 1:
						Toast.makeText(context, "이어폰이 연결 되었습니다.", Toast.LENGTH_SHORT).show();
						break;
					default:
						Toast.makeText(context, "이어폰 정보를 알수 없음.", Toast.LENGTH_SHORT).show();
					}
				} else {
					first_bol = false;
				}
			}
		}
	}

}
