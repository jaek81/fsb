package com.yoons.fsb.student.ui.base;

import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.yoons.fsb.student.ServiceCommon;

public class BaseDialog extends Dialog {

	public static final int DIALOG_CONFIRM = 1;
	public static final int DIALOG_CANCEL = 2;

	protected Context mContext = null;
	/**
	 * <Pre>
	 * Id를 세팅하여 한 액티비티에서 같은 형태의 다이얼로그를 사용시 구별한다.
	 * 
	 * void setId(int id)
	 * int getId()
	 * */
	private int mDialogId = 0;

	public void setId(int id) {
		mDialogId = id;
	}

	public int getId() {
		return mDialogId;
	}
	
	protected OnDialogDismissListener mDismissListener = null;

	public BaseDialog(Context context, int theme) {
		super(context, theme);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	// DismissListener
	public interface OnDialogDismissListener {
		public void onDialogDismiss(int result, int dialogId);
	}

	public void setOnDialogDismissListener(OnDialogDismissListener li) {
		mDismissListener = li;
	}

    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	
        switch (keyCode) {
	        case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT: {
	        	AudioManager am = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
	        	am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
	        	return true;
	        }
	
	        case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS: {
	        	AudioManager am = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
	            am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
	            return true;
	        }
        }

        return super.onKeyDown(keyCode, event);
    }
	
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_MENU:
			case KeyEvent.KEYCODE_SEARCH: 
				return true;

			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
				return true;

			case KeyEvent.KEYCODE_BACK: {
				if (mDismissListener != null) 
					mDismissListener.onDialogDismiss(DIALOG_CANCEL, getId());
				break;
			}
		}

		return super.onKeyUp(keyCode, event);
	}
}
