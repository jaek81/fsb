package com.yoons.fsb.student.ui.base;

import android.media.AudioManager;
import android.media.MediaPlayer;

import com.yoons.fsb.student.custom.CustomVideoPlayer;
import com.yoons.fsb.student.custom.CustomVorbisPlayer;
import com.yoons.fsb.student.custom.CustomWavRecoder;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.hssb.student.custom.CustomMp3Recorder;
import com.yoons.recognition.VoiceRecognizerWeb;

import java.io.File;

/**
 * 학습 화면(복습,오디오학습,단어시험,문장시험)의 Base Class 
 * 
 * @author jaek
 */
public class BaseStudyActivity extends BaseActivity {
	
	// 학습 데이터 
	protected StudyData mStudyData = null;
	
	// video player
	protected CustomVideoPlayer mVideoPlayer = null;
		
	// vorbis file player
	protected CustomVorbisPlayer mVorbisPlayer = null;
	
	// android media player 
	protected MediaPlayer mMediaPlayer = null, mRecordPlayer = null, mRecordNotiPlayer = null;
	
	// mp3 recorder 
	protected CustomMp3Recorder mMp3Recorder = null;
	
	// wav recorder (음성 -> text 변환 및 채점을 위한) 
	protected CustomWavRecoder mWavRecorder = null;
	
	// 음성 인식 채점 task 
	protected VoiceRecognizerWeb VRUTask = null;
	
	// 쓰기, 녹음 효과 애니메이션 쓰레드 
	protected Thread mIngEffectThread = null, mCompleteEffectThread = null;
	
	// 단어시험,보충 타이머 쓰레드 
	protected Thread mTimerThread = null;
	
	// 문장시험,보충 결과 저장 완료 여부 체크 쓰레드 
	protected Thread mResultAddCompleteCheckThread = null;
	
	// 일회성 녹음 저장 경로.. 재녹음시 기존 파일은 삭제함 
	protected String mRecordFilePath = "";
	
	// 음성 텍스트 
	protected String mVoiceText = "";
	
	// 음성 점수 
	protected int mVoiceScore = 0;
	
	// int형의 millisecond ex> 1000 -> 1초 
	protected int mCurPosition = 0;
	
	// 현재 틀린문제의 음성 재생 횟수 => 총 2회 들려줌 
	protected int mCurPlayCount = 1;
		
	// 현재 문제 
	protected int mCurStudyIndex = 0; 
	
	// 총 문제 수 
	protected int mTotalStudyCount = 1;
	
	// 현재 틀린 문제 
	protected int mCurWrongStudyIndex = 0;
	
	// 총 틀린 문제 수 
	protected int mTotalWrongStudyCount = 1;
	
	// 현재 틀린 문제 반복 횟수 
	protected int mCurWrongStudyPracticeCount = 1;
	
	// 단어시험,보충 보기 애니메이션 완료 횟수 
	protected int mCurAnswerAniCount = 1;
		
	// 단어시험,보충 타이머 인덱스 
	protected int mTimerIndex = 4;
	
	// 사용자 설정 미디어 볼륨 
	protected int mUserMediaVolume = -1;
	
	// 쓰기, 녹음 버튼 팁 메세지 width 
	protected int mTooltipWidth = 0;
	
	// 쓰기, 녹음 버튼 팁 메세지 height
	protected int mTooltipHeight = 0;
		
	// 최대 녹음 시간 
	protected int mMaxRecTime = 0;
		
	// 단어시험,보충 타이머 준비 여부 
	protected boolean mIsTimerReady = false;
	
	// 음성 인식 사용 여부 
	protected boolean mIsRecoginition = false;
	
	// 쓰기, 녹음 진행 여부 
	protected boolean mIsWriting = false, mIsRecording = false; 
	
	// 학습 시작 여부 
	protected boolean mIsStudyStart = false; 
	
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
        //CommonUtil.conSlogService(this, true);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
        //CommonUtil.conSlogService(this, false);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		setFinish();
	}
	
	/**
	 * 에코 증폭 수치를 현재 미디어 볼륨에 적용함 
	 */
	public void setEchoVolume() {
		if (null == mContext)
			return;
		
		int prefValue = Preferences.getStudyEchoVolume(mContext);
		if (0 < prefValue) {
			AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
			mUserMediaVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
			int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			int echoVolume = mUserMediaVolume + ((maxVolume * prefValue) / 100);
			
			if (echoVolume > maxVolume)
				echoVolume = maxVolume;
			
			am.setStreamVolume(AudioManager.STREAM_MUSIC, echoVolume, 0);
		}
	}
	
	/**
	 * 에코 증폭 수치 적용 전 미디어 볼륨 값으로 원복함 
	 */
	protected void releaseEchoVolume() {
		if (0 <= mUserMediaVolume) {
			AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
			am.setStreamVolume(AudioManager.STREAM_MUSIC, mUserMediaVolume, 0);
		}
	}
	
	/**
	 * 학습 종료시 데이터를 정리함 
	 */
	protected void setFinish() {
		if (null != mContext)
			mContext = null;
		
		if (null != mTimerThread) {
			mTimerThread.interrupt();
			mTimerThread = null;
		}
		
		if (null != mIngEffectThread) {
			mIngEffectThread.interrupt();
			mIngEffectThread = null;
		}
		
		if (null != mCompleteEffectThread) {
			mCompleteEffectThread.interrupt();
			mCompleteEffectThread = null;
		}
		
		if (null != mResultAddCompleteCheckThread) {
			mResultAddCompleteCheckThread.interrupt();
			mResultAddCompleteCheckThread = null;
		}
		
		if (null != mVideoPlayer) 
			mVideoPlayer = null;
		
		if (null != mVorbisPlayer) {
			mVorbisPlayer.stop();
			mVorbisPlayer.release();
			mVorbisPlayer = null;
		}
		
		if (null != mMediaPlayer) {
			if (mMediaPlayer.isPlaying())
				mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		
		if (null != mRecordPlayer) {
			if (mRecordPlayer.isPlaying())
				mRecordPlayer.stop();
			mRecordPlayer.release();
			mRecordPlayer = null;
		}
		
		if (null != mWavRecorder) {
			if (mIsRecording && null == mRecordNotiPlayer)
				mWavRecorder.stopRecording();
			mWavRecorder = null;
		}
		
		if (null != mRecordNotiPlayer) {
			if (mRecordNotiPlayer.isPlaying())
				mRecordNotiPlayer.stop();
			mRecordNotiPlayer.release();
			mRecordNotiPlayer = null;
		}
		
		if (null != mMp3Recorder) {
			if (mMp3Recorder.isRecording())
				mMp3Recorder.stop();
			mMp3Recorder = null;
		}
		
		if (null != mRecordFilePath) {
			File fileRecord = new File(mRecordFilePath);	
			if (fileRecord.exists())
				fileRecord.delete();
		}
		
		releaseEchoVolume();
	}
}
