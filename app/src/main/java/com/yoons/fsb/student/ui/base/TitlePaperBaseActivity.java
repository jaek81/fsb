package com.yoons.fsb.student.ui.base;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.DictationActivity;
import com.yoons.fsb.student.ui.DictationNewActivity;
import com.yoons.fsb.student.ui.MovieReviewActivity;
import com.yoons.fsb.student.ui.SentenceExamActivity;
import com.yoons.fsb.student.ui.SmartStudyActivity;
import com.yoons.fsb.student.ui.StudyOutcomeActivity;
import com.yoons.fsb.student.ui.WordExamActivity;
import com.yoons.fsb.student.ui.WordTitlePaperActivity;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.control.MaxSizeTextView;
import com.yoons.fsb.student.ui.popup.AnimationBox;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.sec.study.OneWeekTestActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 간지화면의 base class<br>
 * (학습 상태에 따른 가이드 음원, 아이콘, 타이틀, 다음화면 이동 설정 기능)
 *
 * @author dckim
 */
public class TitlePaperBaseActivity extends BaseActivity implements OnClickListener, OnDialogDismissListener {

    protected final int TP_TYPE_SMART_STUDY = 0;
    protected final int TP_TYPE_EXAM_PREPARING = 1;
    protected final int TP_TYPE_WORD = 2;
    protected final int TP_TYPE_SENTENCE = 3;
    protected final int TP_TYPE_VANISHING = 4;
    protected final int TP_TYPE_DICTATION = 5;
    protected final int TP_TYPE_NEW_DICTATION = 6;
    protected final int TP_TYPE_MOVIE_REVIEW = 7;
    protected final int TP_TYPE_DAY_TEST = 8;
    protected final int TP_TYPE_WEEK_TEST = 9;

    protected int mTpType = TP_TYPE_SMART_STUDY;
    private final static String TAG = "[TitlePaperBaseActivity]";
    private HttpJSONRequest request;
    private boolean cCheck = false;//확인 버튼 클릭여부
    private boolean reCheck = false;//onresume 상태 체크

    private int mLastId = -1;
    private int mFirstId = -1;
    private int mStateCnt = 0;

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        new Handler().postDelayed(new Runnable() {//바로 안넘어가게 조치
            @Override
            public void run() {
                reCheck = true;
            }
        }, 1000);

    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                findViewById(R.id.btn_ok).setPressed(true);
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                findViewById(R.id.btn_ok).setPressed(false);
                goActivity(false);
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        if (singleProcessChecker())
            return;

        int id = view.getId();

        if (R.id.btn_ok == id || R.id.titlepaper_middle == id || R.id.titlepaper_bottom == id)
            goActivity(false);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (!hasFocus) {
            return;
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int[] point = new int[2];
                View rl = findViewById(R.id.study_length);
                View fp = null;
                if (mFirstId >= 0) {
                    fp = findViewById(mFirstId);
                }
                View lp = null;
                if (mLastId >= 0) {
                    lp = findViewById(mLastId);
                }
                View ch = findViewById(R.id.current_here);
                View wl = findViewById(R.id.white_line);

                View bt = findViewById(R.id.befly_talk);
                View bb = findViewById(R.id.befly_bubble);

                //View av = findViewById(R.id.active_name);
                View bm = findViewById(R.id.befly_moving);

                if (rl == null || fp == null || lp == null
                        || ch == null || wl == null || bm == null) {
                    return;
                }
                ch.getLocationInWindow(point);
                RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) rl.getLayoutParams();
//                if (mLastId == bm.getId()) {
//                    lp1.width = point[0] - fp.getWidth() / 2;
//                } else {
//                    lp1.width = point[0];
//                }
                lp1.width = point[0] - ch.getWidth() / 2;

                Log.d("point : ", "" + point[0]);

                fp.getLocationInWindow(point);
                if (mFirstId == bm.getId()) {
                    lp1.leftMargin = point[0];
                } else {
                    lp1.leftMargin = point[0] + fp.getWidth() / 2;
                }
                rl.setLayoutParams(lp1);

                fp.getLocationInWindow(point);
                RelativeLayout.LayoutParams lp3 = (RelativeLayout.LayoutParams) wl.getLayoutParams();
                lp3.leftMargin = point[0] + fp.getWidth() / 2;
                lp.getLocationInWindow(point);
                lp3.width = point[0] - lp3.leftMargin + lp.getWidth() / 2;
                lp3.rightMargin = 0;
                wl.setLayoutParams(lp3);

                // 활동 준비 간지
                if (bt != null && bm != null) {
                    bm.getLocationInWindow(point);
                    RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) bt.getLayoutParams();
                    RelativeLayout.LayoutParams lp4 = (RelativeLayout.LayoutParams) bb.getLayoutParams();
                    if ((getResources().getConfiguration().screenLayout
                            & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                        lp2.width = (int) (getResources().getDimensionPixelSize(R.dimen.dimen_172));
                    } else {
                        lp2.width = getResources().getDimensionPixelSize(R.dimen.dimen_172);
                    }
                    lp4.width = (int) (lp2.width);
                    lp2.leftMargin = point[0] - lp2.width + 40;
                    lp4.leftMargin = point[0] - lp4.width + 10;
                    bt.setLayoutParams(lp2);
                    bb.setLayoutParams(lp4);
                }
            }
        }, 100);
    }

    /**
     * 간지화면의 타이틀 설정
     *
     * @param nType 간지화면 타입
     */
    protected void initTitlePaper(int nType) {

        mTpType = nType;

        // 타이틀 바의 속성 설정
        StudyData data = StudyData.getInstance();
        if (null != data) {
            if (!data.mIsAudioExist && nType == TP_TYPE_EXAM_PREPARING) {
                goActivity(true);
                return;
            }
            if (null != data.mProductName)
                setTitlebarText(data.mProductName);
        }

        switch (mTpType) {
            case TP_TYPE_DAY_TEST:
            case TP_TYPE_WEEK_TEST:
                setTitlebarCategory(getString(R.string.string_titlebar_category_test));
                break;
            default:
                setTitlebarCategory(getString(R.string.string_titlebar_category_study));
                break;
        }

        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        // 간지 아이콘 설정
        setTitlePaperIcon();

        // 간지 텍스트 설정
        setTitlePaper();

        findViewById(R.id.titlepaper_middle).setOnClickListener(this);
        findViewById(R.id.titlepaper_bottom).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);

        if (mTpType == TP_TYPE_DAY_TEST || mTpType == TP_TYPE_WEEK_TEST) {
            int datacnt = StudyData.getInstance().mOneWeekData.size();
            if (datacnt > 0) {
                ImageView iv = ((ImageView) findViewById(R.id.current_here_txt));
                if (datacnt == 4) {// 일일평가
                    if (iv != null) {
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_today_txt_09));
                    }
                } else {// 주간평가
                    if (iv != null) {
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_today_txt_10));
                    }
                }
            }
        } else {
            int datacnt = StudyData.getInstance().mOneWeekData.size();
            if (datacnt > 0) {
                ImageView iv = (ImageView) ((LinearLayout) findViewById(R.id.last_point)).getChildAt(1);
                if (datacnt == 4) {// 일일평가
                    if (iv != null) {
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_today_txt_09));
                    }
                } else {// 주간평가
                    if (iv != null) {
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_today_txt_10));
                    }
                }
            }

        }

        mStateCnt = 0;

        if (findViewById(R.id.iv_restudy_point) != null) {
            if (data.mIsReviewExist) {
                mLastId = R.id.iv_restudy_point;
                mFirstId = R.id.iv_restudy_point;
                mStateCnt++;
                if (CommonUtil.isCenter()) {
                    ((ImageView) findViewById(R.id.iv_restudy_point))
                            .setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                } else {
                    ((ImageView) findViewById(R.id.iv_restudy_point))
                            .setImageDrawable(getResources().getDrawable(
                                    "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                }
            } else {
                ((LinearLayout) findViewById(R.id.iv_restudy_point).getParent().getParent()).setVisibility(View.GONE);
            }
        }

        if (findViewById(R.id.iv_smart_point) != null) {
            if (mTpType < TP_TYPE_SMART_STUDY) {
                if (data.mIsAudioExist) {
                    mLastId = R.id.iv_smart_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_smart_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_smart_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mIsAudioExist) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_smart_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_smart_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_smart_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_smart_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_ready_point) != null) {
            if (mTpType < TP_TYPE_EXAM_PREPARING) {
                if (data.mIsAudioExist
                        && (data.mWordQuestion.size() > 0 || data.mSentenceQuestion.size() > 0 || (data.mIsParagraph && data.mVanishingQuestion.size() > 0)
                        || (data.mIsDictation && data.mDictation.size() > 0))) {
                    mLastId = R.id.iv_ready_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_ready_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_ready_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mIsAudioExist
                        && (data.mWordQuestion.size() > 0 || data.mSentenceQuestion.size() > 0 || (data.mIsParagraph && data.mVanishingQuestion.size() > 0)
                        || (data.mIsDictation && data.mDictation.size() > 0))) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_ready_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_ready_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_ready_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_ready_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_wp_point) != null) {
            if (mTpType < TP_TYPE_WORD) {
                if (data.mWordQuestion.size() > 0) {
                    mLastId = R.id.iv_wp_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_wp_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_wp_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mWordQuestion.size() > 0) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_wp_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_wp_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_wp_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_wp_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_sp_point) != null) {
            if (mTpType < TP_TYPE_SENTENCE) {
                if (data.mSentenceQuestion.size() > 0) {
                    mLastId = R.id.iv_sp_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_sp_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_sp_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mSentenceQuestion.size() > 0) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_sp_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_sp_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_sp_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_sp_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_vip_point) != null) {
            if (mTpType < TP_TYPE_VANISHING) {
                if (data.mIsParagraph && data.mVanishingQuestion.size() > 0) {
                    mLastId = R.id.iv_vip_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_vip_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_vip_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mIsParagraph && data.mVanishingQuestion.size() > 0) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_vip_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_vip_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_vip_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_vip_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_dict_point) != null) {
            if (mTpType < TP_TYPE_DICTATION) {
                if (data.mIsDictation && data.mDictation.size() > 0 || (data.isNewDication && data.mIsDicPirvate)) {
                    mLastId = R.id.iv_dict_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_dict_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_dict_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mIsDictation && data.mDictation.size() > 0 || (data.isNewDication && data.mIsDicPirvate)) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_dict_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_dict_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_dict_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_dict_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_movie_point) != null) {
            if (mTpType < TP_TYPE_MOVIE_REVIEW) {
                if (data.mIsMovieExist) {
                    mLastId = R.id.iv_movie_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_movie_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_movie_point).getParent().getParent()).setVisibility(View.GONE);
                }
            } else {
                if (data.mIsMovieExist) {
                    mStateCnt++;
                    if (mFirstId == -1) {
                        mFirstId = R.id.iv_movie_point;
                    }
                    if (CommonUtil.isCenter()) {
                        ((ImageView) findViewById(R.id.iv_movie_point)).setImageDrawable(getResources().getDrawable(R.drawable.igse_img_cover_chapter_complete));
                    } else {
                        ((ImageView) findViewById(R.id.iv_movie_point))
                                .setImageDrawable(getResources().getDrawable(
                                        "1".equals(Preferences.getLmsStatus(this)) ? R.drawable.w_img_cover_chapter_complete : R.drawable.f_img_cover_chapter_complete));
                    }
                } else {
                    ((LinearLayout) findViewById(R.id.iv_movie_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (findViewById(R.id.iv_day_point) != null) {
            if (mTpType < TP_TYPE_DAY_TEST || mTpType < TP_TYPE_WEEK_TEST) {
                if (StudyData.getInstance().mOneWeekData.size() > 0) {
                    mLastId = R.id.iv_day_point;
                    mStateCnt++;
                    ((ImageView) findViewById(R.id.iv_day_point)).setImageDrawable(getResources().getDrawable(R.drawable.c_img_cover_chapter_normal));
                } else {
                    ((LinearLayout) findViewById(R.id.iv_day_point).getParent().getParent()).setVisibility(View.GONE);
                }
            }
        }

        if (mFirstId == -1) {
            mFirstId = R.id.befly_moving;
        }

        if (mLastId == -1) {
            mLastId = R.id.befly_moving;
        }

        if (mStateCnt == 0) {
            if (findViewById(R.id.study_length) != null) {
                findViewById(R.id.study_length).setVisibility(View.GONE);
            }

            if (findViewById(R.id.white_line) != null) {
                findViewById(R.id.white_line).setVisibility(View.GONE);
            }
        }

        // 간지 가이드 음원 출력
        if (!getIntent().getBooleanExtra(ServiceCommon.PARAM_MUTE_GUIDE, false))
            playGuideMessage();
    }

    /**
     * 간지 화면별 타이틀 문구 취득
     *
     * @return 타이틀 리소스 아이디 반환
     */
    private int getTitlePaperTitle() {

        int title = R.string.string_common_smart_study;

        switch (mTpType) {
            case TP_TYPE_SMART_STUDY:
                title = R.string.string_common_smart_study;
                break;
            case TP_TYPE_EXAM_PREPARING:
                title = R.string.string_common_exam_preparing;
                break;
            case TP_TYPE_WORD:
                title = R.string.string_common_vocabulary_exam;
                break;
            case TP_TYPE_SENTENCE:
                title = R.string.string_common_sentence_exam;
                break;
            case TP_TYPE_VANISHING:
                title = R.string.string_common_vanishing_exam;
                break;
            case TP_TYPE_DICTATION:
                title = R.string.string_common_dictation;
                break;
            case TP_TYPE_MOVIE_REVIEW:
                title = R.string.string_common_movie_review;
                break;
            case TP_TYPE_DAY_TEST:
                title = R.string.string_common_day_test;
                break;
            case TP_TYPE_WEEK_TEST:
                title = R.string.string_common_week_test;
                break;
            case TP_TYPE_NEW_DICTATION:
                title = R.string.string_common_dictation;
                break;
        }

        return title;
    }

    /**
     * 간지 화면별 가이드 문구 취득
     *
     * @return 가이드 리소스 아이디 반환
     */
    private int getDeviceOnlyGuideText() {

        int guide = -1;

        switch (mTpType) {
            case TP_TYPE_SMART_STUDY:
                guide = R.string.string_device_only_smart_restudy_message;
                break;
            case TP_TYPE_WORD:
                guide = R.string.string_device_only_word_message;
                break;
            case TP_TYPE_DICTATION:
                guide = R.string.string_device_only_dictation_message;
                break;
            case TP_TYPE_NEW_DICTATION:
                guide = R.string.string_device_only_dictation_message;
                break;
        }

        return guide;
    }

    protected void setTitlePaper() {
        if (mTpType == TP_TYPE_WORD || mTpType == TP_TYPE_SENTENCE || mTpType == TP_TYPE_VANISHING) {
            setTitlePaperImage();

        } else {
            setTitlePaperTitle();
        }

        if (CommonUtil.isSMT330Device()) {
            int guideText = getDeviceOnlyGuideText();

            if (guideText != -1) {
                TextView tv1 = findViewById(R.id.text_guide);
                //TextView tv2 = (TextView) findViewById(R.id.text_guide_nouse);
                tv1.setText(guideText);
                //tv2.setText(guideText);
                tv1.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 간지 화면별 타이틀 내용 설정
     */
    protected void setTitlePaperTitle() {
        MaxSizeTextView tv = findViewById(R.id.gangi_name);
        if (null == tv)
            return;
        tv.setText(getTitlePaperTitle());
        tv.setVisibility(View.VISIBLE);
    }

    /**
     * 간지 화면별 타이틀 이미지 설정 (텍스트로 표현 불가능한 처리를 위함)
     */
    protected void setTitlePaperImage() {
        int id = R.id.gangi_name_wp;
        switch (mTpType) {
            case TP_TYPE_WORD:
                id = R.id.gangi_name_wp;
                break;
            case TP_TYPE_SENTENCE:
                id = R.id.gangi_name_sp;
                break;
            case TP_TYPE_VANISHING:
                id = R.id.gangi_name_vip;
                break;
        }

        ImageView iv = findViewById(id);
        if (null == iv)
            return;
        iv.setVisibility(View.VISIBLE);
    }

    /**
     * 간지 화면별 이미지 리소스 아이디 취득
     *
     * @return int 케릭터 아이콘 이미지의 리소스 아이디
     */
    protected int getGanjiIconId() {

        int id = R.id.ganji_smart_study_icon;

        switch (mTpType) {
            case TP_TYPE_SMART_STUDY:
                id = R.id.ganji_smart_study_icon;
                break;
            case TP_TYPE_EXAM_PREPARING:
                id = R.id.ganji_exam_perparing_icon;
                break;
            case TP_TYPE_WORD:
                id = R.id.ganji_vocabulary_exam_icon;
                break;
            case TP_TYPE_SENTENCE:
                id = R.id.ganji_sentence_exam_icon;
                break;
            case TP_TYPE_VANISHING:
                id = R.id.ganji_vanishing_exam_icon;
                break;
            case TP_TYPE_DICTATION:
                id = R.id.ganji_dictation_icon;
                break;
            case TP_TYPE_MOVIE_REVIEW:
                id = R.id.ganji_movie_review_icon;
                break;
            case TP_TYPE_DAY_TEST:
            case TP_TYPE_WEEK_TEST:
                id = R.id.ganji_dictation_icon;
                break;
        }

        return id;
    }

    /**
     * 간지 화면별 이미지 설정
     */
    protected void setTitlePaperIcon() {

        ImageView iv = findViewById(getGanjiIconId());
        if (null == iv)
            return;

        iv.setVisibility(View.VISIBLE);
    }

    /**
     * 간지 화면별 가이드 음원 재생
     */
    protected void playGuideMessage() {

        switch (mTpType) {
            case TP_TYPE_SMART_STUDY:
                setGuideMsg(R.raw.b_23);
                break;
            case TP_TYPE_EXAM_PREPARING:
                setGuideMsg(R.raw.b_24);
                break;
            case TP_TYPE_WORD:
                setGuideMsg(R.raw.b_25);
                break;
            case TP_TYPE_SENTENCE:
                setGuideMsg(R.raw.b_26);
                break;
            case TP_TYPE_VANISHING:
                setGuideMsg(R.raw.b_32);
                break;
            case TP_TYPE_DICTATION:
            case TP_TYPE_NEW_DICTATION:
                setGuideMsg(R.raw.b_27);
                break;
            case TP_TYPE_MOVIE_REVIEW:

                break;
        }
    }

    /**
     * 문장학습, 동영상 리뷰로 이동 시 안내 문구 출력<br>
     * ("인터넷이 연결된 상태로 학습해야 최상의 효과가 있고 학습 시간도 짧아지므로, 꼭 인터넷에 연결 상태로 학습하시기 바랍니다.")
     */
    protected void showNetworkNotiDialog() {
        mMsgBox = new MessageBox(this, 0,
                mTpType == TP_TYPE_SENTENCE || mTpType == TP_TYPE_VANISHING
                        ? R.string.string_msg_please_using_network_noti
                        : R.string.string_msg_movie_review_title_paper_network_noti);
        if (mTpType == TP_TYPE_SENTENCE || mTpType == TP_TYPE_VANISHING) {
            mMsgBox.setConfirmText(R.string.string_offline_study);
            mMsgBox.setCancelText(R.string.string_network_setting);
        } else {
            mMsgBox.setConfirmText(R.string.string_common_confirm);
        }
        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    @Override
    public void onDialogDismiss(int result, int dialogId) {
        if (result == BaseDialog.DIALOG_CANCEL) {
            mExitInBackground = false;
            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            intent.putExtra("extra_prefs_show_button_bar", true);
            intent.putExtra("extra_prefs_set_next_text", "");
            intent.putExtra("extra_prefs_set_back_text", getString(R.string.string_common_close)); // 닫기
            startActivity(intent);
        } else {
            if (mTpType == TP_TYPE_SENTENCE)
                goActivity(new Intent(this, SentenceExamActivity.class));
            else if (mTpType == TP_TYPE_VANISHING)
                goActivity(new Intent(this, VanishingExamActivity.class));
            else
                goActivity(new Intent(this, StudyOutcomeActivity.class));
        }
    }

    /**
     * 간지 화면에서 "OK"버튼 선택 시 각각의 다음 화면으로 이동시킴
     *
     * @param pass 버튼으로 넘기는게 아닌 엑티비티에서 넘길때
     */

    protected void goActivity(boolean pass) {
        if ((!cCheck && reCheck) || pass) {
            cCheck = true;//한번만 실행

            switch (mTpType) {
                case TP_TYPE_SMART_STUDY:
                    goActivity(new Intent(this, SmartStudyActivity.class));
                    break;

                case TP_TYPE_EXAM_PREPARING:
                    //goActivity(new Intent(this, ExamPreparingActivity.class));
                    goActivity(new Intent(this, WordTitlePaperActivity.class));
                    break;
                case TP_TYPE_WORD:
                    goActivity(new Intent(this, WordExamActivity.class));
                    break;
                case TP_TYPE_SENTENCE:
                    if (CommonUtil.isAvailableNetwork(this, false)) {
                        goActivity(new Intent(this, SentenceExamActivity.class));
                    } else {
                        showNetworkNotiDialog();
                        return;
                    }
                    break;
                case TP_TYPE_VANISHING:
                    if (CommonUtil.isAvailableNetwork(this, false)) {
                        goActivity(new Intent(this, VanishingExamActivity.class));
                    } else {
                        showNetworkNotiDialog();
                        return;
                    }
                    break;
                case TP_TYPE_DICTATION:
                    goActivity(new Intent(this, DictationActivity.class));
                    break;

                case TP_TYPE_MOVIE_REVIEW:
                    if (CommonUtil.isAvailableNetwork(this, false)) {
                        goActivity(new Intent(this, MovieReviewActivity.class));
                    } else {
                        showNetworkNotiDialog();
                        return;
                    }
                    break;

                case TP_TYPE_DAY_TEST:
                case TP_TYPE_WEEK_TEST:
                    goActivity(new Intent(this, OneWeekTestActivity.class));
                    break;
                case TP_TYPE_NEW_DICTATION:
                    goActivity(new Intent(this, DictationNewActivity.class));
                    break;
            }
        }
    }

    /**
     * 다음 화면으로의 이동에 관한 처리 담당
     *
     * @param intent Intent
     */
    protected void goActivity(Intent intent) {
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * if (getIntent().getBooleanExtra(ServiceCommon.PARAM_GO_SETTING,
         * false)) startActivity(new Intent(this, SettingsActivity.class));
         */
        request = new HttpJSONRequest(this);
        request.requestNoti(mSyncHandler, ServiceCommon.MSG_WHAT_CALL_NOTI);

    }

    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mSyncHandler, true, false);
        }
    }

    public Handler mSyncHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START:
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS:
                    Log.e(TAG, "MSG_DATABASE_UPLOAD_SYNC_SUCCESS");
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL:
                    Log.e(TAG, "MSG_DATABASE_UPLOAD_SYNC_FAIL");
                    break;

            }
            switch (msg.arg1) {
                case ServiceCommon.MSG_WHAT_CALL_NOTI:

                    String status = "";

                    if (msg.obj != null) {
                        JSONObject obj = (JSONObject) (msg.obj);
                        try {
                            JSONArray array = new JSONArray(obj.getString("out1"));

                            for (int i = 0; i < array.length(); i++) {

                                status = array.getJSONObject(i).getString("command");
                                if (status.equals("MANAGE") || StudyData.getInstance().call) {

                                    ani_box = new AnimationBox(mContext, R.anim.ani_call);
                                    ani_box.setConfirmText(R.string.string_common_confirm);

                                    if (!ani_box.isShowing()) {
                                        ani_box.show();
                                        guidePlay(R.raw.p_call);
                                    }
                                    // sendBroadcast(new
                                    // Intent(ServiceCommon.ACTION_NOTI_FOUCS));
                                    StudyData.getInstance().call = false;
                                } else if (status.equals("KEEPGOING")) {// 취소일때
                                    StudyData.getInstance().call = false;
                                } else if (status.equals("INITIALIZE")) {//중복로그인
                                    ((Activity) mContext).finish();
                                }

                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        /* first_Check=false; */
                    }

                    break;
            }

            super.handleMessage(msg);
        }
    };
}
