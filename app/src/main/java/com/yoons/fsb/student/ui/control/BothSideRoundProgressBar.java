package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.yoons.fsb.student.R;

/**
 * 스마트 베플리 Progressbar
 * 
 * @author jaek
 */
public class BothSideRoundProgressBar extends ProgressBar {

	public BothSideRoundProgressBar(Context context) {
		super(context);
	}
	
	public BothSideRoundProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	  
	public BothSideRoundProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected synchronized void onDraw(Canvas canvas) {
		updateProgressBar();
	    super.onDraw(canvas);
	}
	  
	@Override
	public synchronized void setProgress(int progress) {
		super.setProgress(progress);	  
		invalidate();
	}
	
	/**
	 * scale 값을 계산하여 반환함
	 * 
	 * @param progress 진행 수치 
	 * @return scale값 
	 */
	private float getScale(int progress) {
		float scale = getMax() > 0 ? (float) progress / (float) getMax() : 0; 
		return scale;
	}
	
	/**
	 * Progressbar를 업데이트함 
	 * scale이 0이거나 Progressbar의 right 값이 15 이하 인경우 표시하지 않음
	 */
	private void updateProgressBar() {
		Drawable progressDrawable = getProgressDrawable();
		if (progressDrawable != null && progressDrawable instanceof LayerDrawable) {
			LayerDrawable ld = (LayerDrawable) progressDrawable;
			
			final float scale = getScale(getProgress());
			
			Drawable progressBar = ld.findDrawableByLayerId(R.id.progress);
			  
			final int width = ld.getBounds().right - ld.getBounds().left;
			
			if (progressBar != null) {
				Rect progressBarBounds = progressBar.getBounds();
				int right = progressBarBounds.left + (int) (width * scale + 0.5f);
				if (scale == 0f || right <= 15) 
					progressBarBounds.right = 0;
				else {
					if (right < 40)
						progressBarBounds.right = 40;
					else
						progressBarBounds.right = right;
				}
//				Log.e("", "progressBarBounds.right => " + progressBarBounds.right);
				progressBar.setBounds(progressBarBounds);
			}
		}
	}
}
