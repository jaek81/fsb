package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.yoons.fsb.student.R;

public class CornerRoundImageView extends ImageView {

    public static float radius = 8.0f;
    private boolean mTopLeft = false, mTopRight = false, mBottomRight = false, mBottomLeft = false;

    public CornerRoundImageView(Context context) {
        super(context);
    }

    public CornerRoundImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.corner_values_attr);
            mTopLeft = ta.getBoolean(R.styleable.corner_values_attr_topleft, false);
            mTopRight = ta.getBoolean(R.styleable.corner_values_attr_topright, false);
            mBottomRight = ta.getBoolean(R.styleable.corner_values_attr_bottomright, false);
            mBottomLeft = ta.getBoolean(R.styleable.corner_values_attr_bottomleft, false);
        }
    }

    public CornerRoundImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.corner_values_attr);
            mTopLeft = ta.getBoolean(R.styleable.corner_values_attr_topleft, false);
            mTopRight = ta.getBoolean(R.styleable.corner_values_attr_topright, false);
            mBottomRight = ta.getBoolean(R.styleable.corner_values_attr_bottomright, false);
            mBottomLeft = ta.getBoolean(R.styleable.corner_values_attr_bottomleft, false);
        }
    }

    private Path getPath(float radius, boolean topLeft, boolean topRight, boolean bottomRight, boolean bottomLeft) {

        final Path path = new Path();
        final float[] radii = new float[8];

        if (topLeft) {
            radii[0] = radius;
            radii[1] = radius;
        }

        if (topRight) {
            radii[2] = radius;
            radii[3] = radius;
        }

        if (bottomRight) {
            radii[4] = radius;
            radii[5] = radius;
        }

        if (bottomLeft) {
            radii[6] = radius;
            radii[7] = radius;
        }

        path.addRoundRect(new RectF(0, 0, getWidth(), getHeight()),
                radii, Path.Direction.CW);

        return path;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Path clipPath = null;

        clipPath = getPath(radius, mTopLeft, mTopRight, mBottomRight, mBottomLeft);

        canvas.clipPath(clipPath);
        super.onDraw(canvas);
    }
}