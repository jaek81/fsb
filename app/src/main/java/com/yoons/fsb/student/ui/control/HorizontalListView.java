package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Scroller;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 가로 스크롤 뷰
 * @author ejlee
 *
 */
public class HorizontalListView extends AdapterView<ListAdapter> {

	private static final int LOCK_SCROLL_DEMAN = 20;
	private static final float FIING_VELOCITY_FORCE = 1.0f;
	private static final int MSG_ADJUST_ITEM = 0x00ffff;
	
	protected ListAdapter mAdapter;
	private int mLeftViewIndex = -1;
	private int mRightViewIndex = 0;
	protected int mCurrentX;
	protected int mNextX;
	private int mMaxX = Integer.MAX_VALUE;
	private int mDisplayOffset = 0;
	private int mSpacing = 0;
	private float mDiff = 0;
	private boolean mIsLock = false;
	private ScrollView mDisScrollView;
	private ListView mDisListView;
	protected Scroller mScroller;
	private GestureDetector mGesture;
	private Queue<View> mRemovedViewQueue = new LinkedList<View>();
	private OnItemSelectedListener mOnItemSelected;
	private OnItemClickListener mOnItemClicked;
	private OnItemLongClickListener mOnItemLongClicked;
	private boolean mDataChanged = false;
	private boolean mMaxChanged = true;
	private float mInitialX;
	private float mInitialY;
	private boolean mIsAutoFit = true;

	public HorizontalListView(Context context) {
		super(context);
		initView();
	}
	
	public HorizontalListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	private synchronized void initView() {
		mLeftViewIndex = -1;
		mRightViewIndex = 0;
		mDisplayOffset = 0;
		mCurrentX = 0;
		mNextX = 0;
		mMaxX = Integer.MAX_VALUE;
		mScroller = new Scroller(getContext());
		mGesture = new GestureDetector(getContext(), mOnGesture);
		mMaxChanged = true;
	}
	
	public void setDisScrollView(ScrollView scroll) {
		mDisScrollView = scroll;
	}
	
	public void setDisListView(ListView scroll) {
		mDisListView = scroll;
	}
	
	public void setSpacing(int spacing) {
		if (mSpacing < 0) {
			mSpacing = 0;
		}
		
		mSpacing = spacing;
	}
	
	public void setAutoFit(boolean isAutoFit) {
		mIsAutoFit = isAutoFit;
	}

	@Override
	public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
		mOnItemSelected = listener;
	}

	@Override
	public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
		mOnItemClicked = listener;
	}

	@Override
	public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener) {
		mOnItemLongClicked = listener;
	}

	private DataSetObserver mDataObserver = new DataSetObserver() {

		@Override
		public void onChanged() {
			synchronized (HorizontalListView.this) {
				mDataChanged = true;
				mMaxChanged = true;
			}
			invalidate();
			requestLayout();
		}

		@Override
		public void onInvalidated() {
			reset();
			invalidate();
			requestLayout();
		}

	};

	@Override
	public ListAdapter getAdapter() {
		return mAdapter;
	}

	@Override
	public View getSelectedView() {
		// TODO: implement
		return null;
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		if (mAdapter != null) {
			mAdapter.unregisterDataSetObserver(mDataObserver);
		}
		mAdapter = adapter;
		mAdapter.registerDataSetObserver(mDataObserver);
		reset();
	}
	
	public int getSpacing() {
		return mSpacing;
	}

	private synchronized void reset() {
		initView();
		removeAllViewsInLayout();
		requestLayout();
	}

	@Override
	public void setSelection(int position) {
		// TODO: implement
		
	}

	private void addAndMeasureChild(final View child, int viewPos) {
		LayoutParams params = child.getLayoutParams();
		if (params == null) {
			params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		}

		addViewInLayout(child, viewPos, params, true);
		child.measure(MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.AT_MOST));
	}

	@Override
	protected synchronized void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if (mAdapter == null) {
			return;
		}

		if (mDataChanged) {
			mMaxX = Integer.MAX_VALUE;
			mNextX = mCurrentX;
			mDataChanged = false;
		}

		if (mScroller.computeScrollOffset()) {
			int scrollx = mScroller.getCurrX();
			mNextX = scrollx;
		}

		if (mNextX <= 0) {
			mNextX = 0;
			mScroller.forceFinished(true);
		}
		if (mNextX >= mMaxX) {
			mNextX = mMaxX;
			mScroller.forceFinished(true);
		}

		int dx = mCurrentX - mNextX;

		removeNonVisibleItems(dx);
		fillList(dx);
		positionItems(dx);

		mCurrentX = mNextX;

		if (!mScroller.isFinished()) {
			post(new Runnable() {
				@Override
				public void run() {
					
					requestLayout();
				}
			});
		}
	}
	
	public void invalidateChild(int cnt) {
		if (mAdapter == null) {
			return;
		}
		
		int childCount = getChildCount();
		if (cnt > childCount) {
			return;
		}
		
		for(int index = (childCount - cnt); index < childCount; index++) {
			View v = getChildAt(index);
            removeViewInLayout(v);
            
            v = mAdapter.getView((mLeftViewIndex + (index + 1)), v, this);
            addAndMeasureChild(v, index);
		}
	}

	private void fillList(final int dx) {
		int edge = 0;
		View child = getChildAt(getChildCount() - 1);
		if (child != null) {
			edge = child.getRight();
		}
		fillListRight(edge, dx);

		edge = 0;
		child = getChildAt(0);
		if (child != null) {
			edge = child.getLeft();
		}
		fillListLeft(edge, dx);
		
		if (mMaxChanged && child != null) {
			mMaxX = child.getMeasuredWidth() * mAdapter.getCount() - getWidth() + (getWidth() / (child.getMeasuredWidth() + mSpacing)) * mSpacing;
			mMaxChanged = false;
		}

		if (mMaxX < 0) {
			mMaxX = 0;
		}
	}

	private void fillListRight(int rightEdge, final int dx) {
		int width = getWidth();
		int itemCount = mAdapter.getCount();
		
		while (rightEdge + dx < width && mRightViewIndex < itemCount) {
			View child = mAdapter.getView(mRightViewIndex, mRemovedViewQueue.poll(), this);
			addAndMeasureChild(child, -1);
			
			int childWidth = child.getMeasuredWidth();
			rightEdge += childWidth;
			
			mRightViewIndex++;
		}

	}

	private void fillListLeft(int leftEdge, final int dx) {
		while (leftEdge + dx > 0 && mLeftViewIndex >= 0) {
			View child = mAdapter.getView(mLeftViewIndex, mRemovedViewQueue.poll(), this);
			addAndMeasureChild(child, 0);
			leftEdge -= child.getMeasuredWidth();
			mLeftViewIndex--;
			mDisplayOffset -= child.getMeasuredWidth();
		}
	}
	
	public interface OnRemoveViewListener  {
		public void onRemove(View view);
	}
	
	private OnRemoveViewListener mRemoveListener = null;
	
	public void setRemoveViewListener(OnRemoveViewListener li) {
		mRemoveListener = li;
	}

	private void removeNonVisibleItems(final int dx) {
		View child = getChildAt(0);
		while (child != null && child.getRight() + mSpacing + dx < 0) {
			mDisplayOffset += child.getMeasuredWidth();
			mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			mLeftViewIndex++;
			
			if (mRemoveListener != null) {
				mRemoveListener.onRemove(child);
			}
			
			child = getChildAt(0);

		}

		child = getChildAt(getChildCount() - 1);
		while (child != null && child.getLeft() - mSpacing + dx >= getWidth()) {
			mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			mRightViewIndex--;
			
			if (mRemoveListener != null) {
				mRemoveListener.onRemove(child);
			}
			
			child = getChildAt(getChildCount() - 1);
		}
	}

	private void positionItems(final int dx) {
		if (getChildCount() > 0) {
			mDisplayOffset += dx;
			int left = mDisplayOffset;
			for (int i = 0; i < getChildCount(); i++) {
				View child = getChildAt(i);
				int childWidth = 0;
				childWidth = child.getMeasuredWidth();
				child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
				left += (childWidth + mSpacing);
			}
		}
	}
	
	public int getCurrentX() {
		return mCurrentX;
	}

	public synchronized void scrollTo(int x) {
		mScroller.startScroll(mNextX, 0, x - mNextX, 0);
		requestLayout();
	}
	
	public synchronized void scrollTo(int x, int duration) {
		mScroller.startScroll(mNextX, 0, x - mNextX, 0, duration);
		requestLayout();
	}

	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (ev.getAction() == MotionEvent.ACTION_DOWN) {
			mDiff = ev.getX();
		} else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
			
			if (mDisListView != null && !mIsLock) {
				if (Math.abs(mDiff - ev.getX()) > LOCK_SCROLL_DEMAN) {
					mDisListView.requestDisallowInterceptTouchEvent(true);
					mIsLock = true;
				}
			} else if(mDisScrollView != null && !mIsLock) {
				if (Math.abs(mDiff - ev.getX()) > LOCK_SCROLL_DEMAN) {
					mDisScrollView.requestDisallowInterceptTouchEvent(true);
					mIsLock = true;
				}
			}
		} else if (ev.getAction() == MotionEvent.ACTION_UP) {
			if (mDisListView != null) {
				mDisListView.requestDisallowInterceptTouchEvent(false);
				mIsLock = false;
				
				sendMessageDelayedAdjustItem(100, 0);
				
			} else if (mDisScrollView != null) {
				mDisScrollView.requestDisallowInterceptTouchEvent(false);
				mIsLock = false;
				
				sendMessageDelayedAdjustItem(100, 0);				
			}
		}

		boolean handled = mGesture.onTouchEvent(ev);
		if (mIsLock) {
			return handled;
		}

		return super.dispatchTouchEvent(ev);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {

		switch (ev.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
			mInitialX = ev.getX();
			mInitialY = ev.getY();
			return false;
		case MotionEvent.ACTION_MOVE:
			float deltaX = Math.abs(ev.getX() - mInitialX);
			float deltaY = Math.abs(ev.getY() - mInitialY);
			return (deltaX > 5 || deltaY > 5);
		case MotionEvent.ACTION_UP:
			return false;
		default:
			return super.onInterceptTouchEvent(ev);
		}
	}

	protected boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		synchronized (HorizontalListView.this) {
			mScroller.fling(mNextX, 0, (int) -(velocityX * FIING_VELOCITY_FORCE), 0, -Integer.MAX_VALUE, Integer.MAX_VALUE, 0, 0);
		}
		requestLayout();

		sendMessageDelayedAdjustItem(mScroller.getDuration(), 0);
		return true;
	}

	protected boolean onDown(MotionEvent e) {
		mScroller.forceFinished(true);
		return true;
	}
	

	private OnGestureListener mOnGesture = new GestureDetector.SimpleOnGestureListener() {

		@Override
		public boolean onDown(MotionEvent e) {
			return HorizontalListView.this.onDown(e);
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			Rect viewRect = new Rect();
			for (int i = 0; i < getChildCount(); i++) {
				View child = getChildAt(i);
				int left = child.getLeft();
				int right = child.getRight();
				int top = child.getTop();
				int bottom = child.getBottom();
				viewRect.set(left, top, right, bottom);
				if (viewRect.contains((int) e.getX(), (int) e.getY())) {
					if (mOnItemClicked != null) {
						mOnItemClicked
								.onItemClick(HorizontalListView.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId(mLeftViewIndex + 1 + i));
						playSoundEffect(SoundEffectConstants.CLICK);
					}
					if (mOnItemSelected != null) {
						mOnItemSelected.onItemSelected(HorizontalListView.this, child, mLeftViewIndex + 1 + i,
								mAdapter.getItemId(mLeftViewIndex + 1 + i));
					}
					break;
				}

			}
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			return HorizontalListView.this.onFling(e1, e2, velocityX, velocityY);
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			synchronized (HorizontalListView.this) {
				mNextX += (int) distanceX;
			}
			
			requestLayout();

			sendMessageDelayedAdjustItem(100, 0);			
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			return super.onSingleTapConfirmed(e);
		}

		@Override
		public void onLongPress(MotionEvent e) {
			Rect viewRect = new Rect();
			int childCount = getChildCount();
			for (int i = 0; i < childCount; i++) {
				View child = getChildAt(i);
				int left = child.getLeft();
				int right = child.getRight();
				int top = child.getTop();
				int bottom = child.getBottom();
				viewRect.set(left, top, right, bottom);
				if (viewRect.contains((int) e.getX(), (int) e.getY())) {
					if (mOnItemLongClicked != null) {
						mOnItemLongClicked.onItemLongClick(HorizontalListView.this, child, mLeftViewIndex + 1 + i,
								mAdapter.getItemId(mLeftViewIndex + 1 + i));
					}
					break;
				}

			}
		}
	};
	
	/**
	 * @param delay 
	 * 	대기시간
	 * @param state
	 * 	상태변화에 따른 동작 설정
	 * */
	public void sendMessageDelayedAdjustItem(int delay, int state) {
		if (!mIsAutoFit) {
			return;
		}
		
		mHandler.removeMessages(MSG_ADJUST_ITEM);
		mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_ADJUST_ITEM, state, 0), delay);
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if(msg.what == MSG_ADJUST_ITEM) {
				if(mIsLock) 
					return;
				
				if(getChildCount() != 0) {
					View view = getChildAt(0);
					Rect rect = new Rect();
					Point point = new Point();
					int width = view.getWidth();
					int lastItemScrollX = (mAdapter.getCount() - (getWidth() / width)) * width - (getWidth() % (width  + mSpacing));
					int firstItemIndex = mNextX / width;
					int maxItemCount = getWidth() / (width + mSpacing) + 1;
					if(getWidth() % (width + mSpacing) >= mSpacing * 2)
						maxItemCount += 1;
					
					int absOffset = Math.abs(mDisplayOffset);
					int diff = Math.abs(width - absOffset);
					if((width > absOffset) && getWidth() % (width + mSpacing) < diff)
						maxItemCount -= 1;
					
					getChildVisibleRect(view, rect, point);
					
					//마지막 아이템에 걸려 있을때 
					if(mNextX >= lastItemScrollX) {
						scrollTo(lastItemScrollX);
					} else {
						// 첫번째 아이템이 반이상이 왼쪽으로 스크롤 되어 있다.
						if((absOffset > (width / 2) && diff >= mSpacing) || msg.arg1 == 1) {						 	
							//두번째 아이템에 줄 맞춤.
							if(maxItemCount == getChildCount()) {
								scrollTo((firstItemIndex + 1) * width + mSpacing - 1);
							} else {
								scrollTo((firstItemIndex + 1) * width);
							}
						} else {
							if(absOffset >= width) {
								scrollTo(firstItemIndex * width + mSpacing - 1);
							} else {
								scrollTo(firstItemIndex * width);
							}
						}
					}					
				}
			}
			
			super.handleMessage(msg);
		}
	};
}