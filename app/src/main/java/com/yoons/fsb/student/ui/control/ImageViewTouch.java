package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageViewTouch extends ImageViewTouchBase implements OnGestureListener, OnDoubleTapListener {

	private static final String LOG_TAG = "ImageViewTouch";

	public static final float MIN_SCALE = 1f;
	public static final float MAX_SCALE = 5f;
	public static final float ZOOM_DURATION = 400;

	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 150;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;

	public static final int ACTION_PREV_PAGE = 100;
	public static final int ACTION_NEXT_PAGE = 101;
	public static final int ACTION_NORMAL_SCREEN = 102;

	// private final ViewImage mViewImage;
	private boolean mEnableTrackballScroll;

	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();

	// We can be in one of these 3 states
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;

	private Context mContext = null;;
	private Handler mHandler;
	private byte[] mImageData;
	private Bitmap mBitmap;
	private int mOrientation;
	private int mPage;
	private int mPageIndex = 0;
	private int mTotalPage = 0;
	private GestureDetector mGestureDetector;

	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();

	float oldDist = 1f;
	float oldScale = 1f;

	Paint mPaint = null;
	Paint mPaint2 = null;
	Paint mPaint3 = null;

	private boolean mShowScale = false;

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (mShowScale) {
			if (mPaint == null) {
				mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
				mPaint.setTextAlign(Paint.Align.CENTER);
				mPaint.setTextSize(30);
				mPaint.setColor(Color.RED);
				mPaint.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
			}

			canvas.drawText(Float.toString(getScale() * 100), 100, 100, mPaint);
		}

		if (mPaint2 == null) {
			mPaint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
			mPaint2.setTextAlign(Paint.Align.CENTER);
			mPaint2.setTextSize(30);
			// mPaint2.setColor(Color.LTGRAY);
			mPaint2.setColor(Color.argb(200, 255, 255, 255));
			mPaint2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
		}

		if (mPaint3 == null) {
			mPaint3 = new Paint(Paint.ANTI_ALIAS_FLAG);
			mPaint3.setTextAlign(Paint.Align.CENTER);
			mPaint3.setTextSize(25);
			mPaint3.setColor(Color.argb(255, 50, 50, 50));
			mPaint3.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
		}

		if (mTotalPage != 0) {
			int width = getWidth();
			RectF rect = new RectF(width - 120, 20, width - 20, 60);
			// RectF rect = new RectF(width - 120, 70, width - 20, 110);
			canvas.drawRoundRect(rect, 4, 4, mPaint2);

			if (mTotalPage < 100)
				canvas.drawText(String.format("%d / %d", mPageIndex, mTotalPage), width - 70, 50, mPaint3);
			else
				canvas.drawText(String.format("%d/%d", mPageIndex, mTotalPage), width - 70, 50, mPaint3);
		}

	}

	public ImageViewTouch(Context context) {
		super(context);
		
		mContext = context;
		initView();
	}

	public ImageViewTouch(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mContext = context;
		initView();
	}

	public void setHandler(Handler handler) {
		mHandler = handler;
	}

	private void initView() {
		mMaxZoom = MAX_SCALE;
		mGestureDetector = new GestureDetector(this);
	}

	public void setOrientation(int orientation) {
		if (mOrientation != orientation) {
			mOrientation = orientation;
			resizeBitmap();
		}
	}

	public void clearBitmap() {
		setImageBitmap(null);
	}

	public void resizeBitmap() {
		if (mImageData == null || mImageData.length == 0)
			return;

		boolean needRotate = true;

		clearBitmap();

		Bitmap bitmap = BitmapFactory.decodeByteArray(mImageData, 0, mImageData.length);

		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		Log.e("ImageViewTouch", "[resizeBitmap] W = " + width);
		Log.e("ImageViewTouch", "[resizeBitmap] H = " + height);

		Matrix matrix = new Matrix();

		if (mOrientation == Configuration.ORIENTATION_PORTRAIT) {
			if (width > height) { // 640 480
				matrix.setRotate(90);
			} else
				needRotate = false;
		} else {
			if (height > width) {
				matrix.setRotate(270);
			} else
				needRotate = false;
		}

		needRotate = false;

		if (needRotate)
			mBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
		else
			mBitmap = bitmap;

		setImageBitmap(mBitmap);
	}

	public boolean setPage(int page, String imagePath) {
		mPage = page;

		mPageIndex = page;
		
		try {
			mImageData = getFileBytes(imagePath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		resizeBitmap();

		if (mImageData == null) {
			return false;
		}

		return true;
	}
	
	// throw OutOfMemoryError
/*	public boolean setPage(int page, String imagePath) {
		mPage = page;

		mPageIndex = page;
		
		try {
			mImageData = getFileBytes(new File(imagePath));
		} catch (IOException e) {
			e.printStackTrace();
		}

		resizeBitmap();

		if (mImageData == null) {
			return false;
		}

		return true;
	}
	*/
	public byte[] getFileBytes(String fileName) throws IOException {
	    ByteArrayOutputStream ous = null;
	    InputStream ios = null;
	    try {
	        byte[] buffer = new byte[4096];
	        ous = new ByteArrayOutputStream();
	        ios = mContext.getResources().getAssets().open(fileName);
	        
	        int read = 0;
	        while ((read = ios.read(buffer)) != -1)
	            ous.write(buffer, 0, read);
	    } finally {
	        try {
	            if (ous != null)
	                ous.close();
	        } catch (IOException e) {
	            // swallow, since not that important
	        }
	        try {
	            if (ios != null)
	                ios.close();
	        } catch (IOException e) {
	            // swallow, since not that important
	        }
	    }
	    return ous.toByteArray();
	}

	public void setTotalPage(int page) {
		mTotalPage = page;
	}

	private void sendMessageParent(int msgType) {
		Message msg = mHandler.obtainMessage(msgType, null);
		mHandler.sendMessage(msg);
	}

	public void setEnableTrackballScroll(boolean enable) {
		mEnableTrackballScroll = enable;
	}

	protected void postTranslateCenter(float dx, float dy) {
		super.postTranslate(dx, dy);
		center(true, true);
	}

	public void zoomToMin() {
		float px, py;
		px = getWidth() / 2f;
		py = getHeight() / 2f;
		zoomTo(MIN_SCALE, px, py, ZOOM_DURATION);
	}

	public void zoomToMax(float px, float py) {
		zoomTo(MAX_SCALE, px, py, ZOOM_DURATION);
	}

	public void zoomToMax() {
		float px, py;
		px = getWidth() / 2f;
		py = getHeight() / 2f;
		zoomTo(MAX_SCALE, px, py, ZOOM_DURATION);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// dumpEvent(event);

		int action = event.getAction() & MotionEvent.ACTION_MASK;

		switch (action) {
			case MotionEvent.ACTION_MOVE:
				if (mode == DRAG) {
					postTranslateCenter(event.getX() - start.x, event.getY() - start.y);
					start.set(event.getX(), event.getY());
				} else if (mode == ZOOM) {
					if (event.getPointerCount() > 1) {
						float newDist = spacing(event);
						if (newDist > 10f) {
							float scale = (newDist / oldDist) * oldScale;

							if (scale < MIN_SCALE) {
								scale = MIN_SCALE;
							} else if (scale > MAX_SCALE) {
								scale = MAX_SCALE;
							}

							zoomTo(scale);
						}
					}
				}
				break;
			case MotionEvent.ACTION_DOWN:
				savedMatrix.set(matrix);
				start.set(event.getX(), event.getY());
				mode = DRAG;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				oldDist = spacing(event);
				if (oldDist > 10f) {
					savedMatrix.set(matrix);
					midPoint(mid, event);
					mode = ZOOM;
					oldScale = getScale();
				}
				break;
			case MotionEvent.ACTION_UP:
				mode = NONE;
				break;
			case MotionEvent.ACTION_POINTER_UP:
				savedMatrix.set(matrix);
				start.set(event.getX(), event.getY());
				mode = DRAG;
				break;

		}

		return this.mGestureDetector.onTouchEvent(event);
		// return true;
	}

	public boolean onTouch(View view, MotionEvent event) {
		// TODO Auto-generated method stub
		return this.onTouchEvent(event);
	}

	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float)Math.sqrt(x * x + y * y);
	}

	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	/** Show an event in the LogCat view, for debugging */
	private void dumpEvent(MotionEvent event) {
		// ...
		String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
		StringBuilder sb = new StringBuilder();
		int action = event.getAction();
		int actionCode = action & MotionEvent.ACTION_MASK;
		sb.append("event ACTION_").append(names[actionCode]);
		if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
			sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
			sb.append(")");
		}
		sb.append("[");
		for (int i = 0; i < event.getPointerCount(); i++) {
			sb.append("#").append(i);
			sb.append("(pid ").append(event.getPointerId(i));
			sb.append(")=").append((int) event.getX(i));
			sb.append(",").append((int) event.getY(i));
			if (i + 1 < event.getPointerCount())
				sb.append(";");
		}
		sb.append("]");

		Log.d(LOG_TAG, sb.toString());
	}

	// GestureDetector Method
	public boolean onSingleTapUp(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onSingleTapUp] " +
		// ev.toString());
		sendMessageParent(ACTION_NORMAL_SCREEN);
		return true;
	}

	public void onShowPress(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onShowPress] " +
		// ev.toString());
	}

	public void onLongPress(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onLongPress] " +
		// ev.toString());
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onScroll] " +
		// e1.toString());
		return true;
	}

	public boolean onDown(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onDown] " +
		// ev.toString());
		return true;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onFling] e1=" +
		// e1.toString());
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onFling] e2=" +
		// e2.toString());

		try {

			if (getScale() <= MIN_SCALE) {
				// down to up swipe
				if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
					sendMessageParent(ACTION_NEXT_PAGE);
				}

				// up to down swipe
				else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
					sendMessageParent(ACTION_PREV_PAGE);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean onDoubleTap(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onDoubleTap] " +
		// ev.toString());

		if (getScale() > MIN_SCALE)
			zoomToMin();
		else {
			zoomTo(2.0f, getWidth() / 2f, getHeight() / 2f, ZOOM_DURATION);
		}

		return true;
	}

	public boolean onDoubleTapEvent(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onDoubleTapEvent] " +
		// ev.toString());
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent ev) {
		// LogUtil.e("ImageViewTouch", "[ImageViewer::onSingleTapConfirmed] " +
		// ev.toString());
		return true;
	}

}
