package com.yoons.fsb.student.ui.control;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.service.DownloadServiceAgent;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

/**
 * 화면 상단의 타이틀바 영역으로 사용될 레이아웃을 컨트롤화 함 좌측(사용자이름 및 홈 버튼), 중앙(타이틀 명), 우측(환경설정 버튼)을
 * 설정함.
 *
 * @author dckim
 */
public class Indicator extends LinearLayout implements OnClickListener {

    private TextView mTitle = null, mCategory = null, mUserName = null;
    private ImageView mSensitiveIcon, mAutoArtt = null;
    private LinearLayout mLayoutLeft = null, mLayoutCategory = null;

    private boolean mIsDetached = false, mIsVisit = true;

    private Context mContext = null;

    private MessageBox mMsgBox;

    // 학습 데이터
    protected StudyData mStudyData = null;

    public Indicator(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public Indicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public Indicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init() {

        View.inflate(getContext(), R.layout.layout_titlebar, this);
        mAutoArtt = (ImageView) findViewById(R.id.titlebar_mIsAutoArtt);
        mSensitiveIcon = (ImageView) findViewById(R.id.titlebar_sensitive_icon);

        mStudyData = StudyData.getInstance();

        Log.d("autoartt", "" + mStudyData.mIsAutoArtt);

        /*if (mStudyData.mIsAutoArtt) {
            mAutoArtt.setVisibility(View.VISIBLE);
        } else {
            mAutoArtt.setVisibility(View.GONE);
        }*/

		/*mOfflineLinear.setVisibility(View.INVISIBLE);
        mOfflineImage.setVisibility(View.INVISIBLE);*/
    }

    /**
     * 블루투스 전원의 On/Off 상태 감시 설정
     */
    public void reflectChangeStatus() {
        //setBluetoothIcon(CommonUtil.isBluetoothEnabled(getContext()));
    }

    @Override
    protected void onDetachedFromWindow() {

        mIsDetached = true;

        super.onDetachedFromWindow();
    }

    /**
     * 타이틀 바의 중앙 부분에 표시할 문자열 입력
     * <p>
     * 표시할 문자열
     */
    public void setTitle(String title) {
        if (null != mTitle && null != title)
            mTitle.setText(title);
    }

    /**
     * 타이틀 바의 중앙 부분에 표시할 문자열 리소스의 아이디 입력
     * <p>
     * 표시할 문자열 리소스의 아이디
     */
    public void setTitle(int title) {
        if (null != mTitle && 0 != title)
            mTitle.setText(title);
    }

    /**
     * 타이틀 바의 좌측 문자열 노출 여부 및 문자열 설정
     *
     * @param show 노출 여부
     */
    public void showCategory(boolean show, String category) {

        if (null == mLayoutLeft)
            return;

        if (!show) {
            mLayoutLeft.setVisibility(View.INVISIBLE);
            return;
        }

        mLayoutLeft.setVisibility(View.VISIBLE);
        mLayoutCategory.setVisibility(View.VISIBLE);

        if (null != mCategory)
            mCategory.setText(category);
    }

    /**
     * 홈 버튼 노출 유무 설정
     *
     * @param show     노출 여부
     * @param listener listener를 등록하면 버튼 클릭 시 입력된 listener 동작함.(null이면 default 동작임)
     */
    public void showHomeButton(boolean show, OnClickListener listener) {

        if (null == mLayoutLeft)
            return;

        if (!show) {
            mLayoutLeft.setVisibility(View.INVISIBLE);
            return;
        }

        mLayoutLeft.setVisibility(View.VISIBLE);
        mLayoutCategory.setVisibility(View.GONE);
    }

    /**
     * 타이틀 바의 좌측에 사용자 이름을 표시함
     *
     * @param show 노출 여부
     */
    public void setUserName(boolean show) {

        if (null == mUserName)
            return;

        if (!show) {
            mUserName.setVisibility(View.INVISIBLE);
            return;
        }

        String name = Preferences.getCustomerName(getContext());
        if (null != name && 0 < name.length()) {
            mUserName.setVisibility(View.VISIBLE);
            mUserName.setText(String.format("[%s]", name));
        }
    }

    /**
     * 네트워크 감도 아이콘 표시
     */
    public void setSensitiveIcon() {
        LinearLayout.LayoutParams params = (LayoutParams) findViewById(R.id.titlebar_sensitive_icon).getLayoutParams();

        if (Preferences.getNetworkType(getContext()) == ConnectivityManager.TYPE_ETHERNET) {
            mSensitiveIcon.setImageDrawable(getResources().getDrawable(R.drawable.c_icon_indicator_lancable));
            params.width = CommonUtil.convertPx(18, mContext);
            findViewById(R.id.titlebar_sensitive_icon).setLayoutParams(params);

        } else if (Preferences.getNetworkType(getContext()) == ConnectivityManager.TYPE_WIFI) {
            mSensitiveIcon.setImageDrawable(getResources().getDrawable(R.drawable.c_icon_indicator_wifi_on));
            params.width = CommonUtil.convertPx(36, mContext);
            findViewById(R.id.titlebar_sensitive_icon).setLayoutParams(params);

        } else if (Preferences.getNetworkType(getContext()) == ConnectivityManager.TYPE_MOBILE) {
            mSensitiveIcon.setImageDrawable(getResources().getDrawable(R.drawable.c_icon_indicator_lte_on));
            params.width = CommonUtil.convertPx(36, mContext);
            findViewById(R.id.titlebar_sensitive_icon).setLayoutParams(params);
        } else {
            if (mContext != null && Preferences.getCustomerNo(mContext) != 0) { //인터넷끊기면 엑티비티 모두 종료
                Toast.makeText(mContext, "네트워크 연결이 불안정하여 학습앱이 종료됩니다.", Toast.LENGTH_SHORT).show();
                ((Activity) mContext).finish();
            }

            //mSensitiveIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_wifi_off));
            /* setTimerOffLine(); */
        }

        View parent = (View) mSensitiveIcon.getParent();
        CommonUtil.setSizeLayout(mContext, parent);
    }

    public interface CloseEvent {
        public void onClose(View v);
    }

    private CloseEvent mItemEvent = new CloseEvent() {

        @Override
        public void onClose(View v) {
            onClick(v);
        }
    };

    @Override
    public void onClick(View view) {
        try {
            if (mMsgBox == null) {
                mMsgBox = new MessageBox(mContext, 0, R.string.string_msg_close_app);
                mMsgBox.setConfirmText(R.string.string_common_confirm);
                mMsgBox.setCancelText(R.string.string_common_cancel);
                mMsgBox.setOnDialogDismissListener(new OnDialogDismissListener() {

                    @Override
                    public void onDialogDismiss(int result, int dialogId) {
                        // TODO Auto-generated method stub
                        if (BaseDialog.DIALOG_CONFIRM == result) {
                            StudyData data = StudyData.getInstance();
                            //학습앱 종료
                            Crashlytics.log(mContext.getString(R.string.string_common_close));
                            RequestPhpPost reqFCASE = new RequestPhpPost(mContext);
                            reqFCASE.sendCASE(String.valueOf(data.mStudyResultNo), "EX3");
                            ((Activity) mContext).finish();
                        } else {
                            mMsgBox = null;
                        }
                    }
                });
            }
            if (!mMsgBox.isShowing()) {
                mMsgBox.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
