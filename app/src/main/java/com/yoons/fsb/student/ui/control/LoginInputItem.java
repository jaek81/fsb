package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ui.LoginActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

public class LoginInputItem extends FrameLayout {
    public static int mSelectNum = 0, mOldSelectNum = 0;
    private LoginActivity.LoginItemEvent mListener;
    private Context mContext;
    private EditText numedit;

    public LoginInputItem(@NonNull Context context) {
        super(context);
        mContext = context;
        init();
    }

    public LoginInputItem(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public LoginInputItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    public void init() {
        View item = LayoutInflater.from(mContext).inflate(R.layout.login_input_item, this);

        item.findViewById(R.id.num_up).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onUpDown(true);
                }
            }
        });

        item.findViewById(R.id.num_down).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onUpDown(false);
                }
            }
        });

        numedit = ((EditText) findViewById(R.id.num_edit));

        if (CommonUtil.isCenter()) // igse
            numedit.setBackground(getResources().getDrawable(R.drawable.igse_btn_login_num_bg));
        else
            numedit.setBackground(getResources().getDrawable("1".equals(Preferences.getLmsStatus(mContext)) ? R.drawable.w_btn_login_num_bg : R.drawable.f_btn_login_num_bg));

        numedit.setInputType(0);
        numedit.setSelected(false);
        numedit.setTextColor(Color.parseColor("#bbbbbb"));

        numedit.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    findViewById(R.id.num_up).setVisibility(VISIBLE);
                    findViewById(R.id.num_down).setVisibility(VISIBLE);

                    if (CommonUtil.isCenter()) // igse
                        v.setBackground(getResources().getDrawable(R.drawable.igse_btn_login_num_bg));
                    else {
                        if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                            v.setBackground(getResources().getDrawable(R.drawable.w_btn_login_num_bg));
                        else // 숲
                            v.setBackground(getResources().getDrawable(R.drawable.f_btn_login_num_bg));
                    }
                    ((EditText) v).setTextColor(Color.parseColor("#000000"));
                } else {
                    findViewById(R.id.num_up).setVisibility(INVISIBLE);
                    findViewById(R.id.num_down).setVisibility(INVISIBLE);

                    if (CommonUtil.isCenter()) {
                        // igse
                        v.setBackground(getResources().getDrawable(R.drawable.igse_btn_login_solid_bg));
                    } else {
                        if ("1".equals(Preferences.getLmsStatus(mContext))) {
                            //우영
                            v.setBackground(getResources().getDrawable(R.drawable.w_btn_login_solid_bg));
                        } else {
                            // 숲
                            v.setBackground(getResources().getDrawable(R.drawable.f_btn_login_solid_bg));
                        }
                    }
                    ((EditText) v).setTextColor(Color.parseColor("#ffffff"));
                }
            }
        });

        numedit.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int id = 0;
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        id = LoginInputItem.this.getId();
                        if (id == R.id.num1_edit) {
                            mSelectNum = 0;
                        } else if (id == R.id.num2_edit) {
                            mSelectNum = 1;
                        } else if (id == R.id.num3_edit) {
                            mSelectNum = 2;
                        } else {
                            mSelectNum = 3;
                        }

                        if (mListener != null) {
                            if (mOldSelectNum < mSelectNum) {
                                mListener.onSelect(mSelectNum, false);
                            } else {
                                mListener.onSelect(mSelectNum, true);
                            }
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        id = LoginInputItem.this.getId();
                        if (id == R.id.num1_edit) {
                            mSelectNum = 0;
                            mOldSelectNum = 0;
                        } else if (id == R.id.num2_edit) {
                            mSelectNum = 1;
                            mOldSelectNum = 1;
                        } else if (id == R.id.num3_edit) {
                            mSelectNum = 2;
                            mOldSelectNum = 2;
                        } else {
                            mSelectNum = 3;
                            mOldSelectNum = 3;
                        }
                        break;
                }
                return false;
            }
        });
    }

    public void setInterface(LoginActivity.LoginItemEvent listener) {
        mListener = listener;
    }

    public void setPressed(boolean bool) {
        numedit.setPressed(bool);
    }

    public void setText(String str) {
        numedit.setText(str);
    }

    public String getText() {
        return numedit.getText().toString();
    }

    public void setEnable(boolean val) {
        numedit.setEnabled(val);
    }
}
