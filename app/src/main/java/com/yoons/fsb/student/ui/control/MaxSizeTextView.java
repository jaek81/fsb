package com.yoons.fsb.student.ui.control;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * 임의의 크기의 TextView에 글씨가 꽉 차도록 조정함(간지 텍스트에만 사용) 
 * @author ejlee
 *
 */
public class MaxSizeTextView extends TextView {

	public MaxSizeTextView(Context context) {
		super(context);
	}
	
	public MaxSizeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setIncludeFontPadding(false);
	}
	
	public MaxSizeTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		Rect rt = new Rect();
		getDrawingRect(rt);
		this.setTextSize(TypedValue.COMPLEX_UNIT_PX, rt.height() - 6);
		super.onDraw(canvas);
	}

}
