package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.data.StudyData;

import java.util.ArrayList;

/**
 * @brief 학습 단계 상태를 노출(복습 > 오디오 학습 > 시험준비 > 단어시험 > 문장시험 > 받아쓰기> 평가)
 * @date 2013.05.30
 * @author dckim
 */
public class StepStatusBar extends LinearLayout {

	/** 복습 */
	public static final int STATUS_REVIEW = 0x01;
	/** 오디오 학습 */
	public static final int STATUS_SMART_STUDY = 0x02;
	/** 시험준비 */
	public static final int STATUS_EXAM_PREPARING = 0x04;
	/** 단어시험 */
	public static final int STATUS_WORD_EXAM = 0x08;
	/** 문장시험 */
	public static final int STATUS_SENTENCE_EXAM = 0x10;

	/** 문단연습 */
	public static final int STATUS_VANISHING_EXAM = 0x20;
	/** 받아쓰기 */
	public static final int STATUS_DICTATION = 0x40;
	/** 받아쓰기 */
	public static final int STATUS_MOVIE_REVIEW = 0x80;
	// 일일주간
	public static final int STATUS_ONE_WEEK_TEST = 256;

	public static final int STATUS_ALL = STATUS_REVIEW | STATUS_SMART_STUDY | STATUS_EXAM_PREPARING | STATUS_WORD_EXAM
			| STATUS_SENTENCE_EXAM | STATUS_VANISHING_EXAM | STATUS_DICTATION | STATUS_MOVIE_REVIEW
			| STATUS_ONE_WEEK_TEST;

	private ArrayList<Float> mOrgSizes = new ArrayList<Float>();
	private float mOrgSum = 0;
	private LinearLayout mLayout = null;
	private int mStatusCnt = 0;

	public StepStatusBar(Context context) {
		super(context);
		init();
	}

	public StepStatusBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public StepStatusBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {

		View.inflate(getContext(), R.layout.layout_study_status, this);

		// ImageView를 제외하고 나머지(TextView)는 기존 고정 비율로 조절한다.
		mLayout = (LinearLayout) findViewById(R.id.learning_status_layout);
		if (null == mLayout)
			return;

		if (0 < mOrgSizes.size())
			return;

		int status = getStepStatus();

		for (int i = 0; i < mLayout.getChildCount(); i++) {
			View v = mLayout.getChildAt(i);

			if (null == v)
				continue;

			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
			if (v instanceof TextView) {
				if (4 < mStatusCnt) {
					((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX,
							getResources().getDimensionPixelSize(R.dimen.dimen_18));
					lp.weight /= 1.2;
				}

				mOrgSizes.add(lp.weight);
				mOrgSum += lp.weight;
			}
		}

		setStepInfo(status);
	}

	public int getStepStatus() {
		StudyData data = StudyData.getInstance();
		int status = 0;
		// int status = StepStatusBar.STATUS_SMART_STUDY; //오디오 학습은 무조건 있음.
		// mStatusCnt++;

		if (data.mIsReviewExist) {
			status |= StepStatusBar.STATUS_REVIEW;
			mStatusCnt++;
		}

		if (data.mIsAudioExist) {
			status |= StepStatusBar.STATUS_SMART_STUDY;
			mStatusCnt++;

			if (data.mWordQuestion.size() > 0 || data.mSentenceQuestion.size() > 0) {
				status |= StepStatusBar.STATUS_EXAM_PREPARING;
				mStatusCnt++;
			}
		}

		if (data.mWordQuestion.size() > 0) {
			status |= StepStatusBar.STATUS_WORD_EXAM;
			mStatusCnt++;
		}

		if (data.mSentenceQuestion.size() > 0) {
			status |= StepStatusBar.STATUS_SENTENCE_EXAM;
			mStatusCnt++;
		}

		if (data.mIsParagraph && data.mVanishingQuestion.size() > 0) {
			status |= StepStatusBar.STATUS_VANISHING_EXAM;
			mStatusCnt++;
		}

		if ((data.mIsDictation && data.mDictation.size() > 0) || (data.isNewDication&&data.mIsDicPirvate)) {
			status |= StepStatusBar.STATUS_DICTATION;
			mStatusCnt++;
		}

		if (data.mIsMovieExist) {
			status |= StepStatusBar.STATUS_MOVIE_REVIEW;
			mStatusCnt++;
		}
		if (data.mOneWeekData != null && data.mOneWeekData.size() > 0) {
			status |= StepStatusBar.STATUS_ONE_WEEK_TEST;
			mStatusCnt++;
		}

		return status;
	}

	/**
	 * 노출할 단계를 설정함(복습 > 오디오 학습 > 시험준비 > 단어시험 > 문장시험 > 문단연습 >받아쓰기)
	 * 
	 * @param status
	 *            (ex, "STATUS_REVIEW | STATUS_AUDIO_LEARNING"는 "복습 > 오디오 학습"와
	 *            같이 출력함.
	 */
	public void setStepInfo(int status) {

		// STATUS_REVIEW을 시작으로 2배씩 증가하며 체크함.
		int acceptStatus = STATUS_REVIEW;
		int orgInx = 0;
		float rate = mOrgSum / getNewSum(status);

		if (null == mLayout)
			return;

		for (int i = 0; i < mLayout.getChildCount(); i++) {

			View v = mLayout.getChildAt(i);
			boolean isTV = (v instanceof TextView) ? true : false;

			if (0 < (status & acceptStatus)) {
				// Visible
				if (isTV) {
					LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) v.getLayoutParams();
					lp.weight = rate * mOrgSizes.get(orgInx);
					v.setLayoutParams(lp);
				}

				v.setVisibility((isTV || status >= (acceptStatus * 2)) ? View.VISIBLE : View.GONE);
			} else {
				// Gone
				v.setVisibility(View.GONE);
			}

			if (!isTV) {
				acceptStatus *= 2;
				orgInx++;
			}
		}
	}

	/**
	 * 새로 적용할 컨트롤들의 전체 너비(비율 계산에 사용됨)
	 * 
	 * @param status
	 *            노출할 학습 단계 정보 설정(ex, "STATUS_REVIEW | STATUS_AUDIO_LEARNING")
	 * @return int 전체 너비 값
	 */
	private int getNewSum(int status) {

		int ret = 0, inx = 0;
		// 복습
		if (0 < (STATUS_REVIEW & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx++);
		// 오디오 학습
		if (0 < (STATUS_SMART_STUDY & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx++);
		// 시험준비
		if (0 < (STATUS_EXAM_PREPARING & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx++);
		// 단어시험
		if (0 < (STATUS_WORD_EXAM & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx++);
		// 문장시험
		if (0 < (STATUS_SENTENCE_EXAM & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx++);
		// 문단엽습
		if (0 < (STATUS_VANISHING_EXAM & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx++);
		// 받아쓰기
		if (0 < (STATUS_DICTATION & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx);
		// 동영상 리뷰
		if (0 < (STATUS_MOVIE_REVIEW & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx);
		// 동영상 리뷰
		if (0 < (STATUS_ONE_WEEK_TEST & status) && inx < mOrgSizes.size())
			ret += mOrgSizes.get(inx);
		return ret;
	}

	/**
	 * 설정 된 "학습 단계" 중 활성단계를 설정함(다른 색상으로 표현됨)
	 * 
	 * @param status
	 *            (ex, STATUS_REVIEW or STATUS_AUDIO_LEARNING or ...)
	 */
	public void setHighlights(int status) {

		int acceptStatus = 1;

		if (null == mLayout)
			return;

		for (int i = 0; i < mLayout.getChildCount(); i++) {

			View v = mLayout.getChildAt(i);
			if (null == v)
				continue;

			boolean isTV = (v instanceof TextView) ? true : false;
			if (0 < (status & acceptStatus)) {
				if (isTV)
					((TextView) v).setTextColor(getResources().getColor(R.color.color_black));
			}

			if (isTV)
				acceptStatus *= 2;
		}
	}

}
