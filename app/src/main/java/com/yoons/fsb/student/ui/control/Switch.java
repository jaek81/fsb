package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.yoons.fsb.student.R;


/**
 * On/Off 스위치
 * @author dckim
 * @date 13.06.03
 */
public class Switch extends RelativeLayout implements OnClickListener {

	private LinearLayout mOnLayout = null;
	private LinearLayout mOffLayout = null;
	
	/**
	 * 스위치의 값 변화를 통지하는 리스너
	 * @author dckim
	 */
	public interface OnSwitchChangedListener {
		public void onSwitchChanged(boolean on, View v);
	}

	/**
	 * 스위치의 값 변화를 통지
	 */
	private OnSwitchChangedListener mSwitchChangedListener = null;
	
	/**
	 * 값 변화를 통지하는 리스너 등록
	 * @param li
	 */
	public void setOnSwitchChangedListener(OnSwitchChangedListener li) {
		mSwitchChangedListener = li;
	}
	
	public Switch(Context context) {
		super(context);
		init();
	}

	public Switch(Context context, AttributeSet attrs){
		super(context, attrs);
		init();
	}

	public Switch(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {

		View.inflate(getContext(), R.layout.layout_on_off_switch, this);
		mOnLayout = (LinearLayout)findViewById(R.id.switch_on_layout);
		mOffLayout = (LinearLayout)findViewById(R.id.switch_off_layout);
		setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		setOn(!isOn());
	}
	
	/**
	 * 스위치의 상태를 설정한다.
	 * @param isOn true이면 On 상태로 설정함.
	 */
	private void setValue(boolean isOn) {
		mOnLayout.setVisibility((isOn) ? View.VISIBLE : View.GONE);
		mOffLayout.setVisibility((isOn) ? View.GONE: View.VISIBLE);
	}

	/**
	 * 스위치의 상태를 반환한다.
	 * @return true이면 On 상태 임
	 */
	public boolean isOn() {
		return (View.VISIBLE == mOnLayout.getVisibility()) ? true : false;
	}
	
	/**
	 * On/Off의 상태를 변경한다.(Listener 호출 배제 됨)
	 * @param isOn
	 */
	public void setOnDefault(boolean isOn) {
		if (isOn() == isOn)
			return;

		setValue(isOn);
	}
	
	/**
	 * On/Off의 상태를 변경한다.(Listener가 등록된 경우 호출됨)
	 * @param isOn
	 */
	public void setOn(boolean isOn) {
		if (isOn() == isOn)
			return;

		setValue(isOn);

		if (null != mSwitchChangedListener)
			mSwitchChangedListener.onSwitchChanged(isOn, this);
	}
}
