package com.yoons.fsb.student.ui.control;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

public class TitleView extends LinearLayout implements OnClickListener {

    private Context mContext = null;
    private MessageBox mMsgBox = null;
    private Button btnclose, btnclose2, btnclose3  = null;

    public TitleView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public TitleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public TitleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    public void Test() {
        Log.d("test", " : TEST te+st");
    }

    private void init() {
        if (CommonUtil.isCenter()) // Igse
            View.inflate(getContext(), R.layout.igse_layout_titleview, this);
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                View.inflate(getContext(), R.layout.w_layout_titleview, this);
            else // 숲
                View.inflate(getContext(), R.layout.f_layout_titleview, this);
        }

        btnclose = findViewById(R.id.btn_close);
        btnclose2 = findViewById(R.id.btn_close2);
        btnclose3 = findViewById(R.id.btn_close3);

        btnclose.setOnClickListener(this);
        btnclose2.setOnClickListener(this);
        btnclose3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            if (mMsgBox == null) {
                mMsgBox = new MessageBox(mContext, 0, R.string.string_msg_close_app);
                mMsgBox.setConfirmText(R.string.string_common_confirm);
                mMsgBox.setCancelText(R.string.string_common_cancel);
                mMsgBox.setOnDialogDismissListener(new OnDialogDismissListener() {

                    @Override
                    public void onDialogDismiss(int result, int dialogId) {
                        // TODO Auto-generated method stub
                        if (BaseDialog.DIALOG_CONFIRM == result) {
                            StudyData data = StudyData.getInstance();
                            //학습앱 종료
                            Crashlytics.log(mContext.getString(R.string.string_common_close));
                            RequestPhpPost reqFCASE = new RequestPhpPost(mContext);
                            reqFCASE.sendCASE(String.valueOf(data.mStudyResultNo), "EX3");
                            ((Activity) mContext).finish();
                        } else {
                            mMsgBox = null;
                        }
                    }
                });
            }
            if (!mMsgBox.isShowing()) {
                mMsgBox.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
