package com.yoons.fsb.student.ui.control;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.yoons.fsb.student.R;


/**
 * 펼치지/접기 버튼용 컨트롤<br>
 * 비율로 이미지의 크기를 조절하기 위해 사용함
 * @author dckim
 */
public class ToggleImage extends ImageView implements OnClickListener {

	private boolean mIsOn = false;
	private Drawable mOn = null, mOff = null;

	/**
	 * 스위치의 값 변화를 통지하는 리스너
	 * @author dckim
	 */
	public interface OnChangedListener {
		public void onChanged(boolean on, View v);
	}
		
	/**
	 * 값 변화를 통지 리스너
	 */
	private OnChangedListener mListener = null;
	
	/**
	 * 값 변화를 통지하는 리스너 등록
	 * @param li
	 */
	public void setOnChangeListener(OnChangedListener li) {
		mListener = li;
	}
	
	public ToggleImage(Context context) {
		super(context);
		init(context, null);
	}

	public ToggleImage(Context context, AttributeSet attrs){
		super(context, attrs);
		init(context, attrs);
	}

	public ToggleImage(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}
	
	private void init(Context context, AttributeSet attrs) {

		int check = android.R.drawable.checkbox_on_background;
		int uncheck = android.R.drawable.checkbox_off_background;
		
		if (null != attrs) {

			TypedArray ar = context.obtainStyledAttributes(attrs, R.styleable.custom_attr);
			check = ar.getResourceId(R.styleable.custom_attr_check_image, android.R.drawable.checkbox_on_background);
			uncheck = ar.getResourceId(R.styleable.custom_attr_uncheck_image, android.R.drawable.checkbox_off_background);
	
			mIsOn = ar.getBoolean(R.styleable.custom_attr_checked, false);
		}

		mOn = context.getResources().getDrawable(check);
		mOff = context.getResources().getDrawable(uncheck);

		setOnDefault(mIsOn);
		setOnClickListener(this);
	}
	
	@Override
	public void onClick(View view) {
		setOn(!isOn());
	}
	
	/**
	 * Toggle 상태를 반환한다.
	 * @return true이면 On 상태 임
	 */
	public boolean isOn() {
		return mIsOn;
	}
	
	/**
	 * Toggle 상태를 설정한다.<br>
	 * On/Off의 상태를 변경한다.(Listener 호출 배제 됨)
	 * @param isOn true이면 On 상태로 설정함.
	 */
	public void setOnDefault(boolean isOn) {
		mIsOn = isOn;
		setImageDrawable(mIsOn ? mOn : mOff);
	}
	
	/**
	 * On/Off의 상태를 변경한다.(Listener가 등록된 경우 호출됨)
	 * @param isOn
	 */
	public void setOn(boolean isOn) {
		if (isOn() == isOn)
			return;

		setOnDefault(isOn);

		if (null != mListener)
			mListener.onChanged(mIsOn, this);
	}
	
}
