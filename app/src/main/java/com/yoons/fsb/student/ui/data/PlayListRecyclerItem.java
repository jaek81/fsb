package com.yoons.fsb.student.ui.data;

public class PlayListRecyclerItem implements Comparable<PlayListRecyclerItem> {

    private String m_imgageRes = "";
    private int m_pageNo = 0;

    public PlayListRecyclerItem(){
    }

    public PlayListRecyclerItem(String strImgPath, int pageNo){
        this.m_imgageRes = strImgPath;
        this.m_pageNo = pageNo;
    }

    public String getImage() {
        return m_imgageRes;
    }

    public void setImage(String simage){
        this.m_imgageRes = simage;
    }

    public int getPageNo() {
        return m_pageNo;
    }

    public void setPageNo(int pageNo) {
        m_pageNo = pageNo;
    }

    // 정렬 20181120
    @Override
    public int compareTo(PlayListRecyclerItem o) {
        return 0;
    }
}
