package com.yoons.fsb.student.ui.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ui.adapter.ReviewStudyUnitListAdapter;
import com.yoons.fsb.student.ui.control.HorizontalListView;
import com.yoons.fsb.student.util.ImageDownloader;
import com.yoons.fsb.student.util.ImageDownloader.DownloadCompleteListener;

import java.util.ArrayList;

/**
 * content test 교재 리스트 아이템
 * @author ejlee
 */
public class ReviewBookSelectListItem extends LinearLayout implements OnItemClickListener, OnClickListener, DownloadCompleteListener {
	private static final String TAG = "[ReviewBookSelectListItem]";
	
	private Context mContext =  null;
	private LinearLayout mMainLayout = null;
	private ImageView mThumbnail = null;
	private TextView mBookName = null;
	private ImageView mStudyCompleteIcon = null;
	private ImageView mArrowIcon = null;
	private HorizontalListView mUnitListView = null;
	private ReviewStudyUnitListAdapter mUnitListAdapter = null;
	public OnItemClickListener mItemClickListener = null; 
	private ImageDownloader mImageDownloader = null;
	
	private ItemInfo mItemInfo = null;

	public enum ItemClickType {
		CLICK_TYPE_BOOK,
		CLICK_TYPE_UNIT,
	}
	
	public interface OnItemClickListener {
		 void onItemClick(View v, ItemClickType type, int unitPos);
	}
	
	public static class ItemInfo extends Object implements Cloneable {
		public String 		mThumbnail = "";
		public String		mBookName = "";
		public boolean 	mIsAllStudied = false;
		public boolean 	mIsExpanded = false;
		
		public ArrayList<ReviewStudyUnitListItem.ItemInfo> mUnitList = new ArrayList<ReviewStudyUnitListItem.ItemInfo>();
		
		@Override
		public Object clone() {
			Object obj = null;
			
			try {
				obj = super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			
			return obj;
		}
	}
	
	public ReviewBookSelectListItem(Context context) {
		super(context);
		
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext); 
		mImageDownloader.setOnDownloadComplete(this);
		
		initLayout();
	}
	
	public ReviewBookSelectListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext); 
		mImageDownloader.setOnDownloadComplete(this);
		initLayout();
	}
	
	public void setOnItemClickListener(OnItemClickListener listener) {
		mItemClickListener = listener;
	}
	
	public void setDisListView(ListView listView) {
		mUnitListView.setDisListView(listView);
	}
	
	private void initLayout() {
		LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		layoutInflater.inflate(R.layout.layout_review_book_select_item, this, true);
		
		mMainLayout = (LinearLayout)findViewById(R.id.main_layout);
		mThumbnail = (ImageView)findViewById(R.id.thumbnail);
		mBookName = (TextView)findViewById(R.id.book_name);
		mStudyCompleteIcon = (ImageView)findViewById(R.id.study_complete_icon);
		mArrowIcon = (ImageView)findViewById(R.id.arrow_icon);
		
		setOrientation(VERTICAL);
		
		mUnitListView = new HorizontalListView(mContext);
		LinearLayout.LayoutParams listParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.dimen_80));
		mUnitListView.setLayoutParams(listParams);
		mUnitListView.setBackgroundColor(0xffffffff);
		mUnitListView.setAlwaysDrawnWithCacheEnabled(true);
		mUnitListView.setAnimationCacheEnabled(true);
		mUnitListView.setFocusable(false);
		mUnitListView.setFocusableInTouchMode(true);
		mUnitListView.setPersistentDrawingCache(PERSISTENT_ALL_CACHES);
		mUnitListView.setDuplicateParentStateEnabled(true);
		mUnitListView.setOnItemClickListener(this);
		mUnitListView.setClickable(true);
		
		mUnitListAdapter = new ReviewStudyUnitListAdapter(mContext);
		mUnitListView.setAdapter(mUnitListAdapter);
		
		addView(mUnitListView);
		
		mMainLayout.setOnClickListener(this);
	}
	
	public void setItemInfo(ItemInfo info) {
		boolean bThumbnailLoading = false;
		
		if(mItemInfo == null) {
			bThumbnailLoading = true;
		} else if(mItemInfo.mThumbnail.equals(info.mThumbnail) == false) {
			bThumbnailLoading = true;
		}
		
		if(bThumbnailLoading) {
			mThumbnail.setImageBitmap(null);
			mImageDownloader.download(info.mThumbnail, mThumbnail);
		}
		
		mItemInfo = (ItemInfo)info.clone();
		
		mBookName.setText(mItemInfo.mBookName);
		mStudyCompleteIcon.setVisibility(mItemInfo.mIsAllStudied?View.VISIBLE:View.INVISIBLE);
		mArrowIcon.setImageResource(mItemInfo.mIsExpanded?R.drawable.icon_arrow_right:R.drawable.icon_arrow_down);
		
		mUnitListView.setVisibility(mItemInfo.mIsExpanded?View.VISIBLE:View.GONE);
		mUnitListView.scrollTo(0, 0);
		mUnitListView.setAdapter(mUnitListAdapter);
		
		mUnitListAdapter.setItemInfoList(mItemInfo.mUnitList);	
		mUnitListAdapter.notifyDataSetChanged();	
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(action ==  MotionEvent.ACTION_DOWN) {
		
		} else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
		
		}
		
		return super.onTouchEvent(event);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mItemClickListener.onItemClick(view, ItemClickType.CLICK_TYPE_UNIT, position);
	}

	@Override
	public void onClick(View v) {		
		mItemClickListener.onItemClick(v, ItemClickType.CLICK_TYPE_BOOK, 0);
	}

	@Override
	public void onDownloadComplete(ImageView imageView, Bitmap bitmap) {
		try {
			if(bitmap == null) {
				mThumbnail.setImageResource(R.drawable.book_thumbnail_default);
			} else {
				mThumbnail.setImageBitmap(bitmap);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
