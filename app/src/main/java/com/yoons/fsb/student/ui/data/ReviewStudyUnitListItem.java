package com.yoons.fsb.student.ui.data;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.util.Log;

/**
 * content test 교재 차시 아이탬
 * @author ejlee
 *
 */
public class ReviewStudyUnitListItem extends LinearLayout {
	private Context mContext =  null;
	private LinearLayout mMainLayout = null;
	private ImageView mUnitNumberImageView = null;
	private TextView mUnitNumberTextView = null;
	private ProgressBar mProgressbar = null;

	private ItemInfo mItemInfo = null;

	public static class ItemInfo extends Object implements Cloneable {
		public int			mStudyUnitNumber = 1;
		public boolean 	mIsStudied = false;
		public boolean		mIsPossibleStudy = false;
		public int			mContentDownloadPercent = 0; //퍼센트 
		
		@Override
		public Object clone() {
			Object obj = null;
			
			try {
				obj = super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			
			return obj;
		}
	}
	
	public ReviewStudyUnitListItem(Context context) {
		super(context);
		
		mContext = context;
		initLayout();
	}
	
	public ReviewStudyUnitListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mContext = context;
		initLayout();
	}
	
	private void initLayout() {
		LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		layoutInflater.inflate(R.layout.layout_review_study_unit_item, this, true);
		
		mMainLayout = (LinearLayout)findViewById(R.id.main_layout);
		mUnitNumberImageView = (ImageView)findViewById(R.id.unit_number_imageview);
		mUnitNumberTextView = (TextView)findViewById(R.id.unit_number_textview);
		
		mProgressbar = (ProgressBar)findViewById(R.id.progressbar);
	}
	
	public void setItemInfo(ItemInfo info) {
		mItemInfo = (ItemInfo)info.clone();
		
		if(mItemInfo.mIsPossibleStudy) {
			mMainLayout.setBackgroundColor(0xffffffff);
			mUnitNumberImageView.setVisibility(View.VISIBLE);
			mUnitNumberTextView.setTextColor(0xffffffff);
			
			mUnitNumberImageView.setImageResource(mItemInfo.mIsStudied?R.drawable.unit_number_after_bg:R.drawable.unit_number_before_bg);
			mProgressbar.setVisibility(View.VISIBLE);
			mProgressbar.setProgress(mItemInfo.mContentDownloadPercent);
			Log.e("percent", "percent : " + mItemInfo.mContentDownloadPercent + " " + mProgressbar.getMax());
		} else {
			mMainLayout.setBackgroundColor(0xffc6c6c6);
			mUnitNumberImageView.setVisibility(View.GONE);
			mUnitNumberTextView.setTextColor(0xff9c9c9c);
			mProgressbar.setVisibility(View.INVISIBLE);
		}
		
		mUnitNumberTextView.setText(String.valueOf(mItemInfo.mStudyUnitNumber));
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//클릭 일때 가이드 적용 필요.
		int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(action ==  MotionEvent.ACTION_DOWN) {
		
		} else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
		
		}
		
		return super.onTouchEvent(event);
	}
}
