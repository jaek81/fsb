package com.yoons.fsb.student.ui.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.ImageDownloader;
import com.yoons.fsb.student.util.ImageDownloader.DownloadCompleteListener;
import com.yoons.fsb.student.util.Preferences;

/**
 * 교재 선택화면 교재 아이탬
 * @author ejlee
 */
public class StudyBookSelectListItem extends RelativeLayout implements DownloadCompleteListener, OnClickListener {
	private static final String TAG = "[StudyBookSelectListItem]";
	private Context mContext =  null;
	private ImageView mThumbnail = null;
	private TextView mBookName = null;
	private LinearLayout mStudyStoppedLayout = null;	// 중단 학습 layout
	private RelativeLayout mTodayStudyLayout = null;	// 오늘의 학습 layout
	private LinearLayout mWhiteDimmedLayout = null;	// 흰색 Layout (교재의 컨텐츠를 모두 받지 못했을 때 적용)
	private LinearLayout mSelectEffectLayout = null;	// 교재 선택 layout(블루투스 키보드 조작에 의해 선택 아이템이 적용)
	private LinearLayout mTouchEffectLayout = null;	// 손으로 터치시 효과 layout
	private LinearLayout mBlackDimmedLayout = null;	// 검은 layout (교재 다운로드시 다운로드 받지 않는 교재에 적용)
	private LinearLayout mDownloadProgressLayout = null;
	private TextView	  mDownloadPercent = null;
	private ProgressBar  mDownloadProgressBar = null;
	private LinearLayout mStudyCountLayout = null;
	private TextView	  mStudyCount = null;
	
	private ItemInfo mItemInfo = null;
	private ImageDownloader mImageDownloader = null;
	public OnBookItemClickListener mItemClickListener = null; 
	
	public static class ItemInfo extends Object implements Cloneable {
		public String 		mThumbnail = "";		// Thumbnail URL
		public String		mBookName = "";			// 교재 이름
		public String      mBookNo = "";
		public boolean 	mIsStudyStopped = false;	// 학습이 중단된 교재인지 여부
		public boolean 	mIsTodayStudy = false;		// 오늘의 학습인지 여부
		public boolean 	mIsDownloadComplete = false;	// 다운로드가 완료 되었는지 여부
		public boolean 	mIsBlackDimmed = false; // 다른 아이탬이 선택 되었을 때 true가 됨.
		public int 		mStudyUnitIndex = 0;	// 교재 정보에서의 차시 index
		public boolean		mIsDownloading = false;	// 다운로드 받고 있는지 여부
		public int			mDownloadPercent = 0;	// 다운로드 percent
		public int 		mStudyUnitCount = 0; 	// 오늘 학습할 차시 count
		public boolean		mIsSelect = false; // 블루투스 키 처리
		public boolean		mIsPress = false; // 블루투스 키 처리
		public int		mProductNo = 0; // 블루투스 키 처리
		public String		mStudyUnitCode = ""; // 블루투스 키 처리
		public int		mSeriseNo = 0; // 블루투스 키 처리
		public int		mBookDetailid = 0; // 
		public int		mStudentBookId = 0; // 
		//public int		mBookNo = 0; // 권
		public int mRealBookNo = 0;

		@Override
		public Object clone() {
			Object obj = null;
			
			try {
				obj = super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			
			return obj;
		}
	}
	
	public interface OnBookItemClickListener {
		 void onItemClick(View v);
	}
	
	public StudyBookSelectListItem(Context context) {
		super(context);
		
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext); 
		mImageDownloader.setOnDownloadComplete(this);
		
		initLayout();
	}
	
	public StudyBookSelectListItem(Context context, AttributeSet attrs) {
		super(context, attrs);
				
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext); 
		mImageDownloader.setOnDownloadComplete(this);
		
		initLayout();
	}
	
	private void initLayout() {
		LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (CommonUtil.isCenter()) // Igse
			layoutInflater.inflate(R.layout.igse_layout_study_book_select_item, this, true);
		else {
			if ("1".equals(Preferences.getLmsStatus(mContext))) //우영
				layoutInflater.inflate(R.layout.w_layout_study_book_select_item, this, true);
			else // 숲
				layoutInflater.inflate(R.layout.f_layout_study_book_select_item, this, true);
		}

		mThumbnail = (ImageView)findViewById(R.id.thumbnail);
		mBookName = (TextView)findViewById(R.id.book_name);
		mStudyStoppedLayout = (LinearLayout)findViewById(R.id.study_stopped_layout);
		mTodayStudyLayout = (RelativeLayout)findViewById(R.id.today_study_layout);
		mSelectEffectLayout = (LinearLayout)findViewById(R.id.select_effect_layout);
		mTouchEffectLayout = (LinearLayout)findViewById(R.id.touch_effect_layout);
		mWhiteDimmedLayout = (LinearLayout)findViewById(R.id.white_dimmed_layout);
		mBlackDimmedLayout = (LinearLayout)findViewById(R.id.black_dimmed_layout);
		mDownloadProgressLayout = (LinearLayout)findViewById(R.id.download_progress_layout);
		mDownloadPercent = (TextView)findViewById(R.id.download_percent);
		mDownloadProgressBar = (ProgressBar)findViewById(R.id.download_progressbar);
		mStudyCountLayout = (LinearLayout)findViewById(R.id.study_count_layout);
		mStudyCount = (TextView)findViewById(R.id.study_count);
		
		this.setOnClickListener(this);
	}
	
	public void setOnItemClickListener(OnBookItemClickListener listener) {
		mItemClickListener = listener;
	}
	
	public void setItemInfo(ItemInfo info) {
		boolean bThumbnailLoading = false;
		
		if(mItemInfo == null) {
			bThumbnailLoading = true;
		} else if(mItemInfo.mThumbnail.equals(info.mThumbnail) == false && !mItemInfo.mIsBlackDimmed && !mItemInfo.mIsDownloading) {
			bThumbnailLoading = true;
		}
		
		if(bThumbnailLoading) {
			mImageDownloader.download(info.mThumbnail, mThumbnail);
		}
		
		mItemInfo = (ItemInfo)info.clone();

		mBookName.setText(mItemInfo.mBookName);
		mSelectEffectLayout.setVisibility(mItemInfo.mIsSelect?View.VISIBLE:View.INVISIBLE);
		mStudyStoppedLayout.setVisibility(View.INVISIBLE);
		mTodayStudyLayout.setVisibility(
				
				
				
				View.INVISIBLE);
/*		mStudyStoppedLayout.setVisibility(mItemInfo.mIsStudyStopped?View.VISIBLE:View.INVISIBLE);
		mTodayStudyLayout.setVisibility(mItemInfo.mIsTodayStudy?View.VISIBLE:View.INVISIBLE);
*/		//mWhiteDimmedLayout.setVisibility(mItemInfo.mIsDownloadComplete?View.GONE:View.VISIBLE);
		mBlackDimmedLayout.setVisibility(mItemInfo.mIsBlackDimmed?View.VISIBLE:View.GONE);
		mDownloadProgressLayout.setVisibility(mItemInfo.mIsDownloading?View.VISIBLE:View.GONE);
		mDownloadPercent.setText(mItemInfo.mDownloadPercent + "%");
		mDownloadProgressBar.setProgress(mItemInfo.mDownloadPercent);
		
		mStudyCountLayout.setVisibility(mItemInfo.mStudyUnitCount == 0?View.GONE:View.VISIBLE);
		mStudyCount.setText(String.valueOf(mItemInfo.mStudyUnitCount));
		
		if(mItemInfo.mIsPress) {
			mTouchEffectLayout.setBackgroundColor(0x66000000);
		} else {
			mTouchEffectLayout.setBackgroundColor(0x00000000);
		}
	}
	
	
	@Override
	public void onClick(View v) {		
		if(mItemClickListener != null) {
			mItemClickListener.onItemClick(v);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(!mItemInfo.mIsDownloading && !mItemInfo.mIsBlackDimmed) {
			if(action ==  MotionEvent.ACTION_DOWN) {
				mTouchEffectLayout.setBackgroundColor(0x66000000);
			} else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
				mTouchEffectLayout.setBackgroundColor(0x00000000);
			}
		}
		
		return super.onTouchEvent(event);
	}

	@Override
	public void onDownloadComplete(ImageView imageView, Bitmap bitmap) {
		try {
			if(bitmap == null) {
				mThumbnail.setImageResource(R.drawable.book_thumbnail_default);
			} else {
				mThumbnail.setImageBitmap(bitmap);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
