package com.yoons.fsb.student.ui.popup;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.util.CommonUtil;

/**
 * 메시지 박스
 * 
 * @author dckim
 */
public class AnimationBox extends BaseDialog implements android.view.View.OnClickListener, OnTouchListener {

	private TextView mConfirm;

	private AnimatedImageView aniView =null;

	private Object mObject;

	/**
	 * 메시지 박스 화면 생성
	 * 
	 * @param context
	 *            Context
	 * @param string
	 *            타이틀 문구(리소스 아이디)
	 * @param message
	 *            메시지 문구
	 */
	public AnimationBox(Context context, int resid) {
		super(context, R.style.NoAnimationDialog);
		init(context);
		setAniView(resid);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.height = lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);

		setCanceledOnTouchOutside(false);

		useFocus(true);
	}

	private void useFocus(boolean bUse) {

		boolean bUseFocus = bUse;

		// 중단된 학습팝업에서 크래들 OK버튼 동작시 버튼하나인 경우에도 포커싱 필요하여 주석처리함
		// if (bUseFocus && View.VISIBLE != mCancel.getVisibility()) {
		// // 버튼이 하나만 보일 경우 포커싱 처리하지 않도록 예외를 둠.
		// bUseFocus = false;
		// }

		if (bUseFocus) {
			mConfirm.setFocusable(true);
			mConfirm.setFocusableInTouchMode(true);

			return;
		}

		mConfirm.setFocusable(false);
	}

	/**
	 * 화면 기본구성 설정
	 * 
	 * @param ctx
	 *            Context
	 */
	private void init(Context ctx) {

		mContext = ctx;
		setContentView(R.layout.ani_box);

		aniView=(AnimatedImageView) findViewById(R.id.msgbox_content);

		mConfirm = (TextView) findViewById(R.id.btn_confirm);

		mConfirm.setOnTouchListener(this);
		mConfirm.setOnClickListener(this);
	}


	/**
	 * 노출할 타이틀 문구를 설정함.
	 * 
	 * @param title
	 *            타이틀 문구(리소스 아이디)
	 */
	public void setTitle(int title) {
		if (0 >= title)
			return;

		setTitle(mContext.getString(title));
	}


	/**
	 * 버튼의 문구 설정(긍정)
	 * 
	 * @param text
	 *            설정할 버튼 문구
	 */
	public void setConfirmText(String text) {
		if (null != text && 0 < text.length()) {
			mConfirm.setText(text);
			mConfirm.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 버튼의 문구 설정(긍정)
	 * 
	 * @param text
	 *            설정할 버튼 문구(리소스 아이디)
	 */
	public void setConfirmText(int text) {
		if (0 >= text)
			return;

		setConfirmText(mContext.getString(text));
	}


	/**
	 * 버튼의 속성(show/hide) 설정(긍정)
	 * 
	 * @param visibility
	 */
	public void setConfirmVisibility(int visibility) {
		mConfirm.setVisibility(visibility);
	}



	@Override
	public void show() {
		// TODO Auto-generated method stub
		super.show();
		if (View.VISIBLE != mConfirm.getVisibility()) {
			mConfirm.setVisibility(View.GONE);

		} else {
			mConfirm.setVisibility(View.VISIBLE);
		}
	}

	private int getCorrectKeyCode(int keyCode) {
		if (CommonUtil.isQ7Device() && keyCode == 0) {
			return ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD;
		}

		return keyCode;
	}

	// for bluetooth 7/24
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);
		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE: {
			View focus = getCurrentFocus();
			if (null != focus && View.VISIBLE == focus.getVisibility() && !focus.isPressed())
				focus.setPressed(true);
		}
			break;

		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
		default:
			return super.onKeyDown(keyCode, event);
		}

		return false;
	}

	// for bluetooth 7/24
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);
		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE: {
			View focus = getCurrentFocus();
			if (null == focus)
				break;

			if (View.VISIBLE == focus.getVisibility() && !focus.isPressed())
				focus.setPressed(false);

			if (R.id.btn_confirm == focus.getId())
				onConfirm();
			else
				onCancel();
		}
			break;

		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
			nextFocus();
			break;

		default:
			return super.onKeyUp(keyCode, event);
		}

		return false;
	}

	/**
	 * move focus to next
	 */
	private void nextFocus() {
		View focus = getCurrentFocus();
		if (null != focus) {
			focus = focus.focusSearch(View.FOCUS_RIGHT);
			if (null != focus)
				focus.requestFocus();
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		if (MotionEvent.ACTION_UP != event.getAction())
			return false;

		if (v.isFocusable() && v.isFocusableInTouchMode()) {
			if (v.isPressed() && !v.isFocused()) {
				v.performClick();
				return true;
			}
		}

		return false;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_confirm:
			onConfirm();
			break;

		case R.id.btn_cancel:
		case R.id.msgbox_layout:
			onCancel();
			break;
		}

	}

	/**
	 * 메시지 박스의 "확인" 버튼 기능
	 */
	private void onConfirm() {
		if (mDismissListener != null)
			mDismissListener.onDialogDismiss(DIALOG_CONFIRM, getId());

		dismiss();
	}

	/**
	 * 메시지 박스의 "취소" 버튼 기능
	 */
	private void onCancel() {
		if (mDismissListener != null)
			mDismissListener.onDialogDismiss(DIALOG_CANCEL, getId());

		dismiss();
	}

	public void setAniView(int resid) {
		aniView.setImageResource(resid);
	}
	
}