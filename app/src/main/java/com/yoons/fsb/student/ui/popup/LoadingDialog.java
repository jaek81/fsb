package com.yoons.fsb.student.ui.popup;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

/**
 * 로딩화면을 구성함.
 * @author dckim
 */
public class LoadingDialog extends BaseDialog {

	private TextView mTitle;
	private TextView mMessage;
	public boolean mIsStop = false;
	private boolean mIsStudyResult = false;

	public LoadingDialog(Context context) {
		super(context, R.style.NoAnimationTranslucentDialog);
		mIsStudyResult = true;
		init(context);
	}

	/**
	 * 로딩화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구
	 * @param message	메시지 문구
	 */
	public LoadingDialog(Context context, String title, String message) {
		super(context, R.style.NoAnimationTranslucentDialog);
		mIsStudyResult = false;
		init(context);
		setTitle(title);
		setMessage(message);
	}

	/**
	 * 로딩화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구(리소스 아이디)
	 * @param message	메시지 문구(리소스 아이디)
	 */
	public LoadingDialog(Context context, int title, int message) {
		super(context, R.style.NoAnimationTranslucentDialog);
		mIsStudyResult = false;
		init(context);
		setTitle(title);
		setMessage(message);
	}

	/**
	 * 로딩화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구(리소스 아이디)
	 * @param message	메시지 문구
	 */
	public LoadingDialog(Context context, int title, String message) {
		super(context, R.style.NoAnimationTranslucentDialog);
		mIsStudyResult = false;
		init(context);
		setTitle(title);
		setMessage(message);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		/*super.onCreate(savedInstanceState);
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.height = lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);

		setCanceledOnTouchOutside(false);*/
	}
	
	
	@Override
	protected void onStop() {
		mIsStop = true;
		super.onStop();
	}
	
	/**
	 * 화면 기본구성 설정
	 * @param ctx Context
	 */
	private void init(Context ctx) {

		mContext = ctx;

		if (CommonUtil.isCenter()) // igse
			setContentView(R.layout.igse_new_loading);
		else {
			if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
				setContentView(R.layout.w_new_loading);
			else // 숲
				setContentView(R.layout.f_new_loading);
		}

		findViewById(R.id.loading_befly_layout).setVisibility(View.GONE);
		findViewById(R.id.loading_layout).setVisibility(View.GONE);

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.height = lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);

		CommonUtil.setSizeLayout(mContext, getWindow().getDecorView());

		setCanceledOnTouchOutside(false);

		if (mIsStudyResult) {
			findViewById(R.id.loading_befly_layout).setVisibility(View.VISIBLE);
			ImageView iv = (ImageView) findViewById(R.id.loading_befly);
			AnimationDrawable ad = (AnimationDrawable) iv.getDrawable();
			ad.start();
		} else {
			//getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			findViewById(R.id.loading_layout).setVisibility(View.VISIBLE);
			ImageView loading = (ImageView) findViewById(R.id.new_loading_img);
			final Animation ani = AnimationUtils.loadAnimation(mContext, R.anim.rotate_loading);
			ani.setDuration(1000);
			loading.startAnimation(ani);
		}

		/*mTitle = (TextView) findViewById(R.id.loading_title);
		mMessage = (TextView) findViewById(R.id.loading_message);*/
	}

	/**
	 * 노출할 타이틀 문구를 설정함.
	 * @param title 타이틀 문구
	 */
	public void setTitle(String title) {
		//if (null != title && 0 < title.length())
			//mTitle.setText(title);
	}
	
	/**
	 * 노출할 타이틀 문구를 설정함.
	 * @param title 타이틀 문구(리소스 아이디)
	 */
	public void setTitle(int title) {
		if (0 >= title)
			return;

		//setTitle(mContext.getString(title));
	}

	/**
	 * 노출할 메시지 문구를 설정함.
	 * @param title 메시지 문구
	 */
	public void setMessage(String message) {
		//mMessage.setText(message);
	}
	
	/**
	 * 노출할 메시지 문구를 설정함.
	 * @param title 메시지 문구(리소스 아이디)
	 */
	public void setMessage(int message) {
		if (0 >= message)
			return;

		//setMessage(mContext.getString(message));
	}

	/**
	 * 로딩화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구
	 * @param message	메시지 문구
	 * @return LoadingDialog
	 */
	public static LoadingDialog show(Context context, String title, String message) {
		LoadingDialog loading = new LoadingDialog(context, title, message);
		loading.setCancelable(false);
		if (context != null) {
			loading.show();
		}
		return loading;
	}
	
	/**
	 * 로딩화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구(리소스 아이디)
	 * @param message	메시지 문구(리소스 아이디)
	 * @return LoadingDialog
	 */
	public static LoadingDialog show(Context context, int title, int message) {
		LoadingDialog loading = new LoadingDialog(context, title, message);
		loading.setCancelable(false);
		if (context != null) {
			loading.show();
		}
		return loading;
	}

	public static LoadingDialog show(Context context) {
		LoadingDialog loading = new LoadingDialog(context);
		loading.setCancelable(false);
		if (context != null) {
			loading.show();
		}
		return loading;
	}
}
