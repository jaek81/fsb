package com.yoons.fsb.student.ui.popup;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

/**
 * 메시지 박스
 *
 * @author dckim
 */
public class MessageBox extends BaseDialog implements android.view.View.OnClickListener, OnTouchListener {

	private TextView mTitle;
	private TextView mMessage;
	private TextView mConfirm;
	private TextView mCancel;

	private LinearLayout mButtonsLayout = null;
	private LinearLayout mAllLayout = null, mFocusLayout = null;
	private TextView mGap10 = null, mGap20 = null;
	private View mPrevFocus = null;
	private CheckBox mCheck = null;

	private Object mObject;

	/**
	 * 메시지 박스 화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구
	 * @param message	메시지 문구
	 */
	public MessageBox(Context context, String title, String message) {
		super(context, R.style.NoAnimationDialog);
		init(context);
		setTitle(title);
		setMessage(message);
	}

	/**
	 * 메시지 박스 화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구(리소스 아이디)
	 * @param message	메시지 문구(리소스 아이디)
	 */
	public MessageBox(Context context, int title, int message) {
		super(context, R.style.NoAnimationDialog);
		init(context);
		setTitle(title);
		setMessage(message);
	}

	/**
	 * 메시지 박스 화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구(리소스 아이디)
	 * @param message	메시지 문구(리소스 아이디)
	 * @param cbMessage	체크박스 문구(리소스 아이디)
	 */
	public MessageBox(Context context, int title, int message, int cbMessage) {
		super(context, R.style.NoAnimationDialog);
		init(context);
		initCheckBox(cbMessage);
		setTitle(title);
		setMessage(message);
	}

	/**
	 * 메시지 박스 화면 생성
	 * @param context	Context
	 * @param title		타이틀 문구(리소스 아이디)
	 * @param message	메시지 문구
	 */
	public MessageBox(Context context, int title, String message) {
		super(context, R.style.NoAnimationDialog);
		init(context);
		setTitle(title);
		setMessage(message);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.height = lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);

		setCanceledOnTouchOutside(false);

		useFocus(true);//CommonUtil.isBluetoothEnabled(getContext()));

		CommonUtil.setGlobalFont(mContext, getWindow().getDecorView());
		CommonUtil.setSizeLayout(mContext, getWindow().getDecorView());
	}

	private void useFocus(boolean bUse) {

		boolean bUseFocus = bUse;

		// 중단된 학습팝업에서 크래들 OK버튼 동작시 버튼하나인 경우에도 포커싱 필요하여 주석처리함
//		if (bUseFocus && View.VISIBLE != mCancel.getVisibility()) {
//			// 버튼이 하나만 보일 경우 포커싱 처리하지 않도록 예외를 둠.
//			bUseFocus = false;
//		}

		if (bUseFocus) {
			mConfirm.setFocusable(true);
			mConfirm.setFocusableInTouchMode(true);
			mCancel.setFocusable(true);
			mCancel.setFocusableInTouchMode(true);

			if (null == mPrevFocus || mPrevFocus == mFocusLayout)
				mPrevFocus = mConfirm;

			mPrevFocus.requestFocus();
			return;
		}

		mPrevFocus = getCurrentFocus();
		mConfirm.setFocusable(false);
		mCancel.setFocusable(false);
		mFocusLayout.requestFocus();
	}

	/**
	 * 화면 기본구성 설정
	 * @param ctx Context
	 */
	private void init(Context ctx) {

		mContext = ctx;

		if (CommonUtil.isCenter()) // Igse
			setContentView(R.layout.igse_msg_box);
		else {
			if ("1".equals(Preferences.getLmsStatus(mContext))) {
				setContentView(R.layout.w_msg_box);
			} else {
				setContentView(R.layout.f_msg_box);
			}
		}

		mTitle = (TextView) findViewById(R.id.msgbox_title);
		mMessage = (TextView) findViewById(R.id.msgbox_content);

		mButtonsLayout = (LinearLayout)findViewById(R.id.buttons_area);
		mAllLayout = (LinearLayout)findViewById(R.id.msgbox_layout);
		mFocusLayout = (LinearLayout)findViewById(R.id.btn_layout);
		mConfirm = (TextView)findViewById(R.id.btn_confirm);
		mCancel = (TextView)findViewById(R.id.btn_cancel);
		mGap10 = (TextView)findViewById(R.id.gap_10);

		mConfirm.setOnTouchListener(this);
		mConfirm.setOnClickListener(this);
		mCancel.setOnTouchListener(this);
		mCancel.setOnClickListener(this);
	}

	private void initCheckBox(int message) {

		//mGap20 = (TextView)findViewById(R.id.gap_20);
		mCheck = (CheckBox)findViewById(R.id.msgbox_check);

		//mGap20.setVisibility(View.GONE);
		mCheck.setVisibility(View.VISIBLE);
		mCheck.setText(mContext.getString(message));
		mCheck.setOnClickListener(this);
	}

	/**
	 * 노출할 타이틀 문구를 설정함.
	 * @param title 타이틀 문구
	 */
	public void setTitle(String title) {
		if (null != title && 0 < title.length()) {
			mTitle.setText(title);
			mTitle.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 노출할 타이틀 문구를 설정함.
	 * @param title 타이틀 문구(리소스 아이디)
	 */
	public void setTitle(int title) {
		if (0 >= title)
			return;

		setTitle(mContext.getString(title));
	}

	public void setMessage(String message) {
		mMessage.setText(message);
	}

	public void setMessage(int message) {
		if (0 >= message)
			return;

		setMessage(mContext.getString(message));
	}

	/**
	 * 버튼의 문구 설정(긍정)
	 * @param text 설정할 버튼 문구
	 */
	public void setConfirmText(String text) {
		if (null != text && 0 < text.length()) {
			mConfirm.setText(text);
			mConfirm.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 버튼의 문구 설정(긍정)
	 * @param text 설정할 버튼 문구(리소스 아이디)
	 */
	public void setConfirmText(int text) {
		if (0 >= text)
			return;

		setConfirmText(mContext.getString(text));
	}

	/**
	 * 버튼의 문구 설정(부정)
	 * @param text 설정할 버튼 문구
	 */
	public void setCancelText(String text) {
		if (null != text && 0 < text.length()) {
			mCancel.setVisibility(View.VISIBLE);
			mCancel.setText(text);
		}
	}

	/**
	 * 버튼의 문구 설정(부정)
	 * @param text 설정할 버튼 문구(리소스 아이디)
	 */
	public void setCancelText(int text) {
		if (0 >= text)
			return;

		setCancelText(mContext.getString(text));
	}

	/**
	 * 버튼의 속성(show/hide) 설정(긍정)
	 * @param visibility
	 */
	public void setConfirmVisibility(int visibility) {
		mConfirm.setVisibility(visibility);
	}

	/**
	 * 버튼의 속성(show/hide) 설정(부정)
	 * @param visibility
	 */
	public void setCancelVisibility(int visibility) {
		mCancel.setVisibility(visibility);
	}

	/**
	 * 타이틀 문구의 align을 설정함.<br>default is center
	 * @param center 중앙정렬 할 것인지 여부(false이면 좌측으로 설정됨)
	 */
	public void setTitleGravityCenter(boolean center) {
		mTitle.setGravity((center) ? (Gravity.CENTER_HORIZONTAL | Gravity.TOP) : (Gravity.LEFT | Gravity.TOP));
	}

	/**
	 * 메시지 문구의 align을 설정함.<br>default is center
	 * @param center 중앙정렬 할 것인지 여부(false이면 좌측으로 설정됨)
	 */
	public void setMessageGravityCenter(boolean center) {
		mMessage.setGravity((center) ? Gravity.CENTER : Gravity.LEFT);
	}

	public void setTag(Object obj) {
		mObject = obj;
	}

	public Object getTag() {
		return mObject;
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		if (mContext == null) {
			return;
		}

		super.show();
		if (View.VISIBLE != mConfirm.getVisibility() && View.VISIBLE != mCancel.getVisibility()) {
			mButtonsLayout.setVisibility(View.GONE);
			mGap10.setVisibility(View.VISIBLE);

			// 버튼이 없는 경우 아무 곳이나 터치하면 창 종료
			mAllLayout.setOnClickListener(this);
		} else {
			mButtonsLayout.setVisibility(View.VISIBLE);
			mGap10.setVisibility(View.GONE);
		}
	}

	private int getCorrectKeyCode(int keyCode) {
		if (CommonUtil.isQ7Device() && keyCode == 0) {
			return ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD;
		}

		return keyCode;
	}

	// for bluetooth 7/24
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);
		switch (keyCode) {
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE: {
				View focus = getCurrentFocus();
				if (null != focus && View.VISIBLE == focus.getVisibility() && !focus.isPressed())
					focus.setPressed(true);
			}
			break;

			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
			default:
				return super.onKeyDown(keyCode, event);
		}

		return false;
	}

	// for bluetooth 7/24
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);
		switch (keyCode) {
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE: {
				View focus = getCurrentFocus();
				if (null == focus)
					break;

				if (View.VISIBLE == focus.getVisibility() && !focus.isPressed())
					focus.setPressed(false);

				if (R.id.btn_confirm == focus.getId())
					onConfirm();
				else
					onCancel();
			}
			break;

			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
				nextFocus(View.FOCUS_LEFT);
				break;
			case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
				nextFocus(View.FOCUS_RIGHT);
				break;

			default:
				return super.onKeyUp(keyCode, event);
		}

		return false;
	}

	/**
	 * move focus to next
	 */
	private void nextFocus(int dest) {
		View focus = getCurrentFocus();
		if (null != focus) {
			focus = focus.focusSearch(dest);
			if (null != focus)
				focus.requestFocus();
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		if (MotionEvent.ACTION_UP != event.getAction())
			return false;

		if (v.isFocusable() && v.isFocusableInTouchMode()) {
			if (v.isPressed() && !v.isFocused()) {
				v.performClick();
				return true;
			}
		}

		return false;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.btn_confirm:
				onConfirm();
				break;

			case R.id.btn_cancel:
			case R.id.msgbox_layout:
				onCancel();
				break;
			case R.id.msgbox_check:
//				if (mCheck.isChecked()) {
//					Preferences.setPopupCheckedDate(mContext, CommonUtil.getCurrentDate());
//				} else {
//					Preferences.setPopupCheckedDate(mContext, "");
//				}
				break;
		}
	}

	/**
	 * 메시지 박스의 "확인" 버튼 기능
	 */
	private void onConfirm() {
		if (mDismissListener != null)
			mDismissListener.onDialogDismiss(DIALOG_CONFIRM, getId());

		dismiss();
	}

	/**
	 * 메시지 박스의 "취소" 버튼 기능
	 */
	private void onCancel() {
		if (mDismissListener != null)
			mDismissListener.onDialogDismiss(DIALOG_CANCEL, getId());

		dismiss();
	}
}