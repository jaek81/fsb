package com.yoons.fsb.student.ui.popup;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.util.CommonUtil;

/**
 * 메시지 박스
 * 
 * @author dckim
 */
public class ScrollBox extends BaseDialog implements android.view.View.OnClickListener, OnTouchListener {

	private TextView mTitle;
	private TextView mMessage;
	private TextView mConfirm;
	private TextView mCancel;

	private LinearLayout mAllLayout = null, mFocusLayout = null;
	private View mPrevFocus = null;

	private Object mObject;
	private ScrollView scrollView;

	/**
	 * 메시지 박스 화면 생성
	 * 
	 * @param context
	 *            Context
	 * @param title
	 *            타이틀 문구
	 * @param message
	 *            메시지 문구
	 */
	public ScrollBox(Context context, String title, String message) {
		super(context, R.style.NoAnimationDialog);
		init(context);
		setTitle(title);
		setMessage(message);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.height = lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);

		setCanceledOnTouchOutside(false);

	}

	/**
	 * 화면 기본구성 설정
	 * 
	 * @param ctx
	 *            Context
	 */
	private void init(Context ctx) {

		mContext = ctx;
		setContentView(R.layout.scroll_box);

		mTitle = (TextView) findViewById(R.id.msgbox_title);
		mMessage = (TextView) findViewById(R.id.msgbox_content);

		mAllLayout = (LinearLayout) findViewById(R.id.msgbox_layout);
		mFocusLayout = (LinearLayout) findViewById(R.id.btn_layout);
		mConfirm = (TextView) findViewById(R.id.btn_confirm);
		mCancel = (TextView) findViewById(R.id.btn_cancel);
		scrollView = (ScrollView) findViewById(R.id.scroll_view);

		mMessage.setMovementMethod(new ScrollingMovementMethod());
		//mConfirm.setOnTouchListener(this);
		mConfirm.setOnClickListener(this);
		//mCancel.setOnTouchListener(this);
		mCancel.setOnClickListener(this);
	}

	/**
	 * 노출할 타이틀 문구를 설정함.
	 * 
	 * @param title
	 *            타이틀 문구
	 */
	public void setTitle(String title) {
		if (null != title && 0 < title.length()) {
			mTitle.setText(title);
			mTitle.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 노출할 타이틀 문구를 설정함.
	 * 
	 * @param title
	 *            타이틀 문구(리소스 아이디)
	 */
	public void setTitle(int title) {
		if (0 >= title)
			return;

		setTitle(mContext.getString(title));
	}

	/**
	 * 노출할 메시지 문구를 설정함.
	 * 
	 * @param title
	 *            메시지 문구
	 */
	public void setMessage(String message) {
		mMessage.setText(message);
	}

	/**
	 * 노출할 메시지 문구를 설정함.
	 * 
	 * @param title
	 *            메시지 문구(리소스 아이디)
	 */
	public void setMessage(int message) {
		if (0 >= message)
			return;

		setMessage(mContext.getString(message));
	}

	/**
	 * 버튼의 문구 설정(긍정)
	 * 
	 * @param text
	 *            설정할 버튼 문구
	 */
	public void setConfirmText(String text) {
		if (null != text && 0 < text.length()) {
			mConfirm.setText(text);
			mConfirm.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 버튼의 문구 설정(긍정)
	 * 
	 * @param text
	 *            설정할 버튼 문구(리소스 아이디)
	 */
	public void setConfirmText(int text) {
		if (0 >= text)
			return;

		setConfirmText(mContext.getString(text));
	}

	/**
	 * 버튼의 문구 설정(부정)
	 * 
	 * @param text
	 *            설정할 버튼 문구
	 */
	public void setCancelText(String text) {
		if (null != text && 0 < text.length()) {
			mCancel.setVisibility(View.VISIBLE);
			mCancel.setText(text);
		}
	}

	/**
	 * 버튼의 문구 설정(부정)
	 * 
	 * @param text
	 *            설정할 버튼 문구(리소스 아이디)
	 */
	public void setCancelText(int text) {
		if (0 >= text)
			return;

		setCancelText(mContext.getString(text));
	}

	/**
	 * 버튼의 속성(show/hide) 설정(긍정)
	 * 
	 * @param visibility
	 */
	public void setConfirmVisibility(int visibility) {
		mConfirm.setVisibility(visibility);
	}

	/**
	 * 버튼의 속성(show/hide) 설정(부정)
	 * 
	 * @param visibility
	 */
	public void setCancelVisibility(int visibility) {
		mCancel.setVisibility(visibility);
	}

	/**
	 * 타이틀 문구의 align을 설정함.<br>
	 * default is center
	 * 
	 * @param center
	 *            중앙정렬 할 것인지 여부(false이면 좌측으로 설정됨)
	 */
	public void setTitleGravityCenter(boolean center) {
		mTitle.setGravity((center) ? (Gravity.CENTER_HORIZONTAL | Gravity.TOP) : (Gravity.LEFT | Gravity.TOP));
	}

	/**
	 * 메시지 문구의 align을 설정함.<br>
	 * default is center
	 * 
	 * @param center
	 *            중앙정렬 할 것인지 여부(false이면 좌측으로 설정됨)
	 */
	public void setMessageGravityCenter(boolean center) {
		mMessage.setGravity((center) ? Gravity.CENTER : Gravity.LEFT);
	}

	public void setTag(Object obj) {
		mObject = obj;
	}

	public Object getTag() {
		return mObject;
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		try {
			super.show();
			if (View.VISIBLE != mConfirm.getVisibility() && View.VISIBLE != mCancel.getVisibility()) {

				// 버튼이 없는 경우 아무 곳이나 터치하면 창 종료
				mAllLayout.setOnClickListener(this);
			} else {
				mConfirm.setActivated(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int getCorrectKeyCode(int keyCode) {
		if (CommonUtil.isQ7Device() && keyCode == 0) {
			return ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD;
		}

		return keyCode;
	}

	// for bluetooth 7/24
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);
		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE: {

			if (mConfirm.isActivated()) {
				mConfirm.setPressed(true);
				mCancel.setPressed(false);
			} else {
				mConfirm.setPressed(false);
				mCancel.setPressed(true);
			}

		}
			break;

		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
			break;
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
			scrollView.smoothScrollBy(0, -40);
			break;
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
			scrollView.scrollBy(0, 40);
			break;

		default:
			break;
		}

		return false;
	}

	// for bluetooth 7/24
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		keyCode = getCorrectKeyCode(keyCode);
		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE: {
			mConfirm.setPressed(false);
			mCancel.setPressed(false);

			if (mConfirm.isActivated()) {
				onConfirm();
			} else {
				onCancel();
			}
		}
			break;

		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
			if (mConfirm.isActivated() && mCancel.isActivated()) {
				nextFocus();
			}

			break;
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
			break;

		default:
			return super.onKeyUp(keyCode, event);
		}

		return false;
	}

	/**
	 * move focus to next
	 */
	private void nextFocus() {
		if (mConfirm.isActivated() && mConfirm.isActivated()) {
			if (mConfirm.isActivated()) {
				mConfirm.setActivated(false);
				mCancel.setActivated(true);
			} else {
				mConfirm.setActivated(true);
				mCancel.setActivated(false);
			}
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		if (MotionEvent.ACTION_UP != event.getAction())
			return false;

		if (v.isFocusable() && v.isFocusableInTouchMode()) {
			if (v.isPressed() && !v.isFocused()) {
				v.performClick();
				return true;
			}
		}

		return false;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_confirm:
			onConfirm();
			break;

		case R.id.btn_cancel:
		case R.id.msgbox_layout:
			onCancel();
			break;
		}

	}

	/**
	 * 메시지 박스의 "확인" 버튼 기능
	 */
	private void onConfirm() {
		if (mDismissListener != null)
			mDismissListener.onDialogDismiss(DIALOG_CONFIRM, getId());

		dismiss();
	}

	/**
	 * 메시지 박스의 "취소" 버튼 기능
	 */
	private void onCancel() {
		if (mDismissListener != null)
			mDismissListener.onDialogDismiss(DIALOG_CANCEL, getId());

		dismiss();
	}
}