package com.yoons.fsb.student.ui.popup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.ui.SmartStudyActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import kr.pe.burt.android.lib.faimageview.FAImageView;

public class ShowPopup {
    private static final String _TAG = ShowPopup.class.getSimpleName();
    private static ShowPopup mInstance = null;
    private static boolean isPopupFlag = false;

    private SmartStudyActivity.SmartFuncImpl mStudyCallBack;

    public static synchronized ShowPopup getInstance() {
        if (null == mInstance) {
            mInstance = new ShowPopup();
        }
        return mInstance;
    }

    private Thread mLeaningRecordThread = null;
    private ImageView ivLearningRecord;
    private Context mContext;

    private FAImageView faIvLearningComplete;
    private boolean mIsClick = false;

    public boolean isPopupLayer() {
        return isPopupFlag;
    }

    public void setPopupStatus(boolean popupFlag) {
        isPopupFlag = popupFlag;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void ShowMainTutorial(final Context context) {
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels);

        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        final boolean[] m_bIsChange = {false};

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // 주간 학습 현황 ( 메인화면 듀토리얼)
        dialog.setContentView(R.layout.popup_tutorial_main);
        Button tvTitle = dialog.findViewById(R.id.btn_go_home);
        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Preferences.setShowGuide(context, false);
            }
        });

        final ImageView iv01 = dialog.findViewById(R.id.iv_indicator01);
        final ImageView iv02 = dialog.findViewById(R.id.iv_indicator02);
        final ImageView m_ivBg = dialog.findViewById(R.id.iv_tutorial_home);

        m_ivBg.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Log.e(_TAG, "touch2");
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE: {
                    }
                    break;
                    case MotionEvent.ACTION_UP: {
                        if (!m_bIsChange[0]) {
                            if (CommonUtil.isCenter()) { // igse
                                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.igse_img_tutorial_02));
                                iv02.setBackgroundResource(R.drawable.igse_img_guide_navi_on);
                            } else {
                                if ("1".equals(Preferences.getLmsStatus(context))) { // 우영
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.w_img_tutorial_02));
                                    iv02.setBackgroundResource(R.drawable.w_img_guide_navi_on);
                                } else { // 숲
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.f_img_tutorial_02));
                                    iv02.setBackgroundResource(R.drawable.f_img_guide_navi_on);
                                }
                            }
                            iv01.setBackgroundResource(R.drawable.c_img_tutorial_navi_off);

                            m_bIsChange[0] = true;
                        } else {
                            if (CommonUtil.isCenter()) { // igse
                                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.igse_img_tutorial_01));
                                iv01.setBackgroundResource(R.drawable.igse_img_guide_navi_on);
                            } else {
                                if ("1".equals(Preferences.getLmsStatus(context))) { // 우영
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.w_img_tutorial_01));
                                    iv01.setBackgroundResource(R.drawable.w_img_guide_navi_on);
                                } else { // 숲
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.f_img_tutorial_01));
                                    iv01.setBackgroundResource(R.drawable.f_img_guide_navi_on);
                                }
                            }
                            iv02.setBackgroundResource(R.drawable.c_img_tutorial_navi_off);

                            m_bIsChange[0] = false;
                        }
                    }
                    break;
                }
                return true;
            }
        });

        try {
            dialog.show();
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x70000000));
            CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public Dialog ShowLearningTutorialForPageStudy(final Context context) {
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels);

        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        final boolean[] m_bIsChange = {false};
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // 주간 학습 현황 ( 메인화면 듀토리얼)
        dialog.setContentView(R.layout.popup_tutorial_main);
        Button tvTitle = dialog.findViewById(R.id.btn_go_home);
        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                Preferences.setShowLearningGuideForPageStudy(context, false);
//            }
//        });

        final ImageView iv01 = dialog.findViewById(R.id.iv_indicator01);
        final ImageView iv02 = dialog.findViewById(R.id.iv_indicator02);
        final ImageView m_ivBg = dialog.findViewById(R.id.iv_tutorial_home);

        if (CommonUtil.isCenter()) // Igse
            m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.igse_img_tutorial_thumb_01));
        else {
            if ("1".equals(Preferences.getLmsStatus(context))) //우영
                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.w_img_tutorial_thumb_01));
            else // 숲
                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.f_img_tutorial_thumb_01));
        }

        m_ivBg.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE: {
                    }
                    break;
                    case MotionEvent.ACTION_UP: {
                        if (!m_bIsChange[0]) {
                            if (CommonUtil.isCenter()) { // igse
                                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.igse_img_tutorial_thumb_02));
                                iv02.setBackgroundResource(R.drawable.igse_img_guide_navi_on);
                            } else {
                                if ("1".equals(Preferences.getLmsStatus(context))) { // 우영
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.w_img_tutorial_thumb_02));
                                    iv02.setBackgroundResource(R.drawable.w_img_guide_navi_on);
                                } else { // 숲
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.f_img_tutorial_thumb_02));
                                    iv02.setBackgroundResource(R.drawable.f_img_guide_navi_on);
                                }
                            }
                            iv01.setBackgroundResource(R.drawable.c_img_tutorial_navi_off);

                            m_bIsChange[0] = true;
                        } else {
                            if (CommonUtil.isCenter()) { // igse
                                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.igse_img_tutorial_thumb_01));
                                iv01.setBackgroundResource(R.drawable.igse_img_guide_navi_on);
                            } else {
                                if ("1".equals(Preferences.getLmsStatus(context))) { // 우영
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.w_img_tutorial_thumb_01));
                                    iv01.setBackgroundResource(R.drawable.w_img_guide_navi_on);
                                } else { // 숲
                                    m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.f_img_tutorial_thumb_01));
                                    iv01.setBackgroundResource(R.drawable.f_img_guide_navi_on);
                                }
                            }
                            iv02.setBackgroundResource(R.drawable.c_img_tutorial_navi_off);

                            m_bIsChange[0] = false;
                        }
                    }
                    break;
                }
                return true;
            }
        });

        try {
            dialog.show();
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x70000000));
            CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return dialog;
    }

    public Dialog ShowLearningTutorial(final Context context) {

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels);

        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        final boolean[] m_bIsChange = {false};
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // 주간 학습 현황 ( 메인화면 듀토리얼)
        dialog.setContentView(R.layout.popup_tutorial_main);
        Button tvTitle = dialog.findViewById(R.id.btn_go_home);
        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                Preferences.setShowLearningGuide(context, false);
//            }
//        });

        final ImageView iv01 = dialog.findViewById(R.id.iv_indicator01);
        final ImageView iv02 = dialog.findViewById(R.id.iv_indicator02);
        iv01.setVisibility(View.INVISIBLE);
        iv02.setVisibility(View.INVISIBLE);
        final ImageView m_ivBg = dialog.findViewById(R.id.iv_tutorial_home);

        if (CommonUtil.isCenter()) // Igse
            m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.igse_img_tutorial_03));
        else {
            if ("1".equals(Preferences.getLmsStatus(context))) //우영
                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.w_img_tutorial_03));
            else // 숲
                m_ivBg.setImageDrawable(context.getResources().getDrawable(R.drawable.f_img_tutorial_03));
        }

        try {
            dialog.show();
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x70000000));
            CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return dialog;
    }

    public void ShowLeaningOk(final Context context, String strText) {
        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        // (스마트학습) 교재활동 상세 팝업
        dialog.setContentView(R.layout.popup_learning);

        CommonUtil.setGlobalFont(context, dialog.getWindow().getDecorView());
        CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());

        faIvLearningComplete = dialog.findViewById(R.id.iv_popup_animation);
        faIvLearningComplete.setInterval(90);
        faIvLearningComplete.setLoop(true);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_01);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_02);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_03);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_04);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_05);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_06);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_07);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_08);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_09);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_10);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_11);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_write_12);
        faIvLearningComplete.startAnimation();

        TextView tvText = dialog.findViewById(R.id.tv_popup_text);
        tvText.setText(strText);
        mIsClick = false;

        final TextView tvCancel = dialog.findViewById(R.id.tv_text_ok);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsClick = true;
                faIvLearningComplete.stopAnimation();
                //((LearningplayActivity) context).writeEnd();
                setPopupStatus(false);
                dialog.dismiss();
            }
        });

        //Dialog Dismiss시 OK버튼 클릭  효과
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!mIsClick) {
                    faIvLearningComplete.stopAnimation();
                    //((LearningplayActivity) context).writeEnd();
                    setPopupStatus(false);
                }
                if (mStudyCallBack != null) {
                    mStudyCallBack.onWriteEnd();
                }
            }
        });

        try {
            int[] wh = getDeviceWidthHeight(context);
            int nWidth = wh[0];
            int nHeight = wh[1];

            dialog.show();
            setPopupStatus(true);
            dialog.setCancelable(false);
            //dialog.getWindow().setLayout(nWidth, nHeight);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setCallBackListener(SmartStudyActivity.SmartFuncImpl callback) {
        mStudyCallBack = callback;
    }

    public Dialog ShowLeaningRecord(final Context context, String strText, String curTagEventType) {
        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        final String tagType = curTagEventType;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        // (스마트학습) 교재활동 상세 팝업
        dialog.setContentView(R.layout.popup_learning);

        CommonUtil.setGlobalFont(context, dialog.getWindow().getDecorView());
        CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());

        dialog.findViewById(R.id.iv_popup_animation).setVisibility(View.GONE);
        mContext = context;
        ivLearningRecord = dialog.findViewById(R.id.iv_popup_animation_record);
        ivLearningRecord.setVisibility(View.VISIBLE);

        /*faIvLearningComplete = dialog.findViewById(R.id.iv_popup_animation);
        faIvLearningComplete.setInterval(500);
        faIvLearningComplete.setLoop(true);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_record_01);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_record_02);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_record_03);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_record_04);
        faIvLearningComplete.startAnimation();*/

        TextView tvText = dialog.findViewById(R.id.tv_popup_text);
        tvText.setText(strText);
        mIsClick = false;
        dialog.setCancelable(false);

        //1초뒤 normal및 클릭리스너 달기
        final TextView ok = dialog.findViewById(R.id.tv_text_ok);

        ok.setEnabled(false);
//        tvCancel.setBackgroundResource(R.drawable.btn_learning_acivity_d);

        //G1,LG1태그는 활성화를 G2,LG2때 한다(Learningplayactivity에서 생성)
        if (tagType.equalsIgnoreCase("G1") || tagType.equalsIgnoreCase("LG1")) {
            dialog.setCancelable(false);
        } else {
            //S 태그 셀렉터 적용 구간
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    tvCancel.setBackgroundResource(R.drawable.btn_learning_acivity_n);
                    ok.setClickable(true);
                    ok.setEnabled(true);
                }
            }, 1500);
        }

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!mIsClick) {
                    stopLeaningRecordThread();
                    //faIvLearningComplete.stopAnimation();
                    setPopupStatus(false);
                }
                if (mStudyCallBack != null) {
                    mStudyCallBack.onRecordEnd();
                }
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsClick = true;

                stopLeaningRecordThread();
                //faIvLearningComplete.stopAnimation();
                //((LearningplayActivity) context).LeaningRecordClick();

                setPopupStatus(false);
                dialog.dismiss();
            }
        });

        ok.setClickable(false);

        try {
            int[] wh = getDeviceWidthHeight(context);
            int nWidth = wh[0];
            int nHeight = wh[1];

            dialog.show();
            runLeaningRecordThread();
            setPopupStatus(true);
            // dialog.getWindow().setLayout(nWidth, nHeight);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dialog;
    }

    private void runLeaningRecordThread() {
        mLeaningRecordThread = new Thread("LeaningRecordThread") {
            public void run() {
                while (null != mLeaningRecordThread && !mLeaningRecordThread.isInterrupted()) {
                    SystemClock.sleep(200);
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ING, 0));
                }
            }
        };

        mLeaningRecordThread.start();
    }


    private void stopLeaningRecordThread() {
        if (null != mLeaningRecordThread) {
            //mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
            mLeaningRecordThread.interrupt();
            mLeaningRecordThread = null;
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (msg.arg1 == ServiceCommon.MSG_REC_ING) {
                        if (mLeaningRecordThread == null)
                            return;
                        int lastLevel = ServiceCommon.RECORD_VOLUME;
                        Log.i("", "MSG_WHAT_RECORDER lastLevel = " + lastLevel);
                        if (lastLevel > 0 && lastLevel <= 20) {
                            ivLearningRecord.setImageDrawable(mContext.getResources().getDrawable(R.drawable.c_seq_learning_activity_record_01));
                        } else if (lastLevel > 20 && lastLevel <= 50) {
                            ivLearningRecord.setImageDrawable(mContext.getResources().getDrawable(R.drawable.c_seq_learning_activity_record_02));
                        } else if (lastLevel > 50 && lastLevel <= 80) {
                            ivLearningRecord.setImageDrawable(mContext.getResources().getDrawable(R.drawable.c_seq_learning_activity_record_03));
                        } else if (lastLevel > 80) {
                            ivLearningRecord.setImageDrawable(mContext.getResources().getDrawable(R.drawable.c_seq_learning_activity_record_04));
                        }
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    public Dialog ShowLeaningListen(Context context, String strText) {
        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        // (스마트학습) 교재활동 상세 팝업
        dialog.setContentView(R.layout.popup_learning);

        CommonUtil.setGlobalFont(context, dialog.getWindow().getDecorView());
        CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());

        faIvLearningComplete = dialog.findViewById(R.id.iv_popup_animation);
        faIvLearningComplete.setInterval(500);
        faIvLearningComplete.setLoop(true);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_listen_01);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_listen_02);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_listen_03);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_listen_04);
        faIvLearningComplete.startAnimation();
        TextView tvText = dialog.findViewById(R.id.tv_popup_text);
        tvText.setText(strText);

        tvText.setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL);

        TextView tvTextOK = dialog.findViewById(R.id.tv_popup_text_ok);
        TextView tvButtonOK = dialog.findViewById(R.id.tv_text_ok);

        tvTextOK.setVisibility(View.GONE);
        tvButtonOK.setEnabled(false);
//        tvButtonOK.setBackgroundResource(R.drawable.btn_learning_acivity_d);

        TextView tvTextPress = dialog.findViewById(R.id.tv_popup_text_press);
        tvTextPress.setVisibility(View.GONE);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                faIvLearningComplete.stopAnimation();
                setPopupStatus(false);
            }
        });

        try {
            int[] wh = getDeviceWidthHeight(context);
            int nWidth = wh[0];
            int nHeight = wh[1];

            dialog.setCancelable(false);
            dialog.show();
            setPopupStatus(true);
            // dialog.getWindow().setLayout(nWidth, nHeight);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dialog;
    }

    public void ShowLeaningTextBook(final Context context, String strText) {
        final ShowPopupDialog dialog = new ShowPopupDialog(context);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        // (스마트학습) 교재활동 상세 팝업
        dialog.setContentView(R.layout.popup_learning);

        CommonUtil.setGlobalFont(context, dialog.getWindow().getDecorView());
        CommonUtil.setSizeLayout(context, dialog.getWindow().getDecorView());

        faIvLearningComplete = dialog.findViewById(R.id.iv_popup_animation);
        faIvLearningComplete.setInterval(90);
        faIvLearningComplete.setLoop(true);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_01);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_02);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_03);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_04);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_05);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_06);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_07);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_08);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_09);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_10);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_11);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_12);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_13);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_14);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_15);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_16);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_17);
        faIvLearningComplete.addImageFrame(R.drawable.c_seq_learning_activity_textbook_18);
        faIvLearningComplete.startAnimation();
        TextView tvText = dialog.findViewById(R.id.tv_popup_text);
        tvText.setText(strText);
        mIsClick = false;

        final TextView tvCancel = dialog.findViewById(R.id.tv_text_ok);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsClick = true;
                faIvLearningComplete.stopAnimation();
                //((LearningplayActivity) context).playerResume();
                setPopupStatus(false);
                dialog.dismiss();

            }
        });
        // Dialog Dismiss시 OK버튼 클릭  효과
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!mIsClick) {
                    faIvLearningComplete.stopAnimation();
                    //((LearningplayActivity) context).playerResume();
                    setPopupStatus(false);
                }
                if (mStudyCallBack != null) {
                    mStudyCallBack.onPlayResume();
                }
            }
        });

        try {
            int[] wh = getDeviceWidthHeight(context);
            int nWidth = wh[0];
            int nHeight = wh[1];

            dialog.show();
            setPopupStatus(true);
            dialog.setCancelable(false);
            // dialog.getWindow().setLayout(nWidth, nHeight);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private int[] getDeviceWidthHeight(Context context) {
        int[] wh = new int[2];

        wh[0] = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.7);
        wh[1] = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.73);

//        if ((context.getResources().getConfiguration().screenLayout
//                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
//            Log.i("", "This is Tablet!!");
//            wh[0] = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.7);
//            wh[1] = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.7);
//        }

        return wh;
    }
}