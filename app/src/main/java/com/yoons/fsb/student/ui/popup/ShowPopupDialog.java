package com.yoons.fsb.student.ui.popup;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;

public class ShowPopupDialog extends Dialog {
    protected ShowPopupDialog(@NonNull Context context) {
        super(context);
    }

    protected ShowPopupDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public ShowPopupDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                View v = findViewById(R.id.tv_text_ok);
                if (v != null && v.isEnabled()) {
                    v.setPressed(true);
                }
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                View v = findViewById(R.id.tv_text_ok);
                if (v != null && v.isEnabled()) {
                    v.setPressed(false);
                    dismiss();
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }
}
