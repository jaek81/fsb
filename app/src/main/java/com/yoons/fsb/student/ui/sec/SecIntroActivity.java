package com.yoons.fsb.student.ui.sec;

import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ui.sec.base.SecBaseActivity;

/**
 * 학습결과 화면
 * 
 * @author dckim
 */
public class SecIntroActivity extends SecBaseActivity implements OnClickListener {
	private static final String TAG = "[SecondIntroActivity]";

	private TextView txt_ok;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//나중에 여기서 판단해서 중단/처음 학습인 지판단해야 한다
		/*SecStudyData studyData = SecStudyData.getInstance();
		if (studyData.getIs_halt_study() == 1) {//처음학습일때

		} else {//중도학습일때

		}*/
		Crashlytics.log(getString(R.string.string_ga_SEC_INTRO));
		next();
		finish();
	}

}
