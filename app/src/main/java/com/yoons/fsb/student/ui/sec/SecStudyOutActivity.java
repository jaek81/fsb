package com.yoons.fsb.student.ui.sec;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.NewHomeActivity;
import com.yoons.fsb.student.ui.StudyBookSelectActivity;
import com.yoons.fsb.student.ui.sec.base.SecBaseActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

/**
 * 학습결과 화면
 *
 * @author dckim
 */
public class SecStudyOutActivity extends SecBaseActivity implements OnClickListener {
    private static final String TAG = "[StudyOutcomeActivity]";

    private TextView txt_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_layout_second_studyout);
        else // 숲
            setContentView(R.layout.f_layout_second_studyout);

        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_ok.setOnClickListener(this);

        // WiFi 감도 아이콘 설정
        setPreferencesCallback();

        MediaPlayer mGuidePlayer = MediaPlayer.create(getBaseContext(), R.raw.b_28);
        mGuidePlayer.start();

        Crashlytics.log(getString(R.string.string_ga_SEC_STUDY_FINISH));
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        /* FlurryAgent.onStartSession(this, ServiceCommon.FLURRY_API_KEY); */
        //GA적용
        RequestPhpPost reqQPRO = new RequestPhpPost(mContext);
        reqQPRO.sendQPRO(0, mStudyData.getClass_second_id(), 0, SecStudyData.STR_Z, 0, ServiceCommon.STATUS_END, 0, CommonUtil.getCurrentYYYYMMDDHHMMSS());
        String mcase = Preferences.getCaseStatus(mContext);
        if ("1".equals(mcase) || "2".equals(mcase) || "3".equals(mcase)) {//로그인 케이스1,2,3 경우
            RequestPhpPost reqCASE = new RequestPhpPost(mContext);
            reqCASE.sendCASE(String.valueOf(mStudyData.getClass_second_id()), "CASE" + mcase);
        }
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                txt_ok.setPressed(true);

                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                txt_ok.setPressed(false);

                startActivity(new Intent(this, NewHomeActivity.class));
                finish();
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        if (singleProcessChecker())
            return;

        switch (view.getId()) {

            case R.id.txt_ok:
                String mcase = Preferences.getCaseStatus(mContext);
                if (!"1".equals(mcase) && !"2".equals(mcase) && !"3".equals(mcase)) {//로그인 케이스1,2,3 이 아닐땐 목록으로
                    startActivity(new Intent(this, NewHomeActivity.class));
                }
                finish();
                break;
        }
    }

}
