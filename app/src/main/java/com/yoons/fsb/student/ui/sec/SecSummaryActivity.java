package com.yoons.fsb.student.ui.sec;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.ui.StudyBookSelectActivity;
import com.yoons.fsb.student.ui.sec.base.SecBaseActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import java.util.IllegalFormatException;

/**
 * 학습결과 화면
 *
 * @author dckim
 */
public class SecSummaryActivity extends SecBaseActivity implements OnClickListener {
    private static final String TAG = "[StudyOutcomeActivity]";

    private TextView txt_ok;
    private LinearLayout layout_vip;
    private TextView txt_read, txt_read_cnt, txt_under, txt_under_cnt, txt_test, txt_test_cnt, txt_vip_cnt, txt_step4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_second_summary);

        txt_ok = (TextView) findViewById(R.id.txt_ok);
        txt_read_cnt = (TextView) findViewById(R.id.txt_read_cnt);
        txt_under_cnt = (TextView) findViewById(R.id.txt_under_cnt);
        txt_test_cnt = (TextView) findViewById(R.id.txt_test_cnt);
        txt_vip_cnt = (TextView) findViewById(R.id.txt_vip_cnt);
        txt_step4 = (TextView) findViewById(R.id.txt_step4);
        txt_read = (TextView) findViewById(R.id.txt_read);
        txt_under = (TextView) findViewById(R.id.txt_under);
        txt_test = (TextView) findViewById(R.id.txt_test);
        txt_ok.setOnClickListener(this);

        layout_vip = (LinearLayout) findViewById(R.id.layout_vip);

        // 타이틀 바의 속성 설정
        /*if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1) {
			setTitlebarText(getString(R.string.string_common_sec_part1));
		} else {
			setTitlebarText(getString(R.string.string_common_sec_part2));
		}*/
        if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1) {//파트1
            layout_vip.setVisibility(View.GONE);
            txt_under_cnt.setText(String.valueOf(mStudyData.getPartList().size()));
            txt_read_cnt.setText(String.valueOf(mStudyData.getPartList().size()));
            txt_test_cnt.setText(String.valueOf(mStudyData.getPartList().size()));
            txt_step4.setText("STEP3");
            Crashlytics.log(getString(R.string.string_ga_SEC_PART1_SUMMARY));
        } else {//파트2
            layout_vip.setVisibility(View.VISIBLE);

            txt_under.setText(getString(R.string.string_common_sec_sentence_understand));
            txt_read.setText(getString(R.string.string_common_sec_sentence_understand));
            txt_test.setText(getString(R.string.string_common_sec_sentence_test));

            txt_under_cnt.setText(String.valueOf(mStudyData.getPartList().size()));
            txt_read_cnt.setText(String.valueOf(mStudyData.getPartReadCnt()));
            txt_test_cnt.setText(String.valueOf(mStudyData.getPartList().size()));
            txt_vip_cnt.setText(String.valueOf(mStudyData.getVip_cnt()));
            Crashlytics.log(getString(R.string.string_ga_SEC_PART2_SUMMARY));
        }
        // WiFi 감도 아이콘 설정
        setPreferencesCallback();


        //GA적용
		/*Tracker t = ((AnalyticsSampleApp) getApplication()).getTracker(AnalyticsSampleApp.TrackerName.APP_TRACKER);
		t.Crashlytics.log(getString(R.string.string_ga_StudyOutcomeActivity));
		t.send(new HitBuilders.AppViewBuilder().build());*/
    }

    /**
     * 숫자 값을 문자 값으로 변환
     *
     * @param value 숫자 값
     * @return String 변환된 문자열
     */
    private String getValue(int value) {
        String rtnValue;

        try {
            rtnValue = String.valueOf(value);
        } catch (NullPointerException e) {
            e.printStackTrace();
            rtnValue = "0";
        } catch (IllegalFormatException e) {
            e.printStackTrace();
            rtnValue = "0";
        }

        return rtnValue;
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                txt_ok.setPressed(true);

                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    // for bluetooth 7/24
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                txt_ok.setPressed(false);

                next();
                finish();
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        if (singleProcessChecker())
            return;

        switch (view.getId()) {

            case R.id.txt_ok:
                next();
                finish();
                break;

        }
        int id = view.getId();
        if (id == R.id.btn_fskill) {
            CommonUtil.startPackage(mContext, ServiceCommon.FOURSKILL_PACKAGE, Preferences.getForestCode(this), Preferences.getTeacherCode(mContext), Preferences.getCustomerNo(this), Preferences.getCenter(this));
            finish();
        } else if (id == R.id.btn_list) {
            StudyData.clear();

			/*
			 * startActivityForResult(new Intent(this,
			 * StudyBookSelectActivity.class), ServiceCommon.MSG_STUDY_FINISH);
			 * finish();
			 */
            Intent mIntent = new Intent(this, StudyBookSelectActivity.class);
            mIntent.putExtra("REFRESH", "TRUE");
            startActivity(mIntent);
            finish();
        }
    }

}
