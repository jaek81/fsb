package com.yoons.fsb.student.ui.sec;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.db.DatabaseSync;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.popup.AnimationBox;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.sec.base.SecBaseActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 간지화면의 base class<br>
 * (학습 상태에 따른 가이드 음원, 아이콘, 타이틀, 다음화면 이동 설정 기능)
 *
 * @author dckim
 */
public class SecTitlePaperActivity extends SecBaseActivity implements OnClickListener, OnDialogDismissListener {

    private final static String TAG = "[SecTitlePaperActivity]";
    private HttpJSONRequest request;
    private int status = 0;
    protected MediaPlayer mGuidePlayer = null;
    private ImageView ganji_character, ganji_part_icon;
    private TextView txt_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) { // igse
            setContentView(R.layout.igse_sec_title_paper);
            findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.igse_btn_color_01));
        } else { // 숲
            setContentView(R.layout.f_sec_title_paper);
            findViewById(R.id.btn_ok).setBackground(ContextCompat.getDrawable(this, R.drawable.f_btn_color_01));
        }
        /*
         * if (getIntent().getBooleanExtra(ServiceCommon.PARAM_GO_SETTING,
         * false)) startActivity(new Intent(this, SettingsActivity.class));
         */
        request = new HttpJSONRequest(this);
        request.requestNoti(mSyncHandler, ServiceCommon.MSG_WHAT_CALL_NOTI);
        status = getIntent().getIntExtra(ServiceCommon.TARGET_ACTIVITY, 0);
        mStudyData.setStep(stepToString(status));
        initTitlePaper();
        stepSetUI();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        if (0 < mGuideMsg) {
            guidePlay(mGuideMsg);
            mGuideMsg = 0; // 한번만 실행
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        guideStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                findViewById(R.id.btn_ok).setPressed(true);
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                findViewById(R.id.btn_ok).setPressed(false);
                next();
                finish();
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        if (singleProcessChecker())
            return;

        int id = view.getId();

        if (R.id.btn_ok == id) {
            next();
            finish();
        }
    }

    /**
     * 간지화면의 타이틀 설정
     * <p>
     * 간지화면 타입
     */
    protected void initTitlePaper() {

        // WiFi 감도 아이콘 설정
        //setPreferencesCallback();

        // 간지 아이콘 설정
        //setTitlePaperIcon();

        ((View) findViewById(R.id.btn_ok)).setOnClickListener(this);
        //ganji_character = (ImageView) findViewById(R.id.ganji_character);
        //ganji_part_icon = (ImageView) findViewById(R.id.ganji_part_icon);
        txt_title = (TextView) findViewById(R.id.txt_title);

		/*switch (status) {
        case SecStudyData.TP_TYPE_SEC_SENTENCE_ANSWER_GANJI:
			img_ox.setVisibility(View.VISIBLE);
			txt_ox.setVisibility(View.VISIBLE);
			ganji_character.setVisibility(View.GONE);
			break;
		
		default:
			setTitlePaper();
			break;*/

        // 간지 가이드 음원 출력
        //if (!getIntent().getBooleanExtra(ServiceCommon.PARAM_MUTE_GUIDE, false))
        playGuideMessage();
    }

    private void stepSetUI() {

        String title = "";
        int vipCount = mStudyData.getVip_cnt();
        switch (status) {
            /*case SecStudyData.TP_TYPE_SEC_PART1_GANJI:
                title = getString(R.string.string_common_sec_enter_part1);
                break;*/
            case SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND_GANJI:
                title = getString(R.string.string_common_sec_enter_word_understand);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.VISIBLE);
                break;
            case SecStudyData.TP_TYPE_SEC_WORD_READ_GANJI:
                title = getString(R.string.string_common_sec_enter_word_read);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_word_read_title_paper).setVisibility(View.VISIBLE);
                break;
            case SecStudyData.TP_TYPE_SEC_WORD_TEST_GANJI:
                title = getString(R.string.string_common_sec_enter_word_test);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_word_test_title_paper).setVisibility(View.VISIBLE);
                break;
            /*case SecStudyData.TP_TYPE_SEC_PART2_GANJI:
                title = getString(R.string.string_common_sec_enter_part2);
                break;*/
            case SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI:
                title = getString(R.string.string_common_sec_enter_sentence_understand);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_sentence_study_title_paper).setVisibility(View.VISIBLE);
                findViewById(R.id.sec_sentence_study_title_paper).findViewById(R.id.vip_test).setVisibility(vipCount > 0 ? View.VISIBLE : View.GONE);
                break;
            case SecStudyData.TP_TYPE_SEC_SENTENCE_READ_GANJI:
                title = getString(R.string.string_common_sec_enter_sentence_read);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_sentence_read_title_paper).setVisibility(View.VISIBLE);
                findViewById(R.id.sec_sentence_read_title_paper).findViewById(R.id.vip_test).setVisibility(vipCount > 0 ? View.VISIBLE : View.GONE);
                break;
            case SecStudyData.TP_TYPE_SEC_SENTENCE_VIP_GANJI:
                title = getString(R.string.string_common_sec_enter_sentence_vip);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_sentence_vip_title_paper).setVisibility(View.VISIBLE);
                break;
            case SecStudyData.TP_TYPE_SEC_SENTENCE_TEST_GANJI:
                title = getString(R.string.string_common_sec_enter_sentence_test);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_sentence_test_title_paper).setVisibility(View.VISIBLE);
                findViewById(R.id.sec_sentence_test_title_paper).findViewById(R.id.vip_test).setVisibility(vipCount > 0 ? View.VISIBLE : View.GONE);
                break;
            case SecStudyData.TP_TYPE_SEC_SENTENCE_ANSWER_GANJI:
                title = getString(R.string.string_common_sec_enter_sentence_answer);
                findViewById(R.id.sec_word_study_title_paper).setVisibility(View.GONE);
                findViewById(R.id.sec_sentence_answer_title_paper).setVisibility(View.VISIBLE);
                findViewById(R.id.sec_sentence_answer_title_paper).findViewById(R.id.vip_test).setVisibility(vipCount > 0 ? View.VISIBLE : View.GONE);
                break;
        }
        txt_title.setText(title);
        Crashlytics.log(title);

    }

    /**
     * 간지 화면별 타이틀 내용 설정
     *//*
		protected void setTitlePaperTitle() {
		TextView tv = (TextView) findViewById(R.id.gangi_name);
		if (null == tv)
			return;
		tv.setText(getTitlePaperTitle());
		tv.setVisibility(View.VISIBLE);
		}*/

    /**
     * 간지 화면별 타이틀 이미지 설정 (텍스트로 표현 불가능한 처리를 위함)
     *//*
		protected void setTitlePaperImage() {
		int id = 0;
		
		switch (status) {
		
		case SecStudyData.TP_TYPE_SEC_PART1_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_WORD_READ_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_WORD_TEST_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_PART2_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_SENTENCE_READ_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_SENTENCE_VIP_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_SENTENCE_TEST_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_SENTENCE_ANSWER_GANJI:
			id = R.id.gangi_name_wp;
			break;
		case SecStudyData.TP_TYPE_SEC_SENTENCE_PART1_INCORRECT:
			break;
		}
		
		ImageView iv = (ImageView) findViewById(id);
		if (null == iv)
			return;
		iv.setVisibility(View.VISIBLE);
		}*/

    /**
     * 간지 화면별 이미지 리소스 아이디 취득
     *
     * @return int 케릭터 아이콘 이미지의 리소스 아이디
     */
	/*protected int getGanjiIconId() {
	
		int id = 0;
	
		switch (status) {
		case SecStudyData.TP_TYPE_SEC_PART1_GANJI:
		case SecStudyData.TP_TYPE_SEC_PART2_GANJI:
			id = R.id.ganji_part_icon;
			break;
		case SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND_GANJI:
		case SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI:
			id = R.id.ganji_understand_icon;
			break;
		case SecStudyData.TP_TYPE_SEC_WORD_READ_GANJI:
		case SecStudyData.TP_TYPE_SEC_SENTENCE_READ_GANJI:
			id = R.id.ganji_read_icon;
			break;
		case SecStudyData.TP_TYPE_SEC_WORD_TEST_GANJI:
		case SecStudyData.TP_TYPE_SEC_SENTENCE_TEST_GANJI:
			id = R.id.ganji_test_icon;
			break;
	
		case SecStudyData.TP_TYPE_SEC_SENTENCE_VIP_GANJI:
			id = R.id.ganji_vip_icon;
			break;
	
		case SecStudyData.TP_TYPE_SEC_SENTENCE_ANSWER_GANJI:
			id = R.id.ganji_wrong_icon;
			break;
	
		}
	
		return id;
	}
	
	*//**
     * 간지 화면별 이미지 설정
     *//*
		protected void setTitlePaperIcon() {
		
		ImageView iv = (ImageView) findViewById(getGanjiIconId());
		if (null == iv)
		return;
		
		iv.setVisibility(View.VISIBLE);
		}*/

    /**
     * 간지 화면별 가이드 음원 재생
     */
    protected void playGuideMessage() {

        switch (status) {
            case SecStudyData.TP_TYPE_SEC_PART1_GANJI:
                setGuideMsg(R.raw.sec_word_title);
                break;
            case SecStudyData.TP_TYPE_SEC_PART2_GANJI:
                setGuideMsg(R.raw.sec_sentence_title);
                break;
        }
    }

    /**
     * 문장학습, 동영상 리뷰로 이동 시 안내 문구 출력<br>
     * ("인터넷이 연결된 상태로 학습해야 최상의 효과가 있고 학습 시간도 짧아지므로, 꼭 인터넷에 연결 상태로 학습하시기 바랍니다.")
     */
    protected void showNetworkNotiDialog() {
        mMsgBox = new MessageBox(this, 0, R.string.string_msg_please_using_network_noti);

        mMsgBox.setConfirmText(R.string.string_common_confirm);

        mMsgBox.setOnDialogDismissListener(this);
        mMsgBox.show();
    }

    @Override
    public void onDialogDismiss(int result, int dialogId) {

        next();
        finish();
    }

    public void syncUploadDatabaseTable() {
        if (CommonUtil.isAvailableNetwork(mContext, false)) {
            DatabaseSync databaseSync = new DatabaseSync(this);
            databaseSync.uploadSyncStart(mSyncHandler, true, false);
        }
    }

    public Handler mSyncHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_START:
                    syncUploadDatabaseTable();
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_SUCCESS:
                    Log.e(TAG, "MSG_DATABASE_UPLOAD_SYNC_SUCCESS");
                    break;

                case ServiceCommon.MSG_DATABASE_UPLOAD_SYNC_FAIL:
                    Log.e(TAG, "MSG_DATABASE_UPLOAD_SYNC_FAIL");
                    break;

            }
            switch (msg.arg1) {
                case ServiceCommon.MSG_WHAT_CALL_NOTI:

                    String status = "";

                    if (msg.obj != null) {
                        JSONObject obj = (JSONObject) (msg.obj);
                        try {
                            JSONArray array = new JSONArray(obj.getString("out1"));

                            for (int i = 0; i < array.length(); i++) {

                                status = array.getJSONObject(i).getString("command");

                                if (status.equals("MANAGE") || StudyData.getInstance().call) {

                                    ani_box = new AnimationBox(mContext, R.anim.ani_call);
                                    ani_box.setConfirmText(R.string.string_common_confirm);

                                    if (!ani_box.isShowing()) {
                                        ani_box.show();
                                        guidePlay(R.raw.p_call);
                                    }
                                    //sendBroadcast(new Intent(ServiceCommon.ACTION_NOTI_FOUCS));
                                    StudyData.getInstance().call = false;
                                } else if (status.equals("KEEPGOING")) {//취소일때
                                    StudyData.getInstance().call = false;
                                } else if (status.equals("INITIALIZE")) {//중복로그인
                                    ((Activity) mContext).finish();
                                }

                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        /* first_Check=false; */
                    }

                    break;
            }

            super.handleMessage(msg);
        }
    };

    /**
     * 가이드 음성,효과음 재생
     *
     * @param soundResId 음원 리소스 id
     */
    protected void guidePlay(int soundResId) {

        if (null != mGuidePlayer)
            guideStop();

        mGuidePlayer = MediaPlayer.create(getBaseContext(), soundResId);
        if (null != mGuidePlayer) {
            mGuidePlayer.start();
        }
    }

    /**
     * 가이드 음성,효과음 재생
     *
     * @param soundResId 음원 리소스 id
     * @param listener   OnCompletionListener(재생 완료 통지)
     */
    protected void guidePlay(int soundResId, OnCompletionListener listener) {

        if (null != mGuidePlayer)
            guideStop();

        mGuidePlayer = MediaPlayer.create(getBaseContext(), soundResId);
        if (null != mGuidePlayer) {
            mGuidePlayer.setOnCompletionListener(listener);
            mGuidePlayer.start();
        }
    }

    /**
     * 가이드 음성,효과음 재생중지(리소스 해제)
     */
    protected void guideStop() {

        if (null == mGuidePlayer)
            return;

        if (mGuidePlayer.isPlaying())
            mGuidePlayer.stop();
        mGuidePlayer.release();
        mGuidePlayer = null;
    }

    /**
     * 가이드 음성,효과음 설정(onResume 시 사운드 재생)
     *
     * @param guideResId
     */
    protected void setGuideMsg(int guideResId) {
        mGuideMsg = guideResId;
    }

    /**
     * 중단학습일때 q에 담을꺼 정리
     *
     * @param step
     * @return
     */
    private String stepToString(int step) {
        String result = "";

        switch (step) {
            //w1
            case SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND_GANJI:
            case SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND:
                result = SecStudyData.STR_W1;
                break;
            //w2
            case SecStudyData.TP_TYPE_SEC_WORD_READ_GANJI:
            case SecStudyData.TP_TYPE_SEC_WORD_READ:
                result = SecStudyData.STR_W2;
                break;
            //w3
            case SecStudyData.TP_TYPE_SEC_WORD_TEST_GANJI:
            case SecStudyData.TP_TYPE_SEC_WORD_TEST:
                result = SecStudyData.STR_W3;
                break;
            //s1
            case SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI:
            case SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND:
                result = SecStudyData.STR_S1;
                break;
            //s2
            case SecStudyData.TP_TYPE_SEC_SENTENCE_READ_GANJI:
            case SecStudyData.TP_TYPE_SEC_SENTENCE_READ:
                result = SecStudyData.STR_S2;
                break;
            //s3
            case SecStudyData.TP_TYPE_SEC_SENTENCE_VIP_GANJI:
            case SecStudyData.TP_TYPE_SEC_SENTENCE_VIP:
                result = SecStudyData.STR_S3;
                break;
            //s4
            case SecStudyData.TP_TYPE_SEC_SENTENCE_TEST_GANJI:
            case SecStudyData.TP_TYPE_SEC_SENTENCE_TEST:
                result = SecStudyData.STR_S4;
                break;
            //n1
            case SecStudyData.TP_TYPE_SEC_SENTENCE_ANSWER_GANJI:
            case SecStudyData.TP_TYPE_SEC_SENTENCE_PART1_RESULT:
            case SecStudyData.TP_TYPE_SEC_SENTENCE_PART2_RESULT:
                result = SecStudyData.STR_N1;
                break;
        }

        return result;
    }
}
