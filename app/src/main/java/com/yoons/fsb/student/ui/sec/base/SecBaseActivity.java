package com.yoons.fsb.student.ui.sec.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.yoons.fsb.student.MediaButtonReceiver;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.ui.NewHomeActivity;
import com.yoons.fsb.student.ui.StudyBookSelectActivity;
import com.yoons.fsb.student.ui.control.Indicator;
import com.yoons.fsb.student.ui.popup.AnimationBox;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.ui.sec.SecStudyOutActivity;
import com.yoons.fsb.student.ui.sec.SecSummaryActivity;
import com.yoons.fsb.student.ui.sec.SecTitlePaperActivity;
import com.yoons.fsb.student.ui.sec.study.SecReadActivity;
import com.yoons.fsb.student.ui.sec.study.SecTestActivity;
import com.yoons.fsb.student.ui.sec.study.SecUnderStandActivity;
import com.yoons.fsb.student.ui.sec.study.SecVipActivity;
import com.yoons.fsb.student.ui.sec.study.SecWrongActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

public class SecBaseActivity extends Activity implements OnClickListener {

    public static final String ACTIVITY_LOG_OUT_EVENT = "ACTIVITY_LOG_OUT_EVENT";
    public static final String LOG_TAG = "BaseActivity";

    protected Context mContext = null;
    protected Indicator mIndicator = null;

    protected boolean mExitInBackground = true;

    protected MessageBox mMsgBox = null;

    protected int mGuideMsg = 0;

    protected boolean mIsProcess = false;

    // 학습 중 화면 잠금,꺼짐으로 onPause()가 발생한 경우 간지로 재시작
    protected boolean mIsReStart = true;

    protected BroadcastReceiver mFinishEventReceiver = null;

    protected BroadcastReceiver mSlogEventReceiver = null;
    private AudioManager mAudioManager;
    private ComponentName mRemoteControlResponder;

    protected OnSharedPreferenceChangeListener mSharedPrefChangeListener = null;
    /**
     * 네트워크 상태 변화 감시
     */
    protected NetworkChangeReceiver mNetChangeReceiver = null;

    // 리시버
    private EarPhoneReceiver earPhoneReceiver;

    protected AnimationBox ani_box;

    protected Queue<String> q = new LinkedList<String>();

    protected SecStudyData mStudyData = null;

    protected boolean isNext = true;

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        mIndicator = (Indicator) findViewById(R.id.ganji_titlebar);
//		mTitleBar.setOnClickListener(this);//나중에 삭제해야됨
//		//setTitlebarCategory(getString(R.string.string_titlebar_category_test));
//		mTitleBar.setTitle(getString(R.string.title_smart_test));
        setTitlebarCategory(mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1 ? getString(R.string.string_sec_category_word) : getString(R.string.string_sec_category_sentence));
        CommonUtil.setGlobalFont(this, getWindow().getDecorView());
        CommonUtil.setSizeLayout(this, getWindow().getDecorView());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        //keepScreen(true);

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);//화면전환설정

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);//오디오 설정
        mRemoteControlResponder = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());//클릭햇을때 오디오가 플레이되지 않게

        mAudioManager.registerMediaButtonEventReceiver(mRemoteControlResponder);

        mStudyData = SecStudyData.getInstance();

        // 볼륨키 동작 시 무조건 미디어 볼륨 조절하도록 함.
        setMediaVolumeOnly();

        // Activity 종료 Receiver 등록
        if (mNetChangeReceiver == null) {
            mNetChangeReceiver = new NetworkChangeReceiver();
        }
        registerReceiver(mNetChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        if (earPhoneReceiver == null) {
            earPhoneReceiver = new EarPhoneReceiver();
        }

        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(earPhoneReceiver, filter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsReStart = true;
    }

    /**
     * Override method (View.setKeepScreenOn)
     *
     * @param isOn true이면 화면 잠기지 않도록 설정함.
     */
    protected void keepScreen(boolean isOn) {
        findViewById(android.R.id.content).setKeepScreenOn(isOn);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mAudioManager.unregisterMediaButtonEventReceiver(mRemoteControlResponder);

        if (null != mContext)
            mContext = null;

        try {
            // Activity 종료 Receiver 해제
            if (null != mFinishEventReceiver) {
                unregisterReceiver(mFinishEventReceiver);
                mFinishEventReceiver = null;
            }
            if (null != mNetChangeReceiver) {
                unregisterReceiver(mNetChangeReceiver);
                mNetChangeReceiver = null;
            }
            if (null != earPhoneReceiver) {
                unregisterReceiver(earPhoneReceiver);
                earPhoneReceiver = null;
            }

            if (null != mSlogEventReceiver) {
                unregisterReceiver(mSlogEventReceiver);
                mSlogEventReceiver = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isShowingMsgBox()) {
            mMsgBox.dismiss();
            mMsgBox.setOnDialogDismissListener(null);
            mMsgBox = null;
        }
    }

    /**
     * 모든 ACTIVITY를 종료 시키는 이벤트 수신
     */
    protected void setSlogReceiver() {
        // 이벤트 Receiver 생성
        registerReceiver(mSlogEventReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, final Intent intent) {

                String action = intent.getAction();
                if (action.equalsIgnoreCase(ServiceCommon.ACTION_SLOG)) {

                    //SLOG 받을때
                }
            }
        }, new IntentFilter(ServiceCommon.ACTION_SLOG));
    }

    /**
     * 메시지 박스가 활성화되어 있는지 확인한다.
     *
     * @return true이면 메시지 박스가 보이는 상태임
     */
    public boolean isShowingMsgBox() {
        if (null != mMsgBox && mMsgBox.isShowing())
            return true;

        return false;
    }

    /**
     * 일정 시간(500mili) 동안 클래그 설정을 변경함<br>
     * 재 진입을 막기 위함
     *
     * @return
     */
    protected boolean singleProcessChecker() {
        if (mIsProcess)
            return true;

        if (null == mContext)
            return true;

        mIsProcess = true;

        TimerTask timerTask = new TimerTask() {
            public void run() {
                mIsProcess = false;
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, 500);
        return false;
    }

    /**
     * 학습 중 Flag 설정 취소(학습 중이 아님)
     */
    private void cancelStudyingFlag() {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Preferences.setStudying(SecBaseActivity.this, false);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 볼륨 키 설정으로 미디어 볼륨만 제어하도록 설정
     */
    protected void setMediaVolumeOnly() {
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void finish() {

        overridePendingTransition(0, R.anim.slide_in_right);
        super.finish();
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT: {
                AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
                am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
                return true;
            }

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS: {
                AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
                am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
                return true;
            }
        }

        return super.onKeyUp(keyCode, event);
    }

    /**
     * 타이틀바의 중앙에 표시 할 내용을 설정함.
     *
     * @param String 표시할 문자열
     */
    public void setTitlebarText(String title) {

		/*if (null == mTitleBar || null == title)
			return;

		mTitleBar.setTitle(title);*/
    }

    /**
     * 타이틀바의 좌측에 표시 할 내용을 설정함.(예, "[학습]", "[복습]")
     *
     * @param String 표시할 문자열
     */
    public void setTitlebarCategory(String category) {

		/*if (null == mTitleBar || null == category)
			return;

		mTitleBar.showCategory(true, category);*/
    }

    /**
     * 타이틀바의 좌측에 사용자 이름을 나타냄
     */
    public void showUserName() {

		/*if (null == mTitleBar)
			return;

		mTitleBar.setUserName(true);*/
    }

    /**
     * Network Change Receiver Class
     */
    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (!ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction()))
                return;

            NetworkInfo ni = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

            if (ni.getState() == ni.getState().CONNECTED || ni.getState() == ni.getState().CONNECTING) {
                Preferences.setNetworkType(context, 9);
            } else {
                Preferences.setNetworkType(context, -1);
            }
        }
    }

    private class EarPhoneReceiver extends BroadcastReceiver {
        boolean first_bol = true;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                if (!first_bol) {
                    int state = intent.getIntExtra("state", -1);
                    switch (state) {
                        case 0:
                            Toast.makeText(context, "이어폰이 제거 되었습니다.", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            Toast.makeText(context, "이어폰이 연결 되었습니다.", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(context, "이어폰 정보를 알수 없음.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    first_bol = false;
                }
            }
        }
    }

    /**
     *
     */
    protected void next() {
        if (isNext) {//증복 실행 방지
            isNext = false;
            int status = 0;
            try {
                status = mStudyData.getStatus_q().poll();//다음 꺼 꺼내기
            } catch (Exception e) {
                startActivity(new Intent(this, NewHomeActivity.class));
            }
            /*if (SecStudyData.TP_TYPE_SEC_PART1_GANJI == status) { //	Part1 간지
                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecWrongActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecTestActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecReadActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                startActivity(new Intent(this, SecVipActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else */if (SecStudyData.TP_TYPE_SEC_PART1_SUMMARY == status) { //	Part1 Summary

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecSummaryActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND_GANJI == status) { // 단어/구 학습 간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND == status) { //	단어/구 학습

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecUnderStandActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_WORD_READ_GANJI == status) { //	단어/구 읽기 간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_WORD_READ == status) { // 단어/구 읽기

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecReadActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_WORD_TEST_GANJI == status) { // 단어/구 TEST 간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_WORD_TEST == status) { // 단어/구 TEST

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART1);
                startActivity(new Intent(this, SecTestActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } /*else if (SecStudyData.TP_TYPE_SEC_PART2_GANJI == status) { //	PART2 간지

				mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
				startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

			} else if (SecStudyData.TP_TYPE_SEC_PART2_SUMMARY == status) { //	문장훈련 Summary

				mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
				startActivity(new Intent(this, SecSummaryActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
			}*/ else if (SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND_GANJI == status) { //	문장 이해 간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND == status) { // 문장 이해

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecUnderStandActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_READ_GANJI == status) { // 문장 읽기 간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_READ == status) { // 문장 읽기

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecReadActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_VIP_GANJI == status) { // VIP훈련간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_VIP == status) { // VIP훈련

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecVipActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_TEST_GANJI == status) { // 문장 TEST 간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_TEST == status) { // 문장 TEST

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecTestActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_ANSWER_GANJI == status) { // 정답확인간지

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                int p1 = mStudyData.getTestWorngCnt(ServiceCommon.PART_GB_PART1);
                int p2 = mStudyData.getTestWorngCnt(ServiceCommon.PART_GB_PART2);
                //startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                if (p1 != 0 || p2 != 0) {
                    startActivity(new Intent(this, SecTitlePaperActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));
                } else {
                    startActivity(new Intent(this, SecStudyOutActivity.class));
                }

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_PART1_RESULT == status) { // PART1결과화면

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_PART1_INCORRECT == status) { // 단어/구 오답 정리

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecWrongActivity.class).putExtra(ServiceCommon.TARGET_ACTIVITY, status));

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_PART2_RESULT == status) { // PART2결과화면

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_PART2_INCORRECT == status) { // 문장 오답정리

                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);

            } else if (SecStudyData.TP_TYPE_SEC_SENTENCE_FINISH == status) { //	완료화면
                mStudyData.setCurrent_part(ServiceCommon.PART_GB_PART2);
                startActivity(new Intent(this, SecStudyOutActivity.class));
            } else {
                startActivity(new Intent(this, NewHomeActivity.class));
            }

            finish();
        }
    }

    /**
     * 환경설정 값 변경 감시 설정<br>
     * 해, 구름 : Preferences.getServerRequestSuccess(context ctx) 값 true (WIFI감도에
     * 따라 해, 구름 표시)<br>
     * 비 : Preferences.getServerRequestSuccess(context ctx) 값 false
     */
    protected void setPreferencesCallback() {

        if (null != mSharedPrefChangeListener)
            return;

        mSharedPrefChangeListener = new OnSharedPreferenceChangeListener() {

            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

                if (null == key)
                    return;

                if (Preferences.KEY_WIFI_LEVEL.equals(key) || Preferences.KEY_NETWORK_TYPE.equals(key))
                    if (mIndicator != null)
                        mIndicator.setSensitiveIcon();
            }
        };

        SharedPreferences prefs = Preferences.getPref(this);
        if (null != prefs) {
            prefs.registerOnSharedPreferenceChangeListener(mSharedPrefChangeListener);
            mSharedPrefChangeListener.onSharedPreferenceChanged(prefs, Preferences.KEY_WIFI_LEVEL);
            mSharedPrefChangeListener.onSharedPreferenceChanged(prefs, Preferences.KEY_NETWORK_TYPE);
        }

        // Wi-Fi 아이콘 설정 시 Bluetooth도 함께 설정하므로 여기에서 처리함.
        if (mIndicator != null) {
            mIndicator.reflectChangeStatus();
        }
    }
}
