package com.yoons.fsb.student.ui.sec.base;

import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomVorbisPlayer;
import com.yoons.fsb.student.custom.CustomWavRecoder;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.hssb.student.custom.CustomMp3Recorder;
import com.yoons.recognition.VoiceRecognizerWeb;

import java.io.File;

/**
 * 학습 화면(복습,오디오학습,단어시험,문장시험)의 Base Class
 * 
 * @author jaek
 */
public class SecBaseStudyActivity extends SecBaseActivity {

	// vorbis file player
	protected CustomVorbisPlayer mVorbisPlayer = null;

	// android media player 
	protected MediaPlayer mMediaPlayer = null, mRecordPlayer = null, mRecordNotiPlayer = null;

	// mp3 recorder 
	protected CustomMp3Recorder mMp3Recorder = null;

	// wav recorder (음성 -> text 변환 및 채점을 위한) 
	protected CustomWavRecoder mWavRecorder = null;

	// 음성 인식 채점 task 
	protected VoiceRecognizerWeb VRUTask = null;

	// 쓰기, 녹음 효과 애니메이션 쓰레드 
	protected Thread mIngEffectThread = null, mCompleteEffectThread = null;

	// 단어시험,보충 타이머 쓰레드 
	protected Thread mTimerThread = null;

	// 문장시험,보충 결과 저장 완료 여부 체크 쓰레드 
	protected Thread mResultAddCompleteCheckThread = null;

	// 일회성 녹음 저장 경로.. 재녹음시 기존 파일은 삭제함 
	protected String mRecordFilePath = "";

	// 음성 텍스트 
	protected String mVoiceText = "";

	// 음성 점수 
	protected int mVoiceScore = 0;

	// int형의 millisecond ex> 1000 -> 1초 
	protected int mCurPosition = 0;

	// 현재 틀린문제의 음성 재생 횟수 => 총 2회 들려줌 
	protected int mCurPlayCount = 1;

	// 현재 문제 
	protected int mCurStudyIndex = 0;

	// 총 문제 수 
	protected int mTotalStudyCount = 1;

	// 현재 틀린 문제 
	protected int mCurWrongStudyIndex = 0;

	// 총 틀린 문제 수 
	protected int mTotalWrongStudyCount = 1;

	// 현재 틀린 문제 반복 횟수 
	protected int mCurWrongStudyPracticeCount = 1;

	// 단어시험,보충 보기 애니메이션 완료 횟수 
	protected int mCurAnswerAniCount = 1;

	// 단어시험,보충 타이머 인덱스 
	protected int mTimerIndex = 4;

	// 사용자 설정 미디어 볼륨 
	protected int mUserMediaVolume = -1;

	// 쓰기, 녹음 버튼 팁 메세지 width 
	protected int mTooltipWidth = 0;

	// 쓰기, 녹음 버튼 팁 메세지 height
	protected int mTooltipHeight = 0;

	// 최대 녹음 시간 
	protected int mMaxRecTime = 0;

	// 단어시험,보충 타이머 준비 여부 
	protected boolean mIsTimerReady = false;

	// 음성 인식 사용 여부 
	protected boolean mIsRecoginition = false;

	// 쓰기, 녹음 진행 여부 
	protected boolean mIsWriting = false, mIsRecording = false;

	// 학습 시작 여부 
	protected boolean mIsStudyStart = false;

	protected boolean mTestSkip = false;

	protected LinearLayout layout_status_part1, layout_status_part2;

	protected TextView txt_part1_under, txt_part1_read, txt_part1_test, txt_part2_under, txt_part2_read, txt_part2_vip, txt_part2_test, txt_part2_wrong_answer;

	protected ImageView img_part2_vip_arraw;
	protected boolean isQPOR = true;//qpro 동작 여부 일일평가때문에
	//통신
	//타이머 관련
	private int pos = 0;
	private Thread callTread;
	private int callCheckTime = 0;
	public static final int TERM_MSEC = 10000;
	private long StartTime = 0;

	@Override
	public void setContentView(int layoutResID) {
		// TODO Auto-generated method stub
		super.setContentView(layoutResID);
		//statusSetting();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		StartTime = System.currentTimeMillis();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (isQPOR) {
			RequestPhpPost reqQPRO = new RequestPhpPost(mContext);
			reqQPRO.sendQPRO(0, mStudyData.getClass_second_id(), 0, mStudyData.getStep(), 0, ServiceCommon.STATUS_START, pos, CommonUtil.getCurrentYYYYMMDDHHMMSS());
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		setFinish();
	}

	/**
	 * 에코 증폭 수치를 현재 미디어 볼륨에 적용함
	 */
	public void setEchoVolume() {
		if (null == mContext)
			return;

		int prefValue = Preferences.getStudyEchoVolume(mContext);
		if (0 < prefValue) {
			AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
			mUserMediaVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
			int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			int echoVolume = mUserMediaVolume + ((maxVolume * prefValue) / 100);

			if (echoVolume > maxVolume)
				echoVolume = maxVolume;

			am.setStreamVolume(AudioManager.STREAM_MUSIC, echoVolume, 0);
		}
	}

	/**
	 * 에코 증폭 수치 적용 전 미디어 볼륨 값으로 원복함
	 */
	protected void releaseEchoVolume() {
		if (0 <= mUserMediaVolume) {
			AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
			am.setStreamVolume(AudioManager.STREAM_MUSIC, mUserMediaVolume, 0);
		}
	}

	/**
	 * 학습 종료시 데이터를 정리함
	 */
	protected void setFinish() {
		if (null != mContext)
			mContext = null;

		if (null != mTimerThread) {
			mTimerThread.interrupt();
			mTimerThread = null;
		}

		if (null != mIngEffectThread) {
			mIngEffectThread.interrupt();
			mIngEffectThread = null;
		}

		if (null != mCompleteEffectThread) {
			mCompleteEffectThread.interrupt();
			mCompleteEffectThread = null;
		}

		if (null != mResultAddCompleteCheckThread) {
			mResultAddCompleteCheckThread.interrupt();
			mResultAddCompleteCheckThread = null;
		}

		if (null != mMediaPlayer) {
			if (mMediaPlayer.isPlaying())
				mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}

		if (null != mRecordPlayer) {
			if (mRecordPlayer.isPlaying())
				mRecordPlayer.stop();
			mRecordPlayer.release();
			mRecordPlayer = null;
		}

		if (null != mWavRecorder) {
			if (mIsRecording && null == mRecordNotiPlayer)
				mWavRecorder.stopRecording();
			mWavRecorder = null;
		}

		if (null != mRecordNotiPlayer) {
			if (mRecordNotiPlayer.isPlaying())
				mRecordNotiPlayer.stop();
			mRecordNotiPlayer.release();
			mRecordNotiPlayer = null;
		}

		if (null != mMp3Recorder) {
			if (mMp3Recorder.isRecording())
				mMp3Recorder.stop();
			mMp3Recorder = null;
		}

		if (null != mRecordFilePath) {
			File fileRecord = new File(mRecordFilePath);
			if (fileRecord.exists())
				fileRecord.delete();
		}

		releaseEchoVolume();
	}

	/**
	 * 만들어놧는데 민트패드에서 안쓰느듯하여 안씀
	 */
	private void StartQPRO() {
		callTread = new Thread("QPRO") {
			public void run() {
				while (null != callTread && !callTread.isInterrupted()) {
					SystemClock.sleep(1000);

					callCheckTime += 1000;
					StudyData data = StudyData.getInstance();
//					if (!(data.audioPlaying.equals(ServiceCommon.STATUS_PAUSE) && curStatus.equals(ServiceCommon.STATUS_PAUSE)) && !(data.audioPlaying.equals(ServiceCommon.STATUS_STOP) && curStatus.equals(ServiceCommon.STATUS_STOP)) || !data.mCurrentStatus.equalsIgnoreCase("S11")) {//pause랃 stop 은 계속 안보내기

					// 10초
					if (TERM_MSEC == callCheckTime) {
						callCheckTime = 0;
						int play_time = (int) ((System.currentTimeMillis() - StartTime) / 1000);
						RequestPhpPost reqQPRO = new RequestPhpPost(mContext);
						//reqQPRO.sendQPRO(mStudyData.getBook_id(), mStudyData.getClass_second_id(), ServiceCommon.STATUS_PLAY, play_time, ServiceCommon.STATUS_START, pos, CommonUtil.getCurrentDateTime2());

					} else if (callCheckTime > TERM_MSEC) {
						callCheckTime = 0;
					}

				}
			}
		};

		callTread.start();
	}

	@Override
	protected void next() {
		// TODO Auto-generated method stub
		super.next();

		RequestPhpPost reqQPRO = new RequestPhpPost(mContext);
		int play_time = (int) ((System.currentTimeMillis() - StartTime) / 1000);
		//reqQPRO.sendQPRO(mStudyData.getBook_id(), mStudyData.getClass_second_id(), mStudyData.getBook_detail_id(), mStudyData.getStep(), play_time, ServiceCommon.STATUS_END, pos, CommonUtil.getCurrentYYYYMMDDHHMMSS());
		reqQPRO.sendQPRO(0, mStudyData.getClass_second_id(), 0, mStudyData.getStep(), play_time, ServiceCommon.STATUS_END, pos, CommonUtil.getCurrentYYYYMMDDHHMMSS());

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.ganji_titlebar:
			/*finish();*/
			if (ServiceCommon.SERVER == ServiceCommon.TEST_SERVER) {
				mTestSkip = true;
			}
			break;

		default:
			break;
		}
	}

	private void statusSetting() {
		layout_status_part1 = (LinearLayout) findViewById(R.id.layout_status_part1);
		layout_status_part2 = (LinearLayout) findViewById(R.id.layout_status_part2);
		txt_part1_under = (TextView) findViewById(R.id.txt_part1_under);
		txt_part1_read = (TextView) findViewById(R.id.txt_part1_read);
		txt_part1_test = (TextView) findViewById(R.id.txt_part1_test);
		txt_part2_under = (TextView) findViewById(R.id.txt_part2_under);
		txt_part2_read = (TextView) findViewById(R.id.txt_part2_read);
		txt_part2_vip = (TextView) findViewById(R.id.txt_part2_vip);
		txt_part2_test = (TextView) findViewById(R.id.txt_part2_test);
		txt_part2_wrong_answer = (TextView) findViewById(R.id.txt_part2_wrong_answer);
		img_part2_vip_arraw = (ImageView) findViewById(R.id.txt_part2_vip_arraw);
		TextView blackTxt = null;
		if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1) {//파트1일때
			layout_status_part1.setVisibility(View.VISIBLE);
			layout_status_part2.setVisibility(View.GONE);
			if (SecStudyData.STR_W1.equals(mStudyData.getStep())) {
				blackTxt = txt_part1_under;
			} else if (SecStudyData.STR_W2.equals(mStudyData.getStep())) {
				blackTxt = txt_part1_read;
			} else if (SecStudyData.STR_W3.equals(mStudyData.getStep())) {
				blackTxt = txt_part1_test;
			}
		} else {
			layout_status_part2.setVisibility(View.VISIBLE);
			layout_status_part1.setVisibility(View.GONE);

			if (mStudyData.getVip_cnt() == 0) {
				img_part2_vip_arraw.setVisibility(View.GONE);
				txt_part2_vip.setVisibility(View.GONE);
			}

			if (SecStudyData.STR_S1.equals(mStudyData.getStep())) {
				blackTxt = txt_part2_under;
			} else if (SecStudyData.STR_S2.equals(mStudyData.getStep())) {
				blackTxt = txt_part2_read;
			} else if (SecStudyData.STR_S3.equals(mStudyData.getStep())) {
				blackTxt = txt_part2_vip;
			} else if (SecStudyData.STR_S4.equals(mStudyData.getStep())) {
				blackTxt = txt_part2_test;
			} else if (SecStudyData.STR_N1.equals(mStudyData.getStep())) {
				blackTxt = txt_part2_wrong_answer;
			}
		}
		if (blackTxt != null) {
			blackTxt.setTextColor(Color.BLACK);
		}

	}

}
