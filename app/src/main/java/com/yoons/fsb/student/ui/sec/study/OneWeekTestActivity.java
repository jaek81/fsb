package com.yoons.fsb.student.ui.sec.study;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.OneWeekData;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.StudyOutcomeActivity;
import com.yoons.fsb.student.ui.control.StepStatusBar;
import com.yoons.fsb.student.ui.sec.base.SecBaseStudyActivity;
import com.yoons.fsb.student.ui.textview.AutofitHelper;
import com.yoons.fsb.student.ui.textview.AutofitTextView;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.FlipAnimation;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import javax.annotation.Resource;

/**
 * 단어시험 화면
 *
 * @author jaek
 */
public class OneWeekTestActivity extends SecBaseStudyActivity
        implements OnClickListener, OnCompletionListener, OnSeekCompleteListener {

    private final String STATUS = "DW";

    private final String KIND_PRESSED = "KIND_PRESSED";
    private final String KIND_ACTIVED = "KIND_ACTIVED";
    private final String KIND_SELECTED = "KIND_SELECTED";
    private final String KIND_ENABLED = "KIND_ENABLED";

    private final String BTN_ALL_TRUE = "BTN_ALL_TRUE";
    private final String BTN_ALL_FALSE = "BTN_ALL_FALSE";

    private LinearLayout mAnswer1 = null, mAnswer2 = null, mAnswer3 = null, mAnswer4 = null, layout_speaker = null;
    private LinearLayout sec_timer_main_layout, sec_test_study_layout, sec_test_result_layout;
    //private LinearLayout sec_setp_statusbar;

    private TextView mStudyView = null, mStudyCountView = null, mResultView = null, mResultView2 = null, mResultTime = null;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;
    private TextView mWrongStudyView = null, mWrongStudyCountView = null, mWrongStudyAnswerView = null;
    private Button mResultConfirm = null, mNextStatus = null, btn_timer_start = null;
    private ArrayList<ImageView> mTimerViewList = null;
    private ArrayList<LinearLayout> mAnswerList = null;//정답 레이아웃 목록
    private ImageView img_result;

    private Drawable mTimerBuble = null;
    private Drawable mTimerBubleDis = null;
    private boolean mWrongStudy = false;
    //private StepStatusBar mStepStatusBar = null;
    private long StartTime = 0;
    private boolean mTesting = false;//문제 풀고있는중인지(애니메이션 안끝낫는지)

    // 타이머
    private long startTime = 0L;
    long timeInMilliseconds = 0L;
    long updatedTime = 0L;

    int correct_idx = 0;
    /*
     * //타이머 private long startTime = 0L; private Handler customHandler = new
     * Handler(); long timeInMilliseconds = 0L; long timeSwapBuff = 0L; long
     * updatedTime = 0L; private TextView txt_small_timer;
     */
    StudyData WeekStudyData;

    // private boolean mOnclick=true;

    private int[] short_answer = {R.id.word_answer_1, R.id.word_answer_2, R.id.word_answer_3, R.id.word_answer_4};
    private int[] short_answer_text = {R.id.word_study_answer1_text, R.id.word_study_answer2_text, R.id.word_study_answer3_text, R.id.word_study_answer4_text};
    private int[] short_linear = {R.id.word_study_answer_1, R.id.word_study_answer_2, R.id.word_study_answer_3, R.id.word_study_answer_4};
    private int[] long_answer = {R.id.list_answer_1, R.id.list_answer_2, R.id.list_answer_3, R.id.list_answer_4};
    private int[] long_linear = {R.id.word_study_list_answer_1, R.id.word_study_list_answer_2, R.id.word_study_list_answer_3, R.id.word_study_list_answer_4};
    private int[] long_answer_text = {R.id.word_study_list_answer1_text, R.id.word_study_list_answer2_text, R.id.word_study_list_answer3_text, R.id.word_study_list_answer4_text};

    private Drawable answer_on, answer_solid = null;
    private int btn_learning_answer_num_on, btn_learning_answer_num_off, text_color = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_sec_test_layout);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                setContentView(R.layout.w_sec_test_layout);
            else // 숲
                setContentView(R.layout.f_sec_test_layout);
        }

        WeekStudyData = StudyData.getInstance();

        setWidget();
        setPreferencesCallback();
        mCurStudyIndex = 0;

        // TitleView
        tBookName.setText(WeekStudyData.mProductName);
        tCategory.setText(R.string.string_common_today_study);
        if (WeekStudyData.mOneWeekData.size() == 4) {
            tCategory2.setText(R.string.string_common_day_test);
            // GA적용
            Crashlytics.log(getString(R.string.string_common_day_test));
        } else {
            tCategory2.setText(R.string.string_common_week_test);
            // GA적용
            Crashlytics.log(getString(R.string.string_common_week_test));
        }

        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);

        sec_test_study_layout.setVisibility(View.VISIBLE);
        sec_timer_main_layout.setVisibility(View.GONE);

        //StartTime = System.currentTimeMillis();

        //php post로 한 이유:lms변동 없이 해야해서,play 없음
        //slog start
        RequestPhpPost reqSlog = new RequestPhpPost(mContext);
        int playingTime = 0;
        int audioTime = 0;
        int aCnt = 0;
        int aTime = 0;
        int mCnt = 0;
        int mTime = 0;
        int mRtime = 0;

        playingTime = WeekStudyData.mAudioStudyTime / 1000;
        audioTime = WeekStudyData.mAudioTotalMin / 1000;
        aCnt = (WeekStudyData.mAudioRecordIndex - WeekStudyData.mRecMCnt) < 0 ? 0 : (WeekStudyData.mAudioRecordIndex - WeekStudyData.mRecMCnt);
        aTime = (WeekStudyData.mTotalRecTime - WeekStudyData.mRecMTime) < 0 ? 0 : (WeekStudyData.mTotalRecTime - WeekStudyData.mRecMTime);
        mCnt = WeekStudyData.mRecMCnt;
        mTime = WeekStudyData.mRecMTime;
        mRtime = WeekStudyData.mRecRTime * WeekStudyData.mRTagRepeatCnt;

        reqSlog.sendSLOG(WeekStudyData.mProductNo, WeekStudyData.mBookDetailId, WeekStudyData.mStudyResultNo, STATUS, audioTime, ServiceCommon.STATUS_START, playingTime, CommonUtil.getCurrentDateTime2(), 0, aCnt, aTime, mCnt, mTime, mRtime);
        //**slog end

        // requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);

        // StudyDataUtil.setCurrentStudyStatus(this, "S31");
        // mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
        // ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        isQPOR = false;
        super.onStart();
        startTime = SystemClock.uptimeMillis();
    }


    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /*
         * if (!mIsStudyStart) return false;
         */

        // keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer1 && mAnswer1.isEnabled()) {
                        mAnswer1.setPressed(true);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer2 && mAnswer2.isEnabled())
                        mAnswer2.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer3 && mAnswer3.isEnabled()) {
                        mAnswer3.setPressed(true);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer4 && mAnswer4.isEnabled())
                        mAnswer4.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.sec_test_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(true);
                } else if (View.VISIBLE == mNextStatus.getVisibility()) {

                    mNextStatus.setPressed(true);
                } else if (View.VISIBLE == sec_timer_main_layout.getVisibility()) {
                    btn_timer_start.setPressed(true);
                }
                break;

            default:
                return false;
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        /*
         * if (!mIsStudyStart) return false;
         */

        // keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer1 && mAnswer1.isEnabled()) {
                        mAnswer1.setPressed(false);
                        selectResult(ServiceCommon.BUTTONTAG_ANSWER_1);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer2 && mAnswer2.isEnabled()) {
                        mAnswer2.setPressed(false);
                        selectResult(ServiceCommon.BUTTONTAG_ANSWER_2);
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer3 && mAnswer3.isEnabled()) {
                        mAnswer3.setPressed(false);
                        selectResult(ServiceCommon.BUTTONTAG_ANSWER_3);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (!mTesting && null != mAnswer4 && mAnswer4.isEnabled()) {
                        mAnswer4.setPressed(false);
                        selectResult(ServiceCommon.BUTTONTAG_ANSWER_4);
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.sec_test_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(false);
                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        // goNextStatus();
                    } else {
                        requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
                    }
                } else if (View.VISIBLE == mNextStatus.getVisibility()) {

                    mNextStatus.setPressed(false);

                    requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);

                }

                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;

    }

    @Override
    public void onClick(View v) {
        /*
         * if (!mIsStudyStart) return;
         */

        if (singleProcessChecker())
            return;

        switch (v.getId()) {
            case R.id.word_study_list_answer_1:
            case R.id.word_study_list_answer_2:
            case R.id.word_study_list_answer_3:
            case R.id.word_study_list_answer_4:
            case R.id.word_study_answer_1:
            case R.id.word_study_answer_2:
            case R.id.word_study_answer_3:
            case R.id.word_study_answer_4:
                int answer = (Integer) v.getTag();
                selectResult(answer);
                break;

            case R.id.word_study_result_confirm_btn:
            case R.id.word_study_wrong_next_status_btn:
                actFinish();
                break;
            case R.id.layout_speaker:
                playerEnd();
                String path = null;
                try {
                    path = WeekStudyData.mOneWeekData.get(mCurStudyIndex).audio_path2;
                } catch (IndexOutOfBoundsException e) {//넘을경우 예외처리
                    e.printStackTrace();
                    if (mCurStudyIndex > 0) {
                        mCurStudyIndex = WeekStudyData.mOneWeekData.size() - 1;
                    }
                    path = WeekStudyData.mOneWeekData.get(mCurStudyIndex).audio_path2;
                }

                setPlayer(path);
                mMediaPlayer.start();
                mCurPlayCount = 1;
                break;

            default:
                super.onClick(v);
        }

    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        if (CommonUtil.isCenter()) { // Igse
            mTimerBuble = getResources().getDrawable(R.drawable.igse_img_learning_timer_on);
            answer_on = ContextCompat.getDrawable(getApplicationContext(), R.drawable.igse_btn_learning_answer_bg_on);
            answer_solid = ContextCompat.getDrawable(getApplicationContext(), R.drawable.igse_btn_learning_answer_solid);
            btn_learning_answer_num_on = R.drawable.igse_btn_learning_answer_num_on;
            btn_learning_answer_num_off = R.drawable.igse_btn_learning_answer_num_off;
            text_color = R.color.color5;
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) { //우영
                mTimerBuble = getResources().getDrawable(R.drawable.w_img_learning_timer_on);
                answer_on = ContextCompat.getDrawable(getApplicationContext(), R.drawable.w_btn_learning_answer_bg_on);
                answer_solid = ContextCompat.getDrawable(getApplicationContext(), R.drawable.w_btn_learning_answer_solid);
                btn_learning_answer_num_on = R.drawable.w_btn_learning_answer_num_on;
                btn_learning_answer_num_off = R.drawable.w_btn_learning_answer_num_off;
                text_color = R.color.color3;
            } else {// 숲
                mTimerBuble = getResources().getDrawable(R.drawable.f_img_learning_timer_on);
                answer_on = ContextCompat.getDrawable(getApplicationContext(), R.drawable.f_btn_learning_answer_bg_on);
                answer_solid = ContextCompat.getDrawable(getApplicationContext(), R.drawable.f_btn_learning_answer_solid);
                btn_learning_answer_num_on = R.drawable.f_btn_learning_answer_num_on;
                btn_learning_answer_num_off = R.drawable.f_btn_learning_answer_num_off;
                text_color = R.color.color1;
            }
        }
        mTimerBubleDis = getResources().getDrawable(R.drawable.c_img_learning_timer_off);

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        layout_speaker = (LinearLayout) findViewById(R.id.layout_speaker);
        sec_test_result_layout = (LinearLayout) findViewById(R.id.sec_test_result_layout);
        sec_timer_main_layout = (LinearLayout) findViewById(R.id.sec_timer_main_layout);
        sec_test_study_layout = (LinearLayout) findViewById(R.id.sec_test_study_layout);
        mStudyView = (TextView) findViewById(R.id.word_study_question_text);
        mStudyCountView = (TextView) findViewById(R.id.word_study_question_count_text);

        mResultView = (TextView) findViewById(R.id.word_study_result_wrong_count_text);
        mResultView2 = (TextView) findViewById(R.id.word_study_result_wrong_count_text2);
        mResultTime = (TextView) findViewById(R.id.oneweek_test_time_text);
        mResultConfirm = (Button) findViewById(R.id.word_study_result_confirm_btn);
        btn_timer_start = (Button) findViewById(R.id.btn_timer_start);

        mWrongStudyView = (TextView) findViewById(R.id.word_study_wrong_question_text);
        mWrongStudyCountView = (TextView) findViewById(R.id.word_study_wrong_question_count_text);
        mWrongStudyAnswerView = (TextView) findViewById(R.id.word_study_wrong_correct_answer_text);
        mNextStatus = (Button) findViewById(R.id.word_study_wrong_next_status_btn);
        //mTitleBar = (Indicator) findViewById(R.id.ganji_titlebar);
        //setTitlebarCategory(getString(R.string.string_titlebar_category_test));
        //mTitleBar.setTitle(WeekStudyData.mProductName);

        img_result = (ImageView) findViewById(R.id.img_result);

        //staus
        //sec_setp_statusbar = (LinearLayout) findViewById(R.id.sec_setp_statusbar);
        //sec_setp_statusbar.setVisibility(View.GONE);
        //mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);
        //mStepStatusBar.setVisibility(View.VISIBLE);
        //mStepStatusBar.setHighlights(StepStatusBar.STATUS_ONE_WEEK_TEST);

        mResultConfirm.setOnClickListener(this);
        mNextStatus.setOnClickListener(this);
        btn_timer_start.setOnClickListener(this);
        layout_speaker.setOnClickListener(this);

        mTimerViewList = new ArrayList<ImageView>();
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_1));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_2));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_3));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_4));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_5));

        mTotalStudyCount = WeekStudyData.mOneWeekData.size();

        sec_test_study_layout.setVisibility(View.VISIBLE);
        sec_timer_main_layout.setVisibility(View.GONE);

        findViewById(R.id.layout_timer).setVisibility(View.INVISIBLE);
        findViewById(R.id.layout_timer2).setVisibility(View.INVISIBLE);
        findViewById(R.id.txt_small_timer).setVisibility(View.INVISIBLE);

    }

    /**
     * Player를 설정함
     *
     * @param mp3FilePath 재생 파일 경로
     */
    private void setPlayer(String mp3FilePath) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            if (!mp3FilePath.equals("")) {
                mMediaPlayer.setDataSource(mp3FilePath);
            } else {
                AssetFileDescriptor afd = getAssets().openFd("mute.mp3");
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            }
            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.prepare();
            // 단어가 한글일때
            if ((CommonUtil.isKorean(WeekStudyData.mOneWeekData.get(mCurStudyIndex).sentence))) {
                mMediaPlayer.setVolume(0, 0);
                // part2의 t2일때
            } else if (WeekStudyData.mOneWeekData.get(mCurStudyIndex).question_type
                    .equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_2)) {
                layout_speaker.setVisibility(View.VISIBLE);
                mStudyView.setVisibility(View.GONE);
            } else if (WeekStudyData.mOneWeekData.get(mCurStudyIndex).question_type
                    .equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_1)) {
                mMediaPlayer.setVolume(0, 0);
                layout_speaker.setVisibility(View.GONE);
                mStudyView.setVisibility(View.VISIBLE);
            } else {
                layout_speaker.setVisibility(View.GONE);
                mStudyView.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // --- 단어 시험 --

    /**
     * 단어 문제 전환용 Flip Animation Listener
     */
    private AnimationListener studyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            setStudy(false);
            startAnswerAni(mAnswer1);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 단어 문제의 보기 항목 전환용 Flip Animation Listener
     */
    private AnimationListener answerAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            nextAnswerAni();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 단어 학습 문제 전환용 애니메이션을 시작함
     */
    private void startStudyAni() {
        Animation ani = new FlipAnimation(180f, 0f, mStudyView.getWidth() / 2, mStudyView.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(studyAniListener);
        mStudyView.startAnimation(ani);
    }

    /**
     * 단어 학습 문제의 보기 항목 전환용 애니메이션을 시작함
     *
     * @param view 보기 항목 View
     */
    private void startAnswerAni(LinearLayout view) {
        Animation ani = new FlipAnimation(180f, 0f, mAnswer1.getWidth() / 2, mAnswer1.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(answerAniListener);
        view.startAnimation(ani);
    }

    /**
     * 다음 문제의 보기 항목을 애니메이션과 함께 표시함 모든 보기 항목의 애니메이션이 끝난 경우 해당 문제를 재생함
     */
    private void nextAnswerAni() {
        boolean isListType = isListAnswer();
        mCurAnswerAniCount++;

        if (mCurAnswerAniCount == 2) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer1_text))
                        .setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).ex_1);
            else
                ((TextView) findViewById(R.id.word_study_answer1_text))
                        .setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).ex_1);

            startAnswerAni(mAnswer2);
        } else if (mCurAnswerAniCount == 3) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer2_text))
                        .setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).ex_2);
            else
                ((TextView) findViewById(R.id.word_study_answer2_text))
                        .setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).ex_2);

            startAnswerAni(mAnswer3);
        } else if (mCurAnswerAniCount == 4) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer3_text))
                        .setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).ex_3);
            else
                ((TextView) findViewById(R.id.word_study_answer3_text))
                        .setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).ex_3);

            if (mAnswer4.getVisibility() == View.VISIBLE) {
                startAnswerAni(mAnswer4);
            }
        } else {
            /*
             * if (isListType) ((TextView)
             * findViewById(R.id.word_study_list_answer4_text)).setText(
             * WeekStudyData.mOneWeekData.get(mCurStudyIndex).getEx_4());
             *
             * else { ((TextView)
             * findViewById(R.id.word_study_answer4_text)).setText(WeekStudyData
             * .mOneWeekData.get(mCurStudyIndex).getEx_4()); }
             */
            mAnswer1.setEnabled(true);
            mAnswer2.setEnabled(true);
            mAnswer3.setEnabled(true);
            mAnswer4.setEnabled(true);

            mCurPlayCount = 1;
            mCurAnswerAniCount = 1;
            mIsTimerReady = true;
            if (mMediaPlayer != null)
                mMediaPlayer.start();
        }
    }

    /**
     * 단어 학습을 시작함 첫번재 문제는 애니메이션 없이 표시함
     *
     * @param filePath 재생 파일 경로
     */
    private void startStudy(String filePath) {
        mStudyView.setText("");

        initAnswers();

        setPlayer(filePath);

		/*	if (0 < mCurStudyIndex) {
				if (WeekStudyData.mIsCaption) {
					startStudyAni();
				} else {
					setStudy(false);
					startAnswerAni(mAnswer1);
				}
			} else {*/
        setStudy(true);
        mCurPlayCount = 1;
        mIsTimerReady = true;
        mMediaPlayer.start();
        if (!mIsStudyStart) {
            mIsStudyStart = true;
            //}
        }
    }

    /**
     * 단어 문제를 설정함
     *
     * @param isShowAnswer 보기 설정 여부
     */
    private void setStudy(boolean isShowAnswer) {

        //상하모드 제거했음(한글일때 있고,I/Am/boy 이런 문제가 있음)
        mStudyView.setText(WeekStudyData.mOneWeekData.get(mCurStudyIndex).statement.replaceAll("`", "'"));

        mStudyCountView.setText(mCurStudyIndex + 1 + "/" + mTotalStudyCount);

        // 보기 설정
        // if (isShowAnswer)
        setAnswers();
    }

    /**
     * 보기 항목의 표시 유형을 판단하여 반환함
     *
     * @return 보기 항목의 표시 유형(Grid / List)
     */
    private boolean isListAnswer() {
        ArrayList<String> distractorList = WeekStudyData.mOneWeekData.get(mCurStudyIndex).getDistractorList();

        boolean isListType = false;
        for (int i = 0; i < distractorList.size(); i++) {
            if (ServiceCommon.MAX_WORD_LENGTH < distractorList.get(i).length())
                isListType = true;
        }

        return isListType;
    }

    /**
     * 보기 항목의 View를 초기화함
     */
    private void initAnswers() {
        boolean isListType = isListAnswer();
        findViewById(R.id.word_study_lattice_word_select_layout).setVisibility(isListType ? View.GONE : View.VISIBLE);
        findViewById(R.id.word_study_list_word_select_layout).setVisibility(isListType ? View.VISIBLE : View.GONE);

        //가로세로 분기
        if (isListType) {
            mAnswer1 = (LinearLayout) findViewById(R.id.word_study_list_answer_1);
            mAnswer2 = (LinearLayout) findViewById(R.id.word_study_list_answer_2);
            mAnswer3 = (LinearLayout) findViewById(R.id.word_study_list_answer_3);
            mAnswer4 = (LinearLayout) findViewById(R.id.word_study_list_answer_4);

            ((AutofitTextView) findViewById(R.id.word_study_list_answer1_text)).setText("");
            ((AutofitTextView) findViewById(R.id.word_study_list_answer2_text)).setText("");
            ((AutofitTextView) findViewById(R.id.word_study_list_answer3_text)).setText("");
            ((AutofitTextView) findViewById(R.id.word_study_list_answer4_text)).setText("");
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer1_text));
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer2_text));
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer3_text));
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer4_text));
        } else {
            mAnswer1 = (LinearLayout) findViewById(R.id.word_study_answer_1);
            mAnswer2 = (LinearLayout) findViewById(R.id.word_study_answer_2);
            mAnswer3 = (LinearLayout) findViewById(R.id.word_study_answer_3);
            mAnswer4 = (LinearLayout) findViewById(R.id.word_study_answer_4);

            ((TextView) findViewById(R.id.word_study_answer1_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer2_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer3_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer4_text)).setText("");
        }
        //정답 레이아웃 리스트에 등록
        mAnswerList = new ArrayList<LinearLayout>();
        mAnswerList.add(mAnswer1);
        mAnswerList.add(mAnswer2);
        mAnswerList.add(mAnswer3);
        mAnswerList.add(mAnswer4);

        mAnswer1.setOnClickListener(this);
        mAnswer2.setOnClickListener(this);
        mAnswer3.setOnClickListener(this);
        mAnswer4.setOnClickListener(this);

        mAnswer1.setTag(ServiceCommon.BUTTONTAG_ANSWER_1);
        mAnswer2.setTag(ServiceCommon.BUTTONTAG_ANSWER_2);
        mAnswer3.setTag(ServiceCommon.BUTTONTAG_ANSWER_3);
        mAnswer4.setTag(ServiceCommon.BUTTONTAG_ANSWER_4);

        mAnswer1.setEnabled(false);
        mAnswer2.setEnabled(false);
        mAnswer3.setEnabled(false);
        mAnswer4.setEnabled(false);
    }

    /**
     * 보기 항목를 설정함
     */
    private void setAnswers() {
        if (WeekStudyData.mOneWeekData.get(mCurStudyIndex).getDistractorList().isEmpty())
            return;

        ArrayList<String> distractorList = WeekStudyData.mOneWeekData.get(mCurStudyIndex).getDistractorList();

        if (isListAnswer()) {
            ((TextView) findViewById(R.id.word_study_list_answer1_text)).setText(distractorList.get(0));
            ((TextView) findViewById(R.id.word_study_list_answer2_text)).setText(distractorList.get(1));
            ((TextView) findViewById(R.id.word_study_list_answer3_text)).setText(distractorList.get(2));
            if (distractorList.size() != 3) {
                ((LinearLayout) findViewById(R.id.word_study_list_answer_4)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.word_study_list_answer4_text)).setText(distractorList.get(3));
                mAnswer4.setEnabled(true);
            } else {
                ((LinearLayout) findViewById(R.id.word_study_list_answer_4)).setVisibility(View.INVISIBLE);
                mAnswer4.setEnabled(false);
            }

        } else {
            ((TextView) findViewById(R.id.word_study_answer1_text)).setText(distractorList.get(0));
            ((TextView) findViewById(R.id.word_study_answer2_text)).setText(distractorList.get(1));
            ((TextView) findViewById(R.id.word_study_answer3_text)).setText(distractorList.get(2));

            if (distractorList.size() != 3) {
                mAnswer4.setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.word_study_answer4_text)).setText(distractorList.get(3));
                mAnswer4.setEnabled(true);
            } else {
                mAnswer4.setVisibility(View.INVISIBLE);
                mAnswer4.setEnabled(false);
            }
        }

        mAnswer1.setEnabled(true);
        mAnswer2.setEnabled(true);
        mAnswer3.setEnabled(true);

    }

    /**
     * 다음 문제를 시작함 마지막 문제 이후엔 단어 학습의 완료 메세지를 전달함
     */
    private void nextStudy() {
        mCurStudyIndex++;
        mTesting = false;
        mIsTimerReady = false;
        playerEnd();
        if (mCurStudyIndex < mTotalStudyCount) {
            resetTimer();
            startStudy(WeekStudyData.mOneWeekData.get(mCurStudyIndex).audio_path);
        } else {
            mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
                    ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, 0));
        }
    }

    /**
     * 단어 시험의 결과를 저장함
     *
     * @param answer 사용자의 선택 항목
     */
    private void setWordResult(int answer) {
        mTesting = true;
        if (mCurStudyIndex >= mTotalStudyCount)
            return;

        int correct = Integer.valueOf(WeekStudyData.mOneWeekData.get(mCurStudyIndex).correct);

        WeekStudyData.mOneWeekData.get(mCurStudyIndex).choose = String.valueOf(answer);
        WeekStudyData.mOneWeekData.get(mCurStudyIndex).test_result = (answer == correct);
        selectAni(answer, correct);
        RequestPhpPost reqQTLG = new RequestPhpPost(mContext);
        reqQTLG.sendQTLG(WeekStudyData.mBookDetailId, WeekStudyData.mOneWeekData.get(mCurStudyIndex).quizdw_no, answer,
                WeekStudyData.mStudyResultNo);

    }

    /**
     * 단어 시험 제한시간인 5초 타이머 Thread를 시작함
     */
    /*
     * private void runTimerThread() { Runnable runnable = null; runnable = new
     * Runnable() { public void run() { while (null != mTimerThread &&
     * !mTimerThread.isInterrupted()) { SystemClock.sleep(1000);
     * mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
     * ServiceCommon.MSG_STUDY_PROGRESS_UPDATE, 0)); } } }; mTimerThread = new
     * Thread(runnable); mTimerThread.start(); }
     *
     *//**
     * 5초 타이머 Thread를 종료함
     *//*
     * private void stopTimerThread() { if (null != mTimerThread) {
     * mTimerThread.interrupt(); mTimerThread = null; } }
     */

    /**
     * 5초 타이머를 Reset함
     */
    private void resetTimer() {
        for (int i = 0; i < mTimerViewList.size(); i++)
            mTimerViewList.get(i).setImageDrawable(mTimerBuble);
        mTimerIndex = 4;
    }

    /**
     * 1초 단위로 경과 표시를 함 5초 경과 후 자동으로 답안을 설정하고 다음 문제를 시작함
     */
    private void setTimer() {
        if (mIsTimerReady) {
            if (0 > mTimerIndex) {
                selectResult(0);
                return;
            }
            mTimerViewList.get(mTimerIndex).setImageDrawable(mTimerBubleDis);
            mTimerIndex--;
        }
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START) {
                        startStudy(WeekStudyData.mOneWeekData.get(0).audio_path);
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_UPDATE) {
                        setTimer();
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION) {
                        findViewById(R.id.title_view).setVisibility(View.GONE);
                        findViewById(R.id.ganji_titlebar).setVisibility(View.GONE);
                        sec_test_study_layout.setVisibility(View.GONE);
                        sec_test_result_layout.setVisibility(View.VISIBLE);

                        int worngCnt = worngCnt(WeekStudyData.mOneWeekData);
                        //ImageView chImage = (ImageView) findViewById(R.id.word_study_result_ch_img);

                        timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                        updatedTime = timeInMilliseconds;
                        int secs = (int) (updatedTime / 1000);
                        int mins = secs / 60;
                        secs = secs % 60;

                        if (0 < worngCnt) {

                            //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_03));

                            //int qSize = WeekStudyData.mOneWeekData.size();
                            //String result = +qSize + "문제 중 " + (qSize - worngCnt) + "문제를 맞게 풀었어요.\n";
                            //mResultView.setText(result);

                            mResultView.setText((new StringBuilder().append(WeekStudyData.mOneWeekData.size()).toString()));
                            mResultView2.setText((new StringBuilder().append(WeekStudyData.mOneWeekData.size() - worngCnt).append(getString(R.string.string_common_question))).toString());
                        } else {
                            // guidePlay(R.raw.b_15);
                            //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_01));

                            findViewById(R.id.word_study_result_wrong_text).setVisibility(View.GONE);
                            findViewById(R.id.word_study_result_wrong_text2).setVisibility(View.GONE);
                            mResultView.setText(getString(R.string.string_study_result_all_correct));
                        }

                        mResultTime.setText(" (소요 시간 : " + mins + "분  " + secs + "초)");

                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE) {
                        /*
                         * if (msg.arg2 == ServiceCommon.
                         * MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE)
                         * setWrongStudyAnswer(); else nextWrongStudy();
                         */
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void requestServerTimeSync(int type) {
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestServerTimeSync(mNetworkHandler, type);
    }

    private void readyStudy() {
        mHandler.sendMessageDelayed(
                mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "--- S31 -----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, OneWeekTestActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            readyStudy();
                            Log.k("wusi12", "--------------------");
                        }
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "-------S32-----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, OneWeekTestActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {

                            actFinish();
                            Log.k("wusi12", "--------------------");
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        readyStudy();
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        actFinish();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        // TODO Auto-generated method stub
        mMediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.pause();

        // 음성 2번 들려준 후 2초 후 정답 공개
        if (2 > mCurPlayCount && WeekStudyData.mOneWeekData.get(mCurStudyIndex).question_type.equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_2)) {
            playerEnd();
            setPlayer(WeekStudyData.mOneWeekData.get(mCurStudyIndex).audio_path2);
            mMediaPlayer.start();
            mCurPlayCount++;
        }

    }

    private int worngCnt(ArrayList<OneWeekData> list) {
        int result = 0;

        for (OneWeekData data : list) {
            if (!data.test_result)
                result++;
        }

        return result;

    }

    private void actFinish() {
        int stepDuration = (int) ((System.currentTimeMillis() - StartTime) / 1000);
        int playingTime = 0;
        int audioTime = 0;
        int aCnt = 0;
        int aTime = 0;
        int mCnt = 0;
        int mTime = 0;
        int mRtime = 0;

        playingTime = WeekStudyData.mAudioStudyTime / 1000;
        audioTime = WeekStudyData.mAudioTotalMin / 1000;
        aCnt = (WeekStudyData.mAudioRecordIndex - WeekStudyData.mRecMCnt) < 0 ? 0 : (WeekStudyData.mAudioRecordIndex - WeekStudyData.mRecMCnt);
        aTime = (WeekStudyData.mTotalRecTime - WeekStudyData.mRecMTime) < 0 ? 0 : (WeekStudyData.mTotalRecTime - WeekStudyData.mRecMTime);
        mCnt = WeekStudyData.mRecMCnt;
        mTime = WeekStudyData.mRecMTime;
        mRtime = WeekStudyData.mRecRTime * WeekStudyData.mRTagRepeatCnt;

        RequestPhpPost reqSlog = new RequestPhpPost(mContext);
        reqSlog.sendSLOG(WeekStudyData.mProductNo, WeekStudyData.mBookDetailId, WeekStudyData.mStudyResultNo, STATUS, audioTime, ServiceCommon.STATUS_END, playingTime, CommonUtil.getCurrentDateTime2(), stepDuration, aCnt, aTime, mCnt, mTime, mRtime);
        try {
            startActivity(new Intent(mContext, StudyOutcomeActivity.class));
        } catch (NullPointerException e) {//mcontext가 널인경우가 있음
            e.printStackTrace();
            startActivity(new Intent(this, StudyOutcomeActivity.class));
        }
        finish();
    }

    /**
     * 선택 에니메이션 때문에 모아둠
     *
     * @param result
     */
    private void selectResult(int result) {
        if (!mTesting)
            setWordResult(result);

    }

    /**
     * 선택했을때 부터 에니메이션
     *
     * @param answer
     * @param correct
     */
    private void selectAni(int answer, final int correct) {
        //결과값
        final boolean result = answer == correct;

        //누른거 고정
        //setBtnControl(KIND_ENABLED, answer);
        setBtnControl(KIND_PRESSED, answer);

        //지연
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //초기화
                //결과 따라 이미지 변경
                Drawable drawable = null;
                if (result) {
                    drawable = getResources().getDrawable(R.drawable.c_img_learning_answer_correct);
                } else {
                    drawable = getResources().getDrawable(R.drawable.c_img_learning_answer_incorrect);
                }
                img_result.setVisibility(View.VISIBLE);
                img_result.setAdjustViewBounds(true);
                img_result.setBackground(drawable);

                /*//결과 애니메이션 시작(o,x)
                AnimationDrawable aniDrawable = (AnimationDrawable) img_result.getBackground();
                aniDrawable.start();*/

                setBtnAllControl(KIND_PRESSED, false);//전체 취소
                setBtnAllControl(KIND_ENABLED, false);//전체 취소

                setBtnControl(KIND_SELECTED, correct);//정답표시
                setBtnControl(KIND_ENABLED, correct);//아닌거 disable표시
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //초기화
                        setBtnAllControl(KIND_ACTIVED, false);
                        setBtnAllControl(KIND_ENABLED, false);
                        setBtnAllControl(KIND_PRESSED, false);
                        setBtnAllControl(KIND_SELECTED, false);
                        img_result.setVisibility(View.INVISIBLE);
                        nextStudy();
                    }
                }, 2000);
            }
        }, 1100);

    }

    /**
     * 버튼 상태 변경
     *
     * @param kind   KIND_ACTIVED,KIND_PRESS,KIND_SELECTED
     * @param number 결과값 해당값이 아니면 다 false
     */
    private void setBtnControl(String kind, int number) {
        int i = 1;
        correct_idx = number;
        for (LinearLayout btn : mAnswerList) {
            if (kind.equals(KIND_ACTIVED)) {
                btn.setActivated(i == number);
            } else if (kind.equals(KIND_PRESSED)) {
                btn.setPressed(i == number);
                if (View.VISIBLE == findViewById(R.id.word_study_lattice_word_select_layout).getVisibility()) {
                    ((LinearLayout) findViewById(short_linear[number - 1])).setBackground(answer_on);
                    ((TextView) findViewById(short_answer[number - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_on));
                } else {
                    ((LinearLayout) findViewById(long_linear[number - 1])).setBackground(answer_on);
                    ((TextView) findViewById(long_answer[number - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_on));
                }
            } else if (kind.equals(KIND_SELECTED)) {
                btn.setSelected(i == number);
                if (View.VISIBLE == findViewById(R.id.word_study_lattice_word_select_layout).getVisibility()) {
                    ((TextView) findViewById(short_answer[number - 1])).setBackground(getResources().getDrawable(R.drawable.c_img_learning_answer_correct_bg));
                    ((TextView) findViewById(short_answer[number - 1])).setText(getResources().getString(R.string.string_answer));

                    ViewGroup.LayoutParams params = ((TextView) findViewById(short_answer[number - 1])).getLayoutParams();
                    if (params != null) {
                        params.width = CommonUtil.convertDp(45, mContext);
                        ((TextView) findViewById(short_answer[number - 1])).setLayoutParams(params);
                    }

                    ((TextView) findViewById(short_answer_text[number - 1])).setTextColor(getResources().getColor(R.color.color_fsb_bg));
                    ((TextView) findViewById(short_answer[number - 1])).setTextColor(getResources().getColor(text_color));
                    ((LinearLayout) findViewById(short_linear[number - 1])).setBackground(answer_solid);
                } else {
                    ((TextView) findViewById(long_answer[number - 1])).setBackground(getResources().getDrawable(R.drawable.c_img_learning_answer_correct_bg));
                    ((TextView) findViewById(long_answer[number - 1])).setText(getResources().getString(R.string.string_answer));

                    ViewGroup.LayoutParams params = ((TextView) findViewById(long_answer[number - 1])).getLayoutParams();
                    if (params != null) {
                        params.width = CommonUtil.convertDp(45, mContext);
                        ((TextView) findViewById(long_answer[number - 1])).setLayoutParams(params);
                    }

                    ((TextView) findViewById(long_answer[number - 1])).setTextColor(getResources().getColor(text_color));
                    ((LinearLayout) findViewById(long_linear[number - 1])).setBackground(answer_solid);
                    ((TextView) findViewById(long_answer_text[number - 1])).setTextColor(getResources().getColor(R.color.color_fsb_bg));
                }

            } else if (kind.equals(KIND_ENABLED)) {
                btn.setEnabled(i == number);
            }
            i++;
        }

    }

    /**
     * 버튼 상태 변경(전체)
     *
     * @param kind KIND_ACTIVED,KIND_PRESS,KIND_SELECTED
     * @param bol  전체 true 전체 false
     */
    private void setBtnAllControl(String kind, boolean bol) {
        int i = 1;

        for (LinearLayout btn : mAnswerList) {
            if (kind.equals(KIND_ACTIVED)) {
                btn.setActivated(bol);
            } else if (kind.equals(KIND_PRESSED)) {
                btn.setPressed(bol);
            } else if (kind.equals(KIND_SELECTED)) {
                btn.setSelected(bol);
            } else if (kind.equals(KIND_ENABLED)) {
                btn.setEnabled(bol);
            }
            if (correct_idx != 0) {
                if (View.VISIBLE == findViewById(R.id.word_study_lattice_word_select_layout).getVisibility()) {
                    ((TextView) findViewById(short_answer[correct_idx - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_off));
                    ((TextView) findViewById(short_answer[correct_idx - 1])).setText(String.valueOf(correct_idx));

                    ViewGroup.LayoutParams params = ((TextView) findViewById(short_answer[correct_idx - 1])).getLayoutParams();
                    if (params != null) {
                        params.width = CommonUtil.convertDp(33, mContext);
                        ((TextView) findViewById(short_answer[correct_idx - 1])).setLayoutParams(params);
                    }

                    ((TextView) findViewById(short_answer_text[correct_idx - 1])).setTextColor(getResources().getColorStateList(R.color.c_selector_word_answer_text));
                    ((TextView) findViewById(short_answer[correct_idx - 1])).setTextColor(getResources().getColor(R.color.color_white));
                    ((LinearLayout) findViewById(short_linear[correct_idx - 1])).setBackground(getResources().getDrawable(R.drawable.c_btn_learning_answer_bg_off));
                } else {
                    ((TextView) findViewById(long_answer[correct_idx - 1])).setBackground(getResources().getDrawable(btn_learning_answer_num_off));
                    ((TextView) findViewById(long_answer[correct_idx - 1])).setText(String.valueOf(correct_idx));

                    ViewGroup.LayoutParams params = ((TextView) findViewById(long_answer[correct_idx - 1])).getLayoutParams();
                    if (params != null) {
                        params.width = CommonUtil.convertDp(33, mContext);
                        ((TextView) findViewById(long_answer[correct_idx - 1])).setLayoutParams(params);
                    }

                    ((TextView) findViewById(long_answer[correct_idx - 1])).setTextColor(getResources().getColor(R.color.color_white));
                    ((LinearLayout) findViewById(long_linear[correct_idx - 1])).setBackground(getResources().getDrawable(R.drawable.c_btn_learning_answer_bg_off));
                    ((TextView) findViewById(long_answer_text[correct_idx - 1])).setTextColor(getResources().getColorStateList(R.color.c_selector_word_answer_text));
                }
            }
            i++;
        }
    }
}
