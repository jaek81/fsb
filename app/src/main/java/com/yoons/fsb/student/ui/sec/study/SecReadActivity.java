
package com.yoons.fsb.student.ui.sec.study;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomWavRecoder;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.sec.base.SecBaseStudyActivity;
import com.yoons.fsb.student.ui.textview.AutofitHelper;
import com.yoons.fsb.student.ui.textview.AutofitTextView;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

/**
 * 문장시험 화면
 * 2차 학습 단어/구 읽기
 * @author jaek
 */
public class SecReadActivity extends SecBaseStudyActivity implements OnClickListener, OnPreparedListener {

    private RelativeLayout mParentLayout = null;
    private ImageView mListen = null, mRecord = null, mRecordPlay = null;
    private TextView mStudyCountView = null;
    private AutofitTextView txt_mean = null, txt_word;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;
    private LoadingDialog mLoadingDialog = null;
    private Button mNextStatus = null;
    private LinearLayout layout_one, layout_read;
    private int vipcnt = 0;//vip만 따로 카운팅

    private int status = 0;

    /**
     * 2014-02-14 WCPM 적용 지역 변수 였으나 원어민 파일의 재생 시간이 필요 하여 멤버 변수로 선언
     */
    int playDuration = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_sec_read);
        else // 숲
            setContentView(R.layout.f_sec_read);

        mRecordFilePath = ServiceCommon.CONTENTS_PATH + ServiceCommon.WRONG_SENTENCE_RECORD_FILE_NAME + ServiceCommon.WAV_FILE_TAIL;
        mCurStudyIndex = mStudyData.getQuiz_seq(); //중도학습
        if ((mStudyData.getPartList().size() - 1) <= mCurStudyIndex) {
            mCurStudyIndex = 0;
        }
        setWidget();

        status = getIntent().getIntExtra(ServiceCommon.TARGET_ACTIVITY, 0);

        tCategory.setText("스마트 훈련");
        if (status == SecStudyData.TP_TYPE_SEC_WORD_READ)
            tCategory2.setText(R.string.string_common_sec_word_read);
        else if (status == SecStudyData.TP_TYPE_SEC_SENTENCE_READ)
            tCategory2.setText(R.string.string_ga_SEC_SENTENCE_READ);

        // TitleView
        //tBookName.setText(mStudyData.mProductName);

        setPreferencesCallback();

        final Handler handler = new Handler();
        handler.postDelayed(new

                                    Runnable() {
                                        @Override
                                        public void run() {
                                            startStudy();
                                        }
                                    }, 500);

        Crashlytics.log(

                getString(R.string.string_ga_SEC_READ));
    }

    @Override
    public void onClick(View v) {
        if (!mIsStudyStart)
            return;

        if (singleProcessChecker())
            return;

        int id = v.getId();

        switch (id) {
            case R.id.sentence_study_record_btn:
            case R.id.sentence_study_wrong_record_btn:
                if (mIsRecording)
                    recordEnd();
                else
                    recordReady(20000);
                break;

            case R.id.sentence_study_result_confirm_btn:
            case R.id.sentence_study_wrong_next_status_btn:
            case R.id.next_status_btn:
                next();
                finish();
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        layout_one = (LinearLayout) findViewById(R.id.layout_one_btn);
        layout_read = (LinearLayout) findViewById(R.id.sec_read_player_control_layout);

        mParentLayout = (RelativeLayout) findViewById(R.id.sentence_exam_parent_layout);
        txt_word = (AutofitTextView) findViewById(R.id.txt_word);
        txt_mean = (AutofitTextView) findViewById(R.id.txt_mean);
        mStudyCountView = (TextView) findViewById(R.id.sentence_study_question_count_text);
        mListen = (ImageView) findViewById(R.id.sentence_study_listen_btn);
        mRecord = (ImageView) findViewById(R.id.sentence_study_record_btn);
        mRecordPlay = (ImageView) findViewById(R.id.sentence_study_recordplay_btn);

        mNextStatus = (Button) findViewById(R.id.next_status_btn);

        mRecord.setOnClickListener(this);
        mNextStatus.setOnClickListener(this);

        mListen.setEnabled(false);
        mRecord.setEnabled(false);
        mRecordPlay.setEnabled(false);
        AutofitHelper.create(txt_word);
        AutofitHelper.create(txt_mean);

        mTotalStudyCount = mStudyData.getPartReadCnt();

        //setTitlebarCategory(getString(R.string.string_titlebar_category_study));

        findViewById(R.id.sentence_study_highlevel_question_layout).setVisibility(mStudyData.is_caption() ? View.GONE : View.VISIBLE);
        findViewById(R.id.sentence_study_normallevel_question_layout).setVisibility(mStudyData.is_caption() ? View.VISIBLE : View.GONE);

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);
        tooltip.setVisibility(View.INVISIBLE);
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));
        tipText.setText(getResources().getString(R.string.string_record_tool_tip));
        mParentLayout.addView(tooltip);
    }

    /**
     * Recorder를 설정함
     *
     * @param wavFilePath 녹음 파일 저장 경로
     */
    private void setRecorder(String wavFilePath) {
        mWavRecorder = new CustomWavRecoder(wavFilePath);
    }

    /**
     * 3 Player를 설정함
     *
     * @param mp3FilePath 재생 파일 경로
     */
    private void setPlayer(String mp3FilePath) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mp3FilePath);
            //mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));
                }
            });
            mMediaPlayer.prepare();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 5 Player를 Resume함
     */
    private void playerResume() {

       /* if (CommonUtil.isCenter()) {// igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen_n));
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record_d));
        } else if ("1".equals(Preferences.getLmsStatus(this))) {// 우영
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen_n));
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record_d));
        } else {// 숲
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen_n));
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record_d));
        }*/
        //mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_red_btn_bg));
        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));
        mMediaPlayer.start();

        if (CommonUtil.isCenter()) // igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen2));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen2));
            else // 숲
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen2));
        }

        listenIngEffect();
    }

    private void listenIngEffect() {
        int duration = mMediaPlayer.getDuration();
        AnimationDrawable ad = null;
        final LinearLayout parent = ((LinearLayout) mListen.getParent());
        if (CommonUtil.isCenter()) // igse
            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.w_seq_learning_progress);
            else // 숲
                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
        }
        final AnimationDrawable ing = new AnimationDrawable();
        final int frameTime = duration / ad.getNumberOfFrames();

        for (int i = 0; i < ad.getNumberOfFrames(); i++) {
            Drawable draw = ad.getFrame(i);
            ing.addFrame(draw, frameTime);
        }
        ing.setOneShot(true);
        parent.setBackground(ing);
        ((AnimationDrawable) parent.getBackground()).start();
    }

    /**
     * Headset 버튼의 리소스를 변경함
     */
    private void playerPauseAndStop() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);
        /*if (CommonUtil.isCenter()) // igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen_d));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen_d));
            else // 숲
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen_d));
        }*/
        //mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_btn_bg));
        if (CommonUtil.isCenter()) // igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen));
            else // 숲
                mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen));
        }
        LinearLayout parent = ((LinearLayout) mListen.getParent());
        try {
            AnimationDrawable aniDraw = (AnimationDrawable) parent.getBackground();
            aniDraw.stop();
            parent.setBackground(null);
            mListen.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        mMediaPlayer.stop();
        playerPauseAndStop();
        mMediaPlayer.release();
        mMediaPlayer = null;
    }

    /**
     * 2 학습을 시작함
     *
     * @param filePath 문장 파일 경로
     */
    private void startStudy(String filePath) {
        txt_mean.setText("");
        txt_word.setText("");

        setPlayer(filePath);

        setStudy();
        playerResume();
        if (!mIsStudyStart)
            mIsStudyStart = true;
    }

    /**
     * 4 문장 문제를 표시함
     */
    private void setStudy() {
        String word_sentence, mean;
        //word,mean 한글/영어일때 바꾸기
        word_sentence = mStudyData.getEngWordSentence(mCurStudyIndex);
        mean = mStudyData.getKorMeans(mCurStudyIndex);

		/*txt_mean.setTextSize(TypedValue.COMPLEX_UNIT_PX, ServiceCommon.MAX_SENTENCE_LENGTH < word_sentence.length() ? getResources().getDimensionPixelSize(R.dimen.dimen_22) : getResources().getDimensionPixelSize(R.dimen.dimen_34));
		txt_word.setTextSize(TypedValue.COMPLEX_UNIT_PX, ServiceCommon.MAX_SENTENCE_LENGTH < mean.length() ? getResources().getDimensionPixelSize(R.dimen.dimen_22) : getResources().getDimensionPixelSize(R.dimen.dimen_34));
		*/
        txt_word.setText(word_sentence);
        txt_mean.setText(mean);

        mStudyCountView.setText(((mCurStudyIndex + 1) - vipcnt) + "/" + mTotalStudyCount);
    }

    /**
     * 1 다음 문제를 설정함 마지막 문제 이후엔 음성 인식 채점을 요청함
     */
    private void startStudy() {
        //테스트 스킵
        if (mTestSkip) {
            mTestSkip = false;
            mCurStudyIndex = mStudyData.getPartList().size() - 1;
        }
        if (mCurStudyIndex < mStudyData.getPartList().size()) {
            if (mStudyData.getPartList().get(mCurStudyIndex).getVcsentence().length() < 2) {//vip가 아닐때
                startStudy(mStudyData.getPartList().get(mCurStudyIndex).getAudio_path());
            } else {//vip면 다시호출
                vipcnt++;
                mCurStudyIndex++;
                startStudy();
            }
        } else {
            layout_read.setVisibility(View.GONE);
            layout_one.setVisibility(View.VISIBLE);
            //findViewById(R.id.sentence_study_three_btn_parent_layout).setVisibility(View.GONE);
        }
    }

    /**
     * 학습 가이드를 표시함
     *
     * @param tip 학습 가이드 내용
     */
    private void showToolTip(String tip) {
		/*if (!mStudyData.mIsStudyGuide)
			return;*/

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);

        int x = 0, y = 0;
        LinearLayout tbpl = ((LinearLayout) findViewById(R.id.sentence_study_three_btn_parent_layout));
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));

        tipText.setText(tip);
        mParentLayout.addView(tooltip);

        int centerWidth = tbpl.getWidth() / 2;
        x = centerWidth - (mTooltipWidth / 2);

        y = mParentLayout.getHeight() - tbpl.getHeight() - (mTooltipHeight / 2);

        ViewGroup.MarginLayoutParams vm = (MarginLayoutParams) tooltip.getLayoutParams();
        vm.leftMargin = x;
        vm.topMargin = y;
    }

    /**
     * 학습 가이드를 지움
     */
    private void hideToolTip() {
        if (null != findViewById(R.id.tooltip_record_text))
            mParentLayout.removeView(findViewById(R.id.tooltip_record_text));
    }

//--- Recording && Write Animation effect ----

    /**
     * 녹음 진행 효과 시작 Runnable
     */
    private Runnable recordEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과 종료 Runnable
     */
    private Runnable recordEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과를 시작함
     */
    private void startIngEffect() {

      /*  if (CommonUtil.isCenter()) // Igse
            mRecord.setBackground(ContextCompat.getDrawable(this, R.drawable.igse_btn_learning_record_bg));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                mRecord.setBackground(ContextCompat.getDrawable(this, R.drawable.w_btn_learning_record_bg));
            else // 숲
                mRecord.setBackground(ContextCompat.getDrawable(this, R.drawable.f_btn_learning_record_bg));
        }
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_record_volume));

        mIngEffectThread = new Thread(recordEffectStartRunnable);
        mIngEffectThread.start();*/
        if (CommonUtil.isCenter()) // Igse
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_btn_learning_record_bg_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) //우영
                ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.w_btn_learning_record_bg_n));
            else // 숲
                ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_btn_learning_record_bg_n));
        }
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_record_volume));

        mIngEffectThread = new Thread(recordEffectStartRunnable);
        mIngEffectThread.start();
    }

    /**
     * 녹음 진행 효과를 종료함
     */
    private void stopIngEffect() {
        if (null != mIngEffectThread) {
            mIngEffectThread.interrupt();
            mIngEffectThread = null;
        }

        mIngEffectThread = new Thread(recordEffectStopRunnable);
        mIngEffectThread.start();
    }

    /**
     * 녹음 완료 효과를 1초간 진행함
     */
    private void completeEffect() {
        if (CommonUtil.isCenter()) { // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record2));
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_seq_learning_progress));
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) { // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record2));
                ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.w_seq_learning_progress));
            } else { // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record2));
                ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_seq_learning_progress));
            }
        }

        try {
            AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) mRecord.getParent()).getBackground();
            aniDraw.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Runnable runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                try {
                    AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) mRecord.getParent()).getBackground();
                    aniDraw.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    recordPlay();
                }
            }
        };
        ((LinearLayout) mRecord.getParent()).postDelayed(runnable, 1200);

       /* mRecord2.setVisibility(View.VISIBLE);
        if (CommonUtil.isCenter()) // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_seq_learning_progress));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_seq_learning_progress));
            else // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_seq_learning_progress));
        }

        mCompleteEffectThread = new Thread(recordEffectStartRunnable);
        mCompleteEffectThread.start();

        Runnable runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                if (null != mCompleteEffectThread) {
                    mCompleteEffectThread.interrupt();
                    mCompleteEffectThread = null;
                }

                try {
                    AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                    aniDraw.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (CommonUtil.isCenter()) {// igse
                        mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen_n));
                        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record_d));
                    } else if ("1".equals(Preferences.getLmsStatus(mContext))) {// 우영
                        mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen_n));
                        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record_d));
                    } else {// 숲
                        mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen_n));
                        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record_d));
                    }

                    //mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_red_btn_bg));
                    //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));
                    mRecord2.setVisibility(View.GONE);
                    mMediaPlayer.start();
                    //recordPlay();
                }
            }
        };
        mRecord.postDelayed(runnable, 1000); */
    }

//------------------- Recording----------------------

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordNotiCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            //mRecordPlay2.setVisibility(View.GONE);
            recordNotiEnd();
            recordStart();
            //Handler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));
        }
    };

    /**
     * 녹음 시작 알림음을 재생함
     */
    private void recordNotiPlay() {
        if (null != mRecordNotiPlayer)
            return;

        mRecordNotiPlayer = MediaPlayer.create(getBaseContext(), R.raw.ding);
        if (null != mRecordNotiPlayer) {
            mRecordNotiPlayer.setOnCompletionListener(mRecordNotiCompletion);
            mRecordNotiPlayer.start();
        }
    }

    /**
     * 녹음 시작 알림음을 종료함
     */
    private void recordNotiEnd() {
        if (null == mRecordNotiPlayer)
            return;

        if (mRecordNotiPlayer.isPlaying())
            mRecordNotiPlayer.stop();
        mRecordNotiPlayer.release();
        mRecordNotiPlayer = null;
    }

    /**
     * 버튼 비활성화 및 Recorder를 설정하고 녹음 시작 알림음을 재생시켜 녹음을 준비함
     */
    private void recordReady(int maxRecTime) {

        /*if (mIsRecording)
            return;

        if (CommonUtil.isCenter()) {// igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record_d));
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen_d));
        } else if ("1".equals(Preferences.getLmsStatus(this))) {// 우영
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record_d));
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen_d));
        } else {// 숲
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record_d));
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen_d));
        }

        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));
        //mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_btn_bg));

        mIsRecording = true;

        mRecord.setEnabled(false);

        String recordPath = "";
        recordPath = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path().trim();

        // AudioRecorderforWeb는 녹음파일경로에서 고정적인 .wav는 제외
        setRecorder(recordPath.replace(ServiceCommon.WAV_FILE_TAIL, ""));

        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);

        mMaxRecTime = maxRecTime;

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_READY, 0), 100);*/

        if (mIsRecording)
            return;

        mIsRecording = true;

        mRecord.setEnabled(false);

        String recordPath = "";
        recordPath = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path().trim();

        // AudioRecorderforWeb는 녹음파일경로에서 고정적인 .wav는 제외
        setRecorder(recordPath.replace(ServiceCommon.WAV_FILE_TAIL, ""));

        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);

        mMaxRecTime = maxRecTime;

        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_READY, 0), 100);
    }

    /**
     * 녹음을 시작하고 1초 뒤 녹음 완료를 진행 할 수 있도록 녹음 버튼을 활성화 함
     */
    private void recordStart() {

        if (CommonUtil.isCenter()) // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record));
            else // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));
        }

        mWavRecorder.startRecording();

        if (0 < mMaxRecTime)
            mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), mMaxRecTime);

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect();
                showToolTip(getResources().getString(R.string.string_record_tool_tip));
                mRecord.setEnabled(true);
            }
        };
        mRecord.postDelayed(runnable, 1000);

        /*if (CommonUtil.isCenter()) // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record_n));
            else // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record_n));
        }

        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_start));
        mWavRecorder.startRecording();

        if (0 < mMaxRecTime)
            mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), mMaxRecTime);

        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect();
                showToolTip(getResources().getString(R.string.string_record_tool_tip));
                mRecord.setEnabled(true);
            }
        };
        mRecord.postDelayed(runnable, 1000);*/
    }

    /**
     * 녹음을 종료함
     */
    private void recordEnd() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
        if (mIsRecording) {
            mRecord.setEnabled(false);
            mWavRecorder.stopRecording();
            mIsRecording = false;
            setRecordResult(mCurStudyIndex);
            stopIngEffect();
            hideToolTip();
            completeEffect();
        }
    }

    /**
     * 음성 인식 채점을 요청함
     */
	/*private void requestVoiceRecognize() {
		try {
			final int curindex = mCurStudyIndex;
			mVoiceScore = -99;
			mVoiceText = "";

			String fullRecordPath = mStudyData.getPartList().get(curindex).getRecord_path().trim();

			File recordFile = new File(fullRecordPath);
			// 녹음 파일이 없는 경우
			if (!recordFile.exists()) {
				setRecordResult(mVoiceText, mVoiceScore, curindex);
				return;
			}

			// 음성 평가 전 네트워크 상태 불가능의 경우
			if (!CommonUtil.isAvailableNetwork(this, false)) {
				setRecordResult(mVoiceText, mVoiceScore, curindex);
				return;
			}

			mIsRecoginition = true;
			String[] recordPathArray = fullRecordPath.split("/");
			String recordFileName = recordPathArray[recordPathArray.length - 1];
			final String recordPath = fullRecordPath.replace(recordFileName, "").trim();
			if (!recordFileName.contains(ServiceCommon.WAV_FILE_TAIL))
				recordFileName += ServiceCommon.WAV_FILE_TAIL;

			VRUTask = new VoiceRecognizerWeb(getApplicationContext());

			VRUTask.mstt = new OnMSTT() {

				@Override
				public void getSentenceObj(SentenceObj senObj) {
					if (senObj != null) {
						setRecordResult(senObj.getRec_sentence(), NumberUtil.Half6(senObj.getTotal_score()), curindex);
					}
				}

			};

			VRUTask.execute(recordFileName, recordPath, "16000", mStudyData.getEngWordSentence(curindex), String.valueOf(mStudyData.getMcode()),
					playDuration, 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    /**
     * 음성 인식 결과를 설정함
     */
    private void setRecordResult(int curindex) {

        try {
            int quiz_no = mStudyData.getPartList().get(curindex).getQuestionnumber();
            String rec_file = mStudyData.getPartList().get(curindex).getRecord_path().replace(".wav", ".mp3");
            //String rec_file = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path().replace(".wav", ".mp3");
            int study_type = mStudyData.getCurrent_part();

            RequestPhpPost reqQORE = new RequestPhpPost(mContext);

            reqQORE.sendQORE(mStudyData.getPartList().get(curindex).getBook_detail_id(), mStudyData.getPartList().get(curindex).getBook_detail_id(), mStudyData.getClass_second_id(), quiz_no, rec_file, study_type);
            //mStudyData.getPartList().get(curindex).setAnswers(score);
            //StudyDataUtil.addSentenceResult(text == null ? "" : text, score);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 에코를 재생함
     */
    private void recordPlay() {
        if (null != mRecordPlayer && mRecordPlayer.isPlaying())
            return;

        String dataSource = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path();

        ((LinearLayout) mRecord.getParent()).setBackground(null);
        if (CommonUtil.isCenter()) { // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat2));
        } else {
            if ("1".equals(Preferences.getLmsStatus(this))) { // 우영
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record));
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat2));
            } else { // 숲
                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat2));
            }
        }

        try {
            if (dataSource != null) {
                File f = new File(dataSource);
                if (f.exists()) {

                    mRecordPlayer = new MediaPlayer();
                    if (mRecordPlayer != null) {

                        int duration = 1200;

                        if (f.length() > 300) {
                            mRecordPlayer.setDataSource(this, Uri.fromFile(f));
                            mRecordPlayer.setOnCompletionListener(new OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    recordPlayEnd();
                                }
                            });
                            mRecordPlayer.setOnPreparedListener(this);
                            mRecordPlayer.prepare();
                            duration = mRecordPlayer.getDuration();
                        }

                        final LinearLayout parent = ((LinearLayout) mRecordPlay.getParent());
                        AnimationDrawable ad = null;
                        if (CommonUtil.isCenter()) { // igse
                            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
                        } else {
                            if ("1".equals(Preferences.getLmsStatus(this))) { // 우영
                                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.w_seq_learning_progress);
                            } else { // 숲
                                ad = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
                            }
                        }
                        final AnimationDrawable ing = new AnimationDrawable();
                        final int frameTime = duration / ad.getNumberOfFrames();
                        Log.i("", "duration => " + duration + " frame time => " + frameTime);
                        ing.setOneShot(true);
                        for (int i = 0; i < ad.getNumberOfFrames(); i++) {
                            Drawable draw = ad.getFrame(i);
                            ing.addFrame(draw, frameTime);
                        }
                        parent.setBackground(ing);
                        ((AnimationDrawable) parent.getBackground()).start();

                        if (f.length() <= 300) {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    recordPlayEnd();
                                }
                            }, duration);
                        }

                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*if (null != mRecordPlayer && mRecordPlayer.isPlaying())
            return;

        String dataSource = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path();

        if (CommonUtil.isCenter()) {// igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record_d));
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat_d));
        } else if ("1".equals(Preferences.getLmsStatus(this))) {// 우영
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record_d));
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat_d));
        } else {// 숲
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record_d));
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat_d));
        }

        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));
        //mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo_red_btn_bg));
        mRecordPlay2.setVisibility(View.VISIBLE);

        try {
            mRecordPlayer = new MediaPlayer();
            mRecordPlayer.reset();
            mRecordPlayer.setDataSource(dataSource);
            mRecordPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    recordPlayEnd();
                }
            });
            mRecordPlayer.setOnPreparedListener(this);
            mRecordPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
    //mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));

    /**
     * 에코 재생을 종료하고 에코 볼륨을 원복함
     */
    private void recordPlayEnd() {
        // 녹음 재생 후 종료

        if (null != mRecordPlayer) {
            mRecordPlayer.stop();
            mRecordPlayer.release();
            mRecordPlayer = null;

            if (CommonUtil.isCenter()) // igse
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat));
            else {
                if ("1".equals(Preferences.getLmsStatus(this))) // 우영
                    mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat));
                else // 숲
                    mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat));
            }

            //mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo_btn_bg));
            releaseEchoVolume();
        }

        final LinearLayout parent = (LinearLayout) mRecordPlay.getParent();

        try {
            AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) parent).getBackground();
            aniDraw.stop();
            parent.setBackground(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        File fileRecord = new File(mRecordFilePath);
        if (fileRecord.exists())
            fileRecord.delete();

        mCurStudyIndex++;
        startStudy();

		/*else
			nextWrongStudy();*/
    }

    /**
     * 에코 플레이어의 준비 완료시 에코 볼륨 설정 및 재생을 시작함
     */
    public void onPrepared(MediaPlayer mp) {
        if (null != mRecordPlayer) {
            setEchoVolume();
            mRecordPlayer.start();
        }
    }

    /**
     * 문장 학습 결과 화면을 표시함
     */
    private void setSentenceResult() {
        if (null != mLoadingDialog) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
        findViewById(R.id.sentence_study_result_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.sentence_study_ing_layout).setVisibility(View.GONE);
    }

    /**
     * 최대 녹음 시간(녹음 자동 종료 시간)을 계산하여 반환함
     *
     * @param baseMaxRecordTime 20초
     * @param playDuration      문장 재생 시간
     * @return 최대 녹음 시간
     */
    private int getMaxRecordTime(int baseMaxRecordTime, int playDuration) {
        Log.i("", "getMaxRecordTime playDuration => " + playDuration);

        int maxRecordTime = baseMaxRecordTime;
        int customMaxRecordTime = playDuration + (playDuration * 2);

        if (baseMaxRecordTime < customMaxRecordTime)
            maxRecordTime = playDuration * 2;
        else
            maxRecordTime = baseMaxRecordTime - playDuration;

        Log.i("", "getMaxRecordTime customMaxRecordTime => " + customMaxRecordTime + " maxRecordTime => " + maxRecordTime);

        return maxRecordTime;
    }

    /**
     * /** Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START)
                        startStudy(mStudyData.getPartList().get(0).getAudio_path());
                    else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION)
                        setSentenceResult();
                    break;

                case ServiceCommon.MSG_WHAT_PLAYER:
                    if (null == mMediaPlayer)
                        return;

                    if (msg.arg1 == ServiceCommon.MSG_PROGRESS_COMPLETION) {
                        //if (mCurPlayCount == 1) {
                        playDuration = mMediaPlayer.getDuration();
                        playerEnd();
                        recordReady(20000);
                        //mCurPlayCount++;
                        //}

                        /*else {//두번째 재생할때는 재생하고 다음학습준비
                            if (CommonUtil.isCenter()) // igse
                                mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
                            else {
                                if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record));
                                else // 숲
                                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));
                            }
                            //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));
                            playerEnd();
                            mCurPlayCount = 1;
                            mCurStudyIndex++;
                            if (CommonUtil.isCenter()) // igse
                                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat));
                            else {
                                if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                                    mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat));
                                else // 숲
                                    mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat));
                            }
                            //mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo_btn_bg));
                            startStudy();
                        }*/
                    }
                    break;

                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (null == mWavRecorder)
                        return;

                    if (msg.arg1 == ServiceCommon.MSG_REC_READY)
                        recordNotiPlay();
                    else if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END)
                        recordEnd();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void requestServerTimeSync(int type) {
        HttpJSONRequest request = new HttpJSONRequest(mContext);
        request.requestServerTimeSync(mNetworkHandler, type);
    }

    private void readyStudy() {
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SecReadActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            readyStudy();
                        }
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SecReadActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            next();
                            finish();
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        readyStudy();
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        next();
                        finish();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * 화면 구성 완료 후 호출됨 학습 가이드의 Width, Height 값을 저장함
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if (hasFocus) {
            View tooltip = findViewById(R.id.tooltip_record_text);
            if (null != tooltip) {
                mTooltipHeight = tooltip.getHeight();
                mTooltipWidth = tooltip.getWidth();
                mParentLayout.removeView(tooltip);
            }
        }
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == mRecord.getVisibility() && mIsRecording && mRecord.isEnabled()) {
                    mRecord.setPressed(true);
                } else if (View.VISIBLE == layout_one.getVisibility() && View.VISIBLE == mNextStatus.getVisibility())
                    mNextStatus.setPressed(true);
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;

    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!mIsStudyStart)
            return false;

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == mRecord.getVisibility() && mIsRecording && mRecord.isEnabled()) {
                    mRecord.setPressed(false);
                    recordEnd();
                } else if (View.VISIBLE == layout_one.getVisibility() && View.VISIBLE == mNextStatus.getVisibility()) {

                    mNextStatus.setPressed(false);
                    next();
                    finish();
                }
                break;

            default:
                return false;
        }

        return false;
    }

}
