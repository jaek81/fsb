package com.yoons.fsb.student.ui.sec.study;

import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.sec.base.SecBaseStudyActivity;
import com.yoons.fsb.student.ui.textview.AutofitHelper;
import com.yoons.fsb.student.ui.textview.AutofitTextView;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.FlipAnimation;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

/**
 * 단어시험 화면
 *
 * @author jaek
 */
public class SecTestActivity extends SecBaseStudyActivity implements OnClickListener, OnCompletionListener, OnSeekCompleteListener {

    private LinearLayout mAnswer1 = null, mAnswer2 = null, mAnswer3 = null, mAnswer4 = null, layout_speaker = null;
    private LinearLayout sec_timer_main_layout, sec_test_study_layout, sec_test_result_layout;
    private TextView mStudyCountView = null, mResultView = null, mResultView2 = null, mResultTime = null;
    private AutofitTextView mStudyView = null;
    private TextView mWrongStudyView = null, mWrongStudyCountView = null, mWrongStudyAnswerView = null;
    private Button mResultConfirm = null, mNextStatus = null, btn_timer_start = null;
    private ArrayList<ImageView> mTimerViewList = null;
    private Drawable mTimerBuble = null;
    private Drawable mTimerBubleDis = null;
    private boolean mWrongStudy = false;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;

    //타이머
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    private TextView txt_small_timer;
    private LinearLayout layout_timer2;

    private boolean isQlog = false;

    private int status = 0;

    //	private boolean mOnclick=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_sec_test_layout);
        else // 숲
            setContentView(R.layout.f_sec_test_layout);

        setWidget();
        mCurStudyIndex = mStudyData.getQuiz_seq();//중도학습

        status = getIntent().getIntExtra(ServiceCommon.TARGET_ACTIVITY, 0);

        tCategory.setText("스마트 훈련");
        if (status == SecStudyData.TP_TYPE_SEC_WORD_TEST)
            tCategory2.setText(R.string.string_common_sec_word_test);
        else if (status == SecStudyData.TP_TYPE_SEC_SENTENCE_TEST)
            tCategory2.setText(R.string.string_common_sec_enter_sentence_test);
        // TitleView
        //tBookName.setText(mStudyData.mProductName);

        setPreferencesCallback();

        Crashlytics.log(getString(R.string.string_ga_SEC_SENTENCE_TEST));

        // StudyDataUtil.setCurrentStudyStatus(this, "S31");
        // mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
        // ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        /* FlurryAgent.onStartSession(this, ServiceCommon.FLURRY_API_KEY); */
        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);

        sec_test_study_layout.setVisibility(View.VISIBLE);
        sec_timer_main_layout.setVisibility(View.GONE);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                readyStudy();
            }
        }, 200);
    }


    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		/*if (!mIsStudyStart)
			return false;*/

        //keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer1 && mAnswer1.isEnabled()) {
                        mAnswer1.setPressed(true);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer2 && mAnswer2.isEnabled())
                        mAnswer2.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer3 && mAnswer3.isEnabled()) {
                        mAnswer3.setPressed(true);
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer4 && mAnswer4.isEnabled())
                        mAnswer4.setPressed(true);
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.sec_test_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(true);
                } else if (View.VISIBLE == mNextStatus.getVisibility()) {

                    mNextStatus.setPressed(true);
                } else if (View.VISIBLE == sec_timer_main_layout.getVisibility()) {
                    btn_timer_start.setPressed(true);
                }
                break;

            default:
                return super.onKeyDown(keyCode, event);
        }

        return false;
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
		/*if (!mIsStudyStart)
			return false;*/

        //keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer1 && mAnswer1.isEnabled()) {
                        mAnswer1.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_1);
                        //nextStudy();
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer2 && mAnswer2.isEnabled()) {
                        mAnswer2.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_2);
                        //nextStudy();
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer3 && mAnswer3.isEnabled()) {
                        mAnswer3.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_3);
                        //nextStudy();
                        return true;
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                if (View.VISIBLE == findViewById(R.id.sec_test_study_layout).getVisibility()) {
                    if (null != mAnswer4 && mAnswer4.isEnabled()) {
                        mAnswer4.setPressed(false);
                        setWordResult(ServiceCommon.BUTTONTAG_ANSWER_4);
                        //nextStudy();
                    }
                }
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (View.VISIBLE == findViewById(R.id.sec_test_result_layout).getVisibility()) {
                    mResultConfirm.setPressed(false);
                    if (ServiceCommon.IS_CONTENTS_TEST) {
                        //goNextStatus();
                    } else {
                        next();
                        finish();
                    }
                } else if (View.VISIBLE == mNextStatus.getVisibility()) {

                    mNextStatus.setPressed(false);

                    next();
                    finish();

                } else if (View.VISIBLE == sec_timer_main_layout.getVisibility()) {
                    btn_timer_start.setPressed(false);
                    startTime = SystemClock.uptimeMillis();
                    customHandler.postDelayed(updateTimerThread, 0);
                    readyStudy();

                    sec_test_study_layout.setVisibility(View.VISIBLE);
                    sec_timer_main_layout.setVisibility(View.GONE);
                }
                break;

            default:
                return super.onKeyUp(keyCode, event);
        }

        return false;

    }

    @Override
    public void onClick(View v) {
		/*if (!mIsStudyStart)
			return;*/

        if (singleProcessChecker())
            return;

        switch (v.getId()) {
            case R.id.word_study_list_answer_1:
            case R.id.word_study_list_answer_2:
            case R.id.word_study_list_answer_3:
            case R.id.word_study_list_answer_4:
            case R.id.word_study_answer_1:
            case R.id.word_study_answer_2:
            case R.id.word_study_answer_3:
            case R.id.word_study_answer_4:
                int answer = (Integer) v.getTag();
                setWordResult(answer);
                //nextStudy();
                break;

            case R.id.word_study_result_confirm_btn:
            case R.id.word_study_wrong_next_status_btn:
                next();
                finish();
                break;

            case R.id.btn_timer_start:

                break;
            case R.id.layout_speaker:

                if (mMediaPlayer != null)
                    mMediaPlayer.start();
                mCurPlayCount = 1;
                break;
            default:
                super.onClick(v);
        }

    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        layout_speaker = (LinearLayout) findViewById(R.id.layout_speaker);
        layout_timer2 = (LinearLayout) findViewById(R.id.layout_timer2);
        sec_test_result_layout = (LinearLayout) findViewById(R.id.sec_test_result_layout);
        sec_timer_main_layout = (LinearLayout) findViewById(R.id.sec_timer_main_layout);
        sec_test_study_layout = (LinearLayout) findViewById(R.id.sec_test_study_layout);
        txt_small_timer = (TextView) findViewById(R.id.txt_small_timer);
        mStudyView = (AutofitTextView) findViewById(R.id.word_study_question_text);
        mStudyCountView = (TextView) findViewById(R.id.word_study_question_count_text);

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        if (CommonUtil.isCenter()) // igse
            mTimerBuble = getResources().getDrawable(R.drawable.igse_img_learning_timer_on);
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                mTimerBuble = getResources().getDrawable(R.drawable.w_img_learning_timer_on);
            else // 숲
                mTimerBuble = getResources().getDrawable(R.drawable.f_img_learning_timer_on);
        }
        mTimerBubleDis = getResources().getDrawable(R.drawable.c_img_learning_timer_off);

        //mTimerBuble = getResources().getDrawable(R.drawable.timer_buble);
        //mTimerBubleDis = getResources().getDrawable(R.drawable.timer_buble_dis);

        mResultView = (TextView) findViewById(R.id.word_study_result_wrong_count_text);
        mResultView2 = (TextView) findViewById(R.id.word_study_result_wrong_count_text2);
        mResultTime = (TextView) findViewById(R.id.oneweek_test_time_text);
        mResultConfirm = (Button) findViewById(R.id.word_study_result_confirm_btn);
        btn_timer_start = (Button) findViewById(R.id.btn_timer_start);

        mWrongStudyView = (TextView) findViewById(R.id.word_study_wrong_question_text);
        mWrongStudyCountView = (TextView) findViewById(R.id.word_study_wrong_question_count_text);
        mWrongStudyAnswerView = (TextView) findViewById(R.id.word_study_wrong_correct_answer_text);
        mNextStatus = (Button) findViewById(R.id.word_study_wrong_next_status_btn);

        mResultConfirm.setOnClickListener(this);
        mNextStatus.setOnClickListener(this);
        btn_timer_start.setOnClickListener(this);
        layout_speaker.setOnClickListener(this);

        AutofitHelper.create(mStudyView);

        mTimerViewList = new ArrayList<ImageView>();
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_1));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_2));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_3));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_4));
        mTimerViewList.add((ImageView) findViewById(R.id.word_study_timer_5));

        if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART2) {//파트2일때만제거
            for (ImageView imgView : mTimerViewList) {
                imgView.setVisibility(View.INVISIBLE);
            }
            layout_timer2.setVisibility(View.INVISIBLE);

        }
        mTotalStudyCount = mStudyData.getPartList().size();

    }

    /**
     * Player를 설정함
     *
     * @param mp3FilePath 재생 파일 경로
     */
    private void setPlayer(String mp3FilePath) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mp3FilePath);
            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.prepare();
            //단어가 한글일때
            String word = mStudyData.getPartList().get(mCurStudyIndex).getWord();
            //part1일때 한글,숫자일때는 소리 죽이기
            if ((CommonUtil.isKorean(word) && mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1) || (CommonUtil.isNum(word) && mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1)) {
                mMediaPlayer.setVolume(0, 0);
                //part2의 t2일때
            } else if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART2 && mStudyData.getPartList().get(mCurStudyIndex).getQuestion_type().equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_2)) {
                layout_speaker.setVisibility(View.VISIBLE);
                mStudyView.setVisibility(View.GONE);
            } else if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART2 && mStudyData.getPartList().get(mCurStudyIndex).getQuestion_type().equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_1)) {
                mMediaPlayer.setVolume(0, 0);
                layout_speaker.setVisibility(View.GONE);
                mStudyView.setVisibility(View.VISIBLE);
            } else {
                layout_speaker.setVisibility(View.GONE);
                mStudyView.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // --- 단어 시험 --

    /**
     * 단어 문제 전환용 Flip Animation Listener
     */
    private AnimationListener studyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            setStudy(false);
            startAnswerAni(mAnswer1);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 단어 문제의 보기 항목 전환용 Flip Animation Listener
     */
    private AnimationListener answerAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            nextAnswerAni();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 단어 학습 문제 전환용 애니메이션을 시작함
     */
    private void startStudyAni() {
        Animation ani = new FlipAnimation(180f, 0f, mStudyView.getWidth() / 2, mStudyView.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(studyAniListener);
        mStudyView.startAnimation(ani);
    }

    /**
     * 단어 학습 문제의 보기 항목 전환용 애니메이션을 시작함
     *
     * @param view 보기 항목 View
     */
    private void startAnswerAni(LinearLayout view) {
        Animation ani = new FlipAnimation(180f, 0f, mAnswer1.getWidth() / 2, mAnswer1.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(answerAniListener);
        view.startAnimation(ani);
    }

    /**
     * 다음 문제의 보기 항목을 애니메이션과 함께 표시함 모든 보기 항목의 애니메이션이 끝난 경우 해당 문제를 재생함
     */
    private void nextAnswerAni() {
        boolean isListType = isListAnswer();
        mCurAnswerAniCount++;

        if (mCurAnswerAniCount == 2) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer1_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_1());
            else
                ((TextView) findViewById(R.id.word_study_answer1_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_1());

            startAnswerAni(mAnswer2);
        } else if (mCurAnswerAniCount == 3) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer2_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_2());
            else
                ((TextView) findViewById(R.id.word_study_answer2_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_2());

            startAnswerAni(mAnswer3);
        } else if (mCurAnswerAniCount == 4) {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer3_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_3());
            else
                ((TextView) findViewById(R.id.word_study_answer3_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_3());

            if (mAnswer4.getVisibility() == View.VISIBLE) {
                startAnswerAni(mAnswer4);
            }
        } else {
            if (isListType)
                ((TextView) findViewById(R.id.word_study_list_answer4_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_4());

            else {
                ((TextView) findViewById(R.id.word_study_answer4_text)).setText(mStudyData.getPartList().get(mCurStudyIndex).getEx_4());
            }
            mAnswer1.setEnabled(true);
            mAnswer2.setEnabled(true);
            mAnswer3.setEnabled(true);
            mAnswer4.setEnabled(true);

            mCurPlayCount = 1;
            mCurAnswerAniCount = 1;
            mIsTimerReady = true;
            if (mMediaPlayer != null)
                mMediaPlayer.start();
        }
    }

    /**
     * 단어 학습을 시작함 첫번재 문제는 애니메이션 없이 표시함
     *
     * @param filePath 재생 파일 경로
     */
    private void startStudy(String filePath) {
        mStudyView.setText("");

        initAnswers();

        setPlayer(filePath);

		/*if (0 < mCurStudyIndex) {
			if (mStudyData.is_caption()) {
				startStudyAni();
			} else {
				setStudy(false);
				startAnswerAni(mAnswer1);
			}
		} else {*/
        setStudy(true);
        mCurPlayCount = 1;
        mIsTimerReady = true;
        mMediaPlayer.start();
        if (!mIsStudyStart) {
            if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1) {//파트1일때만
                runTimerThread();
            }
            mIsStudyStart = true;
            /*}*/
        }
    }

    /**
     * 단어 문제를 설정함
     *
     * @param isShowAnswer 보기 설정 여부
     */
    private void setStudy(boolean isShowAnswer) {

        if (mStudyData.is_caption()) {
            if (mStudyData.getCurrent_part() == ServiceCommon.PART_GB_PART1) {
                mStudyView.setText(mStudyData.getPartList().get(mCurStudyIndex).getWord().replaceAll("`", "'"));
            } else {
                mStudyView.setText(mStudyData.getPartList().get(mCurStudyIndex).getStatement().replaceAll("`", "'"));
            }
        }
        mStudyCountView.setText(mCurStudyIndex + 1 + "/" + mTotalStudyCount);

        // 보기 설정
        //if (isShowAnswer)
        setAnswers();
    }

    /**
     * 보기 항목의 표시 유형을 판단하여 반환함
     *
     * @return 보기 항목의 표시 유형(Grid / List)
     */
    private boolean isListAnswer() {
        ArrayList<String> distractorList = null;
        try {
            distractorList = mStudyData.getPartList().get(mCurStudyIndex).getDistractorList();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            if (mCurStudyIndex > 0) {
                mCurStudyIndex = mStudyData.getPartList().size() - 1;
            }
            distractorList = mStudyData.getPartList().get(mCurStudyIndex).getDistractorList();
        }
        boolean isListType = false;
        for (int i = 0; i < distractorList.size(); i++) {
            if (ServiceCommon.MAX_WORD_LENGTH < distractorList.get(i).length())
                isListType = true;
        }

        return isListType;
    }

    /**
     * 보기 항목의 View를 초기화함
     */
    private void initAnswers() {
        boolean isListType = isListAnswer();
        findViewById(R.id.word_study_lattice_word_select_layout).setVisibility(isListType ? View.GONE : View.VISIBLE);
        findViewById(R.id.word_study_list_word_select_layout).setVisibility(isListType ? View.VISIBLE : View.GONE);

        if (isListType) {
            mAnswer1 = (LinearLayout) findViewById(R.id.word_study_list_answer_1);
            mAnswer2 = (LinearLayout) findViewById(R.id.word_study_list_answer_2);
            mAnswer3 = (LinearLayout) findViewById(R.id.word_study_list_answer_3);
            mAnswer4 = (LinearLayout) findViewById(R.id.word_study_list_answer_4);

            ((AutofitTextView) findViewById(R.id.word_study_list_answer1_text)).setText("");
            ((AutofitTextView) findViewById(R.id.word_study_list_answer2_text)).setText("");
            ((AutofitTextView) findViewById(R.id.word_study_list_answer3_text)).setText("");
            ((AutofitTextView) findViewById(R.id.word_study_list_answer4_text)).setText("");
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer1_text));
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer2_text));
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer3_text));
            AutofitHelper.create((AutofitTextView) findViewById(R.id.word_study_list_answer4_text));
        } else {
            mAnswer1 = (LinearLayout) findViewById(R.id.word_study_answer_1);
            mAnswer2 = (LinearLayout) findViewById(R.id.word_study_answer_2);
            mAnswer3 = (LinearLayout) findViewById(R.id.word_study_answer_3);
            mAnswer4 = (LinearLayout) findViewById(R.id.word_study_answer_4);

            ((TextView) findViewById(R.id.word_study_answer1_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer2_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer3_text)).setText("");
            ((TextView) findViewById(R.id.word_study_answer4_text)).setText("");
        }

        mAnswer1.setOnClickListener(this);
        mAnswer2.setOnClickListener(this);
        mAnswer3.setOnClickListener(this);
        mAnswer4.setOnClickListener(this);

        mAnswer1.setTag(ServiceCommon.BUTTONTAG_ANSWER_1);
        mAnswer2.setTag(ServiceCommon.BUTTONTAG_ANSWER_2);
        mAnswer3.setTag(ServiceCommon.BUTTONTAG_ANSWER_3);
        mAnswer4.setTag(ServiceCommon.BUTTONTAG_ANSWER_4);

        mAnswer1.setEnabled(false);
        mAnswer2.setEnabled(false);
        mAnswer3.setEnabled(false);
        mAnswer4.setEnabled(false);
    }

    /**
     * 보기 항목를 설정함
     */
    private void setAnswers() {
        if (mStudyData.getPartList().get(mCurStudyIndex).getDistractorList().isEmpty())
            return;

        ArrayList<String> distractorList = mStudyData.getPartList().get(mCurStudyIndex).getDistractorList();

        if (isListAnswer()) {
            ((TextView) findViewById(R.id.word_study_list_answer1_text)).setText(distractorList.get(0));
            ((TextView) findViewById(R.id.word_study_list_answer2_text)).setText(distractorList.get(1));
            ((TextView) findViewById(R.id.word_study_list_answer3_text)).setText(distractorList.get(2));
            if (distractorList.size() != 3) {
                ((LinearLayout) findViewById(R.id.word_study_list_answer_4)).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.word_study_list_answer4_text)).setText(distractorList.get(3));
                mAnswer4.setEnabled(true);
            } else {
                ((LinearLayout) findViewById(R.id.word_study_list_answer_4)).setVisibility(View.INVISIBLE);
                mAnswer4.setEnabled(false);
            }

        } else {
            ((TextView) findViewById(R.id.word_study_answer1_text)).setText(distractorList.get(0));
            ((TextView) findViewById(R.id.word_study_answer2_text)).setText(distractorList.get(1));
            ((TextView) findViewById(R.id.word_study_answer3_text)).setText(distractorList.get(2));

            if (distractorList.size() != 3) {
                mAnswer4.setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.word_study_answer4_text)).setText(distractorList.get(3));
                mAnswer4.setEnabled(true);
            } else {
                mAnswer4.setVisibility(View.INVISIBLE);
                mAnswer4.setEnabled(false);
            }
        }

        mAnswer1.setEnabled(true);
        mAnswer2.setEnabled(true);
        mAnswer3.setEnabled(true);

    }

    /**
     * 다음 문제를 시작함 마지막 문제 이후엔 단어 학습의 완료 메세지를 전달함
     */
    private void nextStudy() {
        mCurStudyIndex++;
        mIsTimerReady = false;
        playerEnd();
        //테스트 스킵
        if (mTestSkip) {
            mTestSkip = false;
            mCurStudyIndex = mStudyData.getPartList().size() - 1;
        }
        if (mCurStudyIndex < mTotalStudyCount) {
            resetTimer();
            startStudy(mStudyData.getPartList().get(mCurStudyIndex).getAudio_path());
        } else {
            mHandler.removeMessages(ServiceCommon.MSG_WHAT_STUDY);
            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, 0));
        }
    }

    /**
     * 단어 시험의 결과를 저장함
     *
     * @param answer 사용자의 선택 항목
     */
    private void setWordResult(int answer) {
        if (isQlog)
            return;
        if (mCurStudyIndex >= mTotalStudyCount)
            return;

        //int correct = Integer.valueOf(mStudyData.getPartList().get(mCurStudyIndex).getCorrect());

        mStudyData.getPartList().get(mCurStudyIndex).setChoose(String.valueOf(answer));
        mStudyData.setTestResult(mCurStudyIndex, String.valueOf(answer));

        RequestPhpPost reqQLOG = new RequestPhpPost(mContext) {
            @Override
            protected void onPostExecute(Map<String, String> result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
                isQlog = false;
                nextStudy();
            }
        };
        isQlog = true;
        reqQLOG.sendQLOG(mStudyData.getUser_no(), mStudyData.getPartList().get(mCurStudyIndex).getBook_detail_id(), mStudyData.getPartList().get(mCurStudyIndex).getQuestionnumber(), answer, mStudyData.getPartList().get(mCurStudyIndex).getRecord_path(), mStudyData.getClass_second_id(), mStudyData.getCurrent_part());

    }

    /**
     * 단어 시험 제한시간인 5초 타이머 Thread를 시작함
     */
    private void runTimerThread() {
        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                while (null != mTimerThread && !mTimerThread.isInterrupted()) {
                    SystemClock.sleep(1000);
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_UPDATE, 0));
                }
            }
        };
        mTimerThread = new Thread(runnable);
        mTimerThread.start();
    }

    /**
     * 5초 타이머 Thread를 종료함
     */
    private void stopTimerThread() {
        if (null != mTimerThread) {
            mTimerThread.interrupt();
            mTimerThread = null;
        }
    }

    /**
     * 5초 타이머를 Reset함
     */
    private void resetTimer() {
        for (int i = 0; i < mTimerViewList.size(); i++)
            mTimerViewList.get(i).setImageDrawable(mTimerBuble);
        mTimerIndex = 4;
    }

    /**
     * 1초 단위로 경과 표시를 함 5초 경과 후 자동으로 답안을 설정하고 다음 문제를 시작함
     */
    private void setTimer() {
        if (mIsTimerReady) {
            if (0 > mTimerIndex) {
                setWordResult(0);
                //nextStudy();
                return;
            }
            mTimerViewList.get(mTimerIndex).setImageDrawable(mTimerBubleDis);
            mTimerIndex--;
        }
    }

    /**
     * Handler
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {
                case ServiceCommon.MSG_WHAT_STUDY:
                    if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START) {
                        startStudy(mStudyData.getPartList().get(0).getAudio_path());
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_UPDATE) {
                        setTimer();
                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION) {
                        stopTimerThread();
                        timeSwapBuff += timeInMilliseconds;
                        customHandler.removeCallbacks(updateTimerThread);
                        sec_test_study_layout.setVisibility(View.GONE);
                        findViewById(R.id.title_view).setVisibility(View.GONE);
                        sec_test_result_layout.setVisibility(View.VISIBLE);

                        int worngCnt = mStudyData.getTestWorngCnt(mStudyData.getCurrent_part());
                        //ImageView chImage = (ImageView) findViewById(R.id.word_study_result_ch_img);

                        timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                        updatedTime = timeInMilliseconds;
                        int secs = (int) (updatedTime / 1000);
                        int mins = secs / 60;
                        secs = secs % 60;

                        if (0 < worngCnt) {

                            if (1 == mTotalWrongStudyCount) {
                                // guidePlay(R.raw.b_16);
                                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_02));
                            } else if (2 == mTotalWrongStudyCount) {
                                // guidePlay(R.raw.b_17);
                                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_03));
                            } else {
                                // guidePlay(R.raw.b_18);
                                //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_04));
                            }
						/*timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
						updatedTime = timeInMilliseconds;
						int secs = (int) (updatedTime / 1000);
						int mins = secs / 60;
						secs = secs % 60;
						String result = "";
						int qSize = mStudyData.getPartList().size();
						result = +qSize + "문제 중 " + (qSize - worngCnt) + "문제를 맞게 풀었어요.\n" + "(소요 시간 : " + mins + "분  " + secs + "초)";
						mResultView.setText(result);*/
                            mResultView.setText((new StringBuilder().append(mStudyData.getPartList().size()).toString()));
                            mResultView2.setText((new StringBuilder().append(mStudyData.getPartList().size() - worngCnt).append(getString(R.string.string_common_question))).toString());
                        } else {
                            //guidePlay(R.raw.b_15);
                            //chImage.setImageDrawable(getResources().getDrawable(R.drawable.ch_01));
                            findViewById(R.id.sentence_study_result_info_text).setVisibility(View.GONE);
                            findViewById(R.id.sentence_study_result_info_text2).setVisibility(View.GONE);
                            mResultView.setText(getString(R.string.string_study_result_all_correct));
                        }
                        mResultTime.setText(" (소요 시간 : " + mins + "분  " + secs + "초)");

                    } else if (msg.arg1 == ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE) {
					/*if (msg.arg2 == ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE)
						setWrongStudyAnswer();
					else
						nextWrongStudy();*/
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private void readyStudy() {
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
    }

    /**
     * Handler
     */
    private Handler mNetworkHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        Log.k("wusi12", "--- S31 -----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SecTestActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            readyStudy();
                            Log.k("wusi12", "--------------------");
                        }
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        Log.k("wusi12", "-------S32-----------");
                        Log.k("wusi12", "Server Time : " + msg.obj.toString());
                        JSONObject objTime = (JSONObject) msg.obj;

                        String serverTime;
                        try {
                            serverTime = objTime.getString("out1");
                            CommonUtil.syncServerTime(serverTime, SecTestActivity.this);
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } finally {
                            next();
                            finish();
                            Log.k("wusi12", "--------------------");
                        }
                    }
                    break;
                case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                    if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
                        readyStudy();
                    } else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
                        next();
                        finish();
                    }
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        // TODO Auto-generated method stub
        mMediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.pause();
        mCurPlayCount++;
        // 음성 2번 들려준 후 2초 후 정답 공개
        if (2 >= mCurPlayCount) {
            mMediaPlayer.seekTo(0);
        } else {
			/*if (findViewById(R.id.word_study_wrong_layout).getVisibility() == View.VISIBLE) {
				mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE, ServiceCommon.MSG_STUDY_WRONG_PROGRESS_UPDATE_ANSWER_RELEASE), 2000);
			}*/
        }

    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);

            int mins = secs / 60;

            secs = secs % 60;

            //int milliseconds = (int) (updatedTime % 1000);

            txt_small_timer.setText("" + String.format("%02d", mins) + ":"

                    + String.format("%02d", secs)); /*+ ":"
													
													+ String.format("%02d", milliseconds));*/

            customHandler.postDelayed(this, 0);

        }

    };

}
