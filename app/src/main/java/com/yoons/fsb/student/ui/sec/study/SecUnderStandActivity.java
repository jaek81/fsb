package com.yoons.fsb.student.ui.sec.study;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.ui.sec.base.SecBaseStudyActivity;
import com.yoons.fsb.student.ui.textview.AutofitHelper;
import com.yoons.fsb.student.ui.textview.AutofitTextView;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.FlipAnimation;
import com.yoons.fsb.student.util.Preferences;

/**
 * 단어시험 화면
 *
 * @author jaek
 */
public class SecUnderStandActivity extends SecBaseStudyActivity implements OnClickListener, OnCompletionListener, OnSeekCompleteListener {

    private LinearLayout layout_bottom = null;
    private TextView txt_cnt;
    private AutofitTextView txtWord = null, txtMean = null;
    private Button mNextStatus = null;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;

    private int status = 0;
    //	private boolean mOnclick=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_sec_understand);
        else  // 숲
            setContentView(R.layout.f_sec_understand);

        setWidget();

        status = getIntent().getIntExtra(ServiceCommon.TARGET_ACTIVITY, 0);

        tCategory.setText("스마트 훈련");
        if (status == SecStudyData.TP_TYPE_SEC_WORD_UNDERSTAND)
            tCategory2.setText(R.string.string_common_sec_word_study);
        else if (status == SecStudyData.TP_TYPE_SEC_SENTENCE_UNDERSTAND)
            tCategory2.setText(R.string.string_ga_SEC_SENTENCE_UNDERSTAND);

        // TitleView
        //tBookName.setText(mStudyData.mProductName);

        setPreferencesCallback();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startStudy();
            }
        }, 500);

        Crashlytics.log(getString(R.string.string_ga_SEC_UNDERSTAND));
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub

        switch (view.getId()) {
            case R.id.next_status_btn:
                next();
                finish();
                break;

            default:
                super.onClick(view);
                break;
        }
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        layout_bottom = (LinearLayout) findViewById(R.id.layout_bottom);
        txtWord = (AutofitTextView) findViewById(R.id.word_study_wrong_question_text);
        txtMean = (AutofitTextView) findViewById(R.id.word_study_wrong_correct_answer_text);
        mNextStatus = (Button) findViewById(R.id.next_status_btn);
        txt_cnt = (TextView) findViewById(R.id.txt_cnt);

        AutofitHelper.create(txtWord);
        AutofitHelper.create(txtMean);
        mNextStatus.setOnClickListener(this);

        mTotalStudyCount = mStudyData.getPartList().size();

    }

    /**
     * Player를 설정함
     *
     * @param mp3FilePath 재생 파일 경로
     */
    private void setPlayer(String mp3FilePath) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mp3FilePath);
            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.prepare();
            mCurPlayCount = 1;
            /*if (mp3FilePath.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
				mMediaPlayer.setVolume(0, 0);
			}*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // --- 틀린 단어 학습 --

    /**
     * 하나끝낫을때
     */
    private AnimationListener studyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            //setWrongStudy();
            mCurPlayCount = 1;

            if (mMediaPlayer != null) {
                mMediaPlayer.start();
            } else {
                if (findViewById(R.id.word_study_wrong_layout).getVisibility() == View.VISIBLE) {
                }
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 틀린 단어 문제 전환용 애니메이션을 시작함
     */
    private void startWrongStudyAni() {
        Animation ani = new FlipAnimation(180f, 0f, txtWord.getWidth() / 2, txtWord.getHeight() / 2, 0f, false);
        ani.setDuration(200);
        ani.setAnimationListener(studyAniListener);
        txtWord.startAnimation(ani);
    }

    /**
     * 이해시작
     * <p>
     * 재생 파일 경로
     */
    private void startStudy() {
        //테스트 스킵
        if (mTestSkip) {
            mTestSkip = false;
            mCurStudyIndex = mStudyData.getPartList().size() - 1;
        }
        if (mCurStudyIndex < mStudyData.getPartList().size()) {
            txt_cnt.setText(mCurStudyIndex + 1 + " / " + mStudyData.getPartList().size());

            String word_sentence, mean;
            word_sentence = mStudyData.getEngWordSentence(mCurStudyIndex);
            mean = mStudyData.getKorMeans(mCurStudyIndex);

            txtWord.setText(word_sentence);
            txtMean.setText(mean);

            setPlayer(mStudyData.getPartList().get(mCurStudyIndex).getAudio_path());
            mMediaPlayer.start();
            //startWrongStudyAni();
        } else {//끝일때
            layout_bottom.setVisibility(View.VISIBLE);
            mNextStatus.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 다음 틀린 단어 문제를 시작함
     */
    private void nextStudy() {
        mCurStudyIndex++;
        playerEnd();
        startStudy();

    }

    /**
     * 틀린 단어 문제 2번 재생 완료시 2초 후 정답 공개 메세지를 전달함
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.pause();

        // 음성 2번 들려준 후 2초 후 정답 공개
        if (2 > mCurPlayCount) {
            mMediaPlayer.seekTo(0);
            mCurPlayCount++;
        } else {
            nextStudy();
        }
    }

    /**
     * 1번 재생 완료 이후 처음으로 Seek 완료 되어 재생을 시작함
     */
    @Override
    public void onSeekComplete(MediaPlayer mp) {
        mMediaPlayer.start();
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		/*if (!mIsStudyStart)
			return false;*/

        //keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mNextStatus.getVisibility() == View.VISIBLE)
                    mNextStatus.setPressed(true);
                break;

            default:
                return false;
        }
        return false;

    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
		/*if (!mIsStudyStart)
			return false;*/

        //keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mNextStatus.getVisibility() == View.VISIBLE) {
                    mNextStatus.setPressed(false);
                    next();
                    finish();
                }
                break;

            default:
                return false;
        }
        return false;

    }

    public static void setTextViewScale(TextView textView, int textViewWidth) {
        float originScaleX = textView.getTextScaleX();
        float scaleX = originScaleX;
        String text = textView.getText().toString();

        Paint paint = textView.getPaint();
        while (paint.measureText(text) > textViewWidth) {
            paint = new Paint(paint);
            scaleX -= 0.1F;
            paint.setTextScaleX(scaleX);
        }

        if (originScaleX != scaleX) {
            textView.setTextScaleX(scaleX);
        }
    }

}
