package com.yoons.fsb.student.ui.sec.study;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomWavRecoder;
import com.yoons.fsb.student.data.sec.PartData;
import com.yoons.fsb.student.data.sec.SecStudyData;
import com.yoons.fsb.student.data.sec.VipWord;
import com.yoons.fsb.student.network.RequestPhpPost;
import com.yoons.fsb.student.ui.popup.LoadingDialog;
import com.yoons.fsb.student.ui.sec.base.SecBaseStudyActivity;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.vanishing.layout.VanishingWordLayout;
import com.yoons.recognition.VoiceRecognizerWeb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecVipActivity extends SecBaseStudyActivity implements OnClickListener, OnPreparedListener {

    /* private VoiceRecognizerWeb_VC VRUTask = null; */
    private VoiceRecognizerWeb VRUTask = null;
    private int mVoiceScore = -99;
    private String mVoiceText = "";
    private int mEchoDuration = 0;
    private String fullRecordPath = "";

    private Button vip_next_btn;

    private boolean isVoiceRecongize;
    private boolean isEchoing;

    private RelativeLayout mParentLayout = null;

    private ImageView mListen = null, mRecord = null, mRecordPlay = null;

    private TextView mStudyCountView = null;

    private LoadingDialog mLoadingDialog = null;

    private LinearLayout text_linear = null;

    private int playDuration = 0;
    private int beforIndex = 0;

    private TextView txt_step_info;
    private int curStep = 0;//0:init 1:one 2:tow 3:three

    //private TextView vanishingToolTip;
    private boolean first_check = true;
    private LinearLayout layout_speaker;
    private int vip_index = 0;
    private Button btn_skip;

    private LinearLayout layout_btn_bottom;
    private LinearLayout layout_next;
    private boolean beforNewLine = false;
    private boolean isNewLine = false;

    private TextView tCategory = null, tCategory2 = null;

    /**
     * 에니메이션(사라지는거) 7초뒤 녹음진행
     */
    private CountDownTimer mCountTimer = new CountDownTimer(7000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
        }

        @Override
        public void onFinish() {
            //vanishingToolTip.setVisibility(View.INVISIBLE);
            recordNotiPlay();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // igse
            setContentView(R.layout.igse_sec_vip);
        else // 숲
            setContentView(R.layout.f_sec_vip);

        mRecordFilePath = ServiceCommon.CONTENTS_PATH + ServiceCommon.WRONG_SENTENCE_RECORD_FILE_NAME + ServiceCommon.WAV_FILE_TAIL;

        setWidget();

        tCategory.setText("스마트 훈련");
        tCategory2.setText(R.string.string_ga_SEC_SENTENCE_VIP);

        setPreferencesCallback();
        if (mTotalStudyCount == 0) {
            next();
            finish();
        }
        mCurStudyIndex = mStudyData.getQuiz_seq();//중도학습

        if (mCurStudyIndex != 0 && mStudyData.getPartList().get(mCurStudyIndex).getVcsentence().length() > 2) {
            try {

                for (PartData pdata : mStudyData.getPartList()) {//중도학습관련 몇번째 vip학습인지 체크
                    if (pdata.getVcsentence().length() > 2) {
                        if (mStudyData.getPartList().get(mCurStudyIndex).getVcsentence().equals(pdata.getVcsentence())) {
                            break;
                        } else {
                            vip_index++;
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                vip_index = 0;
            }
        }
        //GA적용
        Crashlytics.log(getString(R.string.string_ga_SEC_SENTENCE_VIP));

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (first_check) {
            first_check = false;
            startStudy();

        }
    }

    @Override
    public void onClick(View v) {

        if (singleProcessChecker())
            return;

        int id = v.getId();

        switch (id) {
            case R.id.sentence_study_record_btn:
                if (mRecord.isEnabled() && mIsRecording)
                    recordEnd();
			/*else
				recordReady(getMaxRecordTime(ServiceCommon.MAX_REC_TIME_20SEC, playDuration));*/
                break;

            case R.id.sentence_study_result_confirm_btn:
            case R.id.sentence_study_wrong_next_status_btn:
                next();
                finish();
                break;
            case R.id.vanishing_cloze_skip_btn:
                if (curStep != 3) {
                    skipNext();
                }
                break;
            case R.id.vip_next_btn:
                next();
                finish();
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {

        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        layout_speaker = (LinearLayout) findViewById(R.id.text_speaker);
        layout_btn_bottom = (LinearLayout) findViewById(R.id.btn_layout);
        layout_next = (LinearLayout) findViewById(R.id.vip_next_layout);
        mParentLayout = (RelativeLayout) findViewById(R.id.sentence_exam_parent_layout);
        mStudyCountView = (TextView) findViewById(R.id.sentence_study_question_count_text);
        mListen = (ImageView) findViewById(R.id.sentence_study_listen_btn);
        mRecord = (ImageView) findViewById(R.id.sentence_study_record_btn);
        mRecordPlay = (ImageView) findViewById(R.id.sentence_study_recordplay_btn);

        txt_step_info = (TextView) findViewById(R.id.vanishing_cloze_step_info);

        //vanishingToolTip = (TextView) findViewById(R.id.vanishing_tooltip_text);
        btn_skip = (Button) findViewById(R.id.vanishing_cloze_skip_btn);
        vip_next_btn = (Button) findViewById(R.id.vip_next_btn);
        btn_skip.setOnClickListener(this);
        vip_next_btn.setOnClickListener(this);

        mRecord.setOnClickListener(this);

        mListen.setEnabled(false);
        mRecord.setEnabled(false);
        mRecordPlay.setEnabled(false);

        mTotalStudyCount = mStudyData.getVip_cnt();

        //setTitlebarCategory(getString(R.string.string_titlebar_category_study));
        text_linear = (LinearLayout) findViewById(R.id.text_linear);

        findViewById(R.id.text_linear).setVisibility(mStudyData.is_caption() ? View.VISIBLE : View.GONE);

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);
        tooltip.setVisibility(View.INVISIBLE);
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));
        tipText.setText(getResources().getString(R.string.string_vanishing_tooltip_contents));
        mParentLayout.addView(tooltip);
    }

    /**
     * 학습 가이드를 표시함
     *
     * @param tip 학습 가이드 내용
     */
    private void showToolTip(String tip) {
        /*if (!mStudyData.mIsStudyGuide)
            return;*/

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View tooltip = vi.inflate(R.layout.c_layout_record_tooltip, null);

        int x = 0, y = 0;
        LinearLayout tbpl = ((LinearLayout) findViewById(R.id.vip_three_btn_layout));
        TextView tipText = ((TextView) tooltip.findViewById(R.id.tooltip_record_text));

        tipText.setText(tip);
        mParentLayout.addView(tooltip);

        int centerWidth = tbpl.getWidth() / 2;
        x = centerWidth - (mTooltipWidth / 2);

        y = mParentLayout.getHeight() - tbpl.getHeight() - (mTooltipHeight / 2);

        ViewGroup.MarginLayoutParams vm = (MarginLayoutParams) tooltip.getLayoutParams();
        vm.leftMargin = x;
        vm.topMargin = y;
    }

    /**
     * 학습 가이드를 지움
     */
    private void hideToolTip() {
        if (null != findViewById(R.id.tooltip_record_text))
            mParentLayout.removeView(findViewById(R.id.tooltip_record_text));
    }

    /**
     * Recorder를 설정함
     *
     * @param wavFilePath 녹음 파일 저장 경로
     */
    private void setRecorder(String wavFilePath) {
        mWavRecorder = new CustomWavRecoder(wavFilePath);
    }

    /**
     * 3 Player를 설정함
     * <p>
     * 재생 파일 경로
     */
    private void setPlayer() {
        String mp3FilePath = mStudyData.getPartList().get(mCurStudyIndex).getAudio_path();
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mp3FilePath);
            //mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    curStep++;
                    playerComplete();
                    stepNext();
                }
            });
            mMediaPlayer.prepare();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 음성재생(빨간색 나오게)
     */
    private void playerStart() {
        if (curStep == 1 || curStep == 2) {
            text_linear.setVisibility(View.GONE);
            layout_speaker.setVisibility(View.VISIBLE);
        }
        mMediaPlayer.start();
        //mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_red_btn_bg));

        if (CommonUtil.isCenter()) // igse
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen2));
        else // 숲
            mListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen2));

        listenIngEffect();
    }

    private void listenIngEffect() {
        int duration = mMediaPlayer.getDuration();
        AnimationDrawable ad = null;
        final LinearLayout parent = ((LinearLayout) mListen.getParent());
        if (CommonUtil.isCenter()) // igse
            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
        else // 숲
            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
        final AnimationDrawable ing = new AnimationDrawable();
        final int frameTime = duration / ad.getNumberOfFrames();

        for (int i = 0; i < ad.getNumberOfFrames(); i++) {
            Drawable draw = ad.getFrame(i);
            ing.addFrame(draw, frameTime);
        }
        ing.setOneShot(true);
        parent.setBackground(ing);
        ((AnimationDrawable) parent.getBackground()).start();
    }

    /**
     * Player 종료 후 녹음을 준비함
     */
    private void playerComplete() {

        //mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_btn_bg));
        mListen.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_listen_d));
        if (curStep == 1 || curStep == 2) {
            text_linear.setVisibility(View.VISIBLE);
            layout_speaker.setVisibility(View.GONE);
        }

        playDuration = mMediaPlayer.getDuration();

        playerEnd();

        recordReady(getMaxRecordTime(ServiceCommon.MAX_REC_TIME_20SEC, playDuration));
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        mMediaPlayer.stop();
        mMediaPlayer.release();
        mMediaPlayer = null;
        ((LinearLayout) mListen.getParent()).setBackground(null);
    }

    /**
     * 2 학습을 시작함
     * <p>
     * 문장 파일 경로
     */
    private void startPlay() {
        setPlayer();
        playerStart();

    }

    /**
     * 1 다음 문제를 설정함 마지막 문제 이후엔 음성 인식 채점을 요청함
     */
    private void startStudy() {

        if (mTestSkip) {//스킵 테스트에서만
            mTestSkip = false;
            mCurStudyIndex = mStudyData.getPartList().size() - 1;
        }
        if (mCurStudyIndex < mStudyData.getPartList().size()) {//Partlist에서 vip가 들어있는것만 재생
            if (mStudyData.getPartList().get(mCurStudyIndex).getVcsentence().length() > 2) {//vip가 아닐때
                mStudyCountView.setText(vip_index + 1 + "/" + mTotalStudyCount);
                stepInit();
            } else {//vip면 다시호출
                mCurStudyIndex++;
                startStudy();
            }
        } else {
            layout_btn_bottom.setVisibility(View.GONE);
            layout_next.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 태그 파싱해서 순서대로 넣기
     *
     * @param vipSentence
     * @return
     */
    private List<VipWord> makeTxt(String vipSentence) {
        List<VipWord> result = new ArrayList<VipWord>();
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(vipSentence.split(" ")));
        int is_small = isSmaillFont(list);

        //태그 제거 문구
        String sentence = vipSentence.replace("<u1>", "").replace("</u1>", "").replace("<u2>", "").replace("</u2>", "").replace("<u3>", "").replace("</u3>", "");
        int lenLevel = 1;
        if (sentence.length() > ServiceCommon.VANISHING_TEXT_SHORT) {
            if (sentence.length() < ServiceCommon.VANISHING_TEXT_LONG)
                lenLevel = 1;
            else
                lenLevel = 2;
        }

        for (String str : list) {
            String word;
            int index = 0;
            if (str.contains("<u1>") || str.contains("</u1>")) {
                word = str.replace("<u1>", "").replace("</u1>", "");
                index = 1;
            } else if (str.contains("<u2>") || str.contains("</u2>")) {
                word = str.replace("<u2>", "").replace("</u2>", "");
                index = 2;
            } else if (str.contains("<u3>") || str.contains("</u3>")) {
                word = str.replace("<u3>", "").replace("</u3>", "");
                index = 3;
            } else {
                word = str;
            }
            word = word + " ";
            VipWord vipWord = new VipWord(mContext, word, index, is_small, lenLevel, isNewLine);
            result.add(vipWord);
        }
        return result;
    }

    /**
     * 학습준비(텍스트 나오고 음성플레이)
     */
    private void stepInit() {

        curStep = 0;
        txt_step_info.setText("학습준비");

        String vipSentence = mStudyData.getPartList().get(mCurStudyIndex).getVcsentence();
        List<VipWord> list = makeTxt(vipSentence);

        //텍스트 보여 주기
        addTextLinear(list, false);

        startPlay();

    }

    private void stepOne() {

        txt_step_info.setText("학습 Step.1");

        String vipSentence = mStudyData.getPartList().get(mCurStudyIndex).getVcsentence();
        List<VipWord> list = makeTxt(vipSentence);

        //텍스트 보여 주기
        addTextLinear(list, false);
        mCountTimer.start();
        //vanishingToolTip.setVisibility(View.VISIBLE);

    }

    private void stepTwo() {
        txt_step_info.setText("학습 Step.2");

        String vipSentence = mStudyData.getPartList().get(mCurStudyIndex).getVcsentence();
        List<VipWord> list = makeTxt(vipSentence);

        //텍스트 보여 주기
        addTextLinear(list, false);
        mCountTimer.start();
        //vanishingToolTip.setVisibility(View.VISIBLE);

    }

    private void stepThree() {
        txt_step_info.setText("학습 Step.3");

        text_linear.setVisibility(View.VISIBLE);
        layout_speaker.setVisibility(View.GONE);
        String vipSentence = mStudyData.getPartList().get(mCurStudyIndex).getVcsentence();
        List<VipWord> list = makeTxt(vipSentence);

        //텍스트 보여 주기
        addTextLinear(list, false);
        mCountTimer.start();
        //vanishingToolTip.setVisibility(View.VISIBLE);
    }

    private void stepNext() {
        switch (curStep) {
            case 0:
                stepInit();
                break;
            case 1:
                stepOne();
                break;
            case 2:
                stepTwo();
                break;
            case 3:
                stepThree();
                break;

            default:
                break;
        }

    }

//--- Recording && Write Animation effect ----

    /**
     * 녹음 진행 효과 시작 Runnable
     */
    private Runnable recordEffectStartRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 완료 효과 시작 Runnable
     */
    private Runnable recordEffectEndRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) mRecord.getParent()).getBackground();
                aniDraw.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과 종료 Runnable
     */
    private Runnable recordEffectStopRunnable = new Runnable() {
        public void run() {
            if (null == mContext)
                return;

            try {
                AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                aniDraw.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * 녹음 진행 효과를 시작함
     */
    private void startIngEffect() {

        if (CommonUtil.isCenter()) // Igse
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_btn_learning_record_bg_n));
        else // 숲
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_btn_learning_record_bg_n));
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_seq_learning_record_volume));

        mIngEffectThread = new Thread(recordEffectStartRunnable);
        mIngEffectThread.start();
    }

    /**
     * 녹음 진행 효과를 종료함
     */
    private void stopIngEffect() {
        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_nor));

        if (null != recordEffectStartRunnable) {
            mIngEffectThread.interrupt();
            mIngEffectThread = null;
        }
        mIngEffectThread = new Thread(recordEffectStopRunnable);
        mIngEffectThread.start();
    }

    /**
     * 녹음 완료 효과를 1초간 진행함
     */
    private void completeEffect() {
        if (CommonUtil.isCenter()) { // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record2));
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_seq_learning_progress));
        } else { // 숲
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record2));
            ((LinearLayout) mRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_seq_learning_progress));
        }

        //mCompleteEffectThread = new Thread(recordEffectStartRunnable);
        mCompleteEffectThread = new Thread(recordEffectEndRunnable);
        mCompleteEffectThread.start();

        try {
            AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) mRecord.getParent()).getBackground();
            aniDraw.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Runnable runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                if (null != mCompleteEffectThread) {
                    mCompleteEffectThread.interrupt();
                    mCompleteEffectThread = null;
                }

                try {
                    //AnimationDrawable aniDraw = (AnimationDrawable) ((ImageView) mRecord).getDrawable();
                    AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) mRecord.getParent()).getBackground();
                    aniDraw.stop();
                    //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_nor));
                    ((LinearLayout) mRecord.getParent()).setBackground(null);
                    mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_record_d));
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (curStep == 3) {//3번재 학습은 녹음임
                        recordPlay();
                    } else {
                        startPlay();
                    }
                }
            }
        };
        //mRecord.postDelayed(runnable, 1200);
        ((LinearLayout) mRecord.getParent()).postDelayed(runnable, 1200);
    }

//------------------- Recording----------------------

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordNotiCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            recordNotiEnd();
            recordReady(getMaxRecordTime(ServiceCommon.MAX_REC_TIME_20SEC, playDuration));
            recordStart();
            //Handler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));
        }
    };

    /**
     * 녹음 시작 알림음을 재생함
     */
    private void recordNotiPlay() {
        if (null != mRecordNotiPlayer)
            return;

        mRecordNotiPlayer = MediaPlayer.create(getBaseContext(), R.raw.ding);
        if (null != mRecordNotiPlayer) {
            mRecordNotiPlayer.setOnCompletionListener(mRecordNotiCompletion);
            mRecordNotiPlayer.start();
        }
    }

    /**
     * 녹음 시작 알림음을 종료함
     */
    private void recordNotiEnd() {
        if (null == mRecordNotiPlayer)
            return;

        if (mRecordNotiPlayer.isPlaying())
            mRecordNotiPlayer.stop();
        mRecordNotiPlayer.release();
        mRecordNotiPlayer = null;
    }

    /**
     * 버튼 비활성화 및 Recorder를 설정하고 녹음 시작 알림음을 재생시켜 녹음을 준비함
     */
    private void recordReady(int maxRecTime) {
        if (mIsRecording)
            return;

        mIsRecording = true;

        mRecord.setEnabled(false);

        String recordPath = "";
        recordPath = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path().trim();

        // AudioRecorderforWeb는 녹음파일경로에서 고정적인 .wav는 제외
        setRecorder(recordPath.replace(ServiceCommon.WAV_FILE_TAIL, ""));

        mMaxRecTime = maxRecTime;

    }

    /**
     * 녹음을 시작하고 1초 뒤 녹음 완료를 진행 할 수 있도록 녹음 버튼을 활성화 함
     */
    private void recordStart() {
        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_start));

        if (CommonUtil.isCenter()) // igse
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
        else // 숲
            mRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));

        recordHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
        mWavRecorder.startRecording();
        recordHandler.sendMessageDelayed(recordHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), 20000);
        Runnable runnable = null;
        runnable = new Runnable() {
            public void run() {
                if (null == mContext)
                    return;

                startIngEffect();
                //vanishingToolTip.setVisibility(View.VISIBLE);
                showToolTip(getResources().getString(R.string.string_record_tool_tip));
                mRecord.setEnabled(true);
            }
        };

        mRecord.postDelayed(runnable, 1000);
    }

    /**
     * 녹음을 종료함
     */
    private void recordEnd() {

        recordHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);

        if (mIsRecording) {
            mRecord.setEnabled(false);
            mWavRecorder.stopRecording();
            mIsRecording = false;
            if (curStep == 3) {
                setRecordResult(mCurStudyIndex);
            }
            stopIngEffect();
            hideToolTip();
            completeEffect();
        }
    }

    /**
     * 음성 인식 채점을 요청함
     */
	/*private void requestVoiceRecognize() {
		final int curindex = mCurStudyIndex;
		mVoiceScore = -99;
		mVoiceText = "";

		String fullRecordPath = mStudyData.getPartList().get(curindex).getRecord_path().trim();

		File recordFile = new File(fullRecordPath);
		// 녹음 파일이 없는 경우
		if (!recordFile.exists()) {
			setRecordResult(mVoiceText, mVoiceScore, curindex);
			return;
		}

		// 음성 평가 전 네트워크 상태 불가능의 경우
		if (!CommonUtil.isAvailableNetwork(this, false)) {
			setRecordResult(mVoiceText, mVoiceScore, curindex);
			return;
		}

		mIsRecoginition = true;
		String[] recordPathArray = fullRecordPath.split("/");
		String recordFileName = recordPathArray[recordPathArray.length - 1];
		final String recordPath = fullRecordPath.replace(recordFileName, "").trim();
		if (!recordFileName.contains(ServiceCommon.WAV_FILE_TAIL))
			recordFileName += ServiceCommon.WAV_FILE_TAIL;

		try {
			VRUTask = new VoiceRecognizerWeb(getApplicationContext());

			VRUTask.mstt = new OnMSTT() {

				@Override
				public void getSentenceObj(SentenceObj senObj) {
					if (senObj != null) {
						setRecordResult(senObj.getRec_sentence(), NumberUtil.Half6(senObj.getTotal_score()), curindex);
					}
				}

			};
			//vip내용
			String sentence = mStudyData.getPartList().get(curindex).getVcsentence();
			//vip 태그 제거 내용
			String tag_remove_sentence = sentence.replace("<u1>", "").replace("</u1>", "").replace("<u2>", "").replace("</u2>", "").replace("<u3>", "").replace("</u3>", "");

			VRUTask.execute(recordFileName, recordPath, "16000", tag_remove_sentence, String.valueOf(mStudyData.getMcode()), mEchoDuration, tagVip(sentence), 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

    /**
     * 음성 인식 결과를 설정함
     * <p>
     * 음성 인식된 텍스트
     * 음성 인식 하여 채점된 점수 or 오류 코드
     */
    private void setRecordResult(int curindex) {
        try {
            int quiz_no = mStudyData.getPartList().get(curindex).getQuestionnumber();
            String rec_file = mStudyData.getPartList().get(curindex).getRecord_path().replace(".wav", ".mp3");
            int study_type = mStudyData.getCurrent_part();

            RequestPhpPost reqQORE = new RequestPhpPost(mContext);

            reqQORE.sendQORE(mStudyData.getPartList().get(curindex).getBook_id(), mStudyData.getPartList().get(curindex).getBook_detail_id(), mStudyData.getClass_second_id(), quiz_no, rec_file, study_type);
            //mStudyData.getPartList().get(curindex).setAnswers(score);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 에코를 재생함
     */
    private void recordPlay() {
        if (null != mRecordPlayer && mRecordPlayer.isPlaying())
            return;

        text_linear.setVisibility(View.VISIBLE);
        layout_speaker.setVisibility(View.GONE);
        String vipSentence = mStudyData.getPartList().get(mCurStudyIndex).getVcsentence();
        List<VipWord> list = makeTxt(vipSentence);
        //텍스트 보여 주기
        addTextLinear(list, true);

        String dataSource = mStudyData.getPartList().get(mCurStudyIndex).getRecord_path();

        //mRecord.setImageDrawable(getResources().getDrawable(R.drawable.selector_record_btn_bg));
        //mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo_red_btn_bg));

        if (CommonUtil.isCenter()) // igse
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat2));
        else // 숲
            mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat2));

        try {
            /*mRecordPlayer = new MediaPlayer();
            mRecordPlayer.reset();
            mRecordPlayer.setDataSource(dataSource);
            mRecordPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    recordPlayEnd();
                }
            });
            mRecordPlayer.setOnPreparedListener(this);
            mRecordPlayer.prepare();*/
            if (dataSource != null) {
                File f = new File(dataSource);
                if (f.exists()) {

                    mRecordPlayer = new MediaPlayer();
                    if (mRecordPlayer != null) {

                        int duration = 1200;

                        if (f.length() > 300) {
                            mRecordPlayer.setDataSource(this, Uri.fromFile(f));
                            mRecordPlayer.setOnCompletionListener(new OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    recordPlayEnd();
                                }
                            });
                            mRecordPlayer.setOnPreparedListener(this);
                            mRecordPlayer.prepare();
                            duration = mRecordPlayer.getDuration();
                        }

                        final LinearLayout parent = ((LinearLayout) mRecordPlay.getParent());
                        AnimationDrawable ad = null;

                        if (CommonUtil.isCenter()) { // igse
                            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
                        } else { // 숲
                            ad = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
                        }

                        final AnimationDrawable ing = new AnimationDrawable();
                        final int frameTime = duration / ad.getNumberOfFrames();
                        Log.i("", "duration => " + duration + " frame time => " + frameTime);
                        ing.setOneShot(true);
                        for (int i = 0; i < ad.getNumberOfFrames(); i++) {
                            Drawable draw = ad.getFrame(i);
                            ing.addFrame(draw, frameTime);
                        }
                        parent.setBackground(ing);
                        ((AnimationDrawable) parent.getBackground()).start();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            recordPlayEnd();
        }
    }
    //mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0));

    /**
     * 에코 재생을 종료하고 에코 볼륨을 원복함
     */
    private void recordPlayEnd() {
        // 녹음 재생 후 종료

        if (null != mRecordPlayer) {
            mRecordPlayer.stop();
            mRecordPlayer.release();
            mRecordPlayer = null;

            //mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo_btn_bg));

            if (CommonUtil.isCenter()) // igse
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat));
            else // 숲
                mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat));

            releaseEchoVolume();
        }

        final LinearLayout parent = (LinearLayout) mRecordPlay.getParent();

        try {
            AnimationDrawable aniDraw = (AnimationDrawable) ((LinearLayout) parent).getBackground();
            aniDraw.stop();
            parent.setBackground(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        File fileRecord = new File(mRecordFilePath);
        if (fileRecord.exists())
            fileRecord.delete();

        mCurStudyIndex++;
        vip_index++;
        startStudy();

		/*else
			nextWrongStudy();*/
    }

    /**
     * 에코 플레이어의 준비 완료시 에코 볼륨 설정 및 재생을 시작함
     */
    public void onPrepared(MediaPlayer mp) {
        if (null != mRecordPlayer) {
            setEchoVolume();
            mRecordPlayer.start();
        }
    }

    /**
     * 문장 학습 결과 화면을 표시함
     */
    private void setSentenceResult() {
        if (null != mLoadingDialog) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
        findViewById(R.id.sentence_study_result_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.sentence_study_ing_layout).setVisibility(View.GONE);
    }

    /**
     * 최대 녹음 시간(녹음 자동 종료 시간)을 계산하여 반환함
     *
     * @param baseMaxRecordTime 20초
     * @param playDuration      문장 재생 시간
     * @return 최대 녹음 시간
     */
    private int getMaxRecordTime(int baseMaxRecordTime, int playDuration) {
        Log.i("", "getMaxRecordTime playDuration => " + playDuration);

        int maxRecordTime = baseMaxRecordTime;
        int customMaxRecordTime = playDuration + (playDuration * 2);

        if (baseMaxRecordTime < customMaxRecordTime)
            maxRecordTime = playDuration * 2;
        else
            maxRecordTime = baseMaxRecordTime - playDuration;

        Log.i("", "getMaxRecordTime customMaxRecordTime => " + customMaxRecordTime + " maxRecordTime => " + maxRecordTime);

        return maxRecordTime;
    }

    /**
     * 화면 구성 완료 후 호출됨 학습 가이드의 Width, Height 값을 저장함
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if (hasFocus) {
            View tooltip = findViewById(R.id.tooltip_record_text);
            if (null != tooltip) {
                mTooltipHeight = tooltip.getHeight();
                mTooltipWidth = tooltip.getWidth();
                mParentLayout.removeView(tooltip);
            }
        }
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:

                if (View.VISIBLE == findViewById(R.id.vip_next_layout).getVisibility()) {
                    vip_next_btn.setPressed(true);
                }
                break;

            default:
                return false;
        }

        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mRecord.isEnabled() && mIsRecording) {
                    recordEnd();
                } else if (View.VISIBLE == findViewById(R.id.vip_next_layout).getVisibility()) {
                    vip_next_btn.setPressed(false);
                    next();
                    finish();
                }

                break;

            default:
                return false;
        }

        return super.onKeyUp(keyCode, event);
    }

    /**
     * vip관련 txtview하고 몇번째사라질지 정보 가지고 있는 vo
     *
     */

    /**
     * 작은 폰트 적용할지 여부 반환
     *
     * @param list
     * @return
     */
    private int isSmaillFont(ArrayList<String> list) {
        int line = 1;
        int len = 0;
        int before = 0;
        int newline = 0;

        for (int i = 0; i < list.size(); i++) {
            String str = list.get(i);
            len += str.length();

            if (isNewLineWord(str)) {
                newline++;
                continue;
            }

            if (len - before <= ServiceCommon.MAX_VANISHING_LENGTH) {

            } else {
                before = len - str.length();
                line++;
            }
        }
        if (newline > 0) {
            isNewLine = true;
        }

        return line + newline;
//		if(line + newline > 2){
//			return true;
//		}
//		return false;
    }

    private boolean isNewLineWord(String word) {
        if (word.contains("\n")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param list
     * @param is_last, true=마지막
     */
    private void addTextLinear(List<VipWord> list, boolean is_last) {
        text_linear.removeAllViews();
        int cur_len = 0;
        for (VipWord vipWord : list) {

            cur_len = cur_len + vipWord.getTxt().length();
            int isSmall = vipWord.getSmall();
            int lenLevel = vipWord.getLenLevel();
            int cnt = text_linear.getChildCount();
            String txt = vipWord.getWordLayout().getWord();

            if (isNewLineWord(txt)) {
                beforNewLine = true;
                continue;
            }

            int vanishingRevise = ServiceCommon.VANISHING_REVISE;
            int maxVanishingLength = ServiceCommon.MAX_VANISHING_LENGTH;

            int revise = isSmall > 2 ? vanishingRevise : 0;

            if (lenLevel == 1)
                revise = ServiceCommon.VANISHING_REVISE_MIDDLE;
            if (lenLevel == 2 || (isSmall > 3 && isNewLine))
                revise = ServiceCommon.VANISHING_REVISE_LONG;

            if (cnt == 0) {
                LinearLayout linear = new LinearLayout(mContext);
                linear.setOrientation(LinearLayout.HORIZONTAL);
                VanishingWordLayout layout = is_last == true ? getLastVipLayout(vipWord) : getVipLayout(vipWord);

                linear.addView(layout);
                text_linear.addView(linear);
                beforIndex = 0;
            } else {
                if (cur_len - beforIndex <= maxVanishingLength + revise && !beforNewLine) {
                    LinearLayout linear = (LinearLayout) text_linear.getChildAt(cnt - 1);
                    VanishingWordLayout layout = is_last == true ? getLastVipLayout(vipWord) : getVipLayout(vipWord);
                    linear.addView(layout);
                } else {
                    LinearLayout linear = new LinearLayout(mContext);
                    linear.setOrientation(LinearLayout.HORIZONTAL);
                    VanishingWordLayout layout = is_last == true ? getLastVipLayout(vipWord) : getVipLayout(vipWord);
                    linear.addView(layout);
                    text_linear.addView(linear);
                    beforIndex = cur_len - txt.length();
                    if (beforNewLine) {
                        beforNewLine = false;
                    }
                }
            }
        }
    }

    private VanishingWordLayout getVipLayout(VipWord vipWord) {
        VanishingWordLayout result = vipWord.getWordLayout();

        result.setPadding(5, 0, 0, 0);

        if (curStep != 0 && vipWord.getIndex() != 0 && curStep >= vipWord.getIndex()) {//0이아니고 1,2일때
            result.setLineVisual(View.VISIBLE);
            result.startAnimation();
        }

        return result;
    }

    private VanishingWordLayout getLastVipLayout(VipWord vipWord) {
        VanishingWordLayout result = vipWord.getWordLayout();

        result.setPadding(5, 0, 0, 0);

        if (curStep != 0 && vipWord.getIndex() != 0 && curStep >= vipWord.getIndex()) {//0이아니고 1,2일때
            result.setLineVisual(View.VISIBLE);
            result.setLineColor(Color.RED);
            result.setTextColor(Color.RED);
        }

        return result;
    }

    /**
     * /** Handler
     */
    private Handler recordHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null == mContext)
                return;

            switch (msg.what) {

                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (null == mWavRecorder)
                        return;

                    if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END)
                        recordEnd();

                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    /**
     * 스킵은 나중에 될때
     */
    private void skipNext() {
        /*mListen.setImageDrawable(getResources().getDrawable(R.drawable.selector_headset_btn_bg));
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_nor));
        mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.selector_echo_btn_bg));*/

        mListen.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_listen_d));
        mRecord.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_record_d));
        mRecordPlay.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_record_d));

        if (mIsRecording) {
            mWavRecorder.stopRecording();
            mIsRecording = false;
        }

        if (null != mRecordPlayer && mRecordPlayer.isPlaying()) {
            mRecordPlayer.stop();
            mRecordPlayer.release();
            mRecordPlayer = null;

        }

        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }

        curStep++;
        stepNext();

    }

    /**
     * vip 문장에서 태그있는 내용만 배열에 담음
     *
     * @return
     */
    private String[] tagVip(String sentence) {
        List<String> result = new ArrayList<String>();
        String[] temp = null;
        temp = sentence.split(" ");
        for (String str : temp) {
            int index = 0;
            if (str.contains("<u1>") || str.contains("</u1>") || str.contains("<u3>") || str.contains("<u2>") || str.contains("</u2>") || str.contains("</u3>")) {
                result.add(str);
            }
        }
        return result.toArray(new String[result.size()]);

    }

}
