package com.yoons.fsb.student.ui.sec.study;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.sec.PartData;
import com.yoons.fsb.student.ui.sec.base.SecBaseStudyActivity;
import com.yoons.fsb.student.ui.textview.AutofitHelper;
import com.yoons.fsb.student.ui.textview.AutofitTextView;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;

import java.util.ArrayList;

/**
 * 단어시험 화면
 *
 * @author jaek
 */
public class SecWrongActivity extends SecBaseStudyActivity implements OnClickListener, OnCompletionListener, OnSeekCompleteListener {

    private LinearLayout mAnswer1 = null, mAnswer2 = null, mAnswer3 = null, mAnswer4 = null;

    private TextView mWrongStudyCountView = null, txt_cnt;
    private AutofitTextView txtWord = null, txtMean = null;

    private Button mNextStatus = null;
    private ArrayList<PartData> cur_part_list = null;
    private ArrayList<PartData> part1_list = new ArrayList<PartData>();
    private ArrayList<PartData> part2_list = new ArrayList<PartData>();
    private int curPart = ServiceCommon.PART_GB_PART1;
    private LinearLayout layout_bottom = null;
    private TextView tBookName = null, tCategory = null, tCategory2 = null;
    //	private boolean mOnclick=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CommonUtil.isCenter()) // Igse
            setContentView(R.layout.igse_sec_understand);
        else // 숲
            setContentView(R.layout.f_sec_understand);

        for (PartData data1 : mStudyData.getPart1_list()) {

            if (!data1.is_result()) {
                part1_list.add(data1);
            }
        }
        for (PartData data2 : mStudyData.getPart2_list()) {

            //part2에서 틀리고 type1인거
            if (!data2.is_result() && data2.getQuestion_type().equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_1)) {
                part2_list.add(data2);
            }
        }

        if (part1_list.size() == 0 && part2_list.size() != 0) {//part1이없을때
            curPart = ServiceCommon.PART_GB_PART2;
            cur_part_list = part2_list;
        } else if (part1_list.size() == 0 && part2_list.size() == 0) { //둘다 0일땐 패스
            next();
            finish();
            return;
        } else {
            cur_part_list = part1_list;
        }

        setWidget();

        // TitleView
        //tBookName.setText(mStudyData.mProductName);
        tCategory.setText(R.string.title_smart_test);
        tCategory2.setText(R.string.string_common_sec_sentence_answer);

        setPreferencesCallback();

        startStudy();
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub

        switch (view.getId()) {
            case R.id.next_status_btn:
                if (curPart == ServiceCommon.PART_GB_PART2 && mCurStudyIndex + 1 > cur_part_list.size()) {
                    next();
                    finish();
                } else {
                    nextStudy();
                }
                break;

            default:
                super.onClick(view);
                break;
        }
    }

    /**
     * layout을 설정함
     */
    private void setWidget() {
        layout_bottom = (LinearLayout) findViewById(R.id.layout_bottom);
        txtWord = (AutofitTextView) findViewById(R.id.word_study_wrong_question_text);
        mWrongStudyCountView = (TextView) findViewById(R.id.word_study_wrong_question_count_text);
        txtMean = (AutofitTextView) findViewById(R.id.word_study_wrong_correct_answer_text);
        mNextStatus = (Button) findViewById(R.id.next_status_btn);
        txt_cnt = (TextView) findViewById(R.id.txt_cnt);

        // TitleView
        tBookName = (TextView) findViewById(R.id.title_book_name);
        tCategory = (TextView) findViewById(R.id.title_category1);
        tCategory2 = (TextView) findViewById(R.id.title_category2);

        AutofitHelper.create(txtWord);
        AutofitHelper.create(txtMean);

        mNextStatus.setOnClickListener(this);

/*		if (curPart == ServiceCommon.PART_GB_PART1) {
			setTitlebarText(getString(R.string.string_sec_wrong_part1));
		} else {
			setTitlebarText(getString(R.string.string_sec_wrong_part2));
		}
*/
    }

    /**
     * Player를 설정함
     *
     * @param mp3FilePath 재생 파일 경로
     */
    private void setPlayer(String mp3FilePath) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(mp3FilePath);
            mMediaPlayer.setOnSeekCompleteListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.prepare();
			/*if (mp3FilePath.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
				mMediaPlayer.setVolume(0, 0);
			}*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Player를 종료함
     */
    private void playerEnd() {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // --- 틀린 단어 학습 --

    /**
     * 하나끝낫을때
     */
    private AnimationListener studyAniListener = new AnimationListener() {
        @Override
        public void onAnimationEnd(Animation animation) {
            if (null == mContext)
                return;

            //setWrongStudy();
            mCurPlayCount = 1;

            if (mMediaPlayer != null) {
                mMediaPlayer.start();
            } else {
                if (findViewById(R.id.word_study_wrong_layout).getVisibility() == View.VISIBLE) {
                }
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }
    };

    /**
     * 이해시작
     * <p>
     * 재생 파일 경로
     */
    private void startStudy() {
        //테스트 스킵
        if (mTestSkip) {
            mTestSkip = false;
            mCurStudyIndex = mStudyData.getPartList().size() - 1;
        }
        if (mCurStudyIndex < cur_part_list.size()) {
            layout_bottom.setVisibility(View.INVISIBLE);
            mNextStatus.setVisibility(View.INVISIBLE);
            txt_cnt.setText(mCurStudyIndex + 1 + " / " + cur_part_list.size());

            String word_sentence, mean;
            word_sentence = mStudyData.getEngWordSentence(mCurStudyIndex, curPart, cur_part_list);
            mean = mStudyData.getKorMeans(mCurStudyIndex, curPart, cur_part_list);

            txtWord.setText(word_sentence);
            txtMean.setText(mean);

            setPlayer(cur_part_list.get(mCurStudyIndex).getAudio_path());
            mCurPlayCount = 1;
            mMediaPlayer.start();
        } else {//끝일때
            layout_bottom.setVisibility(View.VISIBLE);
            mNextStatus.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 다음 틀린 단어 문제를 시작함
     */
    private void nextStudy() {
        mNextStatus.setVisibility(View.INVISIBLE);
        mCurStudyIndex++;
        playerEnd();
        if (mCurStudyIndex < cur_part_list.size()) {
            startStudy();
        } else if (curPart == ServiceCommon.PART_GB_PART1) {
            nextPart();
        } else {//끝일때
            layout_bottom.setVisibility(View.VISIBLE);
            mNextStatus.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 다음 파트로 넘기기/ 데이터가 없을때 part2로 넘긴다
     */
    private void nextPart() {
        curPart = ServiceCommon.PART_GB_PART2;
        cur_part_list = part2_list;
        mCurStudyIndex = 0;
		/*if (curPart == ServiceCommon.PART_GB_PART1) {
			setTitlebarText(getString(R.string.string_sec_wrong_part1));
		} else {
			setTitlebarText(getString(R.string.string_sec_wrong_part2));
		}*/
        startStudy();
    }

    /**
     * 틀린 단어 문제 2번 재생 완료시 2초 후 정답 공개 메세지를 전달함
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.pause();
        layout_bottom.setVisibility(View.VISIBLE);
        mNextStatus.setVisibility(View.VISIBLE);

    }

    /**
     * 1번 재생 완료 이후 처음으로 Seek 완료 되어 재생을 시작함
     */
    @Override
    public void onSeekComplete(MediaPlayer mp) {
        mMediaPlayer.start();
    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		/*if (!mIsStudyStart)
			return false;*/

        //keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mNextStatus.getVisibility() == View.VISIBLE)
                    mNextStatus.setPressed(true);
                break;

            default:
                return false;
        }
        return false;

    }

    /**
     * for bluetooth 7/24
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
		/*if (!mIsStudyStart)
			return false;*/

        //keyCode = getCorrectKeyCode(keyCode);

        switch (keyCode) {
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_NEXT:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_REWIND:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PREVIOUS:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_FAST_FORWARD:
                break;

            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
            case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
                if (mNextStatus.getVisibility() == View.VISIBLE) {
                    mNextStatus.setPressed(false);
                    if (curPart == ServiceCommon.PART_GB_PART2 && mCurStudyIndex >= cur_part_list.size() - 1) {
                        next();
                        finish();
                    } else {
                        nextStudy();
                    }
                }
                break;

            default:
                return false;
        }
        return false;

    }

}
