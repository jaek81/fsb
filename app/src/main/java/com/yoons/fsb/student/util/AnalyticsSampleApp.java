
package com.yoons.fsb.student.util;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

//GA적용
public class AnalyticsSampleApp extends Application {

    // The following line should be changed to include the correct property id.
    private static final String PROPERTY_ID = "UA-53194983-6";

    public static int GENERAL_TRACKER = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        Application app = (Application) this.getApplicationContext();
        app.registerActivityLifecycleCallbacks(PerfLifecycleCallbacks.getInstance());
        Fabric.with(this, new Crashlytics());


    }

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }



    //HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public AnalyticsSampleApp() {
        super();
    }

	/*public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID) : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker) : analytics.newTracker(R.xml.ecommerce_tracker);
			t.enableExceptionReporting(true);
			t.enableAdvertisingIdCollection(true);
			t.enableAutoActivityTracking(true);
			t.set("&uid", CommonUtil.getMacAddress());
			t.setAnonymizeIp(true);
			t.set("&cd1", CommonUtil.getMacAddress());
			mTrackers.put(trackerId, t);
		}
		return mTrackers.get(trackerId);
	}*/
}