package com.yoons.fsb.student.util;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import java.io.File;

/**
 * 천안블루시티 로그때문에 공통으로 우선 만들어둠
 * 나중에 운영 blob으로 이동할때 공통으로 빼아함
 * Created by 임효영 on 2018-07-06.
 */


public class BlobUtil {
    /**
     * blob스토리지 관련
     */
    private static final String storageConnectionString = "DefaultEndpointsProtocol=http;" + "AccountName=yoonsfiletest;"
            + "AccountKey=vU9lxg/CJsd53KR4DLlfmNQgGVt6mFsBBISUYSWv5H4NwoTlJeDhZvis9thr16Zu2ohmPmZo8e+5ixTNKHyqLw==";

    public static final String LOG_PATH = "log/";

    /**
     * lob으로 파일 업로드 한다.
     *
     * @param blobPath blob
     * @param file     업로드파일
     * @param isDel    파일 삭제 유무
     */
    public static void FileUpload(final String blobPath, final File file, final boolean isDel) {

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

                    // 스토리지 로 업로드//해당 스토리지는 http로 다운이 안되서 blob로 변경

                    CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

                    CloudBlobContainer container = blobClient.getContainerReference("blobbasicscontainer");
                    // 존재하지 않는 컨테이너를 만듭니다.
                    container.createIfNotExists();
                    // 사용 권한 개체 만들기
                    BlobContainerPermissions containerPermissions = new BlobContainerPermissions();
                    // 사용 권한 개체에 공용 액세스 포함
                    containerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
                    // 컨테이너에 대한 권한 설정
                    container.uploadPermissions(containerPermissions);

                    CloudBlockBlob blob = container.getBlockBlobReference(blobPath + file.getName());
                    blob.uploadFromFile(file.getPath());


                } catch (Exception e) {
                    e.printStackTrace();
                } finally {//파일삭제
                    if (isDel) {
                        try {
                            file.delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        thread.start();

    }


}
