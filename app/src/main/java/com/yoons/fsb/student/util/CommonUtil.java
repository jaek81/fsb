package com.yoons.fsb.student.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.RecData;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.service.NotiService;
import com.yoons.fsb.student.service.SlogService;

import org.kisa.SEEDCBC;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.yoons.fsb.student.ui.base.BaseActivity.mTypeface;
import static com.yoons.fsb.student.ui.base.BaseActivity.mTypeface2;

/**
 * util Class
 *
 * @author ejlee
 */
public class CommonUtil {
    public static final String TAG = "[CommonUtil]";

    /**
     * 내장 스토리지 path를 구해오는 함수
     *
     * @return 내장스토리지 path
     */
    public static String getInternalStoragePath() {
        String storagePath = "";
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            storagePath = Environment.getExternalStorageDirectory().getPath();
        }

        return storagePath;
    }

    /**
     * 파일의 크기를 반환한다.
     *
     * @param fileName 파일의 경로
     * @return 파일크기 (단위:byte)
     */
    public static long getFileSize(String fileName) {
        if (null == fileName || 0 >= fileName.length())
            return 0;

        File file = new File(fileName);

        // 파일이 있다.
        return (file.exists()) ? file.length() : 0;
    }

    /**
     * 설정한 경로의 크기를 반환한다.(하위 개체 포함)
     *
     * @param f 파일 또는 경로 정보
     * @return 파일크기 (단위:byte)
     */
    public static long getFileSize(File f) {

        long size = 0;

        if (null == f || !f.exists())
            return 0;

        if (f.isFile())
            return f.length();

        File[] arrFile = f.listFiles();
        for (File file : arrFile)
            size += (file.isDirectory()) ? getFileSize(file) : file.length();

        return size;
    }

    /**
     * 설정한 경로의 개체를 삭제한다.(하위 개체 포함)
     *
     * @param f           파일 또는 경로 정보
     * @param keepRootDir 폴더를 삭제하지 않고 유지하려면 true로 설정
     */
    public static void deleteDir(File f, boolean keepRootDir) {

        if (null == f || !f.exists())
            return;

        if (f.isFile()) {
            f.delete();
            return;
        }

        File[] arrFile = f.listFiles();
        for (File file : arrFile) {

            if (file.isDirectory()) {
                deleteDir(file, false);
                continue;
            }

            file.delete();
        }

        if (!keepRootDir)
            f.delete();
    }

    /**
     * 파일의 확장자를 문자열로 반환한다.
     *
     * @param name 파일명 또는 path
     * @return 문자열로된 확장자 반환
     */
    public static String getFileExt(String name) {

        if (null == name || 0 >= name.length())
            return null;

        String nameArray[] = name.split("\\.");
        if (1 >= nameArray.length)
            return null;

        return nameArray[nameArray.length - 1];
    }

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYYMMDDHHMMSS)
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentYYYYMMDDHHMMSS() {
        Calendar cal = Calendar.getInstance();
        String timeStr = String.format("%d%02d%02d%02d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

        return timeStr;
    }

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYY-MM-DD HH:MM:SS)
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentDateTime() {
        Calendar cal = Calendar.getInstance();
        String timeStr = String.format("%d-%02d-%02d %02d:%02d:%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

        return timeStr;
    }

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYY-MM-DD^HH:MM:SS)
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentDateTime2() {
        Calendar cal = Calendar.getInstance();
        String timeStr = String.format("%d-%02d-%02d+%02d:%02d:%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

        return timeStr;
    }

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYMMDDAMPMHHMM)<br>
     * 임시로 로그인에서 쓸 데이터 생성
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentYYYYMMDDAMPMHHMM() {
        Calendar cal = Calendar.getInstance();
        String timeStr = String.format("%d.%02d.%02d%s%02d:%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE) - 1, //하루뺌
                cal.get(Calendar.AM_PM) == 0 ? "AM" : "PM", cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE));

        return timeStr;
    }

    /**
     * 시각 포멧 변경(2013-06-17 18:20:51 --> 2013.06.17PM06:20)
     *
     * @param dbDate 원본 시각 정보
     * @return String 변경된 시각 정보
     */
    public static String dbDateToEasyLoginDate(String dbDate) {
        String loginDate = "";
        loginDate += dbDate.substring(0, 10).replace("-", ".");
        loginDate += HH24MIToAMPMHHMI(dbDate.substring(11, 16));

        return loginDate;
    }

    /**
     * 시간 포멧 변경(18:20 -> PM06:20)
     *
     * @param hhmm 원본 시각 정보
     * @return String 변경된 시각 정보
     */
    public static String HH24MIToAMPMHHMI(String hhmm) {
        Calendar cal = Calendar.getInstance();
        cal.set(0, 0, 0, Integer.valueOf(hhmm.substring(0, 2)), Integer.valueOf(hhmm.substring(3, 5)));

        return String.format("%s%02d:%02d", cal.get(Calendar.AM_PM) == 0 ? "AM" : "PM", cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE));
    }

    //	/**
    //	 * 날짜를 해당 포멧의 문자열로 반환함(YYYYMM)
    //	 * @return String 문자열 날짜정보
    //	 */
    /*
     * public static String getCurrentYYYYMM() { Calendar cal =
     * Calendar.getInstance(); return String.format("%d%02d",
     * cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1); }
     */

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYY-MM-DD)
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentYYYYMMDD2() {
        Calendar cal = Calendar.getInstance();
        return String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
    }

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYYMMDD)
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentYYYYMMDD() {
        Calendar cal = Calendar.getInstance();
        return String.format("%d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
    }

    /**
     * 날짜를 해당 포멧의 문자열로 반환함(YYYY-MM-DD)
     *
     * @return String 문자열 날짜정보
     */
    public static String getCurrentYYYYMMDD2(Calendar cal) {
        return String.format("%d-%02d-%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
    }

    /**
     * 올해의 주(주일)수를 반환함.
     *
     * @return int 몇번째 주인지 반환
     */
    public static int getWeekday() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 시간 포멧 변경(22:00 --> PM 10:00)
     *
     * @param time 원본 시각 정보
     * @return String 변경된 시각 정보
     */
    public static String HHMMToAMPMHHMM(String time) {
        int hour = Integer.valueOf(time.substring(0, 2));
        int minute = Integer.valueOf(time.substring(3, 5));

        Calendar cal = Calendar.getInstance();
        cal.set(0, 0, 0, hour, minute, 0);

        return String.format("%s %02d:%02d", cal.get(Calendar.AM_PM) == 0 ? "AM" : "PM", cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE));
    }

    /**
     * 폴더를 생성함.
     *
     * @param path 폴더 경로
     * @return true이면 정상적으로 처리됨
     */
    public static boolean makeDirs(String path) {

        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
            return false;

        File dir = new File(path);
        if (dir.exists() == false) {
            try {
                if (!dir.mkdirs()) {
                    dir.createNewFile();
                    Log.e(TAG, "hy mkdirs Failed : " + dir.getAbsolutePath());// mount/unmounnt 시 stack overflow 발생하여 제거
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        Log.e(TAG, "hy  " + path + "  폴더생성  " + dir.isDirectory());
        return true;
    }

    /**
     * ".nomedia" 파일을 생성함
     *
     * @param path 생성할 경로
     */
    public static void makeNomedia(String path) {
        CommonUtil.makeDirs(path);

        File file = new File(path + ".nomedia");
        FileOutputStream fos = null;

        try {
            if (!file.exists()) {
                file.createNewFile();
                fos = new FileOutputStream(file);
                fos.write(0x00);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 를 생성함
     * <p>
     * 생성할 경로
     */
    public static void makeAuthPassFile() {
        String path = "/data/data/app.sb.homesbadmin/";
        CommonUtil.makeDirs(path);

        String fileName = path + "pass" + getCurrentYYYYMMDDHHMMSS() + ".txt";
        File file = new File(fileName);
        FileOutputStream fos = null;

        try {
            if (!file.exists()) {
                file.createNewFile();
                fos = new FileOutputStream(file);
                fos.write(0x00);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 단말기 번호(MSISDN)
     *
     * @param context Context
     * @return String 전화번호를 반환함
     */
    public static String getPhoneNumber(Context context) {

        String phoneNumber = "";

        // MSISDN 추출
        TelephonyManager tpManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        phoneNumber = tpManager.getLine1Number();

        // 국가코드
        if (phoneNumber != null && phoneNumber.length() > 0) {
            phoneNumber = phoneNumber.replace("+82", "0");
        }

        if (phoneNumber == null || phoneNumber.equals("null")) {
            phoneNumber = "";
        }

        return phoneNumber;
    }

    /**
     * DeviceID를 구해옴.
     *
     * @param context Context
     * @return String 디바이스 아이디
     */
    public static String getDeviceId(Context context) {
        String deviceId = Preferences.getDeviceId(context);
        Log.e(TAG, "getDevice Preferences Id : " + deviceId);

        if (deviceId.length() == 0) {
            deviceId = getMacAddress();

            Preferences.setDeviceId(context, deviceId);
        }

        Log.e(TAG, "getDevice Id : " + deviceId);
        return deviceId;
    }

    /**
     * Mac Address 추출
     *
     * @param context
     *            Context
     * @return String Mac Address
     */
    /*public static String getMacAddress(Context context) {
        WifiManager wfMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		String macAddress = wfMgr.getConnectionInfo().getMacAddress();
	
		if (macAddress == null || macAddress.length() == 0) {
			// 비활성이면 활성 후 주소 획득
			boolean wifiStat = wfMgr.isWifiEnabled();
	
			wfMgr.setWifiEnabled(true);
	
			for (int i = 0; i < 15; i++) {
				macAddress = wfMgr.getConnectionInfo().getMacAddress();
				if (macAddress != null && macAddress.length() > 0) {
					break;
				}
				Log.e(TAG, "[getMacAddress] i = " + i);
	
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
				}
			}
	
			wfMgr.setWifiEnabled(wifiStat);
		}
		Log.e(TAG, "[getMacAddress] macAddress = " + macAddress);
	
		return macAddress;
	}*/

    /**
     * 시간 변환(millisec to minutes and seconds and milisec)
     *
     * @param millisec
     * @return String ex "MM:SS:ss"
     */
    public static String convMsecToMinSecMsec(int millisec) {
        double msec = millisec;
        StringBuffer buf = new StringBuffer();

        int minutes = (int) ((msec % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((msec % (1000 * 60 * 60)) % (1000 * 60)) / 1000);
        int milisec = (int) (msec % 1000);

        buf.append(String.format("%02d", minutes)).append(":").append(String.format("%02d", seconds)).append(":").append(String.format("%03d", milisec));

        return buf.toString();
    }

    /**
     * @param millisec
     * @return String ex "MM:SS"
     */
    public static String convMsecToMinSec(int millisec) {
        double msec = millisec;
        StringBuffer buf = new StringBuffer();

        int minutes = (int) ((msec % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((msec % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format("%02d", minutes)).append(":").append(String.format("%02d", seconds));

        return buf.toString();
    }

    /**
     * 전화걸기
     *
     * @param ctx    Context
     * @param number phone number
     * @return true 이면 정상 처리된 것이고 그렇지 않으면 오류가 발생된 것임
     */
    public static boolean makeCalling(Context ctx, String number) {
        if (null == ctx)
            return false;

        if (null != number && 0 < number.length()) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", number)));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent);
            return true;
        }

        return false;
    }

    /**
     * 현재 사용할 수 있는 네트워크가 있는지 확인(환경설정 값과 무관함) 환경 설정의 사용자 삭제, 추가시에 사용함.
     *
     * @param ctx
     * @return boolean true : 네트워크 사용가능
     *//*
     * public static boolean isAvailableNetworkRetry(Context ctx) { if (null
     * == ctx) return false;
     *
     * WifiManager wifiManager = (WifiManager)
     * ctx.getSystemService(Context.WIFI_SERVICE); boolean isAvailable =
     * isAvailableNetwork(ctx); Log.e(TAG, "isAvailableNetwork : " +
     * isAvailable);
     *
     * if(!isAvailable && wifiManager.isWifiEnabled()) { try {
     * Thread.sleep(5000); } catch (InterruptedException e) {
     * e.printStackTrace(); }
     *
     * isAvailable = isAvailableNetwork(ctx);
     *
     * Log.e(TAG, "isAvailableNetwork Retry : " + isAvailable); }
     *
     * return isAvailable; }
     */

    /**
     * 현재 사용할 수 있는 네트워크가 있는지 확인(환경설정 값과 무관함) 환경 설정의 사용자 삭제, 추가시에 사용함.
     *
     * @param ctx
     * @return boolean true : 네트워크 사용가능
     */
    public static boolean isAvailableNetwork(Context ctx) {
        if (null == ctx)
            return false;

        NetworkInfo ni = ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (ni == null) {
            // 활성 네트워크가 없을 경우
            Log.e(TAG, "NetworkInfo is NULL");
            return false;
        }

        return true;
    }

    /**
     * Download 가능 Network 확인(데이터 네트워크 사용 가능 여부 포함)
     *
     * @param ctx             Context
     * @param checkDataUpDown 데이터 네트워크 상에서 업로드/다운로드 가능 여부 포함
     * @return boolean true이면 네트워크 사용 가능
     */
    public static void isAvailableNetwork(final Context ctx, final boolean checkDataUpDown, final Handler handler, final int id) {
        if (null == ctx) {
            Log.e(TAG, "null == ctx");

            if (handler != null)
                handler.sendMessage(handler.obtainMessage(ServiceCommon.MSG_NETWORK_CHECK_DISCONNECTED, id, 0));
        }

        Thread thread = new Thread() {
            @Override
            public void run() {
                WifiManager wifiManager = (WifiManager) ctx.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                boolean isAvailable = isAvailableNetwork(ctx, checkDataUpDown);
                Log.e(TAG, "isAvailableNetwork : " + isAvailable);

                if (!isAvailable && wifiManager.isWifiEnabled()) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    isAvailable = isAvailableNetwork(ctx, checkDataUpDown);
                    Log.e(TAG, "isAvailableNetwork Retry : " + isAvailable);
                }

                if (handler != null)
                    handler.sendMessage(handler.obtainMessage(isAvailable ? ServiceCommon.MSG_NETWORK_CHECK_CONNECTED : ServiceCommon.MSG_NETWORK_CHECK_DISCONNECTED, id, 0));
            }
        };

        thread.start();
    }

    /**
     * Download 가능 Network 확인(데이터 네트워크 사용 가능 여부 포함)
     *
     * @param ctx             Context
     * @param checkDataUpDown 데이터 네트워크 상에서 업로드/다운로드 가능 여부 포함
     * @return boolean true이면 네트워크 사용 가능
     */
    public static boolean isAvailableNetwork(Context ctx, boolean checkDataUpDown) {

        if (null == ctx) {
            Log.e(TAG, "null == ctx");
            return false;
        }

        NetworkInfo ni = ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (ni == null) {
            // 활성 네트워크가 없을 경우
            Log.e(TAG, "NetworkInfo is NULL");
            return false;
        }

        Log.e(TAG, "ni.getType() : " + ni.getType() + " ni.getState() : " + ni.getState());
        int activeNetType = ni.getType();
        if (activeNetType == ConnectivityManager.TYPE_MOBILE) {

            // WCDMA 연결 체크
            if (ni.getState() == NetworkInfo.State.CONNECTED) {
                // FileInfo 3G망 가능여부에 따라 Download 가능
                if (Preferences.getUseDataNetwork(ctx)) {

                    if (checkDataUpDown) {
                        // 3G/LTE에서 다운로드 허용 여부 체크
                        if (!Preferences.getDownloadOnDataNetwork(ctx))
                            return false;

                        // 학습 중이면 다운로드 허용하지 않음.
                        if (Preferences.getStudying(ctx))
                            return false;
                    }

                    return true;
                }

                return false;
            }

        } else if (activeNetType == ConnectivityManager.TYPE_WIFI || activeNetType == ConnectivityManager.TYPE_ETHERNET) {

            //Log.e(TAG, "ni.getState() : " + ni.getState());
            // WIFI 연결 체크
            if (ni.getState() == NetworkInfo.State.CONNECTED) {

                // 학습 중이면 다운로드 허용하지 않음.
                if (checkDataUpDown && Preferences.getStudying(ctx))
                    return false;

                return true;
            }
        }

        return false;
    }

    /************************************************************************************************
     * KISACrypto encrypt/decrypt
     ************************************************************************************************/
    //static { System.loadLibrary("KISACrypto"); }

    private static final byte[] key = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10};
    private static final byte[] iv = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};

    /**
     * KISACrypto encrypt
     *
     * @param value 변환할 값
     * @return String 변환된 값
     */
    public static String SeedEncrypt(String value) {

        if (null == value || 0 >= value.length())
            return null;

        byte[] inText = value.getBytes();
        if (null == inText)
            return null;

        byte[] outText = new byte[inText.length * 2];

        SEEDCBC seed = new SEEDCBC();
        int outputLen = seed.CBC_ENCRYPT(key, iv, inText, 0, inText.length, outText, 0);
        if (0 >= outputLen) {
            Log.e(TAG, "Err!!! ENCRYPT");
            return null;
        }

        return Base64.encodeToString(outText, 0, outputLen, 0);
    }

    /**
     * KISACrypto decrypt
     *
     * @param value 변환할 값
     * @return String 변환된 값
     */
    public static String SeedDecrypt(String value) {

        if (null == value || 0 >= value.length())
            return null;

        byte[] inText = Base64.decode(value, 0);
        if (null == inText)
            return null;

        byte[] outText = new byte[inText.length * 2];
        SEEDCBC seed = new SEEDCBC();

        int outputLen = seed.CBC_DECRYPT(key, iv, inText, 0, inText.length, outText, 0);
        if (0 >= outputLen) {
            Log.e(TAG, "Err!!! DECRYPT");
            return null;
        }

        return new String(outText, 0, outputLen);
    }

    /**
     * return app's version name
     *
     * @param ctx Context
     * @return String
     */
    public static String getAppVersion(Context ctx) {
        String version = "0";

        if (null == ctx)
            return version;

        try {
            PackageInfo i = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            version = i.versionName;
        } catch (NameNotFoundException e) {
            version = "0";
        }

        return version;
    }

    /**
     * 단축아이콘을 추가한다. (홈스크린이 가득 차있는 상태라면 "현재 페이지에는 저장공간이 없습니다."라는 토스트 메시지 발생되며
     * 홈스크린에 새로운 페이지를 자동 생성하지 않음)
     *
     * @param ctx Context
     * @param cls Class<?>
     */
    public static void addShortcut(Context ctx, Class<?> cls) {

        // Add shortcut on homescreen
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClass(ctx, cls);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

        Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intent);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, ctx.getString(R.string.app_name));
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(ctx, R.drawable.ic_launcher));

        // 버전에 따라 다른 동작
        shortcut.putExtra("duplicate", false);

        ctx.sendBroadcast(shortcut);
    }

    /**
     * 경로를 포함하여 모든 하부 콘텐츠를 삭제한다.
     *
     * @param path 지울 디렉토리
     */
    public static boolean deleteDirectory(File path) {
        if (!path.exists()) {
            return false;
        }

        File[] files = path.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDirectory(file);
                continue;
            }

            file.delete();
        }

        return path.delete();
    }

    public static void deleteAllFiles(String path) {

        File file = new File(path);
        //폴더내 파일을 배열로 가져온다.
        File[] tempFile = file.listFiles();

        if (tempFile != null && tempFile.length > 0) {

            for (int i = 0; i < tempFile.length; i++) {

                if (tempFile[i].isFile()) {
                    tempFile[i].delete();
                } else {
                    //재귀함수
                    deleteAllFiles(tempFile[i].getPath());
                }
                tempFile[i].delete();

            }
            //file.delete();

        }

    }

    /**
     * 앱에 포함된 assets 리소스를 로컬 스토리지에 복사한다.
     *
     * @param context    Context
     * @param assetsFile 어셋 구분 정보
     * @param localFile  복사할 위치
     */
    public static void copyAssetsToSDCard(Context context, String assetsFile, String localFile) {
        AssetManager assetManager = context.getAssets();

        try {
            byte[] buffer = new byte[1024];
            InputStream inputStream = assetManager.open(assetsFile);

            OutputStream outputStream = new FileOutputStream(localFile);

            int read;
            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }

            inputStream.close();
            inputStream = null;
            outputStream.flush();
            outputStream.close();
            outputStream = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 패키지를 설치함.
     *
     * @param context  Context
     * @param filePath 패키지의 위치
     */
    public static void installLocalPackage(Context context, String filePath) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(filePath)), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * USIM 존재 여부 체크
     *
     * @param context Context
     * @return true이면 사용가능 유심 있음
     */
    public static boolean isUsimExist(Context context) {

        boolean exist = false;

        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null && tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT && !isNonUsimDevice(context)) {
            exist = false;
        } else {
            String mdn = getPhoneNumber(context);
            exist = (mdn != null && mdn.length() > 0) ? true : false;
        }
        return exist;
    }

    /**
     * USIM이 없는 CDMA 전용 Android 단말(LGT)
     *
     * @param context Context
     * @return true이면 USIM 미지원 단말임
     */
    public static boolean isNonUsimDevice(Context context) {

        PackageManager pkgMgr = context.getPackageManager();
        if (!pkgMgr.hasSystemFeature(PackageManager.FEATURE_TELEPHONY))
            return false;

        if (pkgMgr.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA) && !pkgMgr.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_GSM)) {
            return true;
        }

        return false;
    }

    /**
     * 빌드 시간 구해오는 함수
     *
     * @param context Context
     * @return 빌드시간 못구하면 빈 문자열
     */
    public static String getApplicationBuildTime(Context context) {
        String buildTime = "";

        try {
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            ZipFile zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("classes.dex");
            long time = ze.getTime();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            buildTime = sdf.format(new java.util.Date(time));
        } catch (Exception e) {

        }

        return buildTime;
    }

    /**
     * Bluetooth의 활성 여부를 확인하다.
     *
     * @return true이면 활성 상태임
     */
    public static boolean isBluetoothEnabled(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if (am == null)
            return false;

        return am.isBluetoothA2dpOn();
    }

    /**
     * 본 앱이 최상위에 활성화 되어 있는지를 확인한다.
     *
     * @param context Context
     * @return true이면 최상위에 있음을 의미함.
     */
    public static boolean isHSSBForeground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
        if (packageName.equals("com.yoons.fsb.student")) {
            return true;
        }

        return false;
    }

    /**
     * Q7 Device 여부 확인
     *
     * @return
     */
    public static boolean isQ7Device() {
        String device = "";

        try {
            device = URLEncoder.encode(Build.MODEL, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (device.contains("Q7") || device.contains("VT10") || device.contains("VT20") || device.contains("Y20") || device.contains("ME173X") || device.contains("SM-T335K") || device.contains("SM-T330"))
            return true;

        return false;
    }

    /**
     * VT10 Device 여부 확인
     *
     * @return
     */
    public static boolean isVT10Device() {
        String device = "";

        try {
            device = URLEncoder.encode(Build.MODEL, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (device.contains("VT10"))
            return true;

        return false;
    }

    /**
     * YS20 Device 여부 확인
     *
     * @return
     */
    public static boolean isYS20Device() {
        String device = "";

        try {
            device = URLEncoder.encode(Build.MODEL, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (device.contains("YS20") || device.contains("Y20") || device.contains("YS25"))
            return true;

        return false;
    }

    /**
     * SMT330 Device 여부 확인
     *
     * @return
     */
    public static boolean isSMT330Device() {
        String device = "";

        try {
            device = URLEncoder.encode(Build.MODEL, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (device.contains("SM-T330"))
            return true;

        return false;
    }

    /**
     * SMT335k Device 여부 확인
     *
     * @return
     */
    public static boolean isSMT335kDevice() {
        String device = "";

        try {
            device = URLEncoder.encode(Build.MODEL, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (device.contains("SM-T335K"))
            return true;

        return false;
    }

    /**
     * 파일을 복사함.
     *
     * @param inFile  원본 파일의 정보
     * @param outFile 새로 만들어질 파일의 정보
     */
    public static void copyFile(String inFile, String outFile) {
        FileInputStream finstream = null;
        FileOutputStream foutstream = null;
        FileChannel fcin = null, fcout = null;

        try {
            finstream = new FileInputStream(inFile);
            foutstream = new FileOutputStream(outFile);

            fcin = finstream.getChannel();
            fcout = foutstream.getChannel();

            long size = fcin.size();
            fcin.transferTo(0, size, fcout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fcin)
                    fcin.close();

                if (null != fcout)
                    fcout.close();

                if (null != foutstream)
                    foutstream.close();

                if (null != finstream)
                    finstream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * zip파일 압축 풀기
     *
     * @param filePath
     * @param target
     * @return 성공 여부
     * @throws Exception
     */
    public static boolean unzip(String filePath, String target) throws Exception {
        int BUFFER = 2048;
        boolean isUnZipSuccess = false;
        List<String> zipFiles = new ArrayList<String>();
        File sourceZipFile = new File(filePath);
        File unzipDestinationDirectory = new File(target);
        unzipDestinationDirectory.mkdir();

        ZipFile zipFile;
        // Open Zip file for reading

        if (sourceZipFile.isFile()) {
            zipFile = new ZipFile(sourceZipFile, ZipFile.OPEN_READ);

            // Create an enumeration of the entries in the zip file
            Enumeration<? extends ZipEntry> zipFileEntries = zipFile.entries();

            // Process each entry
            while (zipFileEntries.hasMoreElements()) {
                // grab a zip file entry
                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();

                String currentEntry = entry.getName();

                // 신규문항
                // zip 내부 경로의 path중 \ 인경우 ->  / 로 변환
                currentEntry = currentEntry.replace("\\\\", "/");
                currentEntry = currentEntry.replace("\\", "/");

                File destFile = new File(unzipDestinationDirectory, currentEntry);

                if (currentEntry.endsWith(".zip")) {
                    zipFiles.add(destFile.getAbsolutePath());
                }

                // grab file's parent directory structure
                File destinationParent = destFile.getParentFile();

                // create the parent directory structure if needed
                destinationParent.mkdirs();

                try {
                    // extract file if not a directory
                    if (!entry.isDirectory()) {
                        BufferedInputStream is = new BufferedInputStream(zipFile.getInputStream(entry));
                        int currentByte;
                        // establish buffer for writing file
                        byte data[] = new byte[BUFFER];

                        // write the current file to disk
                        FileOutputStream fos = new FileOutputStream(destFile);
                        BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);

                        // read and write until last byte is encountered
                        while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                            dest.write(data, 0, currentByte);
                            isUnZipSuccess = true;
                        }
                        dest.flush();
                        dest.close();
                        is.close();
                        //fileCount++;
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
            zipFile.close();

            for (Iterator<String> iter = zipFiles.iterator(); iter.hasNext(); ) {
                String zipName = iter.next();
                unzip(zipName, target + File.separatorChar + zipName.substring(0, zipName.lastIndexOf(".zip")));
            }
        }
        return isUnZipSuccess;

    }

    /**
     * 버전 값을 구해옴(스트링 비교로 버전이 비교 되지 않을 수 있으므로 int로 바꿔서 값을 만듬)
     *
     * @param version : version string
     * @return 버전 값
     */
    public static int getVersionValue(String version) {
        String[] token = version.split("[.]");

        if (token.length != 3) {
            return -1;
        }

        return Integer.valueOf(token[0]) * 10000 + Integer.valueOf(token[1]) * 100 + Integer.valueOf(token[2]);
    }

    /**
     * 지원 단말 체크 assets 의 device의 파일의 device리스트를 보고 비교함.
     *
     * @param context
     * @return 지원되는지 여부
     */
    public static boolean deviceModelValidation(Context context) {
        BufferedReader in = null;
        List<String> deviceList = new ArrayList<String>();
        String device = "";

        try {
            device = URLEncoder.encode(Build.MODEL, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }

        try {
            in = new BufferedReader(new InputStreamReader(context.getAssets().open("device")));

            String line;

            while ((line = in.readLine()) != null) {
                deviceList.add(line);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        boolean isOnlyDeviceVersion = false;

        for (int i = 0; i < deviceList.size(); i++) {
            if (deviceList.get(i).equals("Q7")) {
                isOnlyDeviceVersion = true;
                break;
            }
        }

        Log.e(TAG, "isOnlyDeviceVersion : " + isOnlyDeviceVersion);

        boolean isSupport = false;

        if (isOnlyDeviceVersion) {// 전용단말 버전일 때
            if (device.equals("Q7")) {// Q7모델들일때
                if (Build.DISPLAY.equals("Q7_yoon.KOR.ANDROID4.2.2-20130817")) {
                    isSupport = true;
                }
            } else if (device.equals("Q711L1BC_OGS")) {
                if (Build.DISPLAY.equals("A31S_Q71_Q711L1BC_OGS.20130717")) {
                    isSupport = true;
                }
            } else {
                for (int i = 0; i < deviceList.size(); i++) {
                    Log.e(TAG, "deviceList : " + i + " " + deviceList.get(i));
                    if (device.equals(deviceList.get(i))) {
                        isSupport = true;
                        break;
                    }
                }
            }
        } else {

            if (deviceList.size() == 0) {
                Log.e(TAG, "deviceList size is 0, all device support");
                isSupport = true;
            } else {
                for (int i = 0; i < deviceList.size(); i++) {
                    Log.e(TAG, "deviceList : " + i + " " + deviceList.get(i));
                    if (device.contains(deviceList.get(i))) {
                        isSupport = true;
                        break;
                    }
                }
            }
        }

        return isSupport;
    }

    /**
     * 단말기의 서버 시간을 동기화 함.
     *
     * @throws ParseException
     */
    public static void syncServerTime(String serverTime, Context context) throws ParseException {

        String transTime = transTime(serverTime);
        Log.k("wusi12", "Trans Time : " + transTime);
        if (isQ7Device()) {
            if (isVT10Device() || isYS20Device()) {
                setVT10ServerTimeSync(transTime);
            } else {
                autoSystemTime(context);
            }
        }
        /*
         * if (isQ7Device()) { if (isVT10Device() || isY20Device()) {
         * setVT10ServerTimeSync(transTime); } else if (isSMT335kDevice() ||
         * isSMT330Device()) { autoSystemTime(context); } else {
         * setQ7ServerTimeSync(transTime); } }
         */
    }

    private static String transTime(String format1) throws ParseException {
        java.text.SimpleDateFormat formatter1 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.KOREA);
        java.text.SimpleDateFormat formatter2 = new java.text.SimpleDateFormat("yyyyMMdd.HHmmss", java.util.Locale.KOREA);
        Date date1 = null;
        date1 = formatter1.parse(format1);
        return formatter2.format(date1);
    }

    public static boolean compareLocalTime(String serverTime) {
        String localTime = getCurrentDateTime();
        Date serverDate = null;
        Date localDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            serverDate = sdf.parse(serverTime);
            localDate = sdf.parse(localTime);

            Log.e(TAG, "timeCheck serverTime : " + serverDate.getTime() / 1000 + " localTime : " + localDate.getTime() / 1000);

            if (Math.abs(serverDate.getTime() - localDate.getTime()) / 1000 > 300) {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
        return true;
    }

    public static void setQ7ServerTimeSync(String date) {

        try {
            Runtime.getRuntime().exec(new String[]{"su", "-c", "date", "-s", date});
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void setVT10ServerTimeSync(String date) {

        try {
            Runtime.getRuntime().exec(new String[]{"su", "-c", "date -s " + date});
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void autoSystemTime(Context context) {
        Settings.System.putInt(context.getContentResolver(), Settings.System.AUTO_TIME, 1);
    }

    /**
     * Package 설치 체크 Package Name으로 설치 여부 검사
     *
     * @param context
     * @param packageName
     * @return Package 설치 여부
     */
    public static boolean checkInstalledPackage(Context context, String packageName) {
        try {
            PackageInfo pkgInfo = context.getPackageManager().getPackageInfo(packageName, 0);

        } catch (NameNotFoundException e) {
            Log.e(TAG, packageName + " Not Installed");
            return false;
        }
        return true;
    }

    public static boolean isFile(String path) {
        File d = new File(path);

        if (d.isFile()) {
            return true;
        }
        return false;
    }

    public static void startPackage(Context mContext, final String _packageName, String mAgencyNo, String mTeacherNo, int mCustomerNo, String isCenter) {
        try {
            Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(_packageName);
            String mainActivity = intent.getComponent().getClassName();
            String packageString = intent.getComponent().getPackageName();
            String acode = mAgencyNo;
            Intent launchIntent;
            launchIntent = new Intent(Intent.ACTION_MAIN);
            launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            launchIntent.setComponent(new ComponentName(packageString, mainActivity));
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

//            if (!acode.equals("")) {
//                if (acode.length() > 4) {
//                    acode = acode.substring(0, 4);
//                }
//                launchIntent.putExtra("AgencyNo", acode);
//            }
//            if (mCustomerNo != 0) {
//                launchIntent.putExtra(Preferences.KEY_CUSTOMERNO, mCustomerNo);
//            }
//            launchIntent.putExtra(Preferences.KEY_TEACHERNO, mTeacherNo);

            if (_packageName.equals(ServiceCommon.FOURSKILL_PACKAGE)) {
                launchIntent.putExtra("AgencyNo", mAgencyNo);
                launchIntent.putExtra("TeacherNo", mTeacherNo);
                launchIntent.putExtra(ServiceCommon.KEY_LMS_STATUS, Preferences.getLmsStatus(mContext));
                launchIntent.putExtra(ServiceCommon.KEY_CENTER, Preferences.getCenter(mContext));
            }

            if (ServiceCommon.DEV_MODE && ServiceCommon.DEV_SERVER_URL != null && ServiceCommon.DEV_SERVER_URL != "") {
                launchIntent.putExtra(ServiceCommon.KEY_TEST_URL, ServiceCommon.DEV_SERVER_URL);
            }
            mContext.startActivity(launchIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, "해당 앱이 설치되어 있는지 확인해주세요.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void conNotiService(Context mContext, boolean bol) {
        Intent intent = new Intent(mContext, NotiService.class);
        if (intent != null) {
            if (bol) {
                mContext.startService(intent);
            } else {
                mContext.stopService(intent);
            }
        }
    }

    public static void conSlogService(Context mContext, boolean bol) {
        Intent intent = new Intent(mContext, SlogService.class);
        if (intent != null) {
            if (bol) {
                mContext.startService(intent);
            } else {
                if (isServiceRunningCheck(mContext, SlogService.class.getName())) {
                    mContext.stopService(intent);
                }
            }
        }
    }

    /**
     * 업로드 폴더의 Rtag 파일 찾기
     *
     * @return
     *//*
     * private static ArrayList<File> getMp3MeargeFiles() {
     *
     * ArrayList<File> result = new ArrayList<File>();
     *
     * File f = new File(ServiceCommon.EXAM_RECORD_UPLOAD_PATH); if
     * (!f.exists()) { return result; } File[] arrFile = f.listFiles();
     *
     * for (File mFile : arrFile) { if (mFile.getName().contains("r_tag")) {
     * result.add(mFile); } } return result;
     *
     * }
     */
    public static void mergeMp3Files(ArrayList<RecData> recList) throws IOException {
        FileOutputStream outputStream = null;

        long start = System.currentTimeMillis();
        boolean result = false;
        int startTagTime = 0;
        int endTagTime = 0;
        int recTotalTime = 0;
        String filePath = recList.get(0).file_path.replace("S1", "R1").replace("G1", "R1");
        String recType = "";
        int studentBookId = 0;
        int bookDetailId = 0;
        File output;
        outputStream = new FileOutputStream(filePath);
        try {

            for (RecData data : recList) {

                if (startTagTime == 0 && !(data.start_merge_time == 0)) {
                    startTagTime = data.start_merge_time;
                }
                if (endTagTime == 0 && !(data.end_merge_time == 0)) {
                    endTagTime = data.end_merge_time;
                }
                if (filePath.equals("") && !data.file_path.equals("")) {
                    filePath = data.file_path.replace("S1", "R1").replace("G1", "R1");
                }
                if (recType.equals("") && !data.rec_type.equals("")) {//rectype이 _i _o 로 들어와서
                    recType = data.rec_type.replace("_i", "_io");
                }
                if (studentBookId == 0 && !(data.student_book_id == 0)) {
                    studentBookId = data.student_book_id;
                }
                if (bookDetailId == 0 && !(data.book_detail_id == 0)) {
                    bookDetailId = data.book_detail_id;
                }

                recTotalTime = recTotalTime + Integer.valueOf(data.rec_len);

                File input = new File(data.file_path);
                FileInputStream inputStream = null;

                try {
                    IOUtils.copy(inputStream = new FileInputStream(input), outputStream);
                } finally {
                    if (inputStream != null)
                        try {
                            inputStream.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            }
            if (recTotalTime != 0 && endTagTime == 0) {//구간반복에서 끝까지 안햇을경우
                endTagTime = startTagTime + recTotalTime;
            }

        } finally {
            if (outputStream != null)
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        deleteMergeFiles(recList);//조각 파일 삭제

        DatabaseUtil.getInstance().insertRecordDetailC(filePath, recType, startTagTime, endTagTime, recTotalTime, recList.get(0).rep_cnt, 0, bookDetailId, studentBookId, endTagTime, false);

        long end = System.currentTimeMillis();
        Log.e(TAG, "mergeMp3Files 실행 시간 : " + (end - start) / 1000.0);
    }

    /*
     * public static boolean meargeMp3(ArrayList<RecData> recList) { long start
     * = System.currentTimeMillis(); boolean result = false; int startTagTime =
     * 0; int endTagTime = 0; int recTotalTime = 0; String filePath = ""; String
     * recType = ""; int studentBookId = 0; int bookDetailId = 0;
     *
     * try {
     *
     * Vector<InputStream> inputStreams = new Vector<InputStream>();
     *
     * for (File mFile : getMp3MeargeFiles()) {
     *
     * FileInputStream tempStream = new FileInputStream(mFile);
     *
     * inputStreams.add(tempStream); tempStream.close();
     *
     * }
     *
     *
     * //이거 나중에 어떻게 할지 정리 for (RecData data : recList) { FileInputStream
     * tempStream = new FileInputStream(data.file_path); if (startTagTime == 0
     * && !(data.start_merge_time == 0)) { startTagTime = data.start_merge_time;
     * } if (startTagTime == 0 && !(data.end_merge_time == 0)) { endTagTime =
     * data.end_merge_time; } if (filePath.equals("") &&
     * !data.file_path.equals("")) { filePath = data.file_path.replace("S1",
     * "R1"); } if (recType.equals("") && !data.rec_type.equals("")) { recType =
     * data.rec_type; } if (studentBookId == 0 && !(data.student_book_id == 0))
     * { studentBookId = data.student_book_id; } if (bookDetailId == 0 &&
     * !(data.book_detail_id == 0)) { bookDetailId = data.book_detail_id; }
     * recTotalTime = recTotalTime + data.rec_len; inputStreams.add(tempStream);
     * } Enumeration<InputStream> enu = inputStreams.elements(); //
     * SequenceInputStream sis = new SequenceInputStream(enu);
     *
     * SequenceInputStream sis = new SequenceInputStream(enu); int temp;
     * FileOutputStream fostream = new FileOutputStream(filePath); while ((temp
     * = sis.read()) != -1) { fostream.write(temp); } fostream.close();
     *
     * DatabaseUtil.getInstance().insertRecordDetailC(filePath, recType,
     * startTagTime, endTagTime, recTotalTime, recList.get(0).rep_cnt, 0,
     * bookDetailId, studentBookId, endTagTime);
     *
     * } catch (IOException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } long end = System.currentTimeMillis(); Log.e(TAG,
     * "mergeMp3Files 실행 시간 : " + (end - start) / 1000.0); return result;
     *
     * }
     */

    /*
     * 이더넷 Get the STB MacAddress
     *
     * public static String getMacAddress() { try { return
     * loadFileAsString("/sys/class/net/eth0/address").toUpperCase().substring(
     * 0, 17); } catch (IOException e) { e.printStackTrace(); return null; } }
     *
     *
     * Load file content to String
     *
     * public static String loadFileAsString(String filePath) throws
     * java.io.IOException { StringBuffer fileData = new StringBuffer(1000);
     * BufferedReader reader = new BufferedReader(new FileReader(filePath));
     * char[] buf = new char[1024]; int numRead = 0; while ((numRead =
     * reader.read(buf)) != -1) { String readData = String.valueOf(buf, 0,
     * numRead); fileData.append(readData); } reader.close(); return
     * fileData.toString(); }
     */

    public static boolean isServiceRunningCheck(Context mContext, String mServiceName) {
        try {
            ActivityManager manager = (ActivityManager) mContext.getSystemService(Activity.ACTIVITY_SERVICE);
            if (manager != null) {
                for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                    if (mServiceName.equals(service.service.getClassName())) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void deleteMergeFiles(ArrayList<RecData> recList) {

        for (RecData data : recList) {
            File f = new File(data.file_path);
            if (f.isFile()) {
                f.delete();
            }
        }
    }

    public static String getMacAddress() {
        //return "70:3C:03:20:00:4D";
        // return "70:3C:03:20:00:13";
        //if (!isRunningTest()) {
        try {
            return loadFileAsString("/sys/class/net/eth0/address").toUpperCase().substring(0, 17);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }

    public static String loadFileAsString(String filePath) throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

/*	public static String getYYYYMMDDHHMMSS() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		return formatter.format(new java.util.Date());

	}*/

    /**
     * 한국어인지
     *
     * @param str
     * @return
     */
    public static boolean isKorean(String str) {

        return str.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*");
    }

    /**
     * 숫자체크
     *
     * @param s
     * @return
     */
    public static boolean isNum(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String curMonth() {
        Calendar oCalendar = Calendar.getInstance(); // 현재 날짜/시간 등의 각종 정보 얻기

        return String.valueOf((oCalendar.get(Calendar.MONTH) + 1));

    }

    /**
     * 음성녹취 마지막 버튼 터치음 제거
     *
     * @param path 적용될 음성녹취 파일 경로
     * @param temp 원래 경로에 적용되기 전 임시 경로
     */
    public static void removeEndNoise(String path, String temp) {
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        FileChannel fcin = null, fcout = null;
        long size = 0;
        int byteMP3 = 500; //0.1초 단위
        int byteWAVE = 3300; //0.1초 단위
        int count = 2;

        try {
            inputStream = new FileInputStream(path);
            outputStream = new FileOutputStream(temp);
            fcin = inputStream.getChannel();
            fcout = outputStream.getChannel();

            if (path.contains(ServiceCommon.MP3_FILE_TAIL)) {
                size = fcin.size() - byteMP3 * count;
            } else {
                size = fcin.size() - byteWAVE * count;
            }

            fcin.transferTo(0, size, fcout);

            File fileRecord = new File(path);
            if (fileRecord.exists()) {
                fileRecord.delete();
            }

            CommonUtil.copyFile(temp, path);

            File tempRecord = new File(temp);
            if (tempRecord.exists()) {
                tempRecord.delete();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fcin.close();
                fcout.close();
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized boolean isRunningTest() {
        boolean istest;
        try {
            Class.forName("android.support.test.espresso.Espresso");
            istest = true;
        } catch (ClassNotFoundException e) {
            istest = false;
        }
        return istest;
    }

    /**
     * @return true:중앙화,false:로컬모드
     */

    public static boolean isCenter() {
        boolean result = false;
        if (ServiceCommon.SERVER_MODE == ServiceCommon.CENTER_MODE) {
            result = true;
        }
        return result;
    }

    public static void setGlobalFont(Context context, View view) {
        if (view != null) {
            if (view instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for (int i = 0; i < vgCnt; i++) {
                    View v = vg.getChildAt(i);
                    if (v instanceof TextView) {
                        Typeface t = ((TextView) v).getTypeface();
                        if (t != null && t.getStyle() == Typeface.BOLD) {
                            ((TextView) v).setTypeface(mTypeface, Typeface.NORMAL);
                        } else {
                            ((TextView) v).setTypeface(mTypeface2, Typeface.NORMAL);
                        }
                    }

                    if (v instanceof ViewGroup) {
                        setGlobalFont(context, v);
                    }
                }
            }
        }
    }

    private static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return gcd(b, a % b);
        }
    }

    public static void setSizeLayout(Context ctx, final View view) {
        int dpi = ctx.getResources().getDisplayMetrics().densityDpi;
        int width = ctx.getResources().getDisplayMetrics().widthPixels;
        int height = ctx.getResources().getDisplayMetrics().heightPixels;
        float screenWidth = width / ctx.getResources().getDisplayMetrics().xdpi;
        float screenHeight = height / ctx.getResources().getDisplayMetrics().ydpi;
        double x = Math.pow(screenWidth, 2);
        double y = Math.pow(screenHeight, 2);
        double screenInches = Math.sqrt(x + y);
        android.util.Log.i("", "screen inch => " + screenInches);
        android.util.Log.i("", "dpi => " + dpi);

        int div = gcd(width, height);
        int widthRate = width / div;
        int heightRate = height / div;
        //Log.i("", "div => " + div + " widthRate => " + widthRate + " heightRate => " + heightRate);

        if (view != null) {
            if (view instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for (int i = 0; i < vgCnt; i++) {
                    View v = vg.getChildAt(i);
                    ViewGroup.LayoutParams vp = (ViewGroup.LayoutParams) v.getLayoutParams();
                    ViewGroup.MarginLayoutParams mp = (ViewGroup.MarginLayoutParams) v.getLayoutParams();

                    if (vp == null) {
                        continue;
                    }

                    float scale = 1.5f;

                    if ((ctx.getResources().getConfiguration().screenLayout
                            & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {

                        if (v instanceof LinearLayout || v instanceof FrameLayout || v instanceof RelativeLayout
                                || v instanceof TextView || v instanceof ImageView) {

                            if (dpi >= 320) {
                                if (screenInches >= 9.6) {
                                    scale = 1.6f;
                                } else if (screenInches <= 8.5) {
                                    scale = 1.3f;
                                }
                            } else if (dpi >= 240) {
                                if (screenInches >= 9.7) {
                                    scale = 2f;
                                }
                            }

                            if (widthRate == 4 && heightRate == 3 && dpi == 360) {
                                scale = 1.4f;
                            }

                            if (widthRate == 8 && heightRate == 5 && dpi == 280) {
                                scale = 1.7f;
                            }
                        }

                    } else {

                        if (v instanceof LinearLayout || v instanceof FrameLayout || v instanceof RelativeLayout
                                || v instanceof TextView || v instanceof ImageView) {

                            scale = 1f;

                            // dpi별 최소 인치 처리..
                            if (checkSoftkey(ctx) && ((dpi >= 640 && screenInches < 5.43) || (dpi >= 320 && screenInches <= 4.7))) {
                                scale = 0.92f;
                            }

                            // 단말 디스플레이 설정 화면 작게/크게 대응..
                            if (dpi == 720 || dpi == 540 || dpi == 360 || dpi == 270 || dpi == 180) {
                                scale = 0.9f;
                            } else if (/*dpi == 560 || dpi == 420 ||*/dpi == 280 || dpi == 210 || dpi == 140) {
                                scale = 1.14f;
                            }

                            android.util.Log.i("", "scale => " + scale);
                        }
                    }

                    if (v instanceof TextView) {
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_DIP, convertDp(((TextView) v).getTextSize() * scale, ctx));
                    }

                    if (vp.width > 0) {
                        vp.width *= scale;
                    }

                    if (vp.height > 0) {
                        vp.height *= scale;
                    }

                    if (mp.topMargin >= 0) {
                        mp.topMargin *= scale;
                    }

                    if (mp.leftMargin >= 0) {
                        mp.leftMargin *= scale;
                    }

                    if (mp.rightMargin >= 0) {
                        mp.rightMargin *= scale;
                    }

                    if (mp.bottomMargin >= 0) {
                        mp.bottomMargin *= scale;
                    }

                    v.setLayoutParams(vp);

                    if (v instanceof ViewGroup) {
                        setSizeLayout(ctx, v);
                    }
                }
            }
        }
    }

    public static int convertDp(float px, Context context) {
        return (int) (px / context.getResources().getDisplayMetrics().density);
    }

    public static int convertPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    /**
     * 소프트키 존재 유부
     */
    public static boolean checkSoftkey(Context context) {
        //메뉴버튼 유무
        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        //뒤로가기 버튼 유무
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

        if (!hasMenuKey && !hasBackKey) { // lg폰 소프트키일 경우
            Log.i("phoneType", "softkey");
            return true;
        } else { // 삼성폰 등.. 메뉴 버튼, 뒤로가기 버튼 존재
            Log.i("phoneType", "Not softkey");
            return false;
        }
    }
}
