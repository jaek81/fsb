package com.yoons.fsb.student.util;

import android.content.Context;
import android.net.Uri;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.OneWeekData;
import com.yoons.fsb.student.db.DatabaseData.DownloadStudyUnit;
import com.yoons.fsb.student.db.DatabaseUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * 교재의 콘텐츠 파일 관리 util
 * 
 * @author ejlee
 *
 */
public class ContentsUtil {
	private static final String TAG = "[ContentsUtil]";
	public static String CONTENT_URL = ServiceCommon.CDN_SERVER_URL + "study/%d/%d/%d_%d_%d";		// 학습 콘텐츠 URL
	public static String PAGE_URL = ServiceCommon.CDN_SERVER_URL + "page/%d/%d/";		// 페이지 콘텐츠 URL

	public static final String WORD_URL = ServiceCommon.CDN_SERVER_URL + "word/"; // 단어
																					// url
	public static final String CONTENT_LOCAL_PATH = ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d/"; // 로컬
																									// 콘텐츠
																									// 저장
																									// path
	public static final String CONTENT_LOCAL_FILE_PREFIX = ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d/%d_%d_%d"; // 로컬
																													// 콘텐츠
																													// 파일명
																													// prefix
	public static final String WORD_LOCAL_PATH = ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d/"; // 단어
																									// 저장
																									// path
	public static final String PAGE_LOCAL_PATH = ServiceCommon.THUMBNAIL_PATH + "%d/%d/";		// 로컬 페이지 저장 path

	// 콘텐츠
	// URL
	public static final String CONTENT_LOCAL_POPUP_PATH = ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d/%d_%d_%d/"; // 로컬
																													// 콘텐츠
																													// 신규문항
																													// path
	public static final String CONTENT_LOCAL_POPUP_FILE_PREFIX = ServiceCommon.STUDY_CONTENT_PATH + "%d/%d/%d/%d_%d_%d"; // 로컬
																															// 콘텐츠
																															// 신규문항
																															// 파일명
																															// prefix

	public static final String TAG_POSTFIX = ".tag.txt"; // cdn 태그 파일 postfix
	public static final String AUDIO_POSTFIX = ".yda.txt"; // cdn 오디오 학습 파일
															// postfix
	private static final String BODY_POSTFIX = "_b.yda.txt"; // cdn 본문 파일
																// postfix
	public static final String SENTENCE_POSTFIX = "_p_%d.yda.txt"; // cdn 문장 시험
																	// 파일
																	// postfix
	private static final String VANISHING_POSTFIX = "_vc_%d.yda.txt"; // cdn 문장
																		// 시험 파일
																		// postfix
	private static final String DICTATION_POSTFIX = "_d_%d.yda.txt"; // cdn 받아쓰기
																		// 파일
																		// postfix
																		// 신규문항
	public static final String POPUPSTUDY_POSTFIX = ".zip"; // cdn Popup학습 파일
															// postfix
	public static final String DIRECTORY_POSTFIX = "/"; // cdn Popup학습 파일
														// postfix

	public static final String TAG_LOCAL_POSTFIX = ".tag"; // 로컬 태그 파일 postfix
	public static final String AUDIO_LOCAL_POSTFIX = ".yda"; // 로컬 오디오 학습 파일
																// postfix
	private static final String VIDEO_LOCAL_POSTFIX = "_v_%d.mp4"; // 로컬 비디오 학습 파일 postfix
	public static final String BODY_LOCAL_POSTFIX = "_b.yda"; // 로컬 본문 파일
																// postfix
	private static final String BODY_VIDEO_POSTFIX = "_b.mp4"; // 로컬 본문 파일
																// postfix
	public static final String SENTENCE_LOCAL_POSTFIX = "_p_%d.yda"; // 로컬 문장
																		// 시험 파일
																		// postfix
	private static final String VANISHING_LOCAL_POSTFIX = "_vc_%d.yda"; // 로컬
																		// 문단연습
																		// 파일
																		// postfix
	private static final String DICTATION_LOCAL_POSTFIX = "_d_%d.yda";// 로컬 받아쓰기
																		// 파일
																		// postfix
	public static final String WORD_POSTFIX = ".mp3"; // 로컬 및 cdn 단어시험 파일
														// postfix
	private static final String ANITALK_POSTFIX = "_%d.mp4"; // 로컬 및 cdn 애니톡 파일
																// postfix
	private static final String MOVIE_POSTFIX = "_1.mp4"; // 로컬 및 cdn 문법 동영상 파일
															// postfix

	public static final String ONEWEEK_POSTFIX = "_t_%d.mp3";//일주간
	public static final String ONEWEEK_POSTFIX2 = "_t_%d_2.mp3";//일주간
	public static final String SECOND_LOCAL_POSTFIX = "_T_%d.yda";//
	public static final String SECOND_POSTFIX = "_T_%d.yda.txt";
	private static final String OLDANSWER_POSTFIX = ".txt";// 로컬 받아쓰기

	public static final String PAGE_POSTFIX = "%d_%d_%s_%d.jpg";	// cdn 페이지 이미지 postfix


	/**
	 * 다운로드 차시 정보에 의해 다운로드 받을 페이지 리스트를 생성함
	 * @param unit : 다운로드 받을 차시 정보
	 * @param realBookNo : 다운로드 받을 실제 권 정보
	 * @return 다운로드 받을 파일 리스트 (cdn url, 로컬 패스)
	 */
	public static Map<String, String> getDownloadPageList(Context context, DownloadStudyUnit unit, int realBookNo) {
		DatabaseUtil databaseUtil = DatabaseUtil.getInstance(context);
		Map<String, String> list = new Hashtable<String, String>();
		String contentUrl = String.format(PAGE_URL, unit.mSeriesNo, realBookNo);
		String pgaeLocalPath = String.format(PAGE_LOCAL_PATH, unit.mSeriesNo, realBookNo);
		String filePath = "";
		ArrayList<StudyData.PageInfo>	pageList = databaseUtil.selectPageInfo(unit.mSeriesNo, realBookNo, unit.mStudyUnitNo);

		if (pageList.size() > 0) {
			// DB나 Tag 정보에 없지만 시작 페이지에 사용되는 이미지 다운로드
			String pageZero= String.format(PAGE_POSTFIX, unit.mSeriesNo, realBookNo, "S", 0);
			filePath = pgaeLocalPath + pageZero;
			if(!isFileExist(filePath)) {
				if(!list.containsValue(filePath))
					list.put(contentUrl + pageZero, filePath);
			}

			for(int i = 0; i < pageList.size(); i++) {
				StudyData.PageInfo page = pageList.get(i);
				String fileName = String.format(PAGE_POSTFIX, unit.mSeriesNo, realBookNo, page.mType, page.mPageNo);
				filePath = pgaeLocalPath + fileName;
				if(!isFileExist(filePath)) {
					if(!list.containsValue(filePath))
						list.put(contentUrl + fileName, filePath);
				}
			}
		}

		return list;
	}

	/**
	 * 다운로드 차시 정보에 의해 다운로드 받을 파일 리스트를 생성함
	 * 
	 * @param unit
	 *            : 다운로드 받을 차시 정보
	 * @return 다운로드 받을 파일 리스트 (cdn url, 로컬 패스)
	 */
	public static Map<String, String> getDownloadContentList(DownloadStudyUnit unit) throws Exception {
		Map<String, String> list = new Hashtable<String, String>();
		String contentUrl = String.format(ServiceCommon.CONTENT_URL, unit.mSeriesNo, unit.mBookNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		//구교재정답
		String oldAnswerUrl = String.format(ServiceCommon.OLD_ANSWER_URL, unit.mSeriesNo, unit.mBookNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);

		// 신규문항
		String contentPopupUrl = String.format(ServiceCommon.CONTENT_POPUP_URL, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitCode);
		// String videoUrl = String.format(VIDEO_URL, unit.mSeriesNo,
		// unit.mBookNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String wordLocalPath = String.format(WORD_LOCAL_PATH, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String filePath = "";
		int i = 0;
		int videoCnt = 0;
		// tag
		filePath = localPath + TAG_LOCAL_POSTFIX;
		if (!isFileExist(filePath) && unit.mIsCdnAudio) {
			list.put(contentUrl + TAG_POSTFIX, filePath);
		} else {
			videoCnt = getVTag(filePath);

			for (i = 1; i <= videoCnt; i++) {
				filePath = String.format(localPath + VIDEO_LOCAL_POSTFIX, i);
				if (!isFileExist(filePath)) {
					list.put(String.format(contentUrl + VIDEO_LOCAL_POSTFIX, i), filePath);
				}
			}
		}

		// audio
		filePath = localPath + AUDIO_LOCAL_POSTFIX;
		if (!isFileExist(filePath) && unit.mIsCdnAudio) {
			list.put(contentUrl + AUDIO_POSTFIX, filePath);
		}

		// body
		if (unit.mIsCdnBFile) {
			filePath = localPath + BODY_LOCAL_POSTFIX;
			if (!isFileExist(filePath)) {
				list.put(contentUrl + BODY_POSTFIX, filePath);
			}
		}

		// restudy_move_cnt
		if (unit.mIsCdnVideoFile) {
			filePath = localPath + BODY_VIDEO_POSTFIX;
			if (!isFileExist(filePath)) {
				list.put(contentUrl + BODY_VIDEO_POSTFIX, filePath);
			}
		}

		// sentence
		for (i = 1; i <= unit.mSentenceQuestionCnt; i++) {
			filePath = String.format(localPath + SENTENCE_LOCAL_POSTFIX, i);

			if (!isFileExist(filePath)) {
				list.put(String.format(contentUrl + SENTENCE_POSTFIX, i), filePath);
			}
		}

		// vanishing
		for (i = 1; i <= unit.mVanishingQuestionCnt; i++) {
			filePath = String.format(localPath + VANISHING_LOCAL_POSTFIX, i);
			if (!isFileExist(filePath)) {
				list.put(String.format(contentUrl + VANISHING_POSTFIX, i), filePath);
			}
		}

		// word
		for (i = 0; i < unit.mWordQuestionList.size(); i++) {
			String word = unit.mWordQuestionList.get(i);
			word = word.replace("?", "^");
			// word = word.replaceAll("`", "'");
			filePath = wordLocalPath + word + WORD_POSTFIX;

			/*	if (CommonUtil.isKorean(part1.getWord())) {
					wordLastPath = part1.getMeans() + ContentsUtil.WORD_POSTFIX;
					filePath = wordLocalPath + part1.getMeans() + ContentsUtil.WORD_POSTFIX;
				} else {
					wordLastPath = part1.getWord() + ContentsUtil.WORD_POSTFIX;
					filePath = wordLocalPath + part1.getWord() + ContentsUtil.WORD_POSTFIX;
				}
				*/
			if (!isFileExist(filePath) && !CommonUtil.isKorean(word)) {
				list.put(WORD_URL + word + WORD_POSTFIX, filePath);
			}
		}

		// dictation
		if (unit.mDictationCnt != 99) {
			for (i = 1; i <= unit.mDictationCnt; i++) {
				filePath = String.format(localPath + DICTATION_LOCAL_POSTFIX, i);

				if (!isFileExist(filePath)) {
					list.put(String.format(contentUrl + DICTATION_POSTFIX, i), filePath);
				}
			}
		}

		// 신규문항
		if (unit.mIsCdnZip) {
			// if(unit.mSeriesNo == 5881) {
			filePath = localPath + POPUPSTUDY_POSTFIX;
			String directoryPath = localPath + DIRECTORY_POSTFIX;
			contentPopupUrl = contentPopupUrl.replaceAll("5880", "5881");

			if (!isFileExist(filePath) && !isFileExist(directoryPath))
				list.put(contentPopupUrl + POPUPSTUDY_POSTFIX, filePath);
		}

		// 일일/주간평가
		if (unit.mOneWeekList != null) {
			int cnt = 1;//일일/주간 카운팅
			for (OneWeekData mOneWeekData : unit.mOneWeekList) {

				String path = "";

				if (mOneWeekData.question_type.equalsIgnoreCase(ServiceCommon.QUESTION_TYPE_2)) {

					filePath = String.format(localPath + ONEWEEK_POSTFIX, cnt);
					path = String.format(contentUrl + ONEWEEK_POSTFIX, cnt);
					list.put(path, filePath);

					filePath = String.format(localPath + ONEWEEK_POSTFIX2, cnt);
					path = String.format(contentUrl + ONEWEEK_POSTFIX2, cnt);

					list.put(path, filePath);

				}
				cnt++;

			}
		}

		//구교재정답관련
		if (unit.mIsOlndAnswer) {
			filePath = localPath + OLDANSWER_POSTFIX;
			if (!isFileExist(filePath)) {
				list.put(oldAnswerUrl + OLDANSWER_POSTFIX, filePath);
			}
		}

		return list;
	}

	/**
	 * 2r교시다운로드 차시 정보에 의해 다운로드 받을 파일 리스트를 생성함
	 * 
	 * @param unit
	 *            : 다운로드 받을 차시 정보
	 * @return 다운로드 받을 파일 리스트 (cdn url, 로컬 패스)
	 */
	public static Map<String, String> getSecDownloadContentList(DownloadStudyUnit unit) throws Exception {
		Map<String, String> list = new Hashtable<String, String>();
		String contentUrl = String.format(ServiceCommon.CONTENT_URL, unit.mSeriesNo, unit.mBookNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);

		// 신규문항
		// String videoUrl = String.format(VIDEO_URL, unit.mSeriesNo,
		// unit.mBookNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String wordLocalPath = String.format(WORD_LOCAL_PATH, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String filePath = "";
		int i = 0;

		// word
		for (i = 0; i < unit.mWordQuestionList.size(); i++) {
			String word = unit.mWordQuestionList.get(i);
			word = word.replace("?", "^");
			// word = word.replaceAll("`", "'");
			filePath = wordLocalPath + word + WORD_POSTFIX;

			if (!isFileExist(filePath)) {
				list.put(WORD_URL + word + WORD_POSTFIX, filePath);
			}
		}
		// sentence
		for (i = 1; i <= unit.mSentenceQuestionCnt; i++) {
			filePath = String.format(localPath + SENTENCE_LOCAL_POSTFIX, i);

			if (!isFileExist(filePath)) {
				list.put(String.format(contentUrl + SENTENCE_POSTFIX, i), filePath);
			}
		}

		// vanishing
		for (i = 1; i <= unit.mVanishingQuestionCnt; i++) {
			filePath = String.format(localPath + VANISHING_LOCAL_POSTFIX, i);
			if (!isFileExist(filePath)) {
				list.put(String.format(contentUrl + VANISHING_POSTFIX, i), filePath);
			}
		}

		return list;
	}

	/**
	 * 다운로드 차시 정보에 의해 다운로드 받을 파일 리스트를 생성함
	 * @param unit : 다운로드 받을 차시 정보
	 * @return 다운로드 받을 파일 리스트 (cdn url, 로컬 패스)
	 */
	public static Map<String, String> getExtraDownloadContentList(DownloadStudyUnit unit) {
		Map<String, String> list = new Hashtable<String, String>();
		String contentUrl = String.format(CONTENT_URL, unit.mSeriesNo, unit.mBookNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo, unit.mSeriesNo, unit.mBookNo, unit.mStudyUnitNo);
		String filePath = "";
		int i = 0;
		int videoCnt = 0;
		// tag
		filePath = localPath + TAG_LOCAL_POSTFIX;
		if(!isFileExist(filePath) && unit.mIsCdnAudio) {
			list.put(contentUrl + TAG_POSTFIX, filePath);
		} else {
			videoCnt = getVTag(filePath);

			for(i = 1; i <= videoCnt; i++) {
				filePath = String.format(localPath + VIDEO_LOCAL_POSTFIX, i);
				if(!isFileExist(filePath)) {
					list.put(String.format(contentUrl + VIDEO_LOCAL_POSTFIX, i), filePath);
				}
			}
		}

		return list;
	}

	public static int getUnitContentDownlaodPercent(Context context, int productNo, int seriesNo, int bookNo, int realBookNo, int studyUnitNo, String studyUnitCode, int fileCnt[], boolean review) throws Exception {
		DatabaseUtil databaseUtil = DatabaseUtil.getInstance(context);
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		String wordLocalPath = String.format(WORD_LOCAL_PATH, seriesNo, bookNo, studyUnitNo);
		String pgaeLocalPath = String.format(PAGE_LOCAL_PATH, seriesNo, realBookNo);
		String contentUrl = String.format(PAGE_URL, seriesNo, realBookNo);
		int i = 0;
		int existCnt = 0;
		int videoCnt = 0;

		// tag
		if (isFileExist(localPath + TAG_LOCAL_POSTFIX)) {
			existCnt++;
			videoCnt = getVTag(localPath + TAG_LOCAL_POSTFIX);

			for (i = 1; i <= videoCnt; i++) {
				String filePath = String.format(localPath + VIDEO_LOCAL_POSTFIX, i);
				if (isFileExist(filePath)) {
					existCnt++;
				}
			}
		}

		// audio
		if (isFileExist(localPath + AUDIO_LOCAL_POSTFIX)) {
			existCnt++;
		}

		// body
		boolean isCdnBFile = databaseUtil.isExistBody(productNo, studyUnitCode);
		if (isCdnBFile) {
			if (isFileExist(localPath + BODY_LOCAL_POSTFIX)) {
				existCnt++;
			}
		}

		// restudy_video_cnt
		boolean isRestudyVideoFile = databaseUtil.isExistBodyVideo(productNo, studyUnitCode);
		if (isRestudyVideoFile) {
			if (isFileExist(localPath + BODY_VIDEO_POSTFIX)) {
				existCnt++;
			}
		}

		// sentence
		int sentenceCnt = databaseUtil.selectSentenceQuestionCount(productNo, studyUnitCode);
		for (i = 1; i <= sentenceCnt; i++) {
			if (isFileExist(String.format(localPath + SENTENCE_LOCAL_POSTFIX, i)))
				existCnt++;
		}

		// vanishing
		int vanishingCnt = databaseUtil.selectVanishingQuestionCount(productNo, studyUnitCode);
		for (i = 1; i <= vanishingCnt; i++) {
			if (isFileExist(String.format(localPath + VANISHING_LOCAL_POSTFIX, i)))
				existCnt++;
		}

		// word
		ArrayList<String> wordList = databaseUtil.selectWordQuestionWordList(productNo, studyUnitCode);

		for (i = 0; i < wordList.size(); i++) {
			String word = wordList.get(i);
			word = word.replace("?", "^");
			// word = word.replaceAll("`", "'");
			if (isFileExist(wordLocalPath + word + WORD_POSTFIX))
				existCnt++;
		}

		// dictation
		int dictationCnt = databaseUtil.selectDictationCount(productNo, studyUnitCode);
		for (i = 1; i <= dictationCnt; i++) {
			if (isFileExist(String.format(localPath + DICTATION_LOCAL_POSTFIX, i)))
				existCnt++;
		}

		// 스트리밍 변경으로 인한 주석 처리
		/*
		 * // aniTalk int aniTalkCnt =
		 * databaseUtil.selectAniTalkCount(productNo, studyUnitCode); for(i = 1;
		 * i <= aniTalkCnt; i++) { if(isFileExist(String.format(localPath +
		 * ANITALK_POSTFIX, i))) existCnt++; }
		 *
		 * // 문법 동영상 boolean isExistMovie = databaseUtil.isExistMovie(productNo,
		 * studyUnitCode); if(isExistMovie) { if(isFileExist(localPath +
		 * MOVIE_POSTFIX)) { existCnt++; } }
		 */

		// 총 개수 계산
		// int totalCnt = 2 + sentenceCnt + vanishingCnt+ wordList.size() +
		// dictationCnt + (isCdnBFile?1:0)+videoCnt + (isRestudyVideoFile?1:0)/*
		// + aniTalkCnt + (isExistMovie?1:0)*/;
		int wordSize = wordList.size();
		/*for (String word : wordList) {
			if (!CommonUtil.isKorean(word)) {
				wordSize++;
			}
		}*/

		ArrayList<StudyData.PageInfo>	pageList = databaseUtil.selectPageInfo(seriesNo, realBookNo, studyUnitNo);
		int pageCnt = 0;

		if (pageList != null && pageList.size() > 0) {
			Map<String, String> list = new Hashtable<String, String>();
			pageCnt = 1;
			// DB나 Tag 정보에 없지만 시작 페이지에 사용되는 이미지 다운로드
			String pageZero = String.format(PAGE_POSTFIX, seriesNo, realBookNo, "S", 0);
			String pageZeroPath = pgaeLocalPath + pageZero;
			if(isFileExist(pageZeroPath)) {
				existCnt++;
			}

			for(i = 0; i < pageList.size(); i++) {
				StudyData.PageInfo page = pageList.get(i);
				String fileName = String.format(PAGE_POSTFIX, seriesNo, realBookNo, page.mType, page.mPageNo);
				if(!list.containsValue(fileName)) {
					list.put(contentUrl + fileName, fileName);
					pageCnt++;
					if(isFileExist(pgaeLocalPath + fileName)) {
						existCnt++;
					}
				}
			}
		}

		int totalCnt = (databaseUtil.isExistAudio(productNo, studyUnitCode) && !review ? 2 : 0) + sentenceCnt + vanishingCnt + wordSize + dictationCnt + (isCdnBFile ? 1 : 0) + videoCnt + (isRestudyVideoFile ? 1 : 0) + pageCnt/* + aniTalkCnt + (isExistMovie?1:0) */;

		//리뷰정리가 이상한거 때문에
		if (existCnt > totalCnt) {
			totalCnt = existCnt;
		}
		// int totalCnt = (databaseUtil.isExistAudio(productNo,
		// studyUnitCode)?1:0) + sentenceCnt + vanishingCnt+ wordList.size() +
		// dictationCnt + (isCdnBFile?1:0)+videoCnt + (isRestudyVideoFile?1:0)/*
		// + aniTalkCnt + (isExistMovie?1:0)*/;

		Log.e(TAG, "contentDownlaodPercent :  " + (int) ((existCnt / (double) totalCnt) * 100));

		if (fileCnt != null) {
			fileCnt[0] = totalCnt;
			fileCnt[1] = existCnt;
		}

		if (existCnt == 0 && totalCnt == 0) {
			return 100;
		} else if (existCnt == totalCnt) {
			return 100;
		}
		return (int) ((existCnt / (double) totalCnt) * 100);
	}

	/**
	 * ogg로 변환해야할 본문파일 yda리스트를 구성함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return 변환할 yda파일 리스트
	 */
	public static ArrayList<String> getUnitContentListYda(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		ArrayList<String> fileList = new ArrayList<String>();

		fileList.add(localPath + BODY_LOCAL_POSTFIX);

		return fileList;
	}

	/**
	 * ogg로 변환해야할 yda리스트를 구성함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param sentenceCnt
	 *            : 문장시험 개수
	 * @param dictationCnt
	 *            : 받아쓰기 개수
	 * @param isReview
	 *            : 이전 학습 복습인지 여부(본학습과 이전학습 복습은 변환해야하는 파일이 다르기 때문)
	 * @param isBodyExist
	 *            : 본문 존재 여부
	 * @return 변환할 yda파일 리스트
	 */
	public static ArrayList<String> getUnitContentListYda(int seriesNo, int bookNo, int studyUnitNo, int sentenceCnt, int dictationCnt, boolean isReview, boolean isBodyExist) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		ArrayList<String> fileList = new ArrayList<String>();

		if (!isReview) {
			fileList.add(localPath + AUDIO_LOCAL_POSTFIX);
		} else {
			if (isBodyExist) {
				fileList.add(localPath + BODY_LOCAL_POSTFIX);
			}
		}

		for (int i = 1; i <= sentenceCnt; i++) {
			fileList.add(String.format(localPath + SENTENCE_LOCAL_POSTFIX, i));
		}

		if (!isReview) {
			for (int i = 1; i <= dictationCnt; i++) {
				fileList.add(String.format(localPath + DICTATION_LOCAL_POSTFIX, i));
			}
		}

		return fileList;
	}

	/**
	 * ogg로 변환해야할 yda리스트를 구성함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param sentenceCnt
	 *            : 문장시험 개수
	 * @param vanishingCnt
	 *            : 문단연습 개수
	 * @param dictationCnt
	 *            : 받아쓰기 개수
	 * @param isReview
	 *            : 이전 학습 복습인지 여부(본학습과 이전학습 복습은 변환해야하는 파일이 다르기 때문)
	 * @param isBodyExist
	 *            : 본문 존재 여부
	 * @param isAudioExist
	 *            : 학습음원 존재 여부
	 * @return 변환할 yda파일 리스트
	 */
	public static ArrayList<String> getUnitContentListYda(int seriesNo, int bookNo, int studyUnitNo, int sentenceCnt, int vanishingCnt, int dictationCnt, boolean isReview, boolean isBodyExist, boolean isAudioExist) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		ArrayList<String> fileList = new ArrayList<String>();

		if (!isReview && isAudioExist) {
			fileList.add(localPath + AUDIO_LOCAL_POSTFIX);
		} else {
			if (isBodyExist) {
				fileList.add(localPath + BODY_LOCAL_POSTFIX);
			}
		}

		for (int i = 1; i <= sentenceCnt; i++) {
			fileList.add(String.format(localPath + SENTENCE_LOCAL_POSTFIX, i));
		}

		if (!isReview) {
			for (int i = 1; i <= dictationCnt; i++) {
				fileList.add(String.format(localPath + DICTATION_LOCAL_POSTFIX, i));
			}
		}

		if (!isReview) {
			for (int i = 1; i <= vanishingCnt; i++) {
				fileList.add(String.format(localPath + VANISHING_LOCAL_POSTFIX, i));
			}
		}

		return fileList;
	}

	// 학습 진행 데이터를 만들기 위한 파일 path 구하는 함수.
	/**
	 * 로컬 tag파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return tag파일 path
	 */
	public static String getTagFilePath(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return localPath + TAG_LOCAL_POSTFIX;
	}

	/**
	 * 로컬 audio 학습 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return audio 학습 파일 path
	 */
	public static String getAudioFilePath(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return extYdaToOgg(localPath + AUDIO_LOCAL_POSTFIX);
	}

	/**
	 * 로컬 audio 비디오 학습 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return audio 학습 파일 path
	 */
	public static String getVideoFilePath(int seriesNo, int bookNo, int studyUnitNo, int studyOrder) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		
		return String.format(localPath + VIDEO_LOCAL_POSTFIX, studyOrder);
	}

	/**
	 * 로컬 body 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return body 파일 path
	 */
	public static String getBodyFilePath(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return extYdaToOgg(localPath + BODY_LOCAL_POSTFIX);
	}

	/**
	 * 로컬 body 비디오 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return body 파일 path
	 */
	public static String getBodyVideoFilePath(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return localPath + BODY_VIDEO_POSTFIX;
	}

	/**
	 * 로컬 단어 시험 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param word
	 *            : 단어
	 * @return 단어 시험 파일 path
	 */
	public static String getWordFilePath(int seriesNo, int bookNo, int studyUnitNo, String word) {
		String wordLocalPath = String.format(WORD_LOCAL_PATH, seriesNo, bookNo, studyUnitNo);

		word = word.replace("?", "^");

		return wordLocalPath + word + WORD_POSTFIX;
	}

	/**
	 * 로컬 문장 시험 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param studyOrder
	 *            : 문장 시험 순서
	 * @return 문장 시험 파일 path
	 */
	public static String getSentenceFilePath(int seriesNo, int bookNo, int studyUnitNo, int studyOrder) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		return extYdaToOgg(String.format(localPath + SENTENCE_LOCAL_POSTFIX, studyOrder));
	}

	/**
	 * 로컬 문장 시험 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param studyOrder
	 *            : 문장 시험 순서
	 * @return 문장 시험 파일 path
	 */
	public static String getVanishingFilePath(int seriesNo, int bookNo, int studyUnitNo, int studyOrder) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		return extYdaToOgg(String.format(localPath + VANISHING_LOCAL_POSTFIX, studyOrder));
	}

	/**
	 * 로컬 받아쓰기 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param studyOrder
	 *            : 받아쓰기 순서
	 * @return 로컬 받아쓰기 파일 path
	 */
	public static String getDictationFilePath(int seriesNo, int bookNo, int studyUnitNo, int studyOrder) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		return extYdaToOgg(String.format(localPath + DICTATION_LOCAL_POSTFIX, studyOrder));
	}

	/**
	 * 서버 애니톡 url을 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param studyOrder
	 *            : 받아쓰기 순서
	 * @return cdn 애니톡 파일 url
	 */
	public static String getAniTalkFilePath(int seriesNo, int bookNo, int studyUnitNo, int studyOrder) {
		String contentUrl = String.format(ServiceCommon.CONTENT_URL, seriesNo, bookNo, seriesNo, bookNo, studyUnitNo);
		return String.format(contentUrl + ANITALK_POSTFIX, studyOrder);
	}

	/**
	 * 서버 문법 동영상 파일 url을 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return cdn 문법 동영상 파일 url
	 */
	public static String getMovieFilePath(int seriesNo, int bookNo, int studyUnitNo) {
		String contentUrl = String.format(ServiceCommon.CONTENT_URL, seriesNo, bookNo, seriesNo, bookNo, studyUnitNo);

		return contentUrl + MOVIE_POSTFIX;
	}

	// 신규문항
	/**
	 * 로컬 신규문항 콘텐츠 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return audio 학습 파일 path
	 */
	public static String getPopupContentsFilePath(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_POPUP_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return localPath;
	}

	/**
	 * 로컬 신규문항 콘텐츠 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return audio 학습 파일 path
	 */
	public static String getPopupContentsPath(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_POPUP_PATH, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return localPath;
	}

	/**
	 * 확장자 yda -> ogg로 변경
	 * 
	 * @param path
	 *            변경할 yda path
	 * @return ogg 파일 path
	 */
	public static String extYdaToOgg(String path) {
		return path.replace(".yda", ".ogg");
	}

	/**
	 * 파일이 로컬에 존재하는지 여부 조회
	 * 
	 * @param path
	 *            : 조회할 파일 path
	 * @return 존재 여부
	 */
	public static boolean isFileExist(String path) {
		return new File(path).exists();
	}

	/**
	 * ogg 파일 삭제
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 */
	public static void deleteOggFile(int seriesNo, int bookNo, int studyUnitNo) {
		String localPath = String.format(CONTENT_LOCAL_PATH, seriesNo, bookNo, studyUnitNo);

		File file = new File(localPath);
		File[] files = file.listFiles();

		if (files == null)
			return;

		for (int i = 0; i < files.length; i++) {
			if (files[i].getPath().contains(".ogg")) {
				files[i].delete();
			}
		}
	}

	public static int getVTag(String path) {
		BufferedReader in = null;
		int cnt = 0;
		if (new File(path).isFile()) {
			try {
				in = new BufferedReader(new FileReader(path));
				String tagLine = null;
				while (null != (tagLine = in.readLine())) {
					if (tagLine.contains("V1")) {
						cnt++;
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (null != in) {
					try {
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return cnt;
	}

	public static int getVTag(int seriesNo, int bookNo, int unitNo) throws IOException {//V1 태그의 개수를 가져옴

		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, unitNo, seriesNo, bookNo, unitNo);
		String tagPath = localPath + TAG_LOCAL_POSTFIX;

		if (!isFileExist(tagPath)) {
			return 0;
		} else {
			return getVTag(tagPath);
		}
	}

	/**
	 * 뮨던 연습용 음원 복사
	 * 
	 * @param context
	 * @param productNo
	 * @param studyUnitCode
	 * @param seriesNo
	 * @param bookNo
	 * @param unitNo
	 */
	public static void copyPilotContents(Context context, int productNo, String studyUnitCode, int seriesNo, int bookNo, int unitNo) {
		DatabaseUtil databaseUtil = DatabaseUtil.getInstance(context);
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, unitNo, seriesNo, bookNo, unitNo);
		int vanishingCnt = databaseUtil.selectVanishingQuestionCount(productNo, studyUnitCode);
		for (int i = 1; i <= vanishingCnt; i++) {
			if (!isFileExist(String.format(localPath + VANISHING_LOCAL_POSTFIX, i))) {
				String path = "%d_%d_%d" + VANISHING_LOCAL_POSTFIX;

				mkdirs(String.format(localPath + VANISHING_LOCAL_POSTFIX, i));
				CommonUtil.copyAssetsToSDCard(context, String.format(path, seriesNo, bookNo, unitNo, i), String.format(localPath + VANISHING_LOCAL_POSTFIX, i));
			}

		}
	}

	/**
	 * 설정된 위치에 폴더를 생성함.
	 * 
	 * @param fullPath
	 *            생성할 폴더의 전체 경로(마지막 항목이 파일이면 파일명으로 폴더가 생기지 않도록 함, 마지막 항목 제외함)
	 * @return true이면 정상 처리됨을 의미함.
	 */
	public static boolean mkdirs(String fullPath) {

		if (null == fullPath)
			return false;

		StringBuffer sb = new StringBuffer();
		List<String> itr = (Uri.parse(fullPath)).getPathSegments();

		int cnt = itr.size();
		for (int i = 0; i < cnt; i++) {
			String s = itr.get(i);

			// 마지막 항목이 파일이면 제외
			if (i == (cnt - 1)) {
				if (!s.contains(".")) {
					sb.append(File.separator);
					sb.append(s);
				}
			} else {
				sb.append(File.separator);
				sb.append(s);
			}
		}

		if (0 >= sb.length())
			return false;

		File f = new File(sb.toString());
		if (f.exists())
			return true;

		return f.mkdirs();
	}

	/**
	 * 로컬 문장 시험 파일 path를 구함
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @param studyOrder
	 *            : 문장 시험 순서
	 * @return 문장 시험 파일 path
	 */
	public static String getOneWeekFilePath(int seriesNo, int bookNo, int studyUnitNo, int cnt, boolean first) {
		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);
		if (first) {
			return extYdaToOgg(String.format(localPath + ONEWEEK_POSTFIX, cnt));
		} else {
			return extYdaToOgg(String.format(localPath + ONEWEEK_POSTFIX2, cnt));
		}

	}

	/**
	 * 구교재용 답안파일경로
	 * 
	 * @param seriesNo
	 *            : 시리즈 번호
	 * @param bookNo
	 *            : 권 번호
	 * @param studyUnitNo
	 *            : 차시 번호
	 * @return
	 */

	public static String getOLDFilePath(int seriesNo, int bookNo, int studyUnitNo) {

		String localPath = String.format(CONTENT_LOCAL_FILE_PREFIX, seriesNo, bookNo, studyUnitNo, seriesNo, bookNo, studyUnitNo);

		return localPath + OLDANSWER_POSTFIX;
	}

}
