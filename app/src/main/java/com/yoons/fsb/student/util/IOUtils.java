package com.yoons.fsb.student.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class IOUtils {

	private static final int BUF_SIZE = 0x1000; // 4K

	public static long copy(InputStream from, OutputStream to) throws IOException {
		byte[] buf = new byte[BUF_SIZE];
		long total = 0;
		while (true) {
			int r = from.read(buf);
			if (r == -1) {
				break;
			}
			to.write(buf, 0, r);
			total += r;
		}
		return total;
	}

	/**
	 * html 파일 String 으로
	 * 
	 * @param url
	 * @return
	 */
	public static String getHTML(String url) {
		BufferedReader rd; // Used to read results from the web page
		String line; // An individual line of the web page HTML
		String result = ""; // A long string containing all the HTML
		FileInputStream fis = null;// File Input 스트림 생성
		try {
			File file = new File(url);
			fis = new FileInputStream(file);
			rd = new BufferedReader(new InputStreamReader(fis));
			while ((line = rd.readLine()) != null) {
				result += line;
			}
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * html 파일 String 으로
	 * 
	 * @param url
	 * @return
	 */
	public static String getHTML(File file) {
		BufferedReader rd; // Used to read results from the web page
		String line; // An individual line of the web page HTML
		String result = ""; // A long string containing all the HTML
		FileInputStream fis = null;// File Input 스트림 생성
		try {
			fis = new FileInputStream(file);
			rd = new BufferedReader(new InputStreamReader(fis));
			while ((line = rd.readLine()) != null) {
				result += line;
			}
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
