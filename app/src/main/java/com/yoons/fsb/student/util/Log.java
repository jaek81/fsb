package com.yoons.fsb.student.util;

import android.content.Context;
import android.os.Environment;

import com.yoons.fsb.student.ServiceCommon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 로그 처리 클래스
 *
 * @author ejlee
 */
public class Log {
    private static boolean mIsLogable = true;
    private static boolean mIsFileLog = false;

    private static String mLogTag = "[Yoons]";
    private static String mLogFilePath = "";

    public static void d(String tag, String msg) {
        if (mIsLogable) {
            android.util.Log.d(mLogTag, tag + " : " + msg);
            fileLog(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (mIsLogable) {
            android.util.Log.e(mLogTag, tag + " : " + msg);
            fileLog(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (mIsLogable) {
            android.util.Log.i(mLogTag, tag + " : " + msg);
        }
    }

    public static void k(String tag, String msg) {
        if (mIsLogable) {
            android.util.Log.i(tag, msg);
        }
    }

    public static void v(String tag, String msg) {
        if (mIsLogable) {
            android.util.Log.v(mLogTag, tag + " : " + msg);
            fileLog(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (mIsLogable) {
            android.util.Log.w(mLogTag, tag + " : " + msg);
            fileLog(tag, msg);
        }
    }

    /**
     * voc 대응용으로 로그를 남기게 설정
     * 회원번호_날짜.txt파일
     */
    public static void startLog(Context mContext) {

        if (isExternalStorageWritable()) {

            File appDirectory;
            appDirectory = new File(Environment.getExternalStorageDirectory() + "/fsbdata");
            File logDirectory = new File(appDirectory + "/log");
            File logFile = new File(logDirectory, Preferences.getCustomerNo(mContext) + "_" + CommonUtil.getCurrentYYYYMMDDHHMMSS() + ".txt");

            // create app folder
            if (!appDirectory.exists()) {
                appDirectory.mkdir();
            }

            // create log folder
            if (!logDirectory.exists()) {
                logDirectory.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -v time -f  " + logFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (isExternalStorageReadable()) {
            // only readable
        } else {
            // not accessible
        }
    }

    public static void uploadLog() {


    }

    /**
     * 파일 로그로 남기는 함수
     *
     * @param tag : 로그 tag
     * @param msg : 로그 message
     */
    public static void fileLog(String tag, String msg) {
        if (mIsFileLog) {
            if (mLogFilePath.isEmpty()) {
                mLogFilePath = ServiceCommon.LOG_PATH;
            }

            try {
                if (!CommonUtil.makeDirs(mLogFilePath)) {
                    return;
                }
            } catch (Exception e) {
                return;
            }

            String logFilePath = mLogFilePath + "student.log";
            String logfilePathB = mLogFilePath + "student1.log";

            try {
                File file = new File(logFilePath);

                if (file.exists() == false) {
                    file.createNewFile();
                }

                if (file.length() > 500 * 1024) {
                    File file2 = new File(logfilePathB);
                    file2.delete();

                    file.renameTo(file2);
                    file.createNewFile();
                }

                FileWriter fw = new FileWriter(logFilePath, true);
                if (fw != null) {
                    BufferedWriter logSave = new BufferedWriter(fw);

                    if (logSave != null) {
                        if (CommonUtil.getFileSize(logFilePath) == 0) {
                            logSave.write('\ufeff'); //utf8의 BOM(EF BB BF)을 추가함.
                        }

                        logSave.append("[" + CommonUtil.getCurrentYYYYMMDDHHMMSS() + "]" + tag + ": \t" + msg + "\r\n");

                        logSave.flush();
                        logSave.close();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* Checks if external storage is available for read and write */
    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static File[] getLogFiles() {
        File file = new File(Environment.getExternalStorageDirectory() + "/fsbdata/log");
        File[] list = file.listFiles();

        return list;
    }
}
