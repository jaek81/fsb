package com.yoons.fsb.student.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.yoons.fsb.student.ServiceCommon;

/**
 * 환경 설정에 대한 값들을 관리하기 위한 모듈
 *
 * @author dckim
 */
public class Preferences {
    private static final String LOG_TAG = "[Preferences]";
    private static final String NAME = "FSB_STUDENT";

    /**
     * Device ID(wifi macaddress)
     */
    private static final String DEFAULT_DEVICEID = "";
    public static final String KEY_DEVICEID = "DeviceID";

    /**
     * Customer ID
     */
    private static final int DEFAULT_CUSTOMERNO = 0;
    public static final String KEY_CUSTOMERNO = "CustomerNo";


    /**
     * Customer ID
     */
    private static final String DEFAULT_TEACHERNO = "0";
    public static final String KEY_TEACHERNO = "TeacherNo";
    /**
     * USer NO
     */
    private static final int DEFAULT_USERNO = 0;
    public static final String KEY_USERNO = "UserNo";

    /**
     * Customer Name
     */
    private static final String DEFAULT_CUSTOMER_NAME = "";
    public static final String KEY_CUSTOMER_NAME = "CustomerName";

    /**
     * 이동통신망(3G/LTE) 사용 여부
     */
    private static final boolean DEFAULT_USE_DATA_NETWORK = false;
    public static final String KEY_USE_DATA_NETWORK = "UseDataNetwork";

    /**
     * 이동통신망(3G/LTE) 컨텐츠 다운로드
     */
    private static final boolean DEFAULT_DOWNLOAD_ON_DATA_NETWORK = false;
    public static final String KEY_DOWNLOAD_ON_DATA_NETWORK = "DownloadOnUseDataNetwork";

    /**
     * 학습 중 데이터 다운로드 허용
     */
    private static final boolean DEFAULT_ALLOW_DOWNLOADING_OF_STUDYING = false;
    public static final String KEY_ALLOW_DOWNLOADING_OF_STUDYING = "AllowDownloadingOfStudying";

    /**
     * 푸시 메시지를 받을지 여부
     */
    private static final boolean DEFAULT_USE_PUSH_MESSAGE = true;
    public static final String KEY_USE_PUSH_MESSAGE = "UsePushMessage";

    /**
     * 학습 알림 메시지를 받을지 여부
     */
    private static final boolean DEFAULT_USE_ALARM_MESSAGE = true;
    public static final String KEY_USE_ALARM_MESSAGE = "UseAlarmMessage";

    /**
     * 현재 학습 중인지 여부
     */
    private static final boolean DEFAULT_STUDYING = false;
    public static final String KEY_STUDYING = "Studying";

    /**
     * GCM Registration Id
     */
    private static final String DEFAULT_GCM_REGISTRATIONID = "";
    public static final String KEY_GCM_REGISTRATIONID = "GCMRegistrationId";

    /**
     * 단축 아이콘 생성시도 여부
     */
    private static final boolean DEFAULT_CREATED_SHORTCUT = false;
    public static final String KEY_CREATED_SHORTCUT = "CreatedShortCut";

    /**
     * 오디오/문장학습 에코 음량 설정
     */
    private static final int DEFAULT_STUDY_ECHO_VOLUME = 0;
    public static final String KEY_STUDY_ECHO_VOLUME = "StudyEchoVolume";

    /**
     * 와이파이 감도 설정
     */
    private static final int DEFAULT_WIFI_LEVEL = 0;
    public static final String KEY_WIFI_LEVEL = "WifiLevel";

    /**
     * 네트워크 연결 상태
     */
    private static final int DEFAULT_NETWORK_TYPE = -1;
    public static final String KEY_NETWORK_TYPE = "NetworkType";

    /**
     * 서버 request성공 여부
     */
    private static final boolean DEFAULT_SERVER_REQUEST_SUCCESS = false;
    public static final String KEY_SERVER_REQUEST_SUCCESS = "ServerRequestSuccess";

    /**
     * 교재 다운로드 화면에서 동기화 설공 실패 여부
     */
    private static final boolean DEFAULT_SYNC_SUCCESS = true;
    public static final String KEY_SYNC_SUCCESS = "SyncSuccess";
    public static final String KEY_FIRST = "First";

    /**
     * USer NO
     */
    private static final int DEFAULT_LMSSTATUS = 0;
    public static final String KEY_LMSSTATUS = "LmsStatus";

    /**
     * CASE 확인
     */
    private static final String DEFAULT_CASESTATUS = "0";
    public static final String KEY_CASESTATUS = "CaseStatus";

    /** 가이드 화면 노출 여부 */
    private static final boolean DEFAULT_SHOW_GUIDE = true;
    public static final String KEY_SHOW_GUIDE = "ShowGuide";

    /** 러닝 가이드 화면 노출 여부 */
    private static final boolean DEFAULT_SHOW_LEARNING_GUIDE = true;
    public static final String KEY_SHOW_LEARNING_GUIDE = "ShowLearningGuide";

    /** 사용자 가이드 화면 노출 여부 */
    private static final boolean DEFAULT_SHOW_USER_GUIDE = true;
    public static final String KEY_SHOW_USER_GUIDE = "ShowUserGuide";

    /** 미지원 경고 팝업 일주일간 보지 않기  **/
    private static final String DEFAULT_POPUP_CHECKED_DATE = "";
    public static final String KEY_POPUP_CHECKED_DATE ="PopupCheckedDate";

    private static final boolean DEFAULT_SHOW_LEARNING_GUIDE_FOR_PAGE_STUDY = true;
    public static final String KEY_SHOW_LEARNING_GUIDE_FOR_PAGE_STUDY = "ShowLearningGuideForPageStudy";


    /**
     * 사용 할 SharedPreferences를 얻음
     *
     * @param ctx Context
     * @return SharedPreferences
     */
    public static SharedPreferences getPref(Context ctx) {
        if (null != ctx)
            return ctx.getSharedPreferences(NAME, Activity.MODE_WORLD_READABLE);

        return null;
    }

    /**
     * DeviceId를 가져옴
     *
     * @param ctx Context
     * @return String DeviceId를 가져오며, ""일때는 저장된 내용이 없음.
     */
    public static String getDeviceId(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_DEVICEID;

        return pref.getString(KEY_DEVICEID, DEFAULT_DEVICEID);
    }

    /**
     * customerId를 가져옴
     *
     * @param ctx Context
     * @return String customerId를 가져오며, 0일때는 저장된 내용이 없음.
     */
    public static int getCustomerNo(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_CUSTOMERNO;

        return pref.getInt(KEY_CUSTOMERNO, DEFAULT_CUSTOMERNO);
    }

    /**
     * UserNO를 가져옴
     *
     * @param ctx Context
     * @return String 센터번호를 가져오며, 0일때는 저장된 내용이 없음.
     */
    public static String getUserNo(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return "0";

        return pref.getString(KEY_USERNO, "0");
    }

    /**
     * customerName를 가져옴
     *
     * @param ctx Context
     * @return String DeviceName을 가져오며, ""일때는 저장된 내용이 없음.
     */
    public static String getCustomerName(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_CUSTOMER_NAME;

        return pref.getString(KEY_CUSTOMER_NAME, DEFAULT_CUSTOMER_NAME);
    }

    /**
     * 이동통신망(3G/LTE) 사용 여부 확인
     *
     * @param ctx Context
     * @return boolean true이면 사용자가 3G/LTE 망 사용을 동의한 것으로 판단.(최초 초기값은 사용안함)
     */
    public static boolean getUseDataNetwork(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_USE_DATA_NETWORK;

        return pref.getBoolean(KEY_USE_DATA_NETWORK, DEFAULT_USE_DATA_NETWORK);
    }

    /**
     * 이동통신망(3G/LTE) 사용 여부 확인
     *
     * @param prefs SharedPreferences
     * @return boolean true이면 사용자가 3G/LTE 망 사용을 동의한 것으로 판단.(최초 초기값은 사용안함)
     */
    public static boolean getUseDataNetwork(SharedPreferences prefs) {
        if (prefs == null)
            return DEFAULT_USE_DATA_NETWORK;

        return prefs.getBoolean(KEY_USE_DATA_NETWORK, DEFAULT_USE_DATA_NETWORK);
    }

    /**
     * 숲(0),우영(1) 정보를 가져옴
     *
     * @param ctx Context
     * @return 0:숲, 1:우영
     *//*
    public static int getLmsStatus(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_LMSSTATUS;

        return pref.getInt(KEY_LMSSTATUS, DEFAULT_LMSSTATUS);
    }*/

    /**
     * DeviceId 저장
     *
     * @param ctx      Context
     * @param deviceId deviceID
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setDeviceId(Context ctx, String deviceId) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(KEY_DEVICEID, deviceId);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * CustomerId 저장
     *
     * @param ctx        Context
     * @param customerId CustomerId
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setCustomerNo(Context ctx, int customerId) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(KEY_CUSTOMERNO, customerId);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * userNo 저장
     *
     * @param ctx    Context
     * @param userNo CustomerId
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setUserNo(Context ctx, String userNo) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(KEY_USERNO, userNo);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * CustomerName 저장
     *
     * @param ctx          Context
     * @param customerName CustomerName
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setCustomerName(Context ctx, String customerName) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(KEY_CUSTOMER_NAME, customerName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 이동통신망(3G/LTE) 사용 여부 설정
     *
     * @param ctx    Context
     * @param option true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setUseDataNetwork(Context ctx, boolean option) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_USE_DATA_NETWORK, option);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 학습 콘텐츠 다운로드 시 이동통신망(3G/LTE) 사용 여부 확인
     *
     * @param ctx Context
     * @return boolean true이면 사용자가 3G/LTE 망 사용을 동의한 것으로 판단.(최초 초기값은 사용안함)
     */
    public static boolean getDownloadOnDataNetwork(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_DOWNLOAD_ON_DATA_NETWORK;

        return pref.getBoolean(KEY_DOWNLOAD_ON_DATA_NETWORK, DEFAULT_DOWNLOAD_ON_DATA_NETWORK);
    }

    /**
     * 학습 콘텐츠 다운로드 시 이동통신망(3G/LTE) 사용 여부 확인
     *
     * @param prefs SharedPreferences
     * @return boolean true이면 사용자가 3G/LTE 망 사용을 동의한 것으로 판단.(최초 초기값은 사용안함)
     */
    public static boolean getDownloadOnDataNetwork(SharedPreferences prefs) {

        if (prefs == null)
            return DEFAULT_DOWNLOAD_ON_DATA_NETWORK;

        return prefs.getBoolean(KEY_DOWNLOAD_ON_DATA_NETWORK, DEFAULT_DOWNLOAD_ON_DATA_NETWORK);
    }

    /**
     * 학습 콘텐츠 다운로드 시 이동통신망(3G/LTE) 사용 여부 설정
     *
     * @param ctx    Context
     * @param option true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setDownloadOnDataNetwork(Context ctx, boolean option) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_DOWNLOAD_ON_DATA_NETWORK, option);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 학습 콘텐츠 다운로드 시 이동통신망(3G/LTE) 사용 여부 설정
     *
     * @param prefs  SharedPreferences
     * @param option true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setDownloadOnDataNetwork(SharedPreferences prefs, boolean option) {

        if (prefs == null)
            return false;

        try {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(KEY_DOWNLOAD_ON_DATA_NETWORK, option);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 학습 중 데이터 다운로드 허용 여부 확인
     *
     * @param ctx Context
     * @return boolean true이면 "학습 중 데이터 다운로드 허용 여부 확인" 설정 상태임.(최초 초기값은 사용안함)
     */
    public static boolean getAllowDownloadingOfStudying(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_ALLOW_DOWNLOADING_OF_STUDYING;

        return pref.getBoolean(KEY_ALLOW_DOWNLOADING_OF_STUDYING, DEFAULT_ALLOW_DOWNLOADING_OF_STUDYING);
    }

    /**
     * 학습 중 데이터 다운로드 허용 여부 확인
     *
     * @param prefs SharedPreferences
     * @return boolean true이면 "학습 중 데이터 다운로드 허용 여부 확인" 설정 상태임.(최초 초기값은 사용안함)
     */
    public static boolean getAllowDownloadingOfStudying(SharedPreferences prefs) {
        if (prefs == null)
            return DEFAULT_ALLOW_DOWNLOADING_OF_STUDYING;

        return prefs.getBoolean(KEY_ALLOW_DOWNLOADING_OF_STUDYING, DEFAULT_ALLOW_DOWNLOADING_OF_STUDYING);
    }

    /**
     * 학습 중 데이터 다운로드 허용 여부 설정
     *
     * @param ctx    Context
     * @param option true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setAllowDownloadingOfStudying(Context ctx, boolean option) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_ALLOW_DOWNLOADING_OF_STUDYING, option);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 푸시 메시지 수신 확인
     *
     * @param ctx Context
     * @return boolean true이면 수신, false이면 수신 안함.
     */
    public static boolean getUsePushMessage(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_USE_PUSH_MESSAGE;

        return pref.getBoolean(KEY_USE_PUSH_MESSAGE, DEFAULT_USE_PUSH_MESSAGE);
    }

    /**
     * 푸시 메시지 수신 설정
     *
     * @param ctx            Context
     * @param usePushMessage true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setUsePushMessage(Context ctx, boolean usePushMessage) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_USE_PUSH_MESSAGE, usePushMessage);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 알람 메시지 수신 확인
     *
     * @param ctx Context
     * @return boolean true이면 수신, false이면 수신 안함.
     */
    public static boolean getUseAlarmMessage(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_USE_ALARM_MESSAGE;

        return pref.getBoolean(KEY_USE_ALARM_MESSAGE, DEFAULT_USE_ALARM_MESSAGE);
    }

    /**
     * 알람 메시지 수신 설정
     *
     * @param ctx             Context
     * @param useAlarmMessage true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setUseAlarmMessage(Context ctx, boolean useAlarmMessage) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_USE_ALARM_MESSAGE, useAlarmMessage);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 현재 학습중인지 여부 확인
     * <p>
     * true이면 사용함 설정
     *
     * @return boolean true이면 현재 학습중이면 true, 아니면 false
     */
    public static boolean getStudying(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_STUDYING;

        return pref.getBoolean(KEY_STUDYING, DEFAULT_STUDYING);
    }

    /**
     * 현재 학습중인지 여부 확인
     *
     * @param prefs SharedPreferences
     * @return boolean true이면 현재 학습중이면 true, 아니면 false
     */
    public static boolean getStudying(SharedPreferences prefs) {
        if (prefs == null)
            return DEFAULT_STUDYING;

        return prefs.getBoolean(KEY_STUDYING, DEFAULT_STUDYING);
    }

    /**
     * 현재 학습중인지 여부 설정
     *
     * @param ctx        Context
     * @param isStudying true이면 사용함 설정
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setStudying(Context ctx, boolean isStudying) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_STUDYING, isStudying);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * 오디오/문장학습 에코 음량 값을 가져옴
     *
     * @param ctx Context
     * @return String Volume를 가져오며, 0일때는 저장된 내용이 없음.
     */
    public static int getStudyEchoVolume(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_STUDY_ECHO_VOLUME;

        return correctEchoVolume(pref.getInt(KEY_STUDY_ECHO_VOLUME, DEFAULT_STUDY_ECHO_VOLUME));
    }

    /**
     * 특정 값이 들어가지 않도록 보정하는 기능(레벨 0, 10, 20, 40)
     *
     * @param volume 증폭 음량 값
     * @return 사용할 음량 값
     */
    public static int correctEchoVolume(int volume) {
        // volume (레벨 0, 10, 20, 40)
        if (volume <= 5)
            return 0;

        if (volume <= 15)
            return 10;

        if (volume <= 25)
            return 20;

        return 40;
    }

    /**
     * 서버 request성공 여부 확인
     *
     * @param ctx Context
     * @return boolean 성공 true, 실패 false
     */
    public static boolean getServerRequestSuccess(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_SERVER_REQUEST_SUCCESS;

        return pref.getBoolean(KEY_SERVER_REQUEST_SUCCESS, DEFAULT_SERVER_REQUEST_SUCCESS);
    }

    /**
     * 서버 request성공 여부 확인
     *
     * @param pref SharedPreferences
     * @return boolean 성공 true, 실패 false
     */
    public static boolean getServerRequestSuccess(SharedPreferences pref) {
        if (pref == null)
            return DEFAULT_SERVER_REQUEST_SUCCESS;

        return pref.getBoolean(KEY_SERVER_REQUEST_SUCCESS, DEFAULT_SERVER_REQUEST_SUCCESS);
    }

    /**
     * 현재 학습중인지 여부 설정
     *
     * @param ctx       Context
     * @param isSuccess true이면 request성공함.
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setServerRequestSuccess(Context ctx, boolean isSuccess) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_SERVER_REQUEST_SUCCESS, isSuccess);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 교재 다운로드 화면에서 Sync 성공 여부 확인
     *
     * @param ctx context
     * @return boolean 성공 true, 실패 false
     */
    public static boolean getSyncSuccess(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_SYNC_SUCCESS;

        return pref.getBoolean(KEY_SYNC_SUCCESS, DEFAULT_SYNC_SUCCESS);
    }

    /**
     * 교재 다운로드 화면에서 Sync 성공 여부 설정
     *
     * @param ctx       Context
     * @param isSuccess true이면 request성공함.
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setSyncSuccess(Context ctx, boolean isSuccess) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_SYNC_SUCCESS, isSuccess);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 처음인지 확인
     *
     * @param ctx context
     * @return boolean 처음 true, 실패 false
     */
    public static boolean getFirst(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return true;

        return pref.getBoolean(KEY_FIRST, true);
    }

    /**
     * 처음인지
     *
     * @param ctx       Context
     * @param isSuccess true이면 request성공함.
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setFirst(Context ctx, boolean isSuccess) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_FIRST, isSuccess);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * NetworkType 설정
     *
     * @param ctx  Context
     * @param type 네트워크 타입
     */
    public static void setNetworkType(Context ctx, int type) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(KEY_NETWORK_TYPE, type);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * NetworkType 요청
     *
     * @param ctx Context
     * @return 네트워크 타입
     */
    public static int getNetworkType(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_NETWORK_TYPE;

        return pref.getInt(KEY_NETWORK_TYPE, DEFAULT_NETWORK_TYPE);
    }

    /**
     * Wifi 감도 설정
     *
     * @param ctx   Context
     * @param level (0 ~ 4)
     */
    public static void setWifiSignalLevel(Context ctx, int level) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(KEY_WIFI_LEVEL, level);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Wifi 감도 취득
     *
     * @param ctx Context
     * @return int level (0 ~ 4)
     */
    public static int getWifiSignalLevel(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_WIFI_LEVEL;

        return pref.getInt(KEY_WIFI_LEVEL, DEFAULT_WIFI_LEVEL);
    }

    /**
     * Wifi 감도 취득
     *
     * @param prefs SharedPreferences
     * @return int level (0 ~ 4)
     */
    public static int getWifiSignalLevel(SharedPreferences prefs) {
        if (prefs == null)
            return DEFAULT_WIFI_LEVEL;

        return prefs.getInt(KEY_WIFI_LEVEL, DEFAULT_WIFI_LEVEL);
    }

   /* *//**
     * lms (숲,우영) 저장
     *
     * @param ctx
     * @param lmsStatus 0:숲 ,1:우영
     * @return
     *//*
    public static boolean setLmsStatus(Context ctx, int lmsStatus) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            if (lmsStatus != 1 && lmsStatus != 0) {//0,1둘다 아닐때
                lmsStatus = 0;//기본 숲
            }
            editor.putInt(KEY_LMSSTATUS, lmsStatus);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }*/

    /**
     * LOGIN CASE 저장
     *
     * @param ctx
     * @param caseStatus
     * @return
     */
    public static boolean setCaseStatus(Context ctx, String caseStatus) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();

            editor.putString(KEY_CASESTATUS, caseStatus);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * LOGIN CASE 저장
     *
     * @param ctx Context
     * @return
     */
    public static String getCaseStatus(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_CASESTATUS;

        return pref.getString(KEY_CASESTATUS, DEFAULT_CASESTATUS);
    }


    /**
     * 로컬,중앙화 여부
     *
     * @param ctx
     * @return 0:로컬 1: 중앙화
     */
    public static String getCenter(Context ctx) {

        SharedPreferences pref = getPref(ctx);
        return pref.getString(ServiceCommon.KEY_CENTER, ServiceCommon.DEFAULT_CENTER);
    }


    /**
     * 로컬,중앙화 여부
     *
     * @param ctx
     * @param is_center 0:로컬 1: 중앙화
     * @return
     */
    public static boolean setCenter(Context ctx, String is_center) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(ServiceCommon.KEY_CENTER, is_center);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * 센터번호 저장
     *
     * @param ctx
     * @param forestCode
     * @return
     */
    public static boolean setForestCode(Context ctx, String forestCode) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(ServiceCommon.KEY_FOREST_CODE, forestCode);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    /**
     * 센터번호
     *
     * @param ctx
     * @return
     */
    public static String getForestCode(Context ctx) {

        SharedPreferences pref = getPref(ctx);
        return pref.getString(ServiceCommon.KEY_FOREST_CODE, ServiceCommon.DEFAULT_FOREST_CODE);
    }

    /**
     * 원장번호
     *
     * @param ctx
     * @param teacherCode
     * @return
     */
    public static boolean setTeacherCode(Context ctx, String teacherCode) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(ServiceCommon.DEFAULT_TEACHER_CODE, teacherCode);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 원장번호
     *
     * @param ctx
     * @return
     */
    public static String getTeacherCode(Context ctx) {

        SharedPreferences pref = getPref(ctx);
        return pref.getString(ServiceCommon.KEY_TEACHER_CODE, ServiceCommon.DEFAULT_TEACHER_CODE);
    }

    /**
     * LMS구분
     *
     * @param ctx
     * @param lmsStatus 0:영어숲, 1:우영, 2: IGSE
     * @return
     */
    public static boolean setLmsStatus(Context ctx, String lmsStatus) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(ServiceCommon.KEY_LMS_STATUS, lmsStatus);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * LMS구분
     *
     * @param ctx
     * @return lmsStatus 0:영어숲, 1:우영, 2: IGSE
     */
    public static String getLmsStatus(Context ctx) {

        SharedPreferences pref = getPref(ctx);
        return pref.getString(ServiceCommon.KEY_LMS_STATUS, ServiceCommon.DEFAULT_LMS_STATUS);
    }

    /**
     * 가이드 화면 노출 여부 설정
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean setShowGuide(Context ctx, boolean isShow) {
        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_SHOW_GUIDE, isShow);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 가이드 화면 노출 여부 설정
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean setShowLearningGuide(Context ctx, boolean isShow) {
        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_SHOW_LEARNING_GUIDE, isShow);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 사용자 가이드 화면 노출 여부 설정
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean setShowUserGuide(Context ctx, boolean isShow) {
        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_SHOW_USER_GUIDE, isShow);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 가이드 화면 노출 여부를 가져옴
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean getShowGuide(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_SHOW_GUIDE;

        return pref.getBoolean(KEY_SHOW_GUIDE, DEFAULT_SHOW_GUIDE);
    }

    /**
     * 러닝 가이드 화면 노출 여부를 가져옴
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean getShowLearningGuide(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_SHOW_LEARNING_GUIDE;

        return pref.getBoolean(KEY_SHOW_LEARNING_GUIDE, DEFAULT_SHOW_LEARNING_GUIDE);
    }

    /**
     * 사용자 가이드 화면 노출 여부를 가져옴
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean getShowUserGuide(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_SHOW_USER_GUIDE;

        return pref.getBoolean(KEY_SHOW_USER_GUIDE, DEFAULT_SHOW_USER_GUIDE);
    }

    /**
     * 미지원 경고 팝업 일주일간 보지 않기 선택된 날짜 가져오기
     * @param prefs	SharedPreferences
     * @return date
     */
    public static String getPopupCheckedDate(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_POPUP_CHECKED_DATE;

        return pref.getString(KEY_POPUP_CHECKED_DATE, DEFAULT_POPUP_CHECKED_DATE);
    }

    /**
     * 미지원 경고 팝업 일주일간 보지 않기 선택된 날짜 저장
     * @param ctx	Context
     * @param date
     * @return 정보 저장 여부(true : 정상처리됨)
     */
    public static boolean setPopupCheckedDate(Context ctx, String date) {

        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(KEY_POPUP_CHECKED_DATE, date);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 러닝 가이드 화면 노출 여부를 가져옴
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean getShowLearningGuideForPageStudy(Context ctx) {
        SharedPreferences pref = getPref(ctx);
        if (pref == null)
            return DEFAULT_SHOW_LEARNING_GUIDE_FOR_PAGE_STUDY;

        return pref.getBoolean(KEY_SHOW_LEARNING_GUIDE_FOR_PAGE_STUDY, DEFAULT_SHOW_LEARNING_GUIDE_FOR_PAGE_STUDY);
    }

    /**
     * 가이드 화면 노출 여부 설정
     * @param ctx	Context
     * @return boolean . true이면 노출, false이면 비노출
     */
    public static boolean setShowLearningGuideForPageStudy(Context ctx, boolean isShow) {
        SharedPreferences pref = getPref(ctx);
        try {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(KEY_SHOW_LEARNING_GUIDE_FOR_PAGE_STUDY, isShow);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
