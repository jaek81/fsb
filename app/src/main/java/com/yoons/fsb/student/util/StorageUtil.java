package com.yoons.fsb.student.util;


import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Storage 관련된 함수를 정의한 Class임
 * 특히, External Storage가 2개 (내장,와장) 가 존재하는 Device에 대해 Internal Type과 External Type  각각의 
 * Storage Path를 구하는 기능을 포함하고 있음. 
 * (Android는 현재 2개 이상의 External Storage에 대한 Path를 지원하지 않음)
 * 
 * @author kyuh
 *
 */
public class StorageUtil {

	private final static String LOG_TAG = "[StorageUtil]";
	private static boolean LOG_ENABLE = false;

	public static final int INTERNAL_STORAGE = 0x01;   // internal type External Storage
	public static final int EXTERNAL_STORAGE = 0x02;   // external type External Storage
	public static final int NOT_AVAILABLE = -1     ;   // Can't calculate Storage Space because the sd card is removed !!!
	
	/* Internal Type Storage 와 External Type Storage가 동시 존재하는 Phone에서
	 * External Type Storage 의 Path를 정의함
	 * Sony Ericssion 단말은 아직 Internal Storage를 동시지원하는 단말이 없지만 만약 신규 발생할 경우 해당 Path설정필요 
	 * JB에서의 외장메모리 경로(/storage/extSdCard)는 ICS 경로(/mnt/extSdCard)와 함께 접근이 가능하기 때문에 상단에 추가함 
	 */	
	private static final String [] EXTERNAL_TYPE_SD_CARD_PATH= { "/mnt/sdcard/_ExternalSD",          //LG
		                                                         "/mnt/sdcard-ext",                  //MOTOROLA
		                                                         "/mnt/external1",                   //MOTOROLA
		                                                         "/mnt/sdcard/ext_sd",               //SK TeleSys, HTC
		                                                         "/storage/external_SD",			 //LG JB
		                                                         "/storage/extSdCard",				 //JB
		                                                         "/storage/sdcard1",				 //PANTECH JB
		                                                         "/mnt/sdcard/Storages/sd/mmcblk1",  //SAMSUMG TABLET
		                                                         "/mnt/extSdCard",                   //SAMSUMG ICS
		                                                         "/mnt/sdcard/Storages/sd/mmcblk2",  //honeycomb
		                                                         "/mnt/sdcard/extStorages/SdCard",   //ICS
		                                                         "/mnt/external_sd",                 //KT Tech
		                                                         "/mnt/sdcard/external_sd",          //SAMSUMG, PANTECH
		                                                         "/sdcard/sd",                        //SAMSUMG
		                                                         "/mnt/sdcard2"						 //Sony Xperia Tablet SGPT121KR/S
	                                                           };
	private static String interalTypeExternalStoragePath = null;
	private static String externalTypeExternalStoragePath = null;
	
	
	/* 내장 메모리 지원 단말 List
	 * Gingerbread 이전 버전으로 출시된 단말만 관리하면 된다.
	 * 
	 */
	private static final String[] INTERNAL_TYPE_STORAGE_SUPPORT_MODEL_LIST = {
			// Samsung
			//"Galaxy Nexus", 진저 이상 단말이므로 제외함 
			"SHW-M110S", 
			"SHW-M180S", 
			"SHW-M190S", 
			"SHW-M250S", 
			//"SHW-M380S", 허니컴 이상이므로 제외 
		    "SHV-E110S", 
		    "SHV-E120S", 
		    "SHV-E140S", 
		    "SHV-E160S",
		    "SHW-M200S",
		     //LG
		    "LG-SU540", 
		    "LG-SU660", 
		    "LG-SU760",
		    "LG-SU870", 
		    "LG-F100S", 
		    "LG-F120S", 
		    "LG-KU5900", 
		    
		    "SK-S170",
		    
		    //PANTECH
		    "IM-A760S", 
		    "IM-A800S",
		    "IM-A810S", 
		    "IM-T100K", 
		    
		    "MB860", 
		    "MB861", 
		    "MZ601", 
		    //Motorola 
		    //"XT910S", Gingerbread 로 출시된 단말 이므로 불필요
		    
		    "KM-S200", 
		    //"HTC Raider X710e" Gingerbread 로 출시된 단말 이므로 불필요  

			// 타 이통사 단말
			"LG-LU6800", 
			"LG-LU6200",	// 옵티머스 LTE
			"LG-LU5400",	// 프라다 3.0
			"LG-SU880",		// 옵티머스 Ex
			"LG-SU950",		// 옵티머스Z

			"IM-T100K",		// 베가 No.5
			"IM-A800L",		// 베가 LTE
			"IM-A800K",		// 베가 LTE
			"IM-A810K",		// 베가 LTE M
			"IM-A820L"		// 베가 LTE EX
	    };	
	
	/* Gingerbread의 method인 isExternalStorageRemovable 값이 뒤바뀌어 리턴되는 Device를 관리한다.
	 * HTC 단말은 특히 유의 해서 검증 해야 한다.
	 * 팬텍 단말도  IM-760S에도 문제가 있음. 
	 */
	private static final String[] ERR_DEVICE_REGARDING_IS_EXTERNAL_STORAGE_REMOVABLE = {
		"HTC Raider X710e",
		"IM-A760S",
		"IM-A770K"
	};

	/**
	 * Gingerbread 이전 OS로 출시한 단말 중  Dual External Memrory를 지원 하는 단말인지 여부를 
	 * 알려 준다.
	 * @return 단말이 Dual External Memrory를 지원 하는지 여부 
	 */
	private static boolean isDivceSupportInternalTypeExternalMemeory() {

		String mModelName = (Build.MODEL != null) ? Build.MODEL.toLowerCase().trim() : null;

		if (mModelName == null) {
			return false;
		}

		for (String internalTypeStorageSupportModel : INTERNAL_TYPE_STORAGE_SUPPORT_MODEL_LIST) {

			if (mModelName.contains(internalTypeStorageSupportModel.toLowerCase().trim())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Primary External Storage가 Removable 할 수 있는 지 여부를 판단함.
	 * Gingerbread 이상 단말은 isExternalStorageRemovable 로 판단하고,
	 * 이전 OS는 기 정의한 내장 메모리(Internal Type External Storage) 지원 단말 List에 포함 되어 있는 지 확인하고
	 * 만약 있다면  Primary External Storage는 Internal type (non-removable)이므로 false, 
	 * 그렇지 않은 경우 true로 판단함
	 * 
	 * @return Primary External Storage Removable 여
	 */
	private static boolean isSDCardRemovable() {
		
		/* 
		 * 내장 메모리를 보유한  DEVICE 라면 DEFAULT sdcard는 Internal Type External Memory 이므로 Remove 할 수 없음. 
		 */

		// Gingerbread 이전 버전으로 출시한 단말 중 dual storage 존재 유무 먼저 체크
		if (isDivceSupportInternalTypeExternalMemeory()) {
			logMessage("isDivceSupportInternalTypeExternalMemeory is true");
			return false;
		} else {
			logMessage("isDivceSupportInternalTypeExternalMemeory is false");
		}

		try {

			Method mInvMethod = Environment.class.getMethod("isExternalStorageRemovable", new Class[] {});
			Boolean mBoolState = (Boolean) mInvMethod.invoke(null, new Object[] {});
			boolean mIsSDCardRemovable = mBoolState.booleanValue();

			logMessage("GINGERBREAD OR HIGHER VERSION OF ANDROID [" + mIsSDCardRemovable + "]");

			String mModelName = (Build.MODEL != null) ? Build.MODEL.toLowerCase().trim() : null;
		    // isExternalStorageRemovable값의 false 이어야 하는데 true를 리턴하는 일부 HTC 단말에
		    // 대해 반대 값으로 세팅한다.
			if ((mModelName != null) && mIsSDCardRemovable) {

				for (String errDeviceStorage : ERR_DEVICE_REGARDING_IS_EXTERNAL_STORAGE_REMOVABLE) {
					if (mModelName.contains(errDeviceStorage.toLowerCase().trim())) {
						logMessage("Invalid isExternalStorageRemovable from true to false");
						mIsSDCardRemovable = !mIsSDCardRemovable;
					}
				}

			}
			return mIsSDCardRemovable;

		} catch (NoSuchMethodException e) {
			logMessage("[isSDCardRemovable] NoSuchMethodException = ", e);
		} catch (IllegalAccessException e) {
			logMessage("[isSDCardRemovable] IllegalAccessException = ", e);
		} catch (IllegalArgumentException e) {
			logMessage("[isSDCardRemovable] IllegalArgumentException = ", e);
		} catch (InvocationTargetException e) {
			logMessage("[isSDCardRemovable] InvocationTargetException = ", e);
		}
		return true;
	}

	/**
	 * 지원하는 Storage에서 현재 Mounted되어 사용 가능한 Storage가 있는지 여부를 확인함.
	 * @return true if there is a mounted storage. false if not
	 */
	public static boolean checkStorage() {

		boolean mInternal = getStorageState(INTERNAL_STORAGE);
		boolean mExternal = getStorageState(EXTERNAL_STORAGE);

		if (!mInternal && !mExternal) {

			logMessage("[checkStorage] DISABLE STORAGE!!!");
			return false;
		}

		logMessage("[checkStorage] ENABLE STORAGE!!!");

		return true;
	}

	/**
	 * External Storage Type (Internal, External)에 따라 해당 type의 Storage 사용 가능 여부를 판단함
	 * 
	 * @param sdcardType External Storage Type
	 * 
	 * @return 특정 type의 Storage가 사용가능하면 True 아니면 False
	 */
	public static boolean getStorageState(int sdcardType) {

		if (sdcardType == INTERNAL_STORAGE) {

			return isInternalTypeStorageAvailable();
		} else {

			return isExternalTypeStorageAvailable();
		}
	}

	/**
	 * Internal Type의 External Storage가 사용 가능한지 여부를 확인한다.
	 * 
	 * @return 사용 가능하면 true, 아니면 false
	 */
	private static boolean isInternalTypeStorageAvailable() {
	
	    /* PSUDO CODE 
	       IF (External Storage가 mounted)
	           IF (해당 Path가 외장 Type External Storage Path와 일치)
	               외장 Type External Storage가 mounted 된 것이므로 false
	           ELSE
	               IF ( REMOVABLE DISK 라면 )
	                   외장 Type의 External Storage만 존재 한다는 의미 이므로 false
	               ELSE
	                   내장 Type External Storage가 mounted 된 것이므로 true
	           END IF
	       END IF
	       STORAGE가 MOUNT 되지 않앗으므로 False
	            
	    */ 		

		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			String mAndroidExtStoragePath = Environment.getExternalStorageDirectory().getPath();
			logMessage("android storage path[" + mAndroidExtStoragePath + "]");

			// External Type Storage도 함께 있는 단말인 경우 External Type Storage 여부 확인
			for (String extTypeSdCartPath : EXTERNAL_TYPE_SD_CARD_PATH) {
				if (mAndroidExtStoragePath.equals(extTypeSdCartPath)) {
					interalTypeExternalStoragePath = null;
					return false;
				}
			}

			// Removable 이라면 Internal Type External Storage가 아니므로 false
		/*	if (isSDCardRemovable()) {
				logMessage("isExternalStorageRemovable is true");
				return false;
			} else {*/
				logMessage("isExternalStorageRemovable is false");
				interalTypeExternalStoragePath = mAndroidExtStoragePath;
				return true;
//			}
		}

		logMessage("android storage is unmounted");
		interalTypeExternalStoragePath = null;
		return false;
	}


	/**
	 * External Type 외장 Storage 사용가능 여부를 check
	 * @return true (사용가능), false (사용 불가능) 
	 */
	private static boolean isExternalTypeStorageAvailable() {
	    /* PSUDO CODE 
	       CASE 1) 
	         Mounted 되어 있고 2개이상 지원하는 STORAGE를 보유한 
	          딘말에 External Type External Storage Path와 일치하면 true
	       Case 2)   	           
	         Mounted 되어 있는데  Internal Type External Storage가 아니면 true 	        
 	       
 	       CASE 3) 제조사가 지정한 Exteral type Storage Path로 접근 가능하고 
 	               Android 제공 API로 External Storage가 mounted 되어 있지 않으면
 	               External Type 외장 메모리가 맞으므로 true
 	               
 	       CASE 4)제조사가 지정한 Exteral type Storage Path로 접근 가능하
 	               Android 제공 API로 External Storage가 mounted 되어 있는 경우
 	               두 가지 Path가 실제로 모두 internal Type External Storage일 가능성이 있으니
 	               각 Path별로 여유공간을 조사하여 일치하면 internal type이므로 false 아니면 true
 	            
	    */

		boolean mIsAndroidExternalStorageStateIsMounted = false;

		// Mounted 되어 있고
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			mIsAndroidExternalStorageStateIsMounted = true;
			String mAndroidExtStoragePath = Environment.getExternalStorageDirectory().getPath();
			
			// 2개이상 지원하는 STORAGE를 보유한 Device인지 확인
			for (String extTypeSDCardPath : EXTERNAL_TYPE_SD_CARD_PATH) {
				// 만약 2개 이상 지원하는 Storage의 External Type Path와 일치한다면 true
				if (mAndroidExtStoragePath.equals(extTypeSDCardPath)) {
					externalTypeExternalStoragePath = mAndroidExtStoragePath;
					return true;
				}
			}

			// Mounted 되어 있는데 InternalTypeStorage가 아니면 ExternalTypeStorage
			if (isInternalTypeStorageAvailable() == false) {
				externalTypeExternalStoragePath = mAndroidExtStoragePath;
				return true;
			}
		}

		File mFile = null;

		for (String mExtPath : EXTERNAL_TYPE_SD_CARD_PATH) {
			mFile = new File(mExtPath);   	
			// External Type 외장 메모리로 지정된 path에 접근 가능하다
			if ( mFile.exists() && mFile.canWrite()) {
				// logMessage("external dir exists and can write");
				// internal type 외장메모리 mounted되어 있지 않으면 100% 외장 메모리
				if (mIsAndroidExternalStorageStateIsMounted == false) {
					externalTypeExternalStoragePath = mExtPath;
					return true;
				} else {
					
				    // internal type 외장  메모리도 mounted 되어 있다면 External Type 외장 메모리로 지정된 path가 정말 외장 메모리
				    // 속해 있는지 internal type 외장 메모리의 여유공간과 External Type 외장 메모리로 지정된 path가 속해 있는 메모리 여유공간을 비교한다.
					// 일치하면 External Type 외장 메모리로 지정된 path는 internal Type 외장 메모리이므로 false, 그렇지 않으면 true
					long mExternalTypeStorageSize = getStorageFreeSpace(mExtPath);
					if (mExternalTypeStorageSize == NOT_AVAILABLE) {
						logMessage("mExternalTypeStorageSize NOT_AVAILABLE!!!");
						externalTypeExternalStoragePath = null;
						return false;
					}

					long mInternalTypeStorageSize = getStorageFreeSpace(Environment.getExternalStorageDirectory().getPath());
					if (mExternalTypeStorageSize == mInternalTypeStorageSize) {
						logMessage("ExternalTypePath = " + mExtPath);
						logMessage("InternalTypePath = " + Environment.getExternalStorageDirectory().getPath());
						logMessage("mExternalTypeStorageSize == mInternalTypeStorageSize");
						externalTypeExternalStoragePath = null;
						return false;
					} else {
						externalTypeExternalStoragePath = mExtPath;
						return true;
					}
				}
			} else {
				logMessage("external dir exists and can't write");
			}
		}

		externalTypeExternalStoragePath = null;
		return false;
	}
	
	/**
	 * Internal Type 또는 External Type의 외장 메모리 path를 리턴한다.
	 * 
	 * @param sdcardType internal type 또는 externaly type을 명기
	 * @return storage가 mounted 되어 않거나 접근 불가능 하면 null, 그렇지 않은경우 적절한 path를 리턴
	 */
	public static String getStorageDirectoryPath(int sdcardType) {

		if (sdcardType == INTERNAL_STORAGE) {

			return getInternalStorageDirectoryPath();
		} else {

			return getExternalStorageDirectoryPath();
		}
	}

	/**
	 * Internal type의 외장 메모리 경로를 구한다.
	 * 
	 * @return Internal type의 외장 메모리가 mount 되어 있으 해당 경로를 리턴함. 그렇지 않으면 null을 리턴
	 */
	private static String getInternalStorageDirectoryPath() {

		isInternalTypeStorageAvailable();
		return interalTypeExternalStoragePath;
	}

	/**
	 * External Type의 외장 메모리 경로를 구한다.
	 * 
	 * @return External Type의 외장 메모리가 mount 되어 있으 해당 경로를 리턴함. 그렇지 않으면 null을 리턴
	 */
	private static String getExternalStorageDirectoryPath() {

		isExternalTypeStorageAvailable();
		return externalTypeExternalStoragePath;

	}

	/**
	 * android의 default External Storage 경로를 구한다.
	 * 
	 * @return android의 default External Storage 경로 String
	 */
	private static String getDefaultStoragePath() {

		String mDirPath = null;

		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			File mFile = Environment.getExternalStorageDirectory();
			mDirPath = mFile.getPath();
		}

		if (mDirPath != null) {
			logMessage("[getDefaultStoragePath] " + mDirPath);
		}

		return mDirPath;
	}

	/**
	 * Storage의 여유공간을 구한다.
	 * 
	 * @param fullPath 파일 path
	 * @return 여유 storage 공간 (bytes), -1 if sd card is removed
	 */
	public static long getStorageFreeSpace(String fullPath) {

		// 저장 경로 유효성 체크
		if (fullPath == null || fullPath.length() == 0) {
			return 0;
		}

		// 저장할 폴더가 존재하는지 확인
		File fileDir = new File(fullPath);
		if (fileDir.exists() == false) {
			return 0;
		}

		StatFs stat = null;
		try {
			stat = new StatFs(fullPath);
		} catch (IllegalArgumentException e) {
			// This may occure if sd card is removed !!!
			logMessage("[getStorageFreeSpace] IllegalArgumentException = ", e);
			return NOT_AVAILABLE; // sd card is removed !!!!
		}

		long freeBytes = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();

		logMessage("[getStorageFreeSpace] Available Size(Byte) = " + new Long(freeBytes).toString());

		return freeBytes;
	}
	
	/**
	 * Storage의 전체공간을 구한다.
	 * 
	 * @param fullPath 파일 path
	 * @return 전체 크기
	 */
	public static long getStorageTotalSpace(String fullPath) {

		// 저장 경로 유효성 체크
		if (fullPath == null || fullPath.length() == 0) {
			return 0;
		}

		// 저장할 폴더가 존재하는지 확인
		File fileDir = new File(fullPath);
		if (fileDir.exists() == false) {
			return 0;
		}

		StatFs stat = null;
		try {
			stat = new StatFs(fullPath);
		} catch (IllegalArgumentException e) {
			// This may occure if sd card is removed !!!
			logMessage("[getStorageTotalSpace] IllegalArgumentException = ", e);
			return NOT_AVAILABLE; // sd card is removed !!!!
		}

		long totalBytes = (long) stat.getBlockSize() * (long) stat.getBlockCount();

		logMessage("[getStorageTotalSpace] Total Size(Byte) = " + new Long(totalBytes).toString());

		return totalBytes;
	}

	/**
	 * 파일 저장에 충분한 공간이 있는 지 확인하여 가능 여부를 리턴 저장하고자 하는 파일공간보다 2M이상 여유공간이 있어야 true를
	 * 리턴
	 * 
	 * @param fullPath 여유공간이 필요한 path
	 * @param l_filesize 저장하고자 하는 file size
	 * @return 여유공간이 있으 true, 그렇지 않으면 false
	 */
	public static boolean getEnableSaveContents(String fullPath, Long l_filesize) {

		boolean mIsEnable = false;

		// 저장할 폴더가 존재하는지 확인
		File contentsDir = new File(fullPath);
		if (contentsDir.exists() == false) {
			return mIsEnable;
		}

		// Content Size보다 2MB 여유공간 확인
		l_filesize += 1024 * 2000;

		// 저장할 크기가 0보다 작을 경우
		if (l_filesize <= 0) {
			return true;
		}

		// Storage 체크
		if (l_filesize > 0) {

			long mFreeSize = getStorageFreeSpace(fullPath);

			if (mFreeSize > 0) {

				if (mFreeSize > l_filesize.longValue()) {
					mIsEnable = true;
				}
			}
		}

		logMessage("[getStorageFreeSpace] Enable = " + mIsEnable);

		return mIsEnable;
	}

	public static void setLogEnable(boolean enable) {
		LOG_ENABLE = enable;
	}

	private static void logMessage(String log) {

		if (LOG_ENABLE) {
			Log.d(LOG_TAG, log);
		}
	}

	private static void logMessage(String log, Exception e) {
		Log.e(LOG_TAG, log, e);
	}
}