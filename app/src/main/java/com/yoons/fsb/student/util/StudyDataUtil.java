package com.yoons.fsb.student.util;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.RecData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.ExamResult;
import com.yoons.fsb.student.data.StudyData.SentenceQuestion;
import com.yoons.fsb.student.data.StudyData.SentenceResult;
import com.yoons.fsb.student.data.StudyData.StudyResult;
import com.yoons.fsb.student.data.StudyData.VanishingQuestion;
import com.yoons.fsb.student.data.StudyData.VanishingResult;
import com.yoons.fsb.student.data.StudyData.WordQuestion;
import com.yoons.fsb.student.data.StudyData.WordResult;
import com.yoons.fsb.student.db.DatabaseUtil;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.network.HttpMultiPart;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * studydata 관리 util
 *
 * @author ejlee
 */
public class StudyDataUtil {
    private static final String TAG = "StudyDataUtil";
    private static Long START_TIME = 0L;

    /**
     * 문장시험 채점 결과에 기록함
     *
     * @param customerAnswer : 회원 음성 인식 문장
     * @param isCorrect      : 음성 인식 점수, 3점 이상은 맞은 것임. 문제 순서대로 결과가 들어옴.
     */
    public static void addVanishingResult(String customerAnswer, int isCorrect, VanishingQuestion word) {
        StudyData data = StudyData.getInstance();
        VanishingResult result = new VanishingResult();
        result.mIsCorrect = isCorrect;
        result.mQuestionOrder = word.mQuestionOrder;
        result.mCorrectAnswer = word.mTextSentence;
        result.mCustomerAnswer = customerAnswer;
        result.mVanishingQuestionNo = word.mVanishingQuestionNo;
        result.mSoundFile = word.mSoundFile;
        result.mRetryCnt = word.mRetryCnt;
        data.mVanishingResult.add(result);
    }

    /**
     * 문장시험 채점 결과에 기록함 본 학습에서 호출
     *
     * @param customerAnswer : 회원의 답
     * @param isCorrect      : 음성인식 점수
     */
    public static void addSentenceResult(String customerAnswer, int isCorrect) {
        addSentenceResult(customerAnswer, isCorrect, false);
    }

    /**
     * 문장시험 채점 결과에 기록함
     *
     * @param customerAnswer : 회원 음성 인식 문장
     * @param isCorrect      : 음성 인식 점수, 3점 이상은 맞은 것임. 문제 순서대로 결과가 들어옴.
     * @param isReview       : 이전학습 복습인지 여부, true : 이전학습 복습, false : 본학습
     */
    public static void addSentenceResult(String customerAnswer, int isCorrect, boolean isReview) {
        StudyData data = StudyData.getInstance();
        int questionIndex = 0;
        SentenceQuestion question = null;

        try {
            if (isReview) {
                questionIndex = data.mReviewSentenceResult.size();
                question = data.mReviewSentenceQuestion.get(questionIndex);
            } else {
                questionIndex = data.mSentenceResult.size();
                question = data.mSentenceQuestion.get(questionIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        RecData recData = new RecData();
        recData.customer_no = data.mCustomerNo;
        recData.product_no = isReview ? data.mReviewProductNo : data.mProductNo;
        try {
            recData.study_unit_code = URLEncoder.encode(isReview ? data.mReviewStudyUnitCode : data.mStudyUnitCode, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        recData.re_study_cnt = isReview ? data.mReviewReStudyCnt : data.mReStudyCnt;
        recData.study_kind = isReview ? 0 : 1;
        recData.study_order = question.mQuestionOrder;
        recData.is_correct = isCorrect;
        recData.is_correct_answer = question.mSentence;
        recData.is_customer_answer = customerAnswer;
        recData.sentence_question_no = question.mSentenceQuestionNo;
        recData.study_result_no = isReview ? data.mReStudyResultNo : data.mStudyResultNo;
        recData.file_path = question.mRecordFile.replace("wav", "mp3");

        if (CommonUtil.isCenter()) {//중앙화여부
            recData.url = addPackageName("QXNwX0xfU1RVRFlfU0VOVEVOQ0VEVExfRg==");

        } else {
            recData.url = addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9TRU5URU5DRURUTA==");
        }


        HttpMultiPart httpMultiPart = new HttpMultiPart();

        httpMultiPart.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, recData);

        //String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_WORD_DETAIL, data.mCustomerNo, data.mReviewProductNo, quote(data.mReviewStudyUnitCode), data.mReviewReStudyCnt, 2, result.mQuestionOrder, booleanToInt(result.mIsCorrect), quote(String.valueOf(result.mCorrectAnswer)), result.mCustomerAnswer == 0 ? "null" : quote(String.valueOf(result.mCustomerAnswer)), quote(currentDt), result.mWordQuestionNo, quote("C"),data.mStudyResultNo,data.mStudyResultNo);

        SentenceResult result = new SentenceResult();
        result.mIsCorrect = isCorrect;
        result.mQuestionOrder = question.mQuestionOrder;
        result.mCorrectAnswer = question.mSentence;
        result.mCustomerAnswer = customerAnswer;
        result.mSentenceQuestionNo = question.mSentenceQuestionNo;
        result.mSoundFile = question.mSoundFile;

        if (isReview) {
            data.mReviewSentenceResult.add(result);
            if (result.mIsCorrect < 3)
                data.mWrongReviewSentenceQuestion.add(question);
        } else {
            data.mSentenceResult.add(result);
            if (result.mIsCorrect < 3)
                data.mWrongSentenceQuestion.add(question);
        }

    }

    /**
     * 단어시험 결과를 기록함 본학습에서 호출
     *
     * @param customerAnswer
     */
    public static void addWordResult(int customerAnswer, Handler mhHandler, Context mContext) {
        addWordResult(customerAnswer, false, mhHandler, mContext);
    }

    /**
     * 단어 시험 결과를 기록함 문제 순서대로 결과가 들어온다고 봄 customerAnswer이 0이면 5초가 넘어가서 틀린것으로 하는
     * 것임.
     *
     * @param customerAnswer : 회원이 선택한 답
     * @param isReview       : 이전학습 복습인지 여부, true : 이전학습 복습, false : 본학습
     */
    public static void addWordResult(int customerAnswer, boolean isReview, Handler mHandler, Context mContext) {
        StudyData data = StudyData.getInstance();
        int questionIndex = 0;
        WordQuestion question = null;

        try {
            if (isReview) {
                questionIndex = data.mReviewWordResult.size();
                question = data.mReviewWordQuestion.get(questionIndex);
            } else {
                questionIndex = data.mWordResult.size();
                question = data.mWordQuestion.get(questionIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        WordResult result = new WordResult();
        result.mQuestionOrder = question.mQuestionOrder;
        result.mIsCorrect = (customerAnswer == question.mAnswer);
        result.mCorrectAnswer = question.mAnswer;
        result.mCustomerAnswer = customerAnswer;
        result.mWordQuestionNo = question.mWordQuestionNo;
        result.mMeaning = question.mMeaning;
        result.mSoundFile = question.mSoundFile;

        if (isReview) {
            data.mReviewWordResult.add(result);
            if (!result.mIsCorrect)
                data.mWrongReviewWordQuestion.add(question);
        } else {
            data.mWordResult.add(result);
            if (!result.mIsCorrect)
                data.mWrongWordQuestion.add(question);
        }
        HttpJSONRequest request = new HttpJSONRequest(mContext);

        String[] params = new String[11];
        try {
            params[0] = String.valueOf(data.mCustomerNo);
            params[1] = String.valueOf(data.mProductNo);
            params[2] = String.valueOf(URLEncoder.encode(data.mStudyUnitCode, "UTF-8"));
            params[3] = String.valueOf(data.mReStudyCnt);
            params[4] = String.valueOf(isReview ? 0 : 1);
            params[5] = String.valueOf(question.mQuestionOrder);
            params[6] = result.mIsCorrect ? "1" : "0";
            params[7] = String.valueOf(result.mCorrectAnswer);
            params[8] = String.valueOf(result.mCustomerAnswer);
            params[9] = String.valueOf(result.mWordQuestionNo);
            params[10] = String.valueOf(isReview ? data.mReStudyResultNo : data.mStudyResultNo);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ;
        request.requestStudyWordDetail(mHandler, params);

        //문장저장
    }

    // 신규문항

    /**
     * popup 시험 결과를 기록함
     *
     * @param result : 회원의 popup 시험 결과
     */
    public static void addExamResult(ExamResult result, Context mContext, Handler mHandler) {
        StudyData data = StudyData.getInstance();

        for (int i = 0; i < data.mExamResult.size(); i++) {
            if (result.mStudyOrder == data.mExamResult.get(i).mStudyOrder) {
                return;
            }
        }

        HttpJSONRequest request = new HttpJSONRequest(mContext);

        String[] params = new String[13];
        try {
            params[0] = String.valueOf(data.mCustomerNo);
            params[1] = String.valueOf(data.mProductNo);
            params[2] = String.valueOf(URLEncoder.encode(data.mStudyUnitCode, "UTF-8"));
            params[3] = String.valueOf(data.mReStudyCnt);
            params[4] = String.valueOf(1);
            params[5] = String.valueOf(result.mQuestionOrder);//이거변경해야할지물어봐야함
            params[6] = String.valueOf(result.mTagType);
            params[7] = String.valueOf(result.mIsCorrect);
            params[8] = String.valueOf(0);
            params[9] = String.valueOf(result.mQuestionOrder);
            params[10] = String.valueOf(result.mQuestionNo);
            params[11] = result.mCustomerAnswer.replace(",", "_").replace("|", "_");//,| ->|로 대채 나중에 인코딩해야함
            params[12] = String.valueOf(data.mStudyResultNo);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        request.requestStudyExamDetail(mHandler, params);

        data.mExamResult.add(result);
    }

    /**
     * popup 시험 결과를 기록함 (Role&Play, Touch&Play)
     * <p>
     * : 회원의 popup 시험 결과
     */
    public static void addExamResult(ArrayList<ExamResult> resultList, Context mContext, Handler mHandler) {
        StudyData data = StudyData.getInstance();

        for (ExamResult result : resultList) {
            HttpJSONRequest request = new HttpJSONRequest(mContext);

            String[] params = new String[13];
            try {
                params[0] = String.valueOf(data.mCustomerNo);
                params[1] = String.valueOf(data.mProductNo);
                params[2] = String.valueOf(URLEncoder.encode(data.mStudyUnitCode, "UTF-8"));
                params[3] = String.valueOf(data.mReStudyCnt);
                params[4] = String.valueOf(1);
                params[5] = String.valueOf(result.mQuestionOrder);//이거변경해야할지물어봐야함
                params[6] = String.valueOf(result.mTagType);
                params[7] = String.valueOf(result.mIsCorrect);
                params[8] = String.valueOf(1);
                params[9] = String.valueOf(result.mQuestionOrder);
                params[10] = result.mQuestionNo;
                params[11] = result.mCustomerAnswer.replace(",", "_").replace("|", "_");//,| ->|로 대채 나중에 인코딩해야함
                params[12] = String.valueOf(data.mStudyResultNo);
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            request.requestStudyExamDetail(mHandler, params);
        }
        data.mExamResult.addAll(resultList);
    }

    /**
     * 학습 결과를 조회
     *
     * @return 학습 결과 정보
     */
    public static StudyResult getStudyResult() {
        DatabaseUtil databaseUtil = DatabaseUtil.getInstance();

        StudyResult result = databaseUtil.getStudyExamResult();

        //		long studyTime = System.currentTimeMillis() - data.mStudyStartTime;
        //			result.mStudyTotalTime = (int)(studyTime / 1000 / 60);

        return result;
    }

    /**
     * 현재 학습 진행 상태를 업데이트함. 본학습 학습상태 코드(S:학습시작, S11:본학습_음원듣기시작,S12:종료,
     * S21:시험준비,S22 , S31,S32:본학습_단어, S41,S42:본학습_문장, S51,S52:본학습_받아쓰기,
     * S61,S62:본학습_문법동영상SFN:학습종료) 복습 학습상태 코드 (R:학습시작(복습),
     * R11:복습_음원듣기시작,R12:종료.., R31,R32:복습_단어, R41,R42:복습_문장, RFN:학습종료(복습))
     *
     * @param context
     * @param status  : 현재 학습 상태
     */
    public static void setCurrentStudyStatus(Context context, String status) {

        Log.e("StudyDataUtil", "setCurrentStudyStatus : " + status);

        if (ServiceCommon.IS_CONTENTS_TEST) {
            return;
        }

        StudyData data = StudyData.getInstance();

        if ("R01".equals(status) && data.mReviewCheck) {//복습일때 한번만 보냄
            return;
        } else if ("R01".equals(status) && !data.mReviewCheck) {
            data.mReviewCheck = true;
        }

        if (data.mCurrentStatus.isEmpty() && !"SFN".equals(status)) { // 이전 학습 복습 중, 본학습 status에 값이 있으면 무시함.
            return;
        }

        data.mCurrentStatus = status;

        DatabaseUtil.getInstance().updateStudyResultStatus(data.mCurrentStatus); // db에 업데이트

        if (status.equals("S21") || status.equals("S22") || status.equals("SFN") || status.equals("RFN")) { // 현재 상태에 따라 현재 학습중인지 여부 설정
            Preferences.setStudying(context, false);
        } else {
            Preferences.setStudying(context, true);
        }

        // 서버에 상태 정보 전송.
        if (CommonUtil.isAvailableNetwork(context, false)) {
            HttpJSONRequest request = new HttpJSONRequest(context);
            String currentDt = "";
            try {
                currentDt = URLEncoder.encode(CommonUtil.getCurrentDateTime(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            int audioTime = 0;
            int playingTime = 0;
            String playStatus = "";
            int stepDuration = 0;
            int aCnt = 0;
            int aTime = 0;
            int mCnt = 0;
            int mTime = 0;
            int mResultNo = data.mStudyResultNo;
            int mRtime = 0;

            if (status.contains("R")) {

                if (status.equalsIgnoreCase("R01")) {
                    playStatus = ServiceCommon.STATUS_START;
                } else if (status.equalsIgnoreCase("RFN")) {
                    playStatus = ServiceCommon.STATUS_END;
                }
                //mResultNo = data.mReStudyResultNo;
                stepDuration = data.mReviewDuration;
            } else {
                audioTime = data.mAudioTotalMin / 1000;
                playingTime = data.mAudioStudyTime / 1000;
                playStatus = data.audioPlaying;
                //stepDuration = data.mAudioStudyTime / 1000;
                aCnt = (data.mAudioRecordIndex - data.mRecMCnt) < 0 ? 0 : (data.mAudioRecordIndex - data.mRecMCnt);
                aTime = (data.mTotalRecTime - data.mRecMTime) < 0 ? 0 : (data.mTotalRecTime - data.mRecMTime);
                mCnt = data.mRecMCnt;
                mTime = data.mRecMTime;
                mRtime = data.mRecRTime * data.mRTagRepeatCnt;
            }

            if (status.equalsIgnoreCase("S11") || status.equalsIgnoreCase("S21") || status.equalsIgnoreCase("S31") || status.equalsIgnoreCase("S41") || status.equalsIgnoreCase("S51") || status.equalsIgnoreCase("S61") || status.equalsIgnoreCase("S71")) {
                playStatus = ServiceCommon.STATUS_START;
                START_TIME = System.currentTimeMillis();
            } else if (status.equalsIgnoreCase("S12") || status.equalsIgnoreCase("S22") || status.equalsIgnoreCase("S32") || status.equalsIgnoreCase("S42") || status.equalsIgnoreCase("S52") || status.equalsIgnoreCase("S62") || status.equalsIgnoreCase("S72")) {
                playStatus = ServiceCommon.STATUS_END;
                if (START_TIME != 0L) {
                    stepDuration = (int) ((System.currentTimeMillis() - START_TIME) / 1000);
                    START_TIME = 0L;
                }
            }
            if ("S01".equals(status)) {//s01은 홈sb용(중앙화때문에 나둠)
                return;
            }
            if (!"".equals(playStatus) || "SFN".equals(status)) {
                try {
                    request.requestStudyStatus(String.valueOf(String.valueOf(data.mCustomerNo)), //MCODE
                            String.valueOf(data.mProductNo), //교재
                            String.valueOf(URLEncoder.encode(data.mStudyUnitCode, "UTF-8")), //차시
                            String.valueOf(data.mReStudyCnt), //몇회
                            String.valueOf(data.mCurrentStatus), //단계
                            String.valueOf(playStatus), //플레이 상태
                            String.valueOf(CommonUtil.getCurrentDateTime2()), //시간
                            String.valueOf(stepDuration), //단계진행시간
                            String.valueOf(audioTime), //총 오디오 시간
                            String.valueOf(playingTime), //위치
                            String.valueOf(aCnt), //자동 녹음 누적 횟수(전체-수동)
                            String.valueOf(aTime), //자동 녹음 누적 시간
                            String.valueOf(mCnt), //수동 녹음 누적 횟수
                            String.valueOf(mTime), //수동 녹음 누적 시간
                            String.valueOf(mRtime), //권장 녹음 시간
                            String.valueOf(data.mRecordFileYmd), CommonUtil.getMacAddress(), //MAC 주소
                            String.valueOf(mResultNo));
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            //request.requestStudyStatus(String.valueOf(data.mCustomerNo), String.valueOf(data.mProductNo), data.mStudyUnitCode, String.valueOf(data.mReStudyCnt), status, currentDt, String.valueOf(audioTime), data.mRecordFileYmd, CommonUtil.getDeviceId(context));
        }
    }

    /**
     * Tag가 이상한 것에 대해 올림
     *
     * @param context
     */
    public static void requestAppSystemLog(Context context) {
        if (CommonUtil.isAvailableNetwork(context, false)) {
            StudyData data = StudyData.getInstance();
            HttpJSONRequest request = new HttpJSONRequest(context);
            try {
                String message = String.format("%s(%d_%d_%s)에 태그 오류가 있습니다.", data.mProductName, data.mSeriesNo, data.mBookNo, data.mStudyUnitCode);
                request.requestAppSystemLog(String.valueOf(data.mCustomerNo), "M", CommonUtil.getDeviceId(context), Build.VERSION.RELEASE, URLEncoder.encode(Build.MODEL, "UTF-8"), URLEncoder.encode(message, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 학습준비 시 총 반복횟수 서버에 올림
     *
     * @param context
     */
    public static void requestAppTotalRepeatLog(Context context) {
        if (CommonUtil.isAvailableNetwork(context, false)) {
            StudyData data = StudyData.getInstance();
            HttpJSONRequest request = new HttpJSONRequest(context);
            try {
                String message = String.format("MCODE: %s, 교재: %s(%d_%d_%s)에 설정된 총 반복횟수는 %d회입니다.", String.valueOf(data.mCustomerNo), data.mProductName, data.mSeriesNo, data.mBookNo, data.mStudyUnitCode, data.mRTagRepeatCnt);
                request.requestAppSystemLog(String.valueOf(data.mCustomerNo), "M", CommonUtil.getDeviceId(context), Build.VERSION.RELEASE, URLEncoder.encode(Build.MODEL, "UTF-8"), URLEncoder.encode(message, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 학습중 현재 반복횟수를 서버에 올림
     *
     * @param context
     */
    public static void requestAppRepeatLog(Context context, int repeat) {
        if (CommonUtil.isAvailableNetwork(context, false)) {
            StudyData data = StudyData.getInstance();
            HttpJSONRequest request = new HttpJSONRequest(context);
            try {
                String message = String.format("MCODE: %s, 교재: %s(%d_%d_%s)에 반복횟수 %d회 실행중입니다.", String.valueOf(data.mCustomerNo), data.mProductName, data.mSeriesNo, data.mBookNo, data.mStudyUnitCode, repeat);
                request.requestAppSystemLog(String.valueOf(data.mCustomerNo), "M", CommonUtil.getDeviceId(context), Build.VERSION.RELEASE, URLEncoder.encode(Build.MODEL, "UTF-8"), URLEncoder.encode(message, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 5분마다 오디오 학습 상태를 전송함
     *
     * @param context
     */
    public static void requestStudyResultBookCheck(Context context) {
        if (CommonUtil.isAvailableNetwork(context, false)) {
            StudyData data = StudyData.getInstance();
            HttpJSONRequest request = new HttpJSONRequest(context);
            request.requestStudyResultBookChk(String.valueOf(data.mCustomerNo), String.valueOf(data.mProductNo), data.mStudyUnitCode, String.valueOf(data.mReStudyCnt), String.valueOf(""));
        }
    }

    /**
     * 녹음 시간 초과 warning 전송(status : 5)
     *
     * @param context
     */
    public static void addRecordBookWarning(final Context context) {
        addRecordBookWarning(context, false);
    }

    /**
     * 녹음 시간 초과 warning 전송(status : 5) 서버 전송 실패시 db에 기록함
     *
     * @param context
     * @param requestFail : 서버 request fail인지 여부
     */
    public static void addRecordBookWarning(final Context context, boolean requestFail) {
        StudyData data = StudyData.getInstance();
        DatabaseUtil databaseUtil = DatabaseUtil.getInstance(context);
        int checkStatus = 5;
        String currentDateTime = CommonUtil.getCurrentDateTime();

        if (requestFail) {
            databaseUtil.insertStudyBookWarning(data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, checkStatus, currentDateTime, data.mStudyResultNo);
        } else if (CommonUtil.isAvailableNetwork(context, false)) {
            HttpJSONRequest request = new HttpJSONRequest(context);
            try {
                request.requestStudyBookWarning(new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                                addRecordBookWarning(context, true);
                                break;

                            case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                                break;
                        }

                        super.handleMessage(msg);
                    }
                }, String.valueOf(data.mCustomerNo), String.valueOf(data.mProductNo), data.mStudyUnitCode, String.valueOf(data.mReStudyCnt), String.valueOf(checkStatus), URLEncoder.encode(currentDateTime, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            databaseUtil.insertStudyBookWarning(data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, checkStatus, currentDateTime, data.mStudyResultNo);
        }

    }

    /**
     * 오디오 학습 시간 체크 warning(2분마다 오디오 학습 중 호출)
     *
     * @param context
     */
    public static void checkBookWarning(final Context context) {
        checkBookWarning(context, false);
    }

    /**
     * 오디오 학습 시간 체크 warning 서버 전송 실패시 db에 기록함
     *
     * @param context
     * @param requestFail : 서버 request fail인지 여부
     */
    public static void checkBookWarning(final Context context, boolean requestFail) {
        StudyData data = StudyData.getInstance();
        DatabaseUtil databaseUtil = DatabaseUtil.getInstance(context);
        String bookStartDt = databaseUtil.selectBookStartDt(data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt);
        int checkStatus = 0;
        String currentDateTime = CommonUtil.getCurrentDateTime();

        if (bookStartDt.length() == 0 || data.mAudioTotalMin == 0) {
            return;
        }

        Date startDate = null;
        int studyTime = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            startDate = sdf.parse(bookStartDt);
            studyTime = (int) ((System.currentTimeMillis() - startDate.getTime()) / 1000 / 60);
            int mToltalmin = ((int) (data.mAudioTotalMin / 1000.0)) / 60;

            if ((studyTime / mToltalmin) >= 3) {
                checkStatus = data.mIsAutoArtt ? 2 : 4;
            } else if ((studyTime / mToltalmin) >= 2) {
                checkStatus = data.mIsAutoArtt ? 1 : 3;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (checkStatus == 0) {
            return;
        }

        if (requestFail) {
            databaseUtil.insertStudyBookWarning(data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, checkStatus, currentDateTime, data.mStudyResultNo);
        } else if (CommonUtil.isAvailableNetwork(context, false)) {
            HttpJSONRequest request = new HttpJSONRequest(context);
            try {
                request.requestStudyBookWarning(new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
                                checkBookWarning(context, true);
                                break;

                            case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
                                break;
                        }

                        super.handleMessage(msg);
                    }
                }, String.valueOf(data.mCustomerNo), String.valueOf(data.mProductNo), data.mStudyUnitCode, String.valueOf(data.mReStudyCnt), String.valueOf(checkStatus), URLEncoder.encode(currentDateTime, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            databaseUtil.insertStudyBookWarning(data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt, checkStatus, currentDateTime, data.mStudyResultNo);
        }
    }

    //2013-06-12 23:21:28
    //012345678901234567890

    /**
     * 오디오 학습 시간을 체크함(서버에서 구하므로 무의미한 작업)
     *
     * @param startTime : 오디오 학습 시작 시간
     * @param endTime   : 오디오 학습 종료 시간
     * @param audioMin  : 오디오 음원 길이(분)
     * @return 속도 0: 정상 10: 빠름, 11: 너무빠름, 20: 느림, 21 : 너무 느림
     */
    public static int getAudioStudySpeed(String startTime, String endTime, int audioMin) {
        Calendar cal = Calendar.getInstance();
        long start = 0;
        long end = 0;

        // 학습음원이 없는 경우
        if (startTime == null || endTime == null)
            return 0;

        int year = Integer.valueOf(startTime.substring(0, 4));
        int month = Integer.valueOf(startTime.substring(5, 7));
        int day = Integer.valueOf(startTime.substring(8, 10));
        int hour = Integer.valueOf(startTime.substring(11, 13));
        int minute = Integer.valueOf(startTime.substring(14, 16));
        int second = Integer.valueOf(startTime.substring(17, 19));
        cal.set(year, month, day, hour, minute, second);
        start = cal.getTimeInMillis();

        year = Integer.valueOf(endTime.substring(0, 4));
        month = Integer.valueOf(endTime.substring(5, 7));
        day = Integer.valueOf(endTime.substring(8, 10));
        hour = Integer.valueOf(endTime.substring(11, 13));
        minute = Integer.valueOf(endTime.substring(14, 16));
        second = Integer.valueOf(endTime.substring(17, 19));
        cal.set(year, month, day, hour, minute, second);
        end = cal.getTimeInMillis();

        long studyTime = end - start;
        double speed = studyTime / (double) (audioMin * 60 * 1000);

        //0: 정상 10: 빠름, 11: 너무빠름, 20: 느림, 21 : 너무 느림
        if (speed < 0.6) {
            return 11;
        } else if (speed < 0.8) {
            return 10;
        } else if (speed > 1.4) {
            return 21;
        } else if (speed > 1.2) {
            return 20;
        }

        return 0;
    }

    /**
     * 오디오 학습에서 저장하는 자동 녹음 음원 파일 path 초기화함
     */
    public static void initAudioRecordPath() {
        StudyData data = StudyData.getInstance();

        String recordYmd = data.mRecordFileYmd.replace("-", "");
        String audioPrefix = String.format("%s_%d_%d_%s_%d_SA_", recordYmd, data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt);

        data.mAudioRecordPath = ServiceCommon.EXAM_RECORD_UPLOAD_PATH + audioPrefix;
        data.mAudioRecordIndex = getAudioRecordIndex(recordYmd, audioPrefix);
    }

    /**
     * 오디오 학습의 녹음할 파일의 index를 구함
     *
     * @param recordYmd  : 학습 계획 일자
     * @param filePrefix : 공통 파일 이름
     * @return index
     */
    public static int getAudioRecordIndex(String recordYmd, String filePrefix) {
        String[] recordDirList = {ServiceCommon.AUDIO_RECORD_PATH + recordYmd + "/", ServiceCommon.EXAM_RECORD_UPLOAD_PATH,};
        int maxNo = 0;

        for (int i = 0; i < recordDirList.length; i++) {
            File dir = new File(recordDirList[i]);
            if (dir.exists()) {
                File[] arrFile = dir.listFiles();

                if (arrFile != null) {
                    for (File file : arrFile) {
                        String fileName = file.getName();
                        if (fileName.contains(filePrefix)) {
                            fileName = fileName.replace(filePrefix, "");
                            fileName = fileName.replace(".mp3", "");

                            try {
                                fileName = fileName.substring(0, fileName.indexOf("_"));

                                if (Integer.valueOf(fileName) > maxNo) {
                                    maxNo = Integer.valueOf(fileName);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }

        return maxNo + 1;
    }

    /**
     * 학습 중 저장할 오디오 녹음 파일 path를 조회
     *
     * @param tagName : tag 이름(G1, S1, S0 ...)
     * @param tagTime : tag 시간 ms
     * @return 저장할 녹음 파일 path
     */
    public static String getAudioRecordPath(String tagName, int tagTime) {
        StudyData data = StudyData.getInstance();
        String recordPath = "";
        tagTime = tagTime / 1000;
        String name = "";
        //	String tagTimeStr = String.format("00%02d%02d", tagTime / 60, tagTime % 60);
        String tagTimeStr = String.valueOf(tagTime);
        String classId = String.valueOf(data.mStudyResultNo);
        name = "%s%d_%s_%s_%s.mp3";

        recordPath = String.format(name, data.mAudioRecordPath, data.mAudioRecordIndex, tagName, tagTimeStr, classId);
        data.mAudioRecordIndex++;
        data.mAudioRecordFilePath = recordPath;

        return recordPath;
    }

    /**
     * 병합파일 이름
     * <p>
     * : tag 이름(G1, S1, S0 ...)
     *
     * @param tagTime : tag 시간 ms
     * @return 저장할 녹음 파일 path
     */
    public static String getAudioMergeRecordPath(int tagTime, String classId, String mAudioRecordPath) {
        String recordPath = "";
        tagTime = tagTime / 1000;
        String name = "";
        //String tagTimeStr = String.format("00%02d%02d", tagTime / 60, tagTime % 60);
        String tagTimeStr = String.valueOf(tagTime);
        name = "%s%d_%s_%s_%s.mp3";
        recordPath = String.format(name, mAudioRecordPath, 1, "R", tagTimeStr, classId);
        return recordPath;
    }

    /**
     * popup학습 중 저장할 오디오 녹음 파일 path를 조회
     *
     * @param questionNo    : 문항코드 yoons20150120011948
     * @param questionOrder : (0, 1, 2)
     * @return 저장할 녹음 파일 path
     */
    public static String getPopupRecordPath(int studyOrder, String questionNo, int questionOrder) {
        StudyData data = StudyData.getInstance();
        String recordPath = "";

        String recordYmd = data.mRecordFileYmd.replace("-", "");
        String audioPrefix = String.format("%s_%d_%d_%s_%d_PU_", recordYmd, data.mCustomerNo, data.mProductNo, data.mStudyUnitCode, data.mReStudyCnt);

        recordPath = String.format("%s%d_%s_%d.mp3", ServiceCommon.EXAM_RECORD_UPLOAD_PATH + audioPrefix, studyOrder, questionNo, questionOrder);

        return recordPath;
    }

    /**
     * 에코를 재생할 녹음 파일 path를 조회
     *
     * @return 에코할 녹음 파일 path
     */
    public static String getAudioRecordPlayPath() {
        StudyData data = StudyData.getInstance();
        return data.mAudioRecordFilePath;
    }

    /**
     * 복습 전체 결과 clear
     */
    public static void clearReviewResult() {
        StudyData data = StudyData.getInstance();
        data.mReviewWordResult.clear();
        data.mWrongReviewWordQuestion.clear();
        data.mReviewSentenceResult.clear();
        data.mWrongReviewSentenceQuestion.clear();
    }

    /**
     * 복습 단어 결과 clear
     */
    public static void clearReviewWordResult() {
        StudyData data = StudyData.getInstance();
        data.mReviewWordResult.clear();
        data.mWrongReviewWordQuestion.clear();
    }

    /**
     * 복습 문장 결과 clear
     */
    public static void clearReviewSentenceResult() {
        StudyData data = StudyData.getInstance();
        data.mReviewSentenceResult.clear();
        data.mWrongReviewSentenceQuestion.clear();
    }

    /**
     * 단어 시험 결과 clear
     */
    public static void clearWordResult() {
        StudyData data = StudyData.getInstance();
        data.mWordResult.clear();
        data.mWrongWordQuestion.clear();
    }

    /**
     * 문장 시험 결과 clear
     */
    public static void clearSentenceResult() {
        StudyData data = StudyData.getInstance();
        data.mSentenceResult.clear();
        data.mWrongSentenceQuestion.clear();
    }

    /**
     * 문단 시험 결과 clear
     */
    public static void clearVanishingResult() {
        StudyData data = StudyData.getInstance();
        data.mVanishingResult.clear();
    }

    // 신규문항

    /**
     * popup 시험 결과 clear
     */
    public static void clearExamResult() {
        StudyData data = StudyData.getInstance();
        data.mExamResult.clear();
    }

    /**
     * 패키지 이름 추가 함수
     *
     * @param name : 패키지 이름
     * @return
     */
    public static String addPackageName(String name) {
        return ServiceCommon.STUDY_SERVER_URL + "?" + "p_nm" + "=" + name;
    }

    /**
     * 팝업퀴즈 숲용만 체크하기 위해 메모리에 담음
     *
     * @return
     */
    public static String[] getQuizPopList(String array) {
        String[] result;
        result = array.toString().split(",");

        return result;
    }

    public static Boolean isNewDictation(int seriesNo) {

        int[] dicArray = {5991, 5992, 5993, 5994, 5995, 5996, 5997, 5998, 5999, 6000, 6001, 6002, 6003, 6004, 6128, 6129, 6130, 6131, 6132, 6133, 6134, 6135, 6136, 6137, 6138, 6139, 6140, 6141, 6142, 6143, 6149, 6208, 6209, 6210, 6228, 6229};//new Dictation에 필요한 배열

        Arrays.sort(dicArray);//정렬

        int result = Arrays.binarySearch(dicArray, seriesNo);//해당 딕테이션이 있으면 0이상 출력
        return (result >= 0) ? true : false;
    }
}
