package com.yoons.fsb.student.util;

import android.os.Handler;

import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.TagEventData;
import com.yoons.fsb.student.data.TagEventData.TAG_EVENT_TYPE_ENUM;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Converter Class Tag File -> TagEventData 변환
 * 
 * @author jaek
 */
public class TagConverter {
	private static final String TAG = "TagConverter";

	private Handler mHandler = null;

	private Thread mSetTagEventDataThread = null;

	private ArrayList<TagEventData> mTagEventList = null;

	private String mTagFilePath = "";

	private boolean mIsTagWarning = false;

	private int mRecRTime = 0;//권장 녹음 시간

	public TagConverter(Handler handler, String tagFilePath) {
		mHandler = handler;
		mTagFilePath = tagFilePath;
	}

	/**
	 * 변환 Thread를 시작함
	 */
	public void start() {
		mSetTagEventDataThread = new Thread("tagConverterThread") {
			public void run() {
				File tagFile = new File(mTagFilePath);
				if (!tagFile.exists()) {
					sendSetResult();
					return;
				}

				BufferedReader in = null;
				try {
					in = new BufferedReader(new FileReader(mTagFilePath));
					int index = 0;
					int repeatIndex = 0, repeatStartTime = 0;
					int videoIndex = 0;
					int v1TagIndex = 0;
					// 신규문항
					int quizIndex = 1;
					int pageIndex = 0;
					//					int touchplayIndex = 1;
					//					int roleplayIndex = 1;
					boolean isJtagActive = false, isRtagActive = false, isOtagActive = false, isV1tagActive = false,isNUM1TagActive = false;
					String tagLine = null;
					mTagEventList = new ArrayList<TagEventData>();

					while (null != (tagLine = in.readLine())) {
						String hours = "", min = "", sec = "", msec = "";
						String tagLineTotal = "", eventTime = "", eventType = "", eventTypeArg = "";
						String[] eventExceptTimeAll = null;
						int ihours = 0, imin = 0, isec = 0, imsec = 0, iEventTime = 0;

						tagLineTotal = tagLine.trim();
						eventTime = tagLineTotal;
						String[] timeSplit = eventTime.substring(0, 12).split(":");
						int timeSplitLength = timeSplit.length;

						if (0 < timeSplitLength && 3 >= timeSplitLength) {
							hours = timeSplit[0];
							min = timeSplit[1];

							String[] secSplit = timeSplit[2].split("[.]");
							int secSplitLength = secSplit.length;

							if (0 < secSplitLength && 2 >= secSplitLength) {
								sec = secSplit[0];
								msec = secSplit[1];
							}

							ihours = Integer.valueOf(hours);
							imin = Integer.valueOf(min);
							isec = Integer.valueOf(sec);
							imsec = Integer.valueOf(msec);

							iEventTime = (ihours * 60 * 60 * 1000) + (imin * 60 * 1000) + (isec * 1000) + imsec;
						}

						// Tag 타입, 속성을 tab으로 구분
						eventExceptTimeAll = tagLineTotal.substring(13, tagLineTotal.length()).trim().split("\t");
						if (0 < eventExceptTimeAll.length)
							eventType = eventExceptTimeAll[0].trim();

						if (1 < eventExceptTimeAll.length)
							eventTypeArg = eventExceptTimeAll[1].trim();

						if (TAG_EVENT_TYPE_ENUM.checkEventType(eventType) < TAG_EVENT_TYPE_ENUM.checkEventType(TagEventData.TAG_END)) {

							//p1,p2 제외 총합계산할때 걸림
							if (eventType.equalsIgnoreCase(TagEventData.TAG_P1) || eventType.equalsIgnoreCase(TagEventData.TAG_P2))
								continue;

							if (eventType.equalsIgnoreCase(TagEventData.TAG_J2))
								isJtagActive = false;

							if (eventType.equalsIgnoreCase(TagEventData.TAG_V2))
								isV1tagActive = false;

							if (eventType.equalsIgnoreCase(TagEventData.TAG_O2))
								isOtagActive = false;

							if (eventType.contains(TagEventData.TAG_NUM2) && !(eventType.equalsIgnoreCase(TagEventData.TAG_R2) || eventType.equalsIgnoreCase(TagEventData.TAG_PAGE2) || eventType.equalsIgnoreCase(TagEventData.TAG_LG2))) {
								isNUM1TagActive = false;
							}

							// J1과 J2, V1과 V2 사이에 있는 태그를 제외함
							if (isJtagActive || isV1tagActive)
								continue;

							if (eventType.equalsIgnoreCase(TagEventData.TAG_J1))
								isJtagActive = true;

							// O1과 O2 사이에 있는 태그를 제외함
							if (isOtagActive) {
								if (!(eventType.equals(TagEventData.TAG_PAGE0) || eventType.equals(TagEventData.TAG_PAGE1))) {
									continue;
								}
                            }

							if (eventType.equalsIgnoreCase(TagEventData.TAG_O1))
								isOtagActive = true;

							if (eventType.equalsIgnoreCase(TagEventData.TAG_V1))
								isV1tagActive = true;

							if (eventType.contains(TagEventData.TAG_NUM1) && !(eventType.equalsIgnoreCase(TagEventData.TAG_R1) || eventType.equalsIgnoreCase(TagEventData.TAG_PAGE1) || eventType.equalsIgnoreCase(TagEventData.TAG_LG1)
									|| eventType.equalsIgnoreCase(TagEventData.TAG_O1))) {
								isNUM1TagActive = true;
							}

							if (!isNUM1TagActive && (eventType.equalsIgnoreCase(TagEventData.TAG_PAGE0) || eventType.equalsIgnoreCase(TagEventData.TAG_PAGE1))) {
								pageIndex++;
							}

							int vIndex = videoIndex;
							int v1Index = v1TagIndex;
							int rIndex = repeatIndex;
							int rStartTime = 0;
							int pIndex = pageIndex;

							//신규문항
							int qIndex = quizIndex;
							//							int tpIndex = touchplayIndex;
							//							int rpIndex = roleplayIndex;

							if (!eventType.equalsIgnoreCase(TagEventData.TAG_V))
								vIndex = -1;
							if (!eventType.equalsIgnoreCase(TagEventData.TAG_V1))
								v1Index = -1;

							// 신규문항
							if (!(eventType.equalsIgnoreCase(TagEventData.TAG_PQ0) || eventType.equalsIgnoreCase(TagEventData.TAG_PQ1)) && !(eventType.equalsIgnoreCase(TagEventData.TAG_PA0) || eventType.equalsIgnoreCase(TagEventData.TAG_PA1)) && !(eventType.equalsIgnoreCase(TagEventData.TAG_PI0) || eventType.equalsIgnoreCase(TagEventData.TAG_PI1)) && !(eventType.equalsIgnoreCase(TagEventData.TAG_PT0) || eventType.equalsIgnoreCase(TagEventData.TAG_PT1)) && !(eventType.equalsIgnoreCase(TagEventData.TAG_PR0) || eventType.equalsIgnoreCase(TagEventData.TAG_PR1)))
								qIndex = -1;

							//							// 신규문항
							//							if (!(eventType.equalsIgnoreCase(TagEventData.TAG_PQ0) || eventType.equalsIgnoreCase(TagEventData.TAG_PQ1) ||
							//									eventType.equalsIgnoreCase(TagEventData.TAG_PQ2)))
							//								qIndex = -1;
							//							
							//							if (!(eventType.equalsIgnoreCase(TagEventData.TAG_PT0) || eventType.equalsIgnoreCase(TagEventData.TAG_PT1) ||
							//									eventType.equalsIgnoreCase(TagEventData.TAG_PT2))) 
							//								tpIndex = -1;
							//							
							//							if (!(eventType.equalsIgnoreCase(TagEventData.TAG_PR0) || eventType.equalsIgnoreCase(TagEventData.TAG_PR1) ||
							//									eventType.equalsIgnoreCase(TagEventData.TAG_PR2))) 
							//								rpIndex = -1;
							//
							if (eventType.equalsIgnoreCase(TagEventData.TAG_R1)) {
								isRtagActive = true;
								repeatStartTime = iEventTime;
							}

							if (eventType.equalsIgnoreCase(TagEventData.TAG_R2)) {
								if (0 < repeatStartTime) {
									rStartTime = repeatStartTime;
									repeatStartTime = 0;
								}
							}

							if (!isRtagActive)
								rIndex = -1;

							TagEventData te = new TagEventData(index, eventType, iEventTime, eventTypeArg, rIndex, rStartTime, vIndex, v1Index, qIndex, pIndex);
							mTagEventList.add(te);

							if (eventType.equalsIgnoreCase(TagEventData.TAG_V))
								videoIndex++;
							if (eventType.equalsIgnoreCase(TagEventData.TAG_V1)) 
								v1TagIndex++;

							// 신규문항
							if (eventType.equalsIgnoreCase(TagEventData.TAG_PQ0) || eventType.equalsIgnoreCase(TagEventData.TAG_PQ1) || eventType.equalsIgnoreCase(TagEventData.TAG_PA0) || eventType.equalsIgnoreCase(TagEventData.TAG_PA1) || eventType.equalsIgnoreCase(TagEventData.TAG_PI0) || eventType.equalsIgnoreCase(TagEventData.TAG_PI1) || eventType.equalsIgnoreCase(TagEventData.TAG_PT0) || eventType.equalsIgnoreCase(TagEventData.TAG_PT1) || eventType.equalsIgnoreCase(TagEventData.TAG_PR0) || eventType.equalsIgnoreCase(TagEventData.TAG_PR1))
								quizIndex++;

							//							if (eventType.equalsIgnoreCase(TagEventData.TAG_PQ0) || eventType.equalsIgnoreCase(TagEventData.TAG_PQ1))
							//								quizIndex++;
							//							
							//							if (eventType.equalsIgnoreCase(TagEventData.TAG_PT0) || eventType.equalsIgnoreCase(TagEventData.TAG_PT1))
							//								touchplayIndex++;
							//							
							//							if (eventType.equalsIgnoreCase(TagEventData.TAG_PR0) || eventType.equalsIgnoreCase(TagEventData.TAG_PR1))
							//								roleplayIndex++;

							if (eventType.equalsIgnoreCase(TagEventData.TAG_R2)) {
								repeatIndex++;
								isRtagActive = false;
							}

							index++;
						}
					}

					checkTagEventData();

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (null != in) {
						try {
							in.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					sendSetResult();
				}
			}
		};

		mSetTagEventDataThread.start();
	}

	/**
	 * 변환 Thread를 종료함
	 */
	public void end() {
		if (null != mSetTagEventDataThread) {
			mSetTagEventDataThread.interrupt();
			mSetTagEventDataThread = null;
		}
	}

	/**
	 * 태그 정보 보정 처리
	 */
	private void checkTagEventData() {
		boolean isFixedRtag = false;
		int fixedRtagTime = 0, fixedTime = 0;
		long difference = 0;//권장녹음시간 temp
		for (int i = 0; i < mTagEventList.size(); i++) {
			TagEventData te = mTagEventList.get(i);
			String type = te.getEventType();
			String typeArg = te.getEventTypeArg();
			int time = te.getEventTime();
			int repStartTime = te.getRepeatStartTime();
			int quizeIndex = te.getQuizIndex();

			if (i <= mTagEventList.size() - 2) {
				int nextIndex = i + 1;
				TagEventData nextTe = mTagEventList.get(nextIndex);
				String nextType = nextTe.getEventType();
				String nextTypeArg = nextTe.getEventTypeArg();
				int nextTime = nextTe.getEventTime();
				int nextRepIndex = nextTe.getRepeatIndex();
				int nextRepStartTime = nextTe.getRepeatStartTime();
				int nextquizeIndex = nextTe.getQuizIndex();

				if (type.equalsIgnoreCase(TagEventData.TAG_R1) || type.equalsIgnoreCase(TagEventData.TAG_R2)) {//S1,2처음과 끝에 1초씩더하기(민트패드 동일)
					difference = difference + 1000;
				}
				if (type.contains(TagEventData.TAG_NUM1) && !type.equalsIgnoreCase(TagEventData.TAG_R1)) {

					if (nextType.equalsIgnoreCase(TagEventData.TAG_R1) || nextType.equalsIgnoreCase(TagEventData.TAG_R2)) {

						if (i <= mTagEventList.size() - 3) {
							int nextNextIndex = i + 2;
							TagEventData nextNextTe = mTagEventList.get(nextNextIndex);
							String nextNextType = nextNextTe.getEventType();
							String nextNextTypeArg = nextNextTe.getEventTypeArg();
							int nextNextTime = nextNextTe.getEventTime();
							int nextNextRepStartTime = nextNextTe.getRepeatStartTime();
							int nextNextquizeIndex = nextNextTe.getQuizIndex();
							isFixedRtag = true;

							/**
							 * R1, R2 태그가 선, 후 태그 사이 (같을 경우 포함) 에 있을 경우, “중간 시간
							 * 값”을 선 태그와 후 태그의 중간 값으로 할 때, R1, R2 태그가 “중간 시간 값”
							 * 보다 작거나 같을 경우 (선 태그 시간 값 – 200msec) 으로 보정한다. R1,
							 * R2 태그가 “중간 시간 값” 보다 클 경우 (후 태그 시간 값 + 200msec) 으로
							 * 보정한다.
							 */
							if (nextTime <= ((time + nextNextTime) / 2)) {
								fixedRtagTime = time - ServiceCommon.TAG_TIME_FIX_TERM_200 < 0 ? 0 : time - ServiceCommon.TAG_TIME_FIX_TERM_200;
								te.fixEventData(nextType, fixedRtagTime, nextTypeArg, nextRepIndex, nextRepStartTime, nextquizeIndex);
								nextTe.fixEventData(type, time, typeArg, nextRepIndex, repStartTime, quizeIndex);
								Log.i("", "Fixed Rtag Index : " + nextIndex + " -> " + i);
							} else {
								fixedRtagTime = nextNextTime + ServiceCommon.TAG_TIME_FIX_TERM_200;
								nextNextTe.fixEventData(nextType, fixedRtagTime, nextTypeArg, nextRepIndex, nextRepStartTime, nextquizeIndex);
								nextTe.fixEventData(nextNextType, nextNextTime, nextNextTypeArg, -1, nextNextRepStartTime, nextNextquizeIndex);
								Log.i("", "Fixed Rtag Index : " + nextIndex + " -> " + nextNextIndex);
							}
						}
					} else {
						if (!nextType.contains(TagEventData.TAG_NUM2))
							mIsTagWarning = true;
					}
				}
				/**
				 * 권장 녹음 시간
				 */
				if ((type.equals(TagEventData.TAG_G1) && nextType.equals(TagEventData.TAG_G2)) || (type.equals(TagEventData.TAG_LG1) && nextType.equals(TagEventData.TAG_LG2)) || (type.equals(TagEventData.TAG_LS1) && nextType.equals(TagEventData.TAG_LS2)) || (type.equals(TagEventData.TAG_S2) && nextType.equals(TagEventData.TAG_S1))) {

					difference = difference + (nextTime - time);

					Log.e(TAG, "type:" + type + "nextType:" + nextType + "mRecRTime:" + (nextTime - time) + ",nextTime - time :" + difference);
				}

				/**
				 * 선, 후 태그의 시간 차이가 200 msec 미만인 경우, 최소 200 msec 시간이 확보 되도록 후 태그의
				 * 시간 값을 조정
				 */
				fixedTime = te.getEventTime() + ServiceCommon.TAG_TIME_FIX_TERM_200;
				if (fixedTime > nextTe.getEventTime()) {
					nextTe.setEventTime(fixedTime);
					Log.i("", "Fixed Tag Index : " + nextIndex);
				}
			}

			if (isFixedRtag && type.equalsIgnoreCase(TagEventData.TAG_R2)) {
				te.setRepeatStartTime(fixedRtagTime);
				isFixedRtag = false;
			}

			if (ServiceCommon.IS_CONTENTS_TEST) {
				Log.i("", "eventIndex => " + te.getEventIndex() + " eventTime => " + CommonUtil.convMsecToMinSecMsec(te.getEventTime()) + " eventType => " + te.getEventType() + " repeatIndex => " + te.getRepeatIndex() + " repeatStartTime => " + CommonUtil.convMsecToMinSecMsec(te.getRepeatStartTime()) + " eventTypeArg => " + te.getEventTypeArg());
			}
		}

		mRecRTime = (int) (difference / 1000);
	}

	/**
	 * 변환 결과를 전달함
	 */
	private void sendSetResult() {
		int result = null != mTagEventList && 0 < mTagEventList.size() ? ServiceCommon.MSG_SUCCESS : ServiceCommon.MSG_FAIL;
		mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_CONVERT, result, mRecRTime, mTagEventList));
		if (mIsTagWarning)
			mHandler.sendEmptyMessage(ServiceCommon.MSG_WHAT_TAG_WARNING);
		end();
	}
}
