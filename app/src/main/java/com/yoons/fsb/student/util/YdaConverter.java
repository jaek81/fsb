package com.yoons.fsb.student.util;

import android.os.Handler;

import com.smartbefly.library.LibYDA;
import com.yoons.fsb.student.ServiceCommon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Converter Class 
 * Yda File -> Ogg File 변환 
 * 
 * @author jaek
 */
public class YdaConverter {
	
	private Handler mHandler = null;
	
	private Thread mConvYdaToOggThread = null;
	
	private String [] mYdaFilePathArray = null;
	private String [] mYdaTempFilePathArray = null;
	private String [] mOggFilePathArray = null;
	
	private boolean [] mIsConvSuccess = null;
	
	private int mErrorCode = 0;

	public YdaConverter(String ydaFilePath) {
		init(ydaFilePath);
	}
	
	public YdaConverter(Handler handler, String ydaFilePath) {
		mHandler = handler;
		init(ydaFilePath);
	}
	
	public YdaConverter(String[] ydaFilePathArray) {
		init(ydaFilePathArray);
	}
	
	public YdaConverter(Handler handler, String[] ydaFilePathArray) {
		mHandler = handler;
		init(ydaFilePathArray);
	}
	
	/**
	 * @param ydaFilePath Yda 파일 경로 ex. "Environment.getExternalStorageDirectory() + /yoonsdata/4138/1/4138_1_1.yda"
	 */
	private void init(String ydaFilePath) {
		String [] ydaFilePathArray = new String[1];
		ydaFilePathArray[0] = ydaFilePath;
		
		init(ydaFilePathArray);
	}
	
	/**
	 * Yda 파일 경로에 따른 Ogg 및 Temp 파일 경로를 초기화함 
	 * 
	 * @param ydaFilePathArray Yda 파일 경로 배열 
	 */
	private void init(String[] ydaFilePathArray) {
		mYdaFilePathArray = ydaFilePathArray;
		mYdaTempFilePathArray = new String[mYdaFilePathArray.length];
		mOggFilePathArray = new String[mYdaFilePathArray.length];
		mIsConvSuccess = new boolean[mYdaFilePathArray.length];
		
		for (int i = 0; i < ydaFilePathArray.length; i++) {
			mIsConvSuccess[i] = false;
			StringBuilder basePathSB = new StringBuilder();
			String ydaFilePath = ydaFilePathArray[i].trim();
			String [] pathArray = ydaFilePath.split("/");
			String ydaFileName = pathArray[pathArray.length-1];
			
			for (int j = 0; j < pathArray.length - 1; j++) {
				basePathSB.append(pathArray[j]);
				basePathSB.append("/");
			}
			
			mYdaTempFilePathArray[i] = (new StringBuilder().append(basePathSB).append(ServiceCommon.YDA_FILE_TEMP_HEAD).append(ydaFileName)).toString();
			
			String [] ydaFileNameSplit = ydaFileName.split("[.]");
			if (ydaFileNameSplit.length > 0) 
				mOggFilePathArray[i] = (new StringBuilder().append(basePathSB).append(ydaFileNameSplit[0]).append(ServiceCommon.OGG_FILE_TAIL)).toString();
		}
	}
	
	/**
	 * 변환 Thread를 시작함 
	 */
	public void start() {
		mConvYdaToOggThread = new Thread("ydaConverterThread") {
			public void run() {
				boolean isConvSuccess = false;
				
				for (int i = 0; i < mYdaFilePathArray.length; i++) {
					isConvSuccess = false;
					
					File fileYDA = new File(mYdaFilePathArray[i]);	
					if (!fileYDA.exists()) {
						mErrorCode = ServiceCommon.MSG_YDA_ERROR_FILE_NOT_FOUND;
						break;
					}
					
					fileCopy(mYdaFilePathArray[i], mYdaTempFilePathArray[i]);
					
					// Yda -> Ogg 변환 
					LibYDA libyda = new LibYDA();
			        int nRet = libyda.DecryptYDA(mYdaFilePathArray[i]);
			        
			        if (nRet == 0) {
			        	File fileYda = new File(mYdaFilePathArray[i]);
			        	File fileTempYda = new File(mYdaTempFilePathArray[i]);
			    		File fileOgg = new File(mOggFilePathArray[i]);
			    		
			    		if (fileOgg.exists())
			    			fileOgg.delete();
			    		
			    		if (fileYda.renameTo(fileOgg)) { 
			    			if (fileTempYda.exists())
			    				fileTempYda.renameTo(fileYda);
			    			isConvSuccess = true;
			    		}	
			        } else {
			        	mErrorCode = ServiceCommon.MSG_YDA_ERROR_LIB_DECRYPT;
			        	
			        	File fileYda = new File(mYdaFilePathArray[i]);
			        	if (fileYda.exists())
			        		fileYda.delete();
			        	
			        	File fileTempYda = new File(mYdaTempFilePathArray[i]);
			        	if (fileTempYda.exists())
			        		fileTempYda.delete();
			        }
			        
			        mIsConvSuccess[i] = isConvSuccess;
				}
				
				sendConvResult();
			}
		};
		
		mConvYdaToOggThread.start();
	}
	
	/**
	 * 변환 Thread를 종료함 
	 */
	public void end() {
		if (null != mConvYdaToOggThread) {
			mConvYdaToOggThread.interrupt();
			mConvYdaToOggThread = null;
		}
	}
	
	/**
	 * 변환 결과를 확인하여 성공 여부를 반환함 
	 * 
	 * @return 변환 결과 성공 여부 
	 */
	private boolean isConvCompleteSuccess() {
		boolean result = true;
		
		for (boolean success : mIsConvSuccess) {
			if (!success)
				result = false;
		}
		
		return result;
	}
	
	/**
	 * Yda 파일의 Temp 파일을 위해 Yda 파일을 복사함 
	 * 
	 * @param inFilePath 원복 Yda 파일
	 * @param outFilePath 복사본 Temp 파일 
	 */
	private void fileCopy(String inFilePath, String outFilePath) {
		try {
			// Yda 파일 복사본 생성 
			File fileTempYDA = new File(outFilePath);	
			if (fileTempYDA.exists())
				fileTempYDA.delete();
			
			fileTempYDA.createNewFile();
			
			FileInputStream finstream = null;
			FileOutputStream foutstream = null;
			FileChannel fcin = null, fcout = null;
			
			try {	
				finstream = new FileInputStream(inFilePath);
				foutstream = new FileOutputStream(outFilePath);
				
				fcin = finstream.getChannel();
				fcout = foutstream.getChannel();
				
				long size = fcin.size();
				fcin.transferTo(0, size, fcout);
			} catch (Exception e) {
				mErrorCode = ServiceCommon.MSG_YDA_ERROR_FILE_COPY;
			} finally {
				if (null != fcin)
					fcin.close();
				
				if (null != fcout)
					fcout.close();
				
				if (null != foutstream)
					foutstream.close();
				
				if (null != finstream)
					finstream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			mErrorCode = ServiceCommon.MSG_YDA_ERROR_FILE_COPY;
		}
	}
	
	/**
	 * 변환 결과를 전달함  
	 */
	private void sendConvResult() {
		if (null != mHandler) {
			int result = isConvCompleteSuccess() ? ServiceCommon.MSG_SUCCESS : ServiceCommon.MSG_FAIL;
			mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_CONVERT, result, mErrorCode));
		}
		end();
	}
}
