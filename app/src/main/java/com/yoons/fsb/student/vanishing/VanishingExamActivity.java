package com.yoons.fsb.student.vanishing;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.data.StudyData.VanishingQuestion;
import com.yoons.fsb.student.network.HttpJSONRequest;
import com.yoons.fsb.student.ui.DictationNewTitlePaperActivity;
import com.yoons.fsb.student.ui.DictationTitlePaperActivity;
import com.yoons.fsb.student.ui.MovieReviewTitlePaperActivity;
import com.yoons.fsb.student.ui.OneWeekTitlePaperActivity;
import com.yoons.fsb.student.ui.StudyOutcomeActivity;
import com.yoons.fsb.student.ui.base.BaseDialog;
import com.yoons.fsb.student.ui.base.BaseDialog.OnDialogDismissListener;
import com.yoons.fsb.student.ui.base.BaseStudyActivity;
import com.yoons.fsb.student.ui.control.StepStatusBar;
import com.yoons.fsb.student.ui.popup.MessageBox;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;
import com.yoons.fsb.student.vanishing.state.VanishingState;
import com.yoons.fsb.student.vanishing.state.impl.StepInit;
import com.yoons.fsb.student.vanishing.state.impl.StepRetry;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

/**
 * 문단 연습 학습 페이지
 * 
 * @author nexmore
 * 
 */
public class VanishingExamActivity extends BaseStudyActivity implements OnDialogDismissListener {

	private int MSGBOX_ID_STEP_COMPLETE = 0; // 업데이트 메시지 박스 id
	private int MSGBOX_ID_STEP_COMPLETE_RETRY = 1; // 업데이트 메시지 박스 id

	//private StepStatusBar mStepStatusBar = null;
	private VanishingView mVanishingView;
	private VanishingState mState = null;

	private int curIndex;

	private static int STEP_COMPLETE_DELAY_TIME = 3000;

	private TextView tBookName = null, tCategory = null, tCategory2 = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.vanishing_cloze_main);

		mStudyData = StudyData.getInstance();

		setWidget();

		// TitleView
		tBookName.setText(mStudyData.mProductName);
		tCategory.setText(R.string.string_common_today_study);
		tCategory2.setText(R.string.string_common_vanishing_exam);

		curIndex = 0;

		if (ServiceCommon.IS_CONTENTS_TEST) {
			readyStudy();
		} else {
			requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_START);
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// FlurryAgent.onStartSession(this, ServiceCommon.FLURRY_API_KEY);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		/* FlurryAgent.onEndSession(this); */
	}

	@Override
	protected void onPause() {
		// 학습 중 화면 잠금, 꺼짐 시 간지로 재시작
		if (!isFinishing()) {
			if (mIsReStart) {
				StudyDataUtil.clearVanishingResult();
				startActivity(new Intent(this, VanishingTitlePaperActivity.class)
						.putExtra(ServiceCommon.PARAM_MUTE_GUIDE, true));
			}

			setFinish();
			finish();
		}

		super.onPause();
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		mVanishingView.recordPlayDestroy();
		mVanishingView = null;
		mState = null;
		super.finish();
	}

	/**
	 * layout을 설정함
	 */
	private void setWidget() {

		// TitleView
		tBookName = (TextView) findViewById(R.id.title_book_name);
		tCategory = (TextView) findViewById(R.id.title_category1);
		tCategory2 = (TextView) findViewById(R.id.title_category2);

		//setTitlebarCategory(getString(R.string.string_titlebar_category_study));
		//setTitlebarText(mStudyData.mProductName);
		// WiFi 감도 아이콘 설정
		setPreferencesCallback();

		//mStepStatusBar = (StepStatusBar) findViewById(R.id.setp_statusbar);
		//mStepStatusBar.setHighlights(StepStatusBar.STATUS_VANISHING_EXAM);

		mVanishingView = (VanishingView) findViewById(R.id.layout_vanishing);

		curIndex = 0;
	}

	/**
	 * 학습 스테이트 변경
	 * 
	 * @param _state
	 */
	public void changeState(VanishingState _state) {
		if (mState != null) {
			mState.Destory();
		}
		_state.Init();
		mState = _state;
	}

	/**
	 * 문단 시험 음원 재생 완료
	 * 
	 * @param duration
	 */
	public void sendVorbisComplete(int duration) {
		if (mState != null) {
			mState.playerComplete(duration);
		}
	}

	/**
	 * 회원 녹음 완료
	 */
	public void sendRecordComplete() {
		if (mState != null) {
			mState.reocordComplete();
		}
	}

	/**
	 * 회원 녹음된 에코 재생 완료
	 */
	public void sendEchoComplete() {
		if (mState != null) {
			mState.echoComplete();
		}
	}

	/**
	 * 5초 카운트 완료
	 */
	public void endFiveTimeCounter() {
		if (mState != null) {
			mState.endTimeCounter();
		}
	}

	/**
	 * 고객 번호 반환
	 * 
	 * @return
	 */
	public int getCustomerNo() {
		return mStudyData.mCustomerNo;
	}

	/**
	 * 문단 연습 완료
	 * 
	 * @param scroe
	 */
	public void stepComplete(int scroe) {
		mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
				ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION, scroe), STEP_COMPLETE_DELAY_TIME);
	}

	/**
	 * 문단 연습 완료 팝업
	 * 
	 * @param score
	 */
	private void showStepComple(int score) {
		String str = String.format(getString(R.string.string_vanishing_step_complete), (curIndex + 1),
				mStudyData.mVanishingQuestion.size());
		// if (!mStudyData.mIsAudioExist) {
		if (mStudyData.mStudyUnitCode.contains("V")) {
			if (curIndex < mStudyData.mVanishingQuestion.size() - 1) {
				curIndex++;
				mHandler.sendMessageDelayed(
						mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0),
						500);
			} else {
				if (ServiceCommon.IS_CONTENTS_TEST) {
					goNextStatus();
				} else {
					requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
				}
			}
			return;
		}

		if (score >= 3) {
			mMsgBox = new MessageBox(this, 0, str);
			mMsgBox.setOnDialogDismissListener(this);
			mMsgBox.setConfirmText(R.string.string_common_ok);
			mMsgBox.setId(MSGBOX_ID_STEP_COMPLETE);
			mMsgBox.show();
		} else {
			mMsgBox = new MessageBox(this, 0, str);
			mMsgBox.setOnDialogDismissListener(this);
			mMsgBox.setConfirmText(R.string.string_msg_update_retry);
			mMsgBox.setCancelText(R.string.string_common_ok);
			mMsgBox.setId(MSGBOX_ID_STEP_COMPLETE_RETRY);
			mMsgBox.show();
		}

	}

	/**
	 * 문단 연습 시작
	 */
	private void startVanishingCloze() {
		if (curIndex >= mStudyData.mVanishingQuestion.size()) {
			curIndex = mStudyData.mVanishingQuestion.size() - 1;//넘는경우가 잇어서
		}
		VanishingWord word = new VanishingWord(mStudyData.mVanishingQuestion.get(curIndex));
		StepInit init = new StepInit(this, mVanishingView, word);
		mVanishingView.setQuestionCnt((curIndex + 1) + "/" + mStudyData.mVanishingQuestion.size());
		changeState(init);

		if (!mIsStudyStart)
			mIsStudyStart = true;
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (null == mContext)
				return;

			switch (msg.what) {
			case ServiceCommon.MSG_WHAT_STUDY:
				if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_START) {
					startVanishingCloze();
				} else if (msg.arg1 == ServiceCommon.MSG_STUDY_PROGRESS_COMPLETION) {
					showStepComple(msg.arg2);
				}
				break;

			default:
				super.handleMessage(msg);
			}
		}
	};

	@Override
	public void onDialogDismiss(int result, int dialogId) {
		// TODO Auto-generated method stub
		if (MSGBOX_ID_STEP_COMPLETE_RETRY == dialogId) {
			if (result == BaseDialog.DIALOG_CONFIRM) {
				VanishingQuestion question = mStudyData.mVanishingQuestion.get(curIndex);
				question.mRetryCnt++;
				VanishingWord word = new VanishingWord(question);

				changeState(new StepRetry(this, mVanishingView, word));
			} else if (BaseDialog.DIALOG_CANCEL == result) {
				if (curIndex < mStudyData.mVanishingQuestion.size() - 1) {
					curIndex++;
					mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY,
							ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
				} else {
					if (ServiceCommon.IS_CONTENTS_TEST) {
						goNextStatus();
					} else {
						requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
					}
				}

			}
		} else if (MSGBOX_ID_STEP_COMPLETE == dialogId) {
			if (curIndex < mStudyData.mVanishingQuestion.size() - 1) {
				curIndex++;
				mHandler.sendMessageDelayed(
						mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0),
						500);
			} else {
				if (ServiceCommon.IS_CONTENTS_TEST) {
					goNextStatus();
				} else {
					requestServerTimeSync(ServiceCommon.REQUEST_ID_TIME_SYNC_END);
				}
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (!mIsStudyStart)
			return false;

		switch (keyCode) {
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PLAY:
		case ServiceCommon.BLUETOOTH_KEY_EVENT_MEDIA_PAUSE:
			ImageView btnRecord;
			btnRecord = (ImageView) findViewById(R.id.vanishing_cloze_record_btn);
			if (btnRecord != null && btnRecord.isEnabled()) {
				btnRecord.performClick();
			}
			break;

		default:
			return super.onKeyDown(keyCode, event);
		}

		return false;
	}

	@Override
	public void onClick(View v) {
		if (!mIsStudyStart)
			return;

		if (singleProcessChecker())
			return;
	}

	/**
	 * 서버 시간 요청
	 * 
	 * @param type
	 */
	private void requestServerTimeSync(int type) {
		HttpJSONRequest request = new HttpJSONRequest(mContext);
		request.requestServerTimeSync(mNetworkHandler, type);
	}

	/**
	 * 문단 연습 학습 준비
	 */
	private void readyStudy() {
		if (mStudyData.mStudyUnitCode.contains("V")) {
			StudyDataUtil.setCurrentStudyStatus(this, "RFN");
			StudyDataUtil.setCurrentStudyStatus(this, "S11");
			StudyDataUtil.setCurrentStudyStatus(this, "S12");
		}
		StudyDataUtil.setCurrentStudyStatus(this, "S71");
		mHandler.sendMessageDelayed(
				mHandler.obtainMessage(ServiceCommon.MSG_WHAT_STUDY, ServiceCommon.MSG_STUDY_PROGRESS_START, 0), 500);
	}

	/**
	 * 다음 단계로 이동 처리함
	 */
	private void goNextStatus() {
		if (isNext) {
			isNext = false;
			if (mStudyData.mIsDictation && 0 < mStudyData.mDictation.size())
				startActivity(new Intent(this, DictationTitlePaperActivity.class));
			else if (mStudyData.isNewDication && mStudyData.mIsDicPirvate)
				startActivity(new Intent(this, DictationNewTitlePaperActivity.class));
			else if (mStudyData.mIsMovieExist)
				startActivity(new Intent(this, MovieReviewTitlePaperActivity.class));
			else if (mStudyData.mOneWeekData.size() > 0)
				startActivity(new Intent(this, OneWeekTitlePaperActivity.class));
			else
				startActivity(new Intent(this, StudyOutcomeActivity.class));

			// startActivity(new Intent(this, StudyOutcomeActivity.class));
			StudyDataUtil.setCurrentStudyStatus(this, "S72");
			finish();
		}
	}

	/**
	 * Handler
	 */
	private Handler mNetworkHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case ServiceCommon.MSG_HTTP_REQUEST_SUCCESS:
				if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
					Log.k("wusi12", "--- S71 -----------");
					Log.k("wusi12", "Server Time : " + msg.obj.toString());
					JSONObject objTime = (JSONObject) msg.obj;

					String serverTime;
					try {
						serverTime = objTime.getString("out1");
						CommonUtil.syncServerTime(serverTime, VanishingExamActivity.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						readyStudy();
						Log.k("wusi12", "--------------------");
					}
				} else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
					Log.k("wusi12", "-------S72-----------");
					Log.k("wusi12", "Server Time : " + msg.obj.toString());
					JSONObject objTime = (JSONObject) msg.obj;

					String serverTime;
					try {
						serverTime = objTime.getString("out1");
						CommonUtil.syncServerTime(serverTime, VanishingExamActivity.this);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						goNextStatus();
						Log.k("wusi12", "--------------------");
					}
				}
				break;
			case ServiceCommon.MSG_HTTP_REQUEST_FAIL:
				if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_START) {
					readyStudy();
				} else if (msg.arg1 == ServiceCommon.REQUEST_ID_TIME_SYNC_END) {
					goNextStatus();
				}
				break;

			default:
				super.handleMessage(msg);
			}
		}
	};

}
