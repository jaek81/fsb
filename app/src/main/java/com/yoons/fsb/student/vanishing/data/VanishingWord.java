package com.yoons.fsb.student.vanishing.data;

import com.yoons.fsb.student.data.StudyData.VanishingQuestion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 문단 파싱 클래스 
 * 문단 연습 <u1>,<u2>,<u3> 태그가 존재하는 String을 HashMap을 이용하여 분리 한다.
 * @author nexmore
 *
 */
public class VanishingWord {
	
	private VanishingQuestion mWord;
	private ArrayList<String> strList;
	private Map<Integer, ArrayList<Integer>> mapList;

	public VanishingWord(VanishingQuestion word){
		this.mWord = word;
		strList = new ArrayList<String>();
		mapList = new HashMap<Integer, ArrayList<Integer>>();
		parsingData(word.mSentence);
	}
	
	public VanishingQuestion getVanishingQuestion() {
		return mWord;
	}
	
	public String getAudioPath() {
		return mWord.mSoundFile;
	}
	
	public String getRecodePath(){
		return mWord.mRecordFile;
	}
	
	public String getTextSentence(){
		return mWord.mTextSentence;
	}
	
	public ArrayList<String> getStrList() {
		return strList;
	}
	
	public Map<Integer, ArrayList<Integer>> getMapList() {
		return mapList;
	}
	
	public int getTotalLength(){
		return mWord.mTextSentence.length();
	}
	
	public String[] getVanishingWordList(){
		int cnt = getVanishingWordCnt();
		int vcIndex = 0;
		String[] wordList = new String [cnt];
		for(int i = 1; i <=3; i++){
			ArrayList<Integer> index = mapList.get(i);
			for(int j = 0; j<index.size();j++){
				wordList[vcIndex] = strList.get(index.get(j));
				vcIndex++;
			}
		}
		return wordList;
	}
	
	public int getVanishingWordCnt(){
		int cnt = 0;
		for(int i = 1; i <=3; i++){
			ArrayList<Integer> index = mapList.get(i);
			for(int j = 0; j<index.size();j++){
				cnt++;
			}
		}
		return cnt;
	}
	
	/**
	 * <u1>,<u2>,<u3> 태그 파싱하여  ArrayList 밍 HashMap에 저장한다.
	 * @param data
	 */
	private void parsingData(String data){
		String[] array = data.split("<u"); //<u를 제거한 배열로만듬
		for(int i = 0; i< array.length; i++){
			String msg = array[i]; //배열 하나불러냄
			int index  = msg.indexOf(">");//msg에 > 이 몇뻔재 잇는지 확인
			if(index == -1){
				//strList.add(msg);
				addTextDevider(msg);//없으면 
			} else {//태그가 들은 메시지일때
				int strIndex = Integer.parseInt(msg.substring(0, index));//어떤태그인지 뽑아냄
				  
				String[] strSub = msg.substring(index+1).split("</u");//태그 숫자 다음꺼까지 자르고 태그</u제거 해서 배열에 넣음  1>은남아잇음
				for(int j = 0; j<strSub.length;j++){
					int indexSub  = strSub[j].indexOf(">");
					if(indexSub == -1){//태그가 없다면
						if(mapList.containsKey(strIndex)){//첫번째 리스트가 있는지 확인 있다면
							ArrayList<Integer> indexList = mapList.get(strIndex);
							indexList.add(strList.size());
						} else {//없으면
							ArrayList<Integer> indexList = new ArrayList<Integer>();
							indexList.add(strList.size());
							mapList.put(strIndex, indexList);//숫자 태그 뽑아와서 글자(배열) 크기 집어늠 //번째는 앞단걲지 포함해서
						}
						strList.add(strSub[j]);//더함 근데 왜더하지....
						
					} else {
						addTextDevider(strSub[j].substring(indexSub+1));
					}
				}
				
			}
		}
	}
	
	/**
	 * 단어를 ArrayList에 저장한다.
	 * @param message
	 */
	private void addTextDevider(String message){
		int size = message.length();//태그 앞뒤까지 내용
		int len = 0;
		String[] array = message.split(" ");//공백으로 배열갈음
		for(int i = 0; i< array.length; i++){
			String msg = array[i];
			len += msg.length(); //배열 하나의 길이를 구함
			if(len< size){//해당길이<전체길이 일때
				msg+=" ";//메시지를 더함
			}
			strList.add(msg);//strList배열에 추가
		}
		if(len == 0){
			strList.add(" ");
		}
	}
}
