package com.yoons.fsb.student.vanishing.layout;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.custom.CustomVorbisPlayer;
import com.yoons.fsb.student.custom.CustomWavRecoder;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;

import java.io.File;

/**
 * 문단 연습 하단 3버튼(음원재생, 녹음, 에커재생 ) 레이아웃
 *
 * @author nexmore
 */
public class VanishingRecordPlayer extends LinearLayout implements OnClickListener {

    /**
     * 문단 연습 리스터
     *
     * @author nexmore
     */
    public interface VanishingRecorderListener {
        /**
         * 음원 재생 완료
         *
         * @param duration
         */
        public void OnVorbisPlayComplete(int duration);

        /**
         * 문단연습 녹음 완료
         */
        public void onRecordComplete();

        /**
         * 에코 재생 완료
         */
        public void onEchoComplete();
    }

    private VanishingRecorderListener listener;

    private Context mContext;
    private LayoutInflater mInflater;

    private ImageView btnListen;
    private ImageView btnRecord;
    //private ImageView btnRecord2;
    private ImageView btnEcho;

    private CustomVorbisPlayer mVorbisPlayer;
    private CustomWavRecoder mWavRecorder;
    private MediaPlayer mRecordNotiPlayer;
    private MediaPlayer mRecordPlayer;

    private int mMaxRecTime = 0;

    private AnimationDrawable aniIng;
    private AnimationDrawable aniComplete;

    public VanishingRecordPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub

        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);

        if (CommonUtil.isCenter()) // igse
            mInflater.inflate(R.layout.igse_vanishing_record_player, this, true);
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                mInflater.inflate(R.layout.w_vanishing_record_player, this, true);
            else // 숲
                mInflater.inflate(R.layout.f_vanishing_record_player, this, true);
        }

        initLayout();
    }

    /**
     * 레이아웃 초기화
     */
    private void initLayout() {
        btnListen = (ImageView) findViewById(R.id.vanishing_cloze_listen_btn);
        btnRecord = (ImageView) findViewById(R.id.vanishing_cloze_record_btn);
        //btnRecord2 = (ImageView) findViewById(R.id.vanishing_cloze_record_btn2);

        btnRecord.setEnabled(false);
        btnRecord.setOnClickListener(this);
        btnEcho = (ImageView) findViewById(R.id.vanishing_cloze_recordplay_btn);

        aniIng = (AnimationDrawable) getResources().getDrawable(R.drawable.c_seq_learning_record_volume);

        if (CommonUtil.isCenter()) // igse
            aniComplete = (AnimationDrawable) getResources().getDrawable(R.drawable.igse_seq_learning_progress);
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                aniComplete = (AnimationDrawable) getResources().getDrawable(R.drawable.w_seq_learning_progress);
            else // 숲
                aniComplete = (AnimationDrawable) getResources().getDrawable(R.drawable.f_seq_learning_progress);
        }
    }

    /**
     * 문단연습 리스너 설정
     *
     * @param listener
     */
    public void setOnVanishingRecorderListener(VanishingRecorderListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v.getId() == R.id.vanishing_cloze_record_btn) {
            recordEnd();
        }

    }


    public void setBtnListenEnable() {
        btnListen.setEnabled(true);
        btnRecord.setEnabled(false);
        btnEcho.setEnabled(false);
    }

    public void setBtnEchoEnable() {
        btnListen.setEnabled(false);
        btnRecord.setEnabled(false);
        btnEcho.setEnabled(true);
    }

    public void setBtnDisEnableAll() {
        btnListen.setEnabled(false);
        btnRecord.setEnabled(false);
        btnEcho.setEnabled(false);
    }

    public void setAudioPlay(String oggFilePath) {
        setBtnListenEnable();
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_PLAYER);
        mVorbisPlayer = new CustomVorbisPlayer(mHandler, oggFilePath);
        mVorbisPlayer.play();

        playerResume();
    }

    public void stopAudioPlay() {
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_PLAYER, ServiceCommon.MSG_PROGRESS_COMPLETION, 0), 100);
    }

    public void setRecordPlayer(String path, int maxRecTime) {
        String recordPath = path.replace(ServiceCommon.WAV_FILE_TAIL, "");
        btnRecord.setEnabled(false);

        if (CommonUtil.isCenter()) // igse
            btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record_n));
            else // 숲
                btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record_n));
        }

        mWavRecorder = new CustomWavRecoder(recordPath);

        //this.mMaxRecTime = maxRecTime;
        this.mMaxRecTime = getMaxRecordTime(ServiceCommon.MAX_REC_TIME_20SEC, maxRecTime);
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_READY, 0), 100);
    }

    public void stopRecordPlay() {
        mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), 700);
    }

    /**
     * 최대 녹음 시간(녹음 자동 종료 시간)을 계산하여 반환함
     *
     * @param baseMaxRecordTime 20초
     * @param playDuration      문장 재생 시간
     * @return 최대 녹음 시간
     */
    private int getMaxRecordTime(int baseMaxRecordTime, int playDuration) {
        Log.i("", "getMaxRecordTime playDuration => " + playDuration);

        int maxRecordTime = baseMaxRecordTime;
        int customMaxRecordTime = playDuration + (playDuration * 2);

        if (baseMaxRecordTime < customMaxRecordTime)
            maxRecordTime = playDuration * 2;
        else
            maxRecordTime = baseMaxRecordTime - playDuration;

        Log.i("", "getMaxRecordTime customMaxRecordTime => " + customMaxRecordTime + " maxRecordTime => " + maxRecordTime);

        return maxRecordTime;
    }

    /**
     * 녹음 시작 알림음을 재생함
     */
    private void recordNotiPlay() {
        if (null != mRecordNotiPlayer)
            return;

        mRecordNotiPlayer = MediaPlayer.create(mContext, R.raw.ding);

        if (null != mRecordNotiPlayer) {
            mRecordNotiPlayer.setOnCompletionListener(mRecordNotiCompletion);
            mRecordNotiPlayer.start();
        }
    }

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordNotiCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (null == mContext)
                return;

            recordNotiEnd();
            recordStart();
        }
    };

    /**
     * 녹음 시작 알림음을 종료함
     */
    private void recordNotiEnd() {
        if (null == mRecordNotiPlayer)
            return;

        if (mRecordNotiPlayer.isPlaying())
            mRecordNotiPlayer.stop();
        mRecordNotiPlayer.release();
        mRecordNotiPlayer = null;
    }

    /**
     * 녹음을 시작하고 1초 뒤 녹음 완료를 진행 할 수 있도록 녹음 버튼을 활성화 함 듣기
     */
    private void recordStart() {
        //btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.btn_record_start));
        btnRecord.setEnabled(true);
        btnRecord.setImageDrawable(aniIng);

        if (CommonUtil.isCenter()) // Igse
            ((LinearLayout) btnRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.igse_btn_learning_record_bg_n));
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) //우영
                ((LinearLayout) btnRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.w_btn_learning_record_bg_n));
            else // 숲
                ((LinearLayout) btnRecord.getParent()).setBackground(getResources().getDrawable(R.drawable.f_btn_learning_record_bg_n));
        }

        mWavRecorder.startRecording();

        if (0 < mMaxRecTime)
            mHandler.sendMessageDelayed(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_AUTO_END, 0), mMaxRecTime);

        aniIng.start();
    }


    /**
     * 녹음을 종료함
     */
    private void recordEnd() {
        mHandler.removeMessages(ServiceCommon.MSG_WHAT_RECORDER);
        btnRecord.setEnabled(false);
        mWavRecorder.stopRecording();

        aniIng.stop();
        ((LinearLayout) btnRecord.getParent()).setBackground(null);
        ((LinearLayout) btnRecord.getParent()).setBackground(aniComplete);
        //btnRecord.setImageDrawable(aniComplete);

        if (CommonUtil.isCenter()) // igse
            btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record2));
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record2));
            else // 숲
                btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record2));
        }
        aniComplete.setOneShot(true);
        aniComplete.start();

        checkAniEnd();
    }

    public void checkAniEnd() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                // TODO Auto-generated method stub
                while (aniComplete.isRunning()) {
                    Drawable draw = aniComplete.getCurrent();
                    Drawable end_draw = aniComplete.getFrame(aniComplete.getNumberOfFrames() - 1);
                    if (draw == end_draw) {
                        break;
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);
                aniComplete.stop();
                ((LinearLayout) btnRecord.getParent()).setBackground(null);
                //btnRecord2.setImageResource(android.R.color.transparent);

                if (CommonUtil.isCenter()) // igse
                    btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_record));
                else {
                    if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                        btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_record));
                    else // 숲
                        btnRecord.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_record));
                }

                btnRecord.setEnabled(false);
                if (listener != null) {
                    listener.onRecordComplete();
                }
            }
        }.execute();
    }


    public int setEchoPlay(String recordPlayPath) {
        int echoDuration = 0;

        if (CommonUtil.isCenter()) // igse
            btnEcho.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_repeat2));
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                btnEcho.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_repeat2));
            else // 숲
                btnEcho.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_repeat2));
        }

        try {
            mRecordPlayer = new MediaPlayer();
            mRecordPlayer.reset();
            mRecordPlayer.setDataSource(recordPlayPath);
            mRecordPlayer.setOnCompletionListener(mRecordEchoCompletion);
            mRecordPlayer.setOnPreparedListener(mRecordEchoPrepare);
            mRecordPlayer.prepare();
            echoDuration = mRecordPlayer.getDuration();
            Log.i("", "recordPlay echoDuration => " + echoDuration);

            final LinearLayout parent = ((LinearLayout) btnEcho.getParent());
            AnimationDrawable ad = null;
            ad = aniComplete;

            final AnimationDrawable ing = new AnimationDrawable();
            final int frameTime = echoDuration / ad.getNumberOfFrames();
            Log.i("", "duration => " + echoDuration + " frame time => " + frameTime);
            ing.setOneShot(true);
            for (int i = 0; i < ad.getNumberOfFrames(); i++) {
                Drawable draw = ad.getFrame(i);
                ing.addFrame(draw, frameTime);
            }
            parent.setBackground(ing);
            ((AnimationDrawable) parent.getBackground()).start();

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((LinearLayout) btnEcho.getParent()).setBackground(null);
                    btnEcho.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_repeat_d));
                }
            }, echoDuration);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return echoDuration;
    }

    /**
     * 녹음 시작 알림음 재생 완료 리스너
     */
    private MediaPlayer.OnCompletionListener mRecordEchoCompletion = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            //btnEcho.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_repeat_d));
            if (listener != null) {
                listener.onEchoComplete();
            }
        }
    };

    private MediaPlayer.OnPreparedListener mRecordEchoPrepare = new MediaPlayer.OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {
            // TODO Auto-generated method stub
            ((VanishingExamActivity) mContext).setEchoVolume();
            mRecordPlayer.start();
        }
    };

    /**
     * Player를 Resume함
     */
    private void playerResume() {
        if (CommonUtil.isCenter()) // igse
            btnListen.setImageDrawable(getResources().getDrawable(R.drawable.igse_btn_learning_listen2));
        else {
            if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
                btnListen.setImageDrawable(getResources().getDrawable(R.drawable.w_btn_learning_listen2));
            else // 숲
                btnListen.setImageDrawable(getResources().getDrawable(R.drawable.f_btn_learning_listen2));
        }

        listenIngEffect();
    }

    private void listenIngEffect() {
        int duration = mVorbisPlayer.getDuration();
        AnimationDrawable ad = null;
        final LinearLayout parent = ((LinearLayout) btnListen.getParent());
        ad = aniComplete;
        final AnimationDrawable ing = new AnimationDrawable();
        final int frameTime = duration / ad.getNumberOfFrames();

        for (int i = 0; i < ad.getNumberOfFrames(); i++) {
            Drawable draw = ad.getFrame(i);
            ing.addFrame(draw, frameTime);
        }
        ing.setOneShot(true);
        parent.setBackground(ing);
        ((AnimationDrawable) parent.getBackground()).start();
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case ServiceCommon.MSG_WHAT_PLAYER:
                    if (null == mVorbisPlayer) {
                        return;
                    }

                    if (msg.arg1 == ServiceCommon.MSG_PROGRESS_COMPLETION) {
                        btnListen.setImageDrawable(getResources().getDrawable(R.drawable.c_btn_learning_listen_d));
                        ((LinearLayout) btnListen.getParent()).setBackground(null);
                        btnListen.setEnabled(false);
                        int duration = mVorbisPlayer.getDuration();
                        mVorbisPlayer.stop();
                        mVorbisPlayer.release();
                        mVorbisPlayer = null;

                        if (listener != null) {
                            listener.OnVorbisPlayComplete(duration);
                        }


                    }

                    break;

                case ServiceCommon.MSG_WHAT_RECORDER:
                    if (null == mWavRecorder)
                        return;

                    if (msg.arg1 == ServiceCommon.MSG_REC_READY) {
                        recordNotiPlay();
                    } else if (msg.arg1 == ServiceCommon.MSG_REC_AUTO_END) {
                        recordEnd();
                    }
                    break;


                default:
                    super.handleMessage(msg);
            }
        }
    };

    public void playerDestory() {
        if (mVorbisPlayer != null) {
            mVorbisPlayer.stop();
            mVorbisPlayer.release();
            mVorbisPlayer = null;
        }
    }
}
