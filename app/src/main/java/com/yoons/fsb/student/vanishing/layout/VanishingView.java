package com.yoons.fsb.student.vanishing.layout;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Preferences;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingRecordPlayer.VanishingRecorderListener;

import java.util.ArrayList;

/**
 * 문단 연습 View 레이아웃
 * 
 * @author nexmore
 *
 */
public class VanishingView extends LinearLayout implements VanishingRecorderListener {

	private Context mContext;
	private LayoutInflater mInflater;

	private TextView txtStepInfo;
	private TextView txtQuestionCnt;
	private LinearLayout textLinearLayout;
	private CountDownTimer mCountTimer = new CountDownTimer(7000, 1000) {
		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			((VanishingExamActivity) mContext).endFiveTimeCounter();
		}
	};;

//	private ImageView timer_icon1;
//	private ImageView timer_icon2;
//	private ImageView timer_icon3;
//	private ImageView timer_icon4;
//	private ImageView timer_icon5;

	private VanishingRecordPlayer recordPlayer;

	private TextView vanishingToolTip;

	private int beforIndex = 0;

	private boolean beforNewLine = false;
	private boolean isNewLine = false;

	public VanishingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub

		this.mContext = context;
		this.mInflater = LayoutInflater.from(context);

		if (CommonUtil.isCenter()) // igse
			mInflater.inflate(R.layout.igse_vanishing_cloze, this, true);
		else {
			if ("1".equals(Preferences.getLmsStatus(mContext))) // 우영
				mInflater.inflate(R.layout.w_vanishing_cloze, this, true);
			else // 숲
				mInflater.inflate(R.layout.f_vanishing_cloze, this, true);
		}

		initLayout();
	}

	/**
	 * 레이아웃 초기화
	 */
	private void initLayout() {
		txtStepInfo = (TextView) findViewById(R.id.vanishing_cloze_step_info);
		txtQuestionCnt = (TextView) findViewById(R.id.vanishing_cloze_question_count_text);
		textLinearLayout = (LinearLayout) findViewById(R.id.text_linear);

//		timer_icon1 = (ImageView) findViewById(R.id.timer_icon1);
//		timer_icon2 = (ImageView) findViewById(R.id.timer_icon2);
//		timer_icon3 = (ImageView) findViewById(R.id.timer_icon3);
//		timer_icon4 = (ImageView) findViewById(R.id.timer_icon4);
//		timer_icon5 = (ImageView) findViewById(R.id.timer_icon5);

		recordPlayer = (VanishingRecordPlayer) findViewById(R.id.vanishing_cloze_three_btn_parent_layout);
		recordPlayer.setOnVanishingRecorderListener(this);

		vanishingToolTip = (TextView) findViewById(R.id.vanishing_tooltip_text);
	}

	/**
	 * 타이머 초기화
	 */
	public void initTimer() {
//		timer_icon1.setBackgroundResource(R.drawable.timer_buble_dis);
//		timer_icon2.setBackgroundResource(R.drawable.timer_buble_dis);
//		timer_icon3.setBackgroundResource(R.drawable.timer_buble_dis);
//		timer_icon4.setBackgroundResource(R.drawable.timer_buble_dis);
//		timer_icon5.setBackgroundResource(R.drawable.timer_buble_dis);
	}

	/**
	 * 학습 정보 설정
	 * 
	 * @param info
	 */
	public void setStepInfo(String info) {
		txtStepInfo.setText(info);
	}

	/**
	 * 현재 문단 연습 index 설정
	 * 
	 * @param cnt
	 */
	public void setQuestionCnt(String cnt) {
		txtQuestionCnt.setText(cnt);
	}

	/**
	 * 문단 연습 레이아웃 설정
	 * 
	 * @param word
	 */
	public void setVanishingWord(VanishingWord word) {
		textLinearLayout.removeAllViews();
		int len = 0;
		int lenLevel = 0;
		beforIndex = 0;
		ArrayList<String> list = word.getStrList();
		int is_small = isSmaillFont(list);

		if (word.getTextSentence().length() > ServiceCommon.VANISHING_TEXT_SHORT) {
			if (word.getTextSentence().length() < ServiceCommon.VANISHING_TEXT_LONG)
				lenLevel = 1;
			else
				lenLevel = 2;
		}

		for (int i = 0; i < list.size(); i++) {
			String str = list.get(i);
			VanishingWordLayout layout = new VanishingWordLayout(mContext, str, is_small, lenLevel, isNewLine);
			len += str.length();
			layout.setId(i);
			layout.setLineColor(Color.BLACK);
			layout.setTextColor(Color.BLACK);
			addTextLinear(layout, len, is_small, lenLevel);
			//textLinearLayout.addView(layout);
		}
	}

	/**
	 * 작은 폰트 적용할지 여부 반환
	 * 
	 * @param list
	 * @return
	 */
	private int isSmaillFont(ArrayList<String> list) {
		int line = 1;
		int len = 0;
		int before = 0;
		int newline = 0;

		for (int i = 0; i < list.size(); i++) {
			String str = list.get(i);
			len += str.length();

			if (isNewLineWord(str)) {
				newline++;
				continue;
			}

			if (len - before <= ServiceCommon.MAX_VANISHING_LENGTH) {

			} else {
				before = len - str.length();
				line++;
			}
		}
		if (newline > 0) {
			isNewLine = true;
		}

		return line + newline;
//		if(line + newline > 2){
//			return true;
//		}
//		return false;
	}

	private boolean isNewLineWord(String word) {
		if (word.contains("\n")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 문단 연습 Text Linear Layout에 문단 연습 단어 Layout 배치
	 * 
	 * @param layout
	 * @param cur_len
	 * @param isSmall
	 */
	private void addTextLinear(VanishingWordLayout layout, int cur_len, int isSmall, int lenLevel) {
		int cnt = textLinearLayout.getChildCount();
		String txt = layout.getWord();

		if (isNewLineWord(txt)) {
			beforNewLine = true;
			return;
		}

		int vanishingRevise = ServiceCommon.VANISHING_REVISE;
		int maxVanishingLength = ServiceCommon.MAX_VANISHING_LENGTH;

		int revise = isSmall > 2 ? vanishingRevise : 0;

		if (lenLevel == 1)
			revise = ServiceCommon.VANISHING_REVISE_MIDDLE;
		if (lenLevel == 2 || (isSmall > 3 && isNewLine))
			revise = ServiceCommon.VANISHING_REVISE_LONG;
		if (cnt == 0) {
			beforIndex = 0;
			if (beforNewLine) {
				beforNewLine = false;
			}
			if (" ".equals(txt) || "".equals(txt)) { //행 첫 단어가 공백인 경우 SKIP
				return;
			}
			LinearLayout linear = new LinearLayout(mContext);
			linear.setOrientation(LinearLayout.HORIZONTAL);
			linear.addView(layout);
			textLinearLayout.addView(linear);
		} else {
			if (isPunctuation(txt) || (cur_len - beforIndex <= maxVanishingLength + revise && !beforNewLine)) {
				LinearLayout linear = (LinearLayout) textLinearLayout.getChildAt(cnt - 1);
				linear.addView(layout);
			} else {
				if (" ".equals(txt)) { //행 첫 단어가 공백인 경우 SKIP
					return;
				}
				LinearLayout linear = new LinearLayout(mContext);
				linear.setOrientation(LinearLayout.HORIZONTAL);
				linear.addView(layout);
				textLinearLayout.addView(linear);
				beforIndex = cur_len - txt.length();
				if (beforNewLine) {
					beforNewLine = false;
				}
			}
		}
	}

	/**
	 * 해당단어가 구두점인지 여부
	 * 
	 * @param txt
	 */
	private boolean isPunctuation(String txt) {
		if ("?".equals(txt) || "!".equals(txt) || ".".equals(txt) || ",".equals(txt)
				|| "?\" ".equals(txt) || "!\" ".equals(txt)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 배치된 문단 연습 단어 Layout에 fade in 애니메이션 효과를 실행
	 * 
	 * @param list
	 */
	public void showVanishingFadeOut(ArrayList<Integer> list) {

		for (int i = 0; i < textLinearLayout.getChildCount(); i++) {
			LinearLayout linear = (LinearLayout) textLinearLayout.getChildAt(i);

			for (int j = 0; j < linear.getChildCount(); j++) {
				VanishingWordLayout layout = (VanishingWordLayout) linear.getChildAt(j);
				for (int k = 0; k < list.size(); k++) {
					if (layout.getId() == list.get(k)) {
						layout.setLineVisual(View.VISIBLE);
						layout.startAnimation();
					}
				}
			}
		}
	}

	/**
	 * Black Coloer 설정
	 * 
	 * @param list
	 */
	public void showRedLineTextBlack(ArrayList<Integer> list) {

		for (int i = 0; i < textLinearLayout.getChildCount(); i++) {
			LinearLayout linear = (LinearLayout) textLinearLayout.getChildAt(i);
			for (int j = 0; j < linear.getChildCount(); j++) {
				VanishingWordLayout layout = (VanishingWordLayout) linear.getChildAt(j);
				for (int k = 0; k < list.size(); k++) {
					if (layout.getId() == list.get(k)) {
						layout.setLineVisual(View.VISIBLE);
						layout.setWordVisual(View.VISIBLE);
						layout.setLineColor(Color.BLACK);
						layout.setTextColor(Color.BLACK);
					}
				}
			}
		}

	}

	/**
	 * Red Color 설정
	 * 
	 * @param list
	 */
	public void showRedLineTextRed(ArrayList<Integer> list) {

		for (int i = 0; i < textLinearLayout.getChildCount(); i++) {
			LinearLayout linear = (LinearLayout) textLinearLayout.getChildAt(i);
			for (int j = 0; j < linear.getChildCount(); j++) {
				VanishingWordLayout layout = (VanishingWordLayout) linear.getChildAt(j);
				for (int k = 0; k < list.size(); k++) {
					if (layout.getId() == list.get(k)) {
						layout.setLineVisual(View.VISIBLE);
						layout.setWordVisual(View.VISIBLE);
						layout.setLineColor(Color.RED);
						layout.setTextColor(Color.RED);
					}
				}
			}
		}
	}

	/**
	 * 학습 가이드를 표시함
	 * 
	 * @param tip
	 *            학습 가이드 내용
	 */
	public void showToolTip(int visual) {

		vanishingToolTip.setVisibility(visual);
	}

	/**
	 * 5초 카운터 시작
	 */
	public void setCountDownTimer() {

//		mCountTimer = new CountDownTimer(7000, 1000) {
//			
//			@Override
//			public void onTick(long millisUntilFinished) {
//				// TODO Auto-generated method stub
////				if(millisUntilFinished > 4000 && millisUntilFinished < 5000) {
////					timer_icon5.setBackgroundResource(R.drawable.timer_buble);
////				} else if(millisUntilFinished > 3000 && millisUntilFinished < 4000) {
////					timer_icon5.setBackgroundResource(R.drawable.timer_buble);
////					timer_icon4.setBackgroundResource(R.drawable.timer_buble);
////				} else if(millisUntilFinished > 2000 && millisUntilFinished < 3000) {
////					timer_icon5.setBackgroundResource(R.drawable.timer_buble);
////					timer_icon4.setBackgroundResource(R.drawable.timer_buble);
////					timer_icon3.setBackgroundResource(R.drawable.timer_buble);
////				} else if(millisUntilFinished > 1000 && millisUntilFinished < 2000) {
////					timer_icon5.setBackgroundResource(R.drawable.timer_buble);
////					timer_icon4.setBackgroundResource(R.drawable.timer_buble);
////					timer_icon3.setBackgroundResource(R.drawable.timer_buble);
////					timer_icon2.setBackgroundResource(R.drawable.timer_buble);
////				} else {
////					
////				}
//			}
//			
//			@Override
//			public void onFinish() {
//				// TODO Auto-generated method stub
////				timer_icon5.setBackgroundResource(R.drawable.timer_buble);
////				timer_icon4.setBackgroundResource(R.drawable.timer_buble);
////				timer_icon3.setBackgroundResource(R.drawable.timer_buble);
////				timer_icon2.setBackgroundResource(R.drawable.timer_buble);
////				timer_icon1.setBackgroundResource(R.drawable.timer_buble);
//				
//				((VanishingExamActivity)mContext).endFiveTimeCounter();
//			}
//		};
		mCountTimer.start();
	}

	public void stopCountDownTimer() {
		mCountTimer.cancel();
	}

	/**
	 * 재생할 ogg파일을 설정함
	 * 
	 * @param oggFilePath
	 */
	public void setAudioPlay(String oggFilePath) {
		recordPlayer.setAudioPlay(oggFilePath);
	}

	/**
	 * 재생 중지
	 * 
	 * @param oggFilePath
	 */
	public void stopAudioPlay() {
		recordPlayer.stopAudioPlay();
	}

	/**
	 * 녹음할 파일을 설정함
	 * 
	 * @param oggFilePath
	 */
	public void setRecordPlayer(String path, int maxRecTime) {

		recordPlayer.setRecordPlayer(path, maxRecTime);
	}

	/**
	 * 녹음 중지
	 * 
	 * @param oggFilePath
	 */
	public void stopRecordPlay() {
		recordPlayer.stopRecordPlay();
	}

	/**
	 * 에코 재생할 파일을 설정함
	 * 
	 * @param path
	 * @return
	 */
	public int setEchoPlayer(String path) {
		//initTimer();
		return recordPlayer.setEchoPlay(path);
	}

	@Override
	public void OnVorbisPlayComplete(int duration) {
		// TODO Auto-generated method stub
		((VanishingExamActivity) mContext).sendVorbisComplete(duration);
	}

	@Override
	public void onRecordComplete() {
		// TODO Auto-generated method stub sendRecordComplete
		((VanishingExamActivity) mContext).sendRecordComplete();
	}

	@Override
	public void onEchoComplete() {
		// TODO Auto-generated method stub
		((VanishingExamActivity) mContext).sendEchoComplete();
	}

	/**
	 * 문단연습 플레어 리소스를 해재함..
	 */
	public void recordPlayDestroy() {
		recordPlayer.playerDestory();
	}

}
