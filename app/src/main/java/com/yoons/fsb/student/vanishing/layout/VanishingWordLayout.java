package com.yoons.fsb.student.vanishing.layout;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.Log;

/**
 * 문단 연습 단어 레이아웃 Text 와 Line 으로 구성 fade in 애니메이션 동작
 *
 * @author nexmore
 */
public class VanishingWordLayout extends LinearLayout implements AnimationListener {

    private TextView mWord;
    private ImageView mLine;
    private Animation fadeOutAnimation;

    public VanishingWordLayout(Context context, String info, int isSmall, int lenLevel, boolean isNewLine) {
        super(context);
        Log.d("info", "" + info);
        // TODO Auto-generated constructor stub
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.vanishing_item, this, true);

        mWord = (TextView) findViewById(R.id.vanishing_txt);
        mWord.setTextColor(Color.BLACK);
        if (isSmall > 2) {
            if (CommonUtil.isVT10Device() || CommonUtil.isYS20Device()) {
                if (isSmall > 3 && isNewLine) {
                    mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_19));
                } else {
                    if (lenLevel == 0) {
                        mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_24));
                    } else if (lenLevel == 1) {
                        mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_23));
                    } else {
                        mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_21));
                    }
                }
            } else {
                if (isSmall > 3 && isNewLine) {
                    mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_21));
                } else {
                    if (lenLevel == 0) {
                        mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_28));
                    } else if (lenLevel == 1) {
                        mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_26));
                    } else {
                        mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_22));
                    }
                }
            }
        } else {
            if (CommonUtil.isVT10Device() || CommonUtil.isYS20Device()) {
                mWord.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dimen_32));
            }
        }
        mWord.setTypeface(null, Typeface.BOLD);
        mLine = (ImageView) findViewById(R.id.vanishing_line);
        mLine.setBackgroundColor(Color.BLACK);

        mWord.setText(info);

        fadeOutAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_out);

        fadeOutAnimation.setAnimationListener(this);
    }

    public void setWord(String text) {
        mWord.setText(text);
    }

    public String getWord() {
        return mWord.getText().toString();
    }

    public void setLineColor(int coler) {
        mLine.setBackgroundColor(coler);
    }

    public void setTextColor(int color) {
        mWord.setTextColor(color);
    }

    public int getLength() {
        return mWord.getText().length();
    }

    public void setLineVisual(int type) {
        mLine.setVisibility(type);
    }

    public void setWordVisual(int type) {
        mWord.setVisibility(type);
    }

    /**
     * Fade in Animation 시작
     */
    public void startAnimation() {
        mWord.startAnimation(fadeOutAnimation);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub
        mWord.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub
    }
}
