package com.yoons.fsb.student.vanishing.state;

/**
 * 문단 연습 State
 * @author nexmore
 *
 */
public interface VanishingState {
	
	public void Init();
	
	public void Destory();
	
	public void playerComplete(int duration);
	
	public void endTimeCounter();
	
	public void reocordComplete();
	
	public void echoComplete();
}
