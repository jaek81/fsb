package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;
import android.view.View;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;
import com.yoons.fsb.student.vanishing.state.VanishingState;

/**
 * 학습 준비 State
 * 최초 문단 연습 음원 재생
 * 
 * @author nexmore
 *
 */
public class StepInit implements VanishingState{
	
	private Context mContext;
	private VanishingView mView;
	private VanishingWord mWord;
	
	public StepInit(Context context, VanishingView view, VanishingWord word){
		this.mContext = context;
		this.mView = view;
		this.mWord = word;
	}

	@Override
	public void Init() {
		// TODO Auto-generated method stub
		mView.setStepInfo("학습 준비");
		mView.setVanishingWord(mWord);
		//mView.setBtnListenEnable();
		mView.setAudioPlay(mWord.getAudioPath());
		
		mView.findViewById(R.id.text_linear).setVisibility(View.VISIBLE);
		mView.findViewById(R.id.text_speaker).setVisibility(View.GONE);
	}

	@Override
	public void Destory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerComplete(int duration) {
		// TODO Auto-generated method stub
		((VanishingExamActivity)mContext).changeState(new StepStudyOne(mContext, mView, mWord, duration));
	}

	@Override
	public void endTimeCounter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reocordComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void echoComplete() {
		// TODO Auto-generated method stub
		
	}

}
