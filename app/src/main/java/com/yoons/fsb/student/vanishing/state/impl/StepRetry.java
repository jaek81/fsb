package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;
import android.view.View;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;
import com.yoons.fsb.student.vanishing.state.VanishingState;

/**
 * 재도전 State
 * 문단 연습 음원 재생 후 학습Step.3로 이동한다.
 * 
 * @author nexmore
 *
 */
public class StepRetry implements VanishingState{
	
	private Context mContext;
	private VanishingView mView;
	private VanishingWord mWord;
	
	public StepRetry(Context context, VanishingView view, VanishingWord word){
		this.mContext = context;
		this.mView = view;
		this.mWord = word;
	}

	@Override
	public void Init() {
		// TODO Auto-generated method stub
		mView.setStepInfo("학습 Step.3");
		
//		Map<Integer, ArrayList<Integer>> mapList = mWord.getMapList();
//		ArrayList<Integer> indexOne = mapList.get(1);
//		indexOne.addAll(mapList.get(2));
//		indexOne.addAll(mapList.get(3));
//		mView.showRedLineTextBlack(indexOne);
//		
//		mView.setAudioPlay(mWord.getAudioPath());
//		
//		mView.findViewById(R.id.text_linear).setVisibility(View.VISIBLE);
//		mView.findViewById(R.id.text_speaker).setVisibility(View.GONE);
		
		mView.findViewById(R.id.text_linear).setVisibility(View.GONE);
		mView.findViewById(R.id.text_speaker).setVisibility(View.VISIBLE);
		
		mView.initTimer();
		mView.setAudioPlay(mWord.getAudioPath());
	}

	@Override
	public void Destory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerComplete(int duration) {
		// TODO Auto-generated method stub
		((VanishingExamActivity)mContext).changeState(new StepStudyThree(mContext, mView, mWord, duration));
		
	}

	@Override
	public void endTimeCounter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reocordComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void echoComplete() {
		// TODO Auto-generated method stub
		
	}

}
