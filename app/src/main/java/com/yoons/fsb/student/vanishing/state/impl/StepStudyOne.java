package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;
import com.yoons.fsb.student.vanishing.state.VanishingState;

import java.util.ArrayList;
import java.util.Map;

/**
 * 학습 Step.1 State
 * 문단연습 Text가 보이고 <u1>태그에 대하여 fade in 에니메이션 효과를 줌
 * fade in 효과가 완료가 되면 5초 카운터가 시작이 되고 회원 음성녹음이 시작이 됨
 * 
 * @author nexmore
 *
 */
public class StepStudyOne implements OnClickListener, VanishingState{
	
	protected Context mContext;
	protected VanishingView mView;
	protected VanishingWord mWord;
	protected int mDuration;
	protected Button mSkip;
	protected boolean mIsRecording = false;
	protected boolean mIsSkip = false;
	
	public StepStudyOne(Context context, VanishingView view, VanishingWord word, int duration){
		this.mContext = context;
		this.mView = view;
		this.mWord = word;
		this.mDuration = duration;
		mWord.getVanishingWordList();
	}

	@Override
	public void Init() {
		// TODO Auto-generated method stub
		mView.findViewById(R.id.text_linear).setVisibility(View.VISIBLE);
		mView.findViewById(R.id.text_speaker).setVisibility(View.GONE);
		
		mView.setStepInfo("학습 Step.1");
		mView.showToolTip(View.VISIBLE);
		
		// VIP Skip(중학영어 3,6,9 차시)
		if (ServiceCommon.IS_CONTENTS_TEST || 
				(StudyData.getInstance().mIsVIPSkip && StudyData.getInstance().mStudyUnitCode.contains("V"))) {
			mSkip = (Button)mView.findViewById(R.id.vanishing_cloze_skip_btn);
			mSkip.setOnClickListener(this);
			mSkip.setEnabled(true);
			mSkip	.setVisibility(View.VISIBLE);
		}

		Map<Integer, ArrayList<Integer>> mapList = mWord.getMapList();
		ArrayList<Integer> indexOne = mapList.get(1);
		mView.showVanishingFadeOut(indexOne);
		
		mView.setCountDownTimer();
		
		
	}

	@Override
	public void Destory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerComplete(int duration) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endTimeCounter() {
		// TODO 5초 타이머  동작 완료, 음성 녹음 시작
		mIsRecording = true;
		mView.showToolTip(View.INVISIBLE);
		mView.setRecordPlayer(mWord.getRecodePath().trim(), mDuration);
	}

	@Override
	public void reocordComplete() {
		// TODO Auto-generated method stub
		if (mIsSkip) {
			((VanishingExamActivity)mContext).changeState(new StepStudyTwo(mContext, mView, mWord, mDuration));
		} else {
			((VanishingExamActivity)mContext).changeState(new StepStudyOneComplete(mContext, mView, mWord, mDuration));
		}
	}

	@Override
	public void echoComplete() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.vanishing_cloze_skip_btn:
			mSkip.setEnabled(false);
			mView.showToolTip(View.INVISIBLE);
			mIsSkip = true;
			if (mIsRecording) {
				mView.stopRecordPlay();
			} else {
				((VanishingExamActivity)mContext).changeState(new StepStudyTwo(mContext, mView, mWord, mDuration));
			}
			break;
		default:
			break;
		}
	}

}
