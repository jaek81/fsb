package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;
import com.yoons.fsb.student.vanishing.state.VanishingState;

/**
 * 학습 Step.1 완료 State 
 * 회원 녹음이 완료가 되면 문단연습 음원을 재생해주고 
 * 학습 Step.2로 이동한다.
 * @author nexmore
 *
 */
public class StepStudyOneComplete implements OnClickListener, VanishingState{
	
	protected Context mContext;
	protected VanishingView mView;
	protected VanishingWord mWord;
	protected int mDuration;
	protected int mState;
	protected Button mSkip;
	protected boolean mIsSkip = false;
	
	public StepStudyOneComplete(Context context, VanishingView view, VanishingWord word, int duration){
		this.mContext = context;
		this.mView = view;
		this.mWord = word;
		this.mDuration = duration;
	}

	@Override
	public void Init() {
		// TODO Auto-generated method stub
		mView.findViewById(R.id.text_linear).setVisibility(View.GONE);
		mView.findViewById(R.id.text_speaker).setVisibility(View.VISIBLE);
		
		if (ServiceCommon.IS_CONTENTS_TEST || 
				(StudyData.getInstance().mIsVIPSkip && StudyData.getInstance().mStudyUnitCode.contains("V"))) {
			mSkip = (Button)mView.findViewById(R.id.vanishing_cloze_skip_btn);
			mSkip.setOnClickListener(this);
			mSkip.setEnabled(true);
			mSkip.setVisibility(View.VISIBLE);
		}
		
		mView.initTimer();
		mView.setAudioPlay(mWord.getAudioPath());
		
	}

	@Override
	public void Destory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerComplete(int duration) {
		// TODO Auto-generated method stub
		((VanishingExamActivity)mContext).changeState(new StepStudyTwo(mContext, mView, mWord, duration));
	}

	@Override
	public void endTimeCounter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reocordComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void echoComplete() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.vanishing_cloze_skip_btn:
			mSkip.setEnabled(false);
			mView.stopAudioPlay();
			break;
		default:
			break;
		}
	}

}
