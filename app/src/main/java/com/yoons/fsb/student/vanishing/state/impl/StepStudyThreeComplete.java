package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;
import android.view.View;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.RecData;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.network.HttpMultiPart;
import com.yoons.fsb.student.util.CommonUtil;
import com.yoons.fsb.student.util.StudyDataUtil;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;
import com.yoons.fsb.student.vanishing.state.VanishingState;
import com.yoons.recognition.OnMSTT;
import com.yoons.recognition.VoiceRecognizerWeb;
import com.yoons.recognition.data.SentenceObj;
import com.yoons.recognition.util.NumberUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

/**
 * 학습 Step.3 완료 State 회원 녹음이 완료가 되면 <u1>,<u2>,<u3> 단어에 대하여 빨간색 으로 보여준 다음 회원 녹음된
 * 파일에 대하여 에코 재생과 음성인식 을 채점 한다.
 *
 * @author nexmore
 */
public class StepStudyThreeComplete implements VanishingState {

    private Context mContext;
    private VanishingView mView;
    private VanishingWord mWord;
    /* private VoiceRecognizerWeb_VC VRUTask = null; */
    private VoiceRecognizerWeb VRUTask = null;
    private int mVoiceScore = -99;
    private String mVoiceText = "";
    private int mEchoDuration = 0;
    private String fullRecordPath = "";

    private boolean isVoiceRecongize;
    private boolean isEchoing;

    public StepStudyThreeComplete(Context context, VanishingView view, VanishingWord word) {
        this.mContext = context;
        this.mView = view;
        this.mWord = word;
        isEchoing = false;
        isVoiceRecongize = false;
        mWord.getVanishingWordList();
    }

    @Override
    public void Init() {
        // TODO Auto-generated method stub
        mView.findViewById(R.id.text_linear).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.text_speaker).setVisibility(View.GONE);

        Map<Integer, ArrayList<Integer>> mapList = mWord.getMapList();
        ArrayList<Integer> indexOne = mapList.get(1);
        indexOne.addAll(mapList.get(2));
        indexOne.addAll(mapList.get(3));
        mView.showRedLineTextRed(indexOne);

        requestVoiceRecognize();

        fullRecordPath = mWord.getRecodePath().trim();

        File recordFile = new File(fullRecordPath);
        // 녹음 파일이 없는 경우
        if (!recordFile.exists()) {

            return;
        }

        mView.initTimer();
        mEchoDuration = mView.setEchoPlayer(fullRecordPath);

    }

    @Override
    public void Destory() {
        // TODO Auto-generated method stub

    }

    @Override
    public void playerComplete(int duration) {
        // TODO Auto-generated method stub

    }

    @Override
    public void endTimeCounter() {
        // TODO Auto-generated method stub

    }

    @Override
    public void reocordComplete() {
        // TODO Auto-generated method stub

    }

    @Override
    public void echoComplete() {
        // TODO Auto-generated method stub
        isEchoing = true;
        if (isVoiceRecongize) {
            ((VanishingExamActivity) mContext).stepComplete(mVoiceScore);
        }

    }

    private void requestVoiceRecognize() {
        mVoiceScore = -99;
        mVoiceText = "";

        String fullRecordPath = mWord.getRecodePath().trim();

        File recordFile = new File(fullRecordPath);
        // 녹음 파일이 없는 경우
        if (!recordFile.exists()) {
            setRecordResult(mVoiceText, mVoiceScore);
            return;
        }

        // 음성 평가 전 네트워크 상태 불가능의 경우
        if (!CommonUtil.isAvailableNetwork(mContext, false)) {
            setRecordResult(mVoiceText, mVoiceScore);
            return;
        }

        String[] recordPathArray = fullRecordPath.split("/");
        String recordFileName = recordPathArray[recordPathArray.length - 1];
        final String recordPath = fullRecordPath.replace(recordFileName, "").trim();
        if (!recordFileName.contains(ServiceCommon.WAV_FILE_TAIL))
            recordFileName += ServiceCommon.WAV_FILE_TAIL;

        try {
            VRUTask = new VoiceRecognizerWeb(mContext);

            VRUTask.mstt = new OnMSTT() {

                @Override
                public void getSentenceObj(SentenceObj senObj) {

                    // TODO Auto-generated method stub
                    mVoiceScore = NumberUtil.Half6(senObj.getTotal_score());
                    setRecordResult(senObj.getRec_sentence(), NumberUtil.Half6(senObj.getTotal_score()));
                    /*
					 * if (ServiceCommon.IS_CONTENTS_TEST) {
					 * mUIHandler.sendEmptyMessage(1); }
					 */
                }

            };

            /**
             * 2014-02-14 WCPM 반영
             */
            int customerNo = ((VanishingExamActivity) mContext).getCustomerNo();
            VRUTask.execute(recordFileName, recordPath, "16000", mWord.getTextSentence().trim(), String.valueOf(customerNo), mEchoDuration, StudyData.getInstance().mVanishingList, 0);

			/*
			 * SpeechUtil Su = new SpeechUtil();
			 * Su.execute(mWavRecorder.getmTempFilePath());
			 */

        } catch (Exception e) {
            e.printStackTrace();
        }
        //잠깐 보류
		/*
		 * try { VRUTask = new VoiceRecognizerWeb_VC(mContext);
		 * 
		 * VRUTask.mstt = new OnMSTT() {
		 *//**
         * score 점수 or MSLine 정의 오류 값
         *
         * -1 => 알 수 없는 에러 -2 => 클라이언트의 Wifi 확인(OFFLINE 이거나 RSSI 신호로 -75이하인
         * 경우) -3 => 구글 음성인식 서버의 결과값이 없을 경우(대체로 녹음이 빈파일 경우입니다.) -4 => 구글
         * 음성인식 서버의 문제 -5 => 스코어링 서버의 문제 -6 => NLU(Natural Language
         * Understanding) timeout -7 => SSU timeout
         *//*
				* public void setMSTTinish(int score) { Log.k("wusi12", "Score : "
				* + score); mVoiceScore = score; }
				* 
				* public void getHashMap(HashMap<Integer, String> text) {
				* 
				* if (null != text && 0 < text.size()) { for (int i = 0; i <
				* text.size(); i++) Log.i("", "Voice text" + i + " => " +
				* text.get(i)); mVoiceText = (String) text.get(0); }
				* Log.k("wusi12", "mVoiceText : " + mVoiceText);
				* setRecordResult(mVoiceText, mVoiceScore); } };
				* 
				* 
				* 
				* VRUTask.execute(recordFileName, recordPath,
				* "16000",mWord.getTextSentence(), //
				* String.valueOf(customerNo),mEchoDuration);
				* VRUTask.execute(recordFileName, recordPath,
				* "16000",mWord.getTextSentence(),
				* String.valueOf(customerNo),mEchoDuration,
				* mWord.getVanishingWordList(),0);
				* 
				* } catch (Exception e) { e.printStackTrace(); }
				*/
    }

    /**
     * 음성 인식 결과를 설정함
     *
     * @param text  음성 인식된 텍스트
     * @param score 음성 인식 하여 채점된 점수 or 오류 코드
     */
    private void setRecordResult(String text, int score) {
        //StudyDataUtil.addVanishingClozeResult(text, score);
        //Log.k("wusi12", "requestVoiceRecognize end  : " + score);
        setStudyVanishingResult();

        StudyData studyData = StudyData.getInstance();
        StudyData data = studyData.getInstance();

        RecData recData = new RecData();
        recData.customer_no = data.mCustomerNo;
        recData.product_no = data.mProductNo;
        try {
            recData.study_unit_code = URLEncoder.encode(data.mStudyUnitCode, "UTF-8").toUpperCase();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        recData.re_study_cnt = data.mReStudyCnt;
        recData.study_kind = 0;
        recData.study_order = mWord.getVanishingQuestion().mQuestionOrder;
        recData.is_correct = score;
        recData.is_correct_answer = studyData.mVanishingResult.get(studyData.mVanishingResult.size() - 1).mCorrectAnswer;
        recData.is_customer_answer = text;
        recData.sentence_question_no = studyData.mVanishingResult.get(studyData.mVanishingResult.size() - 1).mVanishingQuestionNo;
        recData.study_result_no = data.mStudyResultNo;
        recData.file_path = fullRecordPath.replace("wav", "mp3");
        recData.re_study_cnt = studyData.mVanishingResult.get(0).mRetryCnt;
        if (CommonUtil.isCenter()) {//중앙화여부
            recData.url = StudyDataUtil.addPackageName("QXNwX0xfU1RVRFlfUEFfU0VOVEVOQ0VEVExfRg==");

        } else {
            recData.url = StudyDataUtil.addPackageName("U0JNVVNFUi5TQk1fUEtHX1NfUlQuU1BfTV9SVF9TVFVEWV9QQV9TRU5URU5DRURUTA==");
        }

        HttpMultiPart multiPart = new HttpMultiPart();

        multiPart.execute(recData);

        isVoiceRecongize = true;
        if (isEchoing) {
            ((VanishingExamActivity) mContext).stepComplete(mVoiceScore);
        }
    }

    private void setStudyVanishingResult() {
        //Log.k("wusi12", "setStudyVanishingResult");

        //String query = String.format(DatabaseDefine.INSERT_SBM_STUDY_VANISHING_DETAIL, data.mCustomerNo, data.mProductNo, quote(data.mStudyUnitCode), data.mReStudyCnt, 1, result.mQuestionOrder, result.mIsCorrect, quote(result.mCorrectAnswer.replace("\"", "\"\"")), quote(result.mCustomerAnswer.replace("\"", "\"\"")), 0, result.mRetryCnt, result.mVanishingQuestionNo, quote(currentDt), quote("C"), data.mStudyResultNo);

        StudyDataUtil.addVanishingResult(mVoiceText, mVoiceScore, mWord.getVanishingQuestion());
    }

}
