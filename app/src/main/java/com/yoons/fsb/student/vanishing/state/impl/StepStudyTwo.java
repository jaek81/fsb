package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.yoons.fsb.student.R;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;

import java.util.ArrayList;
import java.util.Map;

/**
 * 학습 Step.2 State 문단연습 Text가 보이고 <u1>,<u2>태그에 대하여 fade in 에니메이션 효과를 줌 fade in
 * 효과가 완료가 되면 5초 카운터가 시작이 되고 회원 음성녹음이 시작이 됨
 * 
 * @author nexmore
 *
 */
public class StepStudyTwo extends StepStudyOne {

	public StepStudyTwo(Context context, VanishingView view, VanishingWord word, int duration) {
		super(context, view, word, duration);
		// TODO Auto-generated constructor stub

		StudyData.getInstance().mVanishingList = word.getVanishingWordList();

	}

	@Override
	public void Init() {
		// TODO Auto-generated method stub
		//super.Init();
		mView.findViewById(R.id.text_linear).setVisibility(View.VISIBLE);
		mView.findViewById(R.id.text_speaker).setVisibility(View.GONE);

		mView.setStepInfo("학습 Step.2");
		mView.showToolTip(View.VISIBLE);

		if (ServiceCommon.IS_CONTENTS_TEST || (StudyData.getInstance().mIsVIPSkip && StudyData.getInstance().mStudyUnitCode.contains("V"))) {
			mSkip = (Button) mView.findViewById(R.id.vanishing_cloze_skip_btn);
			mSkip.setOnClickListener(this);
			mSkip.setEnabled(true);
			mSkip.setVisibility(View.VISIBLE);
		}

		Map<Integer, ArrayList<Integer>> mapList = mWord.getMapList();
		ArrayList<Integer> indexOne = mapList.get(1);
		indexOne.addAll(mapList.get(2));
		mView.showVanishingFadeOut(indexOne);

		mView.setCountDownTimer();
	}

	@Override
	public void reocordComplete() {
		// TODO Auto-generated method stub
		//super.reocordComplete();
		if (mIsSkip) {
			((VanishingExamActivity) mContext).changeState(new StepStudyThree(mContext, mView, mWord, mDuration));
		} else {
			((VanishingExamActivity) mContext).changeState(new StepStudyTwoComplete(mContext, mView, mWord, mDuration));
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.vanishing_cloze_skip_btn:
			mSkip.setEnabled(false);
			mView.showToolTip(View.INVISIBLE);
			mIsSkip = true;
			if (mIsRecording) {
				mView.stopRecordPlay();
			} else {
				((VanishingExamActivity) mContext).changeState(new StepStudyThree(mContext, mView, mWord, mDuration));
			}
			break;
		default:
			break;
		}
	}

}
