package com.yoons.fsb.student.vanishing.state.impl;

import android.content.Context;

import com.yoons.fsb.student.vanishing.VanishingExamActivity;
import com.yoons.fsb.student.vanishing.data.VanishingWord;
import com.yoons.fsb.student.vanishing.layout.VanishingView;

/**
 * 학습 Step.2 완료 State 
 * 회원 녹음이 완료가 되면 문단연습 음원을 재생해주고 
 * 학습 Step.3로 이동한다.
 * @author nexmore
 *
 */
public class StepStudyTwoComplete extends StepStudyOneComplete{

	public StepStudyTwoComplete(Context context, VanishingView view,
			VanishingWord word, int duration) {
		super(context, view, word, duration);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void playerComplete(int duration) {
		// TODO Auto-generated method stub
		//super.playerComplete(duration);
		((VanishingExamActivity)mContext).changeState(new StepStudyThree(mContext, mView, mWord, duration));
	}

}
