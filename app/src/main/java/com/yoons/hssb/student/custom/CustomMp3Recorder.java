package com.yoons.hssb.student.custom;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.os.Handler;

import com.crashlytics.android.Crashlytics;
import com.yoons.fsb.student.ServiceCommon;
import com.yoons.fsb.student.data.StudyData;
import com.yoons.hssb.student.util.SimpleLame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidParameterException;

/**
 * mp3 Recorder Class mp3lame library, android.media.AudioRecord 사용
 *
 * @author jaek
 */
public class CustomMp3Recorder {

    static {
        System.loadLibrary("mp3lame");
    }

    private Handler mHandler = null;

    private Thread mRecordThread = null;

    private String mFilePath = "";

    private int mSampleRate = 0;

    private boolean mIsRecording = false;

    public CustomMp3Recorder(Handler handler, String filePath, int sampleRate) {
        if (0 >= sampleRate)
            throw new InvalidParameterException("Invalid sample rate specified.");

        mFilePath = filePath;
        mSampleRate = sampleRate;
        mHandler = handler;
    }

    public CustomMp3Recorder(Handler handler, int sampleRate) {
        if (0 >= sampleRate)
            throw new InvalidParameterException("Invalid sample rate specified.");

        mHandler = handler;
        mSampleRate = sampleRate;
    }

    public CustomMp3Recorder(String filePath, int sampleRate) {
        if (0 >= sampleRate)
            throw new InvalidParameterException("Invalid sample rate specified.");

        mFilePath = filePath;
        mSampleRate = sampleRate;
    }

    /**
     * Handler를 설정함
     *
     * @param handler 녹음의 시작,끝,에러 메세지 전달을 위한 Handler
     */
    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    /**
     * 파일 경로를 설정함
     *
     * @param filePath 최종 녹음 mp3 파일 경로
     */
    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    /**
     * 녹음을 시작함
     */
    public void start() {
        if (mIsRecording)
            return;

        mRecordThread = new Thread("mp3RecorderThread") {
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

                final int minBufferSize = AudioRecord.getMinBufferSize(mSampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

                if (minBufferSize < 0) {
                    if (null != mHandler)
                        mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_GET_MIN_BUFFERSIZE, 0));
                    return;
                }

                // "W/AudioFlinger(75): RecordThread: buffer overflow
                AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, mSampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 2);

                // PCM buffer size
                short[] buffer = new short[mSampleRate * (16 / 8) * 1 * 2]; // SampleRate[Hz] * 16bit * Mono * 2sec
                byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];

                FileOutputStream output = null;
                try {
                    output = new FileOutputStream(new File(mFilePath));
                } catch (FileNotFoundException e) {
                    if (null != mHandler)
                        mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_CREATE_FILE, 0));
                    return;
                }

                // Lame init
                SimpleLame.init(mSampleRate, 1, mSampleRate, 32);

                mIsRecording = true;
                try {
                    try {
                        audioRecord.startRecording();
                        StudyData.getInstance().audioPlaying = ServiceCommon.STATUS_REC;
                    } catch (IllegalStateException e) {
                        if (null != mHandler)
                            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_REC_START, 0));
                        return;
                    }

                    try {
                        if (null != mHandler)
                            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_START, 0));

                        int readSize = 0;
                        while (null != mRecordThread && !mRecordThread.isInterrupted()) {
                            readSize = audioRecord.read(buffer, 0, minBufferSize);

                            if (0 > readSize) {
                                if (null != mHandler)
                                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_AUDIO_RECORD, 0));
                                break;
                            } else {
                                if (0 < readSize) {
                                    int encResult = SimpleLame.encode(buffer, buffer, readSize, mp3buffer);

                                    if (0 > encResult) {
                                        if (null != mHandler)
                                            mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_AUDIO_ENCODE, 0));
                                        break;
                                    }

                                    if (0 < encResult) {
                                        try {
                                            output.write(mp3buffer, 0, encResult);
                                        } catch (IOException e) {
                                            if (null != mHandler)
                                                mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_WRITE_FILE, 0));
                                            break;
                                        }
                                    }

                                    int bufferReadResult = readSize;
                                    double lastLevel = 0;
                                    double sumLevel = 0;
                                    for (int i = 0; i < bufferReadResult; i++) {
                                        sumLevel += buffer[i];
                                    }
                                    lastLevel = Math.abs((sumLevel / bufferReadResult));
                                    ServiceCommon.RECORD_VOLUME = (int)lastLevel;
                                }
                            }
                        }

                        int flushResult = SimpleLame.flush(mp3buffer);
                        if (0 > flushResult) {
                            if (null != mHandler)
                                mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_AUDIO_ENCODE, 0));
                        }

                        if (0 < flushResult) {
                            try {
                                output.write(mp3buffer, 0, flushResult);
                            } catch (IOException e) {
                                if (null != mHandler)
                                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_WRITE_FILE, 0));
                            }
                        }

                        try {
                            output.close();
                        } catch (IOException e) {
                            if (null != mHandler)
                                mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_ERROR_CLOSE_FILE, 0));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    } finally {
                        audioRecord.stop();
                        audioRecord.release();
                    }
                } finally {
                    SimpleLame.close();
                    mIsRecording = false;
                }

                if (null != mHandler)
                    mHandler.sendMessage(mHandler.obtainMessage(ServiceCommon.MSG_WHAT_RECORDER, ServiceCommon.MSG_REC_END, 0));
            }
        };

        mRecordThread.start();
    }

    /**
     * 녹음을 종료함
     */
    public void stop() {
        if (null != mRecordThread) {
            mRecordThread.interrupt();
            mIsRecording = false;
            mRecordThread = null;
        }
    }

    /**
     * 녹음의 진행 여부를 반환함
     */
    public boolean isRecording() {
        return mIsRecording;
    }

    /**
     * 녹음시간 파일에서 가져옴
     *
     * @return
     */
    public int getDuration() {
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(mFilePath);

        String sDuration = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

        return Integer.valueOf(sDuration);
    }
}
