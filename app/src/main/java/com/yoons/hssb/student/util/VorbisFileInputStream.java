package com.yoons.hssb.student.util;

import com.yoons.fsb.student.data.VorbisData;

import java.io.Closeable;
import java.io.IOException;

/**
 * VorbisFileInputStream Class
 */
public class VorbisFileInputStream implements Closeable {

	private final int oggStreamIdx;

	private final VorbisData info;

	public VorbisData getInfo() {
		return info;
	}

	static {
		System.loadLibrary("ogg");
		System.loadLibrary("vorbis");
		System.loadLibrary("vorbis-stream");
	}

	/**
	 * Opens a file for reading and parses any comments out of the file header.
	 */
	public VorbisFileInputStream(String fname) throws Exception {

		info = new VorbisData();
		oggStreamIdx = this.create(fname, info);
		//Log.e("hy", "VorbisFileInputStream -->" + oggStreamIdx+"fname="+fname);
	}

	@Override
	public void close() throws IOException {
		try {
			//Log.e("hy", "close() -->" + oggStreamIdx);
			this.closeStreamIdx(oggStreamIdx);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the interleaved PCM data from the vorbis stream.
	 * 
	 * @param pcmBuffer
	 * @return
	 * @throws IOException
	 */
	public int read(short[] pcmBuffer) throws Exception {
		//Log.e("hy", "read()1 -->" + oggStreamIdx+"length"+pcmBuffer.length);
		return this.readStreamIdx(oggStreamIdx, pcmBuffer, 0, pcmBuffer.length);
	}

	/**
	 * Returns interleaved PCM data from the vorbis stream.
	 * 
	 * @param pcmBuffer
	 * @param offset
	 * @param length
	 * @return
	 * @throws IOException
	 */
	public int read(short[] pcmBuffer, int offset, int length) throws Exception {
		//Log.e("hy", "read()2 -->" + pcmBuffer + "," + "offset" + offset + "," + "length" + length);
		return this.readStreamIdx(oggStreamIdx, pcmBuffer, offset, length);
	}

	/**
	 * Returns interleaved PCM data from the vorbis stream.
	 * 
	 * @return
	 * @throws IOException
	 */
	public short read() throws Exception {
		short buf[] = new short[1];
		//Log.e("hy", "read()3 -->" + oggStreamIdx);
		if (this.readStreamIdx(oggStreamIdx, buf, 0, 1) == -1)
			return -1;
		else
			return buf[0];
	}

	/**
	 * 
	 * 
	 * @param seekTime
	 * @return
	 */
	public int seekTo(double seekTime) throws Exception {
		try {
			//Log.e("hy", "seekTo -->" + oggStreamIdx);
			return timeSeekIdx(oggStreamIdx, seekTime);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}

	}

	/**
	 * 
	 * @return
	 */
	public double getCurrentPosition() {
		try {
			//Log.e("hy", "getCurrentPosition -->" + oggStreamIdx);
			return timeTellIdx(oggStreamIdx);
		} catch (Exception e) {
			e.printStackTrace();
			return 0.0d;
		}
	}

	/**
	 * 
	 * @return
	 */
	public double getDuration() {
		try {
			//Log.e("hy", "getDuration() -->" + oggStreamIdx);
			return timeTotalIdx(oggStreamIdx);
		} catch (Exception e) {
			e.printStackTrace();
			return 0.0d;
		}
	}

	private native int create(String fname, VorbisData info) throws Exception;

	private native void closeStreamIdx(int sidx) throws Exception;

	/**
	 * This just returns all the channels interleaved together. I assume this is
	 * how android wants it.
	 * 
	 * @param pcm
	 * @return
	 * @throws IOException
	 */
	private native int readStreamIdx(int sidx, short[] pcm, int offset, int size) throws Exception;

	/**
	 * Skips over the number of samples specified. This skip doesn't account for
	 * channels.
	 * 
	 * @param samples
	 * @return
	 * @throws IOException
	 */
	private native long skipStreamIdx(int sidx, long samples) throws Exception;

	/**
	 * 
	 * @param time
	 * @return
	 */
	private native int timeSeekIdx(int sidx, double time) throws Exception;

	/**
	 * 
	 * @return
	 */
	private native double timeTellIdx(int sidx) throws Exception;

	/**
	 * 
	 * @return
	 */
	private native double timeTotalIdx(int sidx) throws Exception;
}
